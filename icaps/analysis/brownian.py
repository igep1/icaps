import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import cv2
import icaps
from icaps.presentation.ProgressBar import SimpleProgressBar
from icaps.io import mk_folder, mk_folders, load_img, save_img, generate_file, get_files, get_frame
from icaps.processing.tables import move_col, row2dict
from icaps.analysis.fitting import *
from icaps.processing.filter import threshold
from icaps.processing.prep import fill_holes
import warnings
from deprecation import deprecated
import matplotlib as mpl
from scipy.optimize import fsolve


def filter_rel_err(df, mass=None, tau=None, mode="trans", ret_count=False, id_column="particle", axes=None, log=None,
                   mass_label=None, tau_label=None):
    if axes is None:
        axes = ("x", "y")
    if isinstance(axes, str):
        axes = [axes]
    count = {}
    if mode == "trans":
        if mass is not None:
            for axis in axes:
                df = df[df["mass_{}_err".format(axis)] / df["mass_{}".format(axis)] <= mass]
            count["mass"] = len(df[id_column].unique())
            if log is not None:
                if len(axes) == 1:
                    if mass_label is None:
                        mass_label = "mass_{}_rel_err"
                    if "{}" in mass_label:
                        mass_label = mass_label.format(axes[0])
                elif mass_label is None:
                    mass_label = "mass_rel_err"
        if tau is not None:
            for axis in axes:
                df = df[df["tau_{}_err".format(axis)] / df["tau_{}".format(axis)] <= tau]
            count["tau"] = len(df[id_column].unique())
            if log is not None:
                if len(axes) == 1:
                    if tau_label is None:
                        tau_label = "tau_{}_rel_err"
                    if "{}" in tau_label:
                        tau_label = tau_label.format(axes[0])
                elif tau_label is None:
                    tau_label = "tau_rel_err"
    else:
        if mass is not None:
            df = df[df["inertia_err"] / df["inertia"] <= mass]
            count["inertia"] = len(df[id_column].unique())
            if log is not None:
                if mass_label is None:
                    mass_label = "moi_rel_err"
        if tau is not None:
            df = df[df["tau_rot_err"] / df["tau_rot"] <= tau]
            count["tau_rot"] = len(df[id_column].unique())
            if log is not None:
                if tau_label is None:
                    tau_label = "tau_rot_rel_err"
    ret = [df]
    if log is not None:
        if mass is not None:
            if mode == "trans":
                mass_count = sum([val if "mass" in key else 0 for key, val in count.items()])
            else:
                mass_count = sum([val if "inertia" in key else 0 for key, val in count.items()])
            log.add(mass_label, mass, mass_count, op="<=")
        if tau is not None:
            tau_count = sum([val if "tau" in key else 0 for key, val in count.items()])
            log.add(tau_label, tau, tau_count, op="<=")
        ret.append(log)
    if ret_count:
        count = sum([val for val in count.values()])
        ret.append(count)
    if len(ret) == 1:
        return ret[0]
    return tuple(ret)


def filter_rss(df, thresh, mode="trans", ret_count=False, id_column="particle", axes=None, log=None, label=None):
    if axes is None:
        axes = ("x", "y")
    if isinstance(axes, str):
        axes = [axes]
    count = {}
    if mode == "trans":
        for axis in axes:
            df = df[df["rss_{}".format(axis)] <= thresh]
        count["rss"] = len(df[id_column].unique())
        if log is not None:
            if len(axes) == 1:
                if label is None:
                    label = "rss_{}"
                if "{}" in label:
                    label = label.format(axes[0])
            elif label is None:
                label = "rss_trans"
    else:
        df = df[df["rss_rot"] <= thresh]
        count["rss_rot"] = len(df[id_column].unique())
        if log is not None:
            if label is None:
                label = "rss_rot"
    ret = [df]
    count = sum([val for val in count.values()])
    if log is not None:
        log.add(label, thresh, count, op="<=")
        ret.append(log)
    if ret_count:
        ret.append(count)
    if len(ret) == 1:
        return ret[0]
    return tuple(ret)


@deprecated
def filter_rel_err_WIP(df, mass=None, tau=None, mode="trans", ret_count=False, op="<=", id_column="particle", axes=None,
                       log=None, mass_label=None, tau_label=None):
    if axes is None:
        axes = ("x", "y")
    if isinstance(axes, str):
        axes = [axes]
    filters = {}
    if mode == "trans":
        if mass is not None:
            filters["mass_{}_err/mass_{}" + op] = mass
            if log is not None:
                if len(axes) == 1:
                    if mass_label is None:
                        mass_label = "mass_{}_rel_err"
                    if "{}" in mass_label:
                        mass_label = mass_label.format(axes[0])
                elif mass_label is None:
                    mass_label = "mass_rel_err"
        if tau is not None:
            filters["tau_{}_err/tau_{}" + op] = tau
            if log is not None:
                if len(axes) == 1:
                    if tau_label is None:
                        tau_label = "tau_{}_rel_err"
                    if "{}" in tau_label:
                        tau_label = tau_label.format(axes[0])
                elif tau_label is None:
                    tau_label = "tau_rel_err"
    else:
        if mass is not None:
            filters["inertia_err/inertia" + op] = mass
            if log is not None:
                if mass_label is None:
                    mass_label = "moi_rel_err"
        if tau is not None:
            filters["tau_rot_err/tau_rot" + op] = tau
            if log is not None:
                if tau_label is None:
                    tau_label = "tau_rot_rel_err"
    df, count = icaps.filter_df(df, filters, id_column=id_column, ret_count=True, axes=axes)
    ret = [df]
    if log is not None:
        if mass is not None:
            if mode == "trans":
                mass_count = sum([val if "mass" in key else 0 for key, val in count.items()])
            else:
                mass_count = sum([val if "inertia" in key else 0 for key, val in count.items()])
            log.add(mass_label, mass, mass_count, op=op)
        if tau is not None:
            tau_count = sum([val if "tau" in key else 0 for key, val in count.items()])
            log.add(tau_label, tau, tau_count, op=op)
        ret.append(log)
    if ret_count:
        count = sum([val for val in count.values()])
        ret.append(count)
    return tuple(ret)


@deprecated
def filter_chisqr_WIP(df, thresh, mode="trans", op="<=", ret_count=False, id_column="particle", axes=None, log=None,
                      label=None):
    if axes is None:
        axes = ("x", "y")
    if isinstance(axes, str):
        axes = [axes]
    if mode == "trans":
        if log is not None:
            if len(axes) == 1:
                if label is None:
                    label = "chisqr_{}"
                if "{}" in label:
                    label = label.format(axes[0])
            elif label is None:
                label = "chisqr_trans"
        df, count = icaps.filter_df(df, {"chisqr_{}" + op: thresh}, axes=axes, ret_count=True, id_column=id_column)
    else:
        if log is not None:
            if label is None:
                label = "chisqr_rot"
        df, count = icaps.filter_df(df, {"chisqr_rot" + op: thresh}, ret_count=True, id_column=id_column)
    ret = [df]
    count = sum([val for val in count.values()])
    if log is not None:
        log.add(label, thresh, count, op=op)
        ret.append(log)
    if ret_count:
        ret.append(count)
    return tuple(ret)


def stitch_displacement(df, dt, in_columns=None, out_columns=None, time_column="frame", id_column="particle",
                        silent=True, append_dt=True, abs_max=None, abs_norm=np.deg2rad(180), convert=None,
                        add_leaps=False):
    if in_columns is None:
        in_columns = ["x", "y"]
    elif isinstance(in_columns, str):
        in_columns = [in_columns]
    if convert is None:
        convert = [1 for i in range(len(in_columns))]
    elif isinstance(convert, float):
        convert = [convert for i in range(len(in_columns))]
    if out_columns is None:
        out_columns = "d{}"
    if isinstance(out_columns, str):
        out_columns = [out_columns.format(in_column) for in_column in in_columns]
    if append_dt:
        out_columns = [out_column + "_{}".format(dt) for out_column in out_columns]
    df2 = pd.DataFrame(index=df.index)
    for out_column in out_columns:
        df2[out_column] = np.nan
    leap_column = "leap_{}".format(dt) if append_dt else "leap"
    df2[leap_column] = 0
    if id_column:
        particles = np.unique(df[id_column].to_numpy())
    else:
        particles = [0]
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles), "Stitching Displacement ({})".format(dt))
    for particle in particles:
        if id_column:
            df_ = df[df[id_column] == particle]
        else:
            df_ = df
        df_ = df_.dropna(subset=in_columns, how="any")
        idx = df_.index
        j = 0
        i = 1
        t_off = 0
        x_off = np.zeros(len(in_columns), dtype=np.float64)
        while i < len(idx):
            dt_ = df_.loc[idx[i], time_column] - df_.loc[idx[j], time_column] - t_off
            if dt_ < dt:
                i += 1
            elif dt_ > dt:
                t_off += df_.loc[idx[i], time_column] - df_.loc[idx[i - 1], time_column]
                x_off = [df_.loc[idx[i], in_column] - df_.loc[idx[i - 1], in_column] for in_column in in_columns]
                i += 1
            else:
                if abs_max is None:
                    for c, in_column in enumerate(in_columns):
                        dx = df_.loc[idx[i], in_column] - df_.loc[idx[j], in_column] - x_off[c]
                        df2.loc[idx[j], out_columns[c]] = dx * convert[c]
                else:
                    for c, in_column in enumerate(in_columns):
                        dx = df_.loc[idx[i], in_column] - df_.loc[idx[j], in_column] - x_off[c]
                        dx *= convert[c]
                        if abs(dx) >= abs_max:
                            if dx >= 0:
                                dx = dx - abs_norm
                            else:
                                dx = abs_norm + dx
                            if add_leaps:
                                df2.loc[idx[j], leap_column] = 1
                        df2.loc[idx[j], out_columns[c]] = dx
                t_off = 0
                x_off = np.zeros(len(in_columns), dtype=np.float64)
                j = i
                i += 1
        if not silent:
            pb.tick()
    if not silent:
        pb.close()
    df = df.join(df2, how="left")
    return df


def stitch_displacements(df, dts, in_columns=None, out_columns=None, time_column="frame", id_column="particle",
                         silent=True, abs_max=None, abs_norm=np.deg2rad(180), convert=None, add_leaps=False):
    for dt in dts:
        df = stitch_displacement(df, dt, in_columns=in_columns, out_columns=out_columns, time_column=time_column,
                                 id_column=id_column, silent=silent, abs_max=abs_max, abs_norm=abs_norm,
                                 convert=convert, add_leaps=add_leaps)
    return df


def plot_stitched_displacement(df, dt, pos_column, disp_column, out_path, particle=None, time_column="frame",
                               id_column="particle", disp_name=None, pos_name=None, time_name="Time", pos_unit="µm",
                               pos_convert=const.px_ldm, time_unit="ms", time_convert=1000/const.fps_ldm,
                               align="cols", leap_column=None, dpi=300):
    if (id_column is not None) and (particle is not None):
        df = df[df[id_column] == particle]
    if pos_name is None:
        pos_name = pos_column
    if disp_name is None:
        disp_name = disp_column
    pos = df[pos_column].to_numpy() * pos_convert
    disp = df[disp_column].to_numpy() * pos_convert
    time = df[time_column].to_numpy() * time_convert
    pos -= np.nanmin(pos)
    t_min = np.min(time)
    time -= t_min
    if align == "cols":
        _, ax = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
    else:
        _, ax = plt.subplots(nrows=2, figsize=[6.4, 4.8 * 2])
    ax[0].set_xlabel("{} ({})".format(time_name, time_unit))
    ax[0].set_ylabel("{} ({})".format(pos_name, pos_unit))
    ax[0].scatter(time, pos, s=2, marker="o", c="black", zorder=1)
    ax[0].plot(time[~np.isnan(disp)], pos[~np.isnan(disp)], linewidth=1, c="red", zorder=2)
    if leap_column is not None:
        if "{}" in leap_column:
            leap_column = leap_column.format(dt)
        leaps = df.loc[df[leap_column] == 1, time_column].to_numpy() * time_convert
        leaps -= t_min
        for leap in leaps:
            ax[0].axvline(leap, c="grey", linewidth=1, linestyle="--", zorder=0)
    ax[0].grid()
    ax[0].set_axisbelow(True)
    ax[1].set_xlabel("{} ({})".format(time_name, time_unit))
    ax[1].set_ylabel("{} ({})".format(disp_name, pos_unit))
    disp = disp[~np.isnan(disp)]
    t_range = np.arange(0, disp.shape[0] * dt, dt)
    ax[1].scatter(t_range, disp, s=2, marker="o", c="black")
    ax[1].grid()
    ax[1].set_axisbelow(True)
    if not np.isnan(np.nanmax(np.abs(disp))):
        ax[1].set_ylim(-1.05 * np.nanmax(np.abs(disp)), 1.05 * np.nanmax(np.abs(disp)))
    x_lim = ax[0].get_xlim()
    if x_lim[1] != -0.:
        ax[1].set_xlim(x_lim)
    plt.savefig(out_path, dpi=dpi)
    plt.close()


def plot_stitched_displacements(df, dts, pos_column, disp_columns, time_column="frame", id_column="particle",
                                append_dts=True, plot_path=None, sub_folder="{}/plots/disp_trans_{}/",
                                disp_name=None, pos_name=None, time_name="Time", pos_unit="µm",
                                pos_convert=const.px_ldm, time_unit="ms", time_convert=1000/const.fps_ldm,
                                silent=True, clear_out=False, align="cols", leap_column=None, dpi=300):
    if id_column is not None:
        particles = np.unique(df[id_column].to_numpy())
    else:
        particles = [0]
    if isinstance(disp_columns, str):
        if append_dts:
            disp_columns = ["{}_{}".format(disp_columns, dt) for dt in dts]
        else:
            disp_columns = [disp_columns]
    assert len(dts) == len(disp_columns)
    if sub_folder is None:
        sub_folder = ""
        if clear_out:
            mk_folder(plot_path, clear=clear_out)
            clear_out = False

    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles) * len(dts), "Plotting Displacements")
    for particle in particles:
        if "{}" in sub_folder:
            sub_folder = sub_folder.format(particle, pos_column)
        out_path = plot_path + sub_folder
        mk_folder(out_path, clear=clear_out)
        if id_column is None:
            df_ = df
        else:
            df_ = df[df[id_column] == particle]
        for i, disp_column in enumerate(disp_columns):
            plot_stitched_displacement(df_, dts[i], pos_column, disp_column,
                                       out_path + "{}_{}.png".format(particle, dts[i]),
                                       time_column=time_column, id_column=id_column, disp_name=disp_name,
                                       pos_name=pos_name, time_name=time_name, pos_unit=pos_unit,
                                       pos_convert=pos_convert, time_unit=time_unit, time_convert=time_convert,
                                       align=align, leap_column=leap_column, dpi=dpi)
            if not silent:
                pb.tick()
    # if not silent:
    #     pb.close()


def calc_erf(df, dts, axes, disp_columns="d{}", id_column="particle", column_frmt="{}_{}", p0=None, maxit=None,
             fit_type=2, n_min=10, disp_convert=const.px_ldm*1e-6, silent=True, max_sigma=1, plot_path=None,
             sub_folder="{}/plots/erf_trans/", disp_names="d{}", disp_unit="µm", disp_convert_plot=const.px_ldm,
             erf_convert=1e6, clear_out=False, dpi=300, double_gauss=True, fickian=False, plot_fails=False,
             var_lr=False, erf_exp=False, plot_until=100, plot_empty=False):
    if id_column is not None:
        particles = np.unique(df[id_column].to_numpy())
    else:
        particles = [0]
    if isinstance(axes, str):
        axes = [axes]
    disp_columns_param = disp_columns
    if isinstance(disp_columns, str):
        if "{}" in disp_columns:
            disp_columns = [disp_columns.format(a) for a in axes]
        else:
            disp_columns = [disp_columns]
    assert len(axes) == len(disp_columns)
    if p0 is None:
        p0 = [0.0, 0.001]
    if plot_path is not None:
        mk_folder(plot_path, clear=clear_out)
    columns = {
        "single": ["sigma", "sigma_err", "drift", "drift_err", "chisqr", "rss", "n_dots"],
        "double": ["w0", "w0_err", "sigma0", "sigma0_err", "sigma1", "sigma1_err", "drift01", "drift01_err",
                   "chisqr_dg", "rss_dg", "sigma_diff"],
        "var_lr": ["var_left", "var_right"],
        "erf_exp": ["w_exp", "w_exp_err", "sigma_exp", "sigma_exp_err", "drift_exp", "drift_exp_err", "chisqr_exp",
                    "rss_exp"],
        "fickian": ["wf", "wf_err", "sigmaf", "sigmaf_err", "lambda", "lambda_err", "driftf", "driftf_err", "chisqr_fk",
                    "rss_fk"]
    }
    df_template = pd.DataFrame(data={"dt": np.repeat(dts, len(axes)), "axis": np.tile(axes, len(dts))})
    for col in columns["single"]:
        df_template[col] = np.nan
    if double_gauss:
        for col in columns["double"]:
            df_template[col] = np.nan
    if var_lr:
        for col in columns["var_lr"]:
            df_template[col] = np.nan
    if erf_exp:
        for col in columns["erf_exp"]:
            df_template[col] = np.nan
    if fickian:
        for col in columns["fickian"]:
            df_template[col] = np.nan
    df_out = None
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles) * len(axes) * len(dts), "Calculating Erf Fits")
    for part_idx, particle in enumerate(particles):
        df_out_ = df_template.copy()
        if id_column is not None:
            df_out_[id_column] = particle
            df_ = df[df["particle"] == particle]
        else:
            df_ = df
        for i, disp_column in enumerate(disp_columns):
            for dt in dts:
                axis_label = column_frmt.format(disp_column, dt)
                disp = np.sort(df_[axis_label].dropna().to_numpy()) * disp_convert
                if len(disp) < n_min:
                    if not silent:
                        pb.tick()
                    continue
                norm_disp = icaps.norm_count(disp)
                out_single = fit_model(fit_erf, disp, norm_disp, p0=p0, fit_type=fit_type, maxit=maxit)
                if max_sigma is not None:
                    if out_single.beta[1] >= max_sigma:
                        if not silent:
                            pb.tick()
                        continue
                idx_out = df_out_.index[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i])]
                df_out_.loc[idx_out, "drift"] = out_single.beta[0]
                df_out_.loc[idx_out, "drift_err"] = out_single.sd_beta[0]
                df_out_.loc[idx_out, "sigma"] = out_single.beta[1]
                df_out_.loc[idx_out, "sigma_err"] = out_single.sd_beta[1]
                chisqr_ = icaps.chisqr(norm_disp, fit_erf(disp, *out_single.beta))
                df_out_.loc[idx_out, "chisqr"] = chisqr_
                rss_ = icaps.rss(norm_disp, fit_erf(disp, *out_single.beta), len(disp))
                df_out_.loc[idx_out, "rss"] = rss_
                df_out_.loc[idx_out, "n_dots"] = len(disp)
                if double_gauss:
                    p0_ = [0.8, out_single.beta[0], out_single.beta[1], out_single.beta[1]]
                    # p0_ = [0.5, out_single.beta[0], np.sqrt(2) * out_single.beta[1], out_single.beta[1] / np.sqrt(2)]
                    # p0_ = [0.7, out_single.beta[0], np.sqrt(2) * out_single.beta[1], out_single.beta[1] / np.sqrt(2)]
                    out = fit_model(fit_erf_dg, disp, norm_disp, p0=p0_, fit_type=fit_type, maxit=maxit)
                    df_out_.loc[idx_out, "w0"] = out.beta[0]
                    df_out_.loc[idx_out, "w0_err"] = out.sd_beta[0]
                    df_out_.loc[idx_out, "drift01"] = out.beta[1]
                    df_out_.loc[idx_out, "drift01_err"] = out.sd_beta[1]
                    df_out_.loc[idx_out, "sigma0"] = out.beta[2]
                    df_out_.loc[idx_out, "sigma0_err"] = out.sd_beta[2]
                    df_out_.loc[idx_out, "sigma1"] = out.beta[3]
                    df_out_.loc[idx_out, "sigma1_err"] = out.sd_beta[3]
                    chisqr_ = icaps.chisqr(norm_disp, fit_erf_dg(disp, *out.beta))
                    df_out_.loc[idx_out, "chisqr_dg"] = chisqr_
                    rss_ = icaps.rss(norm_disp, fit_erf_dg(disp, *out.beta), len(disp))
                    df_out_.loc[idx_out, "rss_dg"] = rss_
                    var_left = fsolve(lambda q: 0.5 - const.var1_inside / 2 - fit_erf_dg(q, *out.beta),
                                      x0=out.beta[1])[0]
                    var_right = fsolve(lambda q: 0.5 + const.var1_inside / 2 - fit_erf_dg(q, *out.beta),
                                       x0=out.beta[1])[0]
                    df_out_.loc[idx_out, "sigma_diff"] = (var_right - var_left) / 2
                    if var_lr:
                        df_out_.loc[idx_out, "var_left"] = var_left
                        df_out_.loc[idx_out, "var_right"] = var_right
                if erf_exp:
                    # mu, sig, w, lambda_
                    p0_ = [out_single.beta[0], out_single.beta[1], 0.2, out_single.beta[1]]
                    out = fit_model(fit_erf_exp, disp, norm_disp, p0=p0_, fit_type=fit_type, maxit=maxit)
                    df_out_.loc[idx_out, "drift_exp"] = out.beta[0]
                    df_out_.loc[idx_out, "drift_exp_err"] = out.sd_beta[0]
                    df_out_.loc[idx_out, "sigma_exp"] = out.beta[1]
                    df_out_.loc[idx_out, "sigma_exp_err"] = out.sd_beta[1]
                    df_out_.loc[idx_out, "w_exp"] = out.beta[2]
                    df_out_.loc[idx_out, "w_exp_err"] = out.sd_beta[2]
                    df_out_.loc[idx_out, "lambda_exp"] = out.beta[3]
                    df_out_.loc[idx_out, "lambda_exp_err"] = out.sd_beta[3]
                    chisqr_ = icaps.chisqr(norm_disp, fit_erf_exp(disp, *out.beta), 1)
                    df_out_.loc[idx_out, "chisqr_exp"] = chisqr_
                    rss_ = icaps.rss(norm_disp, fit_erf_exp(disp, *out.beta), len(disp))
                    df_out_.loc[idx_out, "rss_exp"] = rss_
                if fickian:
                    p0_ = [0.8, out_single.beta[0], out_single.beta[1], np.sqrt(dt/1e3)]
                    out = fit_model(fit_erf_fk, disp, norm_disp, p0=p0_, fit_type=fit_type, maxit=maxit)
                    df_out_.loc[idx_out, "wf"] = out.beta[0]
                    df_out_.loc[idx_out, "wf_err"] = out.sd_beta[0]
                    df_out_.loc[idx_out, "driftf"] = out.beta[1]
                    df_out_.loc[idx_out, "driftf_err"] = out.sd_beta[1]
                    df_out_.loc[idx_out, "sigmaf"] = out.beta[2]
                    df_out_.loc[idx_out, "sigmaf_err"] = out.sd_beta[2]
                    df_out_.loc[idx_out, "lambda"] = out.beta[3]
                    df_out_.loc[idx_out, "lambda_err"] = out.sd_beta[3]
                    chisqr_ = icaps.chisqr(norm_disp, fit_erf_fk(disp, *out.beta))
                    df_out_.loc[idx_out, "chisqr_fk"] = chisqr_
                    rss_ = icaps.rss(norm_disp, fit_erf_fk(disp, *out.beta), len(disp))
                    df_out_.loc[idx_out, "rss_fk"] = rss_
                if not silent:
                    pb.tick()
        if df_out is None:
            df_out = df_out_
        else:
            df_out = df_out.append(df_out_)
        if (plot_path is not None) and (part_idx/len(particles) <= plot_until):
            plot_erfs(df_, df_out_, axes=axes, dts=dts, disp_columns=disp_columns_param, id_column=id_column,
                      plot_path=plot_path, sub_folder=sub_folder, disp_names=disp_names, disp_unit=disp_unit,
                      disp_convert=disp_convert_plot, erf_convert=erf_convert, dpi=dpi, double_gauss=double_gauss,
                      fickian=fickian, erf_exp=erf_exp, plot_fails=plot_fails, var_lr=var_lr, n_min=n_min,
                      plot_empty=plot_empty)
    if not silent:
        pb.close()
    if id_column is not None:
        df_out = move_col(df_out, id_column, 0)
    return df_out


def plot_erf(df_disp, df_erf, axes, disp_columns, out_paths, particle=None, id_column="particle",
             disp_names="d{}", disp_convert=const.px_ldm, erf_convert=1e6,
             disp_unit="µm", dpi=300, double_gauss=True, fickian=False, erf_exp=False,
             plot_fails=False, var_lr=False, n_min=10, plot_empty=False):
    discard_plot = True
    if double_gauss and "chisqr_dg" not in list(df_erf.columns):
        double_gauss = False
    if fickian and "chisqr_fk":
        fickian = False
    if isinstance(axes, str):
        axes = [axes]
    if isinstance(disp_columns, str):
        disp_columns = [disp_columns]
    assert len(axes) == len(disp_columns)
    assert len(axes) == len(out_paths)
    if (id_column is not None) and (particle is not None):
        df_disp = df_disp[df_disp[id_column] == particle]
        df_erf = df_erf[df_erf[id_column] == particle]
    df_erf = df_erf[df_erf["axis"].isin(axes)]
    if disp_names is None:
        disp_names = disp_columns
    if isinstance(disp_names, str):
        if "{}" in disp_names:
            disp_names = [disp_names.format(a) for a in axes]
        else:
            disp_names = [disp_names for a in axes]
    elif isinstance(disp_names, str):
        disp_names = [disp_names for i in range(len(disp_columns))]

    fig, ax = plt.subplots(nrows=2)
    fig.subplots_adjust(hspace=.001, wspace=.001)
    for i in range(len(axes)):
        # _, ax = plt.subplots(nrows=2)
        # plt.subplots_adjust(hspace=.001, wspace=.001)
        s_erf = row2dict(df_erf[df_erf["axis"] == axes[i]])
        if (s_erf["sigma"] is None) or (s_erf["n_dots"] < n_min):
            continue
        disp = np.sort(df_disp[disp_columns[i]].dropna().to_numpy()) * disp_convert
        norm_disp = icaps.norm_count(disp)
        ax[0].scatter(disp, norm_disp, c="black", s=2, marker="o")
        ax[1].set_xlabel("{} ({})".format(disp_names[i], disp_unit))
        ax[0].set_ylabel("Normalized {}".format(disp_names[i]))
        ax[1].set_ylabel("Residual")
        ax[0].set_ylim(-0.02, 1.02)
        ax[1].set_ylim(-0.1, 0.1)
        ax[1].axhline(0, c="gray", zorder=0, lw=2, ls="--")
        x_low = 0.95*min([ax[0].get_xlim()[0] for i in range(len(axes))])
        x_high = 1.05*max([ax[0].get_xlim()[1] for i in range(len(axes))])
        x_lim = np.max(np.abs([x_low, x_high]))
        x_space = np.linspace(x_low, x_high, 1000)

        if x_lim != 0.:
            ax[0].set_xlim(-x_lim, x_lim)
            ax[1].set_xlim(-x_lim, x_lim)
        ax[0].set_xticks([])
        sigma = s_erf["sigma"] * erf_convert
        sigma_err = s_erf["sigma_err"] * erf_convert
        drift = s_erf["drift"] * erf_convert
        drift_err = s_erf["drift_err"] * erf_convert
        if (not np.isnan(sigma)) and (not np.isnan(drift)) and (not sigma_err == 0.) and (not drift_err == 0.):
            discard_plot = False
            ax[0].plot(x_space, fit_erf(x_space, drift, sigma), c="blue", linewidth=1, label="Single Gauss")
            norm_disp = icaps.norm_count(disp)
            ax[1].scatter(disp, fit_erf(disp, drift, sigma) - norm_disp, c="blue", s=2, zorder=1)

        legend_set = False
        if double_gauss:
            w0 = s_erf["w0"]
            w0_err = s_erf["w0_err"]
            sigma0 = s_erf["sigma0"] * erf_convert
            sigma0_err = s_erf["sigma0_err"] * erf_convert
            sigma1 = s_erf["sigma1"] * erf_convert
            sigma1_err = s_erf["sigma1_err"] * erf_convert
            drift01 = s_erf["drift01"] * erf_convert
            drift01_err = s_erf["drift01_err"] * erf_convert
            params = (w0, drift01, sigma0, sigma1)
            if (not np.isnan(sigma)) and (not np.isnan(drift)) and (not sigma_err == 0.) and (not drift_err == 0.):
                discard_plot = False
                ax[0].plot(x_space, fit_erf_dg(x_space, *params), c="red", linewidth=1,
                           label="Double Gauss\n"
                                 + r"$w_0$" + " = {:.2f} ".format(w0) + r"$\pm$"
                                 + " {:.2f}\n".format(w0_err)
                                 + r"$\mu_{01}$"
                                 + " = {:.2f} ".format(drift01) + r"$\pm$"
                                 + " {:.2f} {}\n".format(drift01_err, disp_unit)
                                 + r"$\sigma_0$" + " = {:.2f} ".format(sigma0) + r"$\pm$"
                                 + " {:.2f} {}\n".format(sigma0_err, disp_unit)
                                 + r"$\sigma_1$" + " = {:.2f} ".format(sigma1) + r"$\pm$"
                                 + " {:.2f} {}".format(sigma1_err, disp_unit)
                           )
                ax[1].scatter(disp, fit_erf_dg(disp, *params) - norm_disp, c="red", s=2,
                              zorder=2)
                if var_lr:
                    var_left = s_erf["var_left"] * erf_convert
                    var_right = s_erf["var_right"] * erf_convert
                    ax[0].scatter(var_left, 0.5 - const.var1_inside/2,
                                  facecolor="none", edgecolor="lime", marker="s", s=10, zorder=1000)
                    ax[0].scatter(var_right, 0.5 + const.var1_inside/2,
                                  facecolor="none", edgecolor="lime", marker="s", s=10, zorder=1000)
                ax[0].legend(loc="lower right", fontsize="xx-small")
                legend_set = True
        if erf_exp:
            w_exp = s_erf["w_exp"]
            w_exp_err = s_erf["w_exp_err"]
            sigma_exp = s_erf["sigma_exp"] * erf_convert
            sigma_exp_err = s_erf["sigma_exp_err"] * erf_convert
            lambda_exp = s_erf["lambda_exp"]
            lambda_exp_err = s_erf["lambda_exp_err"]
            drift_exp = s_erf["drift_exp"] * erf_convert
            drift_exp_err = s_erf["drift_exp_err"] * erf_convert
            params = (drift_exp, sigma_exp, w_exp, lambda_exp)
            if (not np.isnan(sigma_exp)) and (not np.isnan(drift_exp)) and (not sigma_exp_err == 0.) and (not drift_exp_err == 0.):
                discard_plot = False
                ax[0].plot(x_space, fit_erf_exp(x_space, *params), c="green", linewidth=1,
                           label="Gauss + Exp\n"
                                 + r"$w_{exp}$" + " = {:.2f} ".format(w_exp) + r"$\pm$"
                                 + " {:.2f}\n".format(w_exp_err)
                                 + r"$\mu_{exp}$"
                                 + " = {:.2f} ".format(drift_exp) + r"$\pm$"
                                 + " {:.2f} {}\n".format(drift_exp_err, disp_unit)
                                 + r"$\sigma_{exp}}$" + " = {:.2f} ".format(sigma_exp) + r"$\pm$"
                                 + " {:.2f} {}\n".format(sigma_exp_err, disp_unit)
                                 + r"$\lambda$" + " = {:.2f} ".format(lambda_exp) + r"$\pm$"
                                 + " {:.2f} {}".format(lambda_exp_err, disp_unit)
                           )
                ax[1].scatter(disp, fit_erf_exp(disp, *params) - norm_disp, c="green", s=2,
                              zorder=2)
                if not legend_set:
                    ax[0].legend(loc="lower right", fontsize="xx-small")
        if fickian:
            wf = s_erf["wf"]
            wf_err = s_erf["wf_err"]
            sigmaf = s_erf["sigmaf"] * erf_convert
            sigmaf_err = s_erf["sigmaf_err"] * erf_convert
            lambda_ = s_erf["lambda"] * erf_convert
            lambda_err = s_erf["lambda_err"] * erf_convert
            driftf = s_erf["driftf"] * erf_convert
            driftf_err = s_erf["driftf_err"] * erf_convert
            params = (wf, driftf, sigmaf, lambda_)
            if (not np.isnan(sigma)) and (not np.isnan(drift)) and (not sigma_err == 0.) and (not drift_err == 0.):
                discard_plot = False
                ax[0].plot(x_space, fit_erf_fk(x_space, *params), c="green", linewidth=1,
                           label="Gauss + Fickian\n"
                                 + r"$w_f$" + " = {:.2f} ".format(wf) + r"$\pm$"
                                 + " {:.2f}\n".format(wf_err)
                                 + r"$\mu_{f}$"
                                 + " = {:.2f} ".format(driftf) + r"$\pm$"
                                 + " {:.2f} {}\n".format(driftf_err, disp_unit)
                                 + r"$\sigma_f$" + " = {:.2f} ".format(sigmaf) + r"$\pm$"
                                 + " {:.2f} {}\n".format(sigmaf_err, disp_unit)
                                 + r"$\lambda$" + " = {:.2f} ".format(lambda_) + r"$\pm$"
                                 + " {:.2f} {}".format(lambda_err, disp_unit)
                           )
                ax[1].scatter(disp, fit_erf_fk(disp, *params) - norm_disp, c="green", s=2,
                              zorder=2)
                if not legend_set:
                    ax[0].legend(loc="lower right", fontsize="xx-small")

        # plt.tight_layout()
        # mpl.use("Agg")
        if i == 0:
            fig.tight_layout()
        if (not discard_plot) or plot_empty:
            plt.savefig(out_paths[i], dpi=dpi)
        ax[0].cla()
        ax[1].cla()
        # plt.close()
    plt.close()


def plot_erfs(df_disp, df_erf, axes, dts, disp_columns="d{}", id_column="particle", append_dts=True, plot_path=None,
              sub_folder="{}/plots/erf_trans/", disp_names="d{}", disp_unit="µm", disp_convert=const.px_ldm,
              erf_convert=1e6, silent=True, clear_out=False, dpi=300, double_gauss=True, fickian=False, erf_exp=False,
              plot_fails=False, var_lr=False, n_min=10, plot_empty=False):
    mpl.use("Agg")
    if double_gauss and "chisqr_dg" not in list(df_erf.columns):
        double_gauss = False
    if fickian and "chisqr_fk":
        fickian = False
    particles = [0]
    if id_column is not None:
        particles = np.unique(df_disp[id_column].to_numpy())
    if isinstance(disp_columns, str):
        if append_dts:
            if "{}" in disp_columns:
                disp_columns = ["{}_{}".format(disp_columns.format(a), dt) for dt in dts for a in axes]
            else:
                disp_columns = ["{}_{}".format(disp_columns, dt) for dt in dts for a in axes]
        elif "{}" in disp_columns:
            disp_columns = [disp_columns.format(a) for dt in dts for a in axes]
        else:
            disp_columns = [disp_columns]
    assert len(dts) * len(axes) == len(disp_columns)
    if sub_folder is None:
        sub_folder = ""
        if clear_out:
            mk_folder(plot_path, clear=clear_out)
            clear_out = False

    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles) * len(dts), "Plotting Erf Fits")
    for particle in particles:
        if "{}" in sub_folder:
            sub_folder = sub_folder.format(particle)
        out_path = plot_path + sub_folder
        mk_folder(out_path, clear=clear_out)
        for i in range(int(len(disp_columns)/len(axes))):
            df_erf_ = df_erf[df_erf["dt"] == dts[i]]
            disp_column = disp_columns[len(axes)*i:len(axes)*(i+1)]
            out_paths = [out_path + "{}_{}_{}.png".format(particle, dts[i], axis) for axis in axes]
            plot_erf(df_disp, df_erf_, axes, disp_column, out_paths,
                     particle=particle, id_column=id_column,
                     disp_names=disp_names, disp_convert=disp_convert, erf_convert=erf_convert, disp_unit=disp_unit,
                     dpi=dpi, double_gauss=double_gauss, fickian=fickian, erf_exp=erf_exp,
                     plot_fails=plot_fails, var_lr=var_lr, n_min=n_min, plot_empty=plot_empty)
            if not silent:
                pb.tick()
    if not silent:
        pb.close()
    mpl.use("TkAgg")


def parse_candidates(dts, sigsqr, sigsqr_err, fit_bounds=(None, None), adapt_margins=(0, 0), n_min=4,
                     max_sigsqr=None, max_err_ratio=1, strict_max=False):
    """
    Calculates all possible candidates for the OF adaptive fitting with a set of parameters.
    :param dts: Time deltas
    :param sigsqr: Squared gauss widths
    :param sigsqr_err: Errors of squared gauss widths
    :param fit_bounds: Hard bounds of the fit in ms
    :param adapt_margins: Lower and upper margins in number of data points. Upper margin is measured from the total
    number of dts. Also NaNs are taken into account.
    :param n_min: Minimal number of data points
    :param max_sigsqr: Maximum squared gauss width
    :param max_err_ratio: Maximum error to value ratio of the squared gauss width
    :return: Candidates for the OF adaptive fitting as index arrays
    """
    adapt_margins = list(adapt_margins)
    if adapt_margins[0] != 0:
        adapt_margins[0] = adapt_margins[0] - np.count_nonzero(np.isnan(sigsqr[:adapt_margins[0]]))
    if adapt_margins[1] != 0:
        adapt_margins[1] = adapt_margins[1] - np.count_nonzero(np.isnan(sigsqr[len(sigsqr)-adapt_margins[1]:]))
    idx = np.arange(0, len(dts), 1)
    idx = idx[~np.isnan(sigsqr)]
    dts = dts[idx]
    if fit_bounds[0] is not None:
        idx = idx[dts > fit_bounds[0]]
        dts = dts[dts > fit_bounds[0]]
    if fit_bounds[1] is not None:
        idx = idx[dts <= fit_bounds[1]]
    idx_base = idx.copy()
    sigsqr_ = sigsqr[idx]
    try:
        sigsqr_err_ = sigsqr_err[idx]
    except IndexError:
        sigsqr_err_ = np.max([sigsqr_err[0, idx], sigsqr_err[1, idx]], axis=0)
    idx = idx[sigsqr_err_ / sigsqr_ < max_err_ratio]
    sigsqr_ = sigsqr[idx]
    if max_sigsqr is not None:
        if strict_max:
            first_occ = np.argmax(sigsqr_ > max_sigsqr)
            idx = idx[:first_occ]
        else:
            idx = idx[sigsqr_ <= max_sigsqr]
    if len(idx) < n_min:
        return []
    candidates = [idx]
    if (adapt_margins[0] == 0) and (adapt_margins[1] == 0):
        return candidates
    elif (adapt_margins[0] == 0) and (adapt_margins[1] > 0):
        i = 1
        while (len(idx) - i >= n_min) and (idx[-i] >= np.max(idx_base) - adapt_margins[1]):
            candidates.append(idx[:-i])
            i += 1
        return candidates
    elif (adapt_margins[0] > 0) and (adapt_margins[1] == 0):
        i = 1
        while (len(idx) - i >= n_min) and (idx[i] <= np.min(idx_base) + adapt_margins[0]):
            candidates.append(idx[i:])
            i += 1
        return candidates
    else:
        candidates = []
        j = 0
        while 1:
            i = 1
            while (len(idx) - i - j >= n_min) and (idx[-i] >= np.max(idx_base) - adapt_margins[1]):
                candidates.append(idx[j:-i])
                i += 1
            if len(idx) - j >= n_min:
                candidates.append(idx[j:])
            j += 1
            if idx[j] > np.min(idx_base) + adapt_margins[0]:
                break
        return candidates


def calc_ornstein_fuerth(df_erf, dts=None, axes=None, particles=None, mode="trans",
                         id_column="particle", mass_column="mass",
                         units=None, fit=None, plot=None, sig_mode="SG",
                         temp_path=None, temp_interval=0.1, ret_erf=False, silent=True):
    """
    :param df_erf:
    :param dts:
    :param axes:
    :param particles:
    :param mode:
    :param id_column:
    :param mass_column:
    :param units:
    :param fit:
    :param plot:
    :param sig_mode: SG, DIFF, DG
    :param temp_path:
    :param temp_interval:
    :param ret_erf:
    :param silent:
    :return:
    """

    # =====================
    # Parameter Setup
    # =====================
    df_erf = df_erf.copy()
    units_ = {"sigma": "m",
              "time": "s",
              "time_cvt": 1e-3,
              "mass_mag": 14}
    if mode == "rot":
        units_ = icaps.merge_dicts(units_, {"sigma": "rad", "mass_mag": 25})
        if mass_column == "mass":
            mass_column = "inertia"
    mass_err_column = mass_column + "_err"
    units = icaps.merge_dicts(units_, {} if units is None else units)
    fit_ = {"p0": None,
            "maxit": None,
            "fit_type": 0,
            "fit_err": True,
            "bounds": (None, None),
            "sequential": True,
            "ballistic_bounds": (None, 6),
            "ballistic_fit": "OF",
            "n_min": 5,
            "max_sigsqr": None,
            "strict_max": False,
            "max_err_ratio": 1.,
            "min_tau": None,
            "semi_auto": False,
            "read_upper_bound": False,
            "adapt_margins": (0, 0),
            "adapt_quantity": "count",
            "adapt_grade_weights": (1., 1.),
            "adapt_grade_type": "gaussian",
            "chisqr_log": False,
            "chisqr_norm": False,
            "rss_log": True,
            "rss_norm": True,
            "corrcoef_log": False,
            "corrcoef_norm": False
            }
    fit = icaps.merge_dicts(fit_, {} if fit is None else fit)
    plot_ = {"path": None,
             "dpi": 600,
             "clear_out": True,
             "errors": True,
             "attempts": False,
             "crop_to_max": True,
             "min_rss": 5e-3,
             "plot_until": 1,
             "dots": "SG"}
    plot = icaps.merge_dicts(plot_, {} if plot is None else plot)
    if sig_mode == "DIFF":
        plot["errors"] = False
        fit["fit_err"] = False
    if plot["path"] is not None:
        mpl.use("Agg")
    n_temp = 0
    if id_column is None:
        df_erf["particle"] = 0
        id_column = "particle"
    if particles is None:
        particles = np.unique(df_erf[id_column].to_numpy())
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_raw = dts.copy()
    dts = dts.astype(np.float64) * units["time_cvt"]
    dt_space = np.linspace(np.min(dts), np.max(dts), 1000)
    if not ret_erf:
        df_erf = df_erf[df_erf["dt"].isin(dts_raw)]
    if fit["read_upper_bound"]:
        fit["semi_auto"] = False
    else:
        df_erf["upper_bound"] = 0
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    elif isinstance(axes, str):
        axes = [axes]
    if plot["path"] is None:
        plot["attempts"] = False
    else:
        if "{}" in plot["path"]:
            plot["path"] = [plot["path"].format(axis) for axis in axes]
        else:
            plot["path"] = [plot["path"] for axis in axes]
        mk_folders(plot["path"], clear=plot["clear_out"])
    if fit["p0"] is None:
        fit["p0"] = [None, None]
    fit["p0"] = list(fit["p0"])
    p0_m = const.m_0
    if mode == "rot":
        p0_m *= const.r_0 ** 2
    fit["p0"][0] = p0_m if fit["p0"][0] is None else fit["p0"][0]
    fit["p0"][1] = 4.8e-3 if fit["p0"][1] is None else fit["p0"][1]
    if fit["ballistic_fit"][0] == "T":
        fit_ballistic = getattr(icaps.fitting, "fit_of_taylor{}".format(fit["ballistic_fit"][1]))
    else:
        fit_ballistic = fit_of_log
    nan_arr = np.zeros(len(particles) * len(axes))
    nan_arr[:] = np.nan
    df_out = pd.DataFrame(data={id_column: np.repeat(particles, len(axes)),
                                "axis": np.tile(axes, len(particles)),
                                mass_column: nan_arr,
                                mass_err_column: nan_arr,
                                "tau": nan_arr,
                                "tau_err": nan_arr,
                                "chisqr_bal": nan_arr,
                                "chisqr": nan_arr,
                                "rss": nan_arr,
                                "corrcoef": nan_arr,
                                "n_dots": np.zeros(len(particles) * len(axes), dtype=int),
                                "min_dt": np.zeros(len(particles) * len(axes), dtype=int),
                                "max_dt": np.zeros(len(particles) * len(axes), dtype=int),
                                "tau0": nan_arr,
                                "chisqr_lin": nan_arr,
                                "rss_lin": nan_arr
                                })
    # =====================
    # Start of Loop
    # =====================
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles) * len(axes), "Calculating OF Fits")
    for p, particle in enumerate(particles):
        df_erf_ = df_erf[(df_erf[id_column] == particle) & (df_erf["dt"].isin(dts_raw))]
        for a, axis in enumerate(axes):
            idx_out = df_out.index[(df_out[id_column] == particle) & (df_out["axis"] == axis)]
            if sig_mode == "DIFF":
                sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_diff"].to_numpy())
                sig_err = np.zeros_like(sig)
            elif sig_mode == "DG":
                sig0 = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma0"].to_numpy())
                sig0_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma0_err"].to_numpy())
                sig1 = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma1"].to_numpy())
                sig1_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma1_err"].to_numpy())
                w0 = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "w0"].to_numpy())
                sig = np.append(sig0[w0 > 0.5], sig1[w0 <= 0.5])
                sig = np.append(sig, sig0[np.isnan(w0)])
                sig_err = np.append(sig0_err[w0 > 0.5], sig1_err[w0 <= 0.5])
                sig_err = np.append(sig_err, sig0_err[np.isnan(w0)])
            else:
                sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma"].to_numpy())
                sig_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_err"].to_numpy())
            sigsqr = sig**2
            sigsqr_err = 2 * sig_err * sig
            # =====================
            # Semiautomatic Mode
            # =====================
            if fit["read_upper_bound"]:
                ubound = df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                                    (df_erf["axis"] == axis), "upper_bound"].to_numpy()[0]
                fit["bounds"] = (fit["bounds"][0], ubound)
            if fit["semi_auto"]:
                mpl.use("TkAgg")
                fig, ax = plt.subplots()
                ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                            fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                ubound = icaps.presentation.gui.PositionSelectionWindow(fig=fig).pos[0]
                ubound = int(ubound/units["time_cvt"])
                fit["bounds"] = (fit["bounds"][0], ubound)
                if ret_erf:
                    df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                               (df_erf["axis"] == axis), "upper_bound"] = ubound
                mpl.use("Agg")
            # =====================
            # Sequential Fitting
            # =====================
            if fit["sequential"]:
                tau_idx = 0
                p0 = [fit["p0"][1]]
                candidates = icaps.brownian.parse_candidates(
                    dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["ballistic_bounds"], adapt_margins=(0, 0),
                    n_min=1, max_sigsqr=fit["max_sigsqr"], max_err_ratio=fit["max_err_ratio"])
                if len(candidates) == 0:
                    pb.tick()
                    continue
                cidx = candidates[0]
                if fit["ballistic_fit"][0] == "T":
                    y_ballistic = sigsqr[cidx]
                    y_ballistic_err = sigsqr_err[cidx]
                else:
                    y_ballistic = np.log10(sigsqr[cidx])
                    y_ballistic_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10)
                out = fit_model(fit_ballistic, dts[cidx], y_ballistic,
                                yerr=y_ballistic_err if fit["fit_err"] else None,
                                p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
                tau0 = abs(out.beta[1])
                mass = out.beta[0]
                mass_err = out.sd_beta[0]
                if fit["ballistic_fit"][0] == "T":
                    chisqr_mass = icaps.chisqr(np.log10(y_ballistic), np.log10(fit_ballistic(dts[cidx], *out.beta)))
                else:
                    chisqr_mass = icaps.chisqr(y_ballistic, fit_ballistic(dts[cidx], *out.beta))
                if fit["chisqr_norm"]:
                    chisqr_mass /= len(cidx)

                def fit_of_here(x0, x1):
                    return fit_of_log(x0, mass, x1)
            else:
                p0 = fit["p0"]
                tau_idx = 1
                mass = np.nan
                mass_err = np.nan
                tau0 = np.nan
                chisqr_mass = np.nan
                fit_of_here = fit_of_log

            # =====================
            # Adaptive Fitting
            # =====================
            candidates = icaps.brownian.parse_candidates(
                dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["bounds"], adapt_margins=fit["adapt_margins"],
                n_min=fit["n_min"], max_sigsqr=fit["max_sigsqr"], max_err_ratio=fit["max_err_ratio"],
                strict_max=fit["strict_max"])
            if len(candidates) == 0:
                pb.tick()
                continue
            nan_arr = np.zeros(len(candidates))
            nan_arr[:] = np.nan
            df_fit = pd.DataFrame(data={"beta0": nan_arr, "sd_beta0": nan_arr, "beta1": nan_arr, "sd_beta1": nan_arr,
                                        "chisqr": nan_arr, "rss": nan_arr, "corrcoef": nan_arr,
                                        "quality": nan_arr, "quantity": nan_arr})

            for ci, cidx in enumerate(candidates):
                log_err_ = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None
                out = fit_model(fit_of_here, dts[cidx], np.log10(sigsqr[cidx]),
                                p0=p0, fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err_)
                if out.res_var == 0:
                    continue
                if fit["min_tau"] is not None:
                    if out.beta[tau_idx] + out.sd_beta[tau_idx] < fit["min_tau"]:
                        continue
                df_fit.loc[ci, "beta0"] = out.beta[0]
                df_fit.loc[ci, "sd_beta0"] = out.sd_beta[0]
                if not fit["sequential"]:
                    df_fit.loc[ci, "beta1"] = out.beta[1]
                    df_fit.loc[ci, "sd_beta1"] = out.sd_beta[1]
                if fit["chisqr_log"]:
                    chisqr_ = icaps.chisqr(np.log10(sigsqr[cidx]), fit_of_here(dts[cidx], *out.beta))
                else:
                    chisqr_ = icaps.chisqr(sigsqr[cidx], 10 ** fit_of_here(dts[cidx], *out.beta))
                if fit["chisqr_norm"]:
                    chisqr_ /= len(cidx)
                df_fit.loc[ci, "chisqr"] = chisqr_
                if fit["rss_log"]:
                    rss_ = icaps.rss(np.log10(sigsqr[cidx]), fit_of_here(dts[cidx], *out.beta))
                else:
                    rss_ = icaps.rss(sigsqr[cidx], 10 ** fit_of_here(dts[cidx], *out.beta))
                if fit["rss_norm"]:
                    rss_ /= len(cidx)
                df_fit.loc[ci, "rss"] = rss_
                if fit["corrcoef_log"]:
                    corrcoef_ = np.corrcoef(np.log10(sigsqr[cidx]), fit_of_here(dts[cidx], *out.beta))[0][1]
                else:
                    corrcoef_ = np.corrcoef(sigsqr[cidx], 10 ** fit_of_here(dts[cidx], *out.beta))[0][1]
                if fit["corrcoef_norm"]:
                    corrcoef_ /= len(cidx)
                # df_fit.loc[ci, "quality"] = 1 / rss_
                df_fit.loc[ci, "quality"] = 1 / (rss_ * len(cidx))
                if fit["adapt_quantity"] == "time":
                    df_fit.loc[ci, "quantity"] = np.max(dts[cidx]) - np.min(dts[cidx])
                else:
                    df_fit.loc[ci, "quantity"] = len(cidx)
            if np.max(df_fit["quality"]) == 0:
                if not silent:
                    pb.tick()
                continue
            df_fit["quality"] = df_fit["quality"] / np.max(df_fit["quality"])
            df_fit["quantity"] = df_fit["quantity"] / np.max(df_fit["quantity"])
            if fit["adapt_grade_type"] == "linear":
                df_fit["grade"] = df_fit["quality"] * fit["adapt_grade_weights"][0]\
                                  + df_fit["quantity"] * fit["adapt_grade_weights"][1]
            else:
                df_fit["grade"] = np.sqrt((df_fit["quality"] * fit["adapt_grade_weights"][0]) ** 2
                                          + (df_fit["quantity"] * fit["adapt_grade_weights"][1]) ** 2)
            best_idx = np.argmax(df_fit["grade"].to_numpy())
            beta = np.squeeze(df_fit.loc[best_idx, ["beta0", "beta1"]])
            sd_beta = np.squeeze(df_fit.loc[best_idx, ["sd_beta0", "sd_beta1"]])
            chisqr_tau = np.squeeze(df_fit.loc[best_idx, "chisqr"])
            rss_tau = np.squeeze(df_fit.loc[best_idx, "rss"])
            corrcoef_tau = np.squeeze(df_fit.loc[best_idx, "corrcoef"])
            cidx = candidates[best_idx]
            tau = abs(beta[tau_idx])
            tau_err = sd_beta[tau_idx]
            if not fit["sequential"]:
                mass = beta[0]
                mass_err = sd_beta[0]
            min_dt = np.min(dts[cidx])
            max_dt = np.max(dts[cidx])

            # ============================================================
            # Investigate linearity of all data to check for "strangeness"
            out = fit_model(icaps.fit_slope2, np.log10(dts), np.log10(sigsqr),
                            fit_type=fit["fit_type"], maxit=fit["maxit"])
            if fit["chisqr_log"]:
                chisqr_lin = icaps.chisqr(np.log10(sigsqr), icaps.fit_slope2(np.log10(dts), *out.beta))
            else:
                chisqr_lin = icaps.chisqr(sigsqr, 10 ** icaps.fit_slope2(np.log10(dts), *out.beta))
            if fit["chisqr_norm"]:
                chisqr_lin /= len(dts)
            df_out.loc[idx_out, "chisqr_lin"] = chisqr_lin
            if fit["rss_log"]:
                rss_lin = icaps.rss(np.log10(sigsqr), icaps.fit_slope2(np.log10(dts), *out.beta))
            else:
                rss_lin = icaps.rss(sigsqr, 10 ** icaps.fit_slope2(np.log10(dts), *out.beta))
            if fit["rss_norm"]:
                rss_lin /= len(dts)
            df_out.loc[idx_out, "rss_lin"] = rss_lin
            # ============================================================

            # =====================
            # Writing to Table
            # =====================
            df_out.loc[idx_out, "tau0"] = tau0
            df_out.loc[idx_out, mass_column] = mass
            df_out.loc[idx_out, mass_err_column] = mass_err
            df_out.loc[idx_out, "tau"] = tau
            df_out.loc[idx_out, "tau_err"] = tau_err
            df_out.loc[idx_out, "chisqr_bal"] = chisqr_mass
            df_out.loc[idx_out, "chisqr"] = chisqr_tau
            df_out.loc[idx_out, "rss"] = rss_tau
            df_out.loc[idx_out, "corrcoef"] = corrcoef_tau
            df_out.loc[idx_out, "n_dots"] = len(cidx)
            df_out.loc[idx_out, "min_dt"] = min_dt
            df_out.loc[idx_out, "max_dt"] = max_dt
            if ret_erf:
                use = np.zeros(len(sigsqr), dtype=bool)
                use[cidx] = True
                df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                           (df_erf["axis"] == axis), "of_used"] = use
            # =====================
            # Plotting
            # =====================
            plot_this = True
            if plot["min_rss"] is not None:
                plot_this = False
                if rss_tau <= plot["min_rss"]:
                    plot_this = True
            if p/len(particles) > plot["plot_until"]:
                plot_this = False
            if (plot["path"] is not None) and plot_this:
                _, ax = plt.subplots()
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                if mode == "rot":
                    ax.set_ylabel(
                        r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.set_xscale("log")
                ax.set_yscale("log")
                if fit["max_sigsqr"] is not None:
                    ax.axhline(fit["max_sigsqr"], lw=2, ls="--", c="black")
                if plot["attempts"]:
                    for i, row in df_fit.iterrows():
                        if fit["sequential"]:
                            beta = [row["beta0"]]
                        else:
                            beta = [row["beta0"], row["beta1"]]
                        ax.plot(dt_space, 10 ** fit_of_here(dt_space, *beta),
                                c="gray", lw=1, ls="-.", alpha=0.3)
                if plot["dots"] == "DG":
                    sigma_type = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigsqr[sigma_type == "single"],
                                yerr=sigsqr_err[sigma_type == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "low double"], sigsqr[sigma_type == "low double"],
                                yerr=sigsqr_err[sigma_type == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "high double"], sigsqr[sigma_type == "high double"],
                                yerr=sigsqr_err[sigma_type == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigsqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigsqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigsqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                else:
                    ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx], sigsqr[cidx], yerr=sigsqr_err[cidx] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                if fit["chisqr_log"]:
                    label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_mass * 1e4, r"$10^{-4}$") + "\n"
                else:
                    label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_mass * 1e14, r"$10^{-14}$") + "\n"
                if mode == "trans":
                    mass_label = r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                                                                                 mass_err * 10 ** units["mass_mag"],
                                                                                 r"$10^{{{}}}$".format(
                                                                                     -units["mass_mag"]))
                else:
                    mass_label = r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                                                                                    mass_err * 10 ** units["mass_mag"],
                                                                                    r"$10^{{{}}}$".format(
                                                                                        -units["mass_mag"]))
                label += mass_label
                if fit["sequential"]:
                    if fit["ballistic_fit"][0] == "T":
                        y_ballistic = fit_ballistic(dt_space, mass, tau0)
                    else:
                        y_ballistic = 10 ** fit_ballistic(dt_space, mass, tau0)
                    ax.plot(dt_space, y_ballistic, c="red", lw=2, ls="--", zorder=20, label=label)
                if fit["chisqr_log"]:
                    label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_tau * 1e4, r"$10^{-4}$") + "\n"
                else:
                    label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_tau * 1e14, r"$10^{-14}$") + "\n"
                if fit["rss_log"]:
                    label += r"$RSS$ = {:.2f} $\cdot$ {}".format(rss_tau * 1e4, r"$10^{-4}$") + "\n"
                else:
                    label += r"$RSS$ = {:.2f} $\cdot$ {}".format(rss_tau * 1e4, r"$10^{-4}$") + "\n"
                if fit["corrcoef_log"]:
                    label += r"$CC$ = {:.2f} $\cdot$ {}".format(corrcoef_tau * 1e4, r"$10^{-4}$") + "\n"
                else:
                    label += r"$CC$ = {:.2f} $\cdot$ {}".format(corrcoef_tau * 1e4, r"$10^{-4}$") + "\n"
                if not fit["sequential"]:
                    label += mass_label + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(tau * 1e3, tau_err * 1e3)
                ax.plot(dt_space, 10 ** fit_of_log(dt_space, mass, tau),
                        c="red", lw=2, ls="-", zorder=20, label=label)
                ax.legend(loc="lower right", framealpha=1)
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot["path"][a] + "sigma_OF_{}_{}_{}.png".format(mode, axis, particle), dpi=plot["dpi"])
                plt.close()
            if not silent:
                pb.tick()
        if temp_path is not None:
            if p/len(particles) > n_temp * temp_interval:
                df_prelim = move_col(df_out, id_column, 0)
                df_prelim.to_csv(temp_path, index=False)
                n_temp += 1
    if temp_path is not None:
        try:
            os.remove(temp_path)
        except FileNotFoundError:
            pass
    if not silent:
        pb.close()
    if (len(df_out[id_column].unique()) == 1) and (df_out[id_column].unique()[0] == 0):
        df_out.drop(columns=[id_column], inplace=True)
    if ret_erf:
        return df_out, df_erf
    else:
        return df_out


def fit_ellipses_bulk(df, particle_path, particles=None, thresh=20, mark=False, mark_thresh=10, mark_enlarge=1,
                      mark_colors=None, clear_out=False, silent=True, alert=False, id_in_filename=True,
                      prefix=None, reduce_df=False, img_folder="ffc", mark_thickness=1):
    if particles is None:
        particles = np.unique(df["particle"].to_numpy())
    n_rows = df[np.in1d(df["particle"], particles)].shape[0]
    if reduce_df:
        df = df[np.in1d(df["particle"], particles)]
    pb = None
    if not silent:
        pb = SimpleProgressBar(n_rows, "Fitting Ellipses")
    df_ellipses = pd.DataFrame(data={"particle": [1], "frame": [1], "ellipse_x": [1.], "ellipse_y": [1.],
                                     "semi_minor_axis": [1.], "semi_major_axis": [1.], "ellipse_angle": [1.]})
    df_ellipses.drop(0)
    df_ellipses.reset_index(drop=True)
    for particle in particles:
        mark_out_path_ = particle_path
        in_path_ = particle_path + "{:0>6}/{}/".format(particle, img_folder)
        if mark:
            mark_out_path_ += "{:0>6}/mark/".format(particle)
            mk_folder(mark_out_path_, clear=clear_out)
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy())
        df_ellipse = pd.DataFrame(data={"particle": [1], "frame": [1], "ellipse_x": [1.], "ellipse_y": [1.],
                                        "semi_minor_axis": [1.], "semi_major_axis": [1.], "ellipse_angle": [1.]})
        df_ellipse.drop(0)
        df_ellipse.reset_index(drop=True)
        for frame in frames:
            file = "{:0>6}".format(particle) + "_" if id_in_filename else ""
            file += generate_file(frame, prefix=prefix)
            img = load_img(in_path_ + file)
            if img is None:
                if alert:
                    print("Missing Frame:", file)
                pb.tick()
                continue
            img = threshold(img, mark_thresh, replace=(0, None))
            if mark:
                box, mark_img = fit_ellipse(img, thresh, mark_enlarge, ret_marked=True, mark_colors=mark_colors,
                                            alert=alert, mark_thickness=mark_thickness)
                if mark_img is not None:
                    save_img(mark_img, mark_out_path_ + file)
            else:
                box = fit_ellipse(img, thresh, mark_enlarge, ret_marked=False, mark_colors=mark_colors, alert=alert)
            if box is None:
                pb.tick()
                continue
            else:
                df_ellipse = df_ellipse.append(pd.DataFrame(data={"particle": [particle],
                                                                  "frame": [frame],
                                                                  "ellipse_x": [box[0][0]],
                                                                  "ellipse_y": [box[0][1]],
                                                                  "semi_minor_axis": [box[1][0]],
                                                                  "semi_major_axis": [box[1][1]],
                                                                  "ellipse_angle": [box[2]]}), sort=False)
                pb.tick()
        # df_ellipse.to_csv(particle_path + "ellipse.csv", index=False)
        df_ellipses = df_ellipses.append(df_ellipse, ignore_index=True, sort=False)
    df = pd.merge(left=df, right=df_ellipses, on=["particle", "frame"], how="left")
    pb.close()
    return df


def fit_ellipses(in_path, thresh=30, files=None, mark_out_path=None, mark_thresh=10, mark_enlarge=1, mark_colors=None,
                 mark_thickness=1, clear_out=False, silent=True, alert=False, prefix="Os7-S1 Camera", suffix=None,
                 loc=None):
    """
    Fit Ellipses to all images in in_path (specified by files)
    :param in_path: path to original images
    :param thresh: threshold to apply prior to fitting
    :param files: specific files to be used
    :param mark_out_path: path to save marked images to. Leaving this None will disable the creation of marked images.
    :param mark_thresh: threshold to apply to the marked image (irrelevant for fitting)
    :param mark_enlarge: factor to enlarge the marked image by
    :param mark_colors: the two colors to be used in the marked image
    :param clear_out: whether to clear the mark_out_path beforehand
    :param silent: whether to show a progress bar
    :param alert: whether to alert when fitting has failed (will not raise an exception!)
    :param prefix: prefix for finding a frame number from filename
    :param suffix: suffix for finding a frame number from filename
    :param loc: exact location of the frame number in filename
    :return:
    """
    if mark_out_path is not None:
        mk_folder(mark_out_path, clear=clear_out)
    if files is None:
        files = get_files(in_path)
    df_ellipse = pd.DataFrame(data={"frame": [1], "ellipse_x": [1.], "ellipse_y": [1.], "semi_minor_axis": [1.],
                                    "semi_major_axis": [1.], "ellipse_angle": [1.]})
    df_ellipse.drop(0)
    df_ellipse.reset_index(drop=True)
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(files), "Fitting Ellipses")
    for file in files:
        img = load_img(in_path + file)
        frame = get_frame(file, prefix, suffix, loc)
        if img is None:
            if alert:
                print("Missing Frame:", file)
            pb.tick()
            continue
        img = threshold(img, mark_thresh, replace=(0, None))
        if mark_out_path is not None:
            box, mark_img = fit_ellipse(img, thresh, mark_enlarge, ret_marked=True, mark_colors=mark_colors,
                                        alert=alert, mark_thickness=mark_thickness)
            if mark_img is not None:
                save_img(mark_img, mark_out_path)
        else:
            box = fit_ellipse(img, thresh, mark_enlarge, ret_marked=False, mark_colors=mark_colors, alert=alert)
        if box is None:
            pb.tick()
            continue
        else:
            df_ellipse = df_ellipse.append(pd.DataFrame(data={"frame": [frame],
                                                              "ellipse_x": [box[0][0]],
                                                              "ellipse_y": [box[0][1]],
                                                              "semi_minor_axis": [box[1][0]],
                                                              "semi_major_axis": [box[1][1]],
                                                              "ellipse_angle": [box[2]]}), sort=False)
    pb.close()
    return df_ellipse


def fit_ellipse(img, thresh, mark_enlarge=1, ret_marked=False, mark_colors=None, alert=False, mark_thickness=1):
    """
    Fit Ellipse to image
    :param img: original image
    :param thresh: threshold to apply prior to fitting
    :param mark_enlarge: factor to enlarge the marked image by
    :param ret_marked: whether to return a marked image
    :param mark_colors: the two colors to be used in the marked image
    :param alert: whether to alert when fitting has failed (will not raise an exception!)
    :return: box, mark_img
    """
    thr_img = threshold(img, thresh, replace=(0, None))
    contour = cv2.findContours(thr_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    try:
        contour = contour[0][0]
    except IndexError:
        if alert:
            print("Contour not found.")
        if ret_marked:
            return None, None
        else:
            return None
    contour = np.squeeze(contour)
    try:
        box = cv2.fitEllipse(contour)
    except cv2.error:
        if alert:
            print("Could not fit ellipse.")
        if ret_marked:
            return None, None
        else:
            return None

    if not ret_marked:
        return box
    else:
        if mark_colors is None:
            mark_colors = [(255, 0, 0), (0, 0, 255)]
        center = np.array(box[0]) * mark_enlarge
        axes = np.array(box[1]) * mark_enlarge
        angle = box[2]
        mask = np.zeros(img.shape, np.uint8)
        mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
        mask = fill_holes(mask)
        img[mask == 0] = 0
        mark_img = icaps.enlarge_pixels(img, mark_enlarge)
        mark_img = cv2.cvtColor(mark_img, cv2.COLOR_GRAY2BGR)
        mark_img = cv2.drawContours(mark_img, [contour * mark_enlarge], contourIdx=0, color=mark_colors[0],
                                    thickness=mark_thickness)
        mark_img = icaps.draw_ellipse(mark_img, center, axes, angle,
                                      color=mark_colors[1], thickness=mark_thickness)
        return box, mark_img


@deprecated
def calc_tilt(df, particles=None, key="ellipse_tilt", silent=True):
    """
    *** STEP 2 ***
    Calculate tilt of
    :param df: dataframe with ellipse data
    :param particles: specific particles to calculate the tilt for
    :param key: column name for tilt (new column will be created)
    :param silent:
    :return: dataframe with tilt
    """
    if key not in df.columns:
        df[key] = np.nan
    if particles is None:
        particles = np.unique(df["particle"].to_numpy()).astype(np.uint64)
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles), "Calculating Tilt")
    for particle in particles:
        df_ = df[df["particle"] == particle]
        sma = df_["semi_major_axis"].to_numpy()
        max_sma = np.max(sma[~np.isnan(sma)])
        for idx in df_.index.to_numpy():
            if not np.isnan(df_.loc[idx, "semi_major_axis"]):
                df.loc[idx, key] = np.degrees(np.arccos(df_.loc[idx, "semi_major_axis"] / max_sma))
        if not silent:
            pb.tick()
    if not silent:
        pb.close()
    return df


def vmu_correction(df, df_vmu):
    df_vmu["time_round"] = df_vmu["timestamp"] - const.start_ldm
    df_full = pd.DataFrame(data={"frame": np.arange(0, const.nframes_ldm, 1),
                                 "time": np.linspace(0, const.nframes_ldm/const.fps_ldm*1000, const.nframes_ldm),
                                 "time_round": np.round(np.linspace(0, const.nframes_ldm/const.fps_ldm*1000,
                                                                    const.nframes_ldm)).astype(int)})
    df = pd.merge(df, df_full, on="frame", how="left")
    df_vmu = df_vmu.dropna()
    dx = df_vmu["dx"].copy()
    dy = df_vmu["dy"].copy()
    dz = df_vmu["dz"].copy()
    theta = np.radians(const.ldm_tilt)
    dx_ldm = dx * np.sin(theta) ** 2 - dy * np.cos(theta) - dz * np.sin(theta) * np.cos(theta)
    dy_ldm = -dx * np.sin(theta) * np.cos(theta) - dy * np.sin(theta) - dz * np.cos(theta) ** 2
    warnings.filterwarnings("ignore")
    df_vmu["dx_T"] = dx_ldm
    df_vmu["dy_T"] = dy_ldm
    warnings.filterwarnings("default")
    df_vmu = df_vmu.drop(columns=["dx", "dy", "dz", "timestamp"])
    df = pd.merge(df, df_vmu, on="time_round", how="left")
    df = df.drop(columns=["time_round"])
    df = move_col(df, "time", "frame", offset=1)
    df = move_col(df, "particle", "time", offset=1)
    print(df[df["frame"] == 141000]["time"])
    print(np.max(df[~np.isnan(df["dx_T"])]["frame"]))
    print(df[~np.isnan(df["dx_T"])])
    print(np.max(df_vmu["time_round"]))
    df["x_micron"] = df["x"] * const.px_ldm
    df["y_micron"] = df["y"] * const.px_ldm
    return df

