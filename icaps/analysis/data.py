import pandas as pd
import numpy as np
from scipy.optimize import curve_fit
from icaps.analysis.fitting import fit_lin


def dist_median(x, x_cum=None, approx="mean"):
    if isinstance(x, pd.Series):
        x = x.to_numpy()
    elif not isinstance(x, np.ndarray):
        x = np.array(x)
    if x_cum is None:
        x_cum = cumulate(x, norm=True)
    if approx == "simple":
        idx = np.argmin(np.abs(x_cum - 0.5))
        return x[idx]
    else:
        x_cum_offset = x_cum - 0.5
        x_cum_prev = np.amax(x_cum_offset[x_cum_offset <= 0]) + 0.5
        x_cum_next = np.amin(x_cum_offset[x_cum_offset > 0]) + 0.5
        i_prev = np.where(x_cum == x_cum_prev)[0][-1]
        i_next = np.where(x_cum == x_cum_next)[0][0]
        x_prev = x[i_prev]
        x_next = x[i_next]
        if approx == "mean":
            return np.mean([x_prev, x_next])
        elif approx == "linear":
            np.warnings.filterwarnings("ignore")
            popt, pcov = curve_fit(fit_lin, [x_prev, x_next], [x_cum_prev, x_cum_next])
            np.warnings.filterwarnings("default")
            return (0.5-popt[1])/popt[0]


def cumulate(x, sort=False, norm=False):
    if isinstance(x, pd.Series):
        x = x.to_numpy()
    elif not isinstance(x, np.ndarray):
        x = np.array(x)
    if sort:
        x = np.sort(x)
    x_cum = np.cumsum(x)
    if norm:
        x_cum = x_cum / x_cum[-1]
    return x_cum


def cumulative_median(x):
    x_cum = cumulate(x, sort=True, norm=True)
    idx = np.argmin(np.abs(x_cum - 0.5))
    return np.sort(x)[idx]


