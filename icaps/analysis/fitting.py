import numpy as np
from scipy.special import erf
# from scipy.integrate import quad
from scipy.odr import ODR, Model, RealData
from inspect import signature
import icaps.const as const


# =================================================================================================================
# Ordinary Functions
def fit_exp(x, amplitude, period):
    return amplitude * np.exp(x/period)


def fit_lin(x, a, b):
    return a * x + b


def fit_lin_log10(x, a, b):
    return 10**(a * np.log10(x) + b)


def fit_lin0(x, a):
    return a * x


def fit_slope1(x, a):
    return x + a


def fit_slope2(x, a):
    return 2 * x + a


def fit_para0(x, a):
    return a*x**2


def fit_para(x, a, b):
    return a*x**2 + b


def fit_pow0(x, a):
    return x**a


def fit_pow(x, a, b):
    return b*x**a


# =================================================================================================================
# Error Integrals
def fit_erf(x, mu, sigma):
    # return 0.5 * (1 + erf((x - mu) / sigma))
    return 0.5 * (1 + erf((x - mu) / (sigma * np.sqrt(2))))


def fit_int_exp(x, mu, lamb):
    res = 0.5 * (1 + erf(np.sqrt(np.abs(x - mu)) / lamb))
    res[x - mu < 0] = 1 - res[x - mu < 0]
    return res


def fit_int_tanh(dx, mu, lamb):
    return 1 * (np.tanh((dx - mu) / lamb) + 1) * 0.5


def fit_int_fickian(x, mu, lambda_):
    # return 0.5 * (1 + erf((x - mu) / (sigma * np.sqrt(2))))
    # return 1 / (np.sqrt(2 * np.pi)) * quad(lambda q: np.exp(-np.abs(q - mu) / lambda_), -np.inf, x)
    return 0.5 * (1 + erf(np.sqrt(np.abs(x-mu)/lambda_)))
    # print(np.array([quad(lambda q: np.exp(-np.abs(q - mu) / lambda_), 0, x_) for x_ in x])[:, 0])
    # return 0.5 * (1 + 2 / np.sqrt(np.pi) *
    #               np.array([quad(lambda q: np.exp(-np.abs(q - mu) / lambda_), 0, x_) for x_ in x])[:, 0])


def fit_erf_dg(x, w0, mu01, sigma0, sigma1):
    # w0 = np.abs(w0)
    # w0 = np.min([w0, 1.])
    return w0 * fit_erf(x, mu01, sigma0) + (1 - w0) * fit_erf(x, mu01, sigma1)


def fit_erf_exp(dx, mu, sigma, w, lamb):
    w = np.abs(w)
    w = np.min([w, 1.])
    res = (1 - w) * fit_erf(dx, mu, sigma) + w * fit_int_exp(dx, mu, lamb)
    return res


def fit_erf_exp_sol(dx, mu, sigma, w, lamb):
    res = (1 - w) * fit_erf(dx, mu, sigma)
    idx_p = dx - mu >= 0
    idx_n = dx - mu < 0
    res[idx_n] += w / 2 * np.exp((dx[idx_n] - mu) / lamb)
    res[idx_p] += w / 2 * (2 - np.exp(-(dx[idx_p] - mu) / lamb))
    return res


def fit_erf_tanh(dx, mu, sigma, w, lamb):
    res = (1 - w) * fit_erf(dx, mu, sigma) + w * fit_int_tanh(dx, mu, lamb)
    return res


def fit_erf_fk(x, w0, mu01, sigma0, lambda1):
    return w0 * fit_erf(x, mu01, sigma0) + (1 - w0) * fit_int_fickian(x, mu01, lambda1)


# =================================================================================================================
# Ornstein-Fuerth
def fit_of_log(t, m, tau):
    tau = np.abs(tau)
    return np.log10(2 * const.k_B * const.T / m) + 2*np.log10(tau) + np.log10(t/tau - 1 + np.exp(-t/tau))


def fit_of_taylor2(t, m):
    a = const.k_B * const.T / m
    return a * t ** 2


def fit_of_taylor3(t, m, tau):
    a = const.k_B * const.T / m
    return a * t**2 - a * t**3 / (3 * tau)


def fit_of_taylor4(t, m, tau):
    a = const.k_B * const.T / m
    return a * t**2 - a * t**3 / (3 * tau) + a * t**4 / (12 * tau**2)


def fit_of_taylor5(t, m, tau):
    a = const.k_B * const.T / m
    return a * t**2 - a * t**3 / (3 * tau) + a * t**4 / (12 * tau**2) - a * t**5 / (60 * tau**3)


def fit_of_lin(t, m, tau):
    return 2 * const.k_B * const.T / m * tau**2 * (t/tau - 1 + np.exp(-t/tau))


# =================================================================================================================
# MoI
def fit_moi(z, lamb0, lamb1, lamb2):
    return lamb0 * z * (1 + lamb1 * z ** (-lamb2)) ** 2


def fit_moi_log(z, lamb0, lamb1, lamb2):
    return np.log10(lamb0) + np.log10(z) + 2 * np.log10(1 + lamb1 * z ** (-lamb2))


def fit_moi_log_half(z, lamb0, lamb1):
    return fit_moi_log(z, lamb0, lamb1, 0.5)

# def fit_MoI(x, a, b):
#     return np.log10(x * (1 + (a/x)**b)**2)
#
#
# def fit_MoI_lin(x, a):
#     return np.log10(x + a)
#
#
# def fit_MoI_half(x, a):
#     return fit_MoI(x, a, 0.5)


# =================================================================================================================
# Growth
def fit_growth(t, k, n_0, tau, vK_0):
    t_ = 1 + t/tau
    c = (n_0 ** (1 - k) - (k - 1) / (k + 1) * tau * vK_0) * tau ** (k - 1)
    print(c)
    return (c * tau ** (1 - k) * t_ ** (1 - k) + (k - 1) / (k + 1) * tau * vK_0 * t_ ** 2) ** (1 / (1 - k))


def fit_growth_c(t, k, tau, vK_0, c):
    t_ = 1 + t / tau
    return (c * tau ** (1 - k) * t_ ** (1 - k) + (k - 1) / (k + 1) * tau * vK_0 * t_ ** 2) ** (1 / (1 - k))


# =================================================================================================================
# Utility
def chisqr(observed, expected, norm=1):
    return 1/norm * np.sum((observed - expected) ** 2 / expected)


def rss(observed, expected, norm=1):
    return 1/norm * np.sum((observed - expected) ** 2)


def log_err(val, err):
    return (err / val) / np.log(10)


def fit2model(fit_func):
    return lambda p, x: fit_func(x, *p)


def fit_model(func, x, y, xerr=None, yerr=None, p0=None, fit_type=0, maxit=None, is_model=False,
              model_args=None, data_args=None, odr_args=None, job_args=None):
    if p0 is None:
        nparams = len(signature(func).parameters.keys()) - 1
        p0 = np.zeros(nparams, dtype=int)
        p0[:] = 1
    if model_args is None:
        model_args = {}
    if data_args is None:
        data_args = {}
    if odr_args is None:
        odr_args = {}
    if job_args is None:
        job_args = {}
    linear_model = Model(func if is_model else fit2model(func), *model_args)
    data = RealData(x, y, sx=xerr, sy=yerr, *data_args)
    odr = ODR(data, linear_model, beta0=p0, maxit=maxit, *odr_args)
    odr.set_job(fit_type=fit_type, *job_args)
    out = odr.run()
    return out


