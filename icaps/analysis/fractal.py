import numpy as np
import cv2
from icaps.presentation.draw import draw_bbox
from icaps.io import save_img, mk_folder
from icaps.presentation.display import cvshow
from icaps.processing.filter import threshold
from icaps.presentation.ProgressBar import ProgressBar


def get_gray_limits(img, bs, orientation="horizontal", suppress_warnings=True):
    grays = img.copy()
    grays[grays == 255] = 0
    grays = np.any(grays, axis=0 if orientation == "horizontal" else 1)
    if grays.size - np.sum(grays) < bs:
        return -1, -1
    grays_diff = np.diff(grays.astype(int))
    idx = np.where(np.abs(grays_diff) == 1)[0] + 1
    inside_idx = idx[idx < 2 * bs]
    inside_idx = inside_idx[inside_idx >= bs]
    if inside_idx.size > 1:
        if not suppress_warnings:
            print("Inside Index Error")
        return -1, -1
    if inside_idx.size == 0:
        if idx.any():
            return bs, 2*bs
        else:
            return 0, 3*bs
    inside_idx = inside_idx[0]
    i = np.argwhere(idx == inside_idx)[0][0]
    if grays[inside_idx]:
        max_idx = inside_idx
        if i == 0:
            min_idx = 0
        else:
            min_idx = idx[i - 1]
    else:
        min_idx = inside_idx
        if i == idx.size - 1:
            max_idx = 3 * bs
        else:
            max_idx = idx[i + 1]
    if max_idx == bs:
        return -1, -1
    else:
        return min_idx, max_idx


def get_best_square(prox_img, bs, min_idx, max_idx, orientation="horizontal"):
    center = 3/2*bs-min_idx
    if orientation == "horizontal":
        prox_img = prox_img[:, min_idx:max_idx]
    else:
        prox_img = prox_img[min_idx:max_idx, :]
    white = np.where(prox_img == 255)
    if white[0].size == 0 and white[1].size == 0:
        return None
    if orientation == "horizontal":
        pos = np.array([np.amin(white[1]), np.amax(white[1])])
    else:
        pos = np.array([np.amin(white[0]), np.amax(white[0])])
    dist_center = np.abs(pos-center)
    if not dist_center[0] == dist_center[1]:
        pos_idx = np.argmin(dist_center)
    else:
        if orientation == "horizontal":
            i0 = prox_img[:, pos[0]:pos[0] + bs]
            i1 = prox_img[:, pos[1] - bs:pos[1]]
        else:
            i0 = prox_img[pos[0]:pos[0] + bs, :]
            i1 = prox_img[pos[1] - bs:pos[1], :]
        pos_idx = np.argmax([np.sum(i0), np.sum(i1)])
    if pos_idx == 0:
        pos_new = min_idx + pos[0]
    else:
        pos_new = min_idx + pos[1] - bs + 1
    if pos_new + bs > max_idx:
        pos_new = max_idx - bs
    if pos_new < min_idx:
        pos_new = min_idx
    return pos_new


def variate(x, y, bs, img, max_bs, count=0, to_variate=None, show_diashow=False,
            movie_out_path=None, diashow_t=1, suppress_warnings=True):
    if to_variate is None:
        to_variate = []
    img[y:y + bs, x:x + bs] = 100
    draw_img = draw_bbox(img, (x, y, bs, bs))
    draw_img = cv2.copyMakeBorder(draw_img, top=max_bs - bs, bottom=max_bs - bs, left=max_bs - bs, right=max_bs - bs,
                                  borderType=cv2.BORDER_CONSTANT, value=0)
    if show_diashow:
        cvshow(draw_img, t=diashow_t)
    if movie_out_path is not None:
        save_img(draw_img, movie_out_path + "{num:0>{fill}}".format(fill=2, num=bs) + "_" +
                 "{num:0>{fill}}".format(fill=4, num=count) + "m.bmp")

    up = img[y - bs:y, x - bs:x + 2 * bs]
    if up.size > 0:
        min_idx, max_idx = get_gray_limits(up, bs, orientation="horizontal", suppress_warnings=suppress_warnings)
        if min_idx >= 0 and max_idx >= 0 and (up[-1, bs:2 * bs] == 255).any():
            dx = get_best_square(up, bs, min_idx, max_idx, orientation="horizontal")
            if dx is not None:
                img[y - bs:y, x - bs + dx:x + dx] = 100
                to_variate.append((x - bs + dx, y - bs))
                count += 1
            draw_img = cv2.copyMakeBorder(img, top=max_bs - bs, bottom=max_bs - bs, left=max_bs - bs, right=max_bs - bs,
                                          borderType=cv2.BORDER_CONSTANT, value=0)
            if show_diashow:
                cvshow(draw_img, t=diashow_t)
            if movie_out_path is not None:
                save_img(draw_img, movie_out_path + "{num:0>{fill}}".format(fill=2, num=bs) + "_" +
                         "{num:0>{fill}}".format(fill=4, num=count) + ".bmp")

    right = img[y - bs:y + 2 * bs, x + bs:x + 2 * bs]
    if right.size > 0:
        min_idx, max_idx = get_gray_limits(right, bs, orientation="vertical", suppress_warnings=suppress_warnings)
        if min_idx >= 0 and max_idx >= 0 and (right[bs:2*bs, 0] == 255).any():
            dy = get_best_square(right, bs, min_idx, max_idx, orientation="vertical")
            if dy is not None:
                img[y - bs + dy:y + dy, x + bs:x + 2 * bs] = 100
                to_variate.append((x+bs, y-bs+dy))
                count += 1
            draw_img = cv2.copyMakeBorder(img, top=max_bs - bs, bottom=max_bs - bs, left=max_bs - bs, right=max_bs - bs,
                                          borderType=cv2.BORDER_CONSTANT, value=0)
            if show_diashow:
                cvshow(draw_img, t=diashow_t)
            if movie_out_path is not None:
                save_img(draw_img, movie_out_path + "{num:0>{fill}}".format(fill=2, num=bs) + "_" +
                         "{num:0>{fill}}".format(fill=4, num=count) + ".bmp")

    left = img[y - bs:y + 2 * bs, x - bs:x]
    if left.size > 0:
        min_idx, max_idx = get_gray_limits(left, bs, orientation="vertical", suppress_warnings=suppress_warnings)
        if min_idx >= 0 and max_idx >= 0 and (left[bs:2 * bs, -1] == 255).any():
            dy = get_best_square(left, bs, min_idx, max_idx, orientation="vertical")
            if dy is not None:
                img[y - bs + dy:y + dy, x - bs:x] = 100
                to_variate.append((x - bs, y - bs + dy))
                count += 1
            draw_img = cv2.copyMakeBorder(img, top=max_bs - bs, bottom=max_bs - bs, left=max_bs - bs, right=max_bs - bs,
                                          borderType=cv2.BORDER_CONSTANT, value=0)
            if show_diashow:
                cvshow(draw_img, t=diashow_t)
            if movie_out_path is not None:
                save_img(draw_img, movie_out_path + "{num:0>{fill}}".format(fill=2, num=bs) + "_" +
                         "{num:0>{fill}}".format(fill=4, num=count) + ".bmp")

    down = img[y + bs:y + 2 * bs, x - bs:x + 2 * bs]
    if down.size > 0:
        min_idx, max_idx = get_gray_limits(down, bs, orientation="horizontal", suppress_warnings=suppress_warnings)
        if min_idx >= 0 and max_idx >= 0 and (down[0, bs:2 * bs] == 255).any():
            dx = get_best_square(down, bs, min_idx, max_idx, orientation="horizontal")
            if dx is not None:
                img[y + bs:y + 2 * bs, x - bs + dx:x + dx] = 100
                to_variate.append((x - bs + dx, y + bs))
                count += 1
            draw_img = cv2.copyMakeBorder(img, top=max_bs - bs, bottom=max_bs - bs, left=max_bs - bs, right=max_bs - bs,
                                          borderType=cv2.BORDER_CONSTANT, value=0)
            if show_diashow:
                cvshow(draw_img, t=diashow_t)
            if movie_out_path is not None:
                save_img(draw_img, movie_out_path + "{num:0>{fill}}".format(fill=2, num=bs) + "_" +
                         "{num:0>{fill}}".format(fill=4, num=count) + ".bmp")

    while to_variate:
        tv = to_variate[0]
        to_variate = to_variate[1:]
        img, to_variate, count = variate(tv[0], tv[1], bs, img, max_bs, count=count, to_variate=to_variate,
                                         show_diashow=show_diashow, diashow_t=diashow_t,
                                         suppress_warnings=suppress_warnings)

    return img, to_variate, count


def boxcount(img, max_bs=10, min_bs=2, iter_bs=1, movie_out_path=None, method="variate", silent=True,
             show_diashow=False, diashow_t=1, suppress_warnings=True):
    if movie_out_path is not None:
        mk_folder(movie_out_path, clear=True, prompt=False)
    img = threshold(img, 0, replace=(0, 255))
    counts = np.zeros(max_bs - min_bs + 1)
    box_sizes = np.arange(max_bs, min_bs - 1, -iter_bs)
    pb = None
    if not silent:
        pb = ProgressBar(min_bs, val=max_bs, title="Box Counting", mode="true")
    for i, bs in enumerate(box_sizes):
        img_ = cv2.copyMakeBorder(img, top=bs, bottom=bs, left=bs, right=bs, borderType=cv2.BORDER_CONSTANT, value=0)
        y = bs
        x = 0
        ynonzero = np.nonzero(img_[y])[0]
        if ynonzero.size > 0:
            x = np.amin(np.nonzero(img_[y])[0])
        mark_img, _, count = variate(x, y, bs, img_, max_bs, movie_out_path=movie_out_path, show_diashow=show_diashow,
                                     diashow_t=diashow_t, suppress_warnings=suppress_warnings)
        counts[i] = count
        if not silent:
            pb.set(bs)
    if not silent:
        pb.finish()
    box_sizes = np.append(box_sizes, 1)
    counts = np.append(counts, np.count_nonzero(img))
    return box_sizes, counts

