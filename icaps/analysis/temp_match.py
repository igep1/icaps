import cv2
import numpy as np


def norm_match_res(res, method=cv2.TM_CCOEFF):
    """
    normalizes the match result
    :param res: match result (ndarray)
    :param method: method used to create res, currently only TM_CCOEFF is implemented
    :return: normalized match result
    """
    res_min = np.amin(res)
    res_max = np.amax(res)
    if res_min < 0:
        res += abs(res_min)
    res = res/np.amax(res)
    # if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
    return res


def cvt_match_res_bool(res, acc=1.):
    """
    thresholds the match result
    :param res: match result (ndarray)
    :param acc: accuracy, between 0 and 1 if match result is normalized
    :return: thresholded match result
    """
    res_bool = res.copy()
    res_bool[res_bool >= acc] = 1
    res_bool[res_bool < acc] = 0
    res_bool = res_bool.astype(np.bool)
    return res_bool


def draw_match_pts(img, res, color=255, centered=True):
    """
    draws all match results > 0 onto image, best to use normalized, thresholed results
    :param img: image (ndarray)
    :param res: thresholded match results (ndarray)
    :param color: color
    :param centered: whether to center the match results (since res.shape is always smaller than img.shape)
    :return: image with match results drawn onto it (ndarray)
    """
    res_c = np.zeros((img.shape[0], img.shape[1]))
    if centered:
        tshape_half = (int((img.shape[0] - res.shape[0])/2), int((img.shape[1] - res.shape[1])/2))
        res_c[tshape_half[0]:tshape_half[0] + res.shape[0], tshape_half[1]:tshape_half[1] + res.shape[1]] = res
    else:
        res_c[0:res.shape[0], 0:res.shape[1]] = res
    res = res_c
    img[res > 0] = color
    return img




