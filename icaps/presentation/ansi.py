none = "\x1b[0m"
black = "\x1b[38;5;0m"
dark_red = "\x1b[38;5;1m"
dark_green = "\x1b[38;5;2m"
gold = "\x1b[38;5;3m"
dark_blue = "\x1b[38;5;4m"
purple = "\x1b[38;5;5m"
teal = "\x1b[38;5;6m"
light_gray = light_grey = "\x1b[38;5;7m"
gray = grey = "\x1b[38;5;8m"
red = "\x1b[38;5;9m"
green = "\x1b[38;5;10m"
yellow = "\x1b[38;5;11m"
blue = "\x1b[38;5;12m"
pink = "\x1b[38;5;13m"
light_blue = "\x1b[38;5;14m"
white = "\x1b[38;5;15m"

bold = "\x1b[1m"
underline = "\x1b[4m"
slow_blink = "\x1b[5m"
font0 = "\x1b[10m"
font1 = "\x1b[11m"
font2 = "\x1b[12m"
font3 = "\x1b[13m"
font4 = "\x1b[14m"
font5 = "\x1b[15m"
font6 = "\x1b[16m"
font7 = "\x1b[17m"
font8 = "\x1b[18m"
font9 = "\x1b[19m"
framed = "\x1b[51m"
overline = "\x1b[53m"


def dye(s, color, effect=None):
    if effect is None:
        effect = ""
    elif isinstance(effect, str):
        if not (effect.startswith("\x1b[") or effect.startswith("\033[")):
            effect = globals()[effect.replace(" ", "_").lower()]
    if isinstance(color, str):
        if not (color.startswith("\x1b[") or color.startswith("\033[")):
            color = globals()[color.replace(" ", "_").lower()]
    return effect + color + s + none













