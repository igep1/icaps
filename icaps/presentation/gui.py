import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseEvent


class PositionSelectionWindow:
    def __init__(self, fig=None, img=None, name="Position Selection Window", cmap="gray", vmin=0, vmax=255):
        self.fig = fig
        if fig is None:
            self.fig = plt.figure(name)
        self.pos = (0, 0)
        if img is not None:
            plt.gca().imshow(img, cmap=cmap, vmin=vmin, vmax=vmax)
        self.fig.canvas.mpl_connect("key_press_event", self.update)
        self.fig.canvas.mpl_connect("button_press_event", self.update)
        self.fig.canvas.mpl_connect("close_event", self.close)
        plt.show()

    def update(self, event):
        if isinstance(event, MouseEvent):
            if event.button == 1:
                self.pos = [event.xdata, event.ydata]
                if self.pos[0] is not None:
                    plt.close()

    @staticmethod
    def close(event):
        plt.close()


class LabelSelectionWindow:
    def __init__(self, img, labels, name="Label Selection Window", cmap="gray", vmin=0, vmax=255):
        self.figure = plt.figure(name)
        self.selected_label = 0
        plt.gca().imshow(img, cmap=cmap, vmin=vmin, vmax=vmax)
        self.labels = labels
        self.figure.canvas.mpl_connect("key_press_event", self.update)
        self.figure.canvas.mpl_connect("button_press_event", self.update)
        self.figure.canvas.mpl_connect("close_event", self.close)
        plt.show()

    def update(self, event):
        if isinstance(event, MouseEvent):
            if event.button == 1:
                mouse_click = [event.xdata, event.ydata]
                self.selected_label = self.labels[int(round(mouse_click[1])), int(round(mouse_click[0]))]
                if self.selected_label > 0:
                    plt.close()

    @staticmethod
    def close(event):
        plt.close()

