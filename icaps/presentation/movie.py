import imageio

import icaps
from icaps.io import get_files, mk_folder, load_img, generate_file, save_img
from icaps.presentation.ProgressBar import ProgressBar
from icaps.processing.tables import row2dict
from icaps.processing.prep import crop
from icaps.presentation.draw import enlarge_pixels
from icaps.presentation.draw import draw_circle, draw_labelled_bbox, apply_color
from icaps.utils import is_iterable
import numpy as np
from shutil import rmtree


def mk_movie(in_path, out_path, files=None, fps=30):
    """
    makes a movie from all given images
    :param in_path: input path, unix and icaps style pathing possible
    :param out_path: output path with file name
    :param files: specific files to use from in_path
    :param fps: frames per second
    :return:
    """
    images = []
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    for file in files:
        images.append(imageio.imread(in_path + file))
    imageio.mimwrite(out_path, images, fps=fps)


def mk_gif(in_path, out_path, files=None, duration=0.2, loop=0):
    """
    makes a gif from all given images
    :param in_path: input path, unix and icaps style pathing possible
    :param out_path: output path with file name
    :param files: specific files to use from in_path
    :param duration: duration of each image
    :param loop: amount of times to loop
    :return:
    """
    assert out_path.endswith(".gif")
    images = []
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    for file in files:
        images.append(imageio.imread(in_path + file))
    imageio.mimwrite(out_path, images, duration=duration, loop=loop)


def mk_track_movies(df, in_path, out_path, camera, sub_folder=None, particle_ids=None, enlarge=1, movie_path=None,
                    crop_bounds=True, draw_bbox=True, draw_label=True, mark_size=1, no_movie=False, color=(0, 0, 255),
                    silent=True, clear_out=True, clear_after=False, frame_gap=1, fps=None, mark_flags=None,
                    drop_flags=None, flag_color=(70, 70, 70)):
    if drop_flags is not None:
        if isinstance(drop_flags, str):
            drop_flags = [drop_flags]
        for drop_flag in drop_flags:
            df = df[df[drop_flag] == 1]
    if mark_flags is not None:
        if isinstance(mark_flags, str):
            mark_flags = [mark_flags]
    # if is_iterable(flag_color):
    #     if not is_iterable(flag_color[0]):
    #         flag_color = [flag_color for mark_flag in mark_flags]
    #     else:
    #         assert len(flag_color) == len(mark_flags)
    if movie_path is not None:
        mk_folder(movie_path)
    if particle_ids is None:
        particle_ids = np.unique(df["particle"].to_numpy())
    count = 0
    for particle in particle_ids:
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy())
        if frame_gap > 1:
            frames = frames[::frame_gap]
        count += len(frames)
    pb = None
    if not silent:
        pb = ProgressBar(count + (0 if no_movie else len(particle_ids)), "Plotting Tracks", n_bars=2)
    for particle in particle_ids:
        df_ = df[df["particle"] == particle]
        frames = df_["frame"].unique()
        if frame_gap > 1:
            frames = frames[::frame_gap]
        if not silent:
            pb.set_stage(1, frames, str(particle) + " track", show_val=True)
        out_path_ = out_path + "{}/".format(particle)
        out_path_ += "{}/".format(sub_folder) if sub_folder is not None else ""
        mk_folder(out_path_, clear=clear_out)
        min_bx = np.min(df_["bx"].to_numpy())
        min_by = np.min(df_["by"].to_numpy())
        max_bx = np.max(df_["bx"].to_numpy() + df_["bw"].to_numpy())
        max_by = np.max(df_["by"].to_numpy() + df_["bh"].to_numpy())
        if not crop_bounds:
            min_bx = min_by = 0
        track = np.zeros((len(frames), 2))
        if "LDM" in camera:
            img = load_img(in_path + generate_file(frames[0]))
        elif "OOS" in camera:
            img = load_img(in_path + generate_file(frames[0], digits=5, prefix=""))
        else:
            raise ValueError("Cameras: LDM, OOS")
        if crop_bounds:
            img = crop(img, (min_bx, min_by), (max_bx, max_by))
        img = enlarge_pixels(img, enlarge)
        track_img = np.zeros(img.shape, dtype=np.uint8)
        for i, frame in enumerate(frames):
            row = row2dict(df_[df_["frame"] == frame])
            if "LDM" in camera:
                img = load_img(in_path + generate_file(frame))
            elif "OOS" in camera:
                img = load_img(in_path + generate_file(frame, digits=5, prefix=""))
            else:
                raise ValueError("Cameras: LDM, OOS")
            if img is None:
                print("Missing Frame: ", frame)
            if crop_bounds:
                img = crop(img, (min_bx, min_by), (max_bx, max_by))
            img = enlarge_pixels(img, enlarge)
            track[i, 0] = (row["x"] - min_bx)*enlarge
            track[i, 1] = (row["y"] - min_by)*enlarge
            bbox = None
            if draw_bbox:
                bbox = np.array((row["bx"] - min_bx, row["by"] - min_by, row["bw"], row["bh"])) * enlarge
            label = ""
            if draw_label:
                label = str(particle)
            pos_x = np.around((row["x"] - min_bx)*enlarge).astype(int)
            pos_y = np.around((row["y"] - min_by)*enlarge).astype(int)
            # flags = np.array([row[mark_flag] for mark_flag in mark_flags])
            flags = [False]     # hotfix
            img, color = apply_color(img, color)
            if np.any(flags):
                flag = int(np.argmax(flags) + 2)
                track_img = draw_circle(track_img, pos=(pos_x, pos_y), color=flag, radius=mark_size, thickness=-1)
            else:
                flag = 1
                track_img = draw_circle(track_img, pos=(pos_x, pos_y), color=flag, radius=mark_size, thickness=-1)
            for m, c in enumerate([color, *flag_color]):
                img[track_img == m + 1] = c
            img = draw_labelled_bbox(img, bbox=bbox, label=label, color=[color, *flag_color][flag - 1],
                                     thickness=mark_size)
            # print(pos_x, pos_y)
            # print(track_img.shape)
            # cvshow(track_img, size=0.5, t=1)
            # img[np.where(track_img)] = color
            # img = draw_labelled_bbox(img, bbox=bbox, label=label, color=color, thickness=mark_size)
            # cvshow(img, size=0.5, t=1)
            save_img(img, out_path_ + "{}_{:0>5}.bmp".format(particle, frame))
            if not silent:
                pb.tick()
        if not no_movie:
            if not silent:
                pb.set_stage(1, -1, str(particle) + " movie")
            if fps is None:
                if "LDM" in camera:
                    fps = max(int(round(30/frame_gap)), 1)
                else:
                    fps = max(int(round(10/frame_gap)), 1)
            if movie_path is None:
                mk_movie(out_path_, out_path + "{}/{}.mp4".format(particle, particle), fps=fps)
            else:
                mk_movie(out_path_, movie_path + "{}.mp4".format(particle), fps=fps)
            if not silent:
                pb.tick()
        if clear_after:
            rmtree(out_path_)
    if not silent:
        pb.close()



