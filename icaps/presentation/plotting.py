import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.legend import Legend
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit

import icaps
from icaps.io import get_files
import inspect
from pandas.plotting import register_matplotlib_converters


# def switch_axis_side(ax, axis="y", minorticks=True):
#     if not icaps.is_iterable(ax):
#         ax = [ax]
#     if axis == "both":
#         axis = ["x", "y"]
#     else:
#         axis = [axis]
#     for ax_ in ax:
#         if "x" in axis:
#             twin = ax_.twiny()
#             twin.set_xlim(*ax_.get_xlim())
#             twin.set_xscale(ax_.get_xscale())
#             twin.xaxis.set_ticklabels([])
#             twin.set_xlabel(ax.get_xlabel())
#             ax.xaxis.set_ticklabels([])
#             ax.set_xlabel()
#             if minorticks:
#                 twin.minorticks_on()
#                 ax.xaxis.minorticks_on()
#         if "y" in axis:
#             twin = ax_.twinx()
#             twin.set_ylim(*ax_.get_ylim())
#             twin.set_yscale(ax_.get_yscale())
#             twin.yaxis.set_ticklabels([])
#             twin.set_ylabel(ax.get_ylabel())
#             ax.yaxis.set_ticklabels([])
#             ax.set_ylabel()
#             if minorticks:
#                 twin.minorticks_on()
#                 ax.yaxis.minorticks_on()

def val2str(value, decimals=None):
    if decimals is not None:
        return "{0:.{1}f}".format(np.around(value, decimals), decimals)
    else:
        return str(value)


def get_param_label(params, space_before_unit=True, simplify_low_high=True, align=False, latex=True):
    """
    :param params: list of parameters, each consisting of
                   [name, value, error(s) (optional, iterable/float), decimals (optional, int), unit (optional, str)].
    :param space_before_unit: Whether to put a space before the unit.
    :param simplify_low_high: Whether to change errors to single error if low error == high error.
    :param align: Whether to align the output on the equal sign (latex only).
    :param latex: Whether to use latex.
    :return: label
    """
    pm = r"\,\pm\," if latex else " \u00b1 "
    if not latex:
        align = False
    res = ""
    if align:
        res = r"\begin{eqnarray*} "
    for i, param in enumerate(params):
        res += param[0]
        if len(param) > 1:
            if align:
                res += r"&=&"
            else:
                res += r"\;=\;" if latex else " = "
            value = param[1]
            errors = []
            decimals = 2
            unit = ""
            if len(param) > 2:
                for p in param[2:]:
                    if isinstance(p, int):
                        decimals = p
                    elif p is None:
                        decimals = None
                    elif isinstance(p, str):
                        unit = p
                    elif isinstance(p, float):
                        errors = [p]
                    elif icaps.is_iterable(p):
                        errors = p
                    else:
                        raise ValueError
            if unit != "":
                if space_before_unit:
                    unit = "\," + unit
                if len(errors) == 1:
                    res += "("
                    unit = ")" + unit
            if len(errors) == 0:
                res += val2str(value, decimals)
            elif len(errors) == 1:
                res += val2str(value, decimals) + pm + val2str(errors[0], decimals)
            else:
                err_str0 = val2str(errors[0], decimals)
                err_str1 = val2str(errors[1], decimals)
                if simplify_low_high and (err_str0[1:] == err_str1):
                    res += val2str(value, decimals) + pm + err_str1
                else:
                    if latex:
                        res += val2str(value, decimals) + r"_{{{}}}^{{+{}}}".format(err_str0, err_str1)
                    else:
                        res += val2str(value, decimals) + "+{}-{}".format(err_str0, err_str1)
            res += unit

        if i < len(params) - 1:
            res += r"\\" if latex else "\n"
    if align:
        res += r"\end{eqnarray*}"
    return res


def twin_cbar(cbar, lim=None, label=None, ticks=None):
    pos = cbar.ax.get_position()
    cbar.ax.set_aspect("auto")
    cax = cbar.ax.twinx()
    if lim is not None:
        if lim == "mirror":
            lim = cbar.ax.get_ylim()
        cax.set_ylim(lim)
    cbar.ax.set_position(pos)
    cax.set_position(pos)
    if label is not None:
        cax.set_ylabel(label)
    if ticks is not None:
        cax.yaxis.set_ticks(ticks)
    return cax


def mirror_axis(ax, axis="both", minorticks=True):
    if not icaps.is_iterable(ax):
        ax = [ax]
    if axis == "both":
        axis = ["x", "y"]
    else:
        axis = [axis]
    for ax_ in ax:
        if "x" in axis:
            twin = ax_.twiny()
            twin.set_xlim(*ax_.get_xlim())
            twin.set_xscale(ax_.get_xscale())
            twin.xaxis.set_ticklabels([])
            if minorticks:
                twin.minorticks_on()
        if "y" in axis:
            twin = ax_.twinx()
            twin.set_ylim(*ax_.get_ylim())
            twin.set_yscale(ax_.get_yscale())
            twin.yaxis.set_ticklabels([])
            if minorticks:
                twin.minorticks_on()


def seek_lim(data, to_seek, keys=None):
    """
    seeks the limits for an axis
    :param data: list, ndarray, Series, Dataframe or path, unix or icaps style possible
    :param to_seek: list of features to seek, currently "min" and "max" are implemented
    :param keys: keys to use if data is Dataframe or path
    :return: lower limit, upper limit
    """
    min_ = max_ = None
    if not to_seek:
        return min_, max_
    if type(data) in (str, pd.DataFrame):
        if type(data) == str:
            files, data = get_files(data, ret_path=True)
        else:
            files = [0]
        for file in files:
            if type(data) == str:
                df = pd.read_csv(data + file)
            else:
                df = data
            if keys:
                df = df[keys]
            d = df.to_numpy()
            if "min" in to_seek:
                dmin = np.amin(d)
                if min_ is None:
                    min_ = dmin
                elif dmin < min_:
                    min_ = dmin
            if "max" in to_seek:
                dmax = np.amax(d)
                if max_ is None:
                    max_ = dmax
                elif dmax > max_:
                    max_ = dmax
        return min_, max_
    elif type(data) in (list, np.ndarray, pd.Series):
        if type(data) == pd.Series:
            data = np.array(data)
        if type(data) == list:
            data_len = len(data)
        else:
            data_len = data.shape[0]
        for i in range(data_len):
            if "min" in to_seek:
                dmin = np.amin(data[i])
                if min_ is None:
                    min_ = dmin
                elif dmin < min_:
                    min_ = dmin
            if "max" in to_seek:
                dmax = np.amax(data[i])
                if max_ is None:
                    max_ = dmax
                elif dmax > max_:
                    max_ = dmax
        return min_, max_
    return min_, max_


def get_lim(data, lim, keys=None):
    """
    interprets and seeks the limits for an axis
    :param data: list, ndarray, Series, Dataframe or path, unix or icaps style possible
    :param lim: tuple of limits, elements being None, int or "min"/"max"
    :param keys: keys to use if data is Dataframe or path
    :return:
    """
    to_seek = []
    ret_lim = [None, None]
    if lim[0] is not None:
        if lim[0] == "min":
            to_seek.append("min")
        else:
            ret_lim[0] = lim[0]
    if lim[1] is not None:
        if lim[1] == "max":
            to_seek.append("max")
        else:
            ret_lim[1] = lim[1]
    if to_seek:
        ret = seek_lim(data, to_seek, keys=keys)
        if ret[0] is not None:
            ret_lim[0] = ret[0]
        if ret[1] is not None:
            ret_lim[1] = ret[1]
    return ret_lim


def parse_y(y):
    """
    interprets y data
    :param y: y data, can be list, ndarray or Series OR 2-dimensional list, ndarray or Dataframe
    :return: y data as list, containing lists, ndarrays or Series
    """
    if isinstance(y, list):
        if type(y[0]) not in (list, np.ndarray, pd.Series):
            y = [y]
    if isinstance(y, np.ndarray):
        if len(y.shape) > 1:
            y = list(y)
        else:
            y = [y]
    if isinstance(y, pd.Series):
        y = [y]
    if isinstance(y, pd.DataFrame):
        y = [y[key] for key in y.keys()]
    return y


def parse_color(color, amount=1, no_cmaps=True):
    """
    interprets the color
    :param color: color (None, int, list of int or Colormap)
    :param amount: amount of colors needed (usually number of y data sets)
    :param no_cmaps: whether Colormaps should be returned as lists instead
    :return: Colormap or list of colors, the length of which matches amount
    """
    if color is None:
        return [color for i in range(amount)]
    if isinstance(color, mcolors.Colormap):
        if no_cmaps:
            return [color(i) for i in range(amount)]
        else:
            return color
    else:
        try:
            rgba = mcolors.to_rgba(color)
        except ValueError:
            rgba = None
        if rgba is None:
            if len(color) == 1:
                return [color[0] for i in range(amount)]
            elif len(color) >= amount:
                return color
            else:
                color_loop = []
                while len(color_loop) < amount:
                    for c in color:
                        color_loop.append(c)
                        if len(color_loop) == amount:
                            break
                return color_loop
        else:
            return [color for i in range(amount)]


def parse_marker(marker, amount=1):
    """
    interprets the marker
    :param marker: marker (str or list of str)
    :param amount: amount of markers needed (usually number of y data sets)
    :return: list of markers, the length of which matches amount
    """
    if isinstance(marker, str) or marker is None:
        marker = [marker]
    if len(marker) < amount:
        marker_loop = []
        while len(marker_loop) < amount:
            for m in marker:
                marker_loop.append(m)
                if len(marker_loop) == amount:
                    break
        return marker_loop
    else:
        return marker


def parse_label(label, amount=1):
    """
    interprets label
    :param label: label (None, str or list of str)
    :param amount: amount of labels needed (usually number of y data sets)
    :return: list of labels
    """
    if isinstance(label, str) or label is None:
        label = [label]
    if len(label) < amount:
        return label + [None for i in range(amount-len(label))]
    else:
        return label


def parse_timestamp(x):
    if isinstance(x, (pd.Series, np.ndarray)):
        # if x.dtype == Timestamp:
        #     x = pd.to_datetime(x.astype(str))
        if x.dtype in (pd.Timestamp, "datetime64[ns]"):
            register_matplotlib_converters()
    # elif isinstance(x, (tuple, list)):
        # if isinstance(x[0], Timestamp):
        #     x_new = np.zeros(len(x), dtype=pd.Timestamp)
        #     for i in range(len(x)):
        #         x_new[i] = pd.Timestamp(str(x[i]))
        #     x = x_new.copy()
        #     register_matplotlib_converters()
    return x


def plot_scatter(x, y, axlabels=(None, None), grid=(True, True), scale=("linear", "linear"), title=None,
                 xlim=(None, None), ylim=(None, None), xbuff=(None, None), ybuff=(None, None),
                 color="black", marker="o", label=None, marker_size=1, legend=False, legend_kwargs=None,
                 *args, **kwargs):
    """
    plots a simple scatter plot
    :param x: x data (list, ndarray, Series)
    :param y: y data, can be list, ndarray or Series OR 2-dimensional list, ndarray or Dataframe OR path, unix or icaps
              pathing possible
    :param axlabels: tuple of axis labels
    :param grid: tuple of booleans, whether to draw a grid on the axis
    :param scale: tuple of axis scales
    :param title: title of the plot
    :param xlim: tuple of limits for the x axis, elements can be None, float or "min"/"max"
    :param ylim: tuple of limits for the y axis, elements can be None, float or "min"/"max"
    :param xbuff: tuple of buffers for the x axis as a percentage of total axis length, elements can be None or float
    :param ybuff: tuple of buffers for the y axis as a percentage of total axis length, elements can be None or float
    :param color: Colormap, color or list of colors, loops up to the number of y data sets
    :param marker: marker or list of markers, loops up to the number of y data sets
    :param label: label or list of labels, length should be <= number of y data sets
    :param marker_size: marker size (int)
    :param legend: whether to draw a legend
    :param legend_kwargs: kwargs for legend
    :param args: args of plt.scatter()
    :param kwargs: kwargs of plt.scatter()
    :return: figure, axis
    """
    fig, ax = plt.subplots()
    if title is not None:
        ax.set_title(title)
    ax.xaxis.grid(grid[0])
    ax.yaxis.grid(grid[1])
    ax.set_axisbelow(True)
    if "lin" in scale:
        new_scale = []
        for s in scale:
            if s == "lin":
                new_scale.append("linear")
            else:
                new_scale.append(s)
        scale = tuple(new_scale)
    ax.set_xscale(scale[0])
    ax.set_yscale(scale[1])

    x = parse_timestamp(x)

    xlim = get_lim(x, xlim)
    if xbuff[0] is not None:
        xlim[0] = xlim[0] - xbuff[0] * np.abs(xlim[1]-xlim[0])
    if xbuff[1] is not None:
        xlim[1] = xlim[1] + xbuff[1] * np.abs(xlim[1]-xlim[0])
    if xlim[0] is not None:
        ax.set_xlim(left=xlim[0])
    if xlim[1] is not None:
        ax.set_xlim(right=xlim[1])

    ylim = get_lim(y, ylim)
    if ybuff[0] is not None:
        ylim[0] = ylim[0] - ybuff[0] * np.abs(ylim[1]-ylim[0])
    if ybuff[1] is not None:
        ylim[1] = ylim[1] + ybuff[1] * np.abs(ylim[1]-ylim[0])
    if ylim[0] is not None:
        ax.set_ylim(bottom=ylim[0])
    if ylim[1] is not None:
        ax.set_ylim(top=ylim[1])

    y = parse_y(y)
    color = parse_color(color, amount=len(y))
    marker = parse_marker(marker, amount=len(y))
    label = parse_label(label, amount=len(y))

    if axlabels[0]:
        ax.set_xlabel(axlabels[0])
    if axlabels[1]:
        ax.set_ylabel(axlabels[1])

    for i, y_ in enumerate(y):
        ax.scatter(x, y_, s=marker_size, marker=marker[i],
                   color=color(i) if color is mcolors.Colormap else color[i],
                   label=label[i] if label[i] else None,
                   *args, **kwargs)
    if legend:
        if legend_kwargs is not None:
            ax.legend(**legend_kwargs)
        else:
            ax.legend()

    fig.tight_layout()

    return fig, ax


def plot_scatter_twinx(x, y1, y2, axlabels=(None, None, None), colorize_axlabels=True, grid=(True, False, False),
                       scale=("linear", "linear", "linear"), title=None, xlim=(None, None), ylim1=(None, None),
                       ylim2=(None, None), xbuff=(None, None), ybuff1=(None, None), ybuff2=(None, None),
                       color1=None, color2=None, marker1=".", marker2=".", label1=None, label2=None,
                       marker_size=1, legend=(False, False), legend_kwargs=(None, None), *args, **kwargs):
    """
    plots a twin axis scatter plot
    :param x: x data (list, ndarray, Series)
    :param y1: y1 data, can be list, ndarray or Series OR 2-dimensional list, ndarray or Dataframe OR path, unix or
               icaps pathing possible
    :param y2: y2 data, can be list, ndarray or Series OR 2-dimensional list, ndarray or Dataframe OR path, unix or
               icaps pathing possible
    :param axlabels: tuple of axis labels
    :param colorize_axlabels: whether to colorize the axis labels, will choose the first of each color
    :param grid: tuple of booleans, whether to draw a grid on the axis
    :param scale: tuple of axis scales
    :param title: title of the plot
    :param xlim: tuple of limits for the x axis, elements can be None, float or "min"/"max"
    :param ylim1: tuple of limits for the y1 axis, elements can be None, float or "min"/"max"
    :param ylim2: tuple of limits for the y2 axis, elements can be None, float or "min"/"max"
    :param xbuff: tuple of buffers for the x axis as a percentage of total axis length, elements can be None or float
    :param ybuff1: tuple of buffers for the y1 axis as a percentage of total axis length, elements can be None or float
    :param ybuff2: tuple of buffers for the y2 axis as a percentage of total axis length, elements can be None or float
    :param color1: Colormap, color or list of colors, loops up to the number of y1 data sets
    :param color2: Colormap, color or list of colors, loops up to the number of y2 data sets
    :param marker1: marker or list of markers, loops up to the number of y1 data sets
    :param marker2: marker or list of markers, loops up to the number of y2 data sets
    :param label1: label or list of labels, length should be <= number of y1 data sets
    :param label2: label or list of labels, length should be <= number of y2 data sets
    :param marker_size: marker size (int)
    :param legend: tuple of booleans, whether to draw a legend for each y axis
    :param legend_kwargs: tuple of kwargs for legends
    :param args: args of plt.scatter()
    :param kwargs: kwargs of plt.scatter()
    :return: figure, axis1, axis2
    """
    fig, ax1 = plt.subplots()
    if title is not None:
        ax1.set_title(title)
    ax1.xaxis.grid(grid[0])
    ax1.yaxis.grid(grid[1])
    ax1.set_axisbelow(True)
    ax1.set_xscale(scale[0])
    ax1.set_yscale(scale[1])

    x = parse_timestamp(x)

    xlim = get_lim(x, xlim)
    if xbuff[0]:
        xlim[0] = xlim[0] - xbuff[0] * np.abs(xlim[1] - xlim[0])
    if xbuff[1]:
        xlim[1] = xlim[1] + xbuff[1] * np.abs(xlim[1] - xlim[0])
    if xlim[0] and xlim[1]:
        ax1.set_xlim(xlim)
    ylim = get_lim(y1, ylim1)
    if ybuff1[0]:
        ylim[0] = ylim[0] - ybuff1[0] * np.abs(ylim[1] - ylim[0])
    if ybuff1[1]:
        ylim[1] = ylim[1] + ybuff1[1] * np.abs(ylim[1] - ylim[0])
    if ylim[0] and ylim[1]:
        ax1.set_ylim(ylim)

    y1 = parse_y(y1)
    color1 = parse_color(color1, amount=len(y1))
    marker1 = parse_marker(marker1, amount=len(y1))
    label1 = parse_label(label1, amount=len(y1))

    if axlabels[0]:
        ax1.set_xlabel(axlabels[0])
    if axlabels[1]:
        c = (color1(0) if isinstance(color1, mcolors.Colormap) else color1[0]) if colorize_axlabels else None
        ax1.set_ylabel(axlabels[1], color=c)

    for i, y_ in enumerate(y1):
        ax1.scatter(x, y_, s=marker_size, marker=marker1[i],
                    color=color1(i) if color1 is mcolors.Colormap else color1[i],
                    label=label1[i] if label1[i] else None,
                    *args, **kwargs)
    if legend[0]:
        if legend_kwargs[0] is not None:
            ax1.legend(**legend_kwargs[0])
        else:
            ax1.legend()

    ax2 = ax1.twinx()
    ax2.yaxis.grid(grid[2])
    ax2.set_axisbelow(True)
    ax2.set_yscale(scale[2])

    ylim = get_lim(y2, ylim2)
    if ybuff2[0]:
        ylim[0] = ylim[0] - ybuff2[0] * np.abs(ylim[1] - ylim[0])
    if ybuff2[1]:
        ylim[1] = ylim[1] + ybuff2[1] * np.abs(ylim[1] - ylim[0])
    if ylim[0] and ylim[1]:
        ax2.set_ylim(ylim)

    y2 = parse_y(y2)
    color2 = parse_color(color2, amount=len(y2))
    marker2 = parse_marker(marker2, amount=len(y2))
    label2 = parse_label(label2, amount=len(y2))

    if axlabels[2]:
        c = (color2(0) if isinstance(color2, mcolors.Colormap) else color2[0]) if colorize_axlabels else None
        ax2.set_ylabel(axlabels[2], color=c)

    for i, y_ in enumerate(y2):
        ax2.scatter(x, y_, s=marker_size, marker=marker2[i],
                    color=color2(i) if color2 is mcolors.Colormap else color2[i],
                    label=label2[i] if label2[i] else None,
                    *args, **kwargs)
    if legend[1]:
        if legend_kwargs[1] is not None:
            ax2.legend(**legend_kwargs[1])
        else:
            ax2.legend()

    fig.tight_layout()

    return fig, ax1, ax2


def plot_fit(x, y, func, xspace=None, xspace_lim=("min", "max"), xspace_num=1000, fig=None, ax=0, guesses=None,
             bounds=(-np.inf, np.inf), linestyle="-", color=None, linewidth=1, legend=True, separate_legend=True,
             legend_kwargs=None, label="Fit", add_info=True, func_label=None, arg_labels=None, decimals=3,
             scale="linear", fit_kwargs=None):
    """
    plots a fit, todo: when parsing timestamp, parse to seconds and then back to timestamps
    :param x: x data (list, ndarray, Series)
    :param y: y data, can be list, ndarray or Series
    :param func: fit function (callable)
    :param xspace: x data over which to plot the fit
    :param xspace_lim: tuple of lower and upper limit, defining xspace, omitted if xspace is given
    :param xspace_num: number of values to use in xspace, omitted if xspace is given
    :param fig: figure to plot on, defaults to current figure
    :param ax: index of axis to plot on, defaults to 0
    :param guesses: initial guesses for fit params
    :param bounds: bounds for fit params
    :param linestyle: linestyle (str)
    :param color: color (int)
    :param linewidth: linewidth (int)
    :param legend: whether to draw a legend for the fit
    :param separate_legend: whether to separate the fit legend from any legends already present
    :param legend_kwargs: kwargs for fit legend
    :param label: label of the fit
    :param add_info: whether to add parameter information to the fit legend
    :param func_label: label of the fit function, omitted if add_info is False, defaults to func.__name__
    :param arg_labels: list of labels of the fit parameters, omitted if add_info is False, defaults to
                       inspect.getfullargspec(func).args
    :param decimals: decimals to round the params in the legend to
    :param scale: axis scale, will adjust fit values before plotting
    :param fit_kwargs: kwargs for the fit, overwrites guesses and bounds if defined in kwargs
    :return: figure, axis, parameters, covariance matrix, errors
    """
    if not fig:
        fig = plt.gcf()
    if isinstance(ax, int):
        ax = fig.get_axes()[ax]

    x = parse_timestamp(x)

    if fit_kwargs:
        if "p0" not in list(fit_kwargs.keys()):
            fit_kwargs["p0"] = guesses
        if "bounds" not in list(fit_kwargs.keys()):
            fit_kwargs["bounds"] = bounds
        popt, pcov = curve_fit(func, x, y, **fit_kwargs)
    else:
        popt, pcov = curve_fit(func, x, y, p0=guesses, bounds=bounds)
    perr = np.sqrt(np.diag(pcov))

    if not xspace:
        if xspace_lim[0] == "min":
            xs_left = np.amin(x)
        elif xspace_lim[0] == "axmin":
            xs_left = ax.get_xlim()[0]
        else:
            xs_left = xspace_lim[0]
        if xspace_lim[1] == "max":
            xs_right = np.amax(x)
        elif xspace_lim[1] == "axmax":
            xs_right = ax.get_xlim()[1]
        else:
            xs_right = xspace_lim[1]
        xspace = np.linspace(xs_left, xs_right, xspace_num)
    if add_info:
        if arg_labels:
            if isinstance(arg_labels, str):
                arg_labels = [arg_labels]
        label = label + "\n" + (func_label if func_label else func.__name__)
        func_args = inspect.getfullargspec(func).args
        for i in range(len(popt)):
            popt_round = np.around(popt[i], decimals=decimals)
            perr_round = np.around(perr[i], decimals=decimals)
            if decimals == 0:
                popt_round = int(popt_round)
                perr_round = int(perr_round)
            if arg_labels:
                if i < len(arg_labels):
                    label += "\n" + arg_labels[i] + " = " + str(popt_round) + " \u00b1 " + str(perr_round)
                else:
                    label += "\n" + func_args[i + 1] + " = " + str(popt_round) + " \u00b1 " + str(perr_round)
            else:
                label += "\n" + func_args[i+1] + " = " + str(popt_round) + " \u00b1 " + str(perr_round)
    if scale == "log":
        yspace = 10**func(xspace, *popt)
    else:
        yspace = func(xspace, *popt)

    legends = [c for c in ax.get_children() if isinstance(c, Legend)]

    fit, = ax.plot(xspace, yspace, linestyle, color=color, linewidth=linewidth, label=label)

    if legend:
        if separate_legend:
            if legend_kwargs:
                ax.legend([fit], [label], **legend_kwargs)
            else:
                ax.legend([fit], [label])
            for leg in legends:
                ax.add_artist(leg)
        else:
            ax.legend()

    return fig, ax, popt, pcov, perr


"""
def restructure_legends(ax, to_unify):
    legends = [c for c in ax.get_children() if isinstance(c, Legend)]
    if isinstance(to_unify[0], int):
        to_unify = [to_unify]
    # ax.get_legend().remove()
    for leg in legends:
        leg.remove()
    new_legends = []
    for uni in to_unify:
        new_legend = ax.legend([leg])
    return ax
"""





