import numpy as np
import pandas as pd
from icaps.io import get_frame


def chunkify(arr, chunk_size, div=False):
    chunk_div = len(arr) // chunk_size
    if div:
        chunk_div = chunk_size
        chunk_size = len(arr) // chunk_div
    chunks = [arr[i * chunk_size: (i + 1) * chunk_size] for i in range(chunk_div)]
    chunks.append(arr[chunk_div * chunk_size:])
    return chunks


def drop_intervals(arg, intervals, key=None, prefix=None, suffix=None, loc=None):
    if isinstance(arg, pd.DataFrame):
        assert(key is not None)
        df = arg
        for intv in intervals:
            df_after = None
            df_before = None
            if not intv[0] <= np.amin(np.array(df[key])):
                df_before = df[df[key] < intv[0]]
            else:
                df = df[df[key] > intv[1]]
            if not intv[1] >= np.amax(np.array(df[key])):
                df_after = df[df[key] > intv[1]]
            else:
                df = df[df[key] < intv[0]]
            if not intv[0] <= np.amin(np.array(df[key])) and not intv[1] >= np.amax(np.array(df[key])):
                df = df_after.append(df_before, ignore_index=True)
        return df
    else:
        arr = np.array(arg)
        files, frames = None, None
        if arr.dtype.kind == "U":
            files = arr.copy()
            frames = np.array([get_frame(file, prefix=prefix, suffix=suffix, loc=loc) for file in files])
            arr = frames.copy()
        for intv in intervals:
            arr_after = None
            arr_before = None
            if not intv[0] <= np.amin(arr):
                arr_before = arr[arr < intv[0]]
            else:
                arr = arr[arr > intv[1]]
            if not intv[1] >= np.amax(arr):
                arr_after = arr[arr > intv[1]]
            else:
                arr = arr[arr < intv[0]]
            if not intv[0] <= np.amin(arr) and not intv[1] >= np.amax(arr):
                arr = np.append(arr_before, arr_after)
        if files is not None:
            arr = [files[frames == a][0] for a in arr]
        return arr


def is_in_intervals(n, intervals):
    intervals = np.array(intervals)
    imin = np.amin(intervals)
    imax = np.amax(intervals)
    if n > imax or n < imin:
        return False
    for intv in intervals:
        if intv[0] <= n <= intv[1]:
            return True
    return False


def cutoffs2intervals(cutoffs, max_val, min_val=0):
    cutoffs = [min_val] + cutoffs + [max_val]
    intervals = []
    for i in range(len(cutoffs[:-1])):
        intervals.append((cutoffs[i], cutoffs[i+1]))
    return intervals

