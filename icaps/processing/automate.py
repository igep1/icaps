from icaps.utils import is_iterable
from icaps.io import mk_folder, get_files, load_img
from icaps.presentation.ProgressBar import ProgressBar
import matplotlib.pyplot as plt
import cv2


def process_sequence(img, funcs):
    if not is_iterable(funcs):  # not nested
        img = funcs(img)
    else:
        if not is_iterable(funcs[0]):  # first order nested, assume only one function
            if type(funcs[-1]) == dict:
                img = funcs[0](img, *funcs[1:-1], **funcs[-1])
            else:
                img = funcs[0](img, *funcs[1:])
        else:
            for i, func in enumerate(funcs):
                if not is_iterable(func):  # first order nested
                    img = func(img)
                else:  # second order nested
                    if type(func[-1]) == dict:
                        img = func[0](img, *func[1:-1], **func[-1])
                    else:
                        img = func[0](img, *func[1:])
    return img


def bulk_sequence(in_path, out_path, funcs, files=None, clear=False, flags=-1, strip=False, height=32, cmap=None,
                  out_frmt=None, silent=True):
    mk_folder(out_path, clear=clear)
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    pb = None
    if not silent:
        pb = ProgressBar(0, len(files), 'Bulk Operations on "' + in_path + "'")
    for file in files:
        img = load_img(in_path + file, flags=flags, strip=strip, height=height)
        img = process_sequence(img, funcs)
        if out_frmt:
            file = file.strip(".")
            file[-1] = out_frmt
            ".".join(file)
        if cmap:
            plt.imsave(out_path + file, img, cmap=cmap)
        else:
            cv2.imwrite(out_path + file, img)
        if pb:
            pb.tick()
    if pb:
        pb.finish()






