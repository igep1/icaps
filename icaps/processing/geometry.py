import numpy as np
from icaps.utils import is_iterable


def rectangle(tl, br, wh=True):
    if wh:
        br = [tl[0] + br[0], tl[1] + br[1]]
    tr = [br[0], tl[1]]
    bl = [tl[0], br[1]]
    return np.array([tl, tr, br, bl])


def get_points_center(points, decimals=-1):
    tpoints = np.transpose(points)
    center = np.array([np.sum(tpoints[0]) / len(tpoints[0]), np.sum(tpoints[1]) / len(tpoints[1])])
    if decimals < 0:
        return center
    elif decimals == 0:
        return np.round(center).astype(int)
    return np.round(center, decimals=decimals)


def scale_points(points, scale, center=None, decimals=-1):
    dims = len(points[0])
    if not is_iterable(scale):
        scale = [scale]
    scale = np.array(scale)
    if len(scale) < dims:
        scale = np.append(scale, np.repeat(scale[-1], dims - len(scale)))
    if center is None:
        center = get_points_center(points)
    points = translate_points(points, -center)
    for i in range(len(scale)):
        points[:, i] = points[:, i] * scale[i]
    points = translate_points(points, center)
    if decimals < 0:
        return points
    elif decimals == 0:
        return np.round(points).astype(int)
    return np.round(points, decimals=decimals)


def move_points(points, target, center=None, decimals=-1):
    target = np.array(target)
    if center is None:
        center = get_points_center(points)
    offset = target - center
    return translate_points(points, offset, decimals=decimals)


def translate_points(points, offset, decimals=-1):
    offset = np.array(offset)
    tpoints = np.transpose(points).astype(offset.dtype)
    tpoints[0, :] += offset[0]
    tpoints[1, :] += offset[1]
    points = np.transpose(tpoints)
    if decimals < 0:
        return points
    elif decimals == 0:
        return np.round(points).astype(int)
    return np.round(points, decimals=decimals)


def rotate_points(points, angle, center=None, decimals=-1):
    if center is None:
        center = get_points_center(points)
    angle = np.radians(angle)
    mat = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
    points = translate_points(points, -center)
    points = np.transpose(np.dot(mat, np.transpose(points)))
    points = translate_points(points, center)
    if decimals < 0:
        return points
    elif decimals == 0:
        return np.round(points).astype(int)
    return np.round(points, decimals=decimals)


def line_intersect(a1, a2, b1, b2):
    s = np.vstack([a1, a2, b1, b2])
    h = np.hstack((s, np.ones((4, 1))))
    l1 = np.cross(h[0], h[1])
    l2 = np.cross(h[2], h[3])
    x, y, z = np.cross(l1, l2)
    if z == 0:
        return float('inf'), float('inf')
    return x/z, y/z


def extend_line(p1, p2, w, h):
    if not p1[0] == 0:
        p1_ = p1 * -w / 2 / p1[0] + p2
        p2_ = p1 * w / 2 / p1[0] + p2
    else:
        p1_ = p1 * -h / 2 / p1[1] + p2
        p2_ = p1 * h / 2 / p1[1] + p2
    return p1_, p2_


def intersect2d(arr1, arr2, val=255):
    where1 = np.where(arr1 == val)
    where2 = np.where(arr2 == val)
    inscty = np.intersect1d(where1[0], where2[0])
    intersecty = []
    intersectx = []
    if len(inscty) > 0:
        for iny in inscty:
            where1x = where1[1][where1[0] == iny]
            where2x = where2[1][where2[0] == iny]
            insctx = np.intersect1d(where1x, where2x)
            if len(insctx) > 0:
                for inx in insctx:
                    intersecty.append(iny)
                    intersectx.append(inx)
    intersecty = np.array(intersecty)
    intersectx = np.array(intersectx)
    return intersecty, intersectx

