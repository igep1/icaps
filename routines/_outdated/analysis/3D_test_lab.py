import icaps
import numpy as np
import cv2
import vtk
import vedo

# drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# project_path = drive_letter + ":/icaps/data/Agglomerates/517224/"
# ffc_path = project_path + "ffc/"
# thresh_path = project_path + "thresh/"
# outline_path = project_path + "outline/"
#
# SHAPE = (1144, 1064)
# DZ = 2.
# DX = 1.001
# DY = 1.001
# THRESH = 80
# AXES = 4

project_path = "F:/Desktop/modeltest/"
thresh_path = project_path + "hull2/"
outline_path = project_path + "outline/"

# SHAPE = (2429, 3008)
SHAPE = (2472, 1792)
DZ = 0.1
DX = 0.03
DY = 0.03
AXES = 4


def vectorize(vxl, cond=None, dx=1., dy=1., dz=1., include_mass=False):
    if cond is None:
        vec = np.transpose(np.nonzero(vxl > 0)).astype(np.uint64)
    else:
        vec = np.transpose(np.nonzero(cond(vxl))).astype(np.uint64)
    if include_mass:
        vec = np.c_[vec, np.zeros(len(vec), dtype=np.uint64)]
        q = np.transpose(vec[:, :3])
        vec[:, 3] = vxl[q[0], q[1], q[2]][:]
    vec = vec.astype(np.float)
    vec[:, 0] *= dz
    vec[:, 1] *= dy
    vec[:, 2] *= dx
    vec[:, [0, 2]] = vec[:, [2, 0]]
    return vec


def calculate_com(vec):
    return np.sum(vec[:, :3] * np.transpose(np.tile(vec[:, 3], (3, 1))), axis=0) / np.sum(vec[:, 3])


def reduce_to_contours(img):
    erosion_kernel = np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8)
    img_er = cv2.erode(img, erosion_kernel, iterations=1)
    return img - img_er


def shift_coordinates(vec, to="min"):
    if isinstance(to, str):
        if to == "min":
            for i in range(3):
                m = np.min(vec[:, i])
                vec[:, i] -= m
    return vec


def normalize(v):
    norm = np.linalg.norm(v)
    if norm == 0:
        return v
    return v / norm


def calculate_normals(vec, com):
    normals = vec[:, :3] - com[:]
    normals = normals / np.transpose(np.tile(np.linalg.norm(normals, axis=1), (3, 1)))
    return normals


def main():
    # files = icaps.get_files(ffc_path, prefix="517224_", start=26620, stop=26690)
    #
    # # todo: cap off top and bottom for all first entries and last exits
    #
    # if True:
    #     pb = icaps.ProgressBar(len(files), "Thresholding")
    #     icaps.mk_folder(thresh_path, clear=True)
    #     icaps.mk_folder(outline_path, clear=True)
    #     start_idx_found = False
    #     start_idx = 0
    #     stop_idx = -1
    #     for i, file in enumerate(files):
    #         img = icaps.load_img(ffc_path + file)
    #         thr_img = icaps.threshold(img, THRESH, replace=(0, None))
    #         outline_img = icaps.threshold(img, THRESH, replace=(0, 255))
    #         if np.count_nonzero(outline_img) == 0 and not start_idx_found:
    #             pb.tick()
    #             continue
    #         elif np.count_nonzero(outline_img) != 0 and not start_idx_found:
    #             start_idx_found = True
    #             start_idx = i
    #         elif (np.count_nonzero(outline_img) == 0 and start_idx_found) or (i == len(files) - 1):
    #             if np.count_nonzero(outline_img) == 0 and start_idx_found:
    #                 img = icaps.load_img(ffc_path + files[i-1])
    #                 outline_img = icaps.threshold(img, THRESH, replace=(0, 255))
    #                 icaps.save_img(outline_img, outline_path + files[i-1])
    #                 stop_idx = i
    #             elif i == len(files) - 1:
    #                 icaps.save_img(outline_img, outline_path + file)
    #                 stop_idx = i + 1
    #             pb.set(len(files))
    #             break
    #         else:
    #             outline_img = reduce_to_contours(outline_img)
    #         icaps.save_img(thr_img, thresh_path + file)
    #         icaps.save_img(outline_img, outline_path + file)
    #         pb.tick()
    #     pb.finish()
    # files = files[start_idx:stop_idx]

    files = icaps.get_files(thresh_path)

    # vxl = np.zeros((len(files), *SHAPE), dtype=np.uint8)
    vxl = np.zeros((len(files), SHAPE[1], SHAPE[0]), dtype=np.uint8)
    pb = icaps.ProgressBar(len(files), "Calculating COM")
    for i, file in enumerate(files):
        thr_img = icaps.load_img(thresh_path + file)
        # vxl[i] = np.flip(thr_img, axis=0)
        vxl[i] = thr_img
        pb.tick()
    pb.finish()
    vec = vectorize(vxl, dx=DX, dy=DY, dz=DZ, include_mass=True)
    # print(vec)
    # vec = shift_coordinates(vec, to="min")
    com = calculate_com(vec)
    # print(com)

    # vxl = np.zeros((len(files), *SHAPE), dtype=np.uint8)
    # pb = icaps.ProgressBar(len(files), "Calculating COM")
    # for i, file in enumerate(files):
    #     outline_img = icaps.load_img(outline_path + file)
    #     vxl[i] = np.flip(outline_img, axis=0)
    #     pb.tick()
    # pb.finish()
    # vec = vectorize(vxl, dx=DX, dy=DY, dz=DZ)
    # vec = shift_coordinates(vec, to="min")
    # normals = calculate_normals(vec, com)

    vp = vedo.Plotter(N=2)
    print(vec)
    print(len(vec))
    print(vec[::1000, :3])
    pts = vedo.Points(vec[::1000, :3], r=3)
    print(pts.points())
    pts.color("blue").legend("original\npoint cloud").alpha(1)
    com = vedo.Sphere(com, r=0.75*8*DX, c="red")
    vp.show([pts, com], at=0, axes=AXES)
    # vp.show(pts, at=0, axes=AXES)

    # f_mls = 0.2
    # n_mls = 5
    # f_skeleton = 0.2
    # n_skeleton = 0
    mls = pts.clone()
    # for i in range(n_mls):
    #     mls = mls.smoothMLS2D(f=f_mls)
    # mls = vedo.pointcloud.removeOutliers(mls, 10, 5)
    # mls.legend("moving least squares\nf = " + str(f_mls)).color("yellow").alpha(1)
    # skeleton = mls.clone()
    # for i in range(n_skeleton):
    #     skeleton = skeleton.smoothMLS1D(f=f_skeleton)
    # skeleton.color("black")
    # # mls.computeNormalsWithPCA()
    # # vp.show([mls, skeleton], at=1, axes=AXES)
    # vp.show(mls, at=1, axes=AXES)

    mesh = vedo.pointcloud.recoSurface(mls, dims=250, radius=10*DZ, sampleSize=100, holeFilling=True)
    mesh.legend("reconstructed\nmesh").color("crimson").alpha(1)
    vp.show(mesh, at=1, axes=AXES, interactive=1)


if __name__ == '__main__':
    main()
