import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os

project = "LDM_ROT5"
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/{}/".format(project)
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
particle_rot_path = project_path + "particles_rot/"
plot_path = project_path + "plots/"
plot_dg_path = project_path + "plots_dg/"
thilo_path = project_path + "thilo/"
thilo_fname = "distribution_values_center_uncertainty_gen_{}.txt"

csv_phases = project_path + "Phases_Hand.csv"
csv_track_all = project_path + "Tracks_all.csv"
csv_track = project_path + "Tracks_PE1L100.csv"
csv_rot_of = project_path + "OF_Rot.csv"
csv_rot_usable = project_path + "usable_rot_OF_particles.txt"
csv_trans_disp = project_path + "DISP_Trans.csv"
csv_trans_erf = project_path + "ERF_Trans.csv"
csv_trans_erf_flagged = project_path + "ERF_Trans_flagged.csv"
csv_trans_of_sg = project_path + "OF_Trans_SG.csv"
csv_final_sg = project_path + "OF_Final_SG.csv"
csv_trans_erf_dg = project_path + "ERF_Trans_DG.csv"
csv_trans_erf_noise = project_path + "ERF_Trans_Noise.csv"
csv_trans_of_dg = project_path + "OF_Trans_DG.csv"
csv_final_dg = project_path + "OF_Final_DG.csv"

dts = np.arange(1, 26, 1)
dts = np.append(dts, np.arange(30, 51, 5))
# dts_dg = np.array([1, 2, 3, 4, 5])
dts_dg = dts

axis = "x"
test_parts = [363347, 364287, 373867, 380776, 389634, 393067, 394504, 389633, 389619, 383295]


def test_ornstein_fuerth(df_erf, dts=None, axes=None, particles=None, mode="trans",
                         id_column="particle", mass_column="mass",
                         units=None, fit=None, plot=None, double_gauss=False, sigma_diff=False,
                         temp_path=None, temp_interval=0.1, ret_erf=False, silent=True):
    # =====================
    # Parameter Setup
    # =====================
    units_ = {"sigma": "m",
              "time": "s",
              "time_cvt": 1e-3,
              "mass_mag": 14}
    if mode == "rot":
        units_ = icaps.merge_dicts(units_, {"sigma": "rad", "mass_mag": 25})
        if mass_column == "mass":
            mass_column = "inertia"
    mass_err_column = mass_column + "_err"
    units = icaps.merge_dicts(units_, {} if units is None else units)
    fit_ = {"p0": None,
            "maxit": None,
            "fit_type": 0,
            "fit_err": True,
            "bounds": (None, None),
            "ballistic_bounds": (None, 5),
            "ballistic_fit": 0,
            "max_sigsqr": None,
            "max_err_ratio": 1.,
            "min_tau": None}
    fit = icaps.merge_dicts(fit_, {} if fit is None else fit)
    plot_ = {"path": None,
             "dpi": 600,
             "clear_out": True,
             "errors": True,
             "attempts": False,
             "crop_to_max": True}
    plot = icaps.merge_dicts(plot_, {} if plot is None else plot)
    if sigma_diff:
        plot["errors"] = False
        fit["fit_err"] = False
    if plot["path"] is not None:
        mpl.use("Agg")
    n_temp = 0
    if id_column is None:
        df_erf["particle"] = 0
        id_column = "particle"
    if particles is None:
        particles = np.unique(df_erf[id_column].to_numpy())
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_raw = dts.copy()
    dts = dts.astype(np.float64) * units["time_cvt"]
    dt_space = np.linspace(np.min(dts), np.max(dts), 1000)
    if not ret_erf:
        df_erf = df_erf[df_erf["dt"].isin(dts_raw)]
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    elif isinstance(axes, str):
        axes = [axes]
    if plot["path"] is None:
        plot["attempts"] = False
    else:
        if "{}" in plot["path"]:
            plot["path"] = [plot["path"].format(axis) for axis in axes]
        else:
            plot["path"] = [plot["path"] for axis in axes]
        icaps.mk_folders(plot["path"], clear=plot["clear_out"])
    if fit["p0"] is None:
        fit["p0"] = [None, None]
    fit["p0"] = list(fit["p0"])
    p0_m = icaps.const.m_0
    if mode == "rot":
        p0_m *= icaps.const.r_0 ** 2
    fit["p0"][0] = p0_m if fit["p0"][0] is None else fit["p0"][0]
    fit["p0"][1] = 4.8e-3 if fit["p0"][1] is None else fit["p0"][1]

    nan_arr = np.zeros(len(particles) * len(axes))
    nan_arr[:] = np.nan
    df_out = pd.DataFrame(data={id_column: np.repeat(particles, len(axes)),
                                "axis": np.tile(axes, len(particles)),
                                mass_column: nan_arr,
                                mass_err_column: nan_arr,
                                "tau": nan_arr,
                                "tau_err": nan_arr,
                                "chisqr_bal": nan_arr,
                                "chisqr": nan_arr,
                                "n_dots": np.zeros(len(particles) * len(axes), dtype=int),
                                "t_diff": nan_arr,
                                "tau0": nan_arr,
                                "chisqr_lin": nan_arr
                                })
    # =====================
    # Start of Loop
    # =====================
    pb = None
    if not silent:
        pb = icaps.ProgressBar(len(particles) * len(axes), "Calculating OF Fits")
    for p, particle in enumerate(particles):
        df_erf_ = df_erf[(df_erf[id_column] == particle) & (df_erf["dt"].isin(dts_raw))]
        for a, axis in enumerate(axes):
            idx_out = df_out.index[(df_out[id_column] == particle) & (df_out["axis"] == axis)]
            ax = None
            if plot["path"] is not None:
                _, ax = plt.subplots()
                if fit["max_sigsqr"] is not None:
                    ax.axhline(fit["max_sigsqr"], lw=2, ls="--", c="black")
            if sigma_diff:
                sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_diff"].to_numpy()) / 2
                sig_err = np.zeros_like(sig)
            else:
                sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma"].to_numpy())
                sig_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_err"].to_numpy())
            sigsqr = sig**2
            sigsqr_err = 2 * sig_err * sig
            chisqr_erf = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "chisqr"].to_numpy())
            # =====================
            # Sequential Fitting
            # =====================
            candidates = icaps.brownian.parse_candidates(dts_raw, sigsqr, sigsqr_err,
                                                         fit_bounds=fit["ballistic_bounds"], adapt_margins=(0, 0),
                                                         n_min=1, max_sigsqr=fit["max_sigsqr"],
                                                         max_err_ratio=fit["max_err_ratio"])
            if len(candidates) == 0:
                pb.tick()
                continue
            cidx = candidates[0]
            log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None

            out = icaps.fit_model(icaps.fit_of_log, dts[cidx], np.log10(sigsqr[cidx]),
                                  yerr=log_err if fit["fit_err"] else None,
                                  p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - icaps.fit_of_log(dts[cidx], *out.beta)) ** 2) / len(cidx)
            ax.plot(dt_space, 10 ** icaps.fit_of_log(dt_space, *out.beta),
                    c="blue", lw=2, ls="-.", zorder=20,
                    label="OF (log) {:.2f}".format(chisqr_mass * 1e4) + r" $\cdot\,10^{-4}$")

            out = icaps.fit_model(icaps.fit_of_taylor2, dts[cidx], sigsqr[cidx],
                                  yerr=log_err if fit["fit_err"] else None,
                                  p0=[fit["p0"][0]], fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - np.log10(icaps.fit_of_taylor2(dts[cidx], *out.beta))) ** 2) / len(cidx)
            ax.plot(dt_space, icaps.fit_of_taylor2(dt_space, *out.beta),
                    c="green", lw=2, ls=":", zorder=20,
                    label="T2 {:.2f}".format(chisqr_mass * 1e4) + r" $\cdot\,10^{-4}$")

            out = icaps.fit_model(icaps.fit_of_taylor3, dts[cidx], sigsqr[cidx],
                                  yerr=sigsqr_err[cidx] if fit["fit_err"] else None,
                                  p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - np.log10(icaps.fit_of_taylor3(dts[cidx], *out.beta))) ** 2)
            ax.plot(dt_space, icaps.fit_of_taylor3(dt_space, *out.beta),
                    c="magenta", lw=2, ls=(0, (3, 5, 1, 5, 1, 5)), zorder=-20,
                    label="T3 {:.2f}".format(chisqr_mass * 1e4) + r" $\cdot\,10^{-4}$")

            out = icaps.fit_model(icaps.fit_of_taylor4, dts[cidx], sigsqr[cidx],
                                  yerr=sigsqr_err[cidx] if fit["fit_err"] else None,
                                  p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
            tau0 = abs(out.beta[1])
            mass = out.beta[0]
            mass_err = out.sd_beta[0]
            chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - np.log10(icaps.fit_of_taylor4(dts[cidx], *out.beta))) ** 2)
            label = "T4"
            label += r" {:.2f} $\cdot$ {}".format(chisqr_mass * 1e4, r"$10^{-4}$") + "\n"
            if mode == "trans":
                label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                                                                             mass_err * 10 ** units["mass_mag"],
                                                                             r"$10^{{{}}}$".format(
                                                                                 -units["mass_mag"]))
            else:
                label += r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                                                                                mass_err * 10 ** units["mass_mag"],
                                                                                r"$10^{{{}}}$".format(
                                                                                    -units["mass_mag"]))
            ax.plot(dt_space, icaps.fit_of_taylor4(dt_space, mass, tau0),
                    c="orange", lw=2, ls="--", zorder=20, label=label)

            out = icaps.fit_model(icaps.fit_of_taylor5, dts[cidx], sigsqr[cidx],
                                  yerr=sigsqr_err[cidx] if fit["fit_err"] else None,
                                  p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - np.log10(icaps.fit_of_taylor5(dts[cidx], *out.beta))) ** 2)
            ax.plot(dt_space, icaps.fit_of_taylor5(dt_space, *out.beta),
                    c="lime", lw=2, ls=(0, (3, 1, 1, 1, 1, 1)), zorder=-20,
                    label="T5 {:.2f}".format(chisqr_mass * 1e4) + r" $\cdot\,10^{-4}$")

            candidates = icaps.brownian.parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["bounds"],
                                                         adapt_margins=(0, 0), n_min=1, max_sigsqr=fit["max_sigsqr"],
                                                         max_err_ratio=fit["max_err_ratio"])
            # print(dts_raw[candidates[0]])
            cidx = candidates[0]
            log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None

            def fit_of_here(x0, x1):
                return icaps.fit_of_log(x0, mass, x1)

            out = icaps.fit_model(fit_of_here, dts[cidx], np.log10(sigsqr[cidx]),
                            p0=[fit["p0"][1]], fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err)
            tau = abs(out.beta[0])
            tau_err = out.sd_beta[0]
            chisqr_tau = np.sum((np.log10(sigsqr[cidx]) - fit_of_here(dts[cidx], *out.beta)) ** 2) / len(cidx)
            t_diff = np.max(dts[cidx]) - np.min(dts[cidx])

            # Investigate linearity of all data to check for "strangeness"
            out = icaps.fit_model(icaps.fit_slope2, np.log10(dts[cidx]), np.log10(sigsqr[cidx]),
                            fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_lin = np.sum((np.log10(sigsqr[cidx]) - icaps.fit_slope2(np.log10(dts[cidx]), *out.beta)) ** 2) / len(cidx)
            df_out.loc[idx_out, "chisqr_lin"] = chisqr_lin

            df_out.loc[idx_out, "tau0"] = tau0
            df_out.loc[idx_out, mass_column] = mass
            df_out.loc[idx_out, mass_err_column] = mass_err
            df_out.loc[idx_out, "tau"] = tau
            df_out.loc[idx_out, "tau_err"] = tau_err
            df_out.loc[idx_out, "chisqr_bal"] = chisqr_mass
            df_out.loc[idx_out, "chisqr"] = chisqr_tau
            df_out.loc[idx_out, "n_dots"] = len(cidx)
            df_out.loc[idx_out, "t_diff"] = t_diff
            if ret_erf:
                use = np.zeros(len(sigsqr), dtype=bool)
                use[cidx] = True
                df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                           (df_erf["axis"] == axis), "of_used"] = use
            # =====================
            # Plotting
            # =====================
            if plot["path"] is not None:
                if double_gauss:
                    sigma_type = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigsqr[sigma_type == "single"],
                                yerr=sigsqr_err[sigma_type == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "low double"], sigsqr[sigma_type == "low double"],
                                yerr=sigsqr_err[sigma_type == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "high double"], sigsqr[sigma_type == "high double"],
                                yerr=sigsqr_err[sigma_type == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigsqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigsqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigsqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                else:
                    ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx], sigsqr[cidx], yerr=sigsqr_err[cidx] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                # label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_mass * 1e4, r"$10^{-4}$") + "\n"
                # if mode == "trans":
                #     label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                #                                                                  mass_err * 10 ** units["mass_mag"],
                #                                                                  r"$10^{{{}}}$".format(
                #                                                                      -units["mass_mag"]))
                # else:
                #     label += r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                #                                                                     mass_err * 10 ** units["mass_mag"],
                #                                                                     r"$10^{{{}}}$".format(
                #                                                                         -units["mass_mag"]))
                # if fit["ballistic_fit"] == 0:
                #     ax.plot(dt_space, 10 ** icaps.fit_of_log(dt_space, mass, tau0),
                #             c="red", lw=2, ls="--", zorder=20, label=label)
                # elif fit["ballistic_fit"] == 2:
                #     ax.plot(dt_space, icaps.fit_of_taylor4(dt_space, mass, tau0),
                #             c="red", lw=2, ls="--", zorder=20, label=label)
                label = r"ALL OF (log) {:.2f} $\cdot$ {}".format(chisqr_tau * 1e4, r"$10^{-4}$") + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(tau * 1e3, tau_err * 1e3)
                ax.plot(dt_space, 10 ** icaps.fit_of_log(dt_space, mass, tau),
                        c="red", lw=2, ls="-", zorder=20, label=label)
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.legend(loc="lower right", framealpha=1, facecolor="white")
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot["path"][a] + "sigma_OF_{}_{}_{}.png".format(mode, axis, particle), dpi=plot["dpi"])
                plt.close()
            if not silent:
                pb.tick()
        if temp_path is not None:
            if p/len(particles) > n_temp * temp_interval:
                df_prelim = icaps.move_col(df_out, id_column, 0)
                df_prelim.to_csv(temp_path, index=False)
                n_temp += 1
    if temp_path is not None:
        os.remove(temp_path)
    if not silent:
        pb.finish()
    if (len(df_out[id_column].unique()) == 1) and (df_out[id_column].unique()[0] == 0):
        df_out.drop(columns=[id_column], inplace=True)
    if ret_erf:
        return df_out, df_erf
    else:
        return df_out


df_erf = pd.read_csv(csv_trans_erf)
plot = True
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
df, df_erf2 = test_ornstein_fuerth(
    df_erf, dts_trim, axes=["x"], mode="trans", particles=test_parts,
    temp_path=None, silent=False, ret_erf=True,
    plot={"path": plot_path + "of_test_trans_{}/" + "sigma_fitted/" if plot else None,
          "attempts": False,
          "error": True,
          "dpi": 600,
          "clear_out": True,
          "crop_to_max": True},
    fit={"p0": None,
         "maxit": None,
         "fit_type": 2,
         "fit_err": True,
         "bounds": (1, None),
         "ballistic_bounds": (1, 6),
         "ballistic_fit": 2,
         "max_sigsqr": 1e-6,
         "max_err_ratio": 1.,
         "min_tau": None})
df, df_erf2 = test_ornstein_fuerth(
    df_erf, dts_trim, axes=["x"], mode="trans", particles=test_parts,
    temp_path=None, silent=False, ret_erf=True,
    plot={"path": plot_path + "of_test_trans_{}/" + "sigma_diff/" if plot else None,
          "attempts": False,
          "error": True,
          "dpi": 600,
          "clear_out": True,
          "crop_to_max": True},
    fit={"p0": None,
         "maxit": None,
         "fit_type": 2,
         "fit_err": True,
         "bounds": (1, None),
         "ballistic_bounds": (1, 6),
         "ballistic_fit": 2,
         "max_sigsqr": 1e-6,
         "max_err_ratio": 1.,
         "min_tau": None}, sigma_diff=True)
# df.to_csv(csv_trans_of_sg, index=False)
# df_erf2.to_csv(csv_trans_erf_flagged, index=False)


