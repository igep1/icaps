import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT5/"
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
particle_rot_path = project_path + "particles_rot/"
plot_path = project_path + "plots/"
plot_dg_path = project_path + "plots_dg/"
thilo_path = project_path + "thilo/"
thilo_fname = "distribution_values_center_uncertainty_gen_{}.txt"
csv_phases_path = project_path + "Phases_Hand.csv"
csv_trans_path = project_path + "Tracks_PE1L100.csv"
csv_seeker_path = project_path + "Tracks_PE1L100A50S20L70.csv"
csv_candidates_path = project_path + "new_candidates_plus.txt"
csv_rot_full_path = project_path + "Tracks_PE1L100C.csv"
# focus_thresh = 13
# csv_rot_path = project_path + "Tracks_PE1L100CF{}L100.csv".format(focus_thresh)
# csv_rot_ellipse_path = project_path + "Tracks_PE1L100CF{}L100_Ellipse.csv".format(focus_thresh)
focus_filter_path = project_path + "focus_filter.csv"
csv_rot_path = project_path + "Tracks_PE1L100CF{}L100.csv".format("adapt")
csv_rot_ellipse_path = project_path + "Tracks_PE1L100CF{}L100_Ellipse.csv".format("adapt")
csv_disp_rot_path = project_path + "DISP_Rot.csv"
csv_erf_rot_path = project_path + "ERF_Rot.csv"
csv_of_rot_path = project_path + "OF_Rot.csv"
csv_disp_trans_path = project_path + "DISP_Trans.csv"
csv_erf_trans_path = project_path + "ERF_Trans.csv"
csv_erf_trans_2_path = project_path + "ERF_Trans_2.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
# chisqr_thresh = 2e-3
chisqr_thresh = 5e-3    # least squares
excluded_particles = (8781427,)
csv_usable_rot_path = project_path + "usable_rot_OF_particles.txt"
csv_of_chiex_path = project_path + "OF_Chi{}Ex.csv".format(chisqr_thresh)
# csv_of_ex_path = project_path + "OF_Ex.csv"
icaps.mk_folders([particle_rot_path, plot_path, plot_dg_path])

dts = np.arange(1, 26, 1)
dts = np.append(dts, np.arange(30, 51, 5))
# dts_dg = np.array([1, 2, 3, 4, 5])
dts_dg = dts


def main():
    # df = pd.read_csv(csv_phases_path)
    # df = df[df["phase"] == 0]
    # print(len(np.unique(df["frame"].to_numpy())))

    # ============================================================
    # HAND-PICKING PARTICLES FOR ROTATION
    # ============================================================
    """
    df = pd.read_csv(csv_track_path)
    df = icaps.filter_area(df, 50, tally=True, show_progress=True)
    df = icaps.filter_sharpness(df, 20, tally=True, show_progress=True)
    df = icaps.filter_track_length(df, 70, tally=True, show_progress=True)
    df.to_csv(csv_seeker_path, index=False)

    df_seeker = pd.read_csv(csv_seeker_path)
    particles = np.unique(df_seeker["particle"].to_numpy())
    df = pd.read_csv(csv_track_path)
    # icaps.mk_track_movies(df[df["particle"].isin(particles)], ffc_path, particle_rot_path, camera="LDM",
    #                       movie_path=movie_path, sub_folder="track", enlarge=4, show_progress=True, draw_bbox=True,
    #                       draw_label=False, clear_out=False, color=100)
    icaps.crop_df_particles(df[df["particle"].isin(particles)], ffc_path, particle_rot_path, enlarge=1, subfolder="ffc",
                            show_progress=True)
    """

    # ============================================================
    # CROPPING CANDIDATES
    # ============================================================
    """
    df = pd.read_csv(csv_trans_path)
    candidates = pd.read_csv(csv_candidates_path, header=None)[0].to_numpy()
    # particles = np.unique(df["particle"].to_numpy())
    # missing_candidates = candidates[~np.in1d(candidates, particles)]
    # print(missing_candidates)
    df_rot = df[df["particle"].isin(candidates)]
    df_rot.to_csv(csv_rot_full_path, index=False)
    # df_rot = pd.read_csv(csv_rot_full_path)
    # icaps.crop_df_particles(df_rot, ffc_path, particle_rot_path, enlarge=1,
    #                         subfolder="ffc", silent=False)
    """

    # ============================================================
    # MEASURE FOCUS AND COPY SHARP IMAGES
    # ============================================================
    # icaps.measure_foci_bulk(particle_rot_path, include_stats=True, detailed_progress=True)
    # icaps.filter_focus_bulk(particle_rot_path, thresh=focus_thresh, silent=False)

    # ============================================================
    # FILTER CANDIDATES AND COLLECT FOCUS
    # ============================================================
    # candidates = pd.read_csv(csv_candidates_path, header=None)[0].to_numpy()
    # df = pd.read_csv(csv_rot_full_path)
    # df = df[df["particle"].isin(candidates)]
    # print("candidates:\t", len(np.unique(df["particle"].to_numpy())))
    # df = icaps.collect_focus(df, particle_rot_path, silent=False)
    # # df.to_csv(csv_rot_full_path, index=False)
    #
    # df_focus_filter = pd.read_csv(focus_filter_path)
    # particles = df_focus_filter["particle"].unique()
    # df = df[df["particle"].isin(particles)]
    # # print(len(df))
    # idcs = []
    # for particle in particles:
    #     focus_thresh = df_focus_filter.loc[df_focus_filter["particle"] == particle, "min_focus"].to_numpy()[0]
    #     idcs.extend(df.index[(df["particle"] == particle) & (df["focus"] < focus_thresh)].to_list())
    # df.drop(idcs, axis=0, inplace=True)
    # df = icaps.filter_track_length(df, 100, silent=False)
    # # print(len(df))
    # print("F{}L100:\t".format("adapt"), len(np.unique(df["particle"].to_numpy())))
    # df.to_csv(csv_rot_path, index=False)

    # ============================================================
    # FIT ELLIPSES
    # ============================================================
    # df = pd.read_csv(csv_rot_path)
    # df = icaps.brownian.fit_ellipses_bulk(df, particle_rot_path, mark=True, mark_enlarge=4, silent=False,
    #                                       clear_out=True, thresh=20, mark_thresh=0, mark_thickness=2)
    # df.to_csv(csv_rot_ellipse_path, index=False)
    # """
    # Prepare 720240 Images
    # df = icaps.fit_ellipses_bulk(df[df["particle"] == 720240], particle_rot_path, mark=True, mark_enlarge=4,
    #                              silent=False, clear_out=True,
    #                              mark_colors=[(0, 255, 0), (0, 0, 255)],
    #                              thresh=20, mark_thresh=0, mark_thickness=3)
    # df_rot = pd.read_csv(csv_rot_full_path)
    # df_ = df_rot[df_rot["particle"] == 720240]
    # icaps.crop_df_particles(df_, ffc_path, particle_rot_path, enlarge=4,
    #                         subfolder="large", silent=False, threshold=20, draw_contours=True, thickness=3,
    #                         color=(0, 255, 0))
    # """
    # frame_margins = (58247, 61116)
    # icaps.mk_movie(particle_rot_path + "{}/large/{}_({}:{}).bmp".format(720240, 720240, *frame_margins),
    #                particle_rot_path + "{}/movie.mp4".format(720240),
    #                fps=60)

    # ============================================================
    # CALCULATE ROT DISPLACEMENT
    # ============================================================
    # df = pd.read_csv(csv_rot_ellipse_path)
    # df = icaps.brownian.stitch_displacements(df, dts, in_columns=["ellipse_angle"], out_columns="omega",
    #                                          silent=False, abs_max=np.deg2rad(90), convert=np.pi/180, add_leaps=True)
    # df.to_csv(csv_disp_rot_path, index=False)

    # ============================================================
    # PLOT ROT DISPLACEMENT
    # ============================================================
    # df = pd.read_csv(csv_disp_rot_path)
    # icaps.brownian.plot_stitched_displacements(df, dts, "ellipse_angle", "omega", silent=False, sub_folder=None,
    #                                            plot_path=plot_path + "disp_rot/", clear_out=False,
    #                                            pos_convert=np.pi/180, pos_unit="rad", pos_name=r"$\theta$",
    #                                            disp_name=r"$\Delta\theta$", leap_column="leap_{}", dpi=600)

    # ============================================================
    # CALCULATE ROT ERF FITS
    # ============================================================
    # df = pd.read_csv(csv_disp_rot_path)
    # plot = True
    # df = icaps.brownian.calc_erf(df, dts, ["rot"], disp_columns="omega", silent=False, fit_type=2, n_min=10,
    #                              plot_path=plot_path + "erf_rot/" if plot else None, sub_folder=None,
    #                              clear_out=True, dpi=300, disp_names=r"$\Delta\theta$", disp_convert=1,
    #                              disp_convert_plot=1, disp_unit="rad", erf_convert=1, max_sigma=None, p0=[0.0, 1.0],
    #                              double_gauss=True, dg_func=icaps.fit_double_erf_center
    #                              )
    # df.to_csv(csv_erf_rot_path, index=False)

    # ============================================================
    # CALC & PLOT ROT OF FITS
    # ============================================================
    # df_erf = pd.read_csv(csv_erf_rot_path)
    # plot = True
    # df = icaps.brownian.calc_ornstein_fuerth(
    #     df_erf, dts, axes=["rot"], plot_path=plot_path + "of_rot_{}/" if plot else None,
    #     mass_column="inertia", fit_error=True,
    #     fit_bounds=(None, None), adapt_margins=(5, 20), grade_weights=(1., 1.), plot_attempts=False, n_min=10,
    #     quality_type="all", quantity_type="count", max_sigma_sqr=2, max_err_ratio=1, silent=False, plot_error=True,
    #     sigma_unit="rad", mode="rot", dpi=600, fit_type=2, clear_out=True, grade_type="gaussian",
    #     prelim_save=project_path + "OF_Rot_prelim.csv", crop_to_max=False, double_gauss="none", dg_as_error=False,
    #     ret_erf=False, min_tau=4.83e-3
    # )
    # df.to_csv(csv_of_rot_path, index=False)

    # ============================================================
    # CALCULATE TRANS DISPLACEMENT
    # ============================================================
    # df = pd.read_csv(csv_trans_path)
    # df = icaps.brownian.stitch_displacements(df, dts, silent=False)
    # df.to_csv(csv_disp_trans_path, index=False)

    # ============================================================
    # PLOT TRANS DISPLACEMENT
    # ============================================================
    # df = pd.read_csv(csv_disp_trans_path)
    # icaps.brownian.plot_stitched_displacements(df, dts, "x", "dx", silent=False, sub_folder=None,
    #                                            plot_path=plot_path + "disp_trans_x/", clear_out=False)
    # icaps.brownian.plot_stitched_displacements(df, dts, "y", "dy", silent=False, sub_folder=None,
    #                                            plot_path=plot_path + "disp_trans_y/", clear_out=False)

    # ============================================================
    # CALCULATE TRANS ERF FITS
    # ============================================================
    # df = pd.read_csv(csv_disp_trans_path)
    # plot = False
    # df = icaps.brownian.calc_erf(df, dts, ["x", "y"], silent=False, fit_type=2, n_min=20,
    #                              plot_path=plot_path + "erf_trans/" if plot else None, sub_folder=None,
    #                              clear_out=True, dpi=200, p0=[0.0, 1.0], disp_names=r"$\Delta {}$",
    #                              double_gauss=True, fickian=False, plot_fails=False)
    # df.to_csv(csv_erf_trans_path, index=False)

    # ============================================================
    # PLOT TRANS ERF FITS (or plot above)
    # ============================================================
    # df_disp = pd.read_csv(csv_disp_trans_path)
    # df_erf = pd.read_csv(csv_erf_trans_path)
    # icaps.brownian.plot_erfs(df_disp, df_erf, ["x", "y"], dts, silent=False, plot_path=plot_path + "erf_trans/",
    #                          sub_folder=None, clear_out=False, dpi=600)

    # ============================================================
    # CALC & PLOT TRANS OF FITS
    # ============================================================
    # df_erf = pd.read_csv(csv_erf_trans_path)
    # plot = True
    # # dts = np.arange(1, 26, 1)
    # dts_trim = np.arange(1, 11, 1)
    # dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
    # dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
    # dts = dts_trim
    # df, df_erf2 = icaps.brownian.calc_ornstein_fuerth(
    #     df_erf, dts, axes=["x", "y"], mode="trans",
    #     temp_path=project_path + "OF_Trans_prelim.csv", silent=False, ret_erf=True,
    #     plot={"path": plot_path + "of_trans_{}/" if plot else None,
    #           "attempts": False,
    #           "error": True,
    #           "dpi": 600,
    #           "clear_out": True,
    #           "crop_to_max": True},
    #     fit={"p0": None,
    #          "maxit": None,
    #          "fit_type": 2,
    #          "fit_err": True,
    #          "bounds": (1, None),
    #          "ballistic_bounds": (1, 6),
    #          "ballistic_fit": 2,
    #          "max_sigsqr": 1e-6,
    #          "max_err_ratio": 1.,
    #          "min_tau": None},
    #     dg={"apply": False,
    #         "max_dt": 5,
    #         "min_chi_ratio": 3})
    # df.to_csv(csv_of_trans_path, index=False)
    # df_erf2.to_csv(project_path + "ERF_Trans_2.csv", index=False)

    # # ============================================================
    # # FILTERING AND COMPILING DATA
    # # ============================================================
    # collect = True
    # if collect:
    #     df_trans = pd.read_csv(csv_of_trans_path)
    #     df_rot = pd.read_csv(csv_of_rot_path)
    #     particles_rot = pd.read_csv(csv_usable_rot_path, header=None)[0].to_numpy()
    #     df_rot = df_rot[df_rot["particle"].isin(particles_rot)]
    #     df_rot = df_rot.drop(columns=["axis"])
    #     df_rot = df_rot.rename({"tau": "tau_rot",
    #                             "tau_err": "tau_rot_err",
    #                             "chisqr": "chisqr_rot",
    #                             "n_dots": "n_dots_rot",
    #                             "t_diff": "t_diff_rot"}, axis=1)
    #     df_trans = df_trans[df_trans["chisqr"] <= chisqr_thresh]
    #     df_trans["n_dots"] = df_trans["n_dots"].astype(np.float64)
    #     particles_trans = np.unique(df_trans["particle"].to_numpy())
    #     particles = np.unique(np.append(particles_trans, particles_rot))
    #
    #     axes = np.unique(df_trans["axis"].to_numpy())
    #     cols = list(df_trans.columns)[2:]
    #     df_clean = None
    #     for axis in axes:
    #         df_ = df_trans[df_trans["axis"] == axis]
    #         df_ = df_.drop("axis", axis=1)
    #         df_ = df_.rename(
    #             columns={col: "{}_{}".format(col, axis) if "_err" not in col else "{}_{}_err".format(col[:-4], axis) for
    #                      col in cols})
    #         if df_clean is None:
    #             df_clean = df_
    #         else:
    #             df_clean = pd.merge(df_clean, df_, how="left", on="particle")
    #     df_trans = df_clean
    #
    #     diff_particles = particles_rot[~np.isin(particles_rot, particles_trans)]
    #     df_trans = df_trans.append(pd.DataFrame(data={"particle": diff_particles}), ignore_index=True, sort=False)
    #
    #     df_track = pd.read_csv(csv_trans_path)
    #     df_track = df_track[df_track["particle"].isin(particles)]
    #     nan_arr = np.zeros(len(particles))
    #     nan_arr[:] = np.nan
    #     df_ex = pd.DataFrame(data={
    #         "particle": particles,
    #         "start_frame": np.zeros(len(particles), dtype=int),
    #         "diff_frames": np.zeros(len(particles), dtype=int),
    #         "n_frames": np.zeros(len(particles), dtype=int),
    #         "x_start": nan_arr,
    #         "x_stop": nan_arr,
    #         "y_start": nan_arr,
    #         "y_stop": nan_arr,
    #         "mean_area": nan_arr,
    #         "mean_area_err": nan_arr,
    #         "mean_ex": nan_arr,
    #         "mean_ex_err": nan_arr,
    #         "mean_gyrad": nan_arr,
    #         "mean_gyrad_err": nan_arr
    #     })
    #     pb = icaps.ProgressBar(len(particles), "Collecting Ex Data")
    #     for particle in particles:
    #         idx = df_ex.index[df_ex["particle"] == particle]
    #         df_t = df_track[df_track["particle"] == particle]
    #         frames = df_t["frame"].to_numpy()
    #         area = df_t["area"].to_numpy()
    #         extinction = df_t["mass"].to_numpy()
    #         gyrad = df_t["gyrad"].to_numpy()
    #         df_ex.loc[idx, "start_frame"] = frames[0]
    #         df_ex.loc[idx, "diff_frames"] = frames[-1] - frames[0]
    #         df_ex.loc[idx, "n_frames"] = frames.shape[0]
    #         df_ex.loc[idx, "mean_area"] = np.mean(area)
    #         df_ex.loc[idx, "mean_area_err"] = np.std(area)
    #         df_ex.loc[idx, "mean_ex"] = np.mean(extinction)
    #         df_ex.loc[idx, "mean_ex_err"] = np.std(extinction)
    #         df_ex.loc[idx, "mean_gyrad"] = np.mean(gyrad)
    #         df_ex.loc[idx, "mean_gyrad_err"] = np.std(gyrad)
    #         for axis in axes:
    #             idx = df_ex.index[df_ex["particle"] == particle]
    #             pos = df_t[axis].to_numpy()
    #             df_ex.loc[idx, "{}_start".format(axis)] = pos[0]
    #             df_ex.loc[idx, "{}_stop".format(axis)] = pos[-1]
    #         pb.tick()
    #     pb.finish()
    #
    #     df = pd.merge(df_trans, df_ex, how="left", on="particle")
    #     df = pd.merge(df, df_rot, how="left", on="particle")
    #     df = icaps.move_col(df, "mass_y", "tau_x")
    #     df = icaps.move_col(df, "mass_y_err", "tau_x")
    #     df = icaps.move_col(df, "tau_y", "chisqr_x")
    #     df = icaps.move_col(df, "tau_y_err", "chisqr_x")
    #     df = icaps.move_col(df, "chisqr_y", "n_dots_x")
    #     df = icaps.move_col(df, "n_dots_y", "t_diff_x")
    #     # df.to_csv(csv_of_ex_path, index=False)
    #     df.to_csv(csv_of_chiex_path, index=False)

    # """
    # ============================================================
    # FILTERING AND COMPILING DATA (OLD)
    # ============================================================
    collect = False
    if collect:
        df_trans = pd.read_csv(csv_of_trans_path)
        df_rot = pd.read_csv(csv_of_rot_path)
        particles_rot = pd.read_csv(csv_usable_rot_path, header=None)[0].to_numpy()
        df_rot = df_rot[df_rot["particle"].isin(particles_rot)]
        df_rot = df_rot.drop(columns=["axis"])
        df_rot = df_rot.rename({"tau": "tau_rot",
                                "tau_err": "tau_rot_err",
                                "chisqr": "chisqr_rot",
                                "n_dots": "n_dots_rot",
                                "t_diff": "t_diff_rot"}, axis=1)
        df_trans = df_trans[df_trans["chisqr"] <= chisqr_thresh]
        df_trans["n_dots"] = df_trans["n_dots"].astype(np.float64)
        particles_trans = np.unique(df_trans["particle"].to_numpy())
        particles = np.unique(np.append(particles_trans, particles_rot))

        axes = np.unique(df_trans["axis"].to_numpy())
        cols = list(df_trans.columns)[2:]
        df_clean = None
        for axis in axes:
            df_ = df_trans[df_trans["axis"] == axis]
            df_ = df_.drop("axis", axis=1)
            df_ = df_.rename(columns={col: "{}_{}".format(col, axis) if "_err" not in col else "{}_{}_err".format(col[:-4], axis) for col in cols})
            if df_clean is None:
                df_clean = df_
            else:
                df_clean = pd.merge(df_clean, df_, how="left", on="particle")
        df_trans = df_clean

        diff_particles = particles_rot[~np.isin(particles_rot, particles_trans)]
        df_trans = df_trans.append(pd.DataFrame(data={"particle": diff_particles}), ignore_index=True, sort=False)

        df_track = pd.read_csv(csv_trans_path)
        df_track = df_track[df_track["particle"].isin(particles)]
        nan_arr = np.zeros(len(particles))
        nan_arr[:] = np.nan
        df_ex = pd.DataFrame(data={
            "particle": particles,
            "start_frame": np.zeros(len(particles), dtype=int),
            "diff_frames": np.zeros(len(particles), dtype=int),
            "n_frames": np.zeros(len(particles), dtype=int),
            "x_start": nan_arr,
            "x_stop": nan_arr,
            "y_start": nan_arr,
            "y_stop": nan_arr,
            "mean_area": nan_arr,
            "mean_area_err": nan_arr,
            "mean_ex": nan_arr,
            "mean_ex_err": nan_arr,
            "mean_gyrad": nan_arr,
            "mean_gyrad_err": nan_arr
        })
        pb = icaps.ProgressBar(len(particles), "Collecting Ex Data")
        for particle in particles:
            idx = df_ex.index[df_ex["particle"] == particle]
            df_t = df_track[df_track["particle"] == particle]
            frames = df_t["frame"].to_numpy()
            area = df_t["area"].to_numpy()
            extinction = df_t["mass"].to_numpy()
            gyrad = df_t["gyrad"].to_numpy()
            df_ex.loc[idx, "start_frame"] = frames[0]
            df_ex.loc[idx, "diff_frames"] = frames[-1] - frames[0]
            df_ex.loc[idx, "n_frames"] = frames.shape[0]
            df_ex.loc[idx, "mean_area"] = np.mean(area)
            df_ex.loc[idx, "mean_area_err"] = np.std(area)
            df_ex.loc[idx, "mean_ex"] = np.mean(extinction)
            df_ex.loc[idx, "mean_ex_err"] = np.std(extinction)
            df_ex.loc[idx, "mean_gyrad"] = np.mean(gyrad)
            df_ex.loc[idx, "mean_gyrad_err"] = np.std(gyrad)
            for axis in axes:
                idx = df_ex.index[df_ex["particle"] == particle]
                pos = df_t[axis].to_numpy()
                df_ex.loc[idx, "{}_start".format(axis)] = pos[0]
                df_ex.loc[idx, "{}_stop".format(axis)] = pos[-1]
            pb.tick()
        pb.finish()

        df = pd.merge(df_trans, df_ex, how="left", on="particle")
        df = pd.merge(df, df_rot, how="left", on="particle")
        df = icaps.move_col(df, "mass_y", "tau_x")
        df = icaps.move_col(df, "mass_y_err", "tau_x")
        df = icaps.move_col(df, "tau_y", "chisqr_x")
        df = icaps.move_col(df, "tau_y_err", "chisqr_x")
        df = icaps.move_col(df, "chisqr_y", "n_dots_x")
        df = icaps.move_col(df, "n_dots_y", "t_diff_x")
        # df.to_csv(csv_of_ex_path, index=False)
        df.to_csv(csv_of_chiex_path, index=False)
    # """

    # ============================================================
    # COPY BAD OF ROT PLOTS
    # ============================================================
    # usable = pd.read_csv(csv_usable_rot_path, header=None)[0].to_numpy()
    # p_in = plot_path + "of_rot_angle/"
    # p_out = plot_path + "of_rot_bad/"
    # icaps.mk_folder(p_out, clear=True)
    # files = icaps.get_files(p_in)
    # for file in files:
    #     particle = icaps.get_frame(file, prefix="sigma_OF_rot_angle_")
    #     if particle not in usable:
    #         shutil.copyfile(p_in + file, p_out + file)

    # ============================================================
    # ROT DOUBLE GAUSS CORRECTION
    # ============================================================
    csv_erf_rot_dg_path = project_path + "ERF_Rot_DG.csv"
    # filter_type = "or"
    # w_thresh = 0.9
    # dg_min_chi_factor = 3
    # max_min_thresh = 2
    # # sig_cap = 9e-5
    # sig_cap = None
    # mass_thresh = -0.8
    # df_erf = pd.read_csv(csv_erf_rot_path)
    # df = pd.read_csv(csv_of_chiex_path)
    # df = df.dropna(subset=["mass_x", "mass_y"])
    # df = df.dropna(subset=["inertia"])
    # df = df[~df["particle"].isin(excluded_particles)]
    # df_mass = df[np.log10(df["mass_x_err"] / df["mass_x"]) <= mass_thresh]
    # df_mass = df_mass[np.log10(df_mass["mass_y_err"] / df_mass["mass_y"]) <= mass_thresh]
    # particles = df_mass["particle"].unique()
    # df_erf_dg = df_erf[["particle", "dt", "axis", "sigma", "sigma_err", "chisqr"]].copy()
    # df_erf_dg = df_erf_dg[df_erf_dg["particle"].isin(particles)]
    # df_erf_dg["sigma_type"] = "single"
    # start_frame = np.zeros(len(particles), dtype=int)
    # for i, particle in enumerate(particles):
    #     start_frame[i] = df_mass.loc[df_mass["particle"] == particle, "start_frame"].to_numpy()[0]
    # particles1 = particles[start_frame < icaps.const.end_ff]
    # particles2 = particles[start_frame >= icaps.const.end_ff]
    # particles = [particles1, particles2]
    # axes = ["rot"]
    # pb = icaps.ProgressBar(len(particles) * len(axes) * len(dts_dg), "Rot Double Gauss Correction")
    # for inject, parts in enumerate(particles):
    #     df_mass_ = df_mass[df_mass["particle"].isin(parts)]
    #     df_erf_inj = df_erf[df_erf["particle"].isin(parts)]
    #     icaps.mk_folder(plot_dg_path + "sigma_mass_rot_i{}/".format(inject + 1), clear=True)
    #     for axis in axes:
    #         for dt in dts_dg:
    #             df_erf_ = df_erf_inj[df_erf_inj["axis"] == axis]
    #             df_erf_ = df_erf_[df_erf_["dt"] == dt]
    #             sig0 = np.squeeze(df_erf_["sigma0"].to_numpy())
    #             sig1 = np.squeeze(df_erf_["sigma1"].to_numpy())
    #             sig = np.squeeze(df_erf_["sigma"].to_numpy())
    #             chisqr_erf = np.squeeze(df_erf_["chisqr"].to_numpy())
    #             chisqr_erf_dg = np.squeeze(df_erf_["chisqr_dg"].to_numpy())
    #             w0 = np.squeeze(df_erf_["w0"].to_numpy())
    #             w1 = 1. - w0
    #             sig0[sig0 <= 0] = sig[sig0 <= 0]
    #             sig1[sig1 <= 0] = sig[sig1 <= 0]
    #             sigmax = np.amax([sig0, sig1], axis=0)
    #             sigmin = np.amin([sig0, sig1], axis=0)
    #             wmax = np.array([w0[i] if w0[i] > w1[i] else w1[i] for i in range(len(w0))])
    #             mean_ex = df_mass_["mean_ex"].to_numpy()
    #
    #             _, ax = plt.subplots()
    #             ax.set_xlabel("Intensity")
    #             ax.set_ylabel(r"$\sigma_{rot}$ (rad)")
    #             ax.set_yscale("log")
    #             ax.set_xscale("log")
    #             ax.set_xlim(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex))
    #             # ax.set_ylim(9e-9, 9e-5)
    #             ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))
    #
    #             f1 = (sigmax / sigmin < max_min_thresh)
    #             f2 = (chisqr_erf / chisqr_erf_dg < dg_min_chi_factor)
    #             f3 = (wmax >= w_thresh)
    #             fsum = None
    #             if filter_type == "or":
    #                 fsum = f1 | f2 | f3
    #             elif filter_type == "and":
    #                 fsum = f1 & f2 & f3
    #             else:
    #                 print("Bad Filter Type!")
    #
    #             parts_ = parts.copy()
    #             mean_ex_ = mean_ex.copy()
    #             if sig_cap is not None:
    #                 fsum = fsum[sig < sig_cap]
    #                 sigmax = sigmax[sig < sig_cap]
    #                 sigmin = sigmin[sig < sig_cap]
    #                 mean_ex_ = mean_ex_[sig < sig_cap]
    #                 parts_ = parts_[sig < sig_cap]
    #                 sig = sig[sig < sig_cap]
    #
    #             sigmax_ = sigmax[~fsum]
    #             sigmin_ = sigmin[~fsum]
    #             sig_ = sig[fsum]
    #             ax.scatter(mean_ex_[~fsum], sigmax_, marker="^", facecolors="none", edgecolors="blue",
    #                        # label=r"Major Double Gauss",
    #                        s=5, alpha=0.3)
    #             ax.scatter(mean_ex_[~fsum], sigmin_, marker="v", facecolors="none", edgecolors="red",
    #                        # label=r"Minor Double Gauss",
    #                        s=5, alpha=0.3)
    #             ax.scatter(mean_ex_[fsum], sig_, marker="x", facecolors="black", edgecolors="none",
    #                        label=r"Single Gauss",
    #                        s=5, alpha=1.)
    #             ax.legend()
    #             out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex_[fsum]), np.log10(sig[fsum]), p0=[-0.5, -3],
    #                                   fit_type=2, maxit=10000)
    #             xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    #             ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="black", ls="-", lw=2,
    #                     label="Single Gauss Fit\nslope = {:.4f} \u00b1 {:.4f}".format(out.beta[0], out.sd_beta[0]))
    #             distmin = np.abs(np.log10(sigmin_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
    #             distmax = np.abs(np.log10(sigmax_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
    #             sigmin_use = sigmin_[distmin <= distmax]
    #             sigmax_use = sigmax_[distmin > distmax]
    #             ax.scatter(mean_ex_[~fsum][distmin > distmax], sigmax_use, marker="^", c="blue",
    #                        label=r"Major Double Gauss", s=5, alpha=1.)
    #             ax.scatter(mean_ex_[~fsum][distmin <= distmax], sigmin_use, marker="v", c="red",
    #                        label=r"Minor Double Gauss", s=5, alpha=1.)
    #
    #             x_full = np.hstack(
    #                 [mean_ex_[fsum], mean_ex_[~fsum][distmin <= distmax],
    #                  mean_ex_[~fsum][distmin > distmax]]).ravel()
    #             y_full = np.hstack([sig_, sigmin_use, sigmax_use]).ravel()
    #             out = icaps.fit_model(icaps.fit_lin, x=np.log10(x_full), y=np.log10(y_full), p0=[-0.5, -5],
    #                                   fit_type=2)
    #             ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="magenta", ls="--", lw=2,
    #                     label="Single and Double Gauss Fit\nslope = {:.4f} \u00b1 {:.4f}".format(out.beta[0],
    #                                                                                              out.sd_beta[0]))
    #             ax.legend()
    #             plt.savefig(plot_dg_path + "sigma_mass_rot_i{}/".format(inject + 1) +
    #                         "sigma_mass_rot_i{}_{}_{}_{}_{}_{}.png".format(inject + 1, axis, dt, w_thresh,
    #                                                                        dg_min_chi_factor, max_min_thresh),
    #                         dpi=600)
    #             plt.close()
    #             for i, part in enumerate(parts_[fsum]):
    #                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
    #                               (df_erf_dg["dt"] == dt), "sigma"] = sig_[i]
    #             for i, part in enumerate(parts_[~fsum][distmin <= distmax]):
    #                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
    #                               (df_erf_dg["dt"] == dt), "sigma"] = sigmin_use[i]
    #                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
    #                               (df_erf_dg["dt"] == dt), "sigma_type"] = "low double"
    #             for i, part in enumerate(parts_[~fsum][distmin > distmax]):
    #                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
    #                               (df_erf_dg["dt"] == dt), "sigma"] = sigmax_use[i]
    #                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
    #                               (df_erf_dg["dt"] == dt), "sigma_type"] = "high double"
    #             pb.tick()
    # pb.finish()
    # df_erf_dg.to_csv(csv_erf_rot_dg_path, index=False)

    # ============================================================
    # TRANS DOUBLE GAUSS CORRECTION
    # ============================================================
    csv_erf_trans_dg_path = project_path + "ERF_Trans_DG.csv"
    csv_erf_trans_noise_path = project_path + "ERF_Trans_Noise.csv"
    """
    filter_type = "or"
    w_thresh = 0.9
    dg_min_chi_factor = 3
    max_min_thresh = 2
    # sig_cap_max = 9e-5
    # sig_cap_min = 1e-7
    sig_cap_max = 10000000
    sig_cap_min = 0

    max_ratio = .2
    plot_xerr = False
    dg_of_used = True
    df_erf = pd.read_csv(csv_erf_trans_2_path)
    df = pd.read_csv(csv_of_chiex_path)
    # df = pd.read_csv(csv_of_ex_path)
    print(len(df["particle"].unique()))
    df = df.dropna(subset=["mass_x", "mass_y"])
    print(len(df["particle"].unique()))
    df = df[~df["particle"].isin(excluded_particles)]
    print(len(df["particle"].unique()))
    df_mass = df[df["mass_x_err"] / df["mass_x"] <= max_ratio]
    df_mass = df_mass[df_mass["mass_y_err"] / df_mass["mass_y"] <= max_ratio]
    df_mass = df_mass[df_mass["tau_x_err"] / df_mass["tau_x"] <= max_ratio]
    df_mass = df_mass[df_mass["tau_y_err"] / df_mass["tau_y"] <= max_ratio]
    print(len(df_mass["particle"].unique()))
    particles = df_mass["particle"].unique()
    df_erf_dg = df_erf[["particle", "dt", "axis", "sigma", "sigma_err", "chisqr", "of_used"]].copy()
    df_erf_dg = df_erf_dg[df_erf_dg["particle"].isin(particles)]
    df_erf_dg["sigma_type"] = "single"
    df_noise = None
    start_frame = np.zeros(len(particles), dtype=int)
    for i, particle in enumerate(particles):
        start_frame[i] = df_mass.loc[df_mass["particle"] == particle, "start_frame"].to_numpy()[0]
    particles1 = particles[start_frame < icaps.const.end_ff]
    print("i1", len(particles1))
    particles2 = particles[start_frame >= icaps.const.end_ff]
    print("i2", len(particles2))
    particles = np.array([particles1, particles2], dtype=object)
    axes = ["x", "y"]
    pb = icaps.ProgressBar(len(particles) * len(axes) * len(dts_dg), "Trans Double Gauss Correction")
    extra_plots = [plt.subplots() for i in range(len(dts))]
    # icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_extra/", clear=True)
    icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_extra/", clear=False)
    for i, xax in enumerate(extra_plots):
        xax[1].set_xlabel(r"$\langle E \rangle$ (arb. units)")
        xax[1].set_ylabel(r"$\sigma_{\rm trans}$ (m)")
        xax[1].set_yscale("log")
        xax[1].set_xscale("log")
        x_mean_ex = df_mass["mean_ex"].to_numpy()
        xax[1].set_xlim(0.8 * np.min(x_mean_ex), 1.2 * np.max(x_mean_ex))
        xax[1].set_ylim(9e-9, 9e-5)
        xax[1].set_title(r"$\Delta t$" + " = {} ms".format(dts[i]))
    for inject, parts in enumerate(particles):
        df_mass_ = df_mass[df_mass["particle"].isin(parts)]
        mean_ex = df_mass_["mean_ex"].to_numpy()
        mean_ex_err = df_mass_["mean_ex_err"].to_numpy()
        df_erf_inj = df_erf[df_erf["particle"].isin(parts)]
        # icaps.mk_folder(plot_dg_path + "sigma_mass_trans_i{}/".format(inject+1), clear=True)
        # icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_i{}/".format(inject + 1), clear=True)
        icaps.mk_folder(plot_dg_path + "sigma_mass_trans_i{}/".format(inject+1), clear=True)
        icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_i{}/".format(inject + 1), clear=True)
        for axis in axes:
            for idt, dt in enumerate(dts_dg):
                df_erf_ = df_erf_inj[df_erf_inj["axis"] == axis]
                df_erf_ = df_erf_[df_erf_["dt"] == dt]
                parts_ = parts[~np.isnan(df_erf_["sigma"].to_numpy())]
                mean_ex_ = mean_ex[~np.isnan(df_erf_["sigma"].to_numpy())]
                mean_ex_err_ = mean_ex_err[~np.isnan(df_erf_["sigma"].to_numpy())]
                df_erf_ = df_erf_[~np.isnan(df_erf_["sigma"])]
                of_used = np.squeeze(df_erf_["of_used"].to_numpy())
                sig0 = np.squeeze(df_erf_["sigma0"].to_numpy())
                sig1 = np.squeeze(df_erf_["sigma1"].to_numpy())
                sig = np.squeeze(df_erf_["sigma"].to_numpy())
                sig_err = np.squeeze(df_erf_["sigma_err"].to_numpy())
                sig0_err = np.squeeze(df_erf_["sigma0_err"].to_numpy())
                sig1_err = np.squeeze(df_erf_["sigma1_err"].to_numpy())
                chisqr_erf = np.squeeze(df_erf_["chisqr"].to_numpy())
                chisqr_erf_dg = np.squeeze(df_erf_["chisqr_dg"].to_numpy())
                w0 = np.squeeze(df_erf_["w0"].to_numpy())
                w1 = 1. - w0
                sig0_err[sig0 <= 0] = sig_err[sig0 <= 0]
                sig1_err[sig1 <= 0] = sig_err[sig1 <= 0]
                sig0[sig0 <= 0] = sig[sig0 <= 0]
                sig1[sig1 <= 0] = sig[sig1 <= 0]
                sigmax = np.amax([sig0, sig1], axis=0)
                sigmin = np.amin([sig0, sig1], axis=0)
                sigmax_err = np.array([sig0_err[i] if sig0[i] > sig1[i] else sig1_err[i] for i in range(len(sig0))])
                sigmin_err = np.array([sig0_err[i] if sig0[i] <= sig1[i] else sig1_err[i] for i in range(len(sig0))])
                wmax = np.array([w0[i] if w0[i] > w1[i] else w1[i] for i in range(len(w0))])

                fig, ax = plt.subplots()
                ax.set_xlabel(r"$\langle E \rangle$ (arb. units)")
                ax.set_ylabel(r"$\sigma_{\rm trans}$ (m)")
                ax.set_yscale("log")
                ax.set_xscale("log")
                ax.set_xlim(0.8 * np.min(mean_ex_), 1.2 * np.max(mean_ex_))
                ax.set_ylim(9e-9, 9e-5)
                ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))

                f1 = (sigmax / sigmin < max_min_thresh)
                f2 = (chisqr_erf / chisqr_erf_dg < dg_min_chi_factor)
                f3 = (wmax >= w_thresh)
                fsum = None
                if filter_type == "or":
                    fsum = f1 | f2 | f3
                elif filter_type == "and":
                    fsum = f1 & f2 & f3
                else:
                    print("Bad Filter Type!")
                if dg_of_used:
                    fsum = fsum | of_used

                parts_ = parts_[sig < sig_cap_max].copy()
                fsum = fsum[sig < sig_cap_max]
                sigmax = sigmax[sig < sig_cap_max]
                sigmin = sigmin[sig < sig_cap_max]
                sigmax_err = sigmax_err[sig < sig_cap_max]
                sigmin_err = sigmin_err[sig < sig_cap_max]
                mean_ex_ = mean_ex_[sig < sig_cap_max]
                mean_ex_err_ = mean_ex_err_[sig < sig_cap_max]
                sig_err = sig_err[sig < sig_cap_max]
                sig = sig[sig < sig_cap_max]

                parts_ = parts_[sig >= sig_cap_min].copy()
                fsum = fsum[sig >= sig_cap_min]
                sigmax = sigmax[sig >= sig_cap_min]
                sigmin = sigmin[sig >= sig_cap_min]
                sigmax_err = sigmax_err[sig >= sig_cap_min]
                sigmin_err = sigmin_err[sig >= sig_cap_min]
                mean_ex_ = mean_ex_[sig >= sig_cap_min]
                mean_ex_err_ = mean_ex_err_[sig >= sig_cap_min]
                sig_err = sig_err[sig >= sig_cap_min]
                sig = sig[sig >= sig_cap_min]

                sigmax_ = sigmax[~fsum]
                sigmin_ = sigmin[~fsum]
                sigmax_err_ = sigmax_err[~fsum]
                sigmin_err_ = sigmin_err[~fsum]
                sig_ = sig[fsum]
                sig_err_ = sig_err[fsum]
                ax.errorbar(mean_ex_[~fsum], sigmax_,
                            xerr=mean_ex_err_[~fsum] if plot_xerr else None, yerr=sigmax_err_,
                            fmt="^", markerfacecolor="none", markeredgecolor="red", markersize=5, alpha=0.3)
                ax.errorbar(mean_ex_[~fsum], sigmin_,
                            xerr=mean_ex_err_[~fsum] if plot_xerr else None, yerr=sigmin_err_,
                            fmt="v", markerfacecolor="none", markeredgecolor="blue", markersize=5, alpha=0.3)
                ax.errorbar(mean_ex_[fsum], sig_,
                            xerr=mean_ex_err_[fsum] if plot_xerr else None, yerr=sig_err_,
                            fmt="x", color="black", label=r"Single Gauss", markersize=5, alpha=1.)
                print("single", len(mean_ex_[fsum]))
                print("double", len(mean_ex_[~fsum]))
                out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex_[fsum]), np.log10(sig_),
                                      xerr=(mean_ex_err_[fsum] / mean_ex_[fsum]) / np.log(10),
                                      yerr=(sig_err_ / sig_) / np.log(10),
                                      p0=[-0.5, -3], fit_type=2, maxit=10000)
                xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
                ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="black", ls="-", lw=2,
                        label="Single Gauss Fit\nSlope = {:.2f} \u00b1 {:.2f}".format(out.beta[0], out.sd_beta[0]))
                distmin = np.abs(np.log10(sigmin_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
                distmax = np.abs(np.log10(sigmax_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
                sigmin_use = sigmin_[distmin <= distmax]
                sigmax_use = sigmax_[distmin > distmax]
                sigmin_use_err = sigmin_err_[distmin <= distmax]
                sigmax_use_err = sigmax_err_[distmin > distmax]
                ax.errorbar(mean_ex_[~fsum][distmin > distmax], sigmax_use,
                            xerr=mean_ex_err_[~fsum][distmin > distmax] if plot_xerr else None, yerr=sigmax_use_err,
                            fmt="^", c="red", label=r"Major Double Gauss", markersize=5, alpha=1.)
                ax.errorbar(mean_ex_[~fsum][distmin <= distmax], sigmin_use,
                            xerr=mean_ex_err_[~fsum][distmin <= distmax] if plot_xerr else None, yerr=sigmin_use_err,
                            fmt="v", c="blue", label=r"Minor Double Gauss", markersize=5, alpha=1.)

                # x_full = np.hstack(
                #     [mean_ex_[fsum], mean_ex_[~fsum][distmin <= distmax], mean_ex_[~fsum][distmin > distmax]]).ravel()
                # y_full = np.hstack([sig_, sigmin_use, sigmax_use]).ravel()
                # out = icaps.fit_model(icaps.fit_lin, x=np.log10(x_full), y=np.log10(y_full),
                #                       p0=[-0.5, -5], fit_type=2)
                # ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="magenta", ls="--", lw=2,
                #         label="Single and Double Gauss Fit\nSlope = {:.2f} \u00b1 {:.2f}".format(out.beta[0],
                #                                                                                  out.sd_beta[0]))
                ax.legend()
                plt.savefig(plot_dg_path + "sigma_mass_trans_i{}/".format(inject + 1) +
                            "sigma_mass_trans_i{}_{}_{}_{}_{}_{}.png".format(inject + 1, axis, dt, w_thresh,
                                                                             dg_min_chi_factor, max_min_thresh),
                            dpi=600)
                plt.close(fig)

                fig, ax = plt.subplots()
                ax.set_xlabel(r"$\langle E \rangle$ (arb. units)")
                ax.set_ylabel(r"$\sigma_{\rm trans}$ (m)")
                ax.set_yscale("log")
                ax.set_xscale("log")
                ax.set_xlim(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex))
                ax.set_ylim(9e-9, 9e-5)
                ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))
                ax.scatter(mean_ex_[~fsum][distmin <= distmax], sigmax_[distmin <= distmax], marker="^", c="red",
                           label=r"Major Double Gauss", s=5, alpha=1.)
                ax.scatter(mean_ex_[~fsum][distmin > distmax], sigmin_[distmin > distmax], marker="v", c="blue",
                           label=r"Minor Double Gauss", s=5, alpha=1.)
                ax.legend()
                plt.savefig(plot_dg_path + "sigma_mass_noise_trans_i{}/".format(inject + 1) +
                            "sigma_mass_noise_trans_i{}_{}_{}_{}_{}_{}.png".format(inject + 1, axis, dt, w_thresh,
                                                                                   dg_min_chi_factor, max_min_thresh),
                            dpi=600)
                plt.close(fig)

                extra_plots[idt][1].errorbar(mean_ex_[~fsum][distmin <= distmax], sigmax_[distmin <= distmax],
                                             fmt="^", color="red", markersize=5, alpha=1.)
                extra_plots[idt][1].errorbar(mean_ex_[~fsum][distmin > distmax], sigmin_[distmin > distmax],
                                             fmt="v", color="blue", markersize=5, alpha=1.)

                if df_noise is None:
                    p = parts_[~fsum][distmin <= distmax]
                    df_noise = pd.DataFrame(data={"particle": p,
                                                  "dt": np.repeat(dt, len(p)),
                                                  "axis": np.repeat(axis, len(p)),
                                                  "inject": np.repeat(inject, len(p)),
                                                  "sigma": sigmax_[distmin <= distmax],
                                                  "sigma_err": sigmax_err_[distmin <= distmax],
                                                  "sigma_type": np.repeat("high double", len(p))})
                    p = parts_[~fsum][distmin > distmax]
                    df_noise_ = pd.DataFrame(data={"particle": p,
                                                   "dt": np.repeat(dt, len(p)),
                                                   "axis": np.repeat(axis, len(p)),
                                                   "inject": np.repeat(inject, len(p)),
                                                   "sigma": sigmin_[distmin > distmax],
                                                   "sigma_err": sigmin_err_[distmin > distmax],
                                                   "sigma_type": np.repeat("low double", len(p))})
                    df_noise = df_noise.append(df_noise_, sort=False)
                else:
                    p = parts_[~fsum][distmin <= distmax]
                    df_noise_ = pd.DataFrame(data={"particle": p,
                                                   "dt": np.repeat(dt, len(p)),
                                                   "axis": np.repeat(axis, len(p)),
                                                   "inject": np.repeat(inject, len(p)),
                                                   "sigma": sigmax_[distmin <= distmax],
                                                   "sigma_err": sigmax_err_[distmin <= distmax],
                                                   "sigma_type": np.repeat("high double", len(p))})
                    df_noise = df_noise.append(df_noise_, sort=False)
                    p = parts_[~fsum][distmin > distmax]
                    df_noise_ = pd.DataFrame(data={"particle": p,
                                                   "dt": np.repeat(dt, len(p)),
                                                   "axis": np.repeat(axis, len(p)),
                                                   "inject": np.repeat(inject, len(p)),
                                                   "sigma": sigmin_[distmin > distmax],
                                                   "sigma_err": sigmin_err_[distmin > distmax],
                                                   "sigma_type": np.repeat("low double", len(p))})
                    df_noise = df_noise.append(df_noise_, sort=False)
                for i, part in enumerate(parts_[fsum]):
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma"] = sig_[i]
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma_err"] = sig_err_[i]
                for i, part in enumerate(parts_[~fsum][distmin <= distmax]):
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma"] = sigmin_use[i]
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma_err"] = sigmin_use_err[i]
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma_type"] = "low double"
                for i, part in enumerate(parts_[~fsum][distmin > distmax]):
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma"] = sigmax_use[i]
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma_err"] = sigmax_use_err[i]
                    df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
                                  (df_erf_dg["dt"] == dt), "sigma_type"] = "high double"
                pb.tick()
    pb.finish()
    for i, xax in enumerate(extra_plots):
        xax[0].savefig(plot_dg_path + "sigma_mass_noise_trans_extra/" +
                       "sigma_mass_noise_trans_extra_{}.png".format(dts[i]), dpi=600)
        plt.close(xax[0])
    plt.close("all")

    df_erf_dg.to_csv(csv_erf_trans_dg_path, index=False)
    df_noise.to_csv(csv_erf_trans_noise_path, index=False)
    """
    # """
    # ============================================================
    # TRANS DOUBLE GAUSS OF
    # ============================================================
    csv_of_trans_dg_path = project_path + "OF_Trans_DG.csv"

    df_erf_dg = pd.read_csv(csv_erf_trans_dg_path)
    plot = True
    dts_trim = np.arange(1, 11, 1)
    dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
    dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
    df_of_dg = icaps.brownian.calc_ornstein_fuerth(
        df_erf_dg, dts_trim, axes=["x", "y"], mode="trans", temp_path=None, silent=False, ret_erf=False,
        plot={"path": project_path + "plots_dg/of_trans_dg_{}/" if plot else None,
              "attempts": False,
              "error": True,
              "dpi": 600,
              "clear_out": True,
              "crop_to_max": True},
        fit={"fit_type": 2,
             "fit_err": True,
             "bounds": (1, None),
             "ballistic_fit": 2,
             "ballistic_bounds": (1, 6),
             "max_sigsqr": 1e-6,
             "max_err_ratio": 1,
             "min_tau": None},
        double_gauss=True)
    df_of_dg.to_csv(csv_of_trans_dg_path, index=False)

    df_of_dg = pd.read_csv(csv_of_trans_dg_path)
    max_ratio = .2

    axes = np.unique(df_of_dg["axis"].to_numpy())
    cols = list(df_of_dg.columns)[2:]
    df_clean = None
    for axis in axes:
        df_ = df_of_dg[df_of_dg["axis"] == axis]
        df_ = df_.drop("axis", axis=1)
        df_ = df_.rename(
            columns={col: "{}_{}".format(col, axis) if "_err" not in col else "{}_{}_err".format(col[:-4], axis) for col
                     in cols})
        if df_clean is None:
            df_clean = df_
        else:
            df_clean = pd.merge(df_clean, df_, how="left", on="particle")

    df_track = pd.read_csv(csv_trans_path)
    particles = df_clean["particle"].unique()
    df_track = df_track[df_track["particle"].isin(particles)]
    nan_arr = np.zeros(len(particles))
    nan_arr[:] = np.nan
    df_ex = pd.DataFrame(data={
        "particle": particles,
        "start_frame": np.zeros(len(particles), dtype=int),
        "diff_frames": np.zeros(len(particles), dtype=int),
        "n_frames": np.zeros(len(particles), dtype=int),
        "x_start": nan_arr,
        "x_stop": nan_arr,
        "y_start": nan_arr,
        "y_stop": nan_arr,
        "mean_area": nan_arr,
        "mean_area_err": nan_arr,
        "mean_ex": nan_arr,
        "mean_ex_err": nan_arr,
        "mean_gyrad": nan_arr,
        "mean_gyrad_err": nan_arr
    })
    pb = icaps.ProgressBar(len(particles), "Collecting Ex Data")
    for particle in particles:
        idx = df_ex.index[df_ex["particle"] == particle]
        df_t = df_track[df_track["particle"] == particle]
        frames = df_t["frame"].to_numpy()
        area = df_t["area"].to_numpy()
        extinction = df_t["mass"].to_numpy()
        gyrad = df_t["gyrad"].to_numpy()
        df_ex.loc[idx, "start_frame"] = frames[0]
        df_ex.loc[idx, "diff_frames"] = frames[-1] - frames[0]
        df_ex.loc[idx, "n_frames"] = frames.shape[0]
        df_ex.loc[idx, "mean_area"] = np.mean(area)
        df_ex.loc[idx, "mean_area_err"] = np.std(area)
        df_ex.loc[idx, "mean_ex"] = np.mean(extinction)
        df_ex.loc[idx, "mean_ex_err"] = np.std(extinction)
        df_ex.loc[idx, "mean_gyrad"] = np.mean(gyrad)
        df_ex.loc[idx, "mean_gyrad_err"] = np.std(gyrad)
        for axis in axes:
            idx = df_ex.index[df_ex["particle"] == particle]
            pos = df_t[axis].to_numpy()
            df_ex.loc[idx, "{}_start".format(axis)] = pos[0]
            df_ex.loc[idx, "{}_stop".format(axis)] = pos[-1]
        pb.tick()
    pb.finish()

    df = pd.merge(df_clean, df_ex, how="left", on="particle")
    # df = pd.merge(df, df_rot, how="left", on="particle")
    df = icaps.move_col(df, "mass_y", "tau_x")
    df = icaps.move_col(df, "mass_y_err", "tau_x")
    df = icaps.move_col(df, "tau_y", "chisqr_x")
    df = icaps.move_col(df, "tau_y_err", "chisqr_x")
    df = icaps.move_col(df, "chisqr_y", "n_dots_x")
    df = icaps.move_col(df, "n_dots_y", "t_diff_x")
    df.to_csv(project_path + "OF_Chi{}Ex_DG.csv".format(chisqr_thresh), index=False)
    # """


if __name__ == '__main__':
    main()
