import os

import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.odr import ODR, Model, RealData


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/"
# ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
particle_path = project_path + "particles/"
# movie_path = project_path + "movies/"
collection_path = project_path + "collection/"
csv_path = project_path + "icaps_virtual_motion.csv"
csv_clean_path = project_path + "virtual_motion_clean.csv"
# csv_phase_path = project_path + "Phases_Hand.csv"
# csv_prefilter_path = project_path + "Tracks_PA50S20L100.csv"
# csv_P_path = project_path + "Tracks_P.csv"
# csv_PCF_path = project_path + "Tracks_PCF.csv"
# csv_PCFE_path = project_path + "Tracks_PCFE.csv"
# csv_disp_rot_path = project_path + "disp_rot.csv"
csv_disp_trans_path = project_path + "disp_trans.csv"
# csv_log_path = project_path + "log.csv"

icaps.mk_folders([particle_path, collection_path])


def find_intervals(df_ingo):
    intervals = []
    lastnan = True
    start, stop = 0, 0
    pb = icaps.ProgressBar(df_ingo.shape[0])
    for i, row in df_ingo.iterrows():
        if lastnan and ~np.isnan(row["posx"]):
            start = round(row["ldm_image"])
            lastnan = False
        if not lastnan and np.isnan(row["posx"]):
            lastnan = True
            intervals.append((start, stop))
        stop = round(row["ldm_image"])
        pb.tick()
    pb.finish()
    return intervals


def main():
    df_ingo = pd.read_csv(csv_path)
    # print(find_intervals(df_ingo))
    intervals = [(31109, 43904), (51508, 65304), (72508, 86403), (93907, 107407)]
    df_ingo = df_ingo[~np.isnan(df_ingo["posx"])]
    df = pd.DataFrame(data={"frame": [0], "particle": [0], "x": [0], "y": [0], "z": [0]})
    df = df.drop(0)
    df["frame"] = np.around(df_ingo["ldm_image"]).astype(int)
    df["x"] = df_ingo["posx"] * 1e6
    df["y"] = df_ingo["posy"] * 1e6
    df["z"] = df_ingo["posz"] * 1e6
    for i, interval in enumerate(intervals):
        start = df[df["frame"] == interval[0]].index[0]
        stop = df[df["frame"] == interval[1]].index[0] + 1
        df.loc[start:stop, "particle"] = i + 1
    df["particle"] = df["particle"].astype(int)
    df.to_csv(csv_clean_path, index=False)

    min_dts = np.arange(4, 49, 4)

    # ==============================================
    # CALCULATE TRANS DISPLACEMENTS
    # ==============================================
    df = pd.read_csv(csv_clean_path)
    df_trans = icaps.calc_trans_displacements(df, min_dts, particle_path=particle_path, silent=False,
                                              plot=True, approx_large_dt=False, align="cols",
                                              in_columns=["x", "y", "z"], out_columns=["vx", "vy", "vz"])
    df_trans.to_csv(csv_disp_trans_path, index=False)

    # ==============================================
    # PLOT ERROR FUNCTIONS
    # ==============================================
    df_trans = pd.read_csv(csv_disp_trans_path)
    icaps.plot_erf_bulk(df_trans, min_dts, particle_path, mode="Translation", plot=True, silent=False,
                        p0=(10, 0), maxfev=1000, dpi=300, axis_labels=["x", "y", "z"], mode_unit="µm", mode_key="v")

    plt.rcParams.update({"font.size": 12})
    _, ax = plt.subplots()
    particle_real_path = drive_letter + ":/icaps/data/LDM_ROT2/particles/"
    files = os.listdir(particle_real_path)
    particles = [int(float(file)) for file in files]
    pb = icaps.ProgressBar(len(particles), "Fitting Real Data")
    tops = []
    s = []
    low_err = []
    high_err = []
    dts = None
    for particle in particles:
        if not os.path.exists(particle_real_path + "{}/erf_trans.csv".format(particle)):
            pb.tick()
            continue
        df = pd.read_csv(particle_real_path + "{}/erf_trans.csv".format(particle))
        dts = np.unique(df["dt"].to_numpy()) / 1e3  # in s
        axis_labels = np.unique(df["axis"].to_numpy())
        sigma_squared = np.zeros((len(axis_labels), len(dts)))
        sigma_squared_err = np.zeros((len(axis_labels), len(dts)))
        for ai, a in enumerate(axis_labels):
            sigma = np.squeeze(df.loc[df["axis"] == a, ["sigma"]].to_numpy())
            sigma_err = np.squeeze(df.loc[df["axis"] == a, ["sigma_err"]].to_numpy())
            sigma_squared[ai] = sigma ** 2
            sigma_squared_err[ai] = 2 * sigma * sigma_err
            sigma_squared[ai, np.isinf(sigma_squared_err[ai])] = np.nan
            sigma_squared_err[ai, np.isinf(sigma_squared_err[ai])] = np.nan
        tops.append(np.max(sigma_squared[~np.isnan(sigma_squared)]))
        s_ = np.nanmean(sigma_squared, axis=0)
        s.append(s_)
        lo = sigma_squared - sigma_squared_err
        lo[lo <= 0] = np.nan
        low_err_ = np.nanmin(lo, axis=0)
        high_err_ = np.nanmax(sigma_squared + sigma_squared_err, axis=0)
        low_err.append(low_err_)
        high_err.append(high_err_)
        pb.tick()
    pb.finish()
    s = np.array(s)
    low_err = np.array(low_err)
    high_err = np.array(high_err)
    s_mean = np.nanmean(s, axis=0)
    s_mean_low_err = np.nanmin(low_err, axis=0)
    s_mean_high_err = np.nanmax(high_err, axis=0)

    ax.errorbar(dts, s_mean, yerr=[s_mean - s_mean_low_err, s_mean_high_err - s_mean], fmt=".", color="black", markersize=10,
                zorder=100, label="mean measurements")

    files = os.listdir(particle_path)
    particles = [int(float(file)) for file in files]
    pb = icaps.ProgressBar(len(particles), "Fitting Real Data")
    tops = []
    s = []
    low_err = []
    high_err = []
    dts = None
    for particle in particles:
        if not os.path.exists(particle_path + "{}/erf_Translation.csv".format(particle)):
            pb.tick()
            continue
        df = pd.read_csv(particle_path + "{}/erf_Translation.csv".format(particle))
        dts = np.unique(df["dt"].to_numpy()) / 1e3  # in s
        axis_labels = np.unique(df["axis"].to_numpy())
        sigma_squared = np.zeros((len(axis_labels), len(dts)))
        sigma_squared_err = np.zeros((len(axis_labels), len(dts)))
        for ai, a in enumerate(axis_labels):
            sigma = np.squeeze(df.loc[df["axis"] == a, ["sigma"]].to_numpy())
            sigma_err = np.squeeze(df.loc[df["axis"] == a, ["sigma_err"]].to_numpy())
            sigma_squared[ai] = sigma ** 2
            sigma_squared_err[ai] = 2 * sigma * sigma_err
            sigma_squared[ai, np.isinf(sigma_squared_err[ai])] = np.nan
            sigma_squared_err[ai, np.isinf(sigma_squared_err[ai])] = np.nan
        tops.append(np.max(sigma_squared[~np.isnan(sigma_squared)]))
        s_ = np.nanmean(sigma_squared, axis=0)
        s.append(s_)
        lo = sigma_squared - sigma_squared_err
        lo[lo <= 0] = np.nan
        low_err_ = np.nanmin(lo, axis=0)
        high_err_ = np.nanmax(sigma_squared + sigma_squared_err, axis=0)
        low_err.append(low_err_)
        high_err.append(high_err_)
        pb.tick()
    pb.finish()
    s = np.array(s)
    low_err = np.array(low_err)
    high_err = np.array(high_err)
    s_mean = np.nanmean(s, axis=0)
    s_mean_low_err = np.nanmin(low_err, axis=0)
    s_mean_high_err = np.nanmax(high_err, axis=0)

    ax.errorbar(dts, s_mean, yerr=[s_mean - s_mean_low_err, s_mean_high_err - s_mean], fmt=".", color="black",
                markersize=5, marker="x",
                zorder=100, label="mean residual motion")

    ax.set_ylabel(r"$\sigma_{trans}^2$ (m$^2$)")
    ax.legend(loc="upper left")
    ax.set_xlabel("dt (s)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    plt.tight_layout()
    plt.show()

    """
    # ==============================================
    # COMPARATIVE OF PLOT (Means)
    # ==============================================
    plt.rcParams.update({"font.size": 12})
    _, ax = plt.subplots()
    particle_real_path = drive_letter + ":/icaps/data/LDM_ROT2/particles/"
    files = os.listdir(particle_real_path)
    particles = [int(float(file)) for file in files]
    pb = icaps.ProgressBar(len(particles), "Fitting Real Data")
    tops = []
    s = []
    s_err = []
    dts = None
    for particle in particles:
        if not os.path.exists(particle_real_path + "{}/erf_trans.csv".format(particle)):
            pb.tick()
            continue
        df = pd.read_csv(particle_real_path + "{}/erf_trans.csv".format(particle))
        dts = np.unique(df["dt"].to_numpy()) / 1e3  # in s
        axis_labels = np.unique(df["axis"].to_numpy())
        sigma_squared = np.zeros((len(axis_labels), len(dts)))
        sigma_squared_err = np.zeros((len(axis_labels), len(dts)))
        for ai, a in enumerate(axis_labels):
            sigma = np.squeeze(df.loc[df["axis"] == a, ["sigma"]].to_numpy())
            sigma_err = np.squeeze(df.loc[df["axis"] == a, ["sigma_err"]].to_numpy())
            sigma_squared[ai] = sigma ** 2
            sigma_squared_err[ai] = 2 * sigma * sigma_err
            sigma_squared[ai, np.isinf(sigma_squared_err[ai])] = np.nan
            sigma_squared_err[ai, np.isinf(sigma_squared_err[ai])] = np.nan
            # sigma_squared[ai, sigma_squared_err[ai] > 1e-12] = np.nan
            # sigma_squared_err[ai, sigma_squared_err[ai] > 1e-12] = np.nan
        tops.append(np.max(sigma_squared[~np.isnan(sigma_squared)]))
        s.append(np.nanmean(sigma_squared, axis=0))
        num = len(axis_labels)
        num_nan = np.count_nonzero(~np.isnan(sigma_squared_err), axis=0)
        s_err_ = np.sqrt(np.nansum(np.square(sigma_squared_err), axis=0))
        f = num-num_nan
        s_err_[f != 0] *= 1/f[f != 0]
        s_err.append(s_err_)
        pb.tick()
    pb.finish()
    s = np.array(s)
    s_err = np.array(s_err)
    s_mean = np.nanmean(s, axis=0)
    num = s.shape[0]
    num_nan = np.count_nonzero(~np.isnan(s_err), axis=0)
    f = num - num_nan
    s_mean_err = np.sqrt(np.nansum(np.square(s_err), axis=0))
    s_mean_err[f != 0] *= 1 / f[f != 0]
    # print(s_mean)
    # print(s_mean_err)

    ax.errorbar(dts, s_mean, yerr=s_mean_err, fmt=".", color="black", markersize=10,
                zorder=100, label="mean measurements")

    files = os.listdir(particle_path)
    particles = [int(float(file)) for file in files]
    pb = icaps.ProgressBar(len(particles), "Fitting Real Data")
    tops = []
    s = []
    s_err = []
    dts = None
    for particle in particles:
        if not os.path.exists(particle_path + "{}/erf_Translation.csv".format(particle)):
            pb.tick()
            continue
        df = pd.read_csv(particle_path + "{}/erf_Translation.csv".format(particle))
        dts = np.unique(df["dt"].to_numpy()) / 1e3  # in s
        axis_labels = np.unique(df["axis"].to_numpy())
        sigma_squared = np.zeros((len(axis_labels), len(dts)))
        sigma_squared_err = np.zeros((len(axis_labels), len(dts)))
        for ai, a in enumerate(axis_labels):
            sigma = np.squeeze(df.loc[df["axis"] == a, ["sigma"]].to_numpy())
            sigma_err = np.squeeze(df.loc[df["axis"] == a, ["sigma_err"]].to_numpy())
            sigma_squared[ai] = sigma ** 2
            sigma_squared_err[ai] = 2 * sigma * sigma_err
            sigma_squared[ai, np.isinf(sigma_squared_err[ai])] = np.nan
            sigma_squared_err[ai, np.isinf(sigma_squared_err[ai])] = np.nan
        tops.append(np.max(sigma_squared[~np.isnan(sigma_squared)]))
        s.append(np.nanmean(sigma_squared, axis=0))
        num = len(axis_labels)
        num_nan = np.count_nonzero(~np.isnan(sigma_squared_err), axis=0)
        s_err_ = np.sqrt(np.nansum(np.square(sigma_squared_err), axis=0))
        f = num - num_nan
        s_err_[f != 0] *= 1 / f[f != 0]
        s_err.append(s_err_)
        pb.tick()
    pb.finish()
    s = np.array(s)
    s_err = np.array(s_err)
    s_mean = np.nanmean(s, axis=0)
    num = s.shape[0]
    num_nan = np.count_nonzero(~np.isnan(s_err), axis=0)
    f = num - num_nan
    s_mean_err = np.sqrt(np.nansum(np.square(s_err), axis=0))
    s_mean_err[f != 0] *= 1 / f[f != 0]
    print(s_mean)
    print(s_mean_err)

    ax.errorbar(dts, s_mean, yerr=s_mean_err, fmt=".", color="black", markersize=10,
                zorder=100, label="mean residual motion", marker="x")

    ax.set_ylabel(r"$\sigma_{trans}^2$ (m$^2$)")
    ax.legend(loc="upper left")
    ax.set_xlabel("dt (s)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    plt.tight_layout()
    plt.show()
    """

    """
    # ==============================================
    # COMPARATIVE OF PLOT (OF-Fits)
    # ==============================================
    plot_error = False
    plot_attempts = False
    grade_weights = (1., 1.)
    maxfev = 0
    quantity_type = "time"
    p0 = (None, None)

    plt.rcParams.update({"font.size": 12})
    _, ax = plt.subplots()
    # ========================
    # PLOT REAL DATA
    particle_real_path = drive_letter + ":/icaps/data/LDM_ROT2/particles/"
    files = os.listdir(particle_real_path)
    particles = [int(float(file)) for file in files]
    pb = icaps.ProgressBar(len(particles), "Fitting Real Data")
    tops = []
    for particle in particles:
        if not os.path.exists(particle_real_path + "{}/erf_trans.csv".format(particle)):
            pb.tick()
            continue
        df = pd.read_csv(particle_real_path + "{}/erf_trans.csv".format(particle))
        axis_labels = np.unique(df["axis"].to_numpy())
        dts = np.unique(df["dt"].to_numpy()) / 1e3  # in s

        candidates = icaps.parse_fit_bounds(dts, fit_bounds=("adapt", "adapt"), adapt_margins=(4, 25), min_diff=6)

        p0 = list(p0)
        if p0[0] is None:
            p0[0] = np.log10(2 * icaps.const.k_B * icaps.const.T / (icaps.const.m_p * icaps.const.r_p ** 2))
        if p0[1] is None:
            p0[1] = 0.001

        # data = pd.DataFrame(data={"axis": axis_labels,
        #                           "tau": [np.nan, np.nan],
        #                           "tau_err": [np.nan, np.nan],
        #                           "mass": [np.nan, np.nan],
        #                           "mass_err": [np.nan, np.nan],
        #                           "idx_from": [0, 0],
        #                           "idx_to": [0, 0],
        #                           "dt_from": [0., 0.],
        #                           "dt_to": [0., 0.],
        #                           })
        for ai, a in enumerate(axis_labels):
            sigma = np.squeeze(df.loc[df["axis"] == a, ["sigma"]].to_numpy())
            sigma_squared = sigma ** 2
            tops.append(np.max(sigma_squared[~np.isnan(sigma_squared)]))
            sigma_squared_err = 2 * df.loc[df["axis"] == a, ["sigma_err"]].to_numpy()
            if plot_error:
                ax.errorbar(dts, sigma_squared, yerr=sigma_squared_err, fmt=".", color="black", markersize=10,
                            zorder=100)
            else:
                ax.scatter(dts, sigma_squared, s=10, c="black", zorder=100)
            popts = []
            perrs = []
            quality = np.zeros(len(candidates), dtype=float)
            quantity = np.zeros(len(candidates), dtype=float)
            for ci, candidate in enumerate(candidates):
                c0, c1 = candidate[0], candidate[1]
                popts.append(None)
                perrs.append(None)
                try:
                    try:
                        popt, pcov = curve_fit(icaps.fit_of_log, dts[c0:c1], np.log10(sigma_squared[c0:c1]), p0=p0,
                                               maxfev=maxfev)
                    except RuntimeError:
                        continue
                    perr = np.sqrt(np.diag(pcov))
                    popts[ci] = popt
                    perrs[ci] = perr
                    quality[ci] = abs(popt[1] / perr[1])
                    if quantity_type == "time":
                        quantity[ci] = dts[c1] - dts[c0]
                    else:
                        quantity[ci] = c1 - c0
                    if plot_attempts:
                        dt_range = np.linspace(np.min(dts[c0:]), np.max(dts[:c1]), 1000)
                        ax.plot(dt_range, 10 ** (icaps.fit_of_log(dt_range, *popt)), color="gray", lw=1, linestyle="-.",
                                alpha=0.3)
                except ValueError or RuntimeError:
                    continue
            if np.max(quality) == 0:
                continue
            quality = quality / np.max(quality)
            quantity = quantity / np.max(quantity)
            grade = quality * grade_weights[0] + quantity * grade_weights[1]
            best_idx = np.argmax(grade)
            popt = popts[best_idx]
            perr = perrs[best_idx]
            if popt is None:
                continue
            c0, c1 = candidates[best_idx][0], candidates[best_idx][1]
            t0, t1 = dts[c0], dts[c1 - 1]
            dt_range = np.linspace(np.min(dts[c0:]), np.max(dts[:c1]), 1000)
            ax.plot(dt_range, 10 ** (icaps.fit_of_log(dt_range, *popt)), color="red", lw=2, linestyle="-",
                    label=r"OF_log " + str((c0, c1)))
            # data.loc[data["axis"] == a, ["tau"]] = abs(popt[1])  # in s
            # data.loc[data["axis"] == a, ["tau_err"]] = perr[1]  # in s
            # data.loc[data["axis"] == a, ["mass"]] = 2 * icaps.const.k_B * icaps.const.T / (10 ** popt[0])  # in kg
            # data.loc[data["axis"] == a, ["mass_err"]] = 2 * icaps.const.k_B * icaps.const.T * np.log(10) /\
            #                                             (10 ** popt[0]) * perr[0]  # in kg
            # data.loc[data["axis"] == a, ["idx_from"]] = c0
            # data.loc[data["axis"] == a, ["idx_to"]] = c1
            # data.loc[data["axis"] == a, ["dt_from"]] = t0  # in s
            # data.loc[data["axis"] == a, ["dt_to"]] = t1  # in s

        pb.tick()
    pb.finish()

    # ====================
    # PLOT VIRTUAL
    plot_error = False
    plot_attempts = False
    grade_weights = (1., 1.)
    maxfev = 0
    quantity_type = "time"
    p0 = (None, None)
    markers = ["x", "^", "s"]
    colors = ["green", "blue", "yellow"]

    files = os.listdir(particle_path)
    particles = [int(float(file)) for file in files]
    pb = icaps.ProgressBar(len(particles), "Fitting Virtual Data")
    for particle in particles:
        if not os.path.exists(particle_path + "{}/erf_Translation.csv".format(particle)):
            pb.tick()
            continue
        df = pd.read_csv(particle_path + "{}/erf_Translation.csv".format(particle))
        axis_labels = np.unique(df["axis"].to_numpy())
        dts = np.unique(df["dt"].to_numpy()) / 1e3  # in s

        candidates = icaps.parse_fit_bounds(dts, fit_bounds=("adapt", "adapt"), adapt_margins=(4, 25), min_diff=6)

        p0 = list(p0)
        if p0[0] is None:
            p0[0] = np.log10(2 * icaps.const.k_B * icaps.const.T / (icaps.const.m_p * icaps.const.r_p ** 2))
        if p0[1] is None:
            p0[1] = 0.001

        # data = pd.DataFrame(data={"axis": axis_labels,
        #                           "tau": [np.nan, np.nan],
        #                           "tau_err": [np.nan, np.nan],
        #                           "mass": [np.nan, np.nan],
        #                           "mass_err": [np.nan, np.nan],
        #                           "idx_from": [0, 0],
        #                           "idx_to": [0, 0],
        #                           "dt_from": [0., 0.],
        #                           "dt_to": [0., 0.],
        #                           })
        for ai, a in enumerate(axis_labels):
            sigma = np.squeeze(df.loc[df["axis"] == a, ["sigma"]].to_numpy())
            sigma_squared = sigma ** 2
            tops.append(np.max(sigma_squared[~np.isnan(sigma_squared)]))
            sigma_squared_err = 2 * df.loc[df["axis"] == a, ["sigma_err"]].to_numpy()
            if plot_error:
                ax.errorbar(dts, sigma_squared, yerr=sigma_squared_err, fmt=".", color=colors[ai], markersize=10,
                            zorder=100)
            else:
                ax.scatter(dts, sigma_squared, s=10, c=colors[ai], zorder=100, marker=markers[ai])
            popts = []
            perrs = []
            quality = np.zeros(len(candidates), dtype=float)
            quantity = np.zeros(len(candidates), dtype=float)
            for ci, candidate in enumerate(candidates):
                c0, c1 = candidate[0], candidate[1]
                popts.append(None)
                perrs.append(None)
                try:
                    try:
                        popt, pcov = curve_fit(icaps.fit_of_log, dts[c0:c1], np.log10(sigma_squared[c0:c1]), p0=p0,
                                               maxfev=maxfev)
                    except RuntimeError:
                        continue
                    perr = np.sqrt(np.diag(pcov))
                    popts[ci] = popt
                    perrs[ci] = perr
                    quality[ci] = abs(popt[1] / perr[1])
                    if quantity_type == "time":
                        quantity[ci] = dts[c1] - dts[c0]
                    else:
                        quantity[ci] = c1 - c0
                    if plot_attempts:
                        dt_range = np.linspace(np.min(dts[c0:]), np.max(dts[:c1]), 1000)
                        ax.plot(dt_range, 10 ** (icaps.fit_of_log(dt_range, *popt)), color="gray", lw=1, linestyle="-.",
                                alpha=0.3)
                except ValueError or RuntimeError:
                    continue
            if np.max(quality) == 0:
                continue
            quality = quality / np.max(quality)
            quantity = quantity / np.max(quantity)
            grade = quality * grade_weights[0] + quantity * grade_weights[1]
            best_idx = np.argmax(grade)
            popt = popts[best_idx]
            perr = perrs[best_idx]
            if popt is None:
                continue
            c0, c1 = candidates[best_idx][0], candidates[best_idx][1]
            t0, t1 = dts[c0], dts[c1 - 1]
            # dt_range = np.linspace(np.min(dts[c0:]), np.max(dts[:c1]), 1000)
            dt_range = np.linspace(0.001, np.max(dts[:c1]), 1000)
            ax.plot(dt_range, 10 ** (icaps.fit_of_log(dt_range, *popt)), color=colors[ai], lw=2, linestyle="--",
                    label=r"OF_log " + str((c0, c1)))
            # data.loc[data["axis"] == a, ["tau"]] = abs(popt[1])  # in s
            # data.loc[data["axis"] == a, ["tau_err"]] = perr[1]  # in s
            # data.loc[data["axis"] == a, ["mass"]] = 2 * icaps.const.k_B * icaps.const.T / (10 ** popt[0])  # in kg
            # data.loc[data["axis"] == a, ["mass_err"]] = 2 * icaps.const.k_B * icaps.const.T * np.log(10) /\
            #                                             (10 ** popt[0]) * perr[0]  # in kg
            # data.loc[data["axis"] == a, ["idx_from"]] = c0
            # data.loc[data["axis"] == a, ["idx_to"]] = c1
            # data.loc[data["axis"] == a, ["dt_from"]] = t0  # in s
            # data.loc[data["axis"] == a, ["dt_to"]] = t1  # in s

        pb.tick()
    pb.finish()
    ax.set_ylabel(r"$\sigma_{trans}^2$ in m$^2$")
    # ax.legend(loc="lower right")
    ax.set_xlabel("dt in s")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(left=1e-3 * 0.95)
    ax.set_ylim(top=np.max(tops) * 1.1)
    plt.show()
    """


if __name__ == '__main__':
    main()








