import icaps
import pandas as pd
import numpy as np
# import matplotlib.pyplot as plt


plane = "YZ"
fps = 50
start = 16670
stop = 17388
thresh = 30
cut = ((750, 0), (1024, 100))
crop_pos = ((192, 494), (286, 767))

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
orig_path = drive_letter + ":/icaps/texus56_SRE_20191115/OOS/" + plane + "/"
orig_path = orig_path + ("000000000_10h_11m_09s_936ms/" if plane == "XZ" else "000000000_10h_11m_11s_462ms/")

project_path = drive_letter + ":/icaps/data/Crazy/" + plane + "/"
in_path = project_path + "orig/"
thr_path = project_path + "thresh/"
crop_path = project_path + "cropped/"
stat_crop_path = project_path + "stationary/"
ellipses_path = project_path + "ellipses/"
plot_path = project_path + "plots/"
movie_path = project_path + "movies/"
track_path_imageJ = project_path + "imageJ/Spots in tracks statistics.csv"
track_link_imageJ = project_path + "imageJ/Links in tracks statistics.csv"
track_path = project_path + "imageJ/track_crazy_imgj_" + plane + ".csv"
spec = "(" + str(start) + ":" + str(stop) + ").bmp"


def format_imagej_track(df1, df2):
    df_new = pd.DataFrame(columns=["Camera", "Frame", "Time", "X", "Y", "Mean_Intensity", "Median_Intensity",
                                   "Min_Intensity", "Max_Intensity", "Total_Intensity", "Standard_Deviation",
                                   "Estimated_Diameter", "Velocity", "Displacement", "Quality", "Contrast", "SNR"])
    df_new["Frame"] = df1["FRAME"].copy() + start
    df_new["Camera"] = plane + "-OOS"
    # df_new["Time"] = df_new["Frame"]/fps
    # df_new["Time"] = icaps.to_timestamp(df_new["Frame"]/fps)
    df_new["Time"] = pd.to_datetime(df_new["Frame"]/fps, unit="s")
    df_new["X"] = df1["POSITION_X"] + crop_pos[0][0]
    df_new["Y"] = df1["POSITION_Y"] + crop_pos[0][1]
    df_new["Quality"] = df1["QUALITY"]
    df_new["Mean_Intensity"] = df1["MEAN_INTENSITY"]
    df_new["Median_Intensity"] = df1["MEDIAN_INTENSITY"]
    df_new["Median_Intensity"] = df1["MEDIAN_INTENSITY"]
    df_new["Min_Intensity"] = df1["MIN_INTENSITY"]
    df_new["Max_Intensity"] = df1["MAX_INTENSITY"]
    df_new["Total_Intensity"] = df1["TOTAL_INTENSITY"]
    df_new["Standard_Deviation"] = df1["STANDARD_DEVIATION"]
    df_new["Estimated_Diameter"] = df1["ESTIMATED_DIAMETER"]
    df_new["Contrast"] = df1["CONTRAST"]
    df_new["SNR"] = df1["SNR"]
    del df1
    df_new["Velocity"] = np.insert(df2["VELOCITY"].to_numpy(), 0, 0)
    df_new["Displacement"] = np.insert(df2["DISPLACEMENT"].to_numpy(), 0, 0)
    del df2
    return df_new


def main():
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)

    # ==================================================================================================================
    # Format ImageJ Data
    # ==================================================================================================================

    # df1 = pd.read_csv(track_path_imageJ)
    # df2 = pd.read_csv(track_link_imageJ)
    # df = format_imagej_track(df1, df2)
    # df.to_csv(track_path, index=False)

    # ==================================================================================================================
    # Preliminary Plots
    # ==================================================================================================================

    # icaps.plot_scatter(icaps.to_seconds(df["Time"]), df["Mean_Intensity"], marker_size=20,
    #                    axlabels=["Time", "Mean Intensity"], color="black")
    # plt.savefig(plot_path + "Mean_Intensity.png")
    # icaps.plot_scatter(icaps.to_seconds(df["Time"]), df["Median_Intensity"], marker_size=20,
    #                    axlabels=["Time", "Median Intensity"], color="black")
    # plt.savefig(plot_path + "Median_Intensity.png")

    # ==================================================================================================================
    # Fit Ellipses
    # ==================================================================================================================

    # areas = icaps.crop_stationary_particles(in_path, stat_crop_path, df, tol=0.6, thresh=40, isolate=True,
    #                                         show_progress=True, return_areas=True)
    #
    # df_boxes = icaps.fit_ellipses_isolated(stat_crop_path, out_path=ellipses_path, thresh=40,
    #                                        method=cv2.CHAIN_APPROX_NONE, show_progress=True, color=(0, 0, 255),
    #                                        thickness=4, enlarge=16)
    #
    # df = pd.merge(left=df, right=df_boxes, on="Frame")
    # df.insert(loc=5, column="Area", value=areas)
    # df.to_csv(track_path, index=False)

    # ==================================================================================================================
    # Angular Velocity
    # ==================================================================================================================

    # df = pd.read_csv(track_path, parse_dates=["Time"])
    # print(df)
    #
    # angle = df["Ellipse_Angle"].to_numpy()
    # angle_kinks = np.where(np.abs(np.gradient(angle)) > 50)[0]
    # kink_dist = np.diff(angle_kinks)
    # kink_delete = np.where(kink_dist < 5)[0]
    # angle_kinks = np.delete(angle_kinks, kink_delete)
    # kink_dist = np.diff(angle_kinks)
    # periods = kink_dist/fps
    #
    # x = icaps.to_seconds(df["Time"])
    # fig, ax = icaps.plot_scatter(x, angle, marker_size=20, color="black",
    #                              axlabels=["Time in Seconds", "Ellipse Angle in Degree"])
    # for i in range(angle_kinks.shape[0]):
    #     x_kinks = x[angle_kinks[i]]
    #     ax.axvline(x_kinks, color="mediumblue", linestyle="--")
    #     if i < angle_kinks.shape[0] - 1:
    #         ax.text(x_kinks+0.01, 175, str(periods[i]), color="mediumblue")
    # plt.savefig(plot_path + "Ellipse_Angle.png")
    # plt.close()
    #
    # for angle_kink in angle_kinks:
    #     angle[angle_kink:] += 180
    # fig, ax = icaps.plot_scatter(x, angle, marker_size=20, color="black",
    #                              axlabels=["Time in Seconds", "Ellipse Angle in Degree"])
    # fig, ax, popt, pcov, perr = icaps.plot_fit(x, angle, icaps.lin_fit, linestyle="-.", color="red", linewidth=2,
    #                                            legend=True, decimals=0,
    #                                            legend_kwargs=dict(loc="lower right", framealpha=1))
    # ax.set_title("Period = " + str(np.round(1/(popt[0]/360), decimals=2)) + " s", fontsize=10)
    # fig.tight_layout()
    # plt.savefig(plot_path + "Ellipse_Angle_Linked.png")

    # ==================================================================================================================
    # Inertia
    # ==================================================================================================================

    df = pd.read_csv(track_path, parse_dates=["Time"])
    print(df)

    monomer_intensity = 255
    inertia = 1/4 * df["Total_Intensity"]/monomer_intensity * ((df["Ellipse_Semi_Major_Axis"]*10)**2 +
                                                               (df["Ellipse_Semi_Minor_Axis"]*10)**2)
    # icaps.plot_scatter(icaps.to_seconds(df["Time"]), inertia, marker_size=20, color="black",
    #                    axlabels=["Time in Seconds", "Inertia around Ellipse Center in Monomers * µm²"])
    # plt.savefig(plot_path + "Ellipse_Center_Inertia.png")
    #
    # icaps.plot_scatter(icaps.to_seconds(df["Time"]), df["Area"], marker_size=20, color="black",
    #                    axlabels=["Time in Seconds", "Area in px"])
    # plt.savefig(plot_path + "Area.png")
    #
    # inertia = icaps.calc_inertia(stat_crop_path)["Geometrical_Inertia"]
    # icaps.plot_scatter(icaps.to_seconds(df["Time"]), inertia, marker_size=20, color="black",
    #                    axlabels=["Time in Seconds", "Geometrical Inertia in arb. units"])
    # plt.savefig(plot_path + "Geometrical_Inertia.png")

    inertia *= 10**2 * df["Median_Intensity"]

    # icaps.plot_scatter(icaps.to_seconds(df["Time"]),
    #                    [df["Ellipse_Semi_Major_Axis"]*10, df["Ellipse_Semi_Minor_Axis"]*10],
    #                    marker_size=20, marker=[".", "x"], color="black",
    #                    axlabels=["Time in Seconds", "Axis Length in µm"], legend=True,
    #                    label=["Semi Major Axis", "Semi Minor Axis"])
    # plt.savefig(plot_path + "Ellipse_Semi_Major_Axis.png")

    # todo: Moment of Inertia -> Rotational Energy, z-Direction from Intensity

    # ==================================================================================================================
    # Present
    # ==================================================================================================================

    # icaps.cvdiashow(ellipses_path, size=3, t=10)
    icaps.mk_movie(ellipses_path, movie_path + "crazy_ellipses.mp4", fps=30)


if __name__ == '__main__':
    main()
