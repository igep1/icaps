import icaps
import pandas as pd
import trackpy as tp
import numpy as np


fit1 = [icaps.lin_fit, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [icaps.lin_fit, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (260300, )

thresh = 3
min_area = 4
connectivity = 4

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/Agglomerates/Agg7/"
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.npy"
orig_path = project_path + "orig/"
ffc_path = project_path + "ffc/"
cca_path = project_path + "cca/"
show_progress = True
# start = 4700
# stop = 5375
# step = 1
# interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)
interval_string = ""
out_path = cca_path + "cca_" + str(thresh) + "/"
temp_path = out_path + "cca_temp/"
contour_path = out_path + "contours/"
mark_path = out_path + "mark/"
mark_track_path = out_path + "mark_track/"
mask_path = None
agg_path = out_path + "agg/"
csv_path = out_path + "cca_" + str(thresh) + ".csv"


def main():
    icaps.mk_folder(ffc_path)
    ff = icaps.load_img(ff_path)
    icaps.ff_subtract_bulk(orig_path + interval_string, ffc_path, ff, fits=(fit1, fit2), fit_cutoffs=fit_cutoffs,
                           silent=False, clear_out=True, strip_in=True)

    icaps.mk_folder(out_path)
    files = icaps.get_files(ffc_path + interval_string)
    if mark_path is not None:
        icaps.mk_folder(mark_path)
    if contour_path is not None:
        icaps.mk_folder(contour_path)
    icaps.mk_folder(temp_path)

    pb = None
    if show_progress:
        pb = icaps.ProgressBar(len(files), title="Connected Component Analysis")
    for file in files:
        frame = icaps.get_frame(file)
        img = icaps.load_img(ffc_path + file)
        mask = None
        if mask_path is not None:
            mask = icaps.load_img(mask_path + file)
        df = icaps.cca(img, frame, thresh, min_area=min_area, mask=mask, connectivity=connectivity,
                       mark_path=mark_path + file if mark_path is not None else None,
                       contour_path=contour_path + ".".join(file.split(".")[:-1]) + ".npy"
                       if contour_path is not None else None)
        df.to_csv(temp_path + "{num:0>{fill}}".format(fill=5, num=frame) + ".csv", index=False)
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()
    icaps.merge_dataframes(temp_path, csv_path, delete_source=True)

    df = pd.read_csv(csv_path)
    df = icaps.link(df)
    df.to_csv(out_path + "cca_track.csv", index=False)

    df = pd.read_csv(out_path + "cca_track.csv")
    icaps.mk_folder(mark_track_path)
    files = icaps.get_files(ffc_path)
    for file in files:
        frame = icaps.get_frame(file)
        df_ = df[df["frame"] == frame]
        mark_img = icaps.load_img(ffc_path + file)
        particles = np.unique(df_["particle"].to_numpy())
        for particle in particles:
            bbox = df_[df_["particle"] == particle][["bx", "by", "bw", "bh"]].to_numpy()[0]
            mark_img = icaps.draw_labelled_bbox(mark_img, bbox, label=particle, color=(0, 0, 255))
        icaps.save_img(mark_img, mark_track_path + file)

    # df = pd.read_csv(out_path + "cca_track.csv")
    # icaps.mk_folder(agg_path)
    # df = df[df["particle"] == 14]
    # bbox = df[["bx", "by", "bw", "bh"]].to_numpy()
    # com = df[["x", "y"]].to_numpy()
    # d1 = com - bbox[:, :2]
    # d2 = bbox[:, :2] + bbox[:, 2:4] - com
    # xmax = np.amax(d1[:, 0])
    # ymax = np.amax(d1[:, 1])
    # wmax = int(np.ceil(np.amax(d1[:, 0]) + np.amax(d2[:, 0])))
    # hmax = int(np.ceil(np.amax(d1[:, 1]) + np.amax(d2[:, 1])))
    #
    # frames = df["frame"].to_numpy()
    # files = icaps.get_files(ffc_path)
    # for file in files:
    #     frame = icaps.get_frame(file)
    #     if frame in frames:
    #         df_ = df[df["frame"] == frame]
    #         cx, cy, bx, by = df_[["x", "y", "bx", "by"]].to_numpy()[0]
    #         x = int(np.floor(cx - xmax))
    #         y = int(np.floor(cy - ymax))
    #         img = icaps.load_img(ffc_path + file)
    #         img = img[y:y+hmax, x:x+wmax]
    #         icaps.save_img(img, agg_path + file)
    # icaps.mk_gif(agg_path, project_path + "agg.gif", duration=0.03)
    # icaps.mk_gif(mark_track_path, project_path + "agg_track.gif", duration=0.03)


if __name__ == '__main__':
    main()
