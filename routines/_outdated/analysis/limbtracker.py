import icaps
import numpy as np
import pandas as pd
import cv2
import trackpy as tp
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.special import erf

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
ldm_path = drive_letter + ":/icaps/data/LDM/"
ffc_path = ldm_path + "ffc/"
tracks_all_path = ldm_path + "Tracks_all.csv"
project_path = drive_letter + ":/icaps/data/Limbtracker/"

part_id = 435818
particle_path = project_path + "{:0>6}/".format(part_id)
crop_path = particle_path + "cropped/"
mark_path = particle_path + "marked/"
track_mark_path = particle_path + "track_marked/"
plot_path = particle_path + "plots/"

track_path = particle_path + "track.csv"
limb_track_path = particle_path + "limbtrack.csv"
limb_track_id_path = particle_path + "limbtrack_id.csv"
limb_link_path = particle_path + "limblink.csv"
axes_path = particle_path + "axes.csv"

enlarge_mark = 8
# frame_interval = (15000, 19363)
frame_interval = (16788, 19363)
thresh = 30
id_0 = [0, 21]
id_1 = [1, 6, 40]
id_2 = [2, 12, 20, 23, 39]


def error_func(z, a, b):    # Phi(z) = 1/2[1 + erf(z/sqrt(2))]
    return 0.5*(1 + erf((z-b)/a))


def small_dt_t(x,a):
    return a * (x**2)


def small_dt_lin(x,a):
    return a * x


def crop_particle(df, particle, in_path, out_path, enlarge=1):
    icaps.mk_folder(out_path)
    df = df[df["particle"] == particle]
    dx1_max = np.amax(df["x"].to_numpy() - df["bx"].to_numpy())
    dy1_max = np.amax(df["y"].to_numpy() - df["by"].to_numpy())
    dx2_max = np.amax(df["bx"].to_numpy() + df["bw"].to_numpy() - df["x"].to_numpy())
    dy2_max = np.amax(df["by"].to_numpy() + df["bh"].to_numpy() - df["y"].to_numpy())
    w = np.ceil(2 * np.amax([dx1_max, dx2_max])).astype(np.uint64)
    h = np.ceil(2 * np.amax([dy1_max, dy2_max])).astype(np.uint64)

    frames = np.unique(df["frame"].to_numpy())
    pb = icaps.ProgressBar(len(frames), "Progress")
    for frame in frames:
        df_ = df[df["frame"] == frame]
        img = icaps.load_img(in_path + icaps.generate_file(frame))
        label = df_["label"].to_numpy()[0]
        bbox = df_[["bx", "by", "bw", "bh"]].to_numpy()[0]
        # if bbox[0] == 0 or bbox[1] == 0 or bbox[2] == 1024 or bbox[3] == 1024:
        #     continue
        part_img = icaps.crop(img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True, copy_=True)
        # part_img = icaps.threshold(part_img, thresh=3, replace=(0, None))
        contour = cv2.findContours(part_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0][0]

        mask = np.zeros(part_img.shape, np.uint8)
        mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
        mask = icaps.fill_holes(mask)
        part_img[mask == 0] = 0

        canvas = np.zeros((h, w), dtype=np.uint8)
        x = int(np.round(w / 2 - (df_["x"].to_numpy()[0] - bbox[0])))
        y = int(np.round(h / 2 - (df_["y"].to_numpy()[0] - bbox[1])))
        canvas[y:y + bbox[3], x:x + bbox[2]] = part_img
        canvas = icaps.enlarge_pixels(canvas, enlarge)
        contour = np.array([np.array([x, y]) + c for c in contour])
        # canvas = cv2.drawContours(canvas, [contour * enlarge], contourIdx=0, color=255)
        # icaps.mk_folder(out_path + "{:0>6}".format(particle) + "/")
        icaps.save_img(canvas, out_path + "{:0>6}".format(particle) + "_{:0>6}".format(frame) + ".bmp")
        pb.tick()
    pb.finish()


def track_limbs(df, in_path, out_path, csv_out_path, threshold, enlarge=1, method=0):
    icaps.mk_folder(out_path, clear=True)
    frames = np.unique(df["frame"].to_numpy())
    particle = np.unique(df["particle"].to_numpy())[0]
    df_limbs = pd.DataFrame(columns=["frame", "cx", "cy", "limbx", "limby", "ellipse_x", "ellipse_y", "semi_minor_axis",
                                     "semi_major_axis", "ellipse_angle"])
    pb = icaps.ProgressBar(len(frames), "Tracking Limbs")
    for frame in frames:
        file = "{:0>6}".format(particle) + "_" + icaps.generate_file(frame, prefix=None)
        orig_img = icaps.load_img(in_path + file)
        orig_img = icaps.threshold(orig_img, thresh=10, replace=(0, None))
        img = icaps.threshold(orig_img, thresh=threshold, replace=(0, None))
        contour = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0][0]
        contour = np.squeeze(contour)
        try:
            ellipse_box = cv2.fitEllipse(contour)
        except cv2.error:
            pb.tick()
            continue
        ellipse_box_large = (np.array(ellipse_box[0]) * enlarge,
                             np.array(ellipse_box[1]) * enlarge,
                             np.array(ellipse_box[2]))

        centers_i = None
        intersect = None
        labels_i = None
        if method == 0:
            intersect_img = np.zeros(np.array(img.shape) * enlarge, dtype=np.uint8)
            intersect_img = icaps.draw_ellipse(intersect_img, ellipse_box_large, 100, thickness=2)
            contour_img = np.zeros(np.array(img.shape) * enlarge, dtype=np.uint8)
            contour_img = cv2.drawContours(contour_img, [contour * enlarge], contourIdx=0, color=100, thickness=1)
            intersect_img = intersect_img + contour_img
            intersect = np.argwhere(intersect_img == 200)
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.)
            flags = cv2.KMEANS_RANDOM_CENTERS
            compactness_i, labels_i, centers_i = cv2.kmeans(intersect.astype(np.float32), 3, None, criteria, 10, flags)
        elif method == 1:
            intersect_img = np.zeros(np.array(img.shape) * enlarge, dtype=np.uint8)
            intersect_img = icaps.draw_ellipse(intersect_img, ellipse_box_large, 255, thickness=2)
            intersect_img = icaps.fill_holes(intersect_img)
            contour_img = np.zeros(np.array(img.shape) * enlarge, dtype=np.uint8)
            contour_img = cv2.drawContours(contour_img, [contour * enlarge], contourIdx=0, color=255, thickness=1)
            contour_img = icaps.fill_holes(contour_img)
            intersect_img = contour_img - intersect_img
            intersect_img = icaps.threshold(intersect_img, thresh=0, replace=(None, None), denoise=True)
            img_large = icaps.enlarge_pixels(orig_img, enlarge)
            intersect_img[img_large < 50] = 0
            intersect = np.argwhere(intersect_img == 255)
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.)
            flags = cv2.KMEANS_RANDOM_CENTERS
            compactness_i, labels_i, centers_i = cv2.kmeans(intersect.astype(np.float32), 3, None, criteria, 10, flags)

        mask = np.zeros(img.shape, np.uint8)
        mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
        mask = icaps.fill_holes(mask)
        img[mask == 0] = 0

        max_pos = np.argwhere(img >= np.amax(img)-10)
        if max_pos.shape[0] != 1:
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.)
            flags = cv2.KMEANS_RANDOM_CENTERS
            _, _, centers_c = cv2.kmeans(max_pos.astype(np.float32), 1, None, criteria, 10, flags)
        else:
            centers_c = [max_pos]

        df_limbs = df_limbs.append(pd.DataFrame(data={"frame": [frame] * 3,
                                                      "cx": [centers_c[0][1]] * 3,
                                                      "cy": [centers_c[0][0]] * 3,
                                                      "limbx": [centers_i[0][1]/enlarge, centers_i[1][1]/enlarge,
                                                                centers_i[2][1]/enlarge],
                                                      "limby": [centers_i[0][0]/enlarge, centers_i[1][0]/enlarge,
                                                                centers_i[2][0]/enlarge],
                                                      "ellipse_x": [ellipse_box[0][0]] * 3,
                                                      "ellipse_y": [ellipse_box[0][1]] * 3,
                                                      "semi_minor_axis": [ellipse_box[1][0]] * 3,
                                                      "semi_major_axis": [ellipse_box[1][1]] * 3,
                                                      "ellipse_angle": [ellipse_box[2]] * 3}))

        canvas = icaps.enlarge_pixels(orig_img, enlarge)
        canvas = cv2.cvtColor(canvas, cv2.COLOR_GRAY2BGR)
        canvas = cv2.drawContours(canvas, [contour * enlarge], contourIdx=0, color=(255, 0, 0))
        canvas = icaps.draw_ellipse(canvas, ellipse_box_large, color=(0, 0, 255))
        canvas = icaps.draw_cross(canvas, np.flip(np.around(centers_c[0]).astype(int)) * enlarge,
                                  color=(255, 255, 255), size=2, thickness=1)
        centers_c = np.flip(np.around(centers_c[0]).astype(int)) * enlarge
        for i, c in enumerate(centers_i):
            if method == 0:
                canvas[tuple(np.transpose(intersect[labels_i.ravel() == i]))] = (0, 255, 0)
            c = np.flip(np.around(c).astype(int))
            canvas = icaps.draw_cross(canvas, c, 3, (0, 255, 0), 1)
            cv2.line(canvas, (centers_c[0], centers_c[1]), (c[0], c[1]), color=(0, 255, 0), thickness=1)
        icaps.cvshow(canvas, t=1)
        icaps.save_img(canvas, out_path + file)
        pb.tick()
    pb.finish()
    df_limbs.to_csv(csv_out_path, index=False)


def init_project():
    icaps.mk_folder(mark_path)
    icaps.mk_folder(crop_path)
    icaps.mk_folder(plot_path)
    df_track = pd.read_csv(tracks_all_path)
    df_track = df_track[df_track["particle"] == part_id]
    df_track.to_csv(track_path, index=False)


def drop_duplicates(df, frames, ids, nids=2):
    if nids == 1:
        ids = (ids, ids)
    to_drop = []
    for frame in frames:
        df_01_ = df[df["frame"] == frame]
        if df_01_["limb"].shape[0] > nids:
            to_drop.append(frame)
    to_drop_idx = []
    for frame in to_drop:
        df_ = df[df["frame"] == frame]
        here_idx = df_.index.to_numpy()
        here_ids = df_["limb"].to_numpy()
        here_id_0 = np.intersect1d(here_ids, ids[0])
        here_id_1 = np.intersect1d(here_ids, ids[1])
        for i in range(len(here_ids)):
            if (here_ids[i] != np.amin(here_id_0)) and (here_ids[i] != np.amin(here_id_1)):
                to_drop_idx.append(here_idx[i])
    return df.drop(index=to_drop_idx)


def calc_limbs(df, center="com", shape=None):
    df_0 = df[np.in1d(df["limb"], id_0)].copy()
    df_1 = df[np.in1d(df["limb"], id_1)].copy()
    df_2 = df[np.in1d(df["limb"], id_2)].copy()
    frames_0 = np.unique(df_0["frame"].to_numpy())
    frames_1 = np.unique(df_1["frame"].to_numpy())
    frames_2 = np.unique(df_2["frame"].to_numpy())
    df_0 = drop_duplicates(df_0, frames_0, id_0, nids=1)
    df_1 = drop_duplicates(df_1, frames_1, id_1, nids=1)
    df_2 = drop_duplicates(df_2, frames_2, id_2, nids=1)
    limb_0, limb_1, limb_2 = None, None, None
    if center == "cross":
        limb_0 = np.transpose(np.array([df_0["limbx"].to_numpy(), df_0["limby"].to_numpy()])) -\
                 np.transpose(np.array([df_0["cx"].to_numpy(), df_0["cy"].to_numpy()]))
        limb_1 = np.transpose(np.array([df_1["limbx"].to_numpy(), df_1["limby"].to_numpy()])) -\
                 np.transpose(np.array([df_1["cx"].to_numpy(), df_1["cy"].to_numpy()]))
        limb_2 = np.transpose(np.array([df_2["limbx"].to_numpy(), df_2["limby"].to_numpy()])) -\
                 np.transpose(np.array([df_2["cx"].to_numpy(), df_2["cy"].to_numpy()]))
    elif center == "com":
        h, w = shape[:2]
        limb_0 = np.transpose(np.array([df_0["limbx"].to_numpy(), df_0["limby"].to_numpy()])) -\
                 np.array((int(w/2), int(w/2)))
        limb_1 = np.transpose(np.array([df_1["limbx"].to_numpy(), df_1["limby"].to_numpy()])) - \
                 np.array((int(w / 2), int(w / 2)))
        limb_2 = np.transpose(np.array([df_2["limbx"].to_numpy(), df_2["limby"].to_numpy()])) - \
                 np.array((int(w / 2), int(w / 2)))

    frames_01 = np.intersect1d(frames_0, frames_1)
    frames_12 = np.intersect1d(frames_1, frames_2)
    frames_20 = np.intersect1d(frames_2, frames_0)
    df_01 = df[np.in1d(df["frame"], frames_01)].copy()
    df_01 = df_01[np.in1d(df_01["limb"], id_0 + id_1)]
    df_01 = drop_duplicates(df_01, frames_01, (id_0, id_1))
    df_12 = df[np.in1d(df["frame"], frames_12)].copy()
    df_12 = df_12[np.in1d(df_12["limb"], id_1 + id_2)]
    df_12 = drop_duplicates(df_12, frames_12, (id_1, id_2))
    df_20 = df[np.in1d(df["frame"], frames_20)].copy()
    df_20 = df_20[np.in1d(df_20["limb"], id_2 + id_0)]
    df_20 = drop_duplicates(df_20, frames_20, (id_2, id_0))

    limb_01 = np.transpose(np.array([df_01[np.in1d(df_01["limb"], id_0)]["limbx"].to_numpy(),
                                     df_01[np.in1d(df_01["limb"], id_0)]["limby"].to_numpy()]))\
            - np.transpose(np.array([df_01[np.in1d(df_01["limb"], id_0)]["cx"].to_numpy(),
                                     df_01[np.in1d(df_01["limb"], id_0)]["cy"].to_numpy()]))
    limb_10 = np.transpose(np.array([df_01[np.in1d(df_01["limb"], id_1)]["limbx"].to_numpy(),
                                     df_01[np.in1d(df_01["limb"], id_1)]["limby"].to_numpy()])) \
              - np.transpose(np.array([df_01[np.in1d(df_01["limb"], id_1)]["cx"].to_numpy(),
                                       df_01[np.in1d(df_01["limb"], id_1)]["cy"].to_numpy()]))
    limb_12 = np.transpose(np.array([df_12[np.in1d(df_12["limb"], id_1)]["limbx"].to_numpy(),
                                     df_12[np.in1d(df_12["limb"], id_1)]["limby"].to_numpy()])) \
              - np.transpose(np.array([df_12[np.in1d(df_12["limb"], id_1)]["cx"].to_numpy(),
                                       df_12[np.in1d(df_12["limb"], id_1)]["cy"].to_numpy()]))
    limb_21 = np.transpose(np.array([df_12[np.in1d(df_12["limb"], id_2)]["limbx"].to_numpy(),
                                     df_12[np.in1d(df_12["limb"], id_2)]["limby"].to_numpy()])) \
              - np.transpose(np.array([df_12[np.in1d(df_12["limb"], id_2)]["cx"].to_numpy(),
                                       df_12[np.in1d(df_12["limb"], id_2)]["cy"].to_numpy()]))
    limb_20 = np.transpose(np.array([df_20[np.in1d(df_20["limb"], id_2)]["limbx"].to_numpy(),
                                     df_20[np.in1d(df_20["limb"], id_2)]["limby"].to_numpy()])) \
              - np.transpose(np.array([df_20[np.in1d(df_20["limb"], id_2)]["cx"].to_numpy(),
                                       df_20[np.in1d(df_20["limb"], id_2)]["cy"].to_numpy()]))
    limb_02 = np.transpose(np.array([df_20[np.in1d(df_20["limb"], id_0)]["limbx"].to_numpy(),
                                     df_20[np.in1d(df_20["limb"], id_0)]["limby"].to_numpy()])) \
              - np.transpose(np.array([df_20[np.in1d(df_20["limb"], id_0)]["cx"].to_numpy(),
                                       df_20[np.in1d(df_20["limb"], id_0)]["cy"].to_numpy()]))

    angle_01 = np.arccos(np.sum(limb_01 * limb_10, axis=1)/
                         (np.linalg.norm(limb_01, axis=1) * np.linalg.norm(limb_10, axis=1)))
    angle_12 = np.arccos(np.sum(limb_12 * limb_21, axis=1)/
                         (np.linalg.norm(limb_12, axis=1) * np.linalg.norm(limb_21, axis=1)))
    angle_20 = np.arccos(np.sum(limb_20 * limb_02, axis=1)/
                         (np.linalg.norm(limb_20, axis=1) * np.linalg.norm(limb_02, axis=1)))

    df_limbs = pd.DataFrame(data={"frame": np.unique(np.hstack((frames_0, frames_1, frames_2)).ravel())},
                            columns=["frame", "limb_0x", "limb_0y", "limb_1x", "limb_1y", "limb_2x", "limb_2y",
                                     "angle_01", "angle_12", "angle_20"])
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_0), "limb_0x"] = np.transpose(limb_0)[0]
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_0), "limb_0y"] = np.transpose(limb_0)[1]
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_1), "limb_1x"] = np.transpose(limb_1)[0]
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_1), "limb_1y"] = np.transpose(limb_1)[1]
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_2), "limb_2x"] = np.transpose(limb_2)[0]
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_2), "limb_2y"] = np.transpose(limb_2)[1]
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_01), "angle_01"] = angle_01
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_12), "angle_12"] = angle_12
    df_limbs.loc[np.in1d(df_limbs["frame"], frames_20), "angle_20"] = angle_20
    for column in df_limbs.columns:
        df_limbs[column] = pd.to_numeric(df_limbs[column])
    return df_limbs


def calc_axes(df, plane_frame, w_z_offset=90):
    time = df["frame"].to_numpy() - np.amin(df["frame"].to_numpy())
    limb_0 = np.transpose(np.array((df["limb_0x"].dropna(), df["limb_0y"].dropna())))
    limb_1 = np.transpose(np.array((df["limb_1x"].dropna(), df["limb_1y"].dropna())))
    limb_2 = np.transpose(np.array((df["limb_2x"].dropna(), df["limb_2y"].dropna())))
    limb = [limb_0, limb_1, limb_2]
    df_plane = df[df["frame"] == plane_frame]
    limb_plane_0 = np.array((df_plane["limb_0x"].to_numpy()[0], df_plane["limb_0y"].to_numpy()[0]))
    limb_plane_1 = np.array((df_plane["limb_1x"].to_numpy()[0], df_plane["limb_1y"].to_numpy()[0]))
    limb_plane_2 = np.array((df_plane["limb_2x"].to_numpy()[0], df_plane["limb_2y"].to_numpy()[0]))
    limb_plane = [limb_plane_0, limb_plane_1, limb_plane_2]
    n_plane = []
    q = np.zeros(3)
    for i in range(3):
        i_b = i + 1 if i + 1 < len(limb_plane) else i - len(limb_plane) + 1
        i_c = i + 2 if i + 2 < len(limb_plane) else i - len(limb_plane) + 2
        q[i] = np.sum(limb_plane[i] * limb_plane[i_c]) / np.sum(limb_plane[i] * limb_plane[i_b])
        n_plane.append(limb_plane[i_c] - q[i] * limb_plane[i_b])
    df["w_x0"] = np.nan
    df["w_x1"] = np.nan
    df["w_x2"] = np.nan
    df["w_y0"] = np.nan
    df["w_y1"] = np.nan
    df["w_y2"] = np.nan
    df["w_z0"] = np.nan
    df["w_z1"] = np.nan
    df["w_z2"] = np.nan

    w_z_offset = np.radians(-w_z_offset)
    w_z_rot = np.array(((np.cos(w_z_offset), -np.sin(w_z_offset)), (np.sin(w_z_offset), np.cos(w_z_offset))))

    _, ax = plt.subplots(figsize=(5, 5))
    ax.set_xlim(-40, 40)
    ax.set_ylim(-40, 40)
    ax.grid()
    ax.set_axisbelow(True)
    ax.invert_yaxis()

    colors = ["C0", "C1", "C2"]
    for i in range(3):
        w_z_base = np.tile(np.dot(w_z_rot, limb_plane[i]), (limb[i].shape[0], 1))
        w_z = np.arccos(np.sum(limb[i] * w_z_base, axis=1) /
                        (np.linalg.norm(limb[i], axis=1) * np.linalg.norm(w_z_base, axis=1)))
        print("Loss", "z" + str(i), np.where(np.isnan(w_z))[0].shape[0])
        df.loc[df["limb_{}x".format(i)].dropna().index, "w_z" + str(i)] = np.degrees(w_z + w_z_offset)

        w_x = np.arccos(np.linalg.norm(limb[i], axis=1)/np.linalg.norm(limb_plane[i]))
        print("Loss", "x" + str(i), np.where(np.isnan(w_x))[0].shape[0])
        df.loc[df["limb_{}x".format(i)].dropna().index, "w_x" + str(i)] = np.degrees(w_x)

        i_b = i + 1 if i + 1 < len(limb_plane) else i - len(limb_plane) + 1
        i_c = i + 2 if i + 2 < len(limb_plane) else i - len(limb_plane) + 2
        idcs_b = df["limb_{}x".format(i_b)].dropna().index
        idcs_c = df["limb_{}x".format(i_c)].dropna().index
        idcs_bc = np.intersect1d(idcs_b, idcs_c)
        df_bc = df.iloc[idcs_bc]
        limb_b = np.transpose(np.array((df_bc["limb_{}x".format(i_b)], df_bc["limb_{}y".format(i_b)])))
        limb_c = np.transpose(np.array((df_bc["limb_{}x".format(i_c)], df_bc["limb_{}y".format(i_c)])))
        n = limb_c - q[i] * limb_b
        w_y = np.arccos(np.linalg.norm(n, axis=1) / np.linalg.norm(n_plane[i]))
        print("Loss", "y" + str(i), np.where(np.isnan(w_y))[0].shape[0])
        df.loc[idcs_bc, "w_y" + str(i)] = np.degrees(w_y)

        ax.scatter(np.transpose(limb[i])[0], np.transpose(limb[i])[1], s=1, color=colors[i], alpha=0.3)
        ax.plot((0, limb_plane[i][0]), (0, limb_plane[i][1]), color=colors[i], linestyle="--")
        ax.plot((0, n_plane[i][0]), (0, n_plane[i][1]), color=colors[i], linestyle="-.")
        ax.plot((0, w_z_base[0][0]), (0, w_z_base[0][1]), color=colors[i], linewidth=2)

    plt.savefig(plot_path + "axes.png")
    plt.close()

    # print(df.to_string())

    _, ax = plt.subplots()
    ax.set_xlabel("time in ms")
    ax.set_ylabel("angle around {}-axis in °".format("z"))
    ax.scatter(time, df["w_z0"], s=1, color=colors[0], label="z0")
    ax.scatter(time, df["w_z1"], s=1, color=colors[1], label="z1")
    ax.scatter(time, df["w_z2"], s=1, color=colors[2], label="z2")
    ax.legend()
    plt.savefig(plot_path + "wz.png")
    plt.close()

    _, ax = plt.subplots()
    ax.set_xlabel("time in ms")
    ax.set_ylabel("angle around {}-axis in °".format("x"))
    ax.scatter(time, df["w_x0"], s=1, color=colors[0], label="x0")
    ax.scatter(time, df["w_x1"], s=1, color=colors[1], label="x1")
    ax.scatter(time, df["w_x2"], s=1, color=colors[2], label="x2")
    ax.legend()
    plt.savefig(plot_path + "wx.png")
    plt.close()

    _, ax = plt.subplots()
    ax.set_xlabel("time in ms")
    ax.set_ylabel("angle around {}-axis in °".format("y"))
    ax.scatter(time, df["w_y0"], s=1, color=colors[0], label="y0")
    ax.scatter(time, df["w_y1"], s=1, color=colors[1], label="y1")
    ax.scatter(time, df["w_y2"], s=1, color=colors[2], label="y2")
    ax.legend()
    plt.savefig(plot_path + "wy.png")
    plt.close()

    return df


def plot_general(df):
    time = df["frame"].to_numpy() - np.amin(df["frame"].to_numpy())
    limb_0 = np.transpose(np.array((df["limb_0x"].dropna(), df["limb_0y"].dropna())))
    limb_1 = np.transpose(np.array((df["limb_1x"].dropna(), df["limb_1y"].dropna())))
    limb_2 = np.transpose(np.array((df["limb_2x"].dropna(), df["limb_2y"].dropna())))
    angle_01 = df["angle_01"].to_numpy()
    angle_12 = df["angle_12"].to_numpy()
    angle_20 = df["angle_20"].to_numpy()

    _, ax = plt.subplots()
    ax.grid()
    ax.set_axisbelow(True)
    ax.scatter(time[df["limb_0x"].dropna().index], np.linalg.norm(limb_0, axis=1), color="blue", s=1, label="0x")
    ax.scatter(time[df["limb_1x"].dropna().index], np.linalg.norm(limb_1, axis=1) + 50, color="green", s=1, label="1x")
    ax.scatter(time[df["limb_2x"].dropna().index], np.linalg.norm(limb_2, axis=1) + 50 * 2, color="red", s=1, label="2x")
    ax.set_xlabel("time in ms")
    ax.set_ylabel("vector length in µm")
    ax.legend()
    plt.savefig(plot_path + "limb_x_length.png")
    plt.close()

    _, ax = plt.subplots()
    ax.grid()
    ax.set_axisbelow(True)
    ax.scatter(time, np.degrees(angle_01), color="blue", s=1, label="01")
    ax.scatter(time, np.degrees(angle_12)+180, color="green", s=1, label="12")
    ax.scatter(time, np.degrees(angle_20)+180*2, color="red", s=1, label="20")
    ax.set_xlabel("time in ms")
    ax.set_ylabel("angle in °")
    ax.legend()
    plt.savefig(plot_path + "limb_angle.png")
    plt.close()


def mark_track(df, in_path, out_path, enlarge=1, threshold=0, center="com"):
    icaps.mk_folder(out_path)
    frames = np.unique(df["frame"].to_numpy())
    for frame in frames:
        file = "{:0>6}".format(435818) + "_" + icaps.generate_file(frame, prefix=None)
        df_ = df[df["frame"] == frame]

        img = icaps.load_img(in_path + file)
        img = icaps.threshold(img, thresh=threshold, replace=(0, None))
        contour = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0][0]
        contour = np.squeeze(contour)

        ellipse_box = ([df_["ellipse_x"].to_numpy()[0], df_["ellipse_x"].to_numpy()[0]],
                       [df_["semi_minor_axis"].to_numpy()[0], df_["semi_major_axis"].to_numpy()[0]],
                       df_["ellipse_angle"].to_numpy()[0])
        ellipse_box_large = (np.array(ellipse_box[0]) * enlarge,
                             np.array(ellipse_box[1]) * enlarge,
                             np.array(ellipse_box[2]))
        c_pos = (0, 0)
        if center == "cross":
            c_pos = (np.round(df_["cx"].to_numpy()[0]*enlarge).astype(int),
                     np.round(df_["cy"].to_numpy()[0]*enlarge).astype(int))
        elif center == "com":
            h, w = img.shape[:2]
            c_pos = (int(w/2*enlarge), int(h/2*enlarge))

        canvas = icaps.enlarge_pixels(img, enlarge)
        canvas = cv2.cvtColor(canvas, cv2.COLOR_GRAY2BGR)
        canvas = cv2.drawContours(canvas, [contour * enlarge], contourIdx=0, color=(255, 0, 0))
        canvas = icaps.draw_ellipse(canvas, ellipse_box_large, color=(0, 0, 255))
        canvas = icaps.draw_cross(canvas, c_pos, color=(255, 255, 255), size=2, thickness=1)

        limbs = df_["limb"].to_numpy()
        for limb in limbs:
            df_l = df_[df_["limb"] == limb]
            x = df_l["limbx"].to_numpy()[0]
            y = df_l["limby"].to_numpy()[0]
            pos = np.array((np.round(x * enlarge).astype(int), np.round(y * enlarge).astype(int)))
            canvas = icaps.draw_cross(canvas, pos, 3, (0, 255, 0), 1)
            canvas = icaps.draw_label(canvas, limb, pos - 5, color=(0, 255, 0))
            cv2.line(canvas, c_pos, tuple(pos), color=(0, 255, 0), thickness=1)

        icaps.cvshow(canvas, t=1)
        icaps.save_img(canvas, out_path + file)


def get_plane_frame(df):
    limb_0 = np.transpose(np.array((df["limb_0x"].fillna(0), df["limb_0y"].fillna(0))))
    limb_1 = np.transpose(np.array((df["limb_1x"].fillna(0), df["limb_1y"].fillna(0))))
    limb_2 = np.transpose(np.array((df["limb_2x"].fillna(0), df["limb_2y"].fillna(0))))
    limb_0_length = np.linalg.norm(limb_0, axis=1)
    limb_1_length = np.linalg.norm(limb_1, axis=1)
    limb_2_length = np.linalg.norm(limb_2, axis=1)
    s = np.sum(np.array((limb_0_length, limb_1_length, limb_2_length)), axis=0)
    i = np.argmax(s)
    return df["frame"][i]


def line_intersect(a1, a2, b1, b2):
    s = np.vstack([a1, a2, b1, b2])
    h = np.hstack((s, np.ones((4, 1))))
    l1 = np.cross(h[0], h[1])
    l2 = np.cross(h[2], h[3])
    x, y, z = np.cross(l1, l2)
    if z == 0:
        return float('inf'), float('inf')
    return x/z, y/z


def extend_line(p1, p2, w, h):
    if not p1[0] == 0:
        p1_ = p1 * -w / 2 / p1[0] + p2
        p2_ = p1 * w / 2 / p1[0] + p2
    else:
        p1_ = p1 * -h / 2 / p1[1] + p2
        p2_ = p1 * h / 2 / p1[1] + p2
    return p1_, p2_


def calc_inertia(df, plane_frame, in_path, threshold=0, debug=False):
    df_plane = df[df["frame"] == plane_frame]
    limb_plane_0 = np.array((df_plane["limb_0x"].to_numpy()[0], df_plane["limb_0y"].to_numpy()[0]))
    limb_plane_1 = np.array((df_plane["limb_1x"].to_numpy()[0], df_plane["limb_1y"].to_numpy()[0]))
    limb_plane_2 = np.array((df_plane["limb_2x"].to_numpy()[0], df_plane["limb_2y"].to_numpy()[0]))
    limb_plane = [limb_plane_0, limb_plane_1, limb_plane_2]
    img = icaps.load_img(in_path + "{:0>6}_".format(part_id) + icaps.generate_file(plane_frame, prefix=None))
    img = icaps.threshold(img, thresh=threshold)
    h, w = img.shape[:2]
    center = np.array((w / 2, h / 2))
    inertia = np.zeros((3, 3))
    for i in range(3):
        i_b = i + 1 if i + 1 < len(limb_plane) else i - len(limb_plane) + 1
        i_c = i + 2 if i + 2 < len(limb_plane) else i - len(limb_plane) + 2
        q = np.sum(limb_plane[i] * limb_plane[i_c]) / np.sum(limb_plane[i] * limb_plane[i_b])
        n_plane = limb_plane[i_c] - q * limb_plane[i_b]
        for px in np.transpose(np.nonzero(img)):
            m = img[px[0], px[1]]
            r_z = np.linalg.norm(np.flip(px) - center)
            r_x = np.linalg.norm(np.cross(limb_plane[i], center - np.flip(px))) / np.linalg.norm(limb_plane[i])
            r_y = np.linalg.norm(np.cross(n_plane, center - np.flip(px))) / np.linalg.norm(n_plane)
            inertia[i][0] = m * r_x ** 2
            inertia[i][1] = m * r_y ** 2
            inertia[i][2] = m * r_z ** 2

        if debug:
            print(inertia[i])
            img_ = icaps.enlarge_pixels(img, 8)
            img_ = icaps.draw_cross(img_, (center * 8).astype(int), color=(0, 255, 0), size=3, thickness=2)
            h, w = img_.shape[:2]
            p1, p2 = extend_line(n_plane * 8, center * 8, w, h)
            cv2.line(img_, tuple(p1.astype(int)), tuple(p2.astype(int)), color=(0, 0, 255))
            p1, p2 = extend_line(limb_plane[i] * 8, center * 8, w, h)
            cv2.line(img_, tuple(p1.astype(int)), tuple(p2.astype(int)), color=(255, 0, 0))
            icaps.save_img(img_, plot_path + "debug_" + str(i) + ".png")
            icaps.cvshow(img_)

    return inertia


def main():
    df_track = pd.read_csv(track_path)
    # crop_particle(df, part_id, in_path=ffc_path, out_path=crop_path)
    df_track = df_track[df_track["frame"] >= frame_interval[0]]
    df_track = df_track[df_track["frame"] <= frame_interval[1]]
    # track_limbs(df_track, threshold=thresh, in_path=crop_path, out_path=mark_path, csv_out_path=limb_track_path,
    #             enlarge=enlarge_mark, method=1)
    # icaps.mk_gif(mark_path + "[::5].bmp", particle_path + "movie.gif", duration=0.04, loop=False)

    # df_limb_track = pd.read_csv(limb_track_path)
    # df_limb_track = tp.link_df(df_limb_track, search_range=3, pos_columns=["limbx", "limby"], memory=100)
    # df_limb_track = df_limb_track.rename(columns={"particle": "limb"})
    # df_limb_track.to_csv(limb_track_id_path, index=False)

    # df_limb_track = pd.read_csv(limb_track_id_path)
    # mark_track(df=df_limb_track, in_path=crop_path, out_path=track_mark_path, enlarge=enlarge_mark, threshold=thresh,
    #            center="com")
    # df_limb_link = calc_limbs(df_limb_track, center="com", shape=(79, 88))
    # df_limb_link.to_csv(limb_link_path, index=False)

    # df_limb_link = pd.read_csv(limb_link_path)
    # p_frame = get_plane_frame(df_limb_link)
    # print(p_frame, 18562)
    # plot_general(df=df_limb_link)
    # df_axes = calc_axes(df_limb_link, plane_frame=18562)
    # df_axes.to_csv(axes_path, index=False)

    # icaps.mk_gif(track_mark_path + "[::5].bmp", particle_path + "movie_track.gif", duration=0.04, loop=False)

    dts = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    axis_labels = np.array(["x", "y", "z"])
    systems = np.array([0, 1, 2])

    # df_axes = pd.read_csv(axes_path)
    # pb = icaps.ProgressBar(len(systems) * len(axis_labels) * len(min_dts), "Calculating Velocities")
    # for s in systems:
    #     for a in axis_labels:
    #         for dt in dts:
    #             df_axes = icaps.calc_velocities(df_axes, min_dt=min_dt,
    #                                             in_columns=["w_{}{}".format(a, s)],
    #                                             out_columns=["dw_{}{}_{}".format(a, s, dt)],
    #                                             id_column=None, approx_large_dt=False, norm_dt=True)
    #             pb.tick()
    # pb.finish()
    # df_axes.to_csv(particle_path + "axes.csv", index=False)

    df_axes = pd.read_csv(axes_path)
    dump_path = plot_path + "dw_dump/"
    icaps.mk_folder(dump_path)
    drift_rot = np.zeros((3, 3, dts.shape[0]))
    sigma_rot = np.zeros((3, 3, dts.shape[0]))

    pb = icaps.ProgressBar(len(dts) * len(axis_labels), "Plotting Erf-Fits")
    for dt in dts:
        for ai, a in enumerate(axis_labels):
            _, ax = plt.subplots(nrows=3)
            xmin = 0
            xmax = 0
            for s in systems:
                dw = df_axes["dw_{}{}_{}".format(a, s, dt)].to_numpy()
                dw = dw[~np.isnan(dw)]
                bins = int(len(dw))
                n, bins = np.histogram(dw, bins, density=True)
                ff = np.cumsum(n) / np.max(np.cumsum(n))
                ax[s].scatter(bins[:-1], ff, s=1, c="black")
                popt, pcov = curve_fit(error_func, bins[:-1], ff)
                ax[s].plot(bins[:-1], error_func(bins[:-1], *popt), color="red", lw=1,
                           label="Rot-Drift: {:.2f}µm, Sigma: {:.2f}µm".format(popt[1], popt[0]))
                ax[s].set_ylabel("Normalized")
                ax[s].legend(loc="upper left")
                xmin = np.min([np.min(dw), xmin])
                xmax = np.max([np.max(dw), xmax])
                drift_rot[ai, s, np.where(dts == dt)] = popt[1]
                sigma_rot[ai, s, np.where(dts == dt)] = popt[0]
            ax[-1].set_xlabel(r"$\Delta\vartheta$ in °")
            for s in systems:
                ax[s].set_xlim(xmin, xmax)
            plt.savefig(dump_path + "erf_{}_{}.png".format(a, dt), dpi=600)
            plt.close()
            pb.tick()
    pb.finish()

    pb = icaps.ProgressBar(len(axis_labels), "Plotting Sigmas")
    for ai, a in enumerate(axis_labels):
        _, ax = plt.subplots(nrows=3)
        ymax = 0
        for s in systems:
            sigma_squared = sigma_rot[ai, s]**2
            ax[s].scatter(dts, sigma_squared, s=1, c="black",
                          # label=r"$\sigma_{rot}^2$"
                          )
            popt, pcov = curve_fit(small_dt_lin, dts, sigma_squared)
            perr = np.sqrt(np.diag(pcov))
            ax[s].plot(dts, small_dt_lin(dts, *popt), color="blue", linestyle="--", lw=1,
                       label="a = {:.2f} \u00b1 {:.2f}".format(np.round(popt[0], 2), np.round(perr[0], 2))
                       )
            # r"Fit: $\sigma^2$ = a $\cdot$ dt"
            popt, pcov = curve_fit(small_dt_t, dts, sigma_squared)
            perr = np.sqrt(np.diag(pcov))
            ax[s].plot(dts, small_dt_t(dts, *popt), color="red", lw=1,
                       label="a = {:.2f} \u00b1 {:.2f}".format(np.round(popt[0], 2), np.round(perr[0], 2)))
            # r"Fit: $\sigma^2$ = a $\cdot$ dt$^2$"
            ax[s].set_ylabel(r"$\sigma_{rot}^2$ in °$^2$")
            ax[s].legend(loc="lower right")
            ymax = np.max([np.max(sigma_squared), ymax, small_dt_lin(dts[-1], *popt), small_dt_t(dts[-1], *popt)])
        for s in systems:
            ax[s].set_ylim(-2, ymax)
        ax[-1].set_xlabel("dt in ms")
        plt.savefig(plot_path + "sigma_{}.png".format(a))
        plt.close()
        pb.tick()
    pb.finish()

    """
    ================
    PLOT (OLD)
    ================
    """

    """
    for a in ["x", "y", "z"]:
        _, ax = plt.subplots(nrows=3)
        for i in range(3):
            ax[i].scatter(df_axes["frame"] - np.amin(df_axes["frame"]), df_axes["dw_{}{}".format(a, i)],
                          s=1, color="black")
            ax[i].set_ylabel(r"$\Omega_{" + "{},{}".format(a, i) + "}$ in °/ms")
            ax[i].set_ylim(-10, 10)
            if i == 2:
                ax[i].set_xlabel("time in ms")
        plt.savefig(plot_path + "dw_{}.png".format(a))
        plt.close()
    for a in ["x", "y", "z"]:
        _, ax = plt.subplots(nrows=3)
        for i in range(3):
            d = np.abs(df_axes["dw_{}{}".format(a, i)])
            d = d[~np.isnan(d)]
            bins = np.arange(-0.5, np.round(np.max(d))+0.5)
            hist, bins = np.histogram(d, bins=bins)
            hist = hist / np.sum(hist)
            # ax[i].hist(d, color="black", bins=bins, stacked=True, density=True)
            ax[i].step(bins[1:], hist, color="black")
            ax[i].set_xlabel(r"$\Omega_{" + "{}".format(a) + "}$ in °/ms")
            ax[i].set_ylabel(r"frequency$_{}$".format(i))
            ax[i].set_ylim(0, 0.5)
            ax[i].set_xlim(-0.5, 15.5)
        plt.savefig(plot_path + "dw_{}_hist.png".format(a))
        plt.close()
    
    """

    inertia = calc_inertia(df_axes, plane_frame=18562, threshold=10, in_path=crop_path, debug=False)
    density = 2.0
    # dts = np.arange(1, 21)
    # dts = np.append(dts, np.arange(25, 55, 5))
    dts = np.arange(1, 11)
    icaps.mk_folder(plot_path + "energy/")
    for min_dt in dts:
        for i in range(3):
            _, ax = plt.subplots(nrows=3)
            for j, a in enumerate(["x", "y", "z"]):
                energy = 1 / 2 * inertia[i][j] * (np.radians(df_axes["dw_{}{}_{}".format(a, i, min_dt)]/min_dt) * 2 * np.pi) ** 2 / (311 / 1.4) * \
                         (density * 4 / 3 * np.pi * 0.75 ** 3) * 10 ** -21
                ax[j].scatter(df_axes["frame"] - np.amin(df_axes["frame"]), energy, s=1, color="black",
                              label="mean=" + "{:.2f}".format(np.round(np.mean(energy * 10**21), 2)) + r"$\,\cdot\, 10^{-21}$ J")
                # ax[j].set_ylabel(r"$E_{}$ in".format(a) + r"$10^{-23}$ J")
                ax[j].set_ylabel(r"$E_{}$ in".format(a) + " J")
                if j == 2:
                    ax[j].set_xlabel("time in ms")
                ax[j].legend(loc="lower right")
                ax[j].set_yscale("log")
                ax[j].set_ylim(10**-28, 10**-18)
            plt.savefig(plot_path + "energy/" + "energy_{}_{}".format(i, min_dt) + ".png")
            plt.close()

    for min_dt in dts:
        for i in range(3):
            _, ax = plt.subplots(nrows=3)
            for j, a in enumerate(["x", "y", "z"]):
                energy = 1/2 * inertia[i][j] * (np.radians(df_axes["dw_{}{}_{}".format(a, i, min_dt)]/min_dt) * 2 * np.pi) ** 2 / (311/1.4) *\
                         (density * 4/3 * np.pi * 0.75 **3) * 10**-21
                # ax[j].scatter(df_axes["frame"] - np.amin(df_axes["frame"]), energy*10**-23, s=1, color="black",
                #               label="mean=" + "{:.2f}".format(np.round(np.mean(energy), 2)) + r"$\,\cdot\, 10^{-23}$ J")
                # ax[j].set_ylabel(r"$E_{}$ in".format(a) + r"$10^{-23}$ J")
                energy = energy[~np.isnan(energy)]
                bins = [0.5*10**-x for x in range(28, 18, -1)]
                hist, bins = np.histogram(energy, bins=bins)
                hist = hist / np.sum(hist)

                # ax[j].hist(energy * 10**-23, color="black", density=True, bins=bins)
                ax[j].step(bins[1:], hist, color="black")
                # ax[j].scatter(bins[:-1], hist, color="black", s=1)
                ax[j].set_ylabel(r"frequency$_{}$".format(a))
                ax[j].set_xlabel(r"$E_{}$ in".format(i) + " J")
                ax[j].set_ylim(0, 0.5)
                ax[j].set_xscale("log")
                # ax[j].legend(loc="lower right")
                # ax[j].set_yscale("log")
                ax[j].set_xlim(0.5*10**-28, 0.5*10**-18)
            plt.savefig(plot_path + "energy/" + "energy_hist_{}_{}".format(i, min_dt) + ".png")
            plt.close()


if __name__ == '__main__':
    # init_project()
    main()


