import icaps
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/PixelGridSim/"
scans = [1, 2, 3, 4, 5, 6]
in_path = [drive_letter + ":/icaps/data/Scan" + str(s) + "/ffc/" for s in scans]
track_path = [drive_letter + ":/icaps/data/Scan" + str(s) + "/T_all.csv" for s in scans]
data_path = project_path + "PixelGridSim.csv"
# particle_path = project_path + "particles/"
plot_path = project_path + "PixelGridSim_MassHist.png"
monomer_intensity = 306
mass_thresh = 50
max_intensity_thresh = 100
grid_size = 12


def main():
    """
    icaps.mk_folder(particle_path, clear=True)
    for s in scans:
        # ============================
        # Strip Tracking Data
        # ============================
        df = pd.read_csv(track_path[s-1])
        df["mass"] = df["mass"]/monomer_intensity
        df = df[df["mass"] >= mass_thresh]
        part_ids = np.unique(df["particle"])
        df_strip = pd.DataFrame(columns=df.columns)
        for part_id in part_ids:
            df_ = df[df["particle"] == part_id]
            df_ = df_[df_["mass"] == np.amax(df_["mass"])]
            df_strip = df_strip.append(df_, ignore_index=True, sort=False)
        df = df_strip.reindex()
        print("Minimum Mass Threshold:\t\t" + str(df.shape[0]))

        # ============================
        # Crop Particles
        # ============================
        count = 0
        frames = np.unique(df["frame"])
        max_part_id = np.amax(df["particle"])
        for frame in frames:
            df_ = df[df["frame"] == frame]
            file = icaps.get_files(in_path[s-1] + "[" + str(frame) + ":" + str(frame+1) + "].bmp")[0][0]
            img = icaps.load_img(in_path[s-1] + file)
            thr_img = icaps.threshold(img, 0)
            for part in df_["particle"]:
                df_p = df_[df_["particle"] == part]
                x = int(df_p["x"].to_numpy()[0])
                y = int(df_p["y"].to_numpy()[0])
                # ============================
                # Locate Nearest Non-Zero
                # ============================
                h, w = thr_img.shape[:2]
                if thr_img[y, x] == 0:
                    search_box = thr_img[np.max([y-100, 0]):np.min([y+100, h]),
                                         np.max([x-100, 0]):np.min([x+100, w])]
                    nonzero = cv2.findNonZero(search_box)
                    distances = np.sqrt((nonzero[:, :, 0] - 100) ** 2 + (nonzero[:, :, 1] - 100) ** 2)
                    nearest_index = np.argmin(distances)
                    nearest_delta = nonzero[nearest_index][0] - np.array([100, 100])
                    x = x + nearest_delta[0]
                    y = y + nearest_delta[1]
                n, res_img, mask, bbox = cv2.floodFill(thr_img,
                                                       mask=np.zeros((img.shape[0]+2, img.shape[1]+2), dtype=np.uint8),
                                                       seedPoint=(x, y), newVal=100)
                part_img = icaps.crop(img, (bbox[0], bbox[1]), (bbox[2], bbox[3]), wh=True)
                part_img = icaps.isolate_particle(part_img, pos=(x-bbox[0], y-bbox[1]))
                if np.amax(part_img) >= max_intensity_thresh:
                    count += 1
                    icaps.save_img(part_img,
                                   particle_path +
                                   str(s) + "_" +
                                   "{num:0>{fill}}".format(fill=len(str(np.amax(frames))), num=frame) +
                                   "_" + "{num:0>{fill}}".format(fill=len(str(max_part_id)), num=part) + ".bmp")
        print("Max Intensity Threshold:\t" + str(count))
    """

    # ============================
    # Threshold
    # ============================
    # pc_path1 = project_path + "particle_collection_1_handfilter/"
    # pc_path2 = project_path + "particle_collection_2_handfilter/"
    # thr = 60
    # thr_path1 = pc_path1[:-1] + "_thresh" + str(thr) + "/"
    # thr_path2 = pc_path2[:-1] + "_thresh" + str(thr) + "/"
    # icaps.mk_folder(thr_path1)
    # icaps.mk_folder(thr_path2)
    # files, _ = icaps.get_files(pc_path1)
    # for file in files:
    #     img = icaps.load_img(pc_path1 + file)
    #     img = icaps.threshold(img, thr, replace=(0, None))
    #     icaps.save_img(img, thr_path1 + file)
    #
    # files, _ = icaps.get_files(pc_path2)
    # for file in files:
    #     img = icaps.load_img(pc_path2 + file)
    #     img = icaps.threshold(img, thr, replace=(0, None))
    #     icaps.save_img(img, thr_path2 + file)

    # ============================
    # Pixel Grid Simulation
    # ============================
    # path = project_path + "particle_collection_2_handfilter_thresh50/"
    # files, _ = icaps.get_files(path)
    # df = pd.DataFrame(columns=["intensity", "x0", "y0", "x_grid", "y_grid", "particle", "frame"])
    # pb = icaps.ProgressBar(grid_size * grid_size * len(files), title="Pixel Grid Simulation")
    # for file in files:
    #     img = icaps.load_img(path + file)
    #     img = cv2.copyMakeBorder(img, left=grid_size, right=grid_size, top=grid_size, bottom=grid_size,
    #                              borderType=cv2.BORDER_CONSTANT, value=0)
    #     frame = int(float(file.split("_")[0]))
    #     part = int(float(file.split("_")[1].split(".")[0]))
    #     h, w = img.shape[:2]
    #     for y0 in range(grid_size):
    #         for x0 in range(grid_size):
    #             ny = int((h - y0) / grid_size)
    #             nx = int((w - x0) / grid_size)
    #             for j in range(ny):
    #                 for i in range(nx):
    #                     y = y0 + j*grid_size
    #                     x = x0 + i*grid_size
    #                     square = img[y:y+grid_size, x:x+grid_size]
    #                     intensity = np.sum(square)
    #                     if intensity != 0:
    #                         df = df.append(pd.DataFrame(data={"intensity": [intensity],
    #                                                           "x0": [x0],
    #                                                           "y0": [y0],
    #                                                           "x_grid": [i],
    #                                                           "y_grid": [j],
    #                                                           "particle": part,
    #                                                           "frame": frame}),
    #                                        ignore_index=True, sort=False)
    #                     # icaps.cvshow(icaps.draw_bbox(img, (x, y, grid_size, grid_size)), t=1)
    #             pb.tick()
    # pb.finish()
    # df.to_csv(data_path, index=False)

    # ============================
    # Combine
    # ============================
    df1 = pd.read_csv(project_path + "PixelGridSim1_thresh50.csv")
    df2 = pd.read_csv(project_path + "PixelGridSim1_thresh50.csv")
    df = df1.append(df2, ignore_index=True, sort=False)
    df.to_csv(data_path, index=False)

    # ============================
    # Plot
    # ============================
    df = pd.read_csv(data_path)
    print(df["particle"].drop_duplicates().to_numpy().size)
    mass = df["intensity"].to_numpy()/monomer_intensity
    min_mass, max_mass = np.amin(mass), np.amax(mass)
    print(min_mass, max_mass)
    hist, bins = np.histogram(mass, bins=np.arange(-0.5, int(max_mass) + 1.5, 1))
    print(bins)
    x = (bins[1:-1]+0.5).astype(int)
    y = hist[1:]/mass.size
    bars = plt.bar(x, y, color="gray", align="center", width=1)
    print(x)
    # plt.gca().set_xlim(left=-0.5)
    mean = np.average(x, weights=y)
    variance = np.average((x-mean)**2, weights=y)
    print(mean, np.sqrt(variance))
    plt.gca().set_xlabel("Mass in Units of Monomer Mass")
    plt.gca().set_ylabel("Frequency")
    # plt.gca().set_yscale("log")
    # plt.savefig(plot_path)
    plt.show()


if __name__ == '__main__':
    main()



