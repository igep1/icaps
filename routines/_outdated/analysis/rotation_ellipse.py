import icaps
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.special import erf
from scipy.stats import chisquare
from shutil import copyfile

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
project_path = drive_letter + ":/icaps/data/LDM_ROT/"
track_tag = "S20L500"
track_path = project_path + "Tracks_P{}.csv".format(track_tag)
particle_path = project_path + "particles/"
contour_path = drive_letter + ":/icaps/data/LDM_ROT/contours_short2/"
mark_path = project_path + "mark/"
plot_collection_path = project_path + "plot_collection/"
table_collection_path = project_path + "table_collection/"


def fit_ellipses(df, particle, in_path, mark_out_path, thresh1=10, thresh2=30, enlarge=1, diashow=False):
    in_path += "{:0>6}/".format(particle)
    mark_out_path += "{:0>6}/".format(particle)
    icaps.mk_folder(mark_out_path)
    df = df[df["particle"] == particle].copy()
    frames = np.unique(df["frame"].to_numpy())
    pb = icaps.ProgressBar(len(frames), "Analysing Rotation")
    df_ellipse = pd.DataFrame(data={"frame": [1], "ellipse_x": [1.], "ellipse_y": [1.], "semi_minor_axis": [1.],
                                    "semi_major_axis": [1.], "ellipse_angle": [1.]})
    df_ellipse.drop(0)
    df_ellipse.reset_index(drop=True)
    for frame in frames:
        file = "{:0>6}".format(particle) + "_" + icaps.generate_file(frame, prefix=None)
        orig_img = icaps.load_img(in_path + file)
        if orig_img is None:
            print("Missing Frame: " + file)
            continue
        orig_img = icaps.threshold(orig_img, thresh=thresh1, replace=(0, None))
        img = icaps.threshold(orig_img, thresh=thresh2, replace=(0, None))
        contour = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        try:
            contour = contour[0][0]
        except IndexError:
            pb.tick()
            continue
        contour = np.squeeze(contour)
        try:
            ellipse_box = cv2.fitEllipse(contour)
        except cv2.error:
            pb.tick()
            continue
        ellipse_box_large = (np.array(ellipse_box[0]) * enlarge,
                             np.array(ellipse_box[1]) * enlarge,
                             np.array(ellipse_box[2]))
        mask = np.zeros(img.shape, np.uint8)
        mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
        mask = icaps.fill_holes(mask)
        img[mask == 0] = 0

        df_ellipse = df_ellipse.append(pd.DataFrame(data={"frame": [frame],
                                                          "ellipse_x": [ellipse_box[0][0]],
                                                          "ellipse_y": [ellipse_box[0][1]],
                                                          "semi_minor_axis": [ellipse_box[1][0]],
                                                          "semi_major_axis": [ellipse_box[1][1]],
                                                          "ellipse_angle": [ellipse_box[2]]}), sort=False)
        canvas = icaps.enlarge_pixels(orig_img, enlarge)
        canvas = cv2.cvtColor(canvas, cv2.COLOR_GRAY2BGR)
        canvas = cv2.drawContours(canvas, [contour * enlarge], contourIdx=0, color=(255, 0, 0))
        canvas = icaps.draw_ellipse(canvas, ellipse_box_large, color=(0, 0, 255))
        if diashow:
            icaps.cvshow(canvas, t=1)
        icaps.save_img(canvas, mark_out_path + file)
        pb.tick()
    pb.finish()
    df = pd.merge(left=df, right=df_ellipse, on=["frame"], how="left")
    return df


def calc_angular_displacement(df, out_path, min_dt=1):
    icaps.mk_folder(out_path + "angular displacement/" + "min_dt_{}/".format(min_dt), clear=True)

    key = "dw_z_" + str(min_dt)
    if key not in df.columns:
        df[key] = np.nan
    particles = np.unique(df["particle"].to_numpy()).astype(np.uint64)
    pb = icaps.ProgressBar(len(particles), "Calculating Angular Displacement with min_dt=" + str(min_dt))
    for particle in particles:
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy()).astype(np.uint64)
        prev_idx = 0
        leaps = []
        for i, frame in enumerate(frames):
            if i == 0:
                continue
            prev_frame = frames[prev_idx]
            prev_angle = df_[df_["frame"] == prev_frame]["ellipse_angle"].to_numpy()[0]
            if np.isnan(prev_angle):
                prev_idx += 1
                continue
            angle = df_[df_["frame"] == frame]["ellipse_angle"].to_numpy()[0]
            if np.isnan(angle):
                continue
            d_angle = angle - prev_angle
            dt = frame - prev_frame
            if dt >= min_dt:
                if np.abs(d_angle) > 90:
                    leaps.append(prev_idx)
                    prev_idx += 1
                else:
                    table_idx = df_[df_["frame"] == prev_frame].index.to_numpy()[0]
                    df.at[table_idx, key] = d_angle / dt * min_dt
                    prev_idx = i

        angular_displacement = df[df["particle"] == particle][key].to_numpy().astype(np.float64)
        angle = df[df["particle"] == particle]["ellipse_angle"].to_numpy()
        leaps = np.array(leaps, dtype=np.uint64)

        plt.close()
        _, axes = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
        ax = axes[0]
        ax.set_axisbelow(True)
        ax.grid()
        ax.set_title(str(particle))
        ax.set_xlabel("Frame")
        ax.set_ylabel("Angle in °")
        idcs = np.argwhere(np.isnan(angular_displacement)).flatten()
        for leap in leaps:
            if leap in idcs:
                idcs = idcs[idcs != leap]
        leap_frames = frames[leaps]
        frames_cut = frames.copy().astype(np.float64)
        frames_cut = np.delete(frames_cut, idcs)
        angle_cut = angle.copy()
        angle_cut = np.delete(angle_cut, idcs)
        for leap_frame in leap_frames:
            idx = np.argwhere(frames_cut == leap_frame)[0]
            frames_cut = np.insert(frames_cut, idx + 1, leap_frame + 0.5)
            angle_cut = np.insert(angle_cut, idx + 1, np.nan)

        ax.plot(frames_cut, angle_cut, c="red", linewidth=1)
        ax.scatter(frames, angle, s=3, c="black")
        xlims = ax.get_xlim()

        ax = axes[1]
        ax.set_axisbelow(True)
        ax.grid()
        ax.set_title("min_dt=" + str(min_dt))
        ax.set_xlabel("Frame")
        ax.set_ylabel("Angular Displacement in °")
        ax.scatter(frames, angular_displacement, s=3, c="black")
        ax.set_ylim(-np.nanmax(np.abs(angular_displacement)), np.nanmax(np.abs(angular_displacement)))
        ax.set_xlim(xlims)
        plt.savefig(
            out_path + "angular displacement/" + "{:0>6}".format(particle) +
            "_{}".format(min_dt) + ".png")
        plt.close()
        pb.tick()
    pb.finish()
    return df


def calc_inertia(img, semi_major_axis, angle, out_path, threshold=30, debug=False):
    img = icaps.threshold(img, threshold)
    center = icaps.measure_com(img)
    inertia = np.zeros(2)
    vec_x = np.array([semi_major_axis * np.cos(np.radians(180 - angle)),
                      semi_major_axis * np.sin(np.radians(180 - angle))])
    for px in np.transpose(np.nonzero(img)):
        m = img[px[0], px[1]]
        r_z = np.linalg.norm(np.flip(px) - center)
        r_x = np.linalg.norm(np.cross(vec_x, center - np.flip(px))) / np.linalg.norm(vec_x)
        inertia[0] = m * r_x ** 2
        inertia[1] = m * r_z ** 2

    if debug:
        print(inertia, semi_major_axis, angle)
        img_ = icaps.enlarge_pixels(img, 8)
        img_ = icaps.draw_cross(img_, (center * 8).astype(int), color=(0, 255, 0), size=3, thickness=2)
        h, w = img_.shape[:2]
        p1, p2 = icaps.extend_line(vec_x * 8, center * 8, w, h)
        cv2.line(img_, tuple(p1.astype(int)), tuple(p2.astype(int)), color=(255, 0, 0))
        icaps.save_img(img_, out_path + "debug_inertia.png")
        icaps.cvshow(img_)

    return inertia


def calc_tilt_inertia(df, in_path, out_path, csv_path, threshold=30, debug=False):
    key = "ellipse_tilt"
    if key not in df.columns:
        df[key] = 0.
    particles = np.unique(df["particle"].to_numpy()).astype(np.uint64)
    df_sma_inertia = pd.DataFrame(columns=["particle", "frame", "max_sma", "inertia_x", "inertia_z"])
    for i, particle in enumerate(particles):
        df_ = df[df["particle"] == particle]
        sma = df_["semi_major_axis"].to_numpy()
        max_sma = np.max(sma[~np.isnan(sma)])
        plane_frame = df_[df_["semi_major_axis"] == max_sma]["frame"].to_numpy()[0].astype(np.uint64)
        angle = df_[df_["semi_major_axis"] == max_sma]["ellipse_angle"].to_numpy()[0].astype(float)
        print("particle:\t{:0>6}".format(particle) + "\tmax_sma:\t" + str(max_sma))
        for idx in df_.index.to_numpy():
            df.loc[idx, key] = np.degrees(np.arccos(df_.loc[idx, "semi_major_axis"]/max_sma))
        img = icaps.load_img(in_path + "{:0>6}/".format(particle) + "{:0>6}_".format(particle) +
                             icaps.generate_file(plane_frame, prefix=None))
        inertia = calc_inertia(img, max_sma, angle, out_path, threshold=threshold, debug=debug)
        df_sma_inertia = df_sma_inertia.append(pd.DataFrame(data={"particle": [particle],
                                                                  "frame": [plane_frame],
                                                                  "max_sma": [max_sma],
                                                                  "inertia_x": [inertia[0]],
                                                                  "inertia_z": [inertia[1]]}), sort=False)
    df_sma_inertia.to_csv(csv_path, index=False)
    return df


def calc_tilt_displacement(df, out_path, min_dt=1):
    icaps.mk_folder(out_path + "tilt displacement/" + "min_dt_{}/".format(min_dt), clear=True)

    key = "dw_x_" + str(min_dt)
    if key not in df.columns:
        df[key] = np.nan
    particles = np.unique(df["particle"].to_numpy()).astype(np.uint64)
    pb = icaps.ProgressBar(len(particles), "Calculating Tilt Displacement with min_dt=" + str(min_dt))
    for particle in particles:
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy()).astype(np.uint64)
        prev_idx = 0
        leaps = []
        for i, frame in enumerate(frames):
            if i == 0:
                continue
            prev_frame = frames[prev_idx]
            prev_angle = df_[df_["frame"] == prev_frame]["ellipse_tilt"].to_numpy()[0]
            if np.isnan(prev_angle):
                prev_idx += 1
                continue
            angle = df_[df_["frame"] == frame]["ellipse_tilt"].to_numpy()[0]
            if np.isnan(angle):
                continue
            d_angle = angle - prev_angle
            dt = frame - prev_frame
            if dt >= min_dt:
                if np.abs(d_angle) > 90:
                    leaps.append(prev_idx)
                    prev_idx += 1
                else:
                    table_idx = df_[df_["frame"] == prev_frame].index.to_numpy()[0]
                    df.at[table_idx, key] = d_angle / dt * min_dt
                    prev_idx = i

        tilt_displacement = df[df["particle"] == particle][key].to_numpy().astype(np.float64)
        tilt = df[df["particle"] == particle]["ellipse_tilt"].to_numpy()
        leaps = np.array(leaps, dtype=np.uint64)

        plt.close()
        _, axes = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
        ax = axes[0]
        ax.set_axisbelow(True)
        ax.grid()
        ax.set_title(str(particle))
        ax.set_xlabel("Frame")
        ax.set_ylabel("Tilt in °")
        idcs = np.argwhere(np.isnan(tilt_displacement)).flatten()
        for leap in leaps:
            if leap in idcs:
                idcs = idcs[idcs != leap]
        leap_frames = frames[leaps]
        frames_cut = frames.copy().astype(np.float64)
        frames_cut = np.delete(frames_cut, idcs)
        tilt_cut = tilt.copy()
        tilt_cut = np.delete(tilt_cut, idcs)
        for leap_frame in leap_frames:
            idx = np.argwhere(frames_cut == leap_frame)[0]
            frames_cut = np.insert(frames_cut, idx + 1, leap_frame + 0.5)
            tilt_cut = np.insert(tilt_cut, idx + 1, np.nan)

        ax.plot(frames_cut, tilt_cut, c="red", linewidth=1)
        ax.scatter(frames, tilt, s=3, c="black")
        xlims = ax.get_xlim()

        ax = axes[1]
        ax.set_axisbelow(True)
        ax.grid()
        ax.set_title("min_dt=" + str(min_dt))
        ax.set_xlabel("Frame")
        ax.set_ylabel("Tilt Displacement in °")
        ax.scatter(frames, tilt_displacement, s=3, c="black")
        ax.set_ylim(-np.nanmax(np.abs(tilt_displacement)), np.nanmax(np.abs(tilt_displacement)))
        ax.set_xlim(xlims)
        plt.savefig(
            out_path + "tilt displacement/" + "{:0>6}".format(particle) +
            "_{}".format(min_dt) + ".png")
        plt.close()
        pb.tick()
    pb.finish()
    return df


def calc_trans_displacement(df, out_path, min_dt=1, plot=False):
    icaps.mk_folder(out_path + "transx displacement/" + "min_dt_{}/".format(min_dt), clear=True)
    icaps.mk_folder(out_path + "transy displacement/" + "min_dt_{}/".format(min_dt), clear=True)

    df = icaps.calc_velocities(df, min_dt=min_dt, append_min_dt=True, norm_dt=True)
    particles = np.unique(df["particle"].to_numpy()).astype(np.uint64)

    pb = icaps.ProgressBar(len(particles), "Calculating Trans Displacement with min_dt=" + str(min_dt))
    for particle in particles:
        df_ = df[df["particle"] == particle]
        if plot:
            frames = np.unique(df_["frame"].to_numpy()).astype(np.uint64)
            vx = df[df["particle"] == particle]["vx_" + str(min_dt)].to_numpy().astype(np.float64)
            x = df[df["particle"] == particle]["x"].to_numpy()

            plt.close()
            _, axes = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
            ax = axes[0]
            ax.set_axisbelow(True)
            ax.grid()
            ax.set_title(str(particle))
            ax.set_xlabel("Frame")
            ax.set_ylabel("x in px")
            idcs = np.argwhere(np.isnan(vx)).flatten()
            frames_cut = frames.copy().astype(np.float64)
            frames_cut = np.delete(frames_cut, idcs)
            angle_cut = x.copy()
            angle_cut = np.delete(angle_cut, idcs)

            ax.plot(frames_cut, angle_cut, c="red", linewidth=1)
            ax.scatter(frames, x, s=3, c="black")
            xlims = ax.get_xlim()

            ax = axes[1]
            ax.set_axisbelow(True)
            ax.grid()
            ax.set_title("min_dt=" + str(min_dt))
            ax.set_xlabel("Frame")
            ax.set_ylabel("x Displacement in px")
            ax.scatter(frames, vx, s=3, c="black")
            ax.set_ylim(-np.nanmax(np.abs(vx)), np.nanmax(np.abs(vx)))
            ax.set_xlim(xlims)
            plt.savefig(
                out_path + "transx displacement/" + "{:0>6}".format(particle) +
                "_{}".format(min_dt) + ".png")
            plt.close()

            vy = df[df["particle"] == particle]["vy_" + str(min_dt)].to_numpy().astype(np.float64)
            y = df[df["particle"] == particle]["y"].to_numpy()

            plt.close()
            _, axes = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
            ax = axes[0]
            ax.set_axisbelow(True)
            ax.grid()
            ax.set_title(str(particle))
            ax.set_xlabel("Frame")
            ax.set_ylabel("y in px")
            idcs = np.argwhere(np.isnan(vy)).flatten()
            frames_cut = frames.copy().astype(np.float64)
            frames_cut = np.delete(frames_cut, idcs)
            angle_cut = y.copy()
            angle_cut = np.delete(angle_cut, idcs)

            ax.plot(frames_cut, angle_cut, c="red", linewidth=1)
            ax.scatter(frames, y, s=3, c="black")
            xlims = ax.get_xlim()

            ax = axes[1]
            ax.set_axisbelow(True)
            ax.grid()
            ax.set_title("min_dt=" + str(min_dt))
            ax.set_xlabel("Frame")
            ax.set_ylabel("y Displacement in px")
            ax.scatter(frames, vy, s=3, c="black")
            ax.set_ylim(-np.nanmax(np.abs(vy)), np.nanmax(np.abs(vy)))
            ax.set_xlim(xlims)
            plt.savefig(
                out_path + "transy displacement/" + "{:0>6}".format(particle) +
                "_{}".format(min_dt) + ".png")
            plt.close()
        pb.tick()
    pb.finish()
    return df


def error_func(z, a, b):    # Phi(z) = 1/2[1 + erf(z/sqrt(2))]
    return 0.5*(1 + erf((z-b)/a))


def fit_para(x, a):
    return a * (x**2)


def fit_para_log(x, a):
    return np.log10(a) + 2*np.log10(x)


def fit_lin(x, a):
    return a * x


def fit_lin2(x, D):
    return 2*D*x


k = 1.38065e-23
T = 301.35
T_err = 0.299
T_f = 0.008
r_p = 0.75e-6   # particle radius in m
rho_p = 2000    # particle density in kg/m^3
m_p = 4/3 * np.pi * rho_p * r_p**3  # particle mass in kg
print(m_p)

def fit_of_mf(x, mass_cc, f):
    a = 2 * k * T / f
    b = f / (mass_cc * m_p)
    return (a / b) * (b * x - 1 + np.exp(-b * x))


def fit_of_log(x, a, b):
    b = np.abs(b)
    return a + 2*np.log10(b) + np.log10(x/b - 1 + np.exp(-x/b))


def plot_erf(df, out_path, data_out_path, dts, plot=False):
    icaps.mk_folder(out_path + "erf/")
    axis_labels = np.array(["x", "z"])
    data = pd.DataFrame(data={"dt": np.repeat(dts, 2),
                              "axis": np.tile(axis_labels, len(dts)),
                              "sigma": np.zeros(2*len(dts)),
                              "sigma_err": np.zeros(2*len(dts)),
                              "drift": np.zeros(2*len(dts)),
                              "drift_err": np.zeros(2*len(dts))})

    pb = icaps.ProgressBar(len(dts), "Plotting Erf-Fits")
    idx = 0
    for dt in dts:
        if plot:
            _, ax = plt.subplots(nrows=2)
        xmin = 0
        xmax = 0
        for ai, a in enumerate(axis_labels):
            dw = df["dw_{}_{}".format(a, dt)].to_numpy()
            dw = dw[~np.isnan(dw)]
            bins = int(len(dw))
            n, bins = np.histogram(dw, bins, density=True)
            ff = np.cumsum(n) / np.max(np.cumsum(n))
            popt, pcov = curve_fit(error_func, bins[:-1], ff)
            if plot:
                ax[ai].scatter(bins[:-1], ff, s=1, c="black")
                ax[ai].plot(bins[:-1], error_func(bins[:-1], *popt), color="red", lw=1,
                            label="Rot-Drift: {:.2f}µm, Sigma: {:.2f}µm".format(popt[1], popt[0]))
                ax[ai].set_ylabel("Normalized " + a)
                ax[ai].legend(loc="upper left")
            xmin = np.min([np.min(dw), xmin])
            xmax = np.max([np.max(dw), xmax])
            perr = np.sqrt(np.diag(pcov))
            data.iloc[idx, data.columns.to_list().index("sigma")] = popt[0] * np.pi/180
            data.iloc[idx, data.columns.to_list().index("sigma_err")] = perr[0] * np.pi/180
            data.iloc[idx, data.columns.to_list().index("drift")] = popt[1] * np.pi/180
            data.iloc[idx, data.columns.to_list().index("drift_err")] = perr[1] * np.pi/180
            idx += 1
        if plot:
            ax[-1].set_xlabel(r"$\Delta\vartheta$ in °")
            for ai, a in enumerate(axis_labels):
                ax[ai].set_xlim(xmin, xmax)
            plt.savefig(out_path + "erf/erf_{}.png".format(dt), dpi=600)
            plt.close()
        pb.tick()
    pb.finish()
    data.to_csv(data_out_path + "erf_results.csv", index=False)


def plot_erf_trans(df, out_path, data_out_path, dts, plot=False):
    icaps.mk_folder(out_path + "erf_trans/")
    axis_labels = np.array(["x", "y"])
    data = pd.DataFrame(data={"dt": np.repeat(dts, 2),
                              "axis": np.tile(axis_labels, len(dts)),
                              "sigma": np.zeros(2*len(dts)),
                              "sigma_err": np.zeros(2*len(dts)),
                              "drift": np.zeros(2*len(dts)),
                              "drift_err": np.zeros(2*len(dts))})

    pb = icaps.ProgressBar(len(dts), "Plotting Erf-Fits Trans")
    idx = 0
    for dt in dts:
        if plot:
            _, ax = plt.subplots(nrows=2)
        xmin = 0
        xmax = 0
        for ai, a in enumerate(axis_labels):
            dw = df["v{}_{}".format(a, dt)].to_numpy()
            dw = dw[~np.isnan(dw)]
            bins = int(len(dw))
            n, bins = np.histogram(dw, bins, density=True)
            ff = np.cumsum(n) / np.max(np.cumsum(n))
            popt, pcov = curve_fit(error_func, bins[:-1], ff)
            if plot:
                ax[ai].scatter(bins[:-1], ff, s=1, c="black")
                ax[ai].plot(bins[:-1], error_func(bins[:-1], *popt), color="red", lw=1,
                            label="Trans-Drift: {:.2f}µm, Sigma: {:.2f}µm".format(popt[1], popt[0]))
                ax[ai].set_ylabel("Normalized " + a)
                ax[ai].legend(loc="upper left")
            xmin = np.min([np.min(dw), xmin])
            xmax = np.max([np.max(dw), xmax])
            perr = np.sqrt(np.diag(pcov))
            data.iloc[idx, data.columns.to_list().index("sigma")] = popt[0] / 1e6   # in m
            data.iloc[idx, data.columns.to_list().index("sigma_err")] = perr[0] / 1e6   # in m
            data.iloc[idx, data.columns.to_list().index("drift")] = popt[1] / 1e6   # in m
            data.iloc[idx, data.columns.to_list().index("drift_err")] = perr[1] / 1e6   # in m
            idx += 1
        if plot:
            ax[-1].set_xlabel("v in µm")
            for ai, a in enumerate(axis_labels):
                ax[ai].set_xlim(xmin, xmax)
            plt.savefig(out_path + "erf_trans/erf_{}.png".format(dt), dpi=600)
            plt.close()
        pb.tick()
    pb.finish()
    data.to_csv(data_out_path + "erf_trans_results.csv", index=False)


def plot_sigma_OF(erf_data, plot_out_path, data_out_path, dts, sma, particle_id, absorption, gyrad, max_dt=50, fit_to=50, fit_OF_from=2, para_lin_cut_off=4, plot_error=False):
    idx_max_dt = int(np.where(dts == max_dt)[0] + 1)
    idx_fit_to = int(np.where(dts == fit_to)[0] + 1)
    idx_para_lin = int(np.where(dts == para_lin_cut_off)[0])
    icaps.mk_folder(plot_out_path)
    axis_labels = np.array(["x", "z"])
    data = pd.DataFrame(data={"particle": [particle_id, particle_id],
                              "axis": ["x", "z"],
                              "absorption": [absorption, absorption],
                              "sma": [sma, sma],
                              "gyrad": [gyrad, gyrad],
                              "tau": [0., 0.],
                              "tau_err": [0., 0.],
                              "inertia": [0., 0.],
                              "inertia_err": [0., 0.]
                              })
    sma = sma / 1e6     # in m
    dts = dts / 1e3     # in s

    pb = icaps.ProgressBar(len(axis_labels), "Plotting Sigmas")
    for ai, a in enumerate(axis_labels):
        _, ax = plt.subplots()
        sigma = np.squeeze(erf_data.loc[erf_data["axis"] == a, ["sigma"]].to_numpy())
        sigma_squared = sigma ** 2
        sigma_squared_err = 2 * erf_data.loc[erf_data["axis"] == a, ["sigma_err"]].to_numpy()
        if plot_error:
            ax.errorbar(dts[:idx_max_dt], sigma_squared[:idx_max_dt], yerr=sigma_squared_err[:idx_max_dt], fmt=".",
                        color="black", markersize=1)
        else:
            ax.scatter(dts[:idx_max_dt], sigma_squared[:idx_max_dt], s=3, c="black",
                       # label=r"$\sigma_{rot}^2$"
                       )

        try:
            popt, pcov = curve_fit(fit_para, dts[:idx_para_lin + 1], sigma_squared[:idx_para_lin + 1])
            perr = np.sqrt(np.diag(pcov))
            dt_range = np.linspace(np.min(dts), np.max(dts[:idx_para_lin]), 1000)
            # ax.plot(dt_range, fit_para(dt_range, *popt), color="blue", lw=1, linestyle="--",
            #         label=r"Fit: $\sigma^2$ = a $\cdot$ dt$^2$" +
            #               "\na = {:.2f} \u00b1 {:.2f}".format(np.round(popt[0], 2), np.round(perr[0], 2))
            #         # label="Para"
            #         )
            angular_velocity = np.sqrt(popt[0])    # in rad/s
            # angular_velocity_err = 1/2*perr[0]*1e6/angular_velocity    # in rad/s
            mass_cc = max(1, ((k * T) / (angular_velocity * sma)**2) / m_p)  # in monomers
            # inertia = mass_cc * m_p * (angular_velocity * radius)**2     # in kg*m²
            print("mass_cc:\t", mass_cc)
            # data.loc[data["axis"] == a, ["angular_velocity"]] = angular_velocity
            # data.loc[data["axis"] == a, ["angular_velocity_err"]] = angular_velocity_err
            # data.loc[data["axis"] == a, ["mass"]] = mass_cc
            # data.loc[data["axis"] == a, ["inertia"]] = inertia
        except ValueError or RuntimeError:
            print('Error: Fit PARA')

        try:
            popt, pcov = curve_fit(fit_lin2, dts[idx_para_lin:idx_fit_to], sigma_squared[idx_para_lin:idx_fit_to])
            fff_x = (popt[0])
            print(fff_x)
            # perr = np.sqrt(np.diag(pcov))
        except ValueError or RuntimeError:
            print('Fehler: Fit LIN')

        # try:
        #     f = k * T / fff_x
        #     p0 = (mass_cc * m_p, f)
        #     popt, pcov = curve_fit(fit_of_mf, dts[fit_OF_from:idx_fit_to], sigma_squared[fit_OF_from:idx_fit_to], p0=p0)
        #     # N_Mono_x_OF = (popt[0]) * 1e12
        #     # fff_x_OF = k * T / (popt[1])
        #     # print(N_Mono_x, N_Mono_x_OF, fff_x, fff_x_OF)
        #     # perr = np.sqrt(np.diag(pcov))
        #     dt_range = np.linspace(np.min(dts[fit_OF_from:]), np.max(dts[:idx_fit_to]), 1000)
        #     ax.plot(dt_range, fit_of_mf(dt_range, *popt), color="black", lw=1, linestyle="-.", label=r'OF')
        #     # ddd = len(dts[:idx_fit_to]) - 2
        #     # Nexp = OF_fit_mf(dts[:idx_fit_to], *popt)
        #     # N = sigma_squared[:idx_fit_to]
        #     # sigff = s_temp.sigma_x_err * s_temp.sigma_x_err
        #     # r = (N - Nexp) / sigff
        #     # chisqx = np.sum(r ** 2)
        #     # chisqx_red = chisqx / ddd
        #     # print("###################### chisqx =", chisqx_red)
        # except ValueError or RuntimeError:
        #     print("Fehler: Fit OF")

        try:
            # for mass_cc in [1, 2, 3, 4, 5, 10, 15, 20]:
            mass_cc = 1
            f = k * T / fff_x
            # p0 = (mass_cc * m_p, f)
            param_a = np.log10(2 * k * T / (mass_cc * m_p))
            param_b = f / (mass_cc * m_p)
            p0 = (param_a, param_b)
            p0 = (np.log10(2*k*T/(m_p*r_p**2)), 0.001)
            # p0 = None
            # p0 = (-6, 10**-3)
            try:
                popt, pcov = curve_fit(fit_of_log, dts[fit_OF_from:idx_fit_to], np.log10(sigma_squared[fit_OF_from:idx_fit_to]), p0=p0, maxfev=1000)
                perr = np.sqrt(np.diag(pcov))
                # N_Mono_x_OF = (popt[0]) * 1e12
                # fff_x_OF = k * T / (popt[1])
                # print(N_Mono_x, N_Mono_x_OF, fff_x, fff_x_OF)
                dt_range = np.linspace(np.min(dts[fit_OF_from:]), np.max(dts[:idx_fit_to]), 1000)
                ax.plot(dt_range, 10 ** (fit_of_log(dt_range, *popt)), color="red", lw=1, linestyle="-",
                        label=r'OF_log')
                # ddd = len(dts[fit_OF_from:idx_fit_to]) - 2
                # Nexp = fit_of_log(dts[fit_OF_from:idx_fit_to], *popt)
                # N = sigma_squared[fit_OF_from:idx_fit_to]
                # r = (N - Nexp) / sigma_squared_err[fit_OF_from:idx_fit_to]
                # r = N-Nexp
                # chisqx = np.sum(r ** 2)
                # chisqx_red = chisqx / ddd
                # c, p = chisquare(fit_of_log(dts[fit_OF_from:idx_fit_to], *popt), sigma_squared[fit_OF_from:idx_fit_to])
                # print("CHI-SQUARED\t", chisqx_red/10**7, chisqx/10**7, c, p)
                # plt.show()
                print("a", popt[0])
                print("10^a", 10 ** popt[0])
                print("inertia", 2 * k * T / (10 ** popt[0]))
                data.loc[data["axis"] == a, ["tau"]] = popt[1]  # in s
                data.loc[data["axis"] == a, ["tau_err"]] = perr[1]  # in s
                data.loc[data["axis"] == a, ["inertia"]] = 2 * k * T / (10**popt[0])  # in kg*m²
                data.loc[data["axis"] == a, ["inertia_err"]] = 2 * k * T * np.log(10) / (10**popt[0]) * perr[0]     # in kg*m²
            except RuntimeError:
                print("Fehler (2): Fit OF_log")
        except ValueError or RuntimeError:
            print("Fehler: Fit OF_log")

        ax.set_ylabel(r"$\sigma_{rot}^2$ in rad$^2$")
        ax.legend(loc="lower right")
        ax.set_xlabel("dt in s")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(left=np.min(dts)*0.95)
        ax.set_title(str(np.round(absorption/311)))
        plt.savefig(plot_out_path + "sigma_OF_{}_{}_{}_{}.png".format(a, max_dt, fit_to, para_lin_cut_off))
        plt.close()
        pb.tick()
    pb.finish()
    data.to_csv(data_out_path + "OF_results.csv", index=False)


def plot_sigma_OF_trans(erf_data, plot_out_path, data_out_path, dts, sma, particle_id, absorption, gyrad, max_dt=50, fit_to=50, fit_OF_from=2, para_lin_cut_off=4, plot_error=False):
    idx_max_dt = int(np.where(dts == max_dt)[0] + 1)
    idx_fit_to = int(np.where(dts == fit_to)[0] + 1)
    idx_para_lin = int(np.where(dts == para_lin_cut_off)[0])
    icaps.mk_folder(plot_out_path)
    axis_labels = np.array(["x", "y"])
    data = pd.DataFrame(data={"particle": [particle_id, particle_id],
                              "axis": ["x", "y"],
                              "absorption": [absorption, absorption],
                              "sma": [sma, sma],
                              "gyrad": [gyrad, gyrad],
                              "tau": [0., 0.],
                              "tau_err": [0., 0.],
                              "mass": [0., 0.],
                              "mass_err": [0., 0.]
                              })
    sma = sma / 1e6     # in m
    dts = dts / 1e3     # in s

    pb = icaps.ProgressBar(len(axis_labels), "Plotting Sigmas")
    for ai, a in enumerate(axis_labels):
        _, ax = plt.subplots()
        sigma = np.squeeze(erf_data.loc[erf_data["axis"] == a, ["sigma"]].to_numpy())
        sigma_squared = sigma ** 2
        sigma_squared_err = 2 * erf_data.loc[erf_data["axis"] == a, ["sigma_err"]].to_numpy()
        if plot_error:
            ax.errorbar(dts[:idx_max_dt], sigma_squared[:idx_max_dt], yerr=sigma_squared_err[:idx_max_dt], fmt=".",
                        color="black", markersize=1)
        else:
            ax.scatter(dts[:idx_max_dt], sigma_squared[:idx_max_dt], s=3, c="black",
                       # label=r"$\sigma_{rot}^2$"
                       )

        try:
            popt, pcov = curve_fit(fit_para, dts[:idx_para_lin + 1], sigma_squared[:idx_para_lin + 1])
            perr = np.sqrt(np.diag(pcov))
            dt_range = np.linspace(np.min(dts), np.max(dts[:idx_para_lin]), 1000)
            # ax.plot(dt_range, fit_para(dt_range, *popt), color="blue", lw=1, linestyle="--",
            #         label=r"Fit: $\sigma^2$ = a $\cdot$ dt$^2$" +
            #               "\na = {:.2f} \u00b1 {:.2f}".format(np.round(popt[0], 2), np.round(perr[0], 2))
            #         # label="Para"
            #         )
            angular_velocity = np.sqrt(popt[0])    # in rad/s
            # angular_velocity_err = 1/2*perr[0]*1e6/angular_velocity    # in rad/s
            mass_cc = max(1, ((k * T) / (angular_velocity * sma)**2) / m_p)  # in monomers
            # inertia = mass_cc * m_p * (angular_velocity * radius)**2     # in kg*m²
            print("mass_cc:\t", mass_cc)
            # data.loc[data["axis"] == a, ["angular_velocity"]] = angular_velocity
            # data.loc[data["axis"] == a, ["angular_velocity_err"]] = angular_velocity_err
            # data.loc[data["axis"] == a, ["mass"]] = mass_cc
            # data.loc[data["axis"] == a, ["inertia"]] = inertia
        except ValueError or RuntimeError:
            print('Error: Fit PARA')

        try:
            popt, pcov = curve_fit(fit_lin2, dts[idx_para_lin:idx_fit_to], sigma_squared[idx_para_lin:idx_fit_to])
            fff_x = (popt[0])
            print(fff_x)
            # perr = np.sqrt(np.diag(pcov))
        except ValueError or RuntimeError:
            print('Fehler: Fit LIN')

        try:
            # for mass_cc in [1, 2, 3, 4, 5, 10, 15, 20]:
            mass_cc = 1
            f = k * T / fff_x
            # p0 = (mass_cc * m_p, f)
            param_a = np.log10(2 * k * T / (mass_cc * m_p))
            param_b = f / (mass_cc * m_p)
            p0 = (param_a, param_b)
            p0 = (param_a, 0.001)
            # p0 = None
            # p0 = (-6, 10**-3)
            try:
                popt, pcov = curve_fit(fit_of_log, dts[fit_OF_from:idx_fit_to], np.log10(sigma_squared[fit_OF_from:idx_fit_to]), p0=p0, maxfev=1000)
                perr = np.sqrt(np.diag(pcov))
                # N_Mono_x_OF = (popt[0]) * 1e12
                # fff_x_OF = k * T / (popt[1])
                # print(N_Mono_x, N_Mono_x_OF, fff_x, fff_x_OF)
                dt_range = np.linspace(np.min(dts[fit_OF_from:]), np.max(dts[:idx_fit_to]), 1000)
                ax.plot(dt_range, 10 ** (fit_of_log(dt_range, *popt)), color="red", lw=1, linestyle="-",
                        label=r'OF_log')
                # ddd = len(dts[fit_OF_from:idx_fit_to]) - 2
                # Nexp = fit_of_log(dts[fit_OF_from:idx_fit_to], *popt)
                # N = sigma_squared[fit_OF_from:idx_fit_to]
                # r = (N - Nexp) / sigma_squared_err[fit_OF_from:idx_fit_to]
                # r = N-Nexp
                # chisqx = np.sum(r ** 2)
                # chisqx_red = chisqx / ddd
                # c, p = chisquare(fit_of_log(dts[fit_OF_from:idx_fit_to], *popt), sigma_squared[fit_OF_from:idx_fit_to])
                # print("CHI-SQUARED\t", chisqx_red/10**7, chisqx/10**7, c, p)
                # plt.show()
                data.loc[data["axis"] == a, ["tau"]] = popt[1]  # in s
                data.loc[data["axis"] == a, ["tau_err"]] = perr[1]  # in s
                print("a", popt[0])
                print("10^a", 10**popt[0])
                print("mass", 2 * k * T / (10**popt[0]))
                data.loc[data["axis"] == a, ["mass"]] = 2 * k * T / (10**popt[0])  # in kg
                data.loc[data["axis"] == a, ["mass_err"]] = 2 * k * T * np.log(10) / (10**popt[0]) * perr[0]     # in kg
            except RuntimeError:
                print("Fehler (2): Fit OF_log")
        except ValueError or RuntimeError:
            print("Fehler: Fit OF_log")

        ax.set_ylabel(r"$\sigma_{trans}^2$ in m$^2$")
        ax.legend(loc="lower right")
        ax.set_xlabel("dt in s")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(left=np.min(dts)*0.95, right=np.max(dts)*1.05)
        ax.set_ylim(bottom=np.min(sigma_squared)*0.95, top=np.max(sigma_squared)*1.05)
        plt.savefig(plot_out_path + "sigma_OF_trans_{}_{}_{}_{}.png".format(a, max_dt, fit_to, para_lin_cut_off))
        plt.close()
        pb.tick()
    pb.finish()
    data.to_csv(data_out_path + "OF_trans_results.csv", index=False)


'''
def plot_sigma_para(erf_data, plot_out_path, data_out_path, dts, radius, max_dt=50, fit_to=4, plot_error=False):
    idx_max_dt = int(np.where(dts == max_dt)[0] + 1)
    idx_fit_to = int(np.where(dts == fit_to)[0] + 1)
    icaps.mk_folder(plot_out_path)
    icaps.mk_folder(data_out_path)
    axis_labels = np.array(["x", "z"])

    data = pd.DataFrame(data={"axis": ["x", "z"],
                              "angular_velocity": [0., 0.],
                              "angular_velocity_err": [0., 0.],
                              "mass": [0., 0.],
                              "radius": [radius, radius],
                              "inertia": [0., 0.],
                              "inertia2": [0., 0.]})
    radius = radius / 1e6   # in m
    dts = dts * 1e3         # in s

    pb = icaps.ProgressBar(len(axis_labels), "Plotting Sigmas")
    for ai, a in enumerate(axis_labels):
        _, ax = plt.subplots()
        sigma = np.squeeze(erf_data.loc[erf_data["axis"] == a, ["sigma"]].to_numpy())
        sigma_squared = sigma ** 2
        if plot_error:
            sigma_squared_err = 2 * erf_data.loc[erf_data["axis"] == a, ["sigma_err"]]
            ax.errorbar(dts[:idx_max_dt], sigma_squared[:idx_max_dt], yerr=sigma_squared_err[:idx_max_dt], fmt=".",
                        color="black", markersize=1)
        else:
            ax.scatter(dts[:idx_max_dt], sigma_squared[:idx_max_dt], s=1, c="black",
                       # label=r"$\sigma_{rot}^2$"
                       )

        try:
            popt, pcov = curve_fit(fit_para, dts[:idx_fit_to], sigma_squared[:idx_fit_to])
            perr = np.sqrt(np.diag(pcov))
            dt_range = np.linspace(np.min(dts), np.max(dts[:idx_fit_to]), 1000)
            ax.plot(dt_range, fit_para(dt_range, *popt), color="blue", lw=1, linestyle="--",
                    # label=r"Fit: $\sigma^2$ = a $\cdot$ dt$^2$" +
                    #       "\na = {:.2f} \u00b1 {:.2f}".format(np.round(popt[0], 2), np.round(perr[0], 2))
                    label="PARA"
                    )
            popt, pcov = curve_fit(fit_para_log, dts[:idx_fit_to], np.log10(sigma_squared[:idx_fit_to]))
            perr = np.sqrt(np.diag(pcov))
            dt_range = np.linspace(np.min(dts), np.max(dts[:idx_fit_to]), 1000)
            ax.plot(dt_range, 10**(fit_para_log(dt_range, *popt)), color="red", lw=1, linestyle="-",
                    # label=r"Fit: $\sigma^2$ = a $\cdot$ dt$^2$" +
                    #       "\na = {:.2f} \u00b1 {:.2f}".format(np.round(popt[0], 2), np.round(perr[0], 2))
                    label="PARA_log"
                    )
            popt[0] = 10**popt[0]
            angular_velocity = np.sqrt(popt[0]*1e6)    # in rad/s
            angular_velocity_err = 1/2*perr[0]*1e6/angular_velocity    # in rad/s
            mass_cc = max(1, ((k * T) / (angular_velocity * radius)**2) / m_p)  # in monomers
            inertia = mass_cc * m_p * (angular_velocity * radius)**2     # in kg*m²
            inertia2 = k*T/angular_velocity**2
            print("mass_cc:\t", mass_cc)
            data.loc[data["axis"] == a, ["angular_velocity"]] = angular_velocity
            data.loc[data["axis"] == a, ["angular_velocity_err"]] = angular_velocity_err
            data.loc[data["axis"] == a, ["mass"]] = mass_cc
            data.loc[data["axis"] == a, ["inertia"]] = inertia
            data.loc[data["axis"] == a, ["inertia2"]] = inertia2
        except ValueError or RuntimeError:
            print('Error: Fit PARA')

        ax.set_ylabel(r"$\sigma_{rot}^2$ in rad$^2$")
        ax.legend(loc="lower right")
        ax.set_xlabel("dt in s")
        ax.set_xscale("log")
        ax.set_yscale("log")
        plt.savefig(plot_out_path + "sigma_PARA_{}_{}_{}.png".format(a, max_dt, fit_to))
        plt.close()
        pb.tick()
    pb.finish()
    data.to_csv(data_out_path + "results.csv", index=False)


def plot_energy(df, inertia, out_path, min_dt=1, density=2.0):
    # inertia in x and z should be equal
    inertia[0] = inertia[1]

    icaps.mk_folder(out_path)
    _, ax = plt.subplots(nrows=2)
    for ai, a in enumerate(["x", "z"]):
        energy = 1 / 2 * inertia[ai] * (np.radians(df["dw_{}_{}".format(a, min_dt)]/min_dt) * 2 * np.pi) ** 2 / (
                    311 / 1.4) * \
                 (density * 4 / 3 * np.pi * 0.75 ** 3) * 10 ** -21
        ax[ai].scatter(df["frame"] - np.amin(df["frame"]), energy, s=1, color="black",
                      label="mean=" + "{:.2f}".format(
                          np.round(np.mean(energy * 10 ** 21), 2)) + r"$\,\cdot\, 10^{-21}$ J")
        # ax[j].set_ylabel(r"$E_{}$ in".format(a) + r"$10^{-23}$ J")
        ax[ai].set_ylabel(r"$E_{}$ in".format(a) + " J")
        if ai == 1:
            ax[ai].set_xlabel("time in ms")
        ax[ai].legend(loc="lower right")
        ax[ai].set_yscale("log")
        ax[ai].set_ylim(10 ** -28, 10 ** -18)
    plt.savefig(out_path + "energy_" + str(min_dt) + ".png")
    plt.close()

    _, ax = plt.subplots(nrows=2)
    for ai, a in enumerate(["x", "z"]):
        energy = 1 / 2 * inertia[ai] * (np.radians(df["dw_{}_{}".format(a, min_dt)]/min_dt) * 2 * np.pi) ** 2 / (
                    311 / 1.4) * \
                 (density * 4 / 3 * np.pi * 0.75 ** 3) * 10 ** -21
        # ax[ai].scatter(df_axes["frame"] - np.amin(df_axes["frame"]), energy*10**-23, s=1, color="black",
        #               label="mean=" + "{:.2f}".format(np.round(np.mean(energy), 2)) + r"$\,\cdot\, 10^{-23}$ J")
        # ax[ai].set_ylabel(r"$E_{}$ in".format(a) + r"$10^{-23}$ J")
        energy = energy[~np.isnan(energy)]
        bins = [0.5 * 10 ** -x for x in range(28, 18, -1)]
        hist, bins = np.histogram(energy, bins=bins)
        hist = hist / np.sum(hist)

        # ax[ai].hist(energy * 10**-23, color="black", density=True, bins=bins)
        ax[ai].step(bins[1:], hist, color="black")
        # ax[ai].scatter(bins[:-1], hist, color="black", s=1)
        ax[ai].set_ylabel(r"frequency$_{}$".format(a))
        ax[ai].set_xlabel(r"$E_{}$ in".format(min_dt) + " J")
        ax[ai].set_ylim(0, 0.5)
        ax[ai].set_xscale("log")
        # ax[ai].legend(loc="lower right")
        # ax[ai].set_yscale("log")
        ax[ai].set_xlim(0.5 * 10 ** -28, 0.5 * 10 ** -18)
    plt.savefig(out_path + "energy_hist_" + str(min_dt) + ".png")
    plt.close()
'''


def read_candidates(path):
    with open(path, "r") as f:
        lines = f.readlines()
        candidates = [int(float(line.replace("\n", ""))) for line in lines]
    return candidates


def main():
    debug = False
    # particle_ids = [
    #     393067,
    #     8782280,
    #     435819,
    #     441925
    # ]
    # particle_ids = [
        # 1779553,
        # 1842750,
        # 3397839,
        # 4239925,
        # 8345429,
        # 8358017,
        # 8416335,
        # 8417507,
        # 8421149,
        # 8424720,
        # 8426033,
        # 8573199,
        # 8574400,
        # 8782280,
        # 8812471,
        # 8992060
    # ]
    # particle_ids = [549923, 539955, 538934, 383295]
    # particle_ids = [549923]
    particle_ids = read_candidates(project_path + "pre_candidates_clean_noedge.txt")

    stages = [
        # 1,
        # 2,
        # 3,
        # 4,
        # 5,
        # 6,
        # 7,
        # 8,
        # 9,
        # 10
    ]

    icaps.mk_folder(plot_collection_path)
    icaps.mk_folder(table_collection_path + "rot/")
    icaps.mk_folder(table_collection_path + "trans/")
    icaps.mk_folder(plot_collection_path + "energy/")
    for particle_id in particle_ids:
        csv_out_path = project_path + "tables/{:0>6}/".format(particle_id)
        icaps.mk_folder(csv_out_path)
        track_rot_path = csv_out_path + "Tracks_EP{}.csv".format(track_tag)
        track_ang_path = csv_out_path + "Tracks_AEP{}.csv".format(track_tag)
        track_trans_path = csv_out_path + "Tracks_TEP{}.csv".format(track_tag)
        plot_path = project_path + "plots/{:0>6}/".format(particle_id)
        sma_inertia_path = csv_out_path + "sma_inertia.csv"

        print(">>PARTICLE: " + str(particle_id))

        if 1 in stages:
            print("===============================================")
            print("STAGE 1: Ellipse Fitting")
            df = pd.read_csv(track_path)
            df = fit_ellipses(df, particle_id, particle_path, mark_path, thresh1=10, thresh2=30, enlarge=8,
                              diashow=False)
            df.to_csv(track_rot_path, index=False)

        if 2 in stages:
            print("===============================================")
            print("STAGE 2: Tilt and Inertia Calculation")
            df = pd.read_csv(track_rot_path)
            df = calc_tilt_inertia(df, particle_path, plot_path, sma_inertia_path, debug=debug)
            df.to_csv(track_rot_path, index=False)

        min_dts = np.arange(1, 26, 1)
        min_dts = np.append(min_dts, np.arange(30, 51, 5))
        print("min_dts:", min_dts)
        if 3 in stages:
            print("===============================================")
            print("STAGE 3: Angular Displacement Calculation and Plotting")
            df = pd.read_csv(track_rot_path)
            for min_dt in min_dts:
                df = calc_angular_displacement(df, plot_path, min_dt=min_dt)
            df.to_csv(track_ang_path, index=False)

        if 4 in stages:
            print("===============================================")
            print("STAGE 4: Tilt Displacement Calculation and Plotting")
            df = pd.read_csv(track_ang_path)
            for min_dt in min_dts:
                df = calc_tilt_displacement(df, plot_path, min_dt=min_dt)
            df.to_csv(track_ang_path, index=False)

        if 5 in stages:
            print("===============================================")
            print("STAGE 5: Trans Displacement Calculation and Plotting")
            df = pd.read_csv(track_rot_path)
            for min_dt in min_dts:
                df = calc_trans_displacement(df, plot_path, min_dt=min_dt)
            df.to_csv(track_trans_path, index=False)

        if 6 in stages:
            print("===============================================")
            print("STAGE 6: Rot Erf Plotting")
            df = pd.read_csv(track_ang_path)
            plot_erf(df, plot_path, csv_out_path, min_dts)

        if 7 in stages:
            print("===============================================")
            print("STAGE 7: Trans Erf Plotting")
            df = pd.read_csv(track_trans_path)
            plot_erf_trans(df, plot_path, csv_out_path, min_dts)

        if 8 in stages:
            print("===============================================")
            print("STAGE 8: Sigma Plotting")
            df_sma_inertia = pd.read_csv(sma_inertia_path)
            df_erf_results = pd.read_csv(csv_out_path + "erf_results.csv")
            df = pd.read_csv(track_path)
            df = df[df["particle"] == particle_id]
            absorption = np.max(df["mass"].to_numpy())
            gyrad = np.max(df["gyrad"].to_numpy())
            sma = df_sma_inertia["max_sma"].to_numpy()[0]
            # plot_sigma_para(df_erf_results, plot_path, csv_out_path, min_dts, sma, max_dt=50, fit_to=4)
            plot_sigma_OF(df_erf_results, plot_path, csv_out_path, min_dts, sma, particle_id, absorption, gyrad,
                          max_dt=50, fit_to=50, para_lin_cut_off=4)

            df_erf_results = pd.read_csv(csv_out_path + "erf_trans_results.csv")
            plot_sigma_OF_trans(df_erf_results, plot_path, csv_out_path, min_dts, sma, particle_id, absorption, gyrad,
                                max_dt=50, fit_to=50, para_lin_cut_off=4, fit_OF_from=0)

        if 9 in stages:
            print("===============================================")
            print("STAGE 9: Energy Calculation and Plotting")
            df = pd.read_csv(track_ang_path)
            df_sma_inertia = pd.read_csv(sma_inertia_path)
            inertia = np.array([df_sma_inertia["inertia_x"].to_numpy()[0], df_sma_inertia["inertia_z"].to_numpy()[0]])
            # for min_dt in min_dts:
                # plot_energy(df, inertia, plot_path + "energy/", min_dt)

        if 10 in stages:
            print("===============================================")
            print("STAGE 10: Copy to Collection")
            icaps.mk_folder(plot_collection_path + "sigma_OF_z_50_50_4/", clear=False)
            icaps.mk_folder(plot_collection_path + "sigma_OF_x_50_50_4/", clear=False)
            icaps.mk_folder(plot_collection_path + "sigma_OF_trans_y_50_50_4/", clear=False)
            icaps.mk_folder(plot_collection_path + "sigma_OF_trans_x_50_50_4/", clear=False)

            # copyfile(plot_path + "sigma_PARA_z_50_4.png", plot_collection_path +
            #          "sigma_PARA_z_50_4/sigma_PARA_z_{}.png".format(particle_id))
            copyfile(plot_path + "sigma_OF_z_50_50_4.png", plot_collection_path +
                     "sigma_OF_z_50_50_4/sigma_OF_z_{}.png".format(particle_id))
            # copyfile(plot_path + "sigma_PARA_x_50_4.png", plot_collection_path +
            #          "sigma_PARA_x_50_4/sigma_PARA_x_{}.png".format(particle_id))
            copyfile(plot_path + "sigma_OF_x_50_50_4.png", plot_collection_path +
                     "sigma_OF_x_50_50_4/sigma_OF_x_{}.png".format(particle_id))
            # copyfile(plot_path + "energy/energy_1.png", plot_collection_path +
            #          "energy/energy_1_{}.png".format(particle_id))
            copyfile(plot_path + "sigma_OF_trans_y_50_50_4.png", plot_collection_path +
                     "sigma_OF_trans_y_50_50_4/sigma_OF_trans_y_{}.png".format(particle_id))
            copyfile(plot_path + "sigma_OF_trans_y_50_50_4.png", plot_collection_path +
                     "sigma_OF_trans_x_50_50_4/sigma_OF_trans_x_{}.png".format(particle_id))
            copyfile(csv_out_path + "OF_results.csv", table_collection_path + "rot/" + "OF_results_{}.csv".format(particle_id))
            copyfile(csv_out_path + "OF_trans_results.csv", table_collection_path + "trans/" + "OF_trans_results_{}.csv".format(particle_id))
        print("")
    # icaps.merge_dataframes(table_collection_path + "rot/", project_path + "OF_rot_results.csv", delete_source=True)
    # icaps.merge_dataframes(table_collection_path + "trans/", project_path + "OF_trans_results.csv",
    #                        delete_source=True)

    df_r = pd.read_csv(project_path + "OF_rot_results.csv")
    df_rx = df_r[df_r["axis"] == "x"]
    df_rz = df_r[df_r["axis"] == "z"]
    df_t = pd.read_csv(project_path + "OF_trans_results.csv")
    df_tx = df_t[df_t["axis"] == "x"]
    df_ty = df_t[df_t["axis"] == "y"]
    df_rz_tx = pd.merge(df_rz, df_tx[["particle", "tau", "tau_err", "mass", "mass_err"]], on="particle")
    df_rz_tx = df_rz_tx[np.abs(df_rz_tx["inertia"])/df_rz_tx["inertia_err"] > 10**-2]
    print(df_rz_tx)

    _, ax = plt.subplots()
    x = (df_rz_tx["gyrad"]/1e6)**2 * df_rz_tx["mass"]
    y = df_rz_tx["inertia"]
    ax.scatter(x, y, s=5, color="black")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("m*r² in kg*m²")
    ax.set_ylabel("I in kg*m²")
    ax.set_xlim(np.min(x) * 0.9, np.max(x) * 1.1)
    ax.set_ylim(np.min(y) * 0.9, np.max(y) * 1.1)
    popt, pcov = curve_fit(fit_lin, x[1:], y[1:])
    perr = np.sqrt(np.diag(pcov))
    x_range = np.linspace(np.min(x[1:]), np.max(x), 1000)
    ax.plot(x_range, fit_lin(x_range, *popt), color="red", linewidth=1,
            label="a = {} \u00b1 {}".format(np.round(popt[0], 5), np.round(perr[0], 5)))
    ax.legend()

    _, ax = plt.subplots()
    x = df_rz_tx["tau_x"]*1e3
    y = df_rz_tx["tau_y"]*1e3
    ax.scatter(x, y, s=5, color="black")
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    ax.set_xlabel("tau_trans in ms")
    ax.set_ylabel("tau_rot in ms")
    ax.set_xlim(np.min(x) * 0.9, np.max(x) * 1.1)
    ax.set_ylim(np.min(y) * 0.9, np.max(y) * 1.1)
    # popt, pcov = curve_fit(fit_lin, x[1:], y[1:])
    # perr = np.sqrt(np.diag(pcov))
    # x_range = np.linspace(np.min(x[1:]), np.max(x), 1000)
    # ax.plot(x_range, fit_lin(x_range, *popt), color="red", linewidth=1,
    #         label="a = {} \u00b1 {}".format(np.round(popt[0], 5), np.round(perr[0], 5)))
    # ax.legend()

    plt.show()

    # df_z = df[df["axis"] == "z"]
    # """PLOT MORE STUFF"""
    # idx = np.where(np.abs(df_z["inertia"])/df_z["inertia_err"] > 10**-2)
    # df_z = df_z[idx]
    #
    # df_trans = df_trans[df_trans["axis"] == "x"]
    # df_trans = df_trans[idx]
    # print(df_z["particle"].to_numpy())
    # print(len(df_z["particle"].to_numpy()))
    # # df_z = df_z[np.abs(df_z["inertia"]) * 10 ** 21 > 0]
    # # _, ax = plt.subplots()
    # mass = df_z["absorption"]/311
    # # ax.scatter(mass, np.abs(df_z["inertia"]*10**21), s=2, color="black")
    # # # ax.set_ylim(0, 5)
    # # ax.set_xlabel("Mass")
    # # ax.set_ylabel("Inertia z")
    #
    # _, ax = plt.subplots()
    # ax.scatter(df_z["gyrad"], np.abs(df_z["tau"]), s=2, color="black")
    # # ax.set_ylim(0, 5)
    # ax.set_xlabel("Gyrad")
    # ax.set_ylabel("Tau z")
    #
    # # _, ax = plt.subplots()
    # # ax.scatter(df_z["gyrad"], np.abs(df_z["inertia"] * 10**21), s=2, color="black")
    # # # ax.set_ylim(0, 5)
    # # ax.set_xlabel("Gyrad")
    # # ax.set_ylabel("Inertia z")
    # # ax.set_xscale("log")
    # # ax.set_yscale("log")
    #
    # # _, ax = plt.subplots()
    # # mass = df_z["absorption"] / 311
    # # ax.scatter(mass, np.abs(df_z["tau"]), s=2, color="black")
    # # # ax.set_ylim(-0.2, 2)
    # # ax.set_xlabel("Mass")
    # # ax.set_ylabel("Tau z")
    #
    # df_x = df[df["axis"] == "x"]
    # df_x = df_x[np.abs(df_x["inertia"]) / df_x["inertia_err"] > 10 ** -2]
    # print(df_x["particle"].to_numpy())
    # print(len(df_x["particle"].to_numpy()))
    # unique = np.unique(np.append(df_x["particle"].to_numpy(), df_z["particle"].to_numpy()))
    # print(unique)
    # print(len(unique))
    # # df_z = df_z[np.abs(df_z["inertia"]) * 10 ** 21 > 0]
    # # _, ax = plt.subplots()
    # # mass = df_x["absorption"] / 311
    # # ax.scatter(mass, np.abs(df_x["inertia"] * 10 ** 21), s=2, color="black")
    # # # ax.set_ylim(0, 5)
    # # ax.set_xlabel("Mass")
    # # ax.set_ylabel("Inertia x")
    #
    # # _, ax = plt.subplots()
    # # mass = df_x["absorption"] / 311
    # # ax.scatter(mass, np.abs(df_x["tau"]), s=2, color="black")
    # # # ax.set_ylim(-0.2, 2)
    # # ax.set_xlabel("Mass")
    # # ax.set_ylabel("Tau x")
    #
    # _, ax = plt.subplots()
    # ax.scatter(df_x["gyrad"], np.abs(df_x["tau"]), s=2, color="black")
    # # ax.set_ylim(0, 5)
    # ax.set_xlabel("Gyrad")
    # ax.set_ylabel("Tau x")
    #
    # _, ax = plt.subplots()
    # ax.scatter((df_x["gyrad"]/1e6)**2 * df_x["absorption"]/311 * m_p * 1e21, np.abs(df_x["inertia"] * 1e21), s=10, color="black", marker="v")
    # ax.scatter((df_z["gyrad"]/1e6)**2 * df_z["absorption"]/311 * m_p * 1e21, np.abs(df_z["inertia"] * 1e21), s=10, color="black")
    # # ax.set_ylim(0, 5)
    # ax.set_xlabel("Gyrad² * Mass in 10*-21 kg*m²")
    # ax.set_ylabel("Inertia x + z in 10*-21 kg*m²")
    # ax.set_xscale("log")
    # ax.set_yscale("log")

    plt.show()


if __name__ == '__main__':
    main()


