import icaps
import pandas as pd
import numpy as np
from shutil import copyfile
import cv2

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT2/"
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
particle_path = project_path + "particles/"
movie_path = project_path + "movies/"
csv_P_path = project_path + "Tracks_P.csv"
csv_prefilter_path = project_path + "Tracks_PA50S20L100.csv"
# csv_postfilter_path = project_path + "Tracks_PA50"
# csv_PS20L500_path = project_path + "Tracks_PS20L500.csv"
csv_phase_path = project_path + "Phases_Hand.csv"


def main():
    particle = 371177
    test_path = "H:/icaps/data/LDM_ROT2/particles/{}/".format(particle)
    in_path = test_path + "ffc/"
    out_path = test_path + "test/"
    info_path = test_path + "info.csv"
    icaps.mk_folder(out_path)

    # df = icaps.measure_foci(in_path, prefix="371177_", silent=False)
    # df.to_csv(info_path, index=False)

    # df = pd.read_csv(info_path)
    # for idx, row in df.iterrows():
    #     if row["focus"] >= 13:
    #         file = icaps.generate_file(row["frame"].astype(int), prefix="371177_")
    #         img = icaps.load_img(in_path + file)
    #         icaps.save_img(img, out_path + file)

    # df_info = pd.read_csv(info_path)
    # use_frames = df_info[df_info["focus"] >= 13]["frame"].to_numpy()
    # df = pd.read_csv(csv_P_path)
    # df = df[df["particle"] == particle]
    # df = df[np.in1d(df["frame"], use_frames)]
    # df = icaps.fit_ellipses_bulk(df, particle_path, mark=True, mark_enlarge=4, silent=False)
    # df.to_csv(test_path + "ellipse.csv", index=False)

    min_dts = np.arange(1, 26, 1)
    min_dts = np.append(min_dts, np.arange(30, 51, 5))
    # df = pd.read_csv(test_path + "ellipse.csv")
    # df = icaps.calc_tilt(df)
    # df_rot = icaps.calc_rot_displacements(df, min_dts, mode="angle", particle_path=particle_path, silent=False)
    # df_rot = icaps.calc_rot_displacements(df_rot, min_dts, mode="tilt", particle_path=particle_path,
    #                                       silent=False)
    # df_rot.to_csv(test_path + "disp_rot.csv")

    # df = pd.read_csv(test_path + "ellipse.csv")
    # df_trans = icaps.calc_trans_displacements(df, min_dts, particle_path=particle_path, silent=False)
    # df_trans.to_csv(test_path + "disp_trans.csv")

    # df_rot = pd.read_csv(test_path + "disp_rot.csv")
    # df_trans = pd.read_csv(test_path + "disp_trans.csv")
    # icaps.plot_erf_bulk(df_rot, min_dts, particle_path, mode="rot", silent=False)
    # icaps.plot_erf_bulk(df_trans, min_dts, particle_path, mode="trans", silent=False)
    df_erf_rot = pd.read_csv(test_path + "erf_rot.csv")
    df_erf_trans = pd.read_csv(test_path + "erf_trans.csv")

    df_of_rot = icaps.plot_ornstein_fuerth(df_erf_rot, mode="rot", fit_bounds=("adapt", "adapt"),
                                           plot_out_path=test_path + "plots/", plot_attempts=True,
                                           adapt_margins=(4, 10))
    df_of_rot.to_csv(test_path + "of_rot.csv", index=False)
    df_of_trans = icaps.plot_ornstein_fuerth(df_erf_trans, mode="trans", fit_bounds=("adapt", "adapt"),
                                             plot_out_path=test_path + "plots/", plot_attempts=True,
                                             adapt_margins=(4, 10))
    df_of_trans.to_csv(test_path + "of_trans.csv", index=False)


if __name__ == '__main__':
    main()


