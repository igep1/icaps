import icaps
import numpy as np
import cv2
import pandas as pd
import matplotlib.pyplot as plt

# project_path = "D:/NOAH/ICAPS/TEXUS56-DATA/cca/cca_3/"
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM/"
contour_path = project_path + "contours/"
csv_path = project_path + "Trackfilter.csv"
temp_path = project_path + "rotation_temp/"
csv_rot_path = project_path + "Rot.csv"
csv_out_path = project_path + "TrackfilterRot.csv"
csv_filter_path = project_path + "TrackRot_LengthPhase.csv"
plot_path = project_path + "plots/"


def fit_ellipses():
    icaps.mk_folder(temp_path)
    df = pd.read_csv(csv_path)
    frames = np.unique(df["frame"].to_numpy())
    pb = icaps.ProgressBar(len(frames), "Analysing Rotation")
    for frame in frames:
        df_ellipse = pd.DataFrame(columns=["frame", "label", "ellipse_x", "ellipse_y", "semi_minor_axis",
                                           "semi_major_axis", "ellipse_angle"])
        df_ = df[df["frame"] == frame]
        contours = np.load(contour_path + "Os7-S1 Camera{:0>6}".format(frame) + ".npy", allow_pickle=True)
        labels = np.unique(df_["label"].to_numpy())
        for label in labels:
            contour = contours[label]
            try:
                box = cv2.fitEllipse(contour)
            except cv2.error:
                box = [(np.nan, np.nan), (np.nan, np.nan), np.nan]
            df_ellipse = df_ellipse.append(pd.DataFrame(data={"frame": [frame],
                                                              "label": [label],
                                                              "ellipse_x": [box[0][0]],
                                                              "ellipse_y": [box[0][1]],
                                                              "semi_minor_axis": [box[1][0]],
                                                              "semi_major_axis": [box[1][1]],
                                                              "ellipse_angle": [box[2]]}), sort=False)
        df_ellipse.to_csv(temp_path + "{:0>5}".format(frame) + ".csv", index=False)
        pb.tick()
    pb.finish()
    icaps.merge_dataframes(temp_path, csv_rot_path, delete_source=False, silent=False)

    df_ellipses = pd.read_csv(csv_rot_path)
    df = pd.merge(left=df, right=df_ellipses, on=["frame", "label"], how="left")
    df.to_csv(csv_out_path, index=False)


def investigate_angle():
    icaps.mk_folder(plot_path, clear=True)
    df = pd.read_csv(csv_out_path)
    # particles = np.unique(df[df["frame"] > 6000]["particle"].to_numpy())
    # print(particles)
    particles = [383294, 399602, 406987, 412675, 422119]
    for particle in particles:
        print(particle)
        df_ = df[df["particle"] == particle]
        angle = df_["ellipse_angle"].to_numpy()
        frame = df_["frame"].to_numpy()

        # angle_kinks = np.where(np.abs(np.gradient(angle)) > 80)[0]
        # angle_kinks = np.where(np.abs(np.diff(angle) > 150))[0]
        # kink_dist = np.diff(angle_kinks)
        # kink_delete = np.where(kink_dist < 10)[0]
        # angle_kinks = np.delete(angle_kinks, kink_delete)
        # kink_dist = np.diff(angle_kinks)
        # periods = kink_dist/1000

        angle_kinks_pos = np.where(np.diff(angle) > 170)[0]
        angle_kinks_neg = np.where(np.diff(angle) < -170)[0]
        # kink_dist = np.diff(angle_kinks)
        # kink_delete = np.where(kink_dist < 10)[0]
        # angle_kinks = np.delete(angle_kinks, kink_delete)
        # kink_dist = np.diff(angle_kinks)

        _, axes = plt.subplots(ncols=2, figsize=[6.4*2, 4.8])
        ax = axes[0]
        ax.set_title(str(particle))
        ax.scatter(frame, angle, s=1, c="black")
        ax.set_ylim(0, 180)
        for i in range(angle_kinks_pos.shape[0]):
            x_kinks = frame[angle_kinks_pos[i]]
            ax.axvline(x_kinks, color="red", linestyle="--")
        for i in range(angle_kinks_neg.shape[0]):
            x_kinks = frame[angle_kinks_neg[i]]
            ax.axvline(x_kinks, color="blue", linestyle="--")
        # plt.savefig(plot_path + "absolute_angle_" + "{:0>6}".format(particle) + ".png")
        # plt.close()

        # _, ax = plt.subplots()
        # ax.set_title(str(particle))
        ax = axes[1]
        angle_delta = np.diff(angle)
        ax.scatter(frame[:-1], angle_delta, s=1, c="black")
        ax.set_ylim(-np.amax(np.abs(angle_delta)), np.amax(np.abs(angle_delta)))

        ax.set_ylabel("Angle in °")
        ax.set_xlabel("Frame")
        # plt.savefig(plot_path + "angle_delta_" + "{:0>6}".format(particle) + ".png")
        plt.savefig(plot_path + "angle_" + "{:0>6}".format(particle) + ".png")
        plt.close()


def apply_filters():
    df = pd.read_csv(csv_out_path)
    df_phases = pd.read_csv(project_path + "Phases_Hand.csv", index_col=0)
    use_frames = df_phases[df_phases["phase"] == 0]["frame"].to_numpy()
    df_c = df.copy()
    particles = np.unique(df["particle"].to_numpy())
    pb = icaps.ProgressBar(len(particles), "Applying Filter")
    for particle in particles:
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy())
        for frame in frames:
            if frame not in use_frames:
                df_c = df_c[df_c["particle"] != particle]
                break
        pb.tick()
    pb.finish()
    df_c.to_csv(csv_filter_path, index=False)
    print(np.amin(df_c["frame"]), np.amax(df_c["frame"]))
    print(np.amin(df["frame"]), np.amax(df["frame"]))


def investigate_angular_displacement():
    # df = pd.read_csv(csv_filter_path)
    df = pd.read_csv(project_path + "TrackRot_LengthPhaseRatio15.csv")
    min_dt = 10
    # df = df.append(pd.DataFrame(columns=["angular_displacement_" + "{:0>3}".format(min_dt)]))
    # particles = np.unique(df["particle"].to_numpy())
    particles = [435818]
    pb = icaps.ProgressBar(len(particles), "Calculating Angular Displacement with min_dt=" + str(min_dt))
    for particle in particles:
        df_ = df[df["particle"] == particle].copy()
        # prev_idx = df_.head(1).index[0]
        # idcs = np.unique(df_.index.to_numpy()[1:])
        frames = np.unique(df_["frame"].to_numpy())
        if not len(frames) > 100:
            continue
        # prev_frame = frames[0]
        prev_idx = 0
        angular_displacement = np.empty(len(frames), dtype=np.float64)
        angular_displacement[:] = np.nan
        for i, frame in enumerate(frames[1:]):
            prev_frame = frames[prev_idx]
            prev_angle = df_[df_["frame"] == prev_frame]["ellipse_angle"].to_numpy()[0]
            if np.isnan(prev_angle):
                prev_idx += 1
                continue
            angle = df_[df_["frame"] == frame]["ellipse_angle"].to_numpy()[0]
            if np.isnan(angle):
                continue
            d_angle = angle - prev_angle
            dt = frame - prev_frame
            if dt >= min_dt:
                if np.abs(d_angle) > 90:
                    prev_idx += 1
                else:
                    # print(min_dt, frame, prev_frame, dt, angle, prev_angle)
                    angular_displacement[prev_idx] = d_angle / dt * min_dt
                    prev_idx = i + 1

        plt.close()
        _, axes = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
        ax = axes[0]
        ax.set_title("id: " + str(particle) + ", mass: " + str(np.round(np.amax(df_["mass"])/311, 2)))
        ax.scatter(frames, df_["ellipse_angle"], s=1, c="black")
        idcs = np.argwhere(np.isnan(angular_displacement))
        frames_d = np.delete(frames, idcs)
        angle_d = np.delete(df_["ellipse_angle"].to_numpy(), idcs)
        ax.plot(frames_d, angle_d, c="red")

        ax = axes[1]
        ax.set_title("min_dt=" + str(min_dt))
        ax.scatter(frames, angular_displacement, s=1, c="black")
        ax.set_ylim(-np.nanmax(np.abs(angular_displacement)), np.nanmax(np.abs(angular_displacement)))
        plt.show()
        plt.close()
        pb.tick()
    pb.finish()


def calc_angular_displacement(min_dt=1):
    # df = pd.read_csv(csv_filter_path)
    thresh_ratio = 20
    min_track_length = 100
    csv_final_filter_path = project_path + "TrackRot_LengthPhaseRatio" + str(thresh_ratio) + "L" + str(min_track_length) + ".csv"
    # before_nrows = len(df.index)
    # before_npart = len(np.unique(df["particle"].to_numpy()))
    # to_drop = []
    # pb = icaps.ProgressBar(before_nrows, "Filtering by Mass-Area-Ratio (" + str(thresh_ratio) + ")")
    # for i, row in df.iterrows():
    #     if row["mass"]/row["area"] < thresh_ratio:
    #         to_drop.append(i)
    #     pb.tick()
    # pb.finish()
    # df = df.drop(index=to_drop)
    # particles = np.unique(df["particle"].to_numpy())
    # pb = icaps.ProgressBar(len(particles), "Filtering by Tracklength (" + str(min_track_length) + ")")
    # for particle in particles:
    #     if len(df[df["particle"] == particle].index) < min_track_length:
    #         df = df[df["particle"] != particle]
    #     pb.tick()
    # pb.finish()
    # after_nrows = len(df.index)
    # after_npart = len(np.unique(df["particle"].to_numpy()))
    # print(before_nrows, after_nrows, str(np.round((1-after_nrows/before_nrows)*100, decimals=2)) + " %")
    # print(before_npart, after_npart, str(np.round((1-after_npart / before_npart)*100, decimals=2)) + " %")
    # df.to_csv(project_path + "TrackRot_LengthPhaseRatio" + str(thresh_ratio) + "L" + str(min_track_length) + ".csv",
    #           index=False)

    icaps.mk_folder(plot_path + "min_dt_{:0>3}/".format(min_dt), clear=True)

    csv_final_path = project_path + "final.csv"
    df = pd.read_csv(csv_final_path)
    key = "angular_displacement_" + "{:0>3}".format(min_dt)
    if key not in df.columns:
        df = df.append(pd.DataFrame(columns=[key]))
    particles = np.unique(df["particle"].to_numpy()).astype(np.uint64)
    pb = icaps.ProgressBar(len(particles), "Calculating Angular Displacement with min_dt=" + str(min_dt))
    for particle in particles:
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy()).astype(np.uint64)
        prev_idx = 0
        leaps = []
        for i, frame in enumerate(frames):
            if i == 0:
                continue
            prev_frame = frames[prev_idx]
            prev_angle = df_[df_["frame"] == prev_frame]["ellipse_angle"].to_numpy()[0]
            if np.isnan(prev_angle):
                prev_idx += 1
                continue
            angle = df_[df_["frame"] == frame]["ellipse_angle"].to_numpy()[0]
            if np.isnan(angle):
                continue
            d_angle = angle - prev_angle
            dt = frame - prev_frame
            if dt >= min_dt:
                if np.abs(d_angle) > 90:
                    leaps.append(prev_idx)
                    prev_idx += 1
                else:
                    table_idx = df_[df_["frame"] == prev_frame].index.to_numpy()[0]
                    df.at[table_idx, key] = d_angle / dt * min_dt
                    prev_idx = i

        angular_displacement = df[df["particle"] == particle][key].to_numpy().astype(np.float64)
        angle = df[df["particle"] == particle]["ellipse_angle"].to_numpy()
        leaps = np.array(leaps, dtype=np.uint64)

        plt.close()
        _, axes = plt.subplots(ncols=2, figsize=[6.4 * 2, 4.8])
        ax = axes[0]
        ax.set_axisbelow(True)
        ax.grid()
        ax.set_title(str(particle))
        ax.set_xlabel("Frame")
        ax.set_ylabel("Angle in °")
        idcs = np.argwhere(np.isnan(angular_displacement)).flatten()
        for leap in leaps:
            if leap in idcs:
                idcs = idcs[idcs != leap]
        leap_frames = frames[leaps]
        frames_cut = frames.copy().astype(np.float64)
        frames_cut = np.delete(frames_cut, idcs)
        angle_cut = angle.copy()
        angle_cut = np.delete(angle_cut, idcs)
        for leap_frame in leap_frames:
            idx = np.argwhere(frames_cut == leap_frame)[0]
            frames_cut = np.insert(frames_cut, idx+1, leap_frame + 0.5)
            angle_cut = np.insert(angle_cut, idx+1, np.nan)

        ax.plot(frames_cut, angle_cut, c="red", linewidth=1)
        ax.scatter(frames, angle, s=3, c="black")
        xlims = ax.get_xlim()

        ax = axes[1]
        ax.set_axisbelow(True)
        ax.grid()
        ax.set_title("min_dt=" + str(min_dt))
        ax.set_xlabel("Frame")
        ax.set_ylabel("Angular Displacement in °")
        ax.scatter(frames, angular_displacement, s=3, c="black")
        ax.set_ylim(-np.nanmax(np.abs(angular_displacement)), np.nanmax(np.abs(angular_displacement)))
        ax.set_xlim(xlims)
        plt.savefig(plot_path + "min_dt_{:0>3}/".format(min_dt) + "{:0>6}".format(particle) + "_{:0>3}".format(min_dt) + ".png")
        plt.close()
        pb.tick()
    pb.finish()
    df.to_csv(csv_final_path, index=False)


if __name__ == '__main__':
    # fit_ellipses()
    # investigate_angle()
    # apply_filters()
    # investigate_angular_displacement()

    min_dts = np.arange(1, 26, 1)
    min_dts = np.append(min_dts, np.arange(30, 51, 5))
    print(min_dts)
    for min_dt in min_dts:
        calc_angular_displacement(min_dt=min_dt)

