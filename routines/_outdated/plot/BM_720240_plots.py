import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import icaps


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT4/"
particle = 720240
plot_path = project_path + "{}/".format(particle)
chi_sqr_thresh = 2e-3    # least squares
# excluded_particles = (8781427,)
csv_of_chiex_path = project_path + "OF_Chi{}Ex.csv".format(chi_sqr_thresh)
csv_disp_trans_path = project_path + "DISP_Trans.csv"
csv_erf_trans_path = project_path + "ERF_Trans.csv"
csv_erf_trans_2_path = project_path + "ERF_Trans_2.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
csv_disp_rot_path = project_path + "DISP_Rot.csv"
csv_erf_rot_path = project_path + "ERF_Rot.csv"
csv_vmu_of_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/particles/0/of.csv"
dpi = 600
plt.rcParams.update({"font.size": 12})


# ============================================================
# TABLE PREPARATION
# ============================================================
df = pd.read_csv(csv_of_chiex_path)
print("chi² {}:\t\t".format(chi_sqr_thresh), len(df["particle"].unique()))
df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
df["tau"] = np.mean([df["tau_x"], df["tau_y"]], axis=0)
df["tau_err"] = 1 / 2 * np.sqrt(df["tau_x_err"] ** 2 + df["tau_y_err"] ** 2)
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df = df[df["mass_x_err"]/df["mass_x"] <= max_ratio]
    df = df[df["mass_y_err"] / df["mass_y"] <= max_ratio]
    print(len(df["particle"].unique()))
    df = df[df["tau_x_err"]/df["tau_x"] <= max_ratio]
    df = df[df["tau_y_err"] / df["tau_y"] <= max_ratio]
    print(len(df["particle"].unique()))


# ============================================================
# 720240 ERF PLOT
# ============================================================
# df_disp = pd.read_csv(csv_disp_trans_path)
# df_disp = df_disp[df_disp["particle"] == particle]
# axis = "x"
# dts = np.arange(1, 26, 1)
# dts = np.append(dts, np.arange(30, 51, 5))
# xlim = 5
# nan_arr = np.zeros(len(dts))
# nan_arr[:] = np.nan
# df_erf = pd.DataFrame(data={"particle": particle,
#                             "axis": axis,
#                             "dt": dts,
#                             "drift": nan_arr,
#                             "drift_err": nan_arr,
#                             "sigma": nan_arr,
#                             "sigma_err": nan_arr,
#                             "w0": nan_arr,
#                             "w0_err": nan_arr,
#                             "drift01": nan_arr,
#                             "drift01_err": nan_arr,
#                             "sigma0": nan_arr,
#                             "sigma0_err": nan_arr,
#                             "sigma1": nan_arr,
#                             "sigma1_err": nan_arr
#                             })
# icaps.mk_folder(plot_path + "erf_{}/".format(particle))
# pb = icaps.ProgressBar(len(dts), "{}".format(particle))
# for dt in dts:
#     dx_label = "d{}_{}".format(axis, dt)
#     df_disp_ = df_disp[["particle", dx_label]].dropna()
#     dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
#     pb.tick()
#
#     _, ax = plt.subplots(nrows=2)
#     x = np.sort(dx)
#     y = np.linspace(1 / len(x), 1, len(x))
#     ax[0].scatter(x, y, s=1, c="black")
#
#     out_single = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.0, 0.1 * dt], fit_type=2)
#     x_space = np.linspace(np.min(x), np.max(x), 1000)
#     ax[0].plot(x_space, icaps.fit_erf(x_space, *out_single.beta), c="blue", alpha=0.6, lw=2,
#                label="Single Gauss Fit"
#                # label=r"$\mu$" + " = ({:.5f} ".format(out_single.beta[0]) + r"$\pm$"
#                #       + " {:.5f}) µm\n".format(out_single.sd_beta[0])
#                #       + r"$\sigma$" + " = ({:.5f} ".format(out_single.beta[1]) + r"$\pm$"
#                #       + " {:.5f}) µm".format(out_single.sd_beta[1])
#                )
#
#     ax[1].scatter(x, y - icaps.fit_erf(x, *out_single.beta), c="blue", s=1, marker="o", zorder=1)
#     df_erf.loc[df_erf["dt"] == dt, "drift"] = out_single.beta[0] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "drift_err"] = out_single.sd_beta[0] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "sigma"] = out_single.beta[1] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "sigma_err"] = out_single.sd_beta[1] / 1e6
#
#     out = icaps.fit_model(icaps.fit_double_erf_center, x, y, p0=[0.65, 0.0, out_single.beta[1], out_single.beta[1]], fit_type=2)
#     x_space = np.linspace(np.min(x), np.max(x), 1000)
#     ax[0].plot(x_space, icaps.fit_double_erf_center(x_space, *out.beta), c="red", alpha=0.6, lw=2,
#                label="Double Gauss Fit\n"
#                      + r"$w_0$" + " = {:.3f} ".format(out.beta[0]) + r"$\pm$"
#                      + " {:.3f}\n".format(out.sd_beta[0])
#                      + r"$\mu_{01}$" + " = ({:.3f} ".format(out.beta[1]) + r"$\pm$"
#                      + " {:.3f}) µm\n".format(out.sd_beta[1])
#                      + r"$\sigma_0$" + " = ({:.3f} ".format(out.beta[2]) + r"$\pm$"
#                      + " {:.3f}) µm\n".format(out.sd_beta[2])
#                      + r"$\sigma_1$" + " = ({:.3f} ".format(out.beta[3]) + r"$\pm$"
#                      + " {:.3f}) µm".format(out.sd_beta[3])
#                )
#     ax[0].set_title(r"$\Delta$ t = {} ms".format(dt))
#     df_erf.loc[df_erf["dt"] == dt, "w0"] = out.beta[0]
#     df_erf.loc[df_erf["dt"] == dt, "w0_err"] = out.sd_beta[0]
#     df_erf.loc[df_erf["dt"] == dt, "drift01"] = out.beta[1] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "drift01_err"] = out.sd_beta[1] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "sigma0"] = out.beta[2] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "sigma0_err"] = out.sd_beta[2] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "sigma1"] = out.beta[3] / 1e6
#     df_erf.loc[df_erf["dt"] == dt, "sigma1_err"] = out.sd_beta[3] / 1e6
#
#     ax[0].set_ylabel("Normalized Cumulative Frequency")
#     ax[0].set_xlim(-xlim, xlim)
#     ax[1].scatter(x, y - icaps.fit_double_erf_center(x, *out.beta), c="red", s=1, marker="o", zorder=1)
#
#     ax[1].set_xlim(-xlim, xlim)
#     ax[0].legend(loc="upper left")
#     ax[1].set_ylabel("Residual")
#     ax[1].set_ylim(-0.017, 0.017)
#     ax[1].axhline(0, c="gray", lw=2, ls="--", zorder=0)
#     ax[1].set_xlabel(r"$\Delta x$ (µm)")
#     plt.savefig(plot_path + "erf_{}/erf_{}_{}.png".format(particle, axis, dt), dpi=dpi)
#     plt.close()
# pb.finish()
#
# df_erf.to_csv(plot_path + "erf_720240.csv", index=False)

# ============================================================
# 720240 OF PLOT SG
# ============================================================
df_of = pd.read_csv(csv_of_trans_path)
df_erf = pd.read_csv(csv_erf_trans_2_path)
df_of = df_of[df_of["particle"] == particle]
df_of = df_of[df_of["axis"] == "x"]
df_erf = df_erf[df_erf["particle"] == particle]
df_erf = df_erf[df_erf["axis"] == "x"]
dts = df_erf["dt"].to_numpy() / 1000
of_used = df_erf["of_used"].to_numpy()
sigma = df_erf["sigma"].to_numpy()
sigma_err = df_erf["sigma_err"].to_numpy()
sigma_sqr = sigma ** 2
sigma_sqr_err = 2 * sigma_err * sigma
mass = df_of["mass"].to_numpy()[0]
mass_err = df_of["mass_err"].to_numpy()[0]
tau = df_of["tau"].to_numpy()[0]
tau_err = df_of["tau_err"].to_numpy()[0]

_, ax = plt.subplots()
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel(r"$\Delta t$ (s)")
ax.set_ylabel(r"$\left<\Delta x^2\right>$ (m$^2$)")
ax.errorbar(dts, sigma_sqr, yerr=sigma_sqr_err, fmt=".", color="gray", markersize=10)
# ax.errorbar(dts[idx], sigma_sqr[idx], yerr=sigma_sqr_err[idx], fmt=".", color="black", markersize=10)
ax.errorbar(dts[of_used], sigma_sqr[of_used], yerr=sigma_sqr_err[of_used], fmt=".", color="black", markersize=10)
dt_range = np.linspace(np.min(dts), np.max(dts), 1000)
ax.plot(dt_range, icaps.fit_of_lin(dt_range, icaps.const.k_B * icaps.const.T / mass, tau), color="red", lw=2,
        linestyle="-", zorder=20,
        label="Ornstein-Fürth Fit\n" +
              r"$m$ = ({:.0f} $\pm$ {:.0f}) $\cdot$ {} kg".format(mass*1e15, mass_err*1e15, r"$10^{-15}$") + "\n"
              + r"{} = ({:.1f} $\pm$ {:.1f}) ms".format(r"$\tau_{t,x}$", tau*1e3, tau_err*1e3)
        )
xlims = ax.get_xlim()
dt_range = np.linspace(xlims[0], 5e-3, 1000)
ax.plot(dt_range, icaps.fit_para(dt_range, 1e-7, 0), color="black", lw=2,
        linestyle="-.", zorder=20,
        label="Ballistic"
        )
dt_range = np.linspace(5e-3, xlims[1], 1000)
ax.plot(dt_range, icaps.fit_lin(dt_range, .8e-9, 0), color="black", lw=2,
        linestyle="--", zorder=20,
        label="Diffusive"
        )
ax.legend(loc="lower right")
plt.savefig(plot_path + "OF_trans_x_{}.png".format(particle), dpi=dpi)
plt.close()

# ============================================================
# 720240 OF PLOT DG
# ============================================================
df_erf = pd.read_csv(plot_path + "erf_720240.csv")
_, ax = plt.subplots()
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel(r"$\Delta t$ (s)")
ax.set_ylabel(r"$\left<\Delta x^2\right>$ (m$^2$)")
dt_ms = np.squeeze(df_erf["dt"].to_numpy())
dt = dt_ms / 1e3
sigma2 = np.squeeze(df_erf["sigma"].to_numpy())
sigma2_sqr = sigma2 ** 2
sigma2_sqr_err = 2 * np.squeeze(df_erf["sigma_err"].to_numpy()) * sigma2

sigma0 = np.squeeze(df_erf["sigma0"].to_numpy())
sigma0_sqr = sigma0 ** 2
sigma0_sqr_err = 2 * np.squeeze(df_erf["sigma0_err"].to_numpy()) * sigma0
sigma1 = np.squeeze(df_erf["sigma1"].to_numpy())
sigma1_sqr = sigma1 ** 2
sigma1_sqr_err = 2 * np.squeeze(df_erf["sigma1_err"].to_numpy()) * sigma1
w0 = np.squeeze(df_erf["w0"].to_numpy())
w1 = 1 - w0
w_greater = (w1 > w0)
sigma_sqr = np.transpose(np.array([sigma0_sqr, sigma1_sqr]))
sigma_sqr_err = np.transpose(np.array([sigma0_sqr_err, sigma1_sqr_err]))
sigma_sqr_gr = [sigma_sqr[i, w_greater[i]] for i in range(len(w_greater))]
sigma_sqr_gr_err = [sigma_sqr_err[i, w_greater[i]] for i in range(len(w_greater))]
ax.errorbar(dt[~w_greater]*0.99, sigma0_sqr[~w_greater], yerr=sigma0_sqr_err[~w_greater], fmt=".", color="red",
            markersize=5, marker="^")
ax.errorbar(dt[w_greater]*0.99, sigma0_sqr[w_greater], yerr=sigma0_sqr_err[w_greater], fmt=".", color="red",
            markersize=5, marker="v")
ax.errorbar(dt[~w_greater]*1.01, sigma1_sqr[~w_greater], yerr=sigma1_sqr_err[~w_greater], fmt=".", color="green",
            markersize=5, marker="v")
ax.errorbar(dt[w_greater]*1.01, sigma1_sqr[w_greater], yerr=sigma1_sqr_err[w_greater], fmt=".", color="green",
            markersize=5, marker="^")
ax.errorbar(dt, sigma2_sqr, yerr=sigma2_sqr_err, fmt=".", color="blue", markersize=5, marker="o")
# ax.errorbar(dt, sigma_sqr_gr, yerr=sigma_sqr_gr_err, fmt=".", color="black", markersize=5, marker="o")
plt.show()


# ============================================================
# ROT TRACK FOR 720240
# ============================================================
dt = 1
df = pd.read_csv(csv_disp_rot_path)
df = df[df["particle"] == 720240]
df = df[(df["frame"] - np.min(df["frame"])) / 1000 < 1.82]
df = df[["frame", "gyrad", "focus", "semi_major_axis", "ellipse_angle", "omega_{}".format(dt)]].dropna()
# start_angle = np.deg2rad(360 - df["ellipse_angle"].to_numpy()[0] + 90)
start_angle = np.deg2rad(df["ellipse_angle"].to_numpy()[0] + 180)
disp = df["omega_{}".format(dt)].to_numpy() * -1
angle = np.cumsum(np.append([start_angle], disp))
# time = np.arange(dt, len(angle) + dt, dt) / 1000
time = np.append([0], (df["frame"].to_numpy() - np.min(df["frame"]) + 1) / 1000)
ckey = df["focus"].to_numpy()
ckey = np.append(ckey[0], ckey)
fig, ax = plt.subplots(subplot_kw={"projection": "polar"})
sct = ax.scatter(angle, time, c=ckey, s=4, cmap=plt.get_cmap("plasma"))
ax.plot(angle, time, c="black", lw=1, ls="-", alpha=0.5)
ax.set_rlabel_position(270)
label_position = ax.get_rlabel_position()
ax.text(np.radians(label_position - 10), ax.get_rmax() / 2., r"Time (s)", rotation=90,
        # rotation=label_position,
        ha="center", va="center")
ax.set_rlim(0, 1.05*np.max(time))
ax.minorticks_on()
# pos = ax.imshow(ckey, cmap="plasma", interpolation="none")
# fig.colorbar(pos, ax=ax)
fig.colorbar(sct, ax=ax, label="Sharpness (arb. units)", pad=0.07)
plt.savefig(plot_path + "Rot_Track_720240.png", dpi=dpi)
plt.close()

# ============================================================
# ERF TRANS COLLAGE FOR 720240
# ============================================================
# """
particle = 720240
df_disp = pd.read_csv(csv_disp_trans_path)
df_disp = df_disp[df_disp["particle"] == particle]
df_erf = pd.read_csv(csv_erf_trans_path)
df_erf = df_erf[df_erf["particle"] == particle]
df_erf = df_erf[df_erf["axis"] == "x"]
dts = [(1, 5), (10, 20), (35, 50)]
fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={'height_ratios': np.tile([2, 1], len(dts))},
                       figsize=[5.2, 6.4])
plt.subplots_adjust(hspace=.001, wspace=.001)
for j in range(len(dts) * 2):
    if j % 2 == 0:
        for i in range(len(dts[int(j/2)])):
            dt = dts[int(j/2)][i]
            ax_ = ax[j][i]
            ax_.minorticks_on()
            disp_column = "{}_{}".format("dx", dt)
            disp = np.sort(df_disp[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
            sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0] * 1e6
            drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0] * 1e6
            # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
            x_space = np.linspace(-11.5, 11.5, 1000)
            ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
            ax_.set_xlim(-11.5, 11.5)
            if int(j/2) < len(dts) - 1:
                ax_.set_xticks([])
            if i == 1:
                ax_.set_yticks([])
            # else:
            #     if j == 2:
            #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
            ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-11, 1), xycoords="data",
                         size=10, ha="left", va="top",
                         bbox=dict(boxstyle="square", fc="w"))
    else:
        for i in range(len(dts[int(j/2)])):
            dt = dts[int(j / 2)][i]
            ax_ = ax[j][i]
            ax_.minorticks_on()
            disp_column = "{}_{}".format("dx", dt)
            disp = np.sort(df_disp[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0] * 1e6
            drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0] * 1e6
            disp_res = norm_disp - icaps.fit_erf(disp, drift, sigma)
            ax_.scatter(disp, disp_res, c="black", s=1, marker="o", zorder=1)
            ax_.set_xlim(-11.5, 11.5)
            ax_.set_ylim(-0.06, 0.06)
            ax_.axhline(0, c="grey", zorder=0, lw=1, linestyle="--")
            ax_.yaxis.tick_right()
            # ax_.yaxis.set_axis_label("right")
            if int(j/2) < len(dts) - 1:
                ax_.set_xticks([])
            # else:
            #     ax_.set_xlabel(r"$dx$ (µm)")
            if i == 0:
                ax_.set_yticks([])
            # else:
            #     ax_.set_ylabel(r"Residual")
fig.text(0.50, 0.02, r"$\Delta x$ (µm)", ha="center")
fig.text(0.025, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
fig.subplots_adjust(bottom=0.08, top=0.95)
fig.subplots_adjust(right=0.88)
plt.savefig(plot_path + "ERF_trans_x_{}.png".format(particle), dpi=dpi)
plt.close()
# """

# ============================================================
# ERF ROT COLLAGE FOR 720240
# ============================================================
# """
particle = 720240
df_disp = pd.read_csv(csv_disp_rot_path)
df_disp = df_disp[df_disp["particle"] == particle]
df_erf = pd.read_csv(csv_erf_rot_path)
df_erf = df_erf[df_erf["particle"] == particle]
dts = [(1, 5), (10, 20), (35, 50)]
fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={'height_ratios': np.tile([2, 1], len(dts))},
                       figsize=[5.2, 6.4])
plt.subplots_adjust(hspace=.001, wspace=.001)
for j in range(len(dts) * 2):
    if j % 2 == 0:
        for i in range(len(dts[int(j / 2)])):
            dt = dts[int(j / 2)][i]
            ax_ = ax[j][i]
            ax_.minorticks_on()
            disp_column = "{}_{}".format("omega", dt)
            disp = np.sort(df_disp[disp_column].dropna().to_numpy())
            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
            sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0]
            drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0]
            # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
            x_space = np.linspace(-1, 1, 1000)
            ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
            ax_.set_xlim(-0.95, 0.95)
            if int(j / 2) < len(dts) - 1:
                ax_.set_xticks([])
            if i == 1:
                ax_.set_yticks([])
            # else:
            #     if j == 2:
            #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
            ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-0.9, 1), xycoords="data",
                         size=10, ha="left", va="top",
                         bbox=dict(boxstyle="square", fc="w"))
    else:
        for i in range(len(dts[int(j / 2)])):
            dt = dts[int(j / 2)][i]
            ax_ = ax[j][i]
            ax_.minorticks_on()
            disp_column = "{}_{}".format("omega", dt)
            disp = np.sort(df_disp[disp_column].dropna().to_numpy())
            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0]
            drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0]
            disp_res = icaps.fit_erf(disp, drift, sigma) - norm_disp
            ax_.scatter(disp, disp_res, c="black", s=1, marker="o", zorder=1)
            ax_.set_xlim(-0.95, 0.95)
            ax_.set_ylim(-0.06, 0.06)
            ax_.axhline(0, c="grey", zorder=0, lw=1, linestyle="--")
            ax_.yaxis.tick_right()
            if int(j / 2) < len(dts) - 1:
                ax_.set_xticks([])
            # else:
            #     ax_.set_xlabel(r"$dx$ (µm)")
            if i == 0:
                ax_.set_yticks([])
            # else:
            #     ax_.set_ylabel(r"Residual")
fig.text(0.50, 0.02, r"$\Delta \theta$ (rad)", ha="center")
fig.text(0.025, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
fig.subplots_adjust(bottom=0.08, top=0.95)
fig.subplots_adjust(right=0.88)
plt.savefig(plot_path + "ERF_rot_{}.png".format(particle), dpi=dpi)
plt.close()
# """


