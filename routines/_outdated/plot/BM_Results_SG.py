import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")

project_path = drive_letter + ":/icaps/data/LDM_ROT5/"
csv_path = project_path + "OF_Chi0.002Ex.csv"
df = pd.read_csv(csv_path)
df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
df["tau"] = np.mean([df["tau_x"], df["tau_y"]], axis=0)
df["tau_err"] = 1 / 2 * np.sqrt(df["tau_x_err"] ** 2 + df["tau_y_err"] ** 2)
print(len(df["particle"].unique()))
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df = df[df["mass_x_err"]/df["mass_x"] <= max_ratio]
    df = df[df["mass_y_err"] / df["mass_y"] <= max_ratio]
    print(len(df["particle"].unique()))
    df = df[df["tau_x_err"]/df["tau_x"] <= max_ratio]
    df = df[df["tau_y_err"] / df["tau_y"] <= max_ratio]
    print(len(df["particle"].unique()))

plot_path = project_path + "/plots/results/"
icaps.mk_folder(plot_path)

with open(plot_path + "info.txt", "w") as f:
    f.write("table\t\t" + csv_path.split("/")[-1] + "\n")
    f.write("n_parts\t\t" + str(len(df["particle"].unique())) + "\n")
    f.write("max_ratio\t" + str(max_ratio))

for axis in ["x", "y"]:
    _, ax = plt.subplots()
    ax.scatter(df["tau_{}".format(axis)], df["chisqr_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\tau_{}$ (s)".format(axis))
    ax.set_ylabel(r"$\chi_{}^2$".format(axis))
    ax.set_xlim(1e-3, 10)
    ax.set_ylim(1e-6, 2.2e-3)
    plt.savefig(plot_path + "tau_chisqr_{}.png".format(axis))
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["chisqr_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{}$ (kg)".format(axis))
    ax.set_ylabel(r"$\chi_{}^2$".format(axis))
    ax.set_xlim(1e-15, 2e-12)
    ax.set_ylim(1e-6, 2.2e-3)
    plt.savefig(plot_path + "mass_chisqr_{}.png".format(axis))
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["tau_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{}$ (kg)".format(axis))
    ax.set_ylabel(r"$\tau_{}$ (s)".format(axis))
    ax.set_xlim(1e-15, 2e-12)
    ax.set_ylim(1e-3, 10)
    plt.savefig(plot_path + "mass_tau_{}.png".format(axis))
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["mean_ex"], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{}$ (kg)".format(axis))
    ax.set_ylabel("Mean Intensity")
    ax.set_xlim(1e-15, 2e-12)
    plt.savefig(plot_path + "mass_int_{}.png".format(axis))
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["mass_{}_err".format(axis)]/df["mass_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{}$ in kg".format(axis))
    ax.set_ylabel(r"$\Delta m_{} / m_{}$".format(axis, axis))
    ax.set_xlim(1e-15, 2e-12)
    ax.set_ylim(1e-3, 0.3)
    plt.savefig(plot_path + "mass_massratio_{}.png".format(axis))
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["tau_{}".format(axis)], df["tau_{}_err".format(axis)]/df["tau_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\tau_{}$ (s)".format(axis))
    ax.set_ylabel(r"$\Delta\tau_{} / \tau_{}$".format(axis, axis))
    ax.set_xlim(1e-3, 10)
    ax.set_ylim(1e-3, 0.3)
    plt.savefig(plot_path + "tau_tauratio_{}.png".format(axis))
    plt.close()
