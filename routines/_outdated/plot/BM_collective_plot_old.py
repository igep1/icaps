import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import icaps


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT3/"
plot_path = project_path + "plots/"
chi_sqr_thresh = 10e-6    # least squares
excluded_particles = (8781427,)
csv_of_chiex_path = project_path + "OF_Chi{}Ex.csv".format(chi_sqr_thresh)
csv_disp_trans_path = project_path + "Displacements_Trans.csv"
csv_erf_trans_path = project_path + "Erf_Trans.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
csv_vmu_of_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/particles/0/of.csv"
dpi = 600
plt.rcParams.update({"font.size": 10})


def main():
    # ============================================================
    # TABLE PREPARATION
    # ============================================================
    df_full = pd.read_csv(csv_of_chiex_path)
    print("chi² {}:\t\t".format(chi_sqr_thresh), len(np.unique(df_full["particle"].to_numpy())))
    df = df_full.copy()
    df = df.dropna(subset=["mass_x", "mass_y"])
    df = df[~df["particle"].isin(excluded_particles)]
    df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
    df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
    print("x and y:\t\t", len(np.unique(df["particle"].to_numpy())))
    print("rot:\t\t\t", len(np.unique(df.dropna(subset=["inertia"])["particle"].to_numpy())))
    mass_thresh = -0.8
    df_mass = df[np.log10(df["mass_x_err"] / df["mass_x"]) <= mass_thresh]
    df_mass = df_mass[np.log10(df_mass["mass_y_err"] / df_mass["mass_y"]) <= mass_thresh]
    df_temp = df_mass.copy()
    df_temp["ratio1"] = np.amax([df_temp["mass_x"] / df_temp["mass_y"], df_temp["mass_y"] / df_temp["mass_x"]], axis=0)
    df_temp["ratio2"] = abs(df_temp["mass_x"] / df_temp["mass_y"] - 1)
    # print(df_temp.sort_values("ratio2", ascending=False).head(10))
    print("mass logfilter:\t", len(np.unique(df["particle"].to_numpy())))
    tau_thresh = -0.8
    df_tau = df_mass[np.log10(df_mass["tau_x_err"] / df_mass["tau_x"]) <= tau_thresh]
    df_tau = df_tau[np.log10(df_tau["tau_y_err"] / df_tau["tau_y"]) <= tau_thresh]
    print("tau logfilter:\t", len(np.unique(df_tau["particle"].to_numpy())))
    # df_tau.to_csv(project_path + "out.csv")

    # ============================================================
    # OF PLOTS
    # ============================================================
    df_erf = pd.read_csv(project_path + "ERF_Trans.csv")
    particles = df_mass["particle"].unique()
    icaps.mk_folder(plot_path + "of_double/")
    axes = ["x", "y"]
    pb = icaps.ProgressBar(len(particles) * len(axes), "Plotting OF")
    for particle in particles:
        for axis in axes:
            df_erf_ = df_erf[df_erf["particle"] == particle]
            df_erf_ = df_erf_[df_erf_["axis"] == axis]
            _, ax = plt.subplots()
            ax.set_xscale("log")
            ax.set_yscale("log")
            ax.set_xlabel(r"$\Delta t$ (s)")
            ax.set_ylabel(r"$\left<\Delta x^2\right>$ (m$^2$)")
            dt_ms = np.squeeze(df_erf_["dt"].to_numpy())
            dt = dt_ms / 1e3
            sigma2 = np.squeeze(df_erf_["sigma"].to_numpy())
            sigma2_sqr = sigma2 ** 2
            sigma2_sqr_err = 2 * np.squeeze(df_erf_["sigma_err"].to_numpy()) * sigma2

            sigma0 = np.squeeze(df_erf_["sigma0"].to_numpy())
            sigma0_sqr = sigma0 ** 2
            sigma0_sqr_err = 2 * np.squeeze(df_erf_["sigma0_err"].to_numpy()) * sigma0
            sigma1 = np.squeeze(df_erf_["sigma1"].to_numpy())
            sigma1_sqr = sigma1 ** 2
            sigma1_sqr_err = 2 * np.squeeze(df_erf_["sigma1_err"].to_numpy()) * sigma1
            w0 = np.squeeze(df_erf_["w0"].to_numpy())
            w1 = 1 - w0
            w_greater = (w1 > w0)
            # sigma_sqr = np.transpose(np.array([sigma0_sqr, sigma1_sqr]))
            # sigma_sqr_err = np.transpose(np.array([sigma0_sqr_err, sigma1_sqr_err]))
            # sigma_sqr_gr = [sigma_sqr[i, w_greater[i]] for i in range(len(w_greater))]
            # sigma_sqr_gr_err = [sigma_sqr_err[i, w_greater[i]] for i in range(len(w_greater))]
            ax.errorbar(dt[~w_greater] * 0.99, sigma0_sqr[~w_greater], yerr=sigma0_sqr_err[~w_greater], fmt=".",
                        color="red", markersize=5, marker="^", markerfacecolor="none")
            ax.errorbar(dt[w_greater] * 0.99, sigma0_sqr[w_greater], yerr=sigma0_sqr_err[w_greater], fmt=".",
                        color="red", markersize=5, marker="v", markerfacecolor="none")
            ax.errorbar(dt[~w_greater] * 1.01, sigma1_sqr[~w_greater], yerr=sigma1_sqr_err[~w_greater], fmt=".",
                        color="green", markersize=5, marker="v", markerfacecolor="none")
            ax.errorbar(dt[w_greater] * 1.01, sigma1_sqr[w_greater], yerr=sigma1_sqr_err[w_greater], fmt=".",
                        color="green", markersize=5, marker="^", markerfacecolor="none")
            ax.errorbar(dt, sigma2_sqr, yerr=sigma2_sqr_err, fmt=".", color="blue", markersize=5, marker="o",
                        markerfacecolor="none")
            # ax.errorbar(dt, sigma_sqr_gr, yerr=sigma_sqr_gr_err, fmt=".", color="black", markersize=5, marker="o")
            ax.set_ylim(0.8 * np.min([sigma0_sqr[0], sigma1_sqr[0], sigma2_sqr[0]]),
                        1.2 * np.max([sigma0_sqr[-1], sigma1_sqr[-1], sigma2_sqr[-1]]))
            plt.savefig(plot_path + "of_double/{}_{}.png".format(particle, axis), dpi=dpi)
            plt.close()
            pb.tick()
    pb.finish()

    # ============================================================
    # 720240 ERF PLOT
    # ============================================================
    particle = 720240
    # df_disp = pd.read_csv(csv_disp_trans_path)
    # df_disp = df_disp[df_disp["particle"] == particle]
    # axis = "x"
    # dts = np.arange(1, 26, 1)
    # dts = np.append(dts, np.arange(30, 51, 5))
    # xlim = 5
    # nan_arr = np.zeros(len(dts))
    # nan_arr[:] = np.nan
    # df_erf = pd.DataFrame(data={"particle": particle,
    #                             "axis": axis,
    #                             "dt": dts,
    #                             "drift": nan_arr,
    #                             "drift_err": nan_arr,
    #                             "sigma": nan_arr,
    #                             "sigma_err": nan_arr,
    #                             "w0": nan_arr,
    #                             "w0_err": nan_arr,
    #                             "drift0": nan_arr,
    #                             "drift0_err": nan_arr,
    #                             "drift1": nan_arr,
    #                             "drift1_err": nan_arr,
    #                             "sigma0": nan_arr,
    #                             "sigma0_err": nan_arr,
    #                             "sigma1": nan_arr,
    #                             "sigma1_err": nan_arr
    #                             })
    # icaps.mk_folder(plot_path + "erf_{}/".format(particle))
    # pb = icaps.ProgressBar(len(dts), "{}".format(particle))
    # for dt in dts:
    #     dx_label = "d{}_{}".format(axis, dt)
    #     df_disp_ = df_disp[["particle", dx_label]].dropna()
    #     dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
    #     pb.tick()
    #
    #     _, ax = plt.subplots(nrows=2)
    #     x = np.sort(dx)
    #     y = np.linspace(1 / len(x), 1, len(x))
    #     ax[0].scatter(x, y, s=1, c="black")
    #
    #     out_single = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.0, 0.1 * dt], fit_type=2)
    #     x_space = np.linspace(np.min(x), np.max(x), 1000)
    #     ax[0].plot(x_space, icaps.fit_erf(x_space, *out_single.beta), c="blue", alpha=0.6, lw=2,
    #                label=r"$\mu$" + " = ({:.5f} ".format(out_single.beta[0]) + r"$\pm$"
    #                      + " {:.5f}) µm\n".format(out_single.sd_beta[0])
    #                      + r"$\sigma$" + " = ({:.5f} ".format(out_single.beta[1]) + r"$\pm$"
    #                      + " {:.5f}) µm".format(out_single.sd_beta[1])
    #                )
    #
    #     ax[1].scatter(x, y - icaps.fit_erf(x, *out_single.beta), c="blue", s=1, marker="o", zorder=1)
    #     df_erf.loc[df_erf["dt"] == dt, "drift"] = out_single.beta[0] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "drift_err"] = out_single.sd_beta[0] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "sigma"] = out_single.beta[1] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "sigma_err"] = out_single.sd_beta[1] / 1e6
    #
    #     out = icaps.fit_model(icaps.fit_double_erf, x, y, p0=[0.65, 0.0, 0.0, out_single.beta[1], out_single.beta[1]], fit_type=2)
    #     x_space = np.linspace(np.min(x), np.max(x), 1000)
    #     ax[0].plot(x_space, icaps.fit_double_erf(x_space, *out.beta), c="red", alpha=0.6, lw=2,
    #                label=r"$\Delta t = {} \, \rm ms$".format(dt) + "\n"
    #                      + r"$a$" + " = {:.5f} ".format(out.beta[0]) + r"$\pm$"
    #                      + " {:.5f}\n".format(out.sd_beta[0])
    #                      + r"$\mu_0$" + " = ({:.5f} ".format(out.beta[1]) + r"$\pm$"
    #                      + " {:.5f}) µm\n".format(out.sd_beta[1])
    #                      + r"$\mu_1$" + " = ({:.5f} ".format(out.beta[2]) + r"$\pm$"
    #                      + " {:.5f}) µm\n".format(out.sd_beta[2])
    #                      + r"$\sigma_0$" + " = ({:.5f} ".format(out.beta[3]) + r"$\pm$"
    #                      + " {:.5f}) µm\n".format(out.sd_beta[3])
    #                      + r"$\sigma_1$" + " = ({:.5f} ".format(out.beta[4]) + r"$\pm$"
    #                      + " {:.5f}) µm".format(out.sd_beta[4])
    #                )
    #     df_erf.loc[df_erf["dt"] == dt, "w0"] = out.beta[0]
    #     df_erf.loc[df_erf["dt"] == dt, "w0_err"] = out.sd_beta[0]
    #     df_erf.loc[df_erf["dt"] == dt, "drift0"] = out.beta[1] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "drift0_err"] = out.sd_beta[1] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "drift1"] = out.beta[2] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "drift1_err"] = out.sd_beta[2] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "sigma0"] = out.beta[3] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "sigma0_err"] = out.sd_beta[3] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "sigma1"] = out.beta[4] / 1e6
    #     df_erf.loc[df_erf["dt"] == dt, "sigma1_err"] = out.sd_beta[4] / 1e6
    #
    #     ax[0].set_ylabel("Normalized Cumulative Frequency")
    #     ax[0].set_xlim(-xlim, xlim)
    #     ax[1].scatter(x, y - icaps.fit_double_erf(x, *out.beta), c="red", s=1, marker="o", zorder=1)
    #
    #     ax[1].set_xlim(-xlim, xlim)
    #     ax[0].legend(loc="upper left")
    #     ax[1].set_ylabel("Residual")
    #     ax[1].set_ylim(-0.017, 0.017)
    #     ax[1].axhline(0, c="gray", lw=2, ls="--", zorder=0)
    #     ax[1].set_xlabel(r"$\Delta x$ (µm)")
    #     plt.savefig(plot_path + "erf_{}/erf_{}_{}.png".format(particle, axis, dt), dpi=dpi)
    #     plt.close()
    # pb.finish()
    #
    # df_erf.to_csv(project_path + "erf_720240.csv", index=False)

    # ============================================================
    # 720420 OF PLOTS
    # ============================================================
    # df_erf = pd.read_csv(project_path + "erf_720240.csv")
    # icaps.mk_folder(plot_path + "of_{}/".format(particle))
    # _, ax = plt.subplots()
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    # ax.set_xlabel(r"$\Delta t$ (s)")
    # ax.set_ylabel(r"$\left<\Delta x^2\right>$ (m$^2$)")
    # dt_ms = np.squeeze(df_erf["dt"].to_numpy())
    # dt = dt_ms / 1e3
    # sigma2 = np.squeeze(df_erf["sigma"].to_numpy())
    # sigma2_sqr = sigma2 ** 2
    # sigma2_sqr_err = 2 * np.squeeze(df_erf["sigma_err"].to_numpy()) * sigma2
    #
    # sigma0 = np.squeeze(df_erf["sigma0"].to_numpy())
    # sigma0_sqr = sigma0 ** 2
    # sigma0_sqr_err = 2 * np.squeeze(df_erf["sigma0_err"].to_numpy()) * sigma0
    # sigma1 = np.squeeze(df_erf["sigma1"].to_numpy())
    # sigma1_sqr = sigma1 ** 2
    # sigma1_sqr_err = 2 * np.squeeze(df_erf["sigma1_err"].to_numpy()) * sigma1
    # w0 = np.squeeze(df_erf["w0"].to_numpy())
    # w1 = 1 - w0
    # w_greater = (w1 > w0)
    # sigma_sqr = np.transpose(np.array([sigma0_sqr, sigma1_sqr]))
    # sigma_sqr_err = np.transpose(np.array([sigma0_sqr_err, sigma1_sqr_err]))
    # sigma_sqr_gr = [sigma_sqr[i, w_greater[i]] for i in range(len(w_greater))]
    # sigma_sqr_gr_err = [sigma_sqr_err[i, w_greater[i]] for i in range(len(w_greater))]
    # ax.errorbar(dt[~w_greater]*0.99, sigma0_sqr[~w_greater], yerr=sigma0_sqr_err[~w_greater], fmt=".", color="red",
    #             markersize=5, marker="^")
    # ax.errorbar(dt[w_greater]*0.99, sigma0_sqr[w_greater], yerr=sigma0_sqr_err[w_greater], fmt=".", color="red",
    #             markersize=5, marker="v")
    # ax.errorbar(dt[~w_greater]*1.01, sigma1_sqr[~w_greater], yerr=sigma1_sqr_err[~w_greater], fmt=".", color="green",
    #             markersize=5, marker="v")
    # ax.errorbar(dt[w_greater]*1.01, sigma1_sqr[w_greater], yerr=sigma1_sqr_err[w_greater], fmt=".", color="green",
    #             markersize=5, marker="^")
    # ax.errorbar(dt, sigma2_sqr, yerr=sigma2_sqr_err, fmt=".", color="blue", markersize=5, marker="o")
    # # ax.errorbar(dt, sigma_sqr_gr, yerr=sigma_sqr_gr_err, fmt=".", color="black", markersize=5, marker="o")
    # plt.show()

    # ============================================================
    # COLLECTIVE ERF PLOT
    # ============================================================
    particles = df_tau["particle"].unique()
    df_disp = pd.read_csv(csv_disp_trans_path)
    df_disp = df_disp[df_disp["particle"].isin(particles)]
    df_erf = pd.read_csv(csv_erf_trans_path)
    df_erf = df_erf[df_erf["particle"].isin(particles)]
    axis = "x"
    dts = np.arange(1, 26, 1)
    dts = np.append(dts, np.arange(30, 51, 5))
    xlim = 5
    icaps.mk_folder(plot_path + "erf_collective_combined/")
    pb = icaps.ProgressBar(len(dts) * len(particles), "Combined")
    for dt in dts:
        dx_label = "d{}_{}".format(axis, dt)
        df_disp_ = df_disp[["particle", dx_label]].dropna()
        df_erf_ = df_erf[df_erf["dt"] == dt]
        df_erf_ = df_erf_[df_erf_["axis"] == axis]
        xi = []
        for particle in particles:
            drift = df_erf_.loc[df_erf_["particle"] == particle, "drift"].to_numpy()[0] * 1e6
            sigma = df_erf_.loc[df_erf_["particle"] == particle, "sigma"].to_numpy()[0] * 1e6
            dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
            xi_ = (dx - drift) / sigma
            xi.extend(xi_)
            pb.tick()
        xi = np.array(xi)

        _, ax = plt.subplots(nrows=2)
        x = np.sort(xi)
        y = np.linspace(1 / len(x), 1, len(x))
        ax[0].scatter(x, y, s=1, c="black")
        out = icaps.fit_model(icaps.fit_double_erf, x, y, p0=[0.65, 0.0, 0.0, 1.0, 2.0], fit_type=2)
        x_space = np.linspace(np.min(x), np.max(x), 1000)
        ax[0].plot(x_space, icaps.fit_double_erf(x_space, *out.beta), c="red", alpha=0.6, lw=2,
                   label=r"$\Delta t = {} \, \rm ms$".format(dt) + "\n"
                         + r"$a$" + " = {:.5f} ".format(out.beta[0]) + r"$\pm$"
                         + " {:.5f}\n".format(out.sd_beta[0])
                         + r"$\mu_0$" + " = {:.5f} ".format(out.beta[1]) + r"$\pm$"
                         + " {:.5f}\n".format(out.sd_beta[1])
                         + r"$\mu_1$" + " = {:.5f} ".format(out.beta[2]) + r"$\pm$"
                         + " {:.5f}\n".format(out.sd_beta[2])
                         + r"$\sigma_0$" + " = {:.5f} ".format(out.beta[3]) + r"$\pm$"
                         + " {:.5f}\n".format(out.sd_beta[3])
                         + r"$\sigma_1$" + " = {:.5f} ".format(out.beta[4]) + r"$\pm$"
                         + " {:.5f}".format(out.sd_beta[4])
                   )
        ax[0].legend(loc="upper left")
        ax[0].set_ylabel("Normalized Cumulative Frequency")
        ax[0].set_xlim(-xlim, xlim)
        ax[1].scatter(x, y - icaps.fit_double_erf(x, *out.beta), c="red", s=1, marker="o", zorder=1)
        out = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.0, 1.0], fit_type=2)
        x_space = np.linspace(np.min(x), np.max(x), 1000)
        ax[0].plot(x_space, icaps.fit_erf(x_space, *out.beta), c="blue", alpha=0.6, lw=2,
                   label=r"$\Delta t = {} \, \rm ms$".format(dt) + "\n"
                         + r"$\mu$" + " = {:.5f} ".format(out.beta[0]) + r"$\pm$"
                         + " {:.5f}\n".format(out.sd_beta[0])
                         + r"$\sigma$" + " = {:.5f} ".format(out.beta[1]) + r"$\pm$"
                         + " {:.5f}\n".format(out.sd_beta[1])
                   )
        ax[1].scatter(x, y - icaps.fit_erf(x, *out.beta), c="blue", s=1, marker="o", zorder=1)
        ax[1].set_xlim(-xlim, xlim)
        ax[1].set_ylabel("Residual")
        ax[1].set_ylim(-0.017, 0.017)
        ax[1].axhline(0, c="gray", lw=2, ls="--", zorder=0)
        ax[1].set_xlabel(r"$\Delta\xi$")
        plt.savefig(plot_path + "erf_collective_combined/erf_coll_{}_{}.png".format(axis, dt), dpi=dpi)
        plt.close()
    pb.finish()

    # ============================================================
    # COLLECTIVE ERF PLOT
    # ============================================================
    particles = df_tau["particle"].unique()
    df_disp = pd.read_csv(csv_disp_trans_path)
    df_disp = df_disp[df_disp["particle"].isin(particles)]
    df_erf = pd.read_csv(csv_erf_trans_path)
    df_erf = df_erf[df_erf["particle"].isin(particles)]
    axis = "x"
    dts = np.arange(1, 26, 1)
    dts = np.append(dts, np.arange(30, 51, 5))
    xlim = 5
    icaps.mk_folder(plot_path + "erf_collective_single/")
    pb = icaps.ProgressBar(len(dts) * len(particles), "Single")
    for dt in dts:
        dx_label = "d{}_{}".format(axis, dt)
        df_disp_ = df_disp[["particle", dx_label]].dropna()
        df_erf_ = df_erf[df_erf["dt"] == dt]
        df_erf_ = df_erf_[df_erf_["axis"] == axis]
        _, ax = plt.subplots(nrows=2)
        for particle in particles:
            drift = df_erf_.loc[df_erf_["particle"] == particle, "drift"].to_numpy()[0] * 1e6
            sigma = df_erf_.loc[df_erf_["particle"] == particle, "sigma"].to_numpy()[0] * 1e6
            dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
            xi_ = (dx - drift) / sigma
            x = np.sort(xi_)
            y = np.linspace(1 / len(x), 1, len(x))
            out = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.1, 0.1], fit_type=2)
            ax[0].scatter(x, y, s=1, c="black")
            x_space = np.linspace(np.min(x), np.max(x), 1000)
            ax[0].plot(x_space, icaps.fit_erf(x_space, *out.beta), c="red", alpha=0.6, lw=2)
            res = y - icaps.fit_erf(x, *out.beta)
            ax[1].scatter(x, res, c="black", s=1, marker="o", zorder=1, alpha=0.005)
            pb.tick()

        # ax[0].legend(loc="upper left")
        ax[0].set_ylabel("Normalized Cumulative Frequency")
        ax[0].set_xlim(-xlim, xlim)
        ax[1].set_xlim(-xlim, xlim)
        ax[1].set_ylabel("Residual")
        ax[1].set_ylim(-0.10, 0.10)
        ax[1].axhline(0, c="gray", lw=2, ls="--", zorder=0)
        ax[1].set_xlabel(r"$\Delta\xi$")
        plt.savefig(plot_path + "erf_collective_single/erf_coll_{}_{}.png".format(axis, dt), dpi=dpi)
        plt.close()
    pb.finish()


if __name__ == '__main__':
    main()

