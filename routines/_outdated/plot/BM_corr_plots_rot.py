import numpy as np
import pandas as pd
import icaps
import matplotlib.pyplot as plt
import matplotlib.colorbar
import matplotlib.colors
from itertools import cycle

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT3/"
plot_path = project_path + "plots_dg/"
# chi_sqr_thresh = 0.5e-6   # odr
chi_sqr_thresh = 10e-6    # least squares
excluded_particles = (8781427,)
csv_of_chiex_path = project_path + "OF_Chi{}Ex.csv".format(chi_sqr_thresh)
csv_disp_rot_path = project_path + "Displacement_Rot.csv"
csv_erf_rot_path = project_path + "Erf_Rot.csv"
csv_of_rot_path = project_path + "OF_Rot.csv"
csv_disp_trans_path = project_path + "Displacements_Trans.csv"
csv_erf_trans_path = project_path + "Erf_Trans.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
csv_vmu_of_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/particles/0/of.csv"
dpi = 600
plt.rcParams.update({"font.size": 10})

filter_type = "or"
w_thresh = 0.9
dg_min_chi_factor = 3
max_min_thresh = 2


def main():
    # ============================================================
    # TABLE PREPARATION
    # ============================================================
    df_full = pd.read_csv(csv_of_chiex_path)
    print("chi² {}:\t\t".format(chi_sqr_thresh), len(np.unique(df_full["particle"].to_numpy())))
    df = df_full.copy()
    df = df.dropna(subset=["mass_x", "mass_y"])
    df = df[~df["particle"].isin(excluded_particles)]
    df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
    df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
    print("x and y:\t\t", len(np.unique(df["particle"].to_numpy())))
    print("rot:\t\t\t", len(np.unique(df.dropna(subset=["inertia"])["particle"].to_numpy())))
    mass_thresh = -0.8
    df_mass = df[np.log10(df["mass_x_err"] / df["mass_x"]) <= mass_thresh]
    df_mass = df_mass[np.log10(df_mass["mass_y_err"] / df_mass["mass_y"]) <= mass_thresh]
    df_temp = df_mass.copy()
    df_temp["ratio1"] = np.amax([df_temp["mass_x"] / df_temp["mass_y"], df_temp["mass_y"] / df_temp["mass_x"]], axis=0)
    df_temp["ratio2"] = abs(df_temp["mass_x"] / df_temp["mass_y"] - 1)
    # print(df_temp.sort_values("ratio2", ascending=False).head(10))
    print("mass logfilter:\t", len(np.unique(df["particle"].to_numpy())))
    tau_thresh = -0.8
    df_tau = df_mass[np.log10(df_mass["tau_x_err"] / df_mass["tau_x"]) <= tau_thresh]
    df_tau = df_tau[np.log10(df_tau["tau_y_err"] / df_tau["tau_y"]) <= tau_thresh]
    print("tau logfilter:\t", len(np.unique(df_tau["particle"].to_numpy())))
    # df_tau.to_csv(project_path + "out.csv")

    df_mass = df_mass.dropna(subset=["inertia"])

    # ============================================================
    # Sigma over Mass
    # ============================================================
    csv_erf_corr_path = project_path + "erf_corr.csv"
    csv_of_corr_path = project_path + "of_corr.csv"
    csv_of_corr_fit_path = project_path + "of_corr_fit.csv"
    dts = np.arange(1, 26, 1)
    dts = np.append(dts, np.arange(30, 51, 5))
    axis = "angle"
    particles = np.unique(df_mass["particle"].to_numpy())
    start_frame = np.zeros(len(particles), dtype=int)
    for i, particle in enumerate(particles):
        start_frame[i] = df_mass.loc[df_mass["particle"] == particle, "start_frame"].to_numpy()[0]

    # _, ax = plt.subplots()
    # start_frame = np.sort(start_frame)
    # norm_start_frame = np.linspace(1 / len(start_frame), 1, len(start_frame))
    # ax.scatter(start_frame, norm_start_frame, c="black", s=5)
    # plt.show()

    # particles = particles[start_frame < 150000]
    # particles = particles[start_frame > 250000]
    df_mass = df_mass[df_mass["particle"].isin(particles)]

    mean_ex = df_mass["mean_ex"].to_numpy()
    # mean_ex = np.repeat(mean_ex, 2)
    mean_ex_err = df_mass["mean_ex_err"].to_numpy()
    mass = df_mass["mass"].to_numpy()
    mass_err = df_mass["mass_err"].to_numpy()
    # mean_ex = df_tau["mean_ex"].to_numpy()
    # mean_ex_err = df_tau["mean_ex_err"].to_numpy()
    # mass = df_tau["mass"].to_numpy()
    # mass_err = df_tau["mass_err"].to_numpy()

    # _, ax = plt.subplots()
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    # # ax.set_ylim(10 ** -15, 10 ** -11)
    # # ax.scatter(df_tau["mean_ex"], df_tau["mass_x"], s=10, c="black")
    # print(icaps.const.m_p)
    # ax.errorbar(mean_ex, mass/icaps.const.m_p, xerr=mean_ex_err, yerr=mass_err/icaps.const.m_p, fmt=".", color="black", markersize=2, marker="o")
    # xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    # # sx = mean_ex_err / mean_ex / np.log(10)
    # # sy = mass_err / mass / np.log(10)
    # # out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex), np.log10(mass), xerr=sx, yerr=sy, fit_type=0)
    # # ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
    # #         label="a * x + b\n" + "a = {0:.4f} \u00b1 {1:.4f}\n".format(out.beta[0], out.sd_beta[0]) +
    # #         "b = {0:.4f} \u00b1 {1:.4f}".format(out.beta[1], out.sd_beta[1]))
    # out = icaps.fit_model(icaps.fit_lin, mean_ex, mass/icaps.const.m_p, xerr=mean_ex_err, yerr=mass_err/icaps.const.m_p, fit_type=0)
    # ax.plot(xspace, icaps.fit_lin(xspace, *out.beta), c="red", ls="-", lw=2,
    #         label="a * x + b\n" + "a = {0:.4f} \u00b1 {1:.4f}\n".format(out.beta[0], out.sd_beta[0]) +
    #               "b = {0:.4f} \u00b1 {1:.4f}".format(out.beta[1], out.sd_beta[1]))
    # ax.legend()
    # ax.set_xlabel("Mean Extinction (arb. units)")
    # ax.set_ylabel("Mean Mass (monomers)")
    # plt.show()

    df_erf = pd.read_csv(csv_erf_rot_path)
    df_erf = df_erf[df_erf["particle"].isin(particles)]
    df_erf_corr = df_erf[["particle", "dt", "axis", "sigma", "sigma_err", "chi_sqr"]].copy()
    df_erf_corr["sigma"] = np.nan
    df_erf_corr["sigma_fit"] = np.nan
    df_erf_corr["sigma_err"] = 0.
    df_erf_corr["sigma_fit_err"] = 0.
    df_erf_corr = df_erf_corr[df_erf_corr["axis"] == axis]
    df_erf_corr = df_erf_corr[df_erf_corr["particle"].isin(particles)]
    icaps.mk_folder(plot_path + "corr_sigma_mass_rot/", clear=True)
    icaps.mk_folder(plot_path + "corr_sigma_mass_2_rot/", clear=True)
    icaps.mk_folder(plot_path + "corr_sigma_mass_3_rot/", clear=True)
    icaps.mk_folder(plot_path + "corr_chi_weights_rot/", clear=True)
    icaps.mk_folder(plot_path + "corr_chi_sigratio_rot/", clear=True)
    icaps.mk_folder(plot_path + "corr_weights_sigratio_rot/", clear=True)
    pb = icaps.ProgressBar(len(dts), "Sigma over Mass")
    for dt in dts:
        df_erf_ = df_erf[df_erf["axis"] == axis]
        df_erf_ = df_erf_[df_erf_["dt"] == dt]
        # df_erf_ = df_erf[df_erf["dt"] == dt]
        sig0 = np.squeeze(df_erf_["sigma0"].to_numpy())
        sig1 = np.squeeze(df_erf_["sigma1"].to_numpy())
        sig = np.squeeze(df_erf_["sigma"].to_numpy())
        chisqr_erf = np.squeeze(df_erf_["chi_sqr"].to_numpy())
        chisqr_erf_double = np.squeeze(df_erf_["chi_sqr_double"].to_numpy())
        w0 = np.squeeze(df_erf_["w0"].to_numpy())
        w1 = 1. - w0
        sig0[sig0 <= 0] = sig[sig0 <= 0]
        sig1[sig1 <= 0] = sig[sig1 <= 0]
        sigwmax = np.array([sig0[i] if w0[i] > w1[i] else sig1[i] for i in range(len(sig0))])
        sigwmin = np.array([sig0[i] if w0[i] <= w1[i] else sig1[i] for i in range(len(sig0))])
        wmax = np.array([w0[i] if w0[i] > w1[i] else w1[i] for i in range(len(w0))])
        wmin = np.array([w0[i] if w0[i] <= w1[i] else w1[i] for i in range(len(w0))])

        # ===========================================================
        _, ax = plt.subplots()
        ax.set_xlabel("Intensity")
        ax.set_ylabel(r"$\sigma$ (m)")
        ax.set_yscale("log")
        ax.set_xscale("log")
        ax.set_xlim(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex))
        ax.set_ylim(9e-9, 9e-5)
        ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))

        ax.scatter(mean_ex[wmax > w_thresh], sigwmax[wmax > w_thresh], marker="o", c="blue",
                   label=r"Large Amp. Double Gauss w > {}".format(w_thresh), s=10, zorder=3)
        ax.scatter(mean_ex[wmax <= w_thresh], sigwmax[wmax <= w_thresh], marker="o", edgecolor="blue", facecolors="none",
                   label="Large Amp. Double Gauss w \u2264 {}".format(w_thresh), s=10, alpha=0.3, zorder=1)
        # ax.scatter(mean_ex, sigwmin, marker="o", edgecolor="red", facecolors="none",
        # label=r"Small Amp. Double Gauss", s=10)
        ax.scatter(mean_ex[chisqr_erf / chisqr_erf_double < dg_min_chi_factor],
                   sig[chisqr_erf / chisqr_erf_double < dg_min_chi_factor], marker="x", c="black",
                   label=r"Single Gauss", s=10, alpha=0.7, zorder=2)

        sigmax = np.amax([sig0, sig1], axis=0)
        sigmin = np.amin([sig0, sig1], axis=0)

        # ax.scatter(mean_ex, sigmax, marker="o", c="blue", label=r"Large Double Gauss", s=10)
        # ax.scatter(mean_ex, sigmin, marker="o", edgecolor="red", facecolors="none", label=r"Small Double Gauss", s=10)
        ax.legend()

        plt.savefig(plot_path + "corr_sigma_mass_2_rot/sigma_mass_2_rot_{}.png".format(dt), dpi=dpi)
        plt.close()

        # ===========================================================
        """
        _, ax = plt.subplots()
        ax.set_xlabel("Intensity")
        ax.set_ylabel(r"$\sigma$ (m)")
        ax.set_yscale("log")
        ax.set_xscale("log")
        ax.set_xlim(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex))
        ax.set_ylim(9e-9, 9e-5)
        ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))

        ax.scatter(mean_ex, sigmax, marker="^", facecolors="none", edgecolors="blue", label=r"Large Double Gauss",
                   s=5, alpha=0.3)
        ax.scatter(mean_ex, sigmin, marker="v", facecolors="none", edgecolors="red", label=r"Small Double Gauss",
                   s=5, alpha=0.3)
        ax.scatter(mean_ex, sig, marker="x", facecolors="black", edgecolors="none", label=r"Single Gauss",
                   s=5, alpha=0.3)
        out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex), np.log10(sig), fit_type=2)
        x_space = np.linspace(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex), 1000)
        ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="black", ls="-", lw=2,
                label=r"$y = a \cdot x + b$" + "\n"
                      + r"$a$" + " = {:.4f} \u00b1 {:.4f}\n".format(out.beta[0], out.sd_beta[0])
                      + r"$b$" + " = {:.4f} \u00b1 {:.4f}".format(out.beta[1], out.sd_beta[1])
                )
        distmin = np.abs(np.log10(sigmin) - icaps.fit_lin(np.log10(mean_ex), *out.beta))
        distmax = np.abs(np.log10(sigmax) - icaps.fit_lin(np.log10(mean_ex), *out.beta))
        # y = sigmax
        # y[distmin < distmax] = sigmin[distmin < distmax]
        y = sigmin.copy()
        y[distmin > distmax] = sigmax[distmin > distmax]
        # # y0 = y.copy()
        # idx = np.where(distmin < distmax)
        # print(idx)
        # idx = idx[0]
        # if len(idx) > 0:
        #     y[idx[0]] = sigmin[idx[0]]
        #     # y0 = np.delete(y0, idx[0])
        #     # y1 = sigmin[idx[0]]
        y[chisqr_erf / chisqr_erf_double < dg_min_chi_factor] = sig[chisqr_erf / chisqr_erf_double < dg_min_chi_factor]
        ax.scatter(mean_ex, y, marker="o", facecolors="none", edgecolors="black", s=5)
        out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex), np.log10(y), p0=out.beta, fit_type=2)
        ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="magenta", ls="--", lw=2,
                label=r"$y = a \cdot x + b$" + "\n"
                      + r"$a$" + " = {:.4f} \u00b1 {:.4f}\n".format(out.beta[0], out.sd_beta[0])
                      + r"$b$" + " = {:.4f} \u00b1 {:.4f}".format(out.beta[1], out.sd_beta[1])
                )
        ax.legend()
        plt.savefig(plot_path + "corr_sigma_mass_rot/sigma_mass_rot_{}.png".format(dt), dpi=dpi)
        plt.close()
        """
        # ===========================================================
        """
        _, ax = plt.subplots()
        ax.set_xlabel("Intensity")
        ax.set_ylabel(r"$\sigma$ (m)")
        ax.set_yscale("log")
        ax.set_xscale("log")
        ax.set_xlim(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex))
        ax.set_ylim(9e-9, 9e-5)
        ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))

        # sig = sig[chisqr_erf / chisqr_erf_double < dg_min_chi_factor]
        # sig_ = sig[sigmax/sigmin < max_min_thresh]
        # sigmax_ = sigmax[sigmax/sigmin >= max_min_thresh]
        # sigmin_ = sigmin[sigmax/sigmin >= max_min_thresh]
        f1 = (sigmax/sigmin < max_min_thresh)
        f2 = (chisqr_erf / chisqr_erf_double < dg_min_chi_factor)
        f3 = (wmax >= w_thresh)
        fsum = None
        if filter_type == "or":
            fsum = f1 | f2 | f3
        elif filter_type == "and":
            fsum = f1 & f2 & f3
        else:
            print("Bad Filter Type!")

        # print(particles[~fsum])
        # print(particles[fsum])
        fsum = fsum[sig < 9e-5]
        sigmax = sigmax[sig < 9e-5]
        sigmin = sigmin[sig < 9e-5]
        mean_ex_ = mean_ex[sig < 9e-5]
        sig = sig[sig < 9e-5]

        sigmax_ = sigmax[~fsum]
        sigmin_ = sigmin[~fsum]
        sig_ = sig[fsum]
        ax.scatter(mean_ex_[~fsum], sigmax_, marker="^", facecolors="none", edgecolors="blue", label=r"Large Double Gauss",
                   s=5, alpha=0.3)
        ax.scatter(mean_ex_[~fsum], sigmin_, marker="v", facecolors="none", edgecolors="red", label=r"Small Double Gauss",
                   s=5, alpha=0.3)
        ax.scatter(mean_ex_[fsum], sig_, marker="x", facecolors="black", edgecolors="none", label=r"Single Gauss",
                   s=5, alpha=1.)
        ax.legend()
        out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex_[fsum]), np.log10(sig[fsum]), p0=[-0.5, -3], fit_type=2, maxit=10000)
        print(out.beta[1])
        xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
        ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="black", ls="-", lw=2,
                label="slope = {:.4f} \u00b1 {:.4f}".format(out.beta[0], out.sd_beta[0]))
        # out = icaps.fit_model(icaps.fit_pow, mean_ex_[fsum], sig[fsum], p0=[-0.4, 3*10**-5], fit_type=2, maxit=10000)
        # print(out.beta[1])
        # xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
        # ax.plot(xspace, icaps.fit_pow(xspace, *out.beta), c="black", ls="-", lw=2,
        #         label="slope = {:.4f} \u00b1 {:.4f}".format(out.beta[0], out.sd_beta[0]))

        distmin = np.abs(np.log10(sigmin_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
        distmax = np.abs(np.log10(sigmax_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
        sigmin_use = sigmin_[distmin <= distmax]
        sigmax_use = sigmax_[distmin > distmax]
        ax.scatter(mean_ex_[~fsum][distmin > distmax], sigmax_use, marker="^", c="blue",
                   label=r"Large Double Gauss", s=5, alpha=1.)
        ax.scatter(mean_ex_[~fsum][distmin <= distmax], sigmin_use, marker="v", c="red",
                   label=r"Small Double Gauss", s=5, alpha=1.)

        x_full = np.hstack([mean_ex_[fsum], mean_ex_[~fsum][distmin <= distmax], mean_ex_[~fsum][distmin > distmax]]).ravel()
        y_full = np.hstack([sig_, sigmin_use, sigmax_use]).ravel()
        out = icaps.fit_model(icaps.fit_lin, x=np.log10(x_full), y=np.log10(y_full), p0=[-0.5, -5], fit_type=2)
        ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="magenta", ls="--", lw=2,
                label="slope = {:.4f} \u00b1 {:.4f}".format(out.beta[0], out.sd_beta[0]))
        # out = icaps.fit_model(icaps.fit_pow, x=x_full, y=y_full, p0=[-0.4, 3*10**-5], fit_type=2)
        # ax.plot(xspace, 10 ** icaps.fit_pow(xspace, *out.beta), c="magenta", ls="--", lw=2,
        #         label="slope = {:.4f} \u00b1 {:.4f}".format(out.beta[0], out.sd_beta[0]))

        ax.legend()
        # plt.show()
        plt.savefig(plot_path + "corr_sigma_mass_3_rot/sigma_mass_3_rot_{}.png".format(dt), dpi=dpi)
        plt.close()
        """

        # ===========================================================
        _, ax = plt.subplots()
        ax.scatter(chisqr_erf / chisqr_erf_double, wmax, c="black", marker="o", s=10)
        ax.set_xlim(0, 20)
        ax.set_ylim(0.49, 1.01)
        ax.set_xlabel(r"$\chi^2_{single}/\chi^2_{double}$")
        ax.set_ylabel(r"$w_{max}$")
        plt.savefig(plot_path + "corr_chi_weights_rot/chi_weights_rot_{}.png".format(dt), dpi=dpi)
        plt.close()

        _, ax = plt.subplots()
        ax.scatter(chisqr_erf / chisqr_erf_double, sigmax / sigmin, c="black", marker="o", s=10)
        ax.set_xlim(0, 20)
        ax.set_ylim(0.9, 10.1)
        ax.set_xlabel(r"$\chi^2_{single}/\chi^2_{double}$")
        ax.set_ylabel(r"$\sigma_{max}/\sigma_{min}$")
        plt.savefig(plot_path + "corr_chi_sigratio_rot/chi_sigratio_rot_{}.png".format(dt), dpi=dpi)
        plt.close()

        _, ax = plt.subplots()
        ax.scatter(wmax, sigmax / sigmin, c="black", marker="o", s=10)
        ax.set_xlim(0.49, 1.01)
        ax.set_ylim(0.9, 10.1)
        ax.set_xlabel(r"$w_{max}$")
        ax.set_ylabel(r"$\sigma_{max}/\sigma_{min}$")
        plt.savefig(plot_path + "corr_weights_sigratio_rot/weights_sigratio_rot_{}.png".format(dt), dpi=dpi)
        plt.close()
        #
        # for particle in particles:
        #     m = df_mass.loc[df_mass["particle"] == particle, "mean_ex"].to_numpy()[0]
        #     idx = np.where(mean_ex == m)[0][0]
        #     df_erf_corr.loc[(df_erf_corr["dt"] == dt) & (df_erf_corr["particle"] == particle), "sigma_fit"] =\
        #         10 ** icaps.fit_lin(np.log10(m), *out.beta)
        #     df_erf_corr.loc[(df_erf_corr["dt"] == dt) & (df_erf_corr["particle"] == particle), "sigma"] = y[idx]
        pb.tick()
    pb.finish()

    df_erf_corr.to_csv(csv_erf_corr_path, index=False)

    df_erf_corr = pd.read_csv(csv_erf_corr_path)
    plot = True
    df_of_corr = icaps.brownian.calc_ornstein_fuerth(
        df_erf_corr, dts, axes=["x"], plot_path=plot_path + "corr_of_trans_{}/" if plot else None,
        mass_column="mass", fit_error=False,
        fit_bounds=(None, None), adapt_margins=(2, 13), grade_weights=(1.2, 1.), plot_attempts=True, n_min=10,
        quantity_type="count", max_sigma_sqr=1e-6, max_err_ratio=1, silent=False, plot_error=False, sigma_unit="m",
        mode="trans", dpi=600, fit_type=2, clear_out=True, grade_type="gaussian",
        crop_to_max=True, double_gauss="fitted")
    df_of_corr.to_csv(csv_of_corr_path, index=False)

    plot = False
    df_of_corr = icaps.brownian.calc_ornstein_fuerth(
        df_erf_corr, dts, axes=["x"], plot_path=plot_path + "corr_of_trans_{}/" if plot else None,
        mass_column="mass", fit_error=False, sigma_column="sigma_fit",
        fit_bounds=(None, None), adapt_margins=(2, 13), grade_weights=(1.2, 1.), plot_attempts=True, n_min=10,
        quantity_type="count", max_sigma_sqr=1e-6, max_err_ratio=1, silent=False, plot_error=False, sigma_unit="m",
        mode="trans", dpi=600, fit_type=2, clear_out=True, grade_type="gaussian",
        crop_to_max=True, double_gauss="fitted")
    df_of_corr.to_csv(csv_of_corr_fit_path, index=False)

    # todo: separate injections

    icaps.mk_folder(plot_path + "corr_tau_plots/", clear=True)
    df_of_corr = pd.read_csv(csv_of_corr_path)
    df_of_corr_fit = pd.read_csv(csv_of_corr_fit_path)
    _, ax = plt.subplots()
    tau = df_of_corr["tau"] * 1000
    # tau_err = df_of_corr["tau_err"] * 1000
    # tau_err = tau_err[tau < 30]
    tau = tau[tau < 30]
    tau = np.sort(tau)
    tau_cumu = np.linspace(1 / len(tau), 1, len(tau))

    ax.errorbar(tau, tau_cumu,
                # xerr=tau_err,
                fmt=".", color="black",
                markersize=5, zorder=100)
    # ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_ylabel(r"Cumulative Normalized $\tau_{t, x}$")
    ax.set_xlabel(r"$\tau_{t, x}$ (ms)")

    # xlims = ax.get_xlim()
    # ylims = ax.get_ylim()
    # lims = (0, max(xlims[1], ylims[1]))
    # ax.set_xlim(lims)
    # ax.set_ylim(lims)
    # ax.plot(lims, lims, color="gray", linestyle="--")
    # ax.set_xscale("log")
    plt.tight_layout()
    plt.savefig(plot_path + "corr_tau_plots/" + "Tau_Hist.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.errorbar(df_of_corr["mass"] / icaps.const.m_0, df_of_corr["tau"] * 1000,
                xerr=df_of_corr["mass_err"]/icaps.const.m_0, yerr=df_of_corr["tau_err"] * 1000,
                fmt=".", color="black",
                markersize=5, zorder=100)
    # ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_xlabel("N")
    ax.set_ylabel(r"$\tau_{t, x}$ (ms)")
    # xlims = ax.get_xlim()
    # ylims = ax.get_ylim()
    # lims = (0, max(xlims[1], ylims[1]))
    # ax.set_xlim(lims)
    # ax.set_ylim(lims)
    # ax.plot(lims, lims, color="gray", linestyle="--")
    ax.set_xscale("log")
    plt.tight_layout()
    plt.savefig(plot_path + "corr_tau_plots/" + "Tau_Mass.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.errorbar(df_of_corr_fit["mass"] / icaps.const.m_0, df_of_corr_fit["tau"] * 1000,
                xerr=df_of_corr_fit["mass_err"] / icaps.const.m_0, yerr=df_of_corr_fit["tau_err"] * 1000,
                fmt=".", color="black",
                markersize=5, zorder=100)
    # ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_xlabel("N")
    ax.set_ylabel(r"$\tau_{t, x}$ (ms)")
    # xlims = ax.get_xlim()
    # ylims = ax.get_ylim()
    # lims = (0, max(xlims[1], ylims[1]))
    # ax.set_xlim(lims)
    # ax.set_ylim(lims)
    # ax.plot(lims, lims, color="gray", linestyle="--")
    ax.set_xscale("log")
    plt.tight_layout()
    plt.savefig(plot_path + "corr_tau_plots/" + "Tau_Mass_Fit.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()

