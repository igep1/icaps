import numpy as np
import pandas as pd
import icaps
import matplotlib.pyplot as plt
import matplotlib.colorbar
import matplotlib.colors
from itertools import cycle

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT3/"
plot_path = project_path + "plots/"
# chi_sqr_thresh = 0.5e-6   # odr
chi_sqr_thresh = 10e-6    # least squares
excluded_particles = (8781427,)
csv_of_chiex_path = project_path + "OF_Chi{}Ex.csv".format(chi_sqr_thresh)
csv_disp_rot_path = project_path + "Displacement_Rot.csv"
csv_erf_rot_path = project_path + "Erf_Rot.csv"
csv_of_rot_path = project_path + "OF_Rot.csv"
csv_disp_trans_path = project_path + "Displacements_Trans.csv"
csv_erf_trans_path = project_path + "Erf_Trans.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
csv_vmu_of_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/particles/0/of.csv"
dpi = 600
plt.rcParams.update({"font.size": 10})


def main():
    # ============================================================
    # TABLE PREPARATION
    # ============================================================
    df_full = pd.read_csv(csv_of_chiex_path)
    print("chi² {}:\t\t".format(chi_sqr_thresh), len(np.unique(df_full["particle"].to_numpy())))
    df = df_full.copy()
    df = df.dropna(subset=["mass_x", "mass_y"])
    df = df[~df["particle"].isin(excluded_particles)]
    df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
    df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
    print("x and y:\t\t", len(np.unique(df["particle"].to_numpy())))
    print("rot:\t\t\t", len(np.unique(df.dropna(subset=["inertia"])["particle"].to_numpy())))
    mass_thresh = -0.8
    df_mass = df[np.log10(df["mass_x_err"] / df["mass_x"]) <= mass_thresh]
    df_mass = df_mass[np.log10(df_mass["mass_y_err"] / df_mass["mass_y"]) <= mass_thresh]
    df_temp = df_mass.copy()
    df_temp["ratio1"] = np.amax([df_temp["mass_x"] / df_temp["mass_y"], df_temp["mass_y"] / df_temp["mass_x"]], axis=0)
    df_temp["ratio2"] = abs(df_temp["mass_x"] / df_temp["mass_y"] - 1)
    # print(df_temp.sort_values("ratio2", ascending=False).head(10))
    print("mass logfilter:\t", len(np.unique(df["particle"].to_numpy())))
    tau_thresh = -0.8
    df_tau = df_mass[np.log10(df_mass["tau_x_err"] / df_mass["tau_x"]) <= tau_thresh]
    df_tau = df_tau[np.log10(df_tau["tau_y_err"] / df_tau["tau_y"]) <= tau_thresh]
    print("tau logfilter:\t", len(np.unique(df_tau["particle"].to_numpy())))
    # df_tau.to_csv(project_path + "out.csv")

    """
    # ============================================================
    # Brownian Inertia/Mass
    # ============================================================
    _, ax = plt.subplots()
    y = df_mass["inertia"].to_numpy()
    yerr = df_mass["inertia_err"].to_numpy()
    x = df_mass["mass"].to_numpy()
    xerr = df_mass["mass_err"].to_numpy()
    idx = np.where(~np.isnan(y))
    y = y[idx]
    yerr = yerr[idx]
    x = x[idx]
    xerr = xerr[idx]
    ax.errorbar(x, y,
                yerr=yerr, xerr=xerr,
                fmt=".", color="black", markersize=5,
                zorder=5)

    x_space = np.linspace(np.min(x), np.max(x), 1000)
    sy = yerr / y * 1 / np.log(10)
    sx = xerr / x * 1 / np.log(10)
    out = icaps.fit_model(icaps.fit_lin, np.log10(x), np.log10(y), xerr=sx, yerr=sy, fit_type=0)
    frac_dim = 2 / (out.beta[0] - 1)
    frac_dim_err = 1 / 2 * frac_dim ** 2 * out.sd_beta[0]
    ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="red", zorder=100,
            label="ODR\n" + r"$\alpha$" + " = {} \u00b1 {}\n".format(np.round(out.beta[0], 4),
                                                                     np.round(out.sd_beta[0], 4)) +
                  "$D_f$ = {} \u00b1 {}".format(np.round(frac_dim, 4), np.round(frac_dim_err, 4))
            )

    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"Brownian Mass (kg)")
    ax.set_ylabel(r"Brownian MoI (kgm$^2$)")
    ax.set_xlim(0.8e-14, 2e-12)
    ax.set_ylim(0.1e-25, 8e-22)
    ax.legend()
    plt.tight_layout()
    plt.savefig(plot_path + "Inertia_Mass_Brownian.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    x = df_mass["inertia"].to_numpy()
    xerr = df_mass["inertia_err"].to_numpy()
    y = df_mass["mass"].to_numpy()
    yerr = df_mass["mass_err"].to_numpy()
    idx = np.where(~np.isnan(x))
    y = y[idx]
    yerr = yerr[idx]
    x = x[idx]
    xerr = xerr[idx]
    ax.errorbar(x, y,
                yerr=yerr, xerr=xerr,
                fmt=".", color="black", markersize=5,
                zorder=5)

    x_space = np.linspace(np.min(x), np.max(x), 1000)
    sy = yerr / y * 1 / np.log(10)
    sx = xerr / x * 1 / np.log(10)
    out = icaps.fit_model(icaps.fit_lin, np.log10(x), np.log10(y), xerr=sx, yerr=sy, fit_type=0)
    frac_dim = 2 / (1 / out.beta[0] - 1)
    frac_dim_err = 1 / 2 * frac_dim ** 2 * out.sd_beta[0]
    ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="red", zorder=100,
            label="ODR\n" + r"$\alpha$" + " = {} \u00b1 {}\n".format(np.round(out.beta[0], 4),
                                                                     np.round(out.sd_beta[0], 4)) +
                  "$D_f$ = {} \u00b1 {}".format(np.round(frac_dim, 4), np.round(frac_dim_err, 4))
            )
    # for i, row in df.dropna(subset=["inertia"]).iterrows():
    #     ax.annotate(str(int(row["particle"])), (row["inertia"], row["mass"]), size=5,
    #                 bbox=dict(boxstyle="square", fc="w", alpha=0.7, zorder=100))

    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_ylabel(r"Brownian Mass (kg)")
    ax.set_xlabel(r"Brownian MoI (kgm$^2$)")
    ax.set_ylim(0.8e-14, 2e-12)
    ax.set_xlim(0.1e-25, 8e-22)
    ax.legend()
    plt.tight_layout()
    plt.savefig(plot_path + "Inertia_Mass_Brownian_Reverse.png", dpi=dpi)
    plt.close()

    # ============================================================
    # Optical Inertia/Mass
    # ============================================================
    x = df_mass["mean_ex"].to_numpy() / icaps.const.ldm_monomer_ex * icaps.const.m_p
    xerr = df_mass["mean_ex_err"].to_numpy() / icaps.const.ldm_monomer_ex * icaps.const.m_p
    y = (df_mass["mean_gyrad"].to_numpy() * icaps.const.px_ldm * 1e-6) ** 2 * x
    yerr = y * np.sqrt((xerr / x) ** 2 + (2 * df_mass["mean_gyrad_err"].to_numpy() / df_mass["mean_gyrad"].to_numpy()) ** 2)
    _, ax = plt.subplots()
    ax.errorbar(x, y,
                yerr=yerr, xerr=xerr,
                fmt=".", color="black", markersize=5,
                zorder=5)
    out = icaps.fit_model(icaps.fit_lin, np.log10(x), np.log10(y), xerr=np.log10(xerr), yerr=np.log10(yerr))
    x_space = np.linspace(np.min(x), np.max(x), 1000)
    frac_dim = 2 / (out.beta[0] - 1)
    frac_dim_err = 1 / 2 * frac_dim ** 2 * out.sd_beta[0]
    ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="red", zorder=100,
            label="ODR\n" + r"$\alpha$" + " = {} \u00b1 {}\n".format(np.round(out.beta[0], 4),
                                                                     np.round(out.sd_beta[0], 4)) +
                  "$D_f$ = {} \u00b1 {}".format(np.round(frac_dim, 4), np.round(frac_dim_err, 4))
            )
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("Optical Mass (kg)")
    ax.set_ylabel(r"Optical MoI (kgm$^2$)")
    ax.set_xlim(0.8e-14, 2e-12)
    ax.set_ylim(0.1e-25, 8e-22)
    ax.legend()
    plt.tight_layout()
    plt.savefig(plot_path + "Inertia_Mass_Optical.png", dpi=dpi)
    plt.close()

    # ============================================================
    # Arb. Optical Inertia/Mass
    # ============================================================
    x = df_mass["mean_ex"].to_numpy()
    xerr = df_mass["mean_ex_err"].to_numpy()
    y = (df_mass["mean_gyrad"].to_numpy() * icaps.const.px_ldm * 1e-6) ** 2 * x
    yerr = y * np.sqrt((xerr / x) ** 2 + (2 * df_mass["mean_gyrad_err"].to_numpy() / df_mass["mean_gyrad"].to_numpy()) ** 2)
    _, ax = plt.subplots()
    ax.errorbar(x, y,
                yerr=yerr, xerr=xerr,
                fmt=".", color="black", markersize=5,
                zorder=5)
    out = icaps.fit_model(icaps.fit_lin, np.log10(x), np.log10(y), xerr=np.log10(xerr), yerr=np.log10(yerr))
    x_space = np.linspace(np.min(x), np.max(x), 1000)
    frac_dim = 2 / (out.beta[0] - 1)
    frac_dim_err = 1 / 2 * frac_dim ** 2 * out.sd_beta[0]
    ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="red", zorder=100,
            label="ODR\n" + r"$\alpha$" + " = {} \u00b1 {}\n".format(np.round(out.beta[0], 4),
                                                                     np.round(out.sd_beta[0], 4)) +
                  "$D_f$ = {} \u00b1 {}".format(np.round(frac_dim, 4), np.round(frac_dim_err, 4))
            )
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("Optical Mass (grayscale value)")
    ax.set_ylabel(r"Optical MoI (grayscale value $\cdot$ m$^2$)")
    # ax.set_xlim(0.8e-14, 2e-12)
    # ax.set_ylim(0.1e-25, 8e-22)
    ax.legend()
    plt.tight_layout()
    plt.savefig(plot_path + "Inertia_Mass_Optical_Arb.png", dpi=dpi)
    plt.close()

    # ============================================================
    # Optical Mass/ BM Mass
    # ============================================================
    x = df_mass["mean_ex"].to_numpy() / icaps.const.ldm_monomer_ex * icaps.const.m_p
    xerr = df_mass["mean_ex_err"].to_numpy() / icaps.const.ldm_monomer_ex * icaps.const.m_p
    y = df_mass["mass"].to_numpy()
    yerr = df_mass["mass_err"].to_numpy()
    _, ax = plt.subplots()
    ax.errorbar(x, y,
                yerr=yerr, xerr=xerr,
                fmt=".", color="black", markersize=5,
                zorder=5)
    out = icaps.fit_model(icaps.fit_lin, np.log10(x), np.log10(y), xerr=np.log10(xerr), yerr=np.log10(yerr))
    x_space = np.linspace(np.min(x), np.max(x), 1000)
    ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="red", zorder=100,
            label="ODR\n" + r"Slope" + " = {} \u00b1 {}\n".format(np.round(out.beta[0], 4), np.round(out.sd_beta[0], 4))
            )
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("Optical Mass (kg)")
    ax.set_ylabel(r"Brownian Mass (kg)")
    # ax.set_xlim(0.8e-14, 2e-12)
    # ax.set_ylim(0.1e-25, 8e-22)
    ax.legend()
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_Optical_Brownian.png", dpi=dpi)
    plt.close()

    # ============================================================
    # Optical Inertia/ BM Inertia
    # ============================================================
    x = (df_mass["mean_gyrad"].to_numpy() * icaps.const.px_ldm * 1e-6) ** 2 * df_mass["mean_ex"].to_numpy() / icaps.const.ldm_monomer_ex * icaps.const.m_p
    xerr = x * np.sqrt(
        (df_mass["mean_ex_err"].to_numpy() / df_mass["mean_ex"].to_numpy()) ** 2 + (2 * df_mass["mean_gyrad_err"].to_numpy() / df_mass["mean_gyrad"].to_numpy()) ** 2)
    y = df_mass["inertia"].to_numpy()
    yerr = df_mass["inertia_err"].to_numpy()
    idx = np.where(~np.isnan(y))
    y = y[idx]
    yerr = yerr[idx]
    x = x[idx]
    xerr = xerr[idx]
    _, ax = plt.subplots()
    ax.errorbar(x, y,
                yerr=yerr, xerr=xerr,
                fmt=".", color="black", markersize=5,
                zorder=5)
    out = icaps.fit_model(icaps.fit_lin, np.log10(x), np.log10(y), xerr=np.log10(xerr), yerr=np.log10(yerr))
    x_space = np.linspace(np.min(x), np.max(x), 1000)
    ax.plot(x_space, 10 ** icaps.fit_lin(np.log10(x_space), *out.beta), c="red", zorder=100,
            label="ODR\n" + r"Slope" + " = {} \u00b1 {}\n".format(np.round(out.beta[0], 4), np.round(out.sd_beta[0], 4))
            )
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"Optical MoI (kgm$^2$)")
    ax.set_ylabel(r"Brownian MoI (kgm$^2$)")
    # ax.set_xlim(0.8e-14, 2e-12)
    # ax.set_ylim(0.1e-25, 8e-22)
    ax.legend()
    plt.tight_layout()
    plt.savefig(plot_path + "Inertia_Optical_Brownian.png", dpi=dpi)
    plt.close()
    """

    # ============================================================
    # VMU COMPARATIVE PLOT
    # ============================================================
    """
    df_vmu = pd.read_csv(csv_vmu_of_path)
    _, ax = plt.subplots()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\Delta t$ (s)")
    ax.set_ylabel(r"$\left<\Delta x ^2\right>$ (m$^2$)")
    ax.set_xlim(0.9e-3, 60e-3)

    particles = df_tau["particle"].to_numpy()
    df_erf = pd.read_csv(csv_erf_trans_path)
    axis = "x"
    df_erf = df_erf[df_erf["axis"] == axis]
    df_erf = df_erf[df_erf["particle"].isin(particles)]
    max_sigma_sqr = 1e-6
    for i, particle in enumerate(particles):
        df_ = df_erf[df_erf["particle"] == particle]
        dt = df_["dt"].to_numpy()
        sigma = df_["sigma"].to_numpy()
        sigma_err = df_["sigma_err"].to_numpy()
        sigma_sqr = sigma ** 2
        sigma_sqr_err = 2 * sigma_err * sigma
        dt = dt[sigma_sqr <= max_sigma_sqr]
        sigma_sqr_err = sigma_sqr_err[sigma_sqr <= max_sigma_sqr]
        sigma_sqr = sigma_sqr[sigma_sqr <= max_sigma_sqr]
        idx = np.where(sigma_sqr_err/sigma_sqr < 10)
        dt = dt[idx]
        sigma_sqr = sigma_sqr[idx]
        sigma_sqr_err = sigma_sqr_err[idx]
        ax.errorbar(dt*1e-3, sigma_sqr, yerr=sigma_sqr_err, fmt=".", color="black", markersize=2,
                    label="Measured Data in {}".format(axis) if i == 0 else None)

    t_range = np.linspace(0.9e-3, 60e-3, 1000)
    linecycler = cycle(["-", "--", ":", "-."])
    for i, a in enumerate(df_vmu["axis"].to_numpy()):
        mass = df_vmu.loc[df_vmu["axis"] == a, "mass"].to_numpy()[0]
        tau = df_vmu.loc[df_vmu["axis"] == a, "tau"].to_numpy()[0]
        ax.plot(t_range, icaps.fit_of_lin(t_range, icaps.const.k_B * icaps.const.T / mass, tau),
                c="red", linestyle=next(linecycler), label="Cloud Track in {}".format(a.upper()))
    ax.legend(loc="lower right")
    plt.savefig(plot_path + "VMU_Drift_Influence.png", dpi=dpi)
    plt.close()
    """

    # ============================================================
    # ROT TRACK FOR 720240
    # ============================================================
    dt = 1
    df = pd.read_csv(csv_disp_rot_path)
    df = df[df["particle"] == 720240]
    df = df[["gyrad", "focus", "semi_major_axis", "ellipse_angle", "omega_{}".format(dt)]].dropna()
    start_angle = np.deg2rad(360 - df["ellipse_angle"].to_numpy()[0] + 90)
    disp = df["omega_{}".format(dt)].to_numpy() * -1
    angle = np.cumsum(np.append([start_angle], disp))
    time = np.arange(dt, len(angle) + dt, dt) / 1000
    ckey = df["focus"].to_numpy()
    ckey = np.append(ckey[0], ckey)
    fig, ax = plt.subplots(subplot_kw={"projection": "polar"})
    sct = ax.scatter(angle, time, c=ckey, s=4, cmap=plt.get_cmap("plasma"))
    ax.plot(angle, time, c="black", lw=1, ls="-", alpha=0.5)
    ax.set_rlabel_position(270)
    label_position = ax.get_rlabel_position()
    ax.text(np.radians(label_position - 10), ax.get_rmax() / 2., r"$t$ (s)",
            # rotation=label_position,
            ha="center", va="center")
    ax.set_rlim(0, 1.05*np.max(time))
    # pos = ax.imshow(ckey, cmap="plasma", interpolation="none")
    # fig.colorbar(pos, ax=ax)
    fig.colorbar(sct, ax=ax, label="Focus Value", pad=0.07)
    plt.savefig(plot_path + "Rot_Track_720240.png", dpi=dpi)
    plt.close()

    # ============================================================
    # ERF TRANS COLLAGE FOR 720240
    # ============================================================
    # """
    particle = 720240
    df_disp = pd.read_csv(csv_disp_trans_path)
    df_disp = df_disp[df_disp["particle"] == particle]
    df_erf = pd.read_csv(csv_erf_trans_path)
    df_erf = df_erf[df_erf["particle"] == particle]
    df_erf = df_erf[df_erf["axis"] == "x"]
    dts = [(1, 5), (10, 20), (35, 50)]
    fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={'height_ratios': np.tile([2, 1], len(dts))},
                           figsize=[5.2, 6.4])
    plt.subplots_adjust(hspace=.001, wspace=.001)
    for j in range(len(dts) * 2):
        if j % 2 == 0:
            for i in range(len(dts[int(j/2)])):
                dt = dts[int(j/2)][i]
                ax_ = ax[j][i]
                disp_column = "{}_{}".format("dx", dt)
                disp = np.sort(df_disp[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
                norm_disp = np.linspace(1 / len(disp), 1, len(disp))
                ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
                sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0] * 1e6
                drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0] * 1e6
                # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
                x_space = np.linspace(-11.5, 11.5, 1000)
                ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
                ax_.set_xlim(-11.5, 11.5)
                if int(j/2) < len(dts) - 1:
                    ax_.set_xticks([])
                if i == 1:
                    ax_.set_yticks([])
                # else:
                #     if j == 2:
                #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
                ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-11, 1), xycoords="data",
                             size=10, ha="left", va="top",
                             bbox=dict(boxstyle="square", fc="w"))
        else:
            for i in range(len(dts[int(j/2)])):
                dt = dts[int(j / 2)][i]
                ax_ = ax[j][i]
                disp_column = "{}_{}".format("dx", dt)
                disp = np.sort(df_disp[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
                norm_disp = np.linspace(1 / len(disp), 1, len(disp))
                sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0] * 1e6
                drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0] * 1e6
                disp_res = norm_disp - icaps.fit_erf(disp, drift, sigma)
                ax_.scatter(disp, disp_res, c="black", s=1, marker="o", zorder=1)
                ax_.set_xlim(-11.5, 11.5)
                ax_.set_ylim(-0.06, 0.06)
                ax_.axhline(0, c="grey", zorder=0, lw=1, linestyle="--")
                ax_.yaxis.tick_right()
                if int(j/2) < len(dts) - 1:
                    ax_.set_xticks([])
                # else:
                #     ax_.set_xlabel(r"$dx$ (µm)")
                if i == 0:
                    ax_.set_yticks([])
                # else:
                #     ax_.set_ylabel(r"Residual")
    fig.text(0.50, 0.02, r"$\Delta x$ (µm)", ha="center")
    fig.text(0.025, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    fig.text(0.97, 0.50, "Residual", va="center", rotation=-90.)
    fig.subplots_adjust(bottom=0.08, top=0.95)
    fig.subplots_adjust(right=0.88)
    plt.savefig(plot_path + "ERF_trans_x_{}.png".format(particle), dpi=dpi)
    plt.close()
    # """

    # ============================================================
    # ERF ROT COLLAGE FOR 720240
    # ============================================================
    # """
    particle = 720240
    df_disp = pd.read_csv(csv_disp_rot_path)
    df_disp = df_disp[df_disp["particle"] == particle]
    df_erf = pd.read_csv(csv_erf_rot_path)
    df_erf = df_erf[df_erf["particle"] == particle]
    dts = [(1, 5), (10, 20), (35, 50)]
    fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={'height_ratios': np.tile([2, 1], len(dts))},
                           figsize=[5.2, 6.4])
    plt.subplots_adjust(hspace=.001, wspace=.001)
    for j in range(len(dts) * 2):
        if j % 2 == 0:
            for i in range(len(dts[int(j / 2)])):
                dt = dts[int(j / 2)][i]
                ax_ = ax[j][i]
                disp_column = "{}_{}".format("omega", dt)
                disp = np.sort(df_disp[disp_column].dropna().to_numpy())
                norm_disp = np.linspace(1 / len(disp), 1, len(disp))
                ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
                sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0]
                drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0]
                # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
                x_space = np.linspace(-1, 1, 1000)
                ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
                ax_.set_xlim(-0.95, 0.95)
                if int(j / 2) < len(dts) - 1:
                    ax_.set_xticks([])
                if i == 1:
                    ax_.set_yticks([])
                # else:
                #     if j == 2:
                #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
                ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-0.9, 1), xycoords="data",
                             size=10, ha="left", va="top",
                             bbox=dict(boxstyle="square", fc="w"))
        else:
            for i in range(len(dts[int(j / 2)])):
                dt = dts[int(j / 2)][i]
                ax_ = ax[j][i]
                disp_column = "{}_{}".format("omega", dt)
                disp = np.sort(df_disp[disp_column].dropna().to_numpy())
                norm_disp = np.linspace(1 / len(disp), 1, len(disp))
                sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0]
                drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0]
                disp_res = icaps.fit_erf(disp, drift, sigma) - norm_disp
                ax_.scatter(disp, disp_res, c="black", s=1, marker="o", zorder=1)
                ax_.set_xlim(-0.95, 0.95)
                ax_.set_ylim(-0.06, 0.06)
                ax_.axhline(0, c="grey", zorder=0, lw=1, linestyle="--")
                ax_.yaxis.tick_right()
                if int(j / 2) < len(dts) - 1:
                    ax_.set_xticks([])
                # else:
                #     ax_.set_xlabel(r"$dx$ (µm)")
                if i == 0:
                    ax_.set_yticks([])
                # else:
                #     ax_.set_ylabel(r"Residual")
    fig.text(0.50, 0.02, r"$\Delta \theta$ (rad)", ha="center")
    fig.text(0.025, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    fig.text(0.97, 0.50, "Residual", va="center", rotation=-90.)
    fig.subplots_adjust(bottom=0.08, top=0.95)
    fig.subplots_adjust(right=0.88)
    plt.savefig(plot_path + "ERF_rot_{}.png".format(particle), dpi=dpi)
    plt.close()
    # """

    # ============================================================
    # ERF TRANS COLLAGE WITHOUT RESIDUALS FOR 720240
    # ============================================================
    # """
    particle = 720240
    df_disp = pd.read_csv(csv_disp_trans_path)
    df_disp = df_disp[df_disp["particle"] == particle]
    df_erf = pd.read_csv(csv_erf_trans_path)
    df_erf = df_erf[df_erf["particle"] == particle]
    df_erf = df_erf[df_erf["axis"] == "x"]
    dts = [(1, 5), (10, 20), (35, 50)]
    fig, ax = plt.subplots(nrows=3, ncols=2, figsize=[6.4, 5.2])
    plt.subplots_adjust(hspace=.001, wspace=.001)
    for j in range(len(dts)):
        for i in range(len(dts[j])):
            dt = dts[j][i]
            ax_ = ax[j][i]
            disp_column = "{}_{}".format("dx", dt)
            disp = np.sort(df_disp[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
            sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0] * 1e6
            drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0] * 1e6
            # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
            x_space = np.linspace(-11.5, 11.5, 1000)
            ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
            ax_.set_xlim(-11.5, 11.5)
            if j < len(dts) - 1:
                ax_.set_xticks([])
            if i == 1:
                ax_.set_yticks([])
            # else:
            #     if j == 2:
            #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
            ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-11, 1), xycoords="data",
                         size=10, ha="left", va="top",
                         bbox=dict(boxstyle="square", fc="w"))
    fig.text(0.50, 0.02, r"$\Delta x$ (µm)", ha="center")
    fig.text(0.025, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    fig.subplots_adjust(bottom=0.08, top=0.95)
    plt.savefig(plot_path + "ERF_trans_nores_x_{}.png".format(particle), dpi=dpi)
    plt.close()
    # """

    # ============================================================
    # ERF ROT COLLAGE WITHOUT RESIDUALS FOR 720240
    # ============================================================
    # """
    particle = 720240
    df_disp = pd.read_csv(csv_disp_rot_path)
    df_disp = df_disp[df_disp["particle"] == particle]
    df_erf = pd.read_csv(csv_erf_rot_path)
    df_erf = df_erf[df_erf["particle"] == particle]
    dts = [(1, 5), (10, 20), (35, 50)]
    fig, ax = plt.subplots(nrows=3, ncols=2, figsize=[6.4, 5.2])
    plt.subplots_adjust(hspace=.001, wspace=.001)
    for j in range(len(dts)):
        for i in range(len(dts[j])):
            dt = dts[j][i]
            ax_ = ax[j][i]
            disp_column = "{}_{}".format("omega", dt)
            disp = np.sort(df_disp[disp_column].dropna().to_numpy())
            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
            sigma = df_erf.loc[df_erf["dt"] == dt, "sigma"].to_numpy()[0]
            drift = df_erf.loc[df_erf["dt"] == dt, "drift"].to_numpy()[0]
            # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
            x_space = np.linspace(-1, 1, 1000)
            ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
            ax_.set_xlim(-0.95, 0.95)
            if j < len(dts) - 1:
                ax_.set_xticks([])
            if i == 1:
                ax_.set_yticks([])
            # else:
            #     if j == 2:
            #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
            ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-0.9, 1), xycoords="data",
                         size=10, ha="left", va="top",
                         bbox=dict(boxstyle="square", fc="w"))
    fig.text(0.50, 0.02, r"$\Delta \theta$ (rad)", ha="center")
    fig.text(0.025, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    fig.subplots_adjust(bottom=0.08, top=0.95)
    plt.savefig(plot_path + "ERF_rot_nores_{}.png".format(particle), dpi=dpi)
    plt.close()
    # """

    # ============================================================
    # OF TRANS FOR 720240
    # ============================================================
    plt.rcParams.update({"font.size": 12})
    particle = 720240
    idx = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
    df_of = pd.read_csv(csv_of_trans_path)
    df_erf = pd.read_csv(csv_erf_trans_path)
    df_of = df_of[df_of["particle"] == particle]
    df_of = df_of[df_of["axis"] == "x"]
    df_erf = df_erf[df_erf["particle"] == particle]
    df_erf = df_erf[df_erf["axis"] == "x"]
    dts = df_erf["dt"].to_numpy() / 1000
    sigma = df_erf["sigma"].to_numpy()
    sigma_err = df_erf["sigma_err"].to_numpy()
    sigma_sqr = sigma ** 2
    sigma_sqr_err = 2 * sigma_err * sigma
    mass = df_of["mass"].to_numpy()[0]
    mass_err = df_of["mass_err"].to_numpy()[0]
    tau = df_of["tau"].to_numpy()[0]
    tau_err = df_of["tau_err"].to_numpy()[0]

    _, ax = plt.subplots()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\Delta t$ (s)")
    ax.set_ylabel(r"$\left<\Delta x^2\right>$ (m$^2$)")
    ax.errorbar(dts, sigma_sqr, yerr=sigma_sqr_err, fmt=".", color="gray", markersize=10)
    ax.errorbar(dts[idx], sigma_sqr[idx], yerr=sigma_sqr_err[idx], fmt=".", color="black", markersize=10)
    dt_range = np.linspace(np.min(dts), np.max(dts), 1000)
    ax.plot(dt_range, icaps.fit_of_lin(dt_range, icaps.const.k_B * icaps.const.T / mass, tau), color="red", lw=2,
            linestyle="-", zorder=20,
            label="Ornstein-Fürth Fit\n" +
                  r"$m$ = ({:.0f} $\pm$ {:.0f}) $\cdot$ {} kg".format(mass*1e15, mass_err*1e15, r"$10^{-15}$") + "\n"
                  + r"{} = ({:.1f} $\pm$ {:.1f}) ms".format(r"$\tau_{t,x}$", tau*1e3, tau_err*1e3)
            )
    xlims = ax.get_xlim()
    dt_range = np.linspace(xlims[0], 5e-3, 1000)
    ax.plot(dt_range, icaps.fit_para(dt_range, 1e-7, 0), color="black", lw=2,
            linestyle="-.", zorder=20,
            label="Ballistic"
            )
    dt_range = np.linspace(5e-3, xlims[1], 1000)
    ax.plot(dt_range, icaps.fit_lin(dt_range, .8e-9, 0), color="black", lw=2,
            linestyle="--", zorder=20,
            label="Diffusive"
            )
    ax.legend(loc="lower right")
    plt.savefig(plot_path + "OF_trans_x_{}.png".format(particle), dpi=dpi)
    plt.close()

    # ============================================================
    # OF ROT FOR 720240
    # ============================================================
    particle = 720240
    idx = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
    df_of = pd.read_csv(csv_of_rot_path)
    df_erf = pd.read_csv(csv_erf_rot_path)
    df_of = df_of[df_of["particle"] == particle]
    df_erf = df_erf[df_erf["particle"] == particle]
    dts = df_erf["dt"].to_numpy() / 1000
    sigma = df_erf["sigma"].to_numpy()
    sigma_err = df_erf["sigma_err"].to_numpy()
    sigma_sqr = sigma ** 2
    sigma_sqr_err = 2 * sigma_err * sigma
    inertia = df_of["inertia"].to_numpy()[0]
    inertia_err = df_of["inertia_err"].to_numpy()[0]
    tau = df_of["tau"].to_numpy()[0]
    tau_err = df_of["tau_err"].to_numpy()[0]

    _, ax = plt.subplots()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\Delta t$ (s)")
    ax.set_ylabel(r"$\left<\Delta \theta^2\right>$ (rad$^2$)")
    ax.errorbar(dts, sigma_sqr, yerr=sigma_sqr_err, fmt=".", color="gray", markersize=10)
    ax.errorbar(dts[idx], sigma_sqr[idx], yerr=sigma_sqr_err[idx], fmt=".", color="black", markersize=10)
    dt_range = np.linspace(np.min(dts), np.max(dts), 1000)
    ax.plot(dt_range, icaps.fit_of_lin(dt_range, icaps.const.k_B * icaps.const.T / inertia, tau), color="red", lw=2,
            linestyle="-", zorder=20,
            label="Ornstein-Fürth Fit\n" +
                  r"$I$ = ({:.0f} $\pm$ {:.0f}) $\cdot$ {} kg".format(inertia * 1e25, inertia_err * 1e25,
                                                                      r"$10^{-25}$") + "\n"
                  + r"{} = ({:.1f} $\pm$ {:.1f}) ms".format(r"$\tau_{r}$", tau * 1e3, tau_err * 1e3)
            )
    xlims = ax.get_xlim()
    dt_range = np.linspace(xlims[0], 5e-3, 1000)
    ax.plot(dt_range, icaps.fit_para(dt_range, 1.5e3, 0), color="black", lw=2,
            linestyle="-.", zorder=20,
            label="Parabola"
            )
    dt_range = np.linspace(5e-3, xlims[1], 1000)
    ax.plot(dt_range, icaps.fit_lin(dt_range, 1e1, 0), color="black", lw=2,
            linestyle="--", zorder=20,
            label="Line"
            )
    ax.legend(loc="lower right")
    plt.savefig(plot_path + "OF_rot_{}.png".format(particle), dpi=dpi)
    plt.close()

    # ============================================================
    # CUMULATIVE MASS VS MASS
    # ============================================================
    mass = df_mass["mass_x"].to_numpy()
    mass_err = df_mass["mass_x_err"].to_numpy()
    idx = np.argsort(mass)
    mass = mass[idx]/icaps.const.m_0
    mass_err = mass_err[idx]/icaps.const.m_0

    _, ax = plt.subplots()
    mass_cum = np.linspace(1 / len(mass), 1, len(mass))
    ax.errorbar(mass, mass_cum, xerr=mass_err, fmt=".", color="black", markersize=5)
    # ax.errorbar(mass, mass_cum, fmt=".", color="black", markersize=5)
    ax.set_xlim(np.min(mass), np.max(mass))
    ax.set_xlabel(r"$m_x/m_0$")
    ax.set_ylabel("Cumulative Normalized Frequency")
    ax.set_xlim(0, 10)
    ax.set_ylim(-0.01, 1.05 * np.max(mass_cum[mass <= 10]))

    bins = np.arange(0, int(np.round(np.max(mass))) + 0.5, 0.5) - 0.25
    hist, bins = np.histogram(mass, bins)
    hist0 = hist[::2]
    hist1 = hist[1::2]
    x0 = bins[:-1:2] + 0.25
    x1 = bins[1:-1:2] + 0.25
    _, ax = plt.subplots()
    ax.plot(x0, hist0, c="red", lw=2, ms=7, marker="o", label="integer")
    ax.plot(x1, hist1, c="blue", lw=2, ms=7, marker="x", label="half-integer")
    ax.set_xlabel(r"$m_x/m_0$")
    ax.set_ylabel("Frequency")
    ax.legend()
    ax.set_xlim(0, 10)

    # plt.show()

    # ============================================================
    # CUMULATIVE MASS VS MASS DT=1ms ONLY
    # ============================================================
    df_erf = pd.read_csv(csv_erf_trans_path)
    print("orig:\t\t\t", len(np.unique(df_erf["particle"].to_numpy())))
    particles = np.unique(df_mass["particle"].to_numpy())
    df_erf = df_erf[df_erf["particle"].isin(particles)]
    dt = 1
    df_erf = df_erf[df_erf["dt"] == dt]
    axis = "x"
    df_erf = df_erf[df_erf["axis"] == axis]
    # print(df_erf["sigma"])

    mass = icaps.const.k_B * icaps.const.T * (df_erf["sigma"].to_numpy()) ** (-2) * (dt * 1e-3) ** 2
    mass_err = 2 * mass * df_erf["sigma_err"].to_numpy()/df_erf["sigma"].to_numpy() * (dt * 1e-3) ** 2
    idx = np.argsort(mass)
    mass = mass[idx] / icaps.const.m_0
    mass_err = mass_err[idx] / icaps.const.m_0
    # idx = np.where(np.log10(mass_err/mass) <= -0.8)
    # mass = mass[idx]
    # mass_err = mass_err[idx]

    _, ax = plt.subplots()
    mass_cum = np.linspace(1 / len(mass), 1, len(mass))
    ax.errorbar(mass, mass_cum, xerr=mass_err, fmt=".", color="black", markersize=2, marker="o")
    ax.set_xlim(np.min(mass), np.max(mass))
    ax.set_xlabel(r"$m_x/m_0$")
    ax.set_ylabel("Cumulative Normalized Frequency")
    ax.set_xlim(0, 10)
    ax.set_ylim(-0.01, 1.05 * np.max(mass_cum[mass <= 10]))

    bins = np.arange(0, int(np.round(np.max(mass))) + 0.5, 0.5) - 0.25
    hist, bins = np.histogram(mass, bins)
    hist0 = hist[::2]
    hist1 = hist[1::2]
    x0 = bins[:-1:2] + 0.25
    x1 = bins[1:-1:2] + 0.25
    _, ax = plt.subplots()
    ax.plot(x0, hist0, c="red", lw=2, ms=7, marker="o", label="integer")
    ax.plot(x1, hist1, c="blue", lw=2, ms=7, marker="x", label="half-integer")
    ax.legend()
    ax.set_xlim(0, 10)
    ax.set_xlabel(r"$m_x/m_0$")
    ax.set_ylabel("Frequency")
    # plt.show()
    plt.close()

    # ============================================================
    # TAU X VS TAU Y
    # ============================================================
    _, ax = plt.subplots()
    ax.errorbar(df_tau["tau_x"] * 1000, df_tau["tau_y"] * 1000,
                xerr=df_tau["tau_x_err"] * 1000, yerr=df_tau["tau_y_err"] * 1000,
                fmt=".", color="black",
                markersize=5, zorder=100)
    ax.set_xlabel(r"$\tau_{t, x}$ (ms)")
    ax.set_ylabel(r"$\tau_{t, y}$ (ms)")
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    lims = (0, max(xlims[1], ylims[1]))
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.plot(lims, lims, color="gray", linestyle="--")
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_TX_TY.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.errorbar(df_tau["tau_x"] * 1000, df_tau["tau_y"] * 1000,
                xerr=df_tau["tau_x_err"] * 1000, yerr=df_tau["tau_y_err"] * 1000,
                fmt=".", color="black",
                markersize=5, zorder=100)
    ax.set_xlabel(r"$\tau_{t, x}$ (ms)")
    ax.set_ylabel(r"$\tau_{t, y}$ (ms)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    lims = (min(xlims[0], ylims[0]), max(xlims[1], ylims[1]))
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.plot(lims, lims, color="gray", linestyle="--")
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_TX_TY_log.png", dpi=dpi)
    plt.close()

    # ============================================================
    # TAU X VS TAU ROT
    # ============================================================
    _, ax = plt.subplots()
    ax.errorbar(df_tau["tau_x"] * 1000, df_tau["tau_rot"] * 1000,
                xerr=df_tau["tau_x_err"] * 1000, yerr=df_tau["tau_rot_err"] * 1000,
                fmt=".", color="black",
                markersize=5, zorder=100)
    ax.set_xlabel(r"$\tau_{t, x}$ (ms)")
    ax.set_ylabel(r"$\tau_{r}$ (ms)")
    # print(df_tau.sort_values("tau_rot_err", ascending=False, axis=0)[["particle", "tau_rot", "tau_rot_err"]])
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    lims = (0, max(xlims[1], ylims[1]))
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.plot(lims, lims, color="gray", linestyle="--")
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_TX_ROT.png", dpi=dpi)
    plt.close()

    # ============================================================
    # MASS X VS MASS Y
    # ============================================================
    _, ax = plt.subplots()
    ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_ylabel(r"$m_y$ (kg)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(df_mass["mass_x"], df_mass["mass_y"],
                xerr=df_mass["mass_x_err"], yerr=df_mass["mass_y_err"],
                fmt=".", color="black",
                markersize=5, zorder=10)
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    x_space = np.linspace(xlims[0], xlims[1], 1000)
    out = icaps.fit_model(icaps.fit_lin0, df_mass["mass_x"], df_mass["mass_y"],
                          xerr=df_mass["mass_x_err"], yerr=df_mass["mass_y_err"])
    ax.plot(x_space, icaps.fit_lin0(x_space, *out.beta), color="green", linestyle="-", zorder=100, lw=2, alpha=1,
            label="ODR: {}\n{} = {} \u00b1 {}".format(r"$m_y = a \cdot m_x$", r"$a$", np.round(out.beta[0], 2),
                                                     np.round(out.sd_beta[0], 2)))
    out = icaps.fit_model(icaps.fit_lin, df_mass["mass_x"], df_mass["mass_y"],
                          xerr=df_mass["mass_x_err"], yerr=df_mass["mass_y_err"], fit_type=0, p0=[1.0, 0.0])
    ax.plot(x_space, icaps.fit_lin(x_space, *out.beta), color="red", linestyle="-", zorder=101, lw=2, alpha=1,
            label="ODR: {}\n{} = {:.2f} \u00b1 {}\n{} = ({} \u00b1 {}) {} kg".format(r"$m_y = a \cdot m_x + b$",
                                                                                     r"$a$",
                                                                                     np.round(out.beta[0], 2),
                                                                                     np.round(out.sd_beta[0], 2),
                                                                                     r"$b$",
                                                                                     np.round(out.beta[1]*1e16, 2),
                                                                                     np.round(out.sd_beta[1]*1e16, 2),
                                                                                     r"$\cdot 10^{-16}$"))
    print(out.beta)
    print(out.sd_beta)
    y = icaps.fit_lin(x_space, *out.beta)
    s = np.mean((y-x_space)**2)
    print(s)
    y = df_mass["mass_y"]
    x = df_mass["mass_x"]
    s = np.mean((y - x) ** 2)
    print(s)
    s = np.mean((np.log10(df_mass["mass_y"]) - np.log10(df_mass["mass_x"])) ** 2)
    print("x", s)
    # s = np.mean((np.log10(df_mass["mass_y"]) - np.log10(df_mass["mass_x"])) ** 2)
    # print("y", s)

    out = icaps.fit_model(icaps.fit_pow, df_mass["mass_x"], df_mass["mass_y"],
                          xerr=df_mass["mass_x_err"], yerr=df_mass["mass_y_err"])
    ax.plot(x_space, icaps.fit_pow(x_space, *out.beta), color="blue", linestyle="-", zorder=102, lw=2, alpha=1,
            label="ODR: {}\n{} = {} \u00b1 {}\n{} = {} \u00b1 {}".format(r"$m_y = b \cdot m_x^a$",
                                                                         r"$a$",
                                                                         np.round(out.beta[0], 2),
                                                                         np.round(out.sd_beta[0], 2),
                                                                         r"$b$",
                                                                         np.round(out.beta[1], 2),
                                                                         np.round(out.sd_beta[1], 2)))
    slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
    ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=11, label=r"$m_y = m_x$")
    ax.legend(loc="upper left")
    plt.tight_layout()
    # plt.show()
    plt.savefig(project_path + "plots/Mass_TX_TY.png", dpi=dpi)
    plt.close()

    # ============================================================
    # MASS X VS MASS Y LABELLED
    # ============================================================
    _, ax = plt.subplots()
    ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_ylabel(r"$m_y$ (kg)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(df_mass["mass_x"], df_mass["mass_y"],
                xerr=df_mass["mass_x_err"], yerr=df_mass["mass_y_err"],
                fmt=".", color="black",
                markersize=5, zorder=10)

    data = df_temp.sort_values("ratio1", ascending=False).head(10)
    ax.errorbar(data["mass_x"], data["mass_y"],
                fmt=".", color="blue", marker="x",
                markersize=5, zorder=10)
    for i, row in data.iterrows():
        ax.annotate(str(int(row["particle"])), (row["mass_x"], row["mass_y"]), size=7,
                    bbox=dict(boxstyle="square", fc="w", alpha=0.7, zorder=120))
    data = df_temp.sort_values("ratio2", ascending=False).head(10)
    ax.errorbar(data["mass_x"], data["mass_y"],
                fmt=".", color="green", marker="+",
                markersize=5, zorder=10)
    for i, row in data.iterrows():
        ax.annotate(str(int(row["particle"])), (row["mass_x"], row["mass_y"]), size=7,
                    bbox=dict(boxstyle="square", fc="w", alpha=0.7), zorder=120)
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    out = icaps.fit_model(icaps.fit_lin0, df_mass["mass_x"], df_mass["mass_y"],
                          xerr=df_mass["mass_x_err"], yerr=df_mass["mass_y_err"])
    x_space = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    ax.plot(x_space, icaps.fit_lin0(x_space, *out.beta), color="red", linestyle="-", zorder=100,
            label="ODR\nSlope = {} \u00b1 {}".format(np.round(out.beta[0], 4), np.round(out.sd_beta[0], 4)))
    slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
    ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=11, label=r"$m_y = m_x$")
    ax.legend()
    plt.tight_layout()
    plt.savefig(project_path + "plots/Mass_TX_TY_Labelled.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()



