import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

dpi = 600
max_chisqr_trans = 40e-4
max_chisqr_rot = 60e-4
max_err_ratio_trans_tau = 0.1
max_err_ratio_trans_mass = 0.1
max_err_ratio_rot = 0.5
min_dt_tau_ratio = 2
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
csv_of_final = project_path + "OF_Final_noseq_adapt3.csv"
df = pd.read_csv(csv_of_final)
n_parts = []
print("all", len(df["particle"].unique()))
n_parts.append(len(df["particle"].unique()))

# ============================================================
df_ = df[df["tau_x_err"] / df["tau_x"] <= max_err_ratio_trans_tau]
df_ = df_[df_["tau_y_err"] / df_["tau_y"] <= max_err_ratio_trans_tau]
for a in ["x", "y"]:
    _, ax = plt.subplots()
    ax.errorbar(df_["tau_{}".format(a)] * 1e3, df_["chisqr_{}".format(a)], fmt="o", c="black", ms=1,
                # xerr=df_["tau_{}_err".format(a)] * 1e3
                )
    ax.set_xlabel(r"$\tau_{{{}}}$ in ms".format(a))
    ax.set_ylabel(r"$\chi_{{{}}}^2$".format(a))
    ax.set_xlim(-1, 20)
    # ax.set_ylim(0, 0.015)
    ax.set_yscale("log")
    plt.savefig("C:/Users/ben_s/Desktop/tau_chi_{}.png".format(a), dpi=dpi)
    plt.close()
# ============================================================
def fit_MoI1(x, a):
    return np.log10(x + a)


def fit_MoI2(x, a, b):
    return np.log10(x * (1 + (a/x)**b)**2)


def fit_MoI3(x, a):
    return np.log10(x * (1 + (a/x)**0.5)**2)

df = df[df["chisqr_x"] <= max_chisqr_trans]
df = df[df["chisqr_y"] <= max_chisqr_trans]
print("chisqr_trans", len(df["particle"].unique()))
n_parts.append(len(df["particle"].unique()))
particles = df["particle"].unique()
if max_err_ratio_trans_mass > 0:
    df = df[df["mass_x_err"] / df["mass_x"] <= max_err_ratio_trans_mass]
    df = df[df["mass_y_err"] / df["mass_y"] <= max_err_ratio_trans_mass]
    print("mass_error_ratio", len(df["particle"].unique()))
if max_err_ratio_trans_tau > 0:
    df = df[df["tau_x_err"] / df["tau_x"] <= max_err_ratio_trans_tau]
    df = df[df["tau_y_err"] / df["tau_y"] <= max_err_ratio_trans_tau]
    print("tau_error_ratio", len(df["particle"].unique()))
if min_dt_tau_ratio > 0:
    ndots_x = df["n_dots_x"].to_numpy().astype(int)
    # df = df[np.array([dts_trim[ndot] for ndot in ndots_x]) / (df["tau_x"]*1e3) >= min_dt_tau_ratio]
    # df = df[np.array([dts_trim[ndot] for ndot in ndots_y]) / (df["tau_y"]*1e3) >= min_dt_tau_ratio]
    df = df[np.array([dts_trim[ndot] for ndot in ndots_x]) >= 10]
    ndots_y = df["n_dots_y"].to_numpy().astype(int)
    df = df[np.array([dts_trim[ndot] for ndot in ndots_y]) >= 10]

n_parts.append(len(df["particle"].unique()))
df_rot = df.dropna(subset=["tau_rot"])
print("rot_notnan", len(df_rot["particle"].unique()))
n_parts.append(len(df_rot["particle"].unique()))
df_rot = df_rot[df_rot["chisqr_rot"] <= max_chisqr_rot]
print("chisqr_rot", len(df_rot["particle"].unique()))
n_parts.append(len(df_rot["particle"].unique()))
if max_err_ratio_rot > 0:
    df_rot = df_rot[df_rot["inertia_err"] / df_rot["inertia"] <= max_err_ratio_rot]
    print("inertia_error_ratio", len(df_rot["particle"].unique()))
    df_rot = df_rot[df_rot["tau_rot_err"] / df_rot["tau_rot"] <= max_err_ratio_rot]
    print("tau_error_ratio", len(df_rot["particle"].unique()))
n_parts.append(len(df_rot["particle"].unique()))
plot_path = project_path + "plots/results/"
plot_param_path = plot_path + "params/"
icaps.mk_folders([plot_path, plot_param_path], clear=True)

# df_info = pd.DataFrame(data={
#     "step": [csv_of_final.split("/")[-1], "max_chisqr_trans", "max_err_ratio", "rot_notnan", "max_chisqr_rot",
#              "max_err_ratio_rot"],
#     "value": [0, max_chisqr_trans, max_err_ratio_trans, 0, max_chisqr_rot, max_err_ratio_rot],
#     "n_parts": n_parts
# })
# df_info.to_csv(plot_path + "info.csv", index=False)

# ============================================================
# RESULT PLOTS
# ============================================================
df_ = df.dropna(subset=["n_dots_x"])
ndots = df_["n_dots_x"].to_numpy().astype(int)
maxdt = [dts_trim[ndot] for ndot in ndots]

# ============================================================
_, ax = plt.subplots()
ax.scatter(df_["tau_x"] * 1000, maxdt, c="black", s=5)
# ax.plot((0, 50), (0, 50), c="red", lw=2)
ax.axhline(6, c="red", lw=2)
ax.plot((0, 50), (0, 100), c="red", lw=2)
ax.set_xlabel(r"$\tau_x$ (ms)")
ax.set_ylabel(r"$\Delta t_{max}$")
ax.set_xlim(0, 20)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.scatter(df_["mass_x"], maxdt, c="black", s=5)
ax.set_xscale("log")
ax.axhline(6, c="red", lw=2)
# ax.plot((0, 50), (0, 50), c="red", lw=2)
# ax.plot((0, 50), (0, 100), c="red", lw=2)
ax.set_xlabel(r"$m_x$ (ms)")
ax.set_ylabel(r"$\Delta t_{max}$")
# ax.set_xlim(0, 20)
# plt.show()
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.set_xlabel(r"$m_{\rm x}+m_{\rm y}$ (kg)")
ax.set_ylabel(r"$\frac{m_{\rm x}-m_{\rm y}}{m_{\rm x}+m_{\rm y}}$")
ax.set_xscale("log")
ax.errorbar(df["mass_x"] + df["mass_y"], (df["mass_x"] - df["mass_y"])/(df["mass_x"] + df["mass_y"]),
            # xerr=df["mass_x_err"], yerr=df["mass_y_err"],
            fmt=".", color="black",
            markersize=5, zorder=10)
plt.tight_layout()
plt.savefig(plot_path + "Mass_RelErr.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.set_xlabel(r"$(m_{\rm x}-m_{\rm y})\,/\,(m_{\rm x}+m_{\rm y})$")
ax.set_ylabel(r"Frequency")
y = (df["mass_x"] - df["mass_y"])/(df["mass_x"] + df["mass_y"])
bin_size = 0.04
bin_lim = np.ceil(np.max(np.abs(y)) * 100) / 100
bins = np.arange(bin_size/2, bin_lim + bin_size, bin_size)
bins = np.append(-bins[::-1], bins)
hist, _ = np.histogram(y, bins=bins)
# hist = hist / np.max(hist)
ax.bar(bins[:-1] + bin_size/2, hist, width=bin_size*0.9, color="black")
ax.set_xlim(-0.86, +0.86)
ax.set_ylim(0, 1.05 * np.max(hist))
plt.tight_layout()
plt.savefig(plot_path + "Mass_RelErr_Hist.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.set_xlabel(r"$\tau_{\rm x}+\tau_{\rm y}$ (ms)")
ax.set_ylabel(r"$\frac{\tau_{\rm x}-\tau_{\rm y}}{\tau_{\rm x}+\tau_{\rm y}}$")
# ax.set_yscale("log")
ax.errorbar((df["tau_x"] + df["tau_y"]) * 1000, (df["tau_x"] - df["tau_y"])/(df["tau_x"] + df["tau_y"]),
            # xerr=df["mass_x_err"], yerr=df["mass_y_err"],
            fmt=".", color="black",
            markersize=5, zorder=10)
ax.set_xlim(left=1, right=np.min([np.max((df["tau_x"] + df["tau_y"]) * 1000), 40]))
plt.tight_layout()
plt.savefig(plot_path + "Tau_RelErr.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.set_xlabel(r"$(\tau_{\rm x}-\tau_{\rm y})\,/\,(\tau_{\rm x}+\tau_{\rm y})$")
ax.set_ylabel(r"Frequency")
y = (df["tau_x"] - df["tau_y"])/(df["tau_x"] + df["tau_y"])
bin_size = 0.04
bin_lim = np.ceil(np.max(np.abs(y)) * 100) / 100
bins = np.arange(bin_size/2, bin_lim + bin_size, bin_size)
bins = np.append(-bins[::-1], bins)
hist, _ = np.histogram(y, bins=bins)
# hist = hist / np.max(hist)
ax.bar(bins[:-1] + bin_size/2, hist, width=bin_size*0.9, color="black")
ax.set_xlim(-0.86, +0.86)
ax.set_ylim(0, 1.05 * np.max(hist))
plt.tight_layout()
plt.savefig(plot_path + "Tau_RelErr_Hist.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots(figsize=(4.8, 4.8))
ax.minorticks_on()
ax.set_xlabel(r"$m_{\rm x}$ (kg)")
ax.set_ylabel(r"$m_{\rm y}$ (kg)")
ax.errorbar(df["mass_x"], df["mass_y"],
            xerr=df["mass_x_err"], yerr=df["mass_y_err"],
            fmt=".", color="black",
            markersize=5, zorder=100)
ax.set_xlim(icaps.const.m_0 * 0.1, icaps.const.m_0 * 7.2)
ax.set_ylim(*ax.get_xlim())
# xlims = ax.get_xlim()
# ylims = ax.get_ylim()
# slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
# ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=101, label=r"$m_y = m_x$"
mono_dots = [icaps.const.m_0 * i for i in range(1, 7)]
ax.scatter(mono_dots, mono_dots, c="red", zorder=99)
for i in range(1, 7):
    circle = plt.Circle((icaps.const.m_0 * i, icaps.const.m_0 * i),
                        radius=icaps.const.m_0_err * i,
                        color="darkorange", zorder=1)
    ax.add_patch(circle)
    circle = plt.Circle((icaps.const.m_0 * i, icaps.const.m_0 * i),
                        radius=2 * icaps.const.m_0_err * i,
                        color="gold", zorder=0)
    ax.add_patch(circle)
plt.tight_layout()
plt.savefig(plot_path + "Mass_XY_Small.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.set_xlabel(r"$m_{\rm x}$ (kg)")
ax.set_ylabel(r"$m_{\rm y}$ (kg)")
ax.set_xscale("log")
ax.set_yscale("log")
ax.errorbar(df["mass_x"], df["mass_y"],
            xerr=df["mass_x_err"], yerr=df["mass_y_err"],
            fmt=".", color="black",
            markersize=5, zorder=10)
xlims = ax.get_xlim()
ylims = ax.get_ylim()
slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=11, label=r"$m_y = m_x$")
ins = ax.inset_axes([0.66, 0.12, 0.43*4.8/6.4, 0.43])
ins.errorbar(df["mass_x"] / icaps.const.m_0, df["mass_y"] / icaps.const.m_0,
             xerr=df["mass_x_err"]/icaps.const.m_0, yerr=df["mass_y_err"] / icaps.const.m_0,
             fmt=".", color="black", elinewidth=1,
             markersize=2, zorder=100)
ins.set_xlabel(r"$m_{\rm x}$ ($m_{\rm 0}$)")
ins.set_ylabel(r"$m_{\rm y}$ ($m_{\rm 0}$)")
ins.set_xlim(0.1, 7.2)
mono_dots = np.arange(1, 8, 1)
ins.xaxis.set_ticks(mono_dots)
ins.yaxis.set_ticks(mono_dots)
ins.set_ylim(*ins.get_xlim())
ins.scatter(mono_dots[:-1], mono_dots[:-1], c="red", zorder=99, s=4)
for i in range(1, 7):
    circle = plt.Circle((i, i),
                        radius=icaps.const.m_0_err / icaps.const.m_0 * i,
                        color="darkorange", zorder=1)
    ins.add_patch(circle)
    circle = plt.Circle((i, i),
                        radius=2 * icaps.const.m_0_err / icaps.const.m_0 * i,
                        color="gold", zorder=0)
    ins.add_patch(circle)
plt.tight_layout()
# plt.show()
plt.savefig(plot_path + "Mass_XY.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.errorbar(df["tau_x"] * 1000,
            df["tau_y"] * 1000,
            xerr=df["tau_x_err"] * 1000,
            yerr=df["tau_y_err"] * 1000,
            fmt=".", color="black",
            markersize=5, zorder=10)
ax.set_xlabel(r"$\tau_{\rm t, x}$ (ms)")
ax.set_ylabel(r"$\tau_{\rm t, y}$ (ms)")
# ax.set_xscale("log")
# ax.set_yscale("log")
xlims = ax.get_xlim()
ylims = ax.get_ylim()
lims = (min(xlims[0], ylims[0]), max(xlims[1], ylims[1]))
ax.set_xlim(lims)
ax.set_ylim(lims)
ax.set_xlim(0, 20)
ax.set_ylim(0, 20)
ax.plot(ax.get_xlim(), ax.get_xlim(), color="gray", linestyle="--")
plt.tight_layout()
# plt.show()
plt.savefig(plot_path + "Tau_XY.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
tau = np.hstack([df["tau_x"].to_numpy(), df["tau_y"].to_numpy()]).ravel()
start_frame = np.tile(df["start_frame"], 2)
mass = np.hstack([df["mass_x"].to_numpy(), df["mass_y"].to_numpy()]).ravel()
mass_err = np.hstack([df["mass_x_err"].to_numpy(), df["mass_y_err"].to_numpy()]).ravel()
tau_err = np.hstack([df["tau_x_err"].to_numpy(), df["tau_y_err"].to_numpy()]).ravel()
# start_frame = start_frame[tau < 0.03]
# mass = mass[tau < 0.03]
# mass_err = mass_err[tau < 0.03]
# tau_err = tau_err[tau < 0.03]
# tau = tau[tau < 0.03]
tau1 = np.sort(tau[start_frame < icaps.const.end_ff]) * 1000
tau2 = np.sort(tau[start_frame >= icaps.const.end_ff]) * 1000
norm_tau1 = np.linspace(1 / len(tau1), 1, len(tau1))
norm_tau2 = np.linspace(1 / len(tau2), 1, len(tau2))
p1 = icaps.const.pressure_i1
p2 = icaps.const.pressure_i2
tau3 = tau2 * p2 / p1
norm_tau3 = np.linspace(1 / len(tau3), 1, len(tau3))
ax.scatter(tau1, norm_tau1, c="blue", marker="o", s=5, label=r"$p_1$" + " = {:.1f} Pa".format(p1))
ax.scatter(tau2, norm_tau2, c="red", marker="s", s=5, label=r"$p_2$" + " = {:.1f} Pa".format(p2))
ax.scatter(tau3, norm_tau3, c="orange", marker="d", s=5, label=r"$\tau_2 \cdot \frac{p_2}{p_1}$")
ax.axvline(4.8, c="gray", ls="--")
ax.axvline(10, c="gray", ls="--")
try:
    perc1 = [np.max(norm_tau1[tau1 < 4.8]), np.max(norm_tau1[tau1 <= 10])]
except ValueError:
    perc1 = [0., np.max(norm_tau1[tau1 <= 10])]
try:
    perc3 = [np.max(norm_tau3[tau3 < 4.8]), np.max(norm_tau3[tau3 <= 10])]
except ValueError:
    perc3 = [0., np.max(norm_tau3[tau3 <= 10])]
ax.set_xlim(left=1, right=np.min([np.max(np.append(tau1, np.append(tau2, tau3))), 40]))
ax.annotate("{:.0f} %".format(perc1[0]*100), xy=(1.7, 0.2), xycoords="data", size=10, ha="left", va="top", c="blue")
ax.annotate("{:.0f} %".format(perc3[0]*100), xy=(1.7, 0.15), xycoords="data", size=10, ha="left", va="top", c="orange")
ax.annotate("{:.0f} %".format((perc1[1]-perc1[0])*100), xy=(7.5, 0.2+0.3), xycoords="data", size=10, ha="left",
            va="top", c="blue")
ax.annotate("{:.0f} %".format((perc3[1]-perc3[0])*100), xy=(7.5, 0.15+0.3), xycoords="data", size=10, ha="left",
            va="top", c="orange")
ax.annotate("{:.0f} %".format((1.-perc1[1])*100), xy=(10.7, 0.2+0.7), xycoords="data", size=10, ha="left", va="top",
            c="blue")
ax.annotate("{:.0f} %".format((1.-perc3[1])*100), xy=(10.7, 0.15+0.7), xycoords="data", size=10, ha="left", va="top",
            c="orange")
ax.legend(loc="lower right")
ax.set_xlabel(r"$\tau_{\rm t}$ (ms)")
ax.set_ylabel("Normalized Cumulative Frequency")
# plt.show()
plt.savefig(plot_path + "Tau_Cumu.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.errorbar(df["tau"] * 1000,
            df["tau_rot"] * 1000,
            xerr=df["tau_err"] * 1000,
            yerr=df["tau_rot_err"] * 1000,
            fmt=".", color="black",
            markersize=5, zorder=10)
ax.set_xlabel(r"$\tau_{\rm t}$ (ms)")
ax.set_ylabel(r"$\tau_{\rm r}$ (ms)")
# ax.set_xscale("log")
# ax.set_yscale("log")
xlims = ax.get_xlim()
ylims = ax.get_ylim()
lims = (min(xlims[0], ylims[0]), max(xlims[1], ylims[1]))
ax.set_xlim(lims)
ax.set_ylim(lims)
ax.set_xlim(0, 20)
ax.set_ylim(0, 20)
ax.plot(lims, lims, color="gray", linestyle="--")
plt.tight_layout()
# plt.show()
plt.savefig(plot_path + "Tau_TR.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.set_xlabel(r"$m$ (kg)")
ax.set_ylabel(r"$\tau_{\rm t}$ (ms)")
ax.set_xscale("log")
# ax.set_yscale("log")
ax.errorbar(mass[start_frame < icaps.const.end_ff], tau[start_frame < icaps.const.end_ff] * 1000,
            xerr=mass_err[start_frame < icaps.const.end_ff], yerr=tau_err[start_frame < icaps.const.end_ff] * 1000,
            fmt="o", color="blue", label=r"$p_1$" + " = {:.1f} Pa".format(p1),
            markersize=5, zorder=10)
ax.errorbar(mass[start_frame >= icaps.const.end_ff], tau[start_frame >= icaps.const.end_ff] * p2 / p1 * 1000,
            xerr=mass_err[start_frame >= icaps.const.end_ff], yerr=tau_err[start_frame >= icaps.const.end_ff] * 1000,
            fmt="d", color="orange", label=r"$\tau_2 \cdot \frac{p_2}{p_1}$",
            markersize=5, zorder=10)
tau_corr = np.append(tau[start_frame < icaps.const.end_ff], tau[start_frame >= icaps.const.end_ff] * p2 / p1) * 1000
out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(tau_corr), fit_type=0)
xspace = np.linspace(np.min(mass), np.max(mass), 1000)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out.beta), c="black", lw=3, zorder=100, alpha=1.0, ls="--",
        label=r"$\tau_t \propto m^a$" + "\n"
              + r"$a = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]))
ax.set_ylim(bottom=0, top=40)

# xlims = ax.get_xlim()
# ylims = ax.get_ylim()
# slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
# ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=11, label=r"$m_y = m_x$")
ax.legend()
plt.tight_layout()
# plt.show()
plt.savefig(plot_path + "Tau_Mass.png", dpi=dpi)
plt.close()


# _, ax = plt.subplots()
# m = np.sort(df["mass"].to_numpy())/icaps.const.m_p
# m = m[m <= 7.25]
# # m_cumu = np.linspace(1 / len(m_cumu), 1, len(m_cumu))
# bins = np.arange(-0.25, 7.25, 0.5)
# hist, _ = np.histogram(m, bins=bins)
# ax.scatter((bins+0.25)[:-1], hist, c="black", s=10)
# plt.show()

# ============================================================
_, ax = plt.subplots()
ax.minorticks_on()
ax.errorbar(df.loc[df["start_frame"] < icaps.const.end_ff, "mass"],
            df.loc[df["start_frame"] < icaps.const.end_ff, "tau"],
            xerr=df.loc[df["start_frame"] < icaps.const.end_ff, "mass_err"],
            yerr=df.loc[df["start_frame"] < icaps.const.end_ff, "tau_err"],
            fmt="o", c="blue", markersize=5, label="p1")
ax.errorbar(df.loc[df["start_frame"] >= icaps.const.end_ff, "mass"],
            df.loc[df["start_frame"] >= icaps.const.end_ff, "tau"] * icaps.const.pressure_i2 / icaps.const.pressure_i1,
            xerr=df.loc[df["start_frame"] >= icaps.const.end_ff, "mass_err"],
            yerr=df.loc[df["start_frame"] >= icaps.const.end_ff, "tau_err"],
            fmt="d", c="orange", markersize=5, label="p2")
ax.set_xscale("log")
ax.legend()
# plt.show()
plt.close()


# ============================================================
# FRACTAL DIMENSION PLOTS
# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Brownian Mass (kg)")
ax.set_ylabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlim(2e-15, 1e-12)
ax.set_ylim(5e-27, 1e-21)
# df_rot = df.dropna(subset=["tau_rot"])
# # df_rot = df_rot[df_rot["chisqr_rot"] <= 10e-4]
# df_rot = df_rot[df_rot["inertia_err"]/df_rot["inertia"] <= 0.5]
# df_rot = df_rot[df_rot["tau_rot_err"]/df_rot["tau_rot"] <= 0.5]
# print(len(df_rot["particle"].unique()))
mass = df_rot["mass"].to_numpy()
inertia = df_rot["inertia"].to_numpy()
mass_err = df_rot["mass_err"].to_numpy()
inertia_err = df_rot["inertia_err"].to_numpy()
ax.errorbar(mass, inertia, xerr=mass_err, yerr=inertia_err,
            fmt="o", c="black", markersize=5)
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(inertia),
                      (mass_err / mass) / np.log(10), (inertia_err / inertia) / np.log(10),
                      fit_type=0)
fracdim = 2 / (out.beta[0] - 1)
fracdim_low = 2 / (out.beta[0] + out.sd_beta[0] - 1)
fracdim_high = 2 / (out.beta[0] - out.sd_beta[0] - 1)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out.beta), c="red", lw=2, zorder=10, alpha=0.5,
        label=r"$\alpha = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]) + "\n"
              + r"$d_f = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high-fracdim, fracdim_low-fracdim))
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "FracDim_Brownian.png", dpi=dpi)
plt.close()

# ============================================================
resolution = 1/24
alpha = np.arange(5/3, 3 + resolution/2, resolution)
chisqr0 = np.zeros(alpha.shape)
chisqr1 = np.zeros(alpha.shape)
for i, alpha_ in enumerate(alpha):
    out = icaps.fit_model(lambda x, b: icaps.fit_lin(x, alpha_, b), np.log10(mass), np.log10(inertia),
                          (mass_err / mass) / np.log(10), (inertia_err / inertia) / np.log(10),
                          fit_type=0)
    chisqr0[i] = np.sum((np.log10(inertia) - icaps.fit_lin(np.log10(mass), alpha_, *out.beta)) ** 2) / len(np.log10(mass))
    chisqr1[i] = out.res_var
_, ax = plt.subplots()
ax.scatter(alpha, chisqr1, c="black", marker="o", s=5)
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$\chi^2$")
ax2 = ax.twiny()
ax2.set_xlabel(r"$d_{\rm f}$")
# ax2.scatter(2/(alpha - 1), chisqr0, c="red", marker="x", s=5, alpha=0)
# ax2.invert_xaxis()
new_tick_locations = np.array(alpha[::4])


def tick_function(q):
    v = 2/(q-1)
    return ["%.2f" % z for z in v]


ax2.set_xlim(ax.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
plt.savefig(plot_path + "alpha.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Optical Mass (kg)")
ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlim(2e-15, 1e-12)
ax.set_ylim(5e-27, 1e-21)
mass = df_rot["mean_ex"].to_numpy() / icaps.const.ldm_ex0 * icaps.const.m_0
inertia = mass * (df_rot["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2 # todo: Hotfix
inertia = inertia + 48.9e-27 - 2 * np.sqrt(inertia * 48.9e-27)
mass_err = mass * np.sqrt((df_rot["mean_ex_err"].to_numpy()/df_rot["mean_ex"].to_numpy()) ** 2
                          + (icaps.const.ldm_ex0_err / icaps.const.ldm_ex0) ** 2
                          + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
inertia_err = inertia * np.sqrt((mass_err/mass)**2 + (df_rot["mean_gyrad_err"].to_numpy() / df_rot["mean_gyrad"].to_numpy())**2)
mass = mass[inertia > 0]
mass_err = mass_err[inertia > 0]
inertia_err = inertia_err[inertia > 0]
inertia = inertia[inertia > 0] # todo: Hotfix

ax.errorbar(mass, inertia, xerr=mass_err, yerr=inertia_err,
            fmt="o", c="black", markersize=5)
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(inertia),
                      # (mass1_err / mass1) / np.log(10), (inertia1_err / inertia1) / np.log(10),
                      fit_type=0)
fracdim = 2 / (out.beta[0] - 1)
fracdim_low = 2 / (out.beta[0] + out.sd_beta[0] - 1)
fracdim_high = 2 / (out.beta[0] - out.sd_beta[0] - 1)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out.beta), c="red", lw=2, zorder=10, alpha=0.5,
        label=r"$\alpha = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]) + "\n"
              + r"$d_f = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high-fracdim, fracdim_low-fracdim))
out_optb = out
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "FracDim_Optical_B.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Optical Mass (kg)")
ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlim(2e-15, 1e-12)
ax.set_ylim(5e-27, 1e-21)
mass = df["mean_ex"].to_numpy() / icaps.const.ldm_ex0 * icaps.const.m_0
inertia = mass * (df["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2 # todo: Hotfix
inertia = inertia + 48.9e-27 - 2 * np.sqrt(inertia * 48.9e-27)
mass_err = mass * np.sqrt((df["mean_ex_err"].to_numpy()/df["mean_ex"].to_numpy()) ** 2
                          + (icaps.const.ldm_ex0_err / icaps.const.ldm_ex0) ** 2
                          + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
inertia_err = inertia * np.sqrt((mass_err/mass)**2 + (df["mean_gyrad_err"].to_numpy() / df["mean_gyrad"].to_numpy())**2)
mass = mass[inertia > 0]
mass_err = mass_err[inertia > 0]
inertia_err = inertia_err[inertia > 0]
inertia = inertia[inertia > 0] # todo: Hotfix

ax.errorbar(mass, inertia, xerr=mass_err, yerr=inertia_err,
            fmt="o", c="black", markersize=5)
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(inertia),
                      # (mass1_err / mass1) / np.log(10), (inertia1_err / inertia1) / np.log(10),
                      fit_type=0)
fracdim = 2 / (out.beta[0] - 1)
fracdim_low = 2 / (out.beta[0] + out.sd_beta[0] - 1)
fracdim_high = 2 / (out.beta[0] - out.sd_beta[0] - 1)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out.beta), c="red", lw=2, zorder=10, alpha=0.5,
        label=r"$\alpha = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]) + "\n"
              + r"$d_f = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high-fracdim, fracdim_low-fracdim))
out_opt = out
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "FracDim_Optical.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Brownian Mass (kg)")
ax.set_ylabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlim(2e-15, 1e-12)
ax.set_ylim(5e-27, 1e-21)
# df_rot = df.dropna(subset=["tau_rot"])
df_rot_ = df_rot.copy()
# df_rot = df_rot[df_rot["chisqr_rot"] <= 10e-4]
df_rot_ = df_rot_[df_rot_["inertia_err"]/df_rot_["inertia"] <= 0.5]
df_rot_ = df_rot[df_rot_["tau_rot_err"]/df_rot_["tau_rot"] <= 0.5]
mass = df_rot_["mass"].to_numpy()
inertia = df_rot_["inertia"].to_numpy()
mass_err = df_rot_["mass_err"].to_numpy()
inertia_err = df_rot_["inertia_err"].to_numpy()

ax.errorbar(mass, inertia, xerr=mass_err, yerr=inertia_err,
            fmt="o", c="black", markersize=5)
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(inertia),
                      (mass_err / mass) / np.log(10), (inertia_err / inertia) / np.log(10),
                      fit_type=0)
fracdim = 2 / (out.beta[0] - 1)
fracdim_low = 2 / (out.beta[0] + out.sd_beta[0] - 1)
fracdim_high = 2 / (out.beta[0] - out.sd_beta[0] - 1)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out.beta), c="blue", lw=2, ls="-.", zorder=10, alpha=0.5,
        label="Brownian fit\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]) + "\n"
              + r"$d_f = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high-fracdim, fracdim_low-fracdim))
fracdim = 2 / (out_opt.beta[0] - 1)
fracdim_low = 2 / (out_opt.beta[0] + out_opt.sd_beta[0] - 1)
fracdim_high = 2 / (out_opt.beta[0] - out_opt.sd_beta[0] - 1)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out_opt.beta), c="red", lw=2, ls="-", zorder=10, alpha=0.5,
        label="Optical fit\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt.beta[0], out_opt.sd_beta[0]) + "\n"
              + r"$d_f = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high-fracdim, fracdim_low-fracdim))
fracdim = 2 / (out_optb.beta[0] - 1)
fracdim_low = 2 / (out_optb.beta[0] + out_optb.sd_beta[0] - 1)
fracdim_high = 2 / (out_optb.beta[0] - out_optb.sd_beta[0] - 1)
ax.plot(xspace, 10**icaps.fit_lin(np.log10(xspace), *out_optb.beta), c="green", lw=2, ls="--", zorder=10, alpha=0.5,
        label="Optical B fit\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_optb.beta[0], out_optb.sd_beta[0]) + "\n"
              + r"$d_f = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high-fracdim, fracdim_low-fracdim))

ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "FracDim_Brownian2.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Brownian Mass (kg)")
ax.set_ylabel(r"$\langle E\rangle \cdot m_0$ (arb. units)")
ax.set_xscale("log")
ax.set_yscale("log")
# df_rot = df.dropna(subset=["tau_rot"])
df_rot_ = df.copy()
mass_brown = df_rot_["mass"].to_numpy()
# mass_opt = df_rot_["mean_ex"].to_numpy() / icaps.const.ldm_ex0 * icaps.const.m_p
mass_opt = df_rot_["mean_ex"].to_numpy() * icaps.const.m_0
mass_brown_err = df_rot_["mass_err"].to_numpy()
mass_opt_err = mass_opt * np.sqrt((df_rot_["mean_ex_err"].to_numpy()/df_rot_["mean_ex"].to_numpy()) ** 2
                                  + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
# mass_opt_err = None
ax.errorbar(mass_brown, mass_opt, xerr=mass_brown_err, yerr=mass_opt_err,
            fmt="o", c="black", markersize=3)
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin0, mass_brown, mass_opt,
                      mass_brown_err, mass_opt_err)
ax.plot(xspace, icaps.fit_lin0(xspace, *out.beta), c="red", ls="-", lw=2,
        label=r"$\langle E\rangle \cdot m_0 = E_0 \cdot m_{Brown}$" + "\n"
              + r"$E_0 = {{{:.0f}}} \pm {{{:.0f}}}$".format(out.beta[0], out.sd_beta[0]), alpha=0.5, zorder=10)
# ax.plot(xspace, xspace, color="gray", ls="--", lw=2, zorder=-1)
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "OptBrown_Mass.png", dpi=dpi)
plt.close()

# ============================================================
# ============================================================
# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Optical Mass (kg)")
ax.set_ylabel(r"Brownian Mass (kg)")
ax.set_xscale("log")
ax.set_yscale("log")
df_rot_ = df_rot.copy()
mass_brown = df_rot_["mass"].to_numpy()
mass_opt = df_rot_["mean_ex"].to_numpy() / icaps.const.ldm_ex0 * icaps.const.m_0
mass_brown_err = df_rot_["mass_err"].to_numpy()
mass_opt_err = mass_opt * np.sqrt((df_rot_["mean_ex_err"].to_numpy()/df_rot_["mean_ex"].to_numpy()) ** 2
                                  + (icaps.const.ldm_ex0_err / icaps.const.ldm_ex0) ** 2
                                  + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
ax.errorbar(mass_opt, mass_brown, xerr=mass_opt_err, yerr=mass_brown_err,
            fmt="o", c="black", markersize=5)
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_slope1, np.log10(mass_opt), np.log10(mass_brown))
ax.plot(xspace, 10 ** icaps.fit_slope1(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
        label="{} \u00b1 {}".format(out.beta[0], out.sd_beta[0]))
ax.plot(xspace, xspace, color="gray", ls="--", lw=2, zorder=-1)
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "OptBrown_Mass_adapted.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
ax.set_xscale("log")
ax.set_yscale("log")
# df_rot = df.dropna(subset=["tau_rot"])
inertia_brown = df_rot["inertia"].to_numpy()
inertia_opt = mass_opt * (df_rot["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
inertia_brown_err = df_rot["inertia_err"].to_numpy()
# inertia_opt_err = df_rot["gyrad_err"].to_numpy()
# inertia_opt_err = None
inertia_opt_err = inertia_opt * np.sqrt((mass_opt_err/mass_opt)**2
                                        + (df_rot["mean_gyrad_err"].to_numpy() / df_rot["mean_gyrad"].to_numpy())**2)
ax.errorbar(inertia_brown, inertia_opt, xerr=inertia_brown_err, yerr=inertia_opt_err,
            fmt="o", c="black", markersize=5,
            # label="1st injection"
            )

# MoI_Opt = MoI_BM * (1+ [a/MoI_BM]^b)^2,
ax.set_ylim(1e-25, 1e-21)
ax.set_xlim(*ax.get_ylim())
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
xlogpspace = np.logspace(-25, -21, 1000)
ax.plot(xspace, xspace, color="gray", ls="--", lw=2, zorder=-1)
log_err = np.log10(inertia_opt_err) / np.log10(inertia_opt) / np.log(10)
out1 = icaps.fit_model(fit_MoI1, inertia_brown, np.log10(inertia_opt), fit_type=0,
                       xerr=inertia_brown_err, yerr=log_err
                       )
chisqr1 = np.sum((np.log10(inertia_opt) - fit_MoI1(inertia_brown, *out1.beta)) ** 2) / len(inertia_brown)
ax.plot(xlogpspace, 10 ** fit_MoI1(xlogpspace, *out1.beta), ls="-", c="red", zorder=10,
        label="a = {:.4f} * 10^-25".format(out1.beta[0] * 1e25)
              + "\n" + r"$\chi^2$" + " = {:.4f}".format(chisqr1))
out2 = icaps.fit_model(fit_MoI2, inertia_brown, np.log10(inertia_opt), fit_type=0, p0=[1e-24, 1],
                       xerr=inertia_brown_err, yerr=log_err
                       )
chisqr2 = np.sum((np.log10(inertia_opt) - fit_MoI2(inertia_brown, *out2.beta)) ** 2) / len(inertia_brown)
ax.plot(xlogpspace, 10 ** fit_MoI2(xlogpspace, *out2.beta), ls="-.", c="blue", zorder=10,
        label="a = {:.4f} * 10^-27\nb = {:.4f}".format(out2.beta[0] * 1e27, out2.beta[1])
              + "\n" + r"$\chi^2$" + " = {:.4f}".format(chisqr2))
out3 = icaps.fit_model(fit_MoI3, inertia_brown, np.log10(inertia_opt), fit_type=0, p0=[1e-24],
                       xerr=inertia_brown_err, yerr=log_err
                       )
chisqr3 = np.sum((np.log10(inertia_opt) - fit_MoI3(inertia_brown, *out3.beta)) ** 2) / len(inertia_brown)
ax.plot(xlogpspace, 10 ** fit_MoI3(xlogpspace, *out3.beta), ls="--", c="green", zorder=10,
        label="a = {:.4f} * 10^-27".format(out3.beta[0] * 1e27)
              + "\n" + r"$\chi^2$" + " = {:.4f}".format(chisqr3))
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "OptBrown_Inertia.png", dpi=dpi)
plt.close()

_, ax = plt.subplots(nrows=2)
res1 = np.log10(inertia_opt) - fit_MoI1(inertia_brown, *out1.beta)
res2 = np.log10(inertia_opt) - fit_MoI2(inertia_brown, *out2.beta)
ax[0].scatter(inertia_brown, res1, c="red", marker="o", s=7,
              label=r"$MoI_{opt} = MoI_{brown} + a$")
ax[1].scatter(inertia_brown, res2, facecolor="none", edgecolor="blue", marker="o", s=7,
              label=r"$MoI_{opt} = MoI_{brown} * (1 + (a/MoI_{brown})^b)^2$")
# ax[0].legend()
# ax[1].legend()
ax[0].set_xscale("log")
# ax[0].set_yscale("log")
ax[1].set_xscale("log")
# ax[1].set_yscale("log")
ax[0].set_xlim(1e-25, 1e-21)
ax[1].set_xlim(*ax[0].get_xlim())
ax[0].set_ylim(-0.52, 0.52)
ax[1].set_ylim(-0.52, 0.52)
ax[1].set_xlabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
ax[0].set_ylabel("Residual (log)")
ax[1].set_ylabel("Residual (log)")
plt.savefig(plot_path + "Inertia_Residuals.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Brownian Radius of Gyration (m)")
ax.set_ylabel(r"Brownian Mass (kg)")
ax.set_xlim(1e-6, 4e-5)
ax.set_ylim(1e-15, 1e-12)
ax.set_xscale("log")
ax.set_yscale("log")
# df_rot = df.dropna(subset=["tau_rot"])
mass_brown = df_rot["mass"].to_numpy()
gyrad_brown = np.sqrt(df_rot["inertia"]/df_rot["mass"])
mass_brown = mass_brown[gyrad_brown > 1e-6]
gyrad_brown = gyrad_brown[gyrad_brown > 1e-6]
# mass_brown_err = df_rot["mass_err"].to_numpy()
mass_brown_err = None
# mass_opt_err = df_rot["mean_ex_err"].to_numpy()
gyrad_brown_err = None
ax.errorbar(gyrad_brown, mass_brown, xerr=gyrad_brown_err, yerr=mass_brown_err,
            fmt="o", c="black", markersize=5,
            # label="1st injection"
            )
# ax.errorbar(mass_brown, gyrad_brown, xerr=mass_brown_err, yerr=gyrad_brown_err,
#             fmt="o", c="black", markersize=5,
#             # label="1st injection"
#             )


def fit2(x, b):
    return 1.6 * x + b


xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin, np.log10(gyrad_brown), np.log10(mass_brown), fit_type=0)
# out = icaps.fit_model(icaps.fit_lin, np.log10(mass_brown), np.log10(gyrad_brown))
# out = icaps.fit_model(fit2, np.log10(gyrad_brown), np.log10(mass_brown), fit_type=0)
ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
        label="{} \u00b1 {}".format(out.beta[0], out.sd_beta[0]))
# ax.plot(xspace, 10 ** fit2(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
#         label="{} \u00b1 {}".format(out.beta[0], out.sd_beta[0]))

ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "GyradMass_Brown.png", dpi=dpi)
plt.close()

# ============================================================
_, ax = plt.subplots()
ax.set_xlabel(r"Optical Radius of Gyration (m)")
ax.set_ylabel(r"Optical Mass (kg)")
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlim(1e-6, 4e-5)
ax.set_ylim(1e-15, 1e-12)
# df_rot = df.dropna(subset=["tau_rot"])
mass_opt = df["mean_ex"].to_numpy() / icaps.const.ldm_ex0 * icaps.const.m_0
gyrad_opt = df["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6
# mass_opt_err = df["mass_err"].to_numpy()
mass_opt_err = None
# mass_opt_err = df_rot["mean_ex_err"].to_numpy()
gyrad_opt_err = None
ax.errorbar(gyrad_opt, mass_opt, xerr=gyrad_opt_err, yerr=mass_opt_err,
            fmt="o", c="black", markersize=5,
            )
xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
out = icaps.fit_model(icaps.fit_lin, np.log10(gyrad_opt), np.log10(mass_opt), fit_type=0)
ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
        label="{} \u00b1 {}".format(out.beta[0], out.sd_beta[0]))
ax.legend()
plt.tight_layout()
plt.savefig(plot_path + "GyradMass_Opt.png", dpi=dpi)
plt.close()

# ============================================================
# PARAM PLOTS
# ============================================================
tau_lims = (1, 30)
mass_lims = (1e-15, 2e-12)
chisqr_lims = (2e-5, 2.2e-2)
ratio_lims = (4e-3, 0.3)
for axis in ["x", "y"]:
    _, ax = plt.subplots()
    ax.scatter(df["tau_{}".format(axis)] * 1000, df["chisqr_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\tau_{{\rm {}}}$ (ms)".format(axis))
    ax.set_ylabel(r"$\chi_{{\rm {}}}^2$".format(axis))
    ax.set_xlim(*tau_lims)
    ax.set_ylim(*chisqr_lims)
    plt.savefig(plot_param_path + "tau_chisqr_{}.png".format(axis), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["chisqr_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{{\rm {}}}$ (kg)".format(axis))
    ax.set_ylabel(r"$\chi_{{\rm {}}}^2$".format(axis))
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*chisqr_lims)
    plt.savefig(plot_param_path + "mass_chisqr_{}.png".format(axis), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["tau_{}".format(axis)] * 1000, c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{{\rm {}}}$ (kg)".format(axis))
    ax.set_ylabel(r"$\tau_{{\rm {}}}$ (ms)".format(axis))
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*tau_lims)
    plt.savefig(plot_param_path + "mass_tau_{}.png".format(axis), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["mean_ex"], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{{\rm {}}}$ (kg)".format(axis))
    ax.set_ylabel("Mean Intensity")
    ax.set_xlim(*mass_lims)
    plt.savefig(plot_param_path + "mass_int_{}.png".format(axis), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["mass_{}".format(axis)], df["mass_{}_err".format(axis)]/df["mass_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_{{\rm {}}}$ in kg".format(axis))
    ax.set_ylabel(r"$\Delta m_{{\rm {}}} / m_{{\rm {}}}$".format(axis, axis))
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*ratio_lims)
    plt.savefig(plot_param_path + "mass_massratio_{}.png".format(axis), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["tau_{}".format(axis)] * 1000, df["tau_{}_err".format(axis)]/df["tau_{}".format(axis)], c="black", s=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$\tau_{{\rm {}}}$ (ms)".format(axis))
    ax.set_ylabel(r"$\Delta\tau_{{\rm {}}} / \tau_{{\rm {}}}$".format(axis, axis))
    ax.set_xlim(*tau_lims)
    ax.set_ylim(*ratio_lims)
    plt.savefig(plot_param_path + "tau_tauratio_{}.png".format(axis), dpi=dpi)
    plt.close()

