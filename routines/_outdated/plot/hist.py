import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def main():
    # df = pd.read_csv("H:/icaps/data/LDM_ROT2/of_all.csv")
    # df = df.dropna()
    #
    # mass = df["mass_tx"].dropna().to_numpy()
    # mass_err = df["mass_tx_err"].to_numpy()
    # idx = np.argsort(mass)
    # mass = mass[idx]/icaps.const.m_p
    # mass_err = mass_err[idx]/icaps.const.m_p
    # idx = np.where(mass_err < 1/4)
    # mass = mass[idx]
    # mass_err = mass_err[idx]
    #
    # _, ax = plt.subplots()
    # ax.errorbar(mass, np.cumsum(mass)/np.sum(mass), xerr=mass_err, fmt=".", color="black", markersize=5)
    # ax.set_xlim(np.min(mass), np.max(mass))
    # ax.set_xlabel("N")
    # ax.set_ylabel("cumulative normalized N")
    #
    # plt.show()

    df = pd.read_csv("H:/icaps/data/LDM_ROT2/BM_Fits_cut.csv")
    df = df.dropna()
    mass = df["m_x_log"].dropna().to_numpy()
    idx = np.argsort(mass)
    mass = mass[idx]
    _, ax = plt.subplots()
    ax.errorbar(mass, np.cumsum(mass) / np.sum(mass), fmt=".", color="black", markersize=5)
    ax.set_xlim(np.min(mass), np.max(mass))
    ax.set_xlabel("N")
    ax.set_ylabel("cumulative normalized N")

    plt.show()


if __name__ == '__main__':
    main()

