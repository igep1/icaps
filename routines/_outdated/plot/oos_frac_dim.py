import icaps
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "XZ"
project_path = drive_letter + ":/icaps/data/OOS_" + plane + "/wacca/"

# start = 1560
# stop = 2200
# start = 2580
# stop = 3270
# start = 3630
# stop = 4325
start = 4700
stop = 5375
step = 1
interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)

scale = 11.9
alpha = 0.002

wacca_path = project_path + interval_string + "/"
csv_path = wacca_path + "wacca_rot_link_" + interval_string + ".csv"
frac_plots_path = wacca_path + "frac_plots/"
int_plot_path = wacca_path + "frac_dim_int_" + interval_string + ".png"
area_plot_path = wacca_path + "frac_dim_area_" + interval_string + ".png"


def main():
    df = pd.read_csv(csv_path)
    intensity = df["mass"].to_numpy()
    area = df["area"].to_numpy()
    gyrad = df["gyrad"].to_numpy()
    idx = np.where(gyrad > 0)
    gyrad = gyrad[idx]
    area = area[idx]
    intensity = intensity[idx]
    gyrad = gyrad * scale
    area = area * scale**2

    popt, pcov = curve_fit(icaps.lin_fit, np.log10(gyrad), np.log10(intensity))
    perr = np.sqrt(np.diag(pcov))
    vals = np.linspace(np.min(gyrad), np.max(gyrad), 1000)

    _, ax = plt.subplots()
    ax.scatter(gyrad, intensity, color="black", s=1, alpha=alpha)
    ax.plot(vals, 10**icaps.lin_fit(np.log10(vals), popt[0], popt[1]), color="red",
            label="log(I) = D * log(R) + const.\n"
                  + "D = " + str(np.around(popt[0], 3)) + " \u00b1 " + str(np.around(perr[0], 3)))
    ax.grid()
    ax.set_axisbelow(True)
    ax.legend()
    ax.set_xlim(left=3, right=350)
    ax.set_ylim(bottom=20, top=10**6)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("Radius of Gyration in µm")
    ax.set_ylabel("Corrected Intensity")

    plt.savefig(int_plot_path)

    popt, pcov = curve_fit(icaps.lin_fit, np.log10(gyrad), np.log10(area))
    perr = np.sqrt(np.diag(pcov))

    _, ax = plt.subplots()
    ax.scatter(gyrad, area, color="black", s=1, alpha=alpha)
    ax.plot(vals, 10**icaps.lin_fit(np.log10(vals), popt[0], popt[1]), color="red",
            label="log(A) = D * log(R) + const.\n"
                  + "D = " + str(np.around(popt[0], 3)) + " \u00b1 " + str(np.around(perr[0], 3)))
    ax.grid()
    ax.set_axisbelow(True)
    ax.legend()
    ax.set_xlim(left=3, right=350)
    ax.set_ylim(bottom=20, top=10 ** 6)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel("Radius of Gyration in µm")
    ax.set_ylabel("Area in µm²")

    plt.savefig(area_plot_path)


if __name__ == '__main__':
    main()
