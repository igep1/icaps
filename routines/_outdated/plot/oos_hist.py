import icaps
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
oos_snippet = "01560_02200_001"
# oos_snippet = "04700_05375_001"
oos_plane = "XZ"
project_path = drive_letter + ":/icaps/data/OOS_{}/wacca/{}/".format(oos_plane, oos_snippet)
table_path = project_path + "wacca_rot_link_{}.csv".format(oos_snippet)
frame = 1700
# frame = 5000
BIN_MODES = ("log", "const")
bin_mode = "const"


def fit_pow(x, a):
    return x**a


def main():
    df = pd.read_csv(table_path)
    df = df[df["frame"] == frame]
    mass = df["mass"].to_numpy()
    min_mass = np.min(mass)
    max_mass = np.max(mass)
    if bin_mode == "log":
        bins = np.array([10**i for i in range(int(np.floor(np.log10(min_mass))), int(np.floor(np.log10(max_mass))) + 2)])
        l = bins.shape[0]
        bins = np.tile(bins, 9)
        bins = np.sort(bins)
        digits = np.tile(np.arange(1, 10, 1), l)
        bins = bins * digits
        x = bins[:-1]
    else:
        spacing = 100
        bins = np.arange(0, 10**int(np.floor(np.log10(max_mass)) + 1) + spacing, spacing)
        x = bins[:-1]
        x = bins + 0.5*spacing
    hist, _ = np.histogram(mass, bins=bins)
    xlim = (np.min(x), np.max(x))
    ylim = (0.9, np.max(hist))
    _, ax = plt.subplots()
    ax.scatter(bins[:-1], hist, color="black", s=3)
    ax.set_title("Frame: {}".format(frame))
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_ylabel("Frequency")
    ax.set_xlabel("Intensity")

    # idx = np.where(bins == 2000)[0][0]
    # print(bins[:idx], hist[:idx])
    # popt, pcov = curve_fit(fit_pow, bins[:idx], hist[:idx])
    # perr = np.sqrt(np.diag(pcov))
    # xspace = np.linspace(np.min(bins[:idx]), np.max(bins[:idx]), 1000)
    # ax.plot(xspace, fit_pow(xspace, *popt), color="red", linestyle="--", linewidth=1,
    #         label="y = x^a\na = {} \u00b1 {}".format(pcov[0], perr[0]))
    # ax.legend()

    plt.show()


if __name__ == '__main__':
    main()

