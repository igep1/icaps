import icaps
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


# drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
drive_letter = "C"
# project_path = drive_letter + ":/icaps/texus56_SRE_20191115/OOS/YZ/"
plane = "YZ"
project_path = drive_letter + ":/Users/ben_s/Desktop/OOS/" + plane + "/"
# project_path = drive_letter + ":/Users/ben_s/Desktop/OOS/XZ/"
if plane == "YZ":
    in_path = project_path + "000000000_10h_11m_11s_462ms/"
else:
    in_path = project_path + "000000000_10h_11m_09s_936ms/"
thr_path = project_path + "thr/"
start = 160
stop = 8800
specs = "(" + str(start) + ":" + str(stop) + ").bmp"
out_path = project_path
fps = 50
scale = 10  # µm per px


if __name__ == '__main__':
    # df = pd.read_csv(out_path + "particles/00160.csv")
    # x = df["lum"].sort_values()
    # x_cum = icaps.cumulate(x, norm=True)
    # x_med = icaps.dist_median(x, x_cum, approx="linear")
    # print(x_med)
    # icaps.plot_scatter(x, x_cum)
    # plt.scatter([x_med], [0.5])
    # plt.show()

    # icaps.mk_movie(out_path + "plots/lum_area_norm/", out_path + "lum_area_norm.mp4", fps=30)
    # icaps.mk_movie(out_path + "plots/lum_area/", out_path + "lum_area.mp4", fps=30)
    scans = [
        (1176, 1542),
        (2226, 2577),
        (3277, 3630),
        (4326, 4699),
        (5376, 5776),
        (6426, 6839),
        (7477, 8858)
    ]
    # scans = None
    """
    df_med = pd.read_csv(out_path + "median.csv")
    df_max = pd.read_csv(out_path + "max.csv")
    df_med = icaps.df_drop_intervals(df_med, scans + [(0*fps, 30*fps), (110*fps, 200*fps)], "frame")
    df_max = icaps.df_drop_intervals(df_max, scans + [(0*fps, 30*fps), (110*fps, 200*fps)], "frame")
    
    icaps.plot_scatter(df_med["frame"] / fps, [df_med["area"], df_max["area"]],
                       axlabels=("Time in Seconds", "Area"),
                       scale=("linear", "log"),
                       xlim=("min", "max"), ylim=("min", "max"),
                       xbuff=(0.03, 0.03), ybuff=(0.0005, 0.5),
                       color=["orangered", "mediumblue"],
                       label=["Median", "Maximum"],
                       legend=True
                       )
    icaps.plot_fit(df_med["frame"]/fps, df_med["area"], icaps.exp_fit,
                   color="black", linestyle="--", scale="linear", guesses=[100, 50],
                   label="Median Fit", func_label="y = a * exp(t[s]/b)", arg_labels=["a", "b"],
                   decimals=1, separate_legend=False)
    icaps.plot_fit(df_max["frame"] / fps, df_max["area"], icaps.exp_fit,
                   color="black", linestyle="-.", scale="linear", guesses=[100, 50],
                   label="Maximum Fit", func_label="y = a * exp(t[s]/b)", arg_labels=["a", "b"],
                   decimals=1, separate_legend=False)
    plt.savefig("C:/Users/ben_s/Desktop/Area_Fit.png")

    icaps.plot_scatter(df_med["frame"] / fps, [df_med["lum"], df_max["lum"]],
                       axlabels=("Time in Seconds", "Luminance"),
                       scale=("linear", "log"),
                       xlim=("min", "max"), ylim=("min", "max"),
                       xbuff=(0.03, 0.03), ybuff=(0.0005, 0.5),
                       color=["orangered", "mediumblue"],
                       label=["Median", "Maximum"],
                       legend=True
                       )
    icaps.plot_fit(df_med["frame"]/fps, df_med["lum"], icaps.exp_fit,
                   color="black", linestyle="--", scale="linear", guesses=[10000, 20],
                   label="Median Fit", func_label="y = a * exp(t[s]/b)", arg_labels=["a", "b"],
                   decimals=1, separate_legend=False)
    icaps.plot_fit(df_max["frame"] / fps, df_max["lum"], icaps.exp_fit,
                   color="black", linestyle="-.", scale="linear", guesses=[10000, 20],
                   label="Maximum Fit", func_label="y = a * exp(t[s]/b)", arg_labels=["a", "b"],
                   decimals=1, separate_legend=False)
    plt.savefig("C:/Users/ben_s/Desktop/Lum_Fit.png")
    """

    particle_path = out_path + "particles/"
    plot_path = out_path + "plots/"
    files, particle_path = icaps.get_files(particle_path + "*.csv", ret_path=True)
    df_tot = pd.DataFrame(columns=["time", "lum_tot", "area_tot"])
    pb = icaps.ProgressBar(len(files), "Plotting")
    for i, file in enumerate(files):
        df = pd.read_csv(particle_path + file)
        df_tot = df_tot.append(pd.DataFrame(data={"time": [icaps.get_frame(file, prefix=None)],
                                                  "lum_tot": [np.sum(df["lum"])],
                                                  "area_tot": [np.sum(df["area"])]
                                                  }))
        pb.tick()
    pb.finish()
    df_tot = icaps.df_drop_intervals(df_tot, scans + [(0 * fps, 30 * fps), (110 * fps, 200 * fps)], "time")
    df_tot["time"] = df_tot["time"]/fps
    df_tot["area_tot"] = df_tot["area_tot"]
    icaps.plot_scatter_twinx(df_tot["time"], df_tot["lum_tot"], df_tot["area_tot"],
                             axlabels=("Time in seconds", "Total Luminance", "Total Area"),
                             colorize_axlabels=True, color1="orangered", color2="mediumblue")
    # icaps.plot_scatter(df_tot["time"], [df_tot["lum_tot"], df_tot["area_tot"]],
    #                    label=["Total Luminance", "Total Area"],
    #                    color=["orangered", "mediumblue"], legend=True)
    plt.savefig(plot_path + "total_twinx.png")












