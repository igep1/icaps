import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import os

project_path = "D:/icaps/data/OOS_XZ/wacca/"
step = 1
fps = 50

folders = os.listdir(project_path)
folders = folders[:-1]
csv_paths = []
for folder in folders:
    csv_paths.append(project_path + folder + "/" + "wacca_" + folder + ".csv")
total_csv_path = project_path + "wacca_xz.csv"

# df = None
# for csv_path in csv_paths:
#     if df is None:
#         df = pd.read_csv(csv_path)
#     else:
#         df = df.append(pd.read_csv(csv_path))
# df.to_csv(total_csv_path, index=False)

markers = ["o", "x", "^", "v", "s"]

df = pd.read_csv(total_csv_path)

_, ax = plt.subplots()
frames = np.unique(df["frame"].to_numpy())
median_int = np.zeros(len(frames))
max_int = np.zeros(len(frames))
median_area = np.zeros(len(frames))
max_area = np.zeros(len(frames))
pb = icaps.ProgressBar(len(frames), "Calculating Median and Max")
for f, frame in enumerate(frames):
    df_ = df[df["frame"] == frame]
    median_int[f] = icaps.cumulative_median(df_["mass"])
    median_area[f] = icaps.cumulative_median(df_["area"])
    max_int[f] = np.amax(df_["mass"])
    max_area[f] = np.amax(df_["area"])
    pb.tick()
pb.finish()
ax.scatter(frames / fps, max_int, color="black", s=1, label="Median/Max Intensity")
ax.scatter(frames/fps, median_int, color="black", s=1)
ax.scatter(frames/fps, max_area, color="gray", s=1, label="Median/Max Area")
ax.scatter(frames/fps, median_area, color="gray", s=1)
ax.grid()
ax.set_axisbelow(True)
ax.legend()
ax.set_xlabel("Time in Seconds")
ax.set_ylabel("Intensity or Area")
ax.set_yscale("log")
ax.set_ylim(bottom=30)
plt.show()


pb = icaps.ProgressBar(len(csv_paths), "Plotting Gyrads")
gmin = np.amin(df[df["gyrad"] > 0]["gyrad"])
gmax = np.amax(df["gyrad"])
imin = np.amin(df["mass"])
imax = np.amax(df["mass"])
for i, csv_path in enumerate(csv_paths):
    _, ax = plt.subplots()
    df_ = pd.read_csv(csv_path)
    frames = np.unique(df_["frame"].to_numpy())
    n = np.amax(frames) - np.amin(frames)
    ax.scatter(df_["gyrad"], df_["mass"], label=str(np.amin(frames)) + " to " + str(np.amax(frames)), color="black", s=1)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(gmin, gmax)
    ax.set_ylim(imin, imax)
    ax.grid()
    ax.legend(loc="upper left")
    ax.set_axisbelow(True)
    plt.savefig("C:/Users/ben_s/Desktop/Gyrad_" + str(np.amin(frames)) + ".png")
    plt.close()
    pb.tick()
pb.finish()


_, ax = plt.subplots()
ax.scatter(df["gyrad"], df["mass"], color="black", s=1)
ax.set_yscale("log")
ax.set_xscale("log")
ax.grid()
ax.set_axisbelow(True)
ax.set_ylabel("Intensity")
ax.set_xlabel("Radius of Gyration in px")
ax.set_xlim(left=0.2)
plt.savefig("C:/Users/ben_s/Desktop/Gyrad.png")


_, ax = plt.subplots()
ax.scatter(df["area"], df["mass"], color="black", s=1)
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel("Area")
ax.set_ylabel("Intensity")
ax.grid()
ax.set_axisbelow(True)
plt.savefig("C:/Users/ben_s/Desktop/IntArea.png")

_, ax = plt.subplots()
int_dens = df["mass"]/df["area"]
bins = np.arange(0, 257, 1)
hist, bins = np.histogram(int_dens, bins)
ax.scatter(bins[:-1], hist, color="black", s=1)
ax.set_xlim(right=150)
# ax.set_xscale("log")
# ax.set_yscale("log")
ax.set_xlabel("Intensity/Area (binned to gray values)")
ax.set_ylabel("Frequency")
ax.grid()
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig("C:/Users/ben_s/Desktop/IntAreaDensity.png")


frames = (1560, 5375)

_, ax = plt.subplots()
pb = icaps.ProgressBar(len(csv_paths), "Plotting Mean Distributions")
for i, csv_path in enumerate(csv_paths):
    df = pd.read_csv(csv_path)
    frames = np.unique(df["frame"].to_numpy())
    n = np.amax(frames) - np.amin(frames)
    mass = np.sort(df["mass"].to_numpy())
    cum_mass = icaps.cumulate(mass, sort=True, norm=False)
    ax.scatter(mass, cum_mass/n, label=str(np.amin(frames)) + " to " + str(np.amax(frames)), marker=markers[i], s=1)
    pb.tick()
pb.finish()
ax.set_xscale("log")
ax.grid()
ax.legend()
ax.set_axisbelow(True)
ax.set_ylabel("Cumulative Intensity (mean per frame)")
ax.set_xlabel("Intensity")
plt.tight_layout()
plt.savefig("C:/Users/ben_s/Desktop/IntDist.png")

_, ax = plt.subplots()
pb = icaps.ProgressBar(len(csv_paths), "Plotting Normalized Distributions")
for i, csv_path in enumerate(csv_paths):
    df = pd.read_csv(csv_path)
    frames = np.unique(df["frame"].to_numpy())
    mass = np.sort(df["mass"].to_numpy())
    cum_mass = icaps.cumulate(mass, sort=True, norm=True)
    ax.scatter(mass, cum_mass, label=str(np.amin(frames)) + " to " + str(np.amax(frames)), marker=markers[i], s=1)
    pb.tick()
pb.finish()
ax.set_xscale("log")
ax.grid()
ax.legend()
ax.set_axisbelow(True)
ax.set_ylabel("Normalized Cumulative Intensity")
ax.set_xlabel("Intensity")
plt.tight_layout()
plt.savefig("C:/Users/ben_s/Desktop/IntDistNorm.png")



# df = None
# for csv_path in csv_paths:
#     if df is None:
#         df = pd.read_csv(csv_path)
#     else:
#         df = df.append(pd.read_csv(csv_path))
# df.to_csv(project_path + "wacca.csv", index=False)
# print("!")
# df = pd.read_csv(project_path + "wacca_xz.csv")
# df1 = pd.read_csv(csv_path1)
# df2 = pd.read_csv(csv_path2)
# df = df1.append(df2)

# _, ax = plt.subplots()
# x = df[df["frame"] == frames[1]]["gyrad"].to_numpy()
# y = df[df["frame"] == frames[1]]["mass"].to_numpy()
# y = y[x != 0]
# x = x[x != 0]
# ax.scatter(x, y, s=1, marker="o", color="black")
# popt, pcov = curve_fit(icaps.lin_fit, np.log10(x), np.log10(y))
# perr = np.sqrt(np.diag(pcov))
# xspace = np.array([0.9*np.amin(x), 1.1*np.amax(x)])
# ax.plot(xspace, 10**icaps.lin_fit(np.log10(xspace), *popt), color="red",
#         label="log(y) = a * log(x) + b\n" +
#               "a = " + str(np.around(popt[0], 3)) + " \u00b1 " + str(np.around(perr[0], 3)))
# ax.set_title(str(frames[1]))
# ax.legend()
# ax.set_yscale("log")
# ax.set_xscale("log")
# ax.set_xlim(left=0.9*np.amin(x))
# ax.set_xlabel("Radius of Gyration in px")
# ax.set_ylabel("Intensity")
# ax.grid()
# ax.set_axisbelow(True)
# plt.show()

# _, ax = plt.subplots()
# colors = ["black", "gray"]
# for f, frame in enumerate(frames):
#     df_ = df[df["frame"] == frame]
#     x = np.sort(df_["mass"].to_numpy())
#     y = icaps.cumulate(x, sort=True, norm=True)
#     ax.scatter(x, y, s=1, marker="o", color=colors[f], label=str(frame))
# # ax.set_yscale("log")
# ax.set_xscale("log")
# ax.grid()
# ax.set_axisbelow(True)
# # ax.set_xlim(0.8, 550000)
# # ax.set_ylim()
# ax.legend()
# plt.show()


# _, ax = plt.subplots()
# df_ = df[df["frame"] == frames[0]]
# x = np.sort(df_["mass"].to_numpy())
# y = icaps.cumulate(x, sort=True, norm=True)
# m = icaps.cumulative_median(x)
# ax.scatter(x, y, s=1, marker="o", color="black", label="Intensity\n" + "Median = " + str(m))
# x = np.sort(df_["area"].to_numpy())
# y = icaps.cumulate(x, sort=True, norm=True)
# m = icaps.cumulative_median(x)
# ax.scatter(x, y, s=1, marker="o", color="gray", label="Area\n" + "Median = " + str(m))
# # ax.set_yscale("log")
# ax.set_xscale("log")
# ax.grid()
# ax.set_axisbelow(True)
# ax.set_xlim(left=0.9)
# # ax.set_xlim(0.8, 550000)
# # ax.set_ylim()
# ax.legend()
# plt.show()

# x = np.unique(df["frame"])
# median_int = np.array([icaps.cumulative_median(df[df["frame"] == x_]["mass"].to_numpy()) for x_ in x])
# median_area = np.array([icaps.cumulative_median(df[df["frame"] == x_]["area"].to_numpy()) for x_ in x])
# max_int = np.array([np.amax(df[df["frame"] == x_]["mass"].to_numpy()) for x_ in x])
# max_area = np.array([np.amax(df[df["frame"] == x_]["area"].to_numpy()) for x_ in x])
#
# _, ax = plt.subplots()
# ax.scatter(x/50, median_int, marker="o", s=1, color="black", label="Median/Max Intensity")
# ax.scatter(x/50, median_area, marker="o", s=1, color="gray", label="Median/Max Area")
# ax.scatter(x/50, max_int, marker="o", s=1, color="black")
# ax.scatter(x/50, max_area, marker="o", s=1, color="gray")
# ax.set_yscale("log")
# ax.legend()
# # ax.set_ylim(10**3, 10**5)
# ax.set_xlabel("Time in Seconds")
# ax.set_ylabel("Intensity or Area")
# ax.grid()
# ax.set_axisbelow(True)
# plt.show()









