import icaps
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_XZ/"
wacca_path_fv = project_path + "wacca_fullvolume/"
wacca_path_av = project_path + "wacca/"

files_fv = icaps.get_files(wacca_path_fv)
files_av = icaps.get_files(wacca_path_av)

_, ax = plt.subplots()
colors = ["C0", "C1", "C2", "C3"]
m = 4
norm = True
pb = icaps.ProgressBar(m, "Plotting")
for i in range(m):
    df_fv = pd.read_csv(wacca_path_fv + files_fv[i] + "/" + icaps.get_files(wacca_path_fv + files_fv[i] + "/*.csv")[0])
    df_av = pd.read_csv(wacca_path_av + files_av[i] + "/" + icaps.get_files(wacca_path_av + files_av[i] + "/*.csv")[0])
    frames = np.unique(df_fv["frame"].to_numpy())
    n = np.amax(frames) - np.amin(frames)
    int_fv = np.sort(df_fv["mass"].to_numpy())
    int_av = np.sort(df_av["mass"].to_numpy())
    # ax.scatter(int_fv, icaps.cumulate(int_fv), s=1, label="Full Volume", color=colors[i])
    # ax.scatter(int_av, icaps.cumulate(int_av), s=1, label="Active Volume", color=colors[i+1])
    ax.plot(int_fv, icaps.cumulate(int_fv, norm=True) if norm else icaps.cumulate(int_fv)/n, linestyle="--",
            color=colors[i], linewidth=1, label="Full Volume" if i == 0 else None)
    ax.plot(int_av, icaps.cumulate(int_av, norm=True) if norm else icaps.cumulate(int_av)/n, linestyle="-",
            color=colors[i], linewidth=1, label="Active Volume" if i == 0 else None)
    pb.tick()
pb.finish()
ax.set_xscale("log")
ax.grid()
ax.set_axisbelow(True)
ax.set_xlabel("Intensity")
ax.set_ylabel("Cumulative Intensity")
ax.legend()
ax.set_xlim(right=1000)
ax.set_ylim(bottom=-0.001, top=0.2)
plt.tight_layout()
plt.savefig("C:/Users/ben_s/Desktop/VolumeCompare.png")



