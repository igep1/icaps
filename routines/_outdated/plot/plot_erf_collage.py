import icaps
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit


particle = 720240
# mode = "trans"
# axis = "x"
mode = "rot"
axis = "z"
dts = 1, 5, 10, 20, 35, 50
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT2/particles/{}/".format(particle)
out_path = project_path + "erf_{}_{}_collage.png".format(mode, axis)
csv_path = drive_letter + ":/icaps/data/LDM_ROT2/disp_{}.csv".format(mode)


def main():
    df = pd.read_csv(csv_path)
    df = df[df["particle"] == particle]

    _, ax = plt.subplots(ncols=2, nrows=3, constrained_layout=True)
    ax = ax.flatten()
    mode_unit = "°" if mode == "rot" else "µm"
    dt_max = np.max(dts)
    key_max = "w{}_{}".format(axis, dt_max) if mode == "rot" else "v{}_{}".format(axis, dt_max)
    dx_max = df[key_max].to_numpy()
    dx_max = dx_max[~np.isnan(dx_max)]
    xlim_abs = np.max([abs(np.min(dx_max)), abs(np.max(dx_max))])
    for i, dt in enumerate(dts):
        key = "w{}_{}".format(axis, dt) if mode == "rot" else "v{}_{}".format(axis, dt)
        dx = df[key].to_numpy()
        dx = dx[~np.isnan(dx)]
        xmax_abs = np.max([abs(np.min(dx)), abs(np.max(dx))])
        dx = np.sort(dx)
        ff = np.linspace(1/len(dx), 1, len(dx))
        dx_space = np.linspace(np.min(dx), np.max(dx))
        popt, pcov = curve_fit(icaps.fit_erf, dx, ff, p0=(10, 0), maxfev=1000)
        ax[i].scatter(dx, ff, s=1, c="black")
        ax[i].plot(dx_space, icaps.fit_erf(dx_space, *popt), color="red", lw=1,
                   label="Drift ({}): {:.2f}{}, Sigma: {:.2f}{}".format(mode, popt[1], mode_unit, popt[0], mode_unit))
        # ax[i].legend(loc="upper left")
        ax[i].set_title("Drift: {:.2f} {}, Sigma: {:.2f} {}".format(popt[1], mode_unit, popt[0], mode_unit),
                        fontsize=10)
        ax[i].set_xlim(-1.05*xmax_abs, 1.05*xmax_abs)
    plt.savefig(out_path)


if __name__ == '__main__':
    main()


