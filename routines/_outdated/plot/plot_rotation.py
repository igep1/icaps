import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/RotTest/"
start = 4700
stop = 5375
step = 1
interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)
data_path = project_path + "wacca_ellipse_" + interval_string + ".csv"


def main():
    df = pd.read_csv(data_path)
    # df = df[df["frame"] == 5375]
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    # dfx = df[df["semi_major_axis"]/df["semi_minor_axis"] < 10**3]
    # dfx = dfx[dfx["semi_major_axis"]/dfx["semi_minor_axis"] > 10**1]
    # print(dfx)
    intensity = df["mass"].to_numpy()
    semi_major_axis = df["semi_major_axis"].to_numpy()
    semi_minor_axis = df["semi_minor_axis"].to_numpy()
    diff = semi_major_axis / semi_minor_axis
    # diff = diff[df["area"] > 4]
    # intensity = intensity[df["area"] > 4]
    intensity = intensity[diff < 10]
    diff = diff[diff < 10]
    mass = intensity/7
    # mass = intensity
    bins = np.arange(0, int(np.amax(mass))+1)
    bins = np.array([i * 10**j for j in range(0, 7) for i in range(1, 10, 1)])
    # print(bins)
    means = np.zeros(bins.size - 1)
    stds = np.zeros(bins.size - 1)
    for i, bin in enumerate(bins[:-1]):
        diff_ = diff[mass >= bin]
        diff_ = diff_[mass[mass >= bin] < bins[i+1]]
        means[i] = np.mean(diff_)
        stds[i] = np.std(diff_)
    # print(means)

    _, ax = plt.subplots()
    # ax.errorbar(bins[1:], means, yerr=stds, fmt="none", color="gray", alpha=0.5)
    ax.scatter(bins[1:], means, s=5, color="black")
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Semi Major Axis/Semi Minor Axis")
    ax.set_xlabel("Mass in Units of Monomer Mass")
    ax.set_xscale("log")
    # ax.set_ylim(top=4.5, bottom=0.8)
    # ax.set_xlim(left=9, right=2*10**4)
    # ax.set_yscale("log")
    # ax.set_ylim(bottom=0.95, top=100)
    plt.show()


if __name__ == '__main__':
    main()
