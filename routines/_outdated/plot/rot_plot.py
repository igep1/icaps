import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import trackpy as tp
from scipy.optimize import curve_fit

project_path = "D:/icaps/data/RotTest/"


def main():
    df = pd.read_csv(project_path + "wacca_linked_04700_05375_001.csv")
    particles = df["particle"].to_numpy()
    part_id = np.unique(particles)
    track_length = np.zeros(part_id.size)
    # pb = icaps.ProgressBar(part_id.size, "Analysing Track Lengths")
    # for i, pid in enumerate(part_id):
    #     track_length[i] = np.argwhere(particles == pid).shape[0]
    #     pb.tick()
    # pb.finish()
    # max_id = np.argwhere(track_length == np.amax(track_length)).squeeze()
    # df_ = tp.filter_stubs(df, threshold=400)
    df_ = df[df["particle"] == 916]
    # tp.plot_traj(df_, label=True)

    angle_thr = 50 + 20
    kink_thr = 5 + 50
    angle = df_["ellipse_angle"].to_numpy()
    angle_kinks = np.where(np.abs(np.gradient(angle)) > angle_thr)[0]
    kink_dist = np.diff(angle_kinks)
    kink_delete = np.where(kink_dist < kink_thr)[0]
    angle_kinks = np.delete(angle_kinks, kink_delete)
    kink_dist = np.diff(angle_kinks)
    periods = kink_dist/50

    x = df_["frame"].to_numpy()/50
    _, ax = icaps.plot_scatter(x, angle, marker_size=1, color="black",
                               axlabels=["Time in Seconds", "Ellipse Angle in Degree"])
    for i in range(angle_kinks.shape[0]):
        x_kinks = x[angle_kinks[i]]
        ax.axvline(x_kinks, color="mediumblue", linestyle="--")
        if i < angle_kinks.shape[0] - 1:
            ax.text(x_kinks+0.01, 175, str(periods[i]), color="mediumblue")
    plt.show()
    plt.close()

    for angle_kink in angle_kinks:
        angle[angle_kink:] += 180
    _, ax = icaps.plot_scatter(x, angle, marker_size=1, color="black",
                               axlabels=["Time in Seconds", "Ellipse Angle in Degree"])
    del_idx = np.where(np.isnan(angle))
    angle = np.delete(angle, del_idx)
    x = np.delete(x, del_idx)
    popt, pcov = curve_fit(icaps.lin_fit, x, angle)
    _, ax, popt, pcov, perr = icaps.plot_fit(x, angle, icaps.lin_fit, linestyle="-.", color="red", linewidth=1,
                                             legend=True, decimals=0,
                                             legend_kwargs=dict(loc="lower right", framealpha=1))
    ax.set_title("Period = " + str(np.round(1/(popt[0]/360), decimals=2)) + " s", fontsize=10)
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()


