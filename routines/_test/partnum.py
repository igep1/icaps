import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
csv_all = project_path + "Tracks_all.csv"

df = pd.read_csv(csv_all)
print(icaps.comma(len(df["particle"].unique())))
# df = df[df["area"] > 1]
# print(len(df["particle"].unique()))
df = icaps.filter_track_length(df, 2)
print(icaps.comma(len(df["particle"].unique())))


