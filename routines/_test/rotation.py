import icaps
import numpy as np
import pandas as pd
import cv2
import os

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM/"
ffc_path = project_path + "ffc/"
contour_path = project_path + "contours_short/"
particle_path = project_path + "particles/"
movie_path = project_path + "movies/"
gif_path = project_path + "gifs/"
temp_path = project_path + "temp/"
track_prelim_path = project_path + "TrackRot_LengthPhaseRatio20L100.csv"
tracK_final_path = project_path + "final.csv"
track_candidates_path = project_path + "track_candidates.csv"
ellipses_path = project_path + "ellipses.csv"


def test():
    df = pd.read_csv(track_prelim_path)
    frames = np.unique(df["frame"].to_numpy())

    df_ = df[df["frame"] == frames[0]]
    img = icaps.load_img(ffc_path + icaps.generate_file(frames[0]))
    contours = np.load(contour_path + icaps.generate_file(frames[0], frmt="npy"), allow_pickle=True)
    labels = df_["label"].to_numpy()

    label = labels[0]
    stats = df_[df_["label"] == label]
    bbox = stats[["bx", "by", "bw", "bh"]].to_numpy()[0]
    part_img = icaps.crop(img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True, copy_=True)
    part_img_large = icaps.enlarge_pixels(part_img, 8)
    part_img_large = cv2.cvtColor(part_img_large, cv2.COLOR_GRAY2BGR)
    part_img_large = cv2.drawContours(part_img_large, contours*8, contourIdx=label, color=(255, 0, 0))
    ellipse_box = cv2.fitEllipse(contours[label])
    ellipse_box = (np.array(ellipse_box[0])*8, np.array(ellipse_box[1])*8, np.array(ellipse_box[2]))
    part_img_large = icaps.draw_ellipse(part_img_large, ellipse_box, color=(0, 0, 255))
    icaps.cvshow(part_img_large)


def main():
    df = pd.read_csv(track_prelim_path)

    icaps.mk_folder(temp_path)

    candidates = pd.read_csv(project_path + "candidates.csv")
    candidates = candidates[candidates["quality"] == "!"]
    candidates = candidates["particle"].to_numpy()
    df = df[np.in1d(df["particle"], candidates)]
    df = df.drop(columns=["ellipse_x", "ellipse_y", "semi_minor_axis", "semi_major_axis", "ellipse_angle"])
    df.to_csv(track_candidates_path, index=False)

    particles = np.unique(df["particle"].to_numpy())
    print(particles)
    shape_dict = dict()
    for i, particle in enumerate(particles):
        df_ = df[df["particle"] == particle]
        dx1_max = np.amax(df_["x"].to_numpy() - df_["bx"].to_numpy())
        dy1_max = np.amax(df_["y"].to_numpy() - df_["by"].to_numpy())
        dx2_max = np.amax(df_["bx"].to_numpy() + df_["bw"].to_numpy() - df_["x"].to_numpy())
        dy2_max = np.amax(df_["by"].to_numpy() + df_["bh"].to_numpy() - df_["y"].to_numpy())
        w = np.ceil(2 * np.amax([dx1_max, dx2_max])).astype(np.uint64)
        h = np.ceil(2 * np.amax([dy1_max, dy2_max])).astype(np.uint64)
        shape_dict[particle] = (h, w)

    frames = np.unique(df["frame"].to_numpy())
    pb = icaps.ProgressBar(len(frames), "Progress")
    for frame in frames:
        df_ = df[df["frame"] == frame]
        df_ellipse = pd.DataFrame(columns=["frame", "label", "ellipse_x", "ellipse_y", "semi_minor_axis",
                                           "semi_major_axis", "ellipse_angle"])
        img = icaps.load_img(ffc_path + icaps.generate_file(frame))
        contours = np.load(contour_path + icaps.generate_file(frame, frmt="npy"), allow_pickle=True)
        labels = df_["label"].to_numpy()
        particles = df_["particle"].to_numpy()
        for i, particle in enumerate(particles):
            label = labels[i]
            stats = df_[df_["label"] == label]
            bbox = stats[["bx", "by", "bw", "bh"]].to_numpy()[0]
            if bbox[0] == 0 or bbox[1] == 0 or bbox[2] == 1024 or bbox[3] == 1024:
                continue
            part_img = icaps.crop(img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True, copy_=True)
            contour = contours[label]
            mask = np.zeros(part_img.shape, np.uint8)
            mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
            mask = icaps.fill_holes(mask)
            part_img[mask == 0] = 0

            if particle in (453584, 554944):
                part_img = icaps.threshold(part_img, thresh=30, replace=(0, None))
                contour = cv2.findContours(part_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0][0]

            h, w = shape_dict[particle]
            canvas = np.zeros((h, w), dtype=np.uint8)
            x = int(np.round(w / 2 - (stats["x"].to_numpy()[0] - bbox[0])))
            y = int(np.round(h / 2 - (stats["y"].to_numpy()[0] - bbox[1])))
            canvas[y:y+bbox[3], x:x+bbox[2]] = part_img
            canvas = icaps.enlarge_pixels(canvas, 8)
            canvas = cv2.cvtColor(canvas, cv2.COLOR_GRAY2BGR)
            try:
                ellipse_box = cv2.fitEllipse(contour)
            except cv2.error:
                continue
            df_ellipse = df_ellipse.append(pd.DataFrame(data={"frame": [frame],
                                                              "label": [label],
                                                              "ellipse_x": [ellipse_box[0][0]],
                                                              "ellipse_y": [ellipse_box[0][1]],
                                                              "semi_minor_axis": [ellipse_box[1][0]],
                                                              "semi_major_axis": [ellipse_box[1][1]],
                                                              "ellipse_angle": [ellipse_box[2]]}), sort=False)
            contour = np.array([np.array([x, y]) + c for c in contour])
            canvas = cv2.drawContours(canvas, [contour * 8], contourIdx=0, color=(255, 0, 0))
            ellipse_box_large = ((np.array([x, y]) + np.array(ellipse_box[0])) * 8,
                                 np.array(ellipse_box[1]) * 8,
                                 np.array(ellipse_box[2]))
            canvas = icaps.draw_ellipse(canvas, ellipse_box_large, color=(0, 0, 255))
            icaps.mk_folder(particle_path + "{:0>6}".format(particle) + "/")
            icaps.save_img(canvas, particle_path + "{:0>6}".format(particle) + "/" + "{:0>6}".format(particle) +
                           "_{:0>6}".format(frame) + ".bmp")
        df_ellipse.to_csv(temp_path + "{:0>5}".format(frame) + ".csv", index=False)
        pb.tick()
    pb.finish()
    icaps.merge_dataframes(temp_path, ellipses_path, delete_source=False, silent=False)

    df_ellipses = pd.read_csv(ellipses_path)
    df = pd.merge(left=df, right=df_ellipses, on=["frame", "label"], how="left")
    df.to_csv(tracK_final_path, index=False)


def mk_movies():
    # icaps.mk_folder(movie_path)
    icaps.mk_folder(gif_path)
    files = os.listdir(particle_path)
    length = 100
    pb = icaps.ProgressBar(len(files))
    for file in files:
        if os.path.isdir(particle_path + file):
            # icaps.mk_movie(particle_path + file + "/", movie_path + file + ".mp4", fps=30)
            nframes = len(os.listdir(particle_path + file + "/"))
            icaps.mk_gif(particle_path + file + "/[::{}].bmp".format(np.max((1, int(np.round(nframes/length))))),
                         gif_path + file + ".gif", loop=-1, duration=0.2)
        pb.tick()
    pb.finish()


if __name__ == '__main__':
    main()
    mk_movies()

