import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import icaps

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT_PhaseCorr/"
particle_path = project_path + "particles/"
test_path = particle_path + "test/"


def generate_test_data():
    size = 200
    img = np.zeros((size, size), dtype=np.uint8)
    semi_major_axis = 80
    semi_minor_axis = 20
    orig_path = test_path + "orig/"
    icaps.mk_folders([test_path, orig_path])
    for angle in np.arange(0, 360):
        img_ = img.copy()
        cv2.ellipse(img_, (int(size / 2), int(size / 2)), (semi_major_axis, semi_minor_axis), angle=angle,
                    startAngle=0, endAngle=360, color=255, thickness=-1)
        icaps.save_img(img_, orig_path + "test_{:0>4}.bmp".format(angle))


def generate_seeded_test_data():
    orig_path = test_path + "orig/"
    files = icaps.get_files(orig_path)
    img = icaps.load_img(orig_path + files[0])
    h, w = img.shape[:2]
    cx, cy = w // 2, h // 2
    for angle in np.arange(1, 360):
        rot_mat = cv2.getRotationMatrix2D((cx, cy), angle, 1.0)
        res = cv2.warpAffine(img, rot_mat, (h, w), flags=cv2.INTER_LINEAR)
        icaps.save_img(res, orig_path + "test{:0>4}.png".format(angle))


def main():
    # generate_test_data()
    # generate_seeded_test_data()

    analyse = False
    if analyse:
        orig_path = test_path + "orig/"
        fft_path = test_path + "fft/"
        icaps.mk_folder(fft_path)
        files = icaps.get_files(orig_path)
        img0 = icaps.load_img(orig_path + files[0], flags=0)
        img0 = img0.astype(np.float32) / 255.
        h, w = img0.shape
        cx, cy = w//2, h//2
        fft0 = np.fft.fft2(img0)
        fshift0 = np.fft.fftshift(fft0)
        # mag0 = np.log(np.abs(fshift0)+1)
        mag0 = 20 * np.log(np.abs(fshift0))
        lap0 = cv2.Laplacian(mag0, cv2.CV_64F)
        log_polar0 = cv2.logPolar(mag0, (cx, cy), min(h, w), 0)
        lin_polar0 = cv2.linearPolar(mag0, (cx, cy), min(h, w), 0)

        _, ax = plt.subplots(ncols=2, nrows=3, constrained_layout=True)
        ax[0, 0].imshow(img0)
        ax[0, 0].set_title("image")
        ax[0, 1].imshow(np.real(fft0))
        ax[0, 1].set_title("real fft")
        ax[1, 0].imshow(mag0)
        ax[1, 0].set_title("magnitude")
        ax[1, 1].imshow(lap0)
        ax[1, 1].set_title("laplacian")
        ax[2, 0].imshow(log_polar0)
        ax[2, 0].set_title("log polar 0.00")
        ax[2, 1].imshow(lin_polar0)
        ax[2, 1].set_title("lin polar 0.00")
        plt.savefig(fft_path + ".".join(files[0].split(".")[:-1]) + ".png")
        plt.close()

        # files = files[29:31]
        pb = icaps.ProgressBar(len(files) - 1)
        df = pd.DataFrame(data={"frame": np.arange(0, len(files)),
                                "disp_log": np.zeros(len(files)),
                                "disp_lin": np.zeros(len(files))})
        for prev_idx, file in enumerate(files[1:]):
            img1 = icaps.load_img(orig_path + file, flags=0)
            img1 = img1.astype(np.float32) / 255.
            fft1 = np.fft.fft2(img1)
            fshift1 = np.fft.fftshift(fft1)
            # mag1 = np.log(np.abs(fshift1)+1)
            mag1 = 20 * np.log(np.abs(fshift1))
            lap1 = cv2.Laplacian(mag1, cv2.CV_64F)
            log_polar1 = cv2.logPolar(mag1, (cx, cy), min(h, w), 0)
            lin_polar1 = cv2.linearPolar(mag1, (cx, cy), min(h, w), 0)

            _, ax = plt.subplots(ncols=2, nrows=3, constrained_layout=True)
            ax[0, 0].imshow(img1)
            ax[0, 0].set_title("image")
            ax[0, 1].imshow(np.real(fft1))
            ax[0, 1].set_title("real fft")
            ax[1, 0].imshow(mag1)
            ax[1, 0].set_title("magnitude")
            ax[1, 1].imshow(lap1)
            ax[1, 1].set_title("laplacian")
            (sx, sy), sf = cv2.phaseCorrelate(log_polar0, log_polar1)
            df.loc[prev_idx + 1, "disp_log"] = -sy / h * 360
            ax[2, 0].imshow(log_polar1)
            ax[2, 0].set_title("log polar {:.2f}".format(np.round(-sy / h * 360, 4)))
            (sx, sy), sf = cv2.phaseCorrelate(lin_polar0, lin_polar1)
            df.loc[prev_idx + 1, "disp_lin"] = -sy / h * 360
            ax[2, 1].imshow(lin_polar1)
            ax[2, 1].set_title("lin polar {:.2f}".format(np.round(-sy / h * 360, 4)))
            # log_polar0 = log_polar1
            # lin_polar0 = lin_polar1

            plt.savefig(fft_path + ".".join(file.split(".")[:-1]) + ".png")
            plt.close()
            pb.tick()
        pb.finish()
        df.to_csv(test_path + "disp.csv", index=False)

    icaps.mk_movie(test_path + "fft/", test_path + "movie.mp4", fps=10)

    df = pd.read_csv(test_path + "disp.csv")
    frames = df["frame"].to_numpy()
    disp_log = df["disp_log"].to_numpy()
    disp_lin = df["disp_lin"].to_numpy()
    _, ax = plt.subplots()
    ax.scatter(frames, disp_log, s=3, c="black")
    ax.set_xlabel("Frame")
    ax.set_ylabel("Angle (°)")
    ax.set_title("Logarithmic Polar")
    # ax.set_ylim(-180, 180)
    plt.savefig(test_path + "logpolar.png")
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(frames, disp_lin, s=3, c="black")
    ax.set_xlabel("Frame")
    ax.set_ylabel("Angle (°)")
    ax.set_title("Linear Polar")
    plt.savefig(test_path + "linpolar.png")
    plt.close()


if __name__ == '__main__':
    main()

