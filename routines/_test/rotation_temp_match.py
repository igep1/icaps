import icaps
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from shutil import copyfile

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT_TempMatch/"
particle_path = project_path + "particles/"
test_path = particle_path + "test2/"

icaps.mk_folders([test_path])


def rotate(img, angle, center=(None, None)):
    h, w = img.shape[:2]
    cx, cy = center
    if cx is None:
        cx = w//2
    if cy is None:
        cy = h//2
    rot_mat = cv2.getRotationMatrix2D((cx, cy), -angle, 1.0)
    res = cv2.warpAffine(img, rot_mat, (w, h), flags=cv2.INTER_LINEAR)
    return res


def main():
    # ldm2_particle_path = drive_letter + ":/icaps/data/LDM_ROT2/particles/"
    # files = icaps.get_files(ldm2_particle_path)
    # pb = icaps.ProgressBar(len(files))
    # for file in files:
    #     ffc_path = particle_path + file + "/ffc/"
    #     ffc2_path = ldm2_particle_path + file + "/ffc/"
    #     icaps.mk_folder(ffc_path)
    #     img_files = icaps.get_files(ffc2_path)
    #     for img_file in img_files:
    #         copyfile(ffc2_path + img_file, ffc_path + img_file)
    #     pb.tick()
    # pb.finish()

    df_pcf = pd.read_csv(project_path + "Tracks_PCF.csv")
    particle = 430753
    df_pcf_ = df_pcf[df_pcf["particle"] == particle]
    out_path = particle_path + "{}/".format(particle)
    ffc_path = out_path + "ffc/"
    plot_path = out_path + "plots/"
    icaps.mk_folder(plot_path)
    frames = np.unique(df_pcf_["frame"].to_numpy())
    frames = frames[frames - np.min(frames) < 600]
    frames = frames[frames - np.min(frames) > 140]
    spread = 45
    resolution = 0.1
    include0 = True
    d0 = np.arange(-resolution, -(spread + resolution), -resolution)[::-1]
    d0 = np.append(d0, np.arange(0, spread + resolution, resolution))
    if not include0:
        d0 = d0[d0 != 0]
    print(d0)
    thresh = 20
    df = pd.DataFrame(data={"frame": frames,
                            "disp": np.zeros(len(frames))})
    pb = icaps.ProgressBar((len(frames)-1) * len(d0))
    prev_img = icaps.load_img(ffc_path + "{}_{:0>6}.bmp".format(particle, frames[0]))
    prev_img = icaps.threshold(prev_img, thresh, replace=(0, None))
    for i, frame in enumerate(frames):
        if i == 0:
            continue
        img = icaps.load_img(ffc_path + "{}_{:0>6}.bmp".format(particle, frame))
        img = icaps.threshold(img, thresh, replace=(0, None))
        q = np.zeros(len(d0))
        for j, d0_ in enumerate(d0):
            prev_img_r = rotate(prev_img, d0_)
            res = cv2.matchTemplate(img, prev_img_r, cv2.TM_CCOEFF)
            q[j] = res[0, 0]
            pb.tick()
        q_max = np.max(q)
        d_max = d0[q == q_max][0]
        df.loc[i, "disp"] = d_max
        prev_img = img
    pb.finish()
    df.to_csv(out_path + "disp.csv", index=False)

    df = pd.read_csv(out_path + "disp.csv")
    _, ax = plt.subplots()
    ax.scatter(df["frame"] - np.min(df["frame"]), df["disp"], s=3, c="black")
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("Angular Displacement (°)")
    # plt.savefig(plot_path + "disp.png")
    # plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["frame"] - np.min(df["frame"]), np.cumsum(df["disp"]), s=3, c="black")
    ax.set_xlabel("Time (ms)")
    ax.set_ylabel("Angle (°)")
    # ax.set_ylim(-180, 180)
    # plt.savefig(plot_path + "angle.png")
    # plt.close()
    plt.show()

    """
    orig_path = test_path + "orig/"

    analyse = False
    if analyse:
        files = icaps.get_files(orig_path)
        df = pd.DataFrame(data={"frame": np.arange(0, len(files)),
                                "disp": np.zeros(len(files)),
                                "quality": np.zeros(len(files))})

        d0 = np.arange(-45, 45 + 1, 1)
        prev_img = icaps.load_img(orig_path + files[0])
        pb = icaps.ProgressBar((len(files) - 1) * len(d0))
        for prev_idx, file in enumerate(files[1:]):
            img = icaps.load_img(orig_path + file)
            q = np.zeros(len(d0))
            for i, d0_ in enumerate(d0):
                prev_img_r = rotate(prev_img, d0_)
                res = cv2.matchTemplate(img, prev_img_r, cv2.TM_CCOEFF)
                # res_n = icaps.temp_match.norm_match_res(res)
                q[i] = res[0, 0]
                pb.tick()
            q_max = np.max(q)
            d_max = d0[q == q_max]
            df.loc[prev_idx + 1, "frame"] = prev_idx + 1
            df.loc[prev_idx + 1, "disp"] = d_max
            df.loc[prev_idx + 1, "quality"] = q_max

            prev_img = img
        pb.finish()
        df.to_csv(test_path + "disp.csv", index=False)

    df = pd.read_csv(test_path + "disp.csv")
    _, ax = plt.subplots()
    ax.scatter(df["frame"], df["disp"], s=3, c="black")
    ax.set_xlabel("Frame")
    ax.set_ylabel("Angular Displacement (°)")
    # ax.set_ylim(-180, 180)
    plt.savefig(test_path + "disp.png")
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df["frame"], np.cumsum(df["disp"]), s=3, c="black")
    ax.set_xlabel("Frame")
    ax.set_ylabel("Angle (°)")
    # ax.set_ylim(-180, 180)
    plt.savefig(test_path + "angle.png")
    plt.close()
    """


if __name__ == '__main__':
    main()


