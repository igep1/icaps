import icaps
import numpy as np
import cv2
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/RotTest/"
in_project_path = drive_letter + ":/icaps/data/OOS_YZ/"
# img_path = in_project_path + "orig_strip/"
# start = 1560
# stop = 2200
start = 2580
stop = 3270
# start = 3630
# stop = 4325
# start = 4700
# stop = 5375
step = 1
interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)
part_data_path = in_project_path + "wacca/" + interval_string + "/"
csv_path = part_data_path + "wacca_" + interval_string + ".csv"
contour_path = part_data_path + "contours/"
out_contour_path = project_path + "contour_imgs/"
temp_path = project_path + "temp/"

icaps.mk_folder(temp_path)


def main():
    df = pd.read_csv(csv_path)
    frames = np.unique(df["frame"].to_numpy())
    pb = icaps.ProgressBar(np.amax(frames) - np.amin(frames) + 1, "Analysing Rotation")
    for frame in frames:
        df_ellipse = pd.DataFrame(columns=["frame", "label", "ellipse_x", "ellipse_y", "semi_minor_axis",
                                           "semi_major_axis", "ellipse_angle"])
        # img = icaps.load_img(img_path + "{:0>5}".format(frame) + ".bmp")
        df_ = df[df["frame"] == frame].to_dict(orient="records")
        contours = np.load(contour_path + "{:0>5}".format(frame) + ".npy", allow_pickle=True)
        for label in range(len(df_)):
            contour = contours[label]
            stats = df_[label]
            # part_img = img[stats["by"]: stats["by"] + stats["bh"], stats["bx"]: stats["bx"] + stats["bw"]]
            # show_img = icaps.enlarge_pixels(part_img, 8)
            # show_img = cv2.drawContours(show_img, [(contour + np.array([1, 1]))*8], contourIdx=0, color=255)
            #
            # cimg = cv2.copyMakeBorder(icaps.threshold(part_img, 13, replace=(0, 255)),
            #                           top=1, bottom=1, left=1, right=1, value=0,
            #                           borderType=cv2.BORDER_CONSTANT)
            # cnts = cv2.findContours(cimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            # contour = cnts[0][0]
            # show_img = icaps.enlarge_pixels(part_img, 8)
            # show_img = cv2.drawContours(show_img, [contour * 8], contourIdx=0, color=255)
            #
            # icaps.save_img(show_img, out_contour_path + "{:0>5}".format(frame) + "_" + "{:0>4}".format(label)
            #                + ".bmp")
            try:
                box = cv2.fitEllipse(contour)
            except cv2.error:
                box = [(np.nan, np.nan), (np.nan, np.nan), np.nan]
            df_ellipse = df_ellipse.append(pd.DataFrame(data={"frame": [frame],
                                                              "label": [label],
                                                              "ellipse_x": [box[0][0]],
                                                              "ellipse_y": [box[0][1]],
                                                              "semi_minor_axis": [box[1][0]],
                                                              "semi_major_axis": [box[1][1]],
                                                              "ellipse_angle": [box[2]]}), sort=False)
        df_ellipse.to_csv(temp_path + "{:0>5}".format(frame) + ".csv", index=False)
        pb.tick()
    pb.finish()
    icaps.oos.merge_dataframes(temp_path, project_path + "ellipses_" + interval_string + ".csv", delete_source=True)
    df_ellipses = pd.read_csv(project_path + "ellipses_" + interval_string + ".csv")
    df = pd.merge(left=df, right=df_ellipses, on=["frame", "label"], how="left")
    df.to_csv(part_data_path + "wacca_rot_" + interval_string + ".csv", index=False)


if __name__ == '__main__':
    main()


