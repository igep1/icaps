import pandas as pd
from PIL import ImageTk, Image
import tkinter as tk
import icaps

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
project_path = drive_letter + ":/icaps/data/LDM_Strange/"
# particle_path = project_path + "particles_rot/"
particle_path = project_path + "particles/"
csv_focus = "focus.csv"
in_folder = "ffc/"
out_path = project_path + "focus_filter.csv"

files = icaps.get_files(particle_path)
try:
    df_out = pd.read_csv(out_path)
except FileNotFoundError:
    particles = [int(float(file)) for file in files]
    df_out = pd.DataFrame(data={"particle": particles, "min_focus": 0.})
particles = df_out.loc[df_out["min_focus"] == 0., "particle"].to_numpy()


class FocusSelector(tk.Tk):
    def __init__(self):
        super(FocusSelector, self).__init__()
        self.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.increment = 0.1
        self.max_thr = 200.

        self.top_frame = tk.Frame(self, width=400, height=400)
        self.top_frame.pack()
        self.bottom_frame = tk.Frame(self, width=400)
        self.bottom_frame.pack()

        self.canvas = tk.Canvas(self.top_frame, width=400, height=400)
        self.canvas.pack()

        self.min_focus = tk.DoubleVar()
        self.spinbox = tk.Spinbox(self.bottom_frame, from_=0., to=self.max_thr, increment=self.increment,
                                  textvariable=self.min_focus, command=self.spinbox_changed)
        self.spinbox.grid(row=1, column=0)
        self.slider = tk.Scale(self.bottom_frame, orient=tk.HORIZONTAL, from_=0., to=self.max_thr,
                               resolution=self.increment, variable=self.min_focus, command=self.slider_moved)
        self.slider.grid(row=2, column=0)

        self.prev_button = tk.Button(self.bottom_frame, text="prev", command=self.prev_pressed, width=10, height=5)
        self.prev_button.grid(row=1, column=1, rowspan=2, sticky=tk.E)
        self.next_button = tk.Button(self.bottom_frame, text="next", command=self.next_pressed, width=10, height=5)
        self.next_button.grid(row=1, column=2, rowspan=2, sticky=tk.E)

        self.particle = tk.IntVar(0)
        part_label = tk.Label(self.bottom_frame, textvariable=self.particle)
        part_label.grid(row=0, column=0)

        self.track_length = tk.IntVar(0)
        track_label = tk.Label(self.bottom_frame, textvariable=self.track_length)
        track_label.grid(row=0, column=1)

        self.idx = 0
        self.part_path = None
        self.ffc_path = None
        self.img = None
        self.df_focus = None
        self.load_particle()

        self.bind("<Key>", self.key_pressed)

    def key_pressed(self, event):
        if event.keysym == "Up":
            if self.min_focus.get() < self.max_thr:
                self.min_focus.set(round(self.min_focus.get() + self.increment, 1))
            self.load_image()
        elif event.keysym == "Down":
            if self.min_focus.get() > 0:
                self.min_focus.set(round(self.min_focus.get() - self.increment, 1))
            self.load_image()
        elif event.keysym == "Left":
            self.prev_pressed()
        elif event.keysym == "Right":
            self.next_pressed()

    def prev_pressed(self):
        if self.idx > 0:
            self.save_df()
            self.idx -= 1
        self.load_particle()

    def next_pressed(self):
        if self.idx < len(particles) - 1:
            self.save_df()
            self.idx += 1
        self.load_particle()

    def on_closing(self):
        self.save_df()
        self.destroy()

    def save_df(self):
        df_out.loc[df_out["particle"] == self.particle.get(), "min_focus"] = self.min_focus.get()

    def slider_moved(self, _):
        self.load_image()

    def spinbox_changed(self):
        self.load_image()

    def load_particle(self):
        self.particle.set(particles[self.idx])
        self.part_path = particle_path + "{}/".format(self.particle.get())
        self.ffc_path = self.part_path + in_folder
        min_focus = df_out.loc[df_out["particle"] == self.particle.get(), "min_focus"].to_numpy()[0]
        self.min_focus.set(min_focus)
        self.df_focus = pd.read_csv(self.part_path + csv_focus)
        self.max_thr = self.df_focus["focus"].max()
        self.load_image()

    def load_image(self):
        df_focus = self.df_focus.sort_values(by="focus", ascending=True)
        df_focus = df_focus[df_focus["focus"] >= self.min_focus.get()]
        frame = df_focus["frame"].to_numpy()
        self.track_length.set(len(frame))
        if len(frame) > 0:
            frame = frame[0]
            img_path = self.ffc_path + "{}_{:0>6}.bmp".format(self.particle.get(), frame)
            img = Image.open(img_path)
            img = img.resize((img.width * 5, img.height * 5), resample=Image.BOX)
            self.img = ImageTk.PhotoImage(img)
            self.canvas.create_image(img.width, img.height, image=self.img, anchor=tk.SE)
        else:
            self.canvas.delete("all")


if len(particles) > 0:
    master = FocusSelector()
    master.mainloop()

df_out.to_csv(out_path, index=False)


