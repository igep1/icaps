import icaps
import pandas as pd
import numpy as np


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM/"
plot_path = project_path + "plots/"
ff_path = project_path + "ff_216700_260300.npy"
icaps.mk_folders([plot_path])

csv_rot_good = project_path + "rot_good.csv"
csv_phase = project_path + "Phases_Hand.csv"
csv_track_all = project_path + "Tracks_P.csv"
csv_track = project_path + "Tracks_PEL100.csv"
csv_track_rot = project_path + "Tracks_Rot.csv"
csv_trans_disp = project_path + "DISP_Trans.csv"
csv_trans_erf = project_path + "ERF_Trans.csv"
csv_trans_erf_flagged = project_path + "ERF_Trans_flagged.csv"
csv_trans_of = project_path + "OF_Trans.csv"
csv_rot_disp = project_path + "DISP_Rot.csv"
csv_rot_erf = project_path + "ERF_Rot.csv"
csv_rot_erf_flagged = project_path + "ERF_Rot_flagged.csv"
csv_rot_of = project_path + "OF_Rot.csv"
csv_final = project_path + "OF_Final.csv"

dts = np.arange(1, 11, 1)
dts = np.append(dts, np.arange(11, 26, 2))
dts = np.append(dts, np.arange(30, 51, 5))


def main():
    # ============================================================
    # ROT | DISPLACEMENT
    # ============================================================
    # plot = True
    # df = pd.read_csv(csv_track_rot)
    # particles_rot_good = pd.read_csv(csv_rot_good)["particle"].unique()
    # print(f"Initial Good Rot Particles:\t\t{len(particles_rot_good)}")
    # df = df[df["particle"].isin(particles_rot_good)]
    # df = df[df["q84_grad"] >= 15]
    # df = icaps.filter_track_length(df, 100)
    # print(f"Filtered Good Rot Particles:\t{len(df['particle'].unique())}")
    # df = icaps.brownian.stitch_displacements(df, dts, in_columns=["ellipse_angle"],
    #                                          out_columns="omega", silent=False,
    #                                          abs_max=np.deg2rad(90), convert=np.pi / 180, add_leaps=True)
    # df.to_csv(csv_rot_disp, index=False)
    # if plot:
    #     df = pd.read_csv(csv_rot_disp)
    #     icaps.brownian.plot_stitched_displacements(df, dts, "ellipse_angle", "omega", silent=False, sub_folder=None,
    #                                                plot_path=plot_path + "disp_rot/", clear_out=False,
    #                                                pos_convert=np.pi / 180, pos_unit="rad", pos_name=r"$\theta$",
    #                                                disp_name=r"$\Delta\theta$", leap_column="leap_{}", dpi=600)

    # ============================================================
    # ROT | ERF FITS
    # ============================================================
    # plot_perc = 1.0
    # df = pd.read_csv(csv_rot_disp)
    # df = icaps.brownian.calc_erf(
    #     df, dts, ["rot"], silent=False, fit_type=2, n_min=20, disp_columns="omega",
    #     plot_path=plot_path + "erf_rot/" if plot_perc > 0 else None, sub_folder=None, clear_out=True, dpi=200,
    #     p0=[0.0, 1.0], disp_names=r"$\Delta \theta$", double_gauss=True, fickian=False, erf_exp=True,
    #     plot_fails=False, var_lr=True, disp_convert=1, disp_convert_plot=1, disp_unit="rad", erf_convert=1,
    #     plot_until=plot_perc, plot_empty=False)
    # df.to_csv(csv_rot_erf, index=False)

    # ============================================================
    # ROT | CALC & PLOT OF FITS
    # ============================================================
    plot_perc = 1.0
    sequential = False
    adapt_to = 1
    flag = True
    semi_auto = False
    read_upper_bound = not semi_auto
    if flag:
        df_erf = pd.read_csv(csv_rot_erf)
    else:
        df_erf = pd.read_csv(csv_rot_erf_flagged)
    if read_upper_bound:
        df_erf_old = pd.read_csv(project_path + "_backup/ERF_Rot_flagged_upperbounds.csv")
        df_erf["upper_bound"] = df_erf_old["upper_bound"]
    df, df2 = icaps.brownian.calc_ornstein_fuerth(
        df_erf, dts, axes=["rot"], mode="rot",
        temp_path=project_path + "OF_Rot_temp.csv", silent=False, ret_erf=True,
        plot={"path": plot_path + "of_rot/" if plot_perc > 0 else None,
              "attempts": True,
              "error": True,
              "dpi": 400,
              "clear_out": True,
              "crop_to_max": True,
              "min_rss": None,
              "plot_until": plot_perc,
              "dots": "SG"},
        fit={"sequential": sequential,
             "p0": None,
             "maxit": None,
             "fit_type": 2,
             "fit_err": True,
             "bounds": (1, None),
             "ballistic_bounds": (1, 6),
             "ballistic_fit": "OF",
             "n_min": 5,
             "max_sigsqr": np.pi ** 2 / 4,
             "strict_max": False,
             "max_err_ratio": 1.,
             "min_tau": None,
             "semi_auto": semi_auto,
             "read_upper_bound": read_upper_bound,
             "adapt_margins": (adapt_to, 0),
             "adapt_quantity": "count",
             "adapt_grade_weights": (1., 1.),
             "adapt_grade_type": "gaussian",
             "chisqr_log": False,
             "chisqr_norm": False,
             "rss_log": True,
             "rss_norm": True,
             "corrcoef_log": False,
             "corrcoef_norm": False
             },
        sig_mode="DIFF")
    df.to_csv(csv_rot_of, index=False)
    if flag:
        df2.to_csv(csv_rot_erf_flagged, index=False)

    # ============================================================
    # TRANS | DISPLACEMENT
    # ============================================================
    # plot = False
    # df = pd.read_csv(csv_track)
    # df = icaps.brownian.stitch_displacements(df, dts, silent=False)
    # df.to_csv(csv_trans_disp, index=False)
    # if plot:
    #     df = pd.read_csv(csv_trans_disp)
    #     icaps.brownian.plot_stitched_displacements(df, dts, "x", "dx", silent=False, sub_folder=None,
    #                                                plot_path=plot_path + "disp_trans_x/", clear_out=False)
    #     icaps.brownian.plot_stitched_displacements(df, dts, "y", "dy", silent=False, sub_folder=None,
    #                                                plot_path=plot_path + "disp_trans_y/", clear_out=False)

    # ============================================================
    # TRANS | ERF FITS
    # ============================================================
    # plot_perc = 1
    # df = pd.read_csv(csv_trans_disp)
    # df = icaps.brownian.calc_erf(
    #     df, dts, ["x", "y"], silent=False, fit_type=2, n_min=20,
    #     plot_path=plot_path + "erf_trans/" if plot_perc > 0 else None, sub_folder=None, clear_out=True, dpi=200,
    #     p0=[0.0, 0.001], disp_names=r"$\Delta {}$", double_gauss=True, fickian=False, erf_exp=True,
    #     plot_fails=False, var_lr=True, plot_until=plot_perc, plot_empty=False, maxit=1000)
    # df.to_csv(csv_trans_erf, index=False)

    # ============================================================
    # TRANS | CALC & PLOT OF FITS
    # ============================================================
    # plot_perc = 1.0
    # sequential = False
    # adapt_to = 3
    # flag = True
    # if flag:
    #     df_erf = pd.read_csv(csv_trans_erf)
    # else:
    #     df_erf = pd.read_csv(csv_trans_erf_flagged)
    # df, df2 = icaps.brownian.calc_ornstein_fuerth(
    #     df_erf, dts, axes=["x", "y"], mode="trans",
    #     temp_path=project_path + "OF_Trans_temp.csv", silent=False, ret_erf=True,
    #     plot={"path": plot_path + "of_trans_{}/" if plot_perc > 0 else None,
    #           "attempts": True,
    #           "error": True,
    #           "dpi": 400,
    #           "clear_out": True,
    #           "crop_to_max": True,
    #           "min_rss": 5e-3,
    #           "plot_until": plot_perc,
    #           "dots": "SG"},
    #     fit={"sequential": sequential,
    #          "p0": None,
    #          "maxit": None,
    #          "fit_type": 2,
    #          "fit_err": True,
    #          "bounds": (1, None),
    #          "ballistic_bounds": (1, 6),
    #          "ballistic_fit": "OF",
    #          "n_min": 5,
    #          "max_sigsqr": 1e-6,
    #          "max_err_ratio": 1.,
    #          "min_tau": None,
    #          "adapt_margins": (adapt_to, 0),
    #          "adapt_quantity": "count",
    #          "adapt_grade_weights": (1., 1.),
    #          "adapt_grade_type": "gaussian",
    #          "chisqr_log": False,
    #          "chisqr_norm": False,
    #          "rss_log": True,
    #          "rss_norm": True,
    #          "corrcoef_log": False,
    #          "corrcoef_norm": False
    #          },
    #     sig_mode="DIFF")
    # df.to_csv(csv_trans_of, index=False)
    # if flag:
    #     df2.to_csv(csv_trans_erf_flagged, index=False)

    # ============================================================
    # FINALIZING SG DATA
    # ============================================================
    df_trans = pd.read_csv(csv_trans_of)
    df_rot = pd.read_csv(csv_rot_of)
    particles_rot = np.unique(df_trans["particle"].to_numpy())
    particles_trans = np.unique(df_trans["particle"].to_numpy())
    particles = np.unique(np.append(particles_trans, particles_rot))
    particles_diff = particles_rot[~np.isin(particles_rot, particles_trans)]
    df_rot = df_rot[df_rot["particle"].isin(particles_rot)]

    df_rot = icaps.flatten_axis(df_rot, leave=["particle", "inertia", "inertia_err"])
    df_trans = icaps.flatten_axis(df_trans)
    df_trans = pd.concat([df_trans, pd.DataFrame(data={"particle": particles_diff})])
    df_track = pd.read_csv(csv_track)
    df_track = df_track[df_track["particle"].isin(particles)]

    df_ex = icaps.compound_track_data(df_track, particles=particles, silent=False)
    df = pd.merge(df_trans, df_ex, how="left", on="particle")
    df = pd.merge(df, df_rot, how="left", on="particle")
    df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
    df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
    df["tau"] = np.mean([df["tau_x"], df["tau_y"]], axis=0)
    df["tau_err"] = 1 / 2 * np.sqrt(df["tau_x_err"] ** 2 + df["tau_y_err"] ** 2)
    df.to_csv(csv_final, index=False)


if __name__ == '__main__':
    main()


