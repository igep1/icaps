import icaps
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":icaps/data/LDM_BM/"
plot_path = project_path + "plots/"
part_img_path = plot_path + "parts_rot/"
particle_path = project_path + "particles_rot/"
movie_path = plot_path + "movies/"
icaps.mk_folders([project_path, plot_path, particle_path, part_img_path, movie_path])

txt_rot_good_old = project_path + "rot_good_old.txt"
txt_rot_candidates = project_path + "rot_candidates.txt"
txt_rot_good = project_path + "rot_good.txt"
csv_phase = project_path + "Phases_Hand.csv"
csv_track_all = project_path + "Tracks_P.csv"
csv_track = project_path + "Tracks_PEL100.csv"
csv_trans_disp = project_path + "DISP_Trans.csv"
csv_trans_erf = project_path + "ERF_Trans.csv"
csv_trans_erf_flagged = project_path + "ERF_Trans_flagged.csv"
csv_trans_of = project_path + "OF_Trans.csv"
csv_rot_disp = project_path + "DISP_Rot.csv"
csv_rot_erf = project_path + "ERF_Rot.csv"
csv_rot_of = project_path + "OF_Rot.csv"
csv_final = project_path + "OF_Final.csv"

ff_path = project_path + "ff_216700_260300.npy"
ff = icaps.load_img(ff_path).astype(float)
ff_mean = np.mean(ff)
fit1 = [icaps.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [icaps.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (260300,)

dts = np.arange(1, 11, 1)
dts = np.append(dts, np.arange(11, 26, 2))
dts = np.append(dts, np.arange(30, 51, 5))


def main():
    # ============================================================
    # FILTER PHASE, EDGE AND TRACK LENGTH
    # ============================================================
    # df = pd.read_csv(csv_track_all)
    # df = icaps.filter_disturbance(df, df_phases=pd.read_csv(csv_phases))
    # df = icaps.filter_edge(df, 1)
    # df = icaps.filter_track_length(df, 100)
    # df.to_csv(csv_track, index=False)

    # ============================================================
    # ROT | SEARCH
    # ============================================================
    # df = pd.read_csv(csv_track)
    # particles_r_old = np.loadtxt(txt_rot_good_old, dtype=int)
    # df_r_q84 = df[df["q84_grad"] >= 10].copy()
    # df_r_q84 = df_r_q84[df_r_q84["area"] > 50]
    # df_r_q84 = icaps.filter_track_length(df_r_q84, 100)
    # particles_r_q84 = df_r_q84["particle"].unique()
    # np.savetxt(txt_rot_candidates, particles_r_q84, fmt="%1u")
    # particles_r_new = df_r_q84[~df_r_q84["particle"].isin(particles_r_old)]["particle"].unique()
    # particles_r = particles_r_new
    # print("Old:\t", len(particles_r_old))
    # print("Q84:\t", len(particles_r_q84))
    # print("New:\t", len(particles_r_new))
    # print("Using:\t", len(particles_r))
    # for particle in particles_r:
    #     df_ = df[df["particle"] == particle].copy()
    #     df_ = df_.sort_values(by="q84_grad", ascending=False)
    #     row = df_.head(1)
    #     frame = row["frame"].to_numpy()[0]
    #     bbox = row[["bx", "by", "bw", "bh"]].to_numpy()[0]
    #     img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame=frame) +
    #                          icaps.generate_file(frame), strip=True)
    #     ff_adj = icaps.adjust_ff(ff, fit=icaps.get_fit([fit1, fit2], fit_cutoffs, frame), t=frame, ff_mean=ff_mean)
    #     img = icaps.ffc(img, ff_adj, inv=True, m=255, threshold=0)
    #     img = icaps.crop(img, bbox[:2], bbox[2:], wh=True)
    #     _, ax = plt.subplots()
    #     ax.imshow(img, cmap=plt.get_cmap("binary_r"), vmin=0, vmax=255)
    #     plt.savefig(part_img_path + f"{particle}_{frame}.png")
    #     plt.close()

    # ============================================================
    # ROT | PREPARATION
    # ============================================================
    candidates = np.loadtxt(txt_rot_candidates, dtype=int)
    icaps.mk_folders([movie_path + "frames/{:07}/".format(candidate) for candidate in candidates])
    df = pd.read_csv(csv_track)
    df = df[df["particle"].isin(candidates)]
    ff_corr = True
    truncate_q84 = 10
    thresh = 4
    thresh_ellipse = 20
    cca = True
    fit_ellipse = True
    draw_center = True
    draw_contour = True
    draw_ellipse = True
    draw_ellipse_contour = True
    draw_ellipse_center = False
    draw_ellipse_guide = "both"
    df["x-bx"] = df["x"] - df["bx"]
    df["y-by"] = df["y"] - df["by"]
    df["bx+bw-x"] = df["bw"] - df["x-bx"]
    df["by+bh-y"] = df["bh"] - df["y-by"]
    df["pad_x"] = df[["x-bx", "bx+bw-x"]].max(axis=1)
    df["pad_y"] = df[["y-by", "by+bh-y"]].max(axis=1)
    df.drop(columns=["x-bx", "y-by", "bx+bw-x", "by+bh-y"], inplace=True)
    for candidate in candidates:
        idx = df["particle"] == candidate
        df.loc[idx, "pad_x"] = df.loc[idx, "pad_x"].max()
        df.loc[idx, "pad_y"] = df.loc[idx, "pad_y"].max()
    df["ellipse_x"] = np.nan
    df["ellipse_y"] = np.nan
    df["ellipse_w"] = np.nan
    df["ellipse_h"] = np.nan
    df["ellipse_angle"] = np.nan

    plt.style.use("dark_background")
    pb = icaps.SimpleProgressBar(len(df))
    for ci, candidate in enumerate(candidates[2:]):
        frames_ = df.loc[df["particle"] == candidate, "frame"].unique()
        if truncate_q84 > 0:
            df_ = df[df["particle"] == candidate]
            q84 = df_["q84_grad"].to_numpy()
            q84[q84 <= truncate_q84] = 0
            i0 = len(q84) - len(np.trim_zeros(q84, "f"))
            i1 = len(np.trim_zeros(q84, "b"))
            idx_pd = df_.index.to_numpy()
            idx = np.zeros_like(idx_pd, dtype=bool)
            idx[i0:i1] = True
            frames_2 = df_.loc[idx, "frame"].to_numpy()
            for i in range(len(frames_) - len(frames_2)):
                pb.tick(skip=True)
            frames_ = frames_2
        imgs = []
        pb.set_stage("{}/{} {:07} Loading Frames".format(ci+1, len(candidates), candidate))
        for frame in frames_:
            imgs.append(icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame) +
                        icaps.generate_file(frame), strip=True))
        imgs = np.array(imgs)
        if ff_corr:
            pb.set_stage("{}/{} {:07} Correcting Frames".format(ci+1, len(candidates), candidate))
            for i in range(len(frames_)):
                ff_adj = icaps.adjust_ff(ff, fit=icaps.get_fit([fit1, fit2], fit_cutoffs, frames_[i]),
                                         ff_mean=ff_mean, frame=frames_[i])
                imgs[i] = icaps.ffc(imgs[i], ff_adj, inv=True, m=255, threshold=0)
        pb.set_stage("{}/{} {:07} Plotting Frames".format(ci+1, len(candidates), candidate))
        pb.reset_clock()
        for i, frame in enumerate(frames_):
            idx = (df["frame"] == frame) & (df["particle"] == candidate)
            bbox = df.loc[idx, ["bx", "by", "bw", "bh"]].to_numpy()[0]
            pos = df.loc[idx, ["x", "y"]].to_numpy()[0]
            pad = df.loc[idx, ["pad_x", "pad_y"]].to_numpy()[0]
            q84 = df.loc[idx, "q84_grad"].to_numpy()[0]
            ex = df.loc[idx, "ex"].to_numpy()[0]
            gyrad = df.loc[idx, "gyrad"].to_numpy()[0]
            part_img = imgs[i, bbox[1]:bbox[1] + bbox[3], bbox[0]: bbox[0] + bbox[2]].copy()
            if thresh is not None:
                part_img = icaps.threshold(part_img, thresh=thresh)
            contour = None
            ellipse = None
            contour_ellipse = None
            if cca:
                df_cca, contours = icaps.cca(np.pad(part_img, pad_width=1, mode='constant', constant_values=0),
                                             thresh=thresh, ret_contours=True)
                if len(df_cca) != 0:
                    diff = np.sum(np.abs(df_cca[["x", "y"]].to_numpy() + bbox[:2] - 1 - pos),
                                  axis=1)
                    idx_cca = np.zeros_like(diff, dtype=bool)
                    idx_cca[np.argmin(diff)] = True
                    cont_idx = df_cca[idx_cca].index[0]
                    contour = np.squeeze(contours[cont_idx])
                    contour += df_cca.loc[idx_cca, ["bx", "by"]].to_numpy()[0] - 1 + bbox[:2]
                    if fit_ellipse:
                        if thresh_ellipse == thresh:
                            contour_ellipse = contour
                        else:
                            df_cca, contours = icaps.cca(np.pad(part_img, pad_width=1, mode='constant', constant_values=0),
                                                         thresh=thresh_ellipse, ret_contours=True)
                            if len(df_cca) != 0:
                                diff = np.sum(np.abs(df_cca[["x", "y"]].to_numpy() + bbox[:2] - 1 - pos),
                                              axis=1)
                                idx_cca = np.zeros_like(diff, dtype=bool)
                                idx_cca[np.argmin(diff)] = True
                                cont_idx = df_cca[idx_cca].index[0]
                                contour_ellipse = np.squeeze(contours[cont_idx])
                                contour_ellipse += df_cca.loc[idx_cca, ["bx", "by"]].to_numpy()[0] - 1 + bbox[:2]
                        try:
                            ellipse = cv2.fitEllipse(contour_ellipse)
                            df.loc[idx, "ellipse_x"] = ellipse[0][0]
                            df.loc[idx, "ellipse_y"] = ellipse[0][1]
                            df.loc[idx, "ellipse_w"] = ellipse[1][0]
                            df.loc[idx, "ellipse_h"] = ellipse[1][1]
                            df.loc[idx, "ellipse_angle"] = ellipse[2]
                        except cv2.error:
                            pass
            _, ax = plt.subplots()
            ax.set_aspect("equal", adjustable="box")
            x = np.arange(0, part_img.shape[1] + 1) + bbox[0]
            y = np.arange(0, part_img.shape[0] + 1) + bbox[1]
            ax.pcolor(x, y, part_img, cmap=plt.get_cmap(f"binary{'_r' if ff_corr else ''}"), vmin=0, vmax=255,
                      zorder=0)
            if draw_center:
                ax.scatter(*pos, c="#FF0000", marker="x", s=20)
            ax.set_xlim(pos[0] - pad[0], pos[0] + pad[0])
            ax.set_ylim(pos[1] - pad[1], pos[1] + pad[1])
            ax.set_ylim(ax.get_ylim()[::-1])
            ann = "q84: {:.2f}\nex:  {:.0f}\ngyr: {:.2f}".format(q84, ex, gyrad)
            if draw_contour and contour is not None:
                ax.plot(*np.transpose(np.append(contour, [contour[0]], axis=0)) + 0.5,
                        c="#FF0000", lw=1, ls="--", zorder=10)
            if draw_ellipse_contour and contour_ellipse is not None:
                ax.plot(*np.transpose(np.append(contour_ellipse, [contour_ellipse[0]], axis=0)) + 0.5,
                        c="#FFFF00", lw=1, ls="-.", zorder=10)
            if draw_ellipse and ellipse is not None:
                center = np.array(ellipse[0]) + 0.5
                angle = ellipse[2]
                ax.add_patch(Ellipse(xy=center, width=ellipse[1][0], height=ellipse[1][1], angle=angle,
                                     fc="none", ec="#0000FF", lw=2, ls="-", zorder=11))
                ann += "\nangle: {:.1f}".format(ellipse[2])
                xspace = ax.get_xlim()
                if draw_ellipse_center:
                    ax.scatter(*center, c="blue", s=30, marker="+", zorder=13)
                delta = np.abs(90 - angle)
                sign = 1 if angle >= 90 else -1
                dx0 = np.abs(xspace[0] - center[0])
                dx1 = np.abs(xspace[1] - center[0])
                if draw_ellipse_guide in ("both", "semimajor"):
                    y0 = center[1] - sign * dx0 * np.tan(np.radians(delta))
                    y1 = center[1] + sign * dx1 * np.tan(np.radians(delta))
                    ax.plot(xspace, [y0, y1], c="#0000FF", lw=1, ls="-", zorder=12)
                if draw_ellipse_guide in ("both", "semiminor"):
                    sign *= -1
                    y0 = center[1] - sign * dx0 * np.tan(np.radians(90 - delta))
                    y1 = center[1] + sign * dx1 * np.tan(np.radians(90 - delta))
                    ax.plot(xspace, [y0, y1], c="#0000FF", lw=1, ls="--", zorder=12)

            ax.annotate(ann, (0.835, 0.5),
                        xycoords="figure fraction", annotation_clip=False, bbox={"facecolor": "black"},
                        c="white")
            # plt.show()
            plt.savefig(movie_path + "frames/{:07}/{:07}_{:06}.jpg".format(candidate, candidate, frame), dpi=100)
            plt.close()
            pb.tick()
        if (ci + 1) % 5 == 0:
            df.to_csv(project_path + "Track_Rot_temp.csv", index=False)
    pb.close()
    plt.style.use("default")
    df.to_csv(project_path + "Track_Rot.csv", index=False)


if __name__ == '__main__':
    main()

