import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Collisions/"
coll_path = project_path + "colls/"
csv_track_all = project_path + "Tracks_all.csv"
csv_coll = project_path + "Tracks_coll.csv"


def get_collisions(df):
    df_c = df.loc[df["coll_with"] >= 0, ["frame", "particle", "coll_with", "coll_prod"]]
    df_c.drop_duplicates("frame", inplace=True)
    df_c.rename(columns={"particle": "partner1", "coll_with": "partner2"}, inplace=True)
    return df_c


def gen_coll_path(coll):
    return f"coll_{coll['partner1']:07d}_{coll['partner2']:07d}_{coll['coll_prod']:07d}/"


def main():
    # ==========================================================
    # PREP COLL TABLE
    # df_all = pd.read_csv(csv_track_all)
    # df_coll = None
    # old_files = icaps.get_files(project_path + "colls_old/")
    # for old_file in old_files:
    #     df_1 = pd.read_csv(project_path + f"colls_old/{old_file}/Particle_1.csv")
    #     df_2 = pd.read_csv(project_path + f"colls_old/{old_file}/Particle_2.csv")
    #     id_1 = df_1["particle"].to_numpy()[0]
    #     id_2 = df_2["particle"].to_numpy()[0]
    #     try:
    #         df_prod = pd.read_csv(project_path + f"colls_old/{old_file}/Particle_New.csv")
    #         id_prod = df_prod["particle"].to_numpy()[0]
    #         impact = np.min(df_prod["frame"]) - 1
    #         case = 1
    #     except FileNotFoundError:
    #         idx = int(max(df_2["frame"]) > max(df_1["frame"]))
    #         id_prod = (id_1, id_2)[idx]
    #         impact = np.max((df_1, df_2)[not idx]["frame"])
    #         case = 2
    #     icaps.mk_folder(coll_path + f"coll_{id_prod:07d}_{id_1:07d}_{id_2:07d}/")
    #     df_coll_ = df_all[df_all["particle"] == id_1].copy()
    #     df_coll_[["coll_with", "coll_prod"]] = -1
    #     print(any(df_coll_["frame"] == impact), case, impact, np.min(df_coll_["frame"]), np.max(df_coll_["frame"]))
    #     df_coll_.loc[df_coll_["frame"] == impact, "coll_with"] = id_2
    #     df_coll_.loc[df_coll_["frame"] == impact, "coll_prod"] = id_prod
    #     if df_coll is None:
    #         df_coll = df_coll_
    #     else:
    #         df_coll = df_coll.append(df_coll_)
    #     df_coll_ = df_all[df_all["particle"] == id_2].copy()
    #     df_coll_[["coll_with", "coll_prod"]] = -1
    #     print(any(df_coll_["frame"] == impact), case, impact, np.min(df_coll_["frame"]), np.max(df_coll_["frame"]))
    #     df_coll_.loc[df_coll_["frame"] == impact, "coll_with"] = id_1
    #     df_coll_.loc[df_coll_["frame"] == impact, "coll_prod"] = id_prod
    #     df_coll = df_coll.append(df_coll_)
    #     if id_prod not in (id_1, id_2):
    #         df_coll_ = df_all[df_all["particle"] == id_prod].copy()
    #         df_coll_[["coll_with", "coll_prod"]] = -1
    #         df_coll = df_coll.append(df_coll_)
    # df_coll.to_csv(csv_coll, index=False)

    # ==========================================================
    # MAKE MOVIES
    pad = 10
    enlarge = 3
    frame_dist = 300
    df_coll = pd.read_csv(csv_coll)
    collisions = get_collisions(df_coll)
    for i, coll in collisions.iterrows():
        out_path = coll_path + gen_coll_path(coll)
        icaps.mk_folder(out_path + "mark/")
        particles = coll[["partner1", "partner2", "coll_prod"]].to_numpy()
        colors = {particle: [255 if q == p else 0 for q in range(3)] for p, particle in enumerate(particles)}
        df_coll_ = df_coll[df_coll["particle"].isin(particles)]
        frame_bounds = np.array([np.min(df_coll_["frame"]), np.max(df_coll_["frame"])])
        frame_bounds[0] = np.max([coll["frame"] - frame_dist, frame_bounds[0]])
        frame_bounds[1] = np.min([coll["frame"] + frame_dist, frame_bounds[1]])
        mov_bounds = [np.min(df_coll_["bx"]), np.min(df_coll_["by"]),
                      np.max(df_coll_["bx"] + df_coll_["bw"]),
                      np.max(df_coll_["by"] + df_coll_["bh"])]
        mov_bounds[0] = np.max([0, mov_bounds[0] - pad])
        mov_bounds[1] = np.max([0, mov_bounds[1] - pad])
        mov_bounds[2] = np.min([icaps.const.shape_ldm[1], mov_bounds[2] + pad])
        mov_bounds[3] = np.min([icaps.const.shape_ldm[0], mov_bounds[3] + pad])
        for frame in range(frame_bounds[0], frame_bounds[1] + 1):
            img_path = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame=frame)
            img = icaps.load_img(img_path + icaps.generate_file(frame), strip=True)
            img = img[mov_bounds[1]: mov_bounds[3], mov_bounds[0]: mov_bounds[2]]
            img = icaps.enlarge_pixels(img, enlarge)
            df_ = df_coll_[df_coll_["frame"] == frame]
            for q, row in df_.iterrows():
                bbox = row[["bx", "by", "bw", "bh"]].astype(int).to_numpy()
                bbox[0] -= mov_bounds[0]
                bbox[1] -= mov_bounds[1]
                particle = int(row["particle"])
                img = icaps.draw_labelled_bbox(img, bbox * enlarge, label=particle, color=colors[particle])
            # icaps.save_img(img, out_path + "mark/{}_{}_{}_{}.bmp".format(*particles[::-1], frame))
            icaps.cvshow(img, t=1)

    # ==========================================================
    # PREP ORIENTATION MEASUREMENT


if __name__ == '__main__':
    main()

