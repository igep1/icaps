import icaps
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":icaps/data/LDM_BM/"
plot_path = project_path + "plots/"
contour_plot_path = plot_path + "contour_samples/"
mark_path = plot_path + "marked/"
temp_path = project_path + "temp/"
icaps.mk_folders([project_path, plot_path, contour_plot_path, mark_path, temp_path])

ff_path = project_path + "ff_216700_260300.npy"
fit1 = [icaps.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [icaps.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (260300,)

csv_full_old = project_path + "Tracks_all_old.csv"
csv_phases = project_path + "Phases_Hand.csv"
csv_debug = project_path + "Tracks_debug.csv"

plot_particles = False
mark_step = 100
# mark_step = 0
bbox_pad_factor = 0.3
thresh_old = 3
thresh_new = 4

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def main():
    df_full = pd.read_csv(csv_full_old)
    df_phases = pd.read_csv(csv_phases)
    df_full = icaps.filter_disturbance(df_full, df_phases)

    # df_debug = df_full[df_full["frame"].isin([5000, 20000, 40825, 120000, 300000])]
    # df_debug.to_csv(csv_debug, index=False)
    # df_full = pd.read_csv(csv_debug)

    frames = df_full["frame"].unique()
    df_full.rename(columns={"mass": "ex"}, inplace=True)
    # df_full["area"] = 0
    df_full.drop(columns=["signal"], inplace=True)
    # df_full[["x", "y", "ex", "gyrad"]] = np.nan
    df_full["max_ex"] = 0.
    df_full["sharpness"] = 0.
    df_full["q84_grad"] = 0.
    df_full["max_grad"] = 0.
    ff = icaps.load_img(ff_path).astype(float)
    ff_mean = np.mean(ff)
    print("\t\t\t\t progress\tchunk\tframe")
    pb = icaps.SimpleProgressBar(len(df_full), "Correcting Table", estimate_time=True)
    chunks = icaps.chunkify(frames, 1000)
    mark_count = mark_step
    for ci, frames in enumerate(chunks):
        df = df_full[df_full["frame"].isin(frames)].copy()
        for frame in frames:
            mark_count += 1
            if mark_count >= mark_step:
                mark_count = 0
            pb.set_stage("{:0>3}\t\t{:0>6}".format(ci, frame))
            img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame) +
                                 icaps.generate_file(frame), strip=True)
            ff_adj = icaps.adjust_ff(ff, fit=icaps.get_fit([fit1, fit2], fit_cutoffs, frame),
                                     frame=frame, ff_mean=ff_mean)
            img_old = None
            if plot_particles or ((mark_step >= 0) and (mark_count == 0)):
                img_old = icaps.ff_subtract(img, ff_adj)
            img = icaps.ffc(img, ff_adj, inv=True, m=255, threshold=0)
            mark_img = None
            if (mark_step >= 0) and (mark_count == 0):
                mark_img = np.around(img).astype(np.uint8)
            idx = df["frame"] == frame
            particles = df.loc[idx, "particle"].to_numpy()
            for particle in particles:
                idx_ = idx & (df["particle"] == particle)
                bbox = df.loc[idx_, ["bx", "by", "bw", "bh"]].to_numpy()[0]
                bbox_pad = np.array([1, 1])
                bbox_pad[0] = np.max([bbox_pad[0], np.round(bbox[2] * bbox_pad_factor)])
                bbox_pad[1] = np.max([bbox_pad[1], np.round(bbox[3] * bbox_pad_factor)])
                bbox = np.array([np.max([bbox[0] - bbox_pad[0], 0]),
                                 np.max([bbox[1] - bbox_pad[1], 0]),
                                 np.min([bbox[0] + bbox[2] + bbox_pad[0], img.shape[1]]),
                                 np.min([bbox[1] + bbox[3] + bbox_pad[1], img.shape[0]])])
                bbox[2] -= bbox[0]
                bbox[3] -= bbox[1]
                part_img = img[bbox[1]: bbox[1] + bbox[3], bbox[0]: bbox[0] + bbox[2]].copy()
                df_cca, contours = icaps.cca(part_img, thresh=thresh_new, ret_contours=True)
                diff = np.sum(np.abs(df_cca[["x", "y"]].to_numpy() + bbox[:2] - df.loc[idx_, ["x", "y"]].to_numpy()),
                              axis=1)
                idx_cca = np.zeros_like(diff, dtype=bool)
                idx_cca[np.argmin(diff)] = True
                cont_idx = df_cca[idx_cca].index[0]
                bbox_cca = df_cca.loc[idx_cca, ["bx", "by", "bw", "bh"]].to_numpy()[0]
                if (mark_step >= 0) and (mark_count == 0):
                    part_img_old = img_old[bbox[1]: bbox[1] + bbox[3], bbox[0]: bbox[0] + bbox[2]].copy()
                    df_cca_old, contours_old = icaps.cca(part_img_old, thresh=thresh_old, ret_contours=True)
                    diff = np.sum(
                        np.abs(df_cca_old[["x", "y"]].to_numpy() + bbox[:2] - df.loc[idx_, ["x", "y"]].to_numpy()),
                        axis=1)
                    idx_cca_old = np.zeros_like(diff, dtype=bool)
                    idx_cca_old[np.argmin(diff)] = True

                    # idx_cca_old = df_cca_old["area"] == np.max(df_cca_old["area"])
                    cont_idx_old = df_cca_old[idx_cca_old].index[0]
                    bbox_cca_old = df_cca_old.loc[idx_cca_old, ["bx", "by", "bw", "bh"]].to_numpy()[0]
                    mark_img = icaps.draw_labelled_bbox(mark_img, bbox, label=particle, color=(0, 255, 0))
                    cv2.drawContours(mark_img, bbox_cca_old[:2] + bbox[:2] + [contours_old[cont_idx_old]],
                                     contourIdx=0, color=(255, 0, 0))
                    cv2.drawContours(mark_img, bbox_cca[:2] + bbox[:2] + [contours[cont_idx]],
                                     contourIdx=0, color=(0, 0, 255))
                    com_old = df_cca_old[["x", "y"]].to_numpy()[cont_idx_old]
                    com = df_cca[["x", "y"]].to_numpy()[cont_idx]
                    mark_img = icaps.draw_cross(mark_img, bbox[:2] + np.round(com_old).astype(int),
                                                color=(255, 0, 0), size=2)
                    mark_img = icaps.draw_plus(mark_img, bbox[:2] + np.round(com).astype(int),
                                               color=(0, 0, 255), size=2)
                if plot_particles:
                    # if (bbox[2] * bbox[3] > 500) and (bbox[2] * bbox[3] < 0.3 * img.size) and (np.max(part_img) > 100):
                    part_img_old = img_old[bbox[1]: bbox[1] + bbox[3], bbox[0]: bbox[0] + bbox[2]].copy()
                    df_cca_old, contours_old = icaps.cca(part_img_old, thresh=thresh_old, ret_contours=True)
                    diff = np.sum(
                        np.abs(df_cca_old[["x", "y"]].to_numpy() + bbox[:2] - df.loc[idx_, ["x", "y"]].to_numpy()),
                        axis=1)
                    idx_cca_old = np.zeros_like(diff, dtype=bool)
                    idx_cca_old[np.argmin(diff)] = True

                    # idx_cca_old = df_cca_old["area"] == np.max(df_cca_old["area"])
                    cont_idx_old = df_cca_old[idx_cca_old].index[0]
                    bbox_cca_old = df_cca_old.loc[idx_cca_old, ["bx", "by", "bw", "bh"]].to_numpy()[0]
                    _, ax = plt.subplots()
                    # print(df_cca.loc[idx_cca, ["sharpness", "median_grad", "max_grad"]].to_numpy()[0])
                    ax.annotate("sharp {:.2f}\nq84 {:.2f}\nmax {:.2f}".format(
                        *df_cca.loc[idx_cca,
                                    ["sharpness", "q84_grad", "max_grad"]].to_numpy()[0]
                    ), (0.85, 0.5), xycoords="figure fraction", annotation_clip=False, bbox={"facecolor": "white"})
                    plt.imshow(part_img, cmap=plt.get_cmap("binary_r"), vmin=0, vmax=255)
                    c1 = np.squeeze(bbox_cca_old[:2] + contours_old[cont_idx_old])
                    c1 = np.transpose(np.append(c1, [c1[0]], axis=0))
                    ax.plot(*c1, c="blue", ls="--")
                    c2 = np.squeeze(bbox_cca[:2] + contours[cont_idx])
                    c2 = np.transpose(np.append(c2, [c2[0]], axis=0))
                    ax.plot(*c2, c="red")
                    com1 = df_cca_old[["x", "y"]].to_numpy()[cont_idx_old]
                    ax.scatter(*com1, fc="none", ec="blue", marker="o", s=20)
                    com2 = df_cca[["x", "y"]].to_numpy()[cont_idx]
                    ax.scatter(*com2, c="red", marker="x", s=20)
                    ax.set_title("X: ({:.2f}, {:.2f}); O: ({:.2f}, {:.2f})".format(*com1, *com2))
                    # stdx = np.std([com1[0], com2[0]])
                    # stdy = np.std([com1[1], com2[1]])
                    plt.savefig(contour_plot_path + f"{particle}_{frame}.png")
                    plt.close()
                df.loc[idx_, "x"] = df_cca["x"].to_numpy()[cont_idx] + bbox[0]
                df.loc[idx_, "y"] = df_cca["y"].to_numpy()[cont_idx] + bbox[1]
                df.loc[idx_, "area"] = df_cca["area"].to_numpy()[cont_idx]
                df.loc[idx_, "ex"] = df_cca["ex"].to_numpy()[cont_idx]
                df.loc[idx_, "gyrad"] = df_cca["gyrad"].to_numpy()[cont_idx]
                df.loc[idx_, "max_ex"] = df_cca["max_ex"].to_numpy()[cont_idx]
                df.loc[idx_, "sharpness"] = df_cca["sharpness"].to_numpy()[cont_idx]
                df.loc[idx_, "q84_grad"] = df_cca["q84_grad"].to_numpy()[cont_idx]
                df.loc[idx_, "max_grad"] = df_cca["max_grad"].to_numpy()[cont_idx]
                pb.tick()
            if (mark_step >= 0) and (mark_count == 0):
                icaps.save_img(mark_img, mark_path + "{:0>6}.bmp".format(frame))
        df.to_csv(temp_path + "temp_{:0>3}_{:0>6}_{:0>6}.csv".format(ci, frames[0], frames[-1]), index=False)


if __name__ == '__main__':
    main()

