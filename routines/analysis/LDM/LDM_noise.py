import icaps
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.npy"
start_frame = 216700
stop_frame = 260300
# stop_frame = 216702
project_path = drive_letter + ":/icaps/data/LDM_Noise/"
cumu_path = project_path + "cumu/"
bin_lim = 6
bin_size = 0.25
csv_hist_path = project_path + "ldm_noise_hist_{}.csv".format(bin_size)
icaps.mk_folder(project_path)
ff = icaps.load_img(ff_path)
img_shape = ff.shape
n_pxls = img_shape[0] * img_shape[1]
n_frames = stop_frame - start_frame + 1

bins = np.arange(-bin_lim - 1/2 * bin_size, bin_lim + 3/2 * bin_size, bin_size)
print(bins)
pb = icaps.ProgressBar(n_frames)
df = pd.DataFrame(data={"bin_center": bins[:-1] + bin_size/2, "count": np.zeros(len(bins) - 1, dtype=np.uint64)})
for frame in range(start_frame, stop_frame + 1, 1):
    img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM2") + icaps.generate_file(frame),
                         strip=True)
    img = img.astype(np.int16)
    img = img - ff
    noise = img.flatten()
    hist, _ = np.histogram(noise, bins=bins)
    df["count"] += hist / n_pxls
    pb.tick()
pb.finish()
df["count"] *= n_pxls * n_frames
df.to_csv(csv_hist_path, index=False)

df = pd.read_csv(csv_hist_path)
_, ax = plt.subplots()
ax.bar(df["bin_center"], df["count"], color="black", width=bin_size*0.9)
ax.set_xlabel("Noise")
ax.set_ylabel("Frequency")
ax.set_xlim(-4, 4)
plt.savefig(project_path + "ldm_noise_hist_{}.png".format(bin_size), dpi=600)


# icaps.mk_folder(cumu_path, clear=True)
# sample_size = 4
# n_samples = 20
# chunk_size = np.floor(n_frames/n_samples).astype(int)
# assert(sample_size < chunk_size)
# samples = np.zeros((n_samples, 2), dtype=np.uint64)
# for i, chunk_start in enumerate(range(start_frame, stop_frame + 1, chunk_size)):
#     if i >= n_samples:
#         break
#     chunk_center = chunk_start + int(chunk_size/2)
#     samples[i, 0] = chunk_center - np.floor(sample_size / 2).astype(int)
#     samples[i, 1] = chunk_center + np.floor(sample_size / 2).astype(int)
#     if sample_size % 2 == 0:
#         samples[i, 1] -= 1
#
# pb = icaps.ProgressBar(len(samples) * sample_size, "Progress")
# for sample in samples:
#     noises = np.array([], dtype=np.float64)
#     s0 = int(sample[0])
#     s1 = int(sample[1])
#     for frame in range(s0, s1 + 1, 1):
#         img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM2") + icaps.generate_file(frame),
#                              strip=True)
#         img = img.astype(np.int16)
#         img = img - ff
#         noise = np.sort(img.flatten())
#         noises = np.append(noises, noise)
#         pb.tick()
#     noise = np.sort(noises)
#     norm_noise = np.linspace(1 / len(noise), 1, len(noise))
#     _, ax = plt.subplots()
#     ax.scatter(noise, norm_noise, c="black", s=3)
#     ax.set_xlabel("Noise")
#     ax.set_ylabel("Normalized Cumulative Frequency")
#     ax.set_title("{} to {}, N: {}".format(s0, s1, s1 - s0 + 1))
#     ax.set_xlim(-3, 3)
#     plt.savefig(cumu_path + "Noise_Cumu_{}_{}_{}.png".format(s0, s1, s1 - s0 + 1), dpi=600)
#     plt.close()
# pb.finish()



