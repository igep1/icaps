import icaps
import numpy as np
import pandas as pd
import cv2

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM/"
plot_path = project_path + "plots/"
movie_path = plot_path + "movies/"
candidate_path = plot_path + "candidates/"
icaps.mk_folders([movie_path, candidate_path])

ff_path = project_path + "ff_216700_260300.npy"
fit1 = [icaps.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [icaps.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (260300,)

csv_track = project_path + "Tracks_PEL100.csv"


def main():
    ff = icaps.load_img(ff_path)
    ff_mean = np.mean(ff)
    df = pd.read_csv(csv_track)
    particles_old = np.squeeze(pd.read_csv(project_path + "good_rot_particles.csv").to_numpy())
    df_q84 = df[df["q84_grad"] >= 20]
    df_q84 = icaps.filter_track_length(df_q84, 100)
    particles = df_q84["particle"].unique()
    print(len(particles), len(particles_old),
          len(df_q84[~df_q84["particle"].isin(particles_old)]["particle"].unique()))
    frames = df_q84["frame"].unique()
    icaps.mk_folders([candidate_path + "{:06d}/".format(particle) for particle in particles])
    for frame in frames:
        img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/", "LDM", frame) + icaps.generate_file(frame),
                             strip=True)
        img = icaps.ffc(img, icaps.adjust_ff(ff, icaps.get_fit([fit1, fit2], fit_cutoffs, frame), frame,
                                             ff_mean=ff_mean))
        df_ = df[df["frame"] == frame]
        particles_ = df_["particle"].unique()
        for particle in particles_:
            part_path = candidate_path + "{:06d}/".format(particle)


if __name__ == '__main__':
    main()

