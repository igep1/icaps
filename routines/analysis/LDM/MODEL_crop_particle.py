import icaps
import numpy as np
import pandas as pd
import cv2

ffc_path = "E:/icaps/data/LDM/ffc/"
track_path = "E:/icaps/data/LDM/Tracks_all.csv"
parts = (517224,)
out_path = "E:/icaps/data/Models/"
enlarge = 8


def main():
    df = pd.read_csv(track_path)
    df = df[np.in1d(df["particle"].to_numpy(), parts)]
    shapes = {}
    for particle in parts:
        df_ = df[df["particle"] == particle]
        dx1_max = np.amax(df_["x"].to_numpy() - df_["bx"].to_numpy())
        dy1_max = np.amax(df_["y"].to_numpy() - df_["by"].to_numpy())
        dx2_max = np.amax(df_["bx"].to_numpy() + df_["bw"].to_numpy() - df_["x"].to_numpy())
        dy2_max = np.amax(df_["by"].to_numpy() + df_["bh"].to_numpy() - df_["y"].to_numpy())
        w = np.ceil(2 * np.amax([dx1_max, dx2_max])).astype(np.uint64)
        h = np.ceil(2 * np.amax([dy1_max, dy2_max])).astype(np.uint64)
        shapes[particle] = (h, w)

    frames = np.unique(df["frame"].to_numpy())
    pb = icaps.ProgressBar(len(frames), "Progress")
    for frame in frames:
        df_ = df[df["frame"] == frame]
        img = icaps.load_img(ffc_path + icaps.generate_file(frame))
        labels = df_["label"].to_numpy()
        particles = df_["particle"].to_numpy()
        for i, particle in enumerate(particles):
            label = labels[i]
            stats = df_[df_["label"] == label]
            bbox = stats[["bx", "by", "bw", "bh"]].to_numpy()[0]
            # if bbox[0] == 0 or bbox[1] == 0 or bbox[2] == 1024 or bbox[3] == 1024:
            #     continue
            part_img = icaps.crop(img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True, copy_=True)
            # part_img = icaps.threshold(part_img, thresh=3, replace=(0, None))
            contour = cv2.findContours(part_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0][0]

            mask = np.zeros(part_img.shape, np.uint8)
            mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
            mask = icaps.fill_holes(mask)
            part_img[mask == 0] = 0

            h, w = shapes[particle]
            canvas = np.zeros((h, w), dtype=np.uint8)
            x = int(np.round(w / 2 - (stats["x"].to_numpy()[0] - bbox[0])))
            y = int(np.round(h / 2 - (stats["y"].to_numpy()[0] - bbox[1])))
            canvas[y:y + bbox[3], x:x + bbox[2]] = part_img
            canvas = icaps.enlarge_pixels(canvas, enlarge)
            contour = np.array([np.array([x, y]) + c for c in contour])
            # canvas = cv2.drawContours(canvas, [contour * enlarge], contourIdx=0, color=255)
            icaps.mk_folder(out_path + "{:0>6}".format(particle) + "/")
            icaps.save_img(canvas, out_path + "{:0>6}".format(particle) + "/" + "{:0>6}".format(particle) +
                           "_{:0>6}".format(frame) + ".bmp")
        pb.tick()
    pb.finish()


if __name__ == '__main__':
    main()


