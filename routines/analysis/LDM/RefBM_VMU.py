import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
vmu_path = project_path + "vmu/"
plot_path = vmu_path + "plots/"
csv_track = vmu_path + "cloud_track.csv"
csv_track_clean = vmu_path + "cloud_track_clean.csv"
csv_phases = project_path + "Phases_Hand.csv"
csv_trans_disp = vmu_path + "DISP_Trans.csv"
csv_trans_erf = vmu_path + "ERF_Trans.csv"
csv_trans_of = vmu_path + "OF_Trans.csv"

min_dts = np.arange(4, 49, 4)

icaps.mk_folders([plot_path])
pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 1000)


def main():
    # """
    df_vmu = pd.read_csv(csv_track)
    df_vmu.dropna(inplace=True)

    df_vmu["particle"] = 0
    df_vmu["time_round"] = df_vmu["timestamp"] - icaps.const.start_ldm
    df_time = pd.DataFrame(data={
        "frame": np.arange(0, icaps.const.nframes_ldm, 1, dtype=int),
        "time": np.linspace(0, icaps.const.nframes_ldm / icaps.const.fps_ldm * 1000, icaps.const.nframes_ldm),
        "time_round": np.round(np.linspace(0, icaps.const.nframes_ldm / icaps.const.fps_ldm * 1000,
                                           icaps.const.nframes_ldm)).astype(int)
    })
    df_vmu = pd.merge(df_vmu, df_time, on="time_round", how="left")
    df_vmu["particle"] = df_vmu["particle"].astype(int)
    df_vmu["timestamp"] = df_vmu["timestamp"].astype(int)
    df_vmu = df_vmu.dropna()
    df_vmu["frame"] = df_vmu["frame"].astype(int)

    dx = df_vmu["dx"].copy()
    dy = df_vmu["dy"].copy()
    dz = df_vmu["dz"].copy()
    theta = np.radians(icaps.const.ldm_tilt)
    dx_ldm = dx * np.sin(theta) ** 2 - dy * np.cos(theta) - dz * np.sin(theta) * np.cos(theta)
    dy_ldm = -dx * np.sin(theta) * np.cos(theta) - dy * np.sin(theta) - dz * np.cos(theta) ** 2
    # warnings.filterwarnings("ignore")
    df_vmu["dx_T"] = dx_ldm
    df_vmu["dy_T"] = dy_ldm
    df_vmu["X"] = dx.cumsum()
    df_vmu["Y"] = dy.cumsum()
    df_vmu["Z"] = dz.cumsum()
    df_vmu["x"] = dx_ldm.cumsum()
    df_vmu["y"] = dy_ldm.cumsum()
    # warnings.filterwarnings("default")
    df_phases = pd.read_csv(csv_phases)
    # undisturbed = df_phases[df_phases["phase"] == 0]["frame"].to_numpy()
    # df_vmu = icaps.filter_disturbance(df_vmu, undisturbed=undisturbed, column="time_round")
    df_vmu = icaps.filter_disturbance(df_vmu, df_phases=df_phases, on="frame")
    df_vmu.to_csv(csv_track_clean, index=False)
    # """

    # """
    df_vmu = pd.read_csv(csv_track_clean)
    df_disp = icaps.brownian.stitch_displacements(df_vmu, min_dts, add_leaps=False,
                                                  in_columns=["X", "Z", "Y"], out_columns="d{}", silent=False)
    df_disp.to_csv(csv_trans_disp, index=False)
    icaps.brownian.plot_stitched_displacements(df_disp, min_dts, "X", "dX", silent=False, sub_folder=None,
                                               plot_path=plot_path + "disp_trans_X/", clear_out=False)
    icaps.brownian.plot_stitched_displacements(df_disp, min_dts, "Y", "dY", silent=False, sub_folder=None,
                                               plot_path=plot_path + "disp_trans_Y/", clear_out=False)
    icaps.brownian.plot_stitched_displacements(df_disp, min_dts, "Z", "dZ", silent=False, sub_folder=None,
                                               plot_path=plot_path + "disp_trans_Z/", clear_out=False)
    # """

    df_disp = pd.read_csv(csv_trans_disp)
    df_erf = icaps.brownian.calc_erf(df_disp, min_dts, ["X", "Y", "Z"], silent=False, fit_type=2, n_min=20,
                                     plot_path=plot_path + "erf_trans/", sub_folder=None, clear_out=True, dpi=300,
                                     p0=[0.0, 1.0], disp_names=r"$\Delta {}$", double_gauss=True, fickian=False,
                                     plot_fails=False, var_lr=True)
    df_erf.to_csv(csv_trans_erf, index=False)

    df, df2 = icaps.brownian.calc_ornstein_fuerth(
        df_erf, min_dts, axes=["X", "Y", "Z"], mode="trans",
        temp_path=vmu_path + "OF_Trans_temp.csv", silent=False, ret_erf=True,
        plot={"path": plot_path + "of_trans_{}/",
              "attempts": True,
              "error": True,
              "dpi": 600,
              "clear_out": True,
              "crop_to_max": True,
              "min_chisqr": 5e-3,
              "plot_until": 1.},
        fit={"sequential": False,
             "p0": None,
             "maxit": None,
             "fit_type": 2,
             "fit_err": True,
             "bounds": (1, None),
             "ballistic_bounds": (1, 6),
             "ballistic_fit": "OF",
             "n_min": 5,
             "max_sigsqr": 1e-6,
             "max_err_ratio": 1.,
             "min_tau": None,
             "adapt_margins": (3, 0),
             "adapt_quantity": "count",
             "adapt_grade_weights": (1., 1.),
             "adapt_grade_type": "gaussian"
             },
        double_gauss=False, sigma_diff=True)
    df.to_csv(csv_trans_of, index=False)


if __name__ == '__main__':
    main()

