import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Noise/"
plot_path = project_path + "plots/"
orig_path = project_path + "orig/"
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.bmp"
temp_path = project_path + "temp/"
csv_fstats = project_path + "frame_stats.csv"
icaps.mk_folders([temp_path, plot_path, orig_path])


def main():
    # batch_perc = 0.005
    # start_frame = 0
    batch_perc = 0.01
    start_frame = 211025
    
    stop_frame = icaps.const.ldm_nframes - 1
    frames = np.arange(start_frame, stop_frame + 1)
    batch_size = int(round(batch_perc * len(frames)))
    print("Batch Size", batch_size)
    nbatches = int(np.ceil(len(frames) / batch_size))
    batch = 1
    while True:
        nframes = np.min([stop_frame + 1 - start_frame, batch_size])
        frames = np.arange(0, nframes) + start_frame
        imgs = np.zeros((nframes, *icaps.const.shape_ldm), dtype=np.uint8)
        pb = icaps.SimpleProgressBar(len(frames), f"Batch {batch}/{nbatches}")
        for i, frame in enumerate(frames):
            pb.set_stage(f"Loading ({frame})")
            file = icaps.generate_file(frame)
            img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame) + file, strip=True)
            imgs[i] = img
            pb.tick()
        pb.set_stage("Calculating Means...")
        medians = np.median(imgs, axis=(1, 2))
        means = np.mean(imgs, axis=(1, 2))
        pb.set_stage("Calculating Std...")
        stds = np.std(imgs, axis=(1, 2))
        pb.set_stage("Calculating Extremes...")
        mins = np.min(imgs, axis=(1, 2))
        maxs = np.max(imgs, axis=(1, 2))
        pb.set_stage("Calculating Totals...")
        sums = np.sum(imgs, axis=(1, 2))
        df = pd.DataFrame(data={
            "frame": frames,
            "median": medians,
            "mean": means,
            "std": stds,
            "min": mins,
            "max": maxs,
            "sum": sums
        })
        pb.set_stage("Writing...")
        df.to_csv(temp_path + "temp_{:06d}_{:06d}.csv".format(start_frame, frames[-1]), index=False)
        pb.set_stage("Done")
        pb.close()
        batch += 1
        start_frame = frames[-1] + 1
        if start_frame >= stop_frame + 1:
            break

    icaps.merge_dataframes(temp_path, csv_fstats, silent=False, delete_source=False)


if __name__ == '__main__':
    main()





