import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Noise/"
plot_path = project_path + "plots/"
orig_path = project_path + "orig/"
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.bmp"
csv_flat_hist = project_path + "flat_hist_nocloud.csv"
csv_stats = project_path + "stats_nocloud.csv"
npy_total = project_path + "total_nocloud.npy"
npy_px_hist = project_path + "px_hist_nocloud.npy"
temp_path = project_path + "temp_nocloud/"
icaps.mk_folders([plot_path, orig_path, temp_path])


def main():
    start_frame, stop_frame = map(int, ff_path.split("/")[-1].split(".")[0].split("_")[1:])
    frames = np.arange(start_frame, stop_frame + 1)
    px_hist = np.zeros((*icaps.const.shape_ldm, 256), dtype=np.uint64)
    px_bins = np.arange(0, 257)
    flat_hist = np.zeros(256, dtype=np.uint64)
    total_img = np.zeros(icaps.const.shape_ldm, dtype=np.uint64)

    batch_perc = 0.005
    batch_size = int(round(batch_perc * len(frames)))
    print("Batch Size", batch_size)
    nbatches = int(np.ceil(len(frames) / batch_size))
    batch = 1
    while True:
        nframes = np.min([stop_frame + 1 - start_frame, batch_size])
        frames = np.arange(0, nframes) + start_frame
        if len(frames) == 0:
            print("no frames")
            break
        imgs = np.zeros((nframes, *icaps.const.shape_ldm), dtype=np.uint8)
        pb = icaps.SimpleProgressBar(len(frames), f"Batch {batch}/{nbatches}")
        for i, frame in enumerate(frames):
            pb.set_stage(f"Loading ({frame})")
            file = icaps.generate_file(frame)
            img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame) + file, strip=True)
            imgs[i] = img
            hist, _ = np.histogram(img, bins=px_bins)
            flat_hist += hist.astype(np.uint64)
            pb.tick()
        pb.set_stage("Calculating Flat Sum...")
        total_img += np.sum(imgs, axis=0)
        pb.set_stage("Calculating Pixel Hist...")
        for j in range(icaps.const.shape_ldm[0]):
            for i in range(icaps.const.shape_ldm[1]):
                px_hist[j, i] += np.histogram(imgs[:, j, i], bins=px_bins)[0].astype(np.uint64)

        pb.set_stage("Calculating Means...")
        medians = np.median(imgs, axis=(1, 2))
        means = np.mean(imgs, axis=(1, 2))
        pb.set_stage("Calculating Std...")
        stds = np.std(imgs, axis=(1, 2))
        pb.set_stage("Calculating Extremes...")
        mins = np.min(imgs, axis=(1, 2))
        maxs = np.max(imgs, axis=(1, 2))
        pb.set_stage("Calculating Totals...")
        sums = np.sum(imgs, axis=(1, 2))
        df = pd.DataFrame(data={
            "frame": frames,
            "median": medians,
            "mean": means,
            "std": stds,
            "min": mins,
            "max": maxs,
            "sum": sums
        })
        pb.set_stage("Writing...")
        df.to_csv(temp_path + f"temp_{batch}.csv", index=False)
        pb.set_stage("Done")
        pb.close()
        batch += 1
        start_frame = frames[-1] + 1
        if start_frame >= stop_frame + 1:
            break
    np.save(npy_total, total_img)
    np.save(npy_px_hist, px_hist)
    df = pd.DataFrame(data={"value": px_bins[:-1], "count": flat_hist})
    df.to_csv(csv_flat_hist, index=False)
    icaps.merge_dataframes(temp_path, csv_stats, silent=False, delete_source=False)


if __name__ == '__main__':
    main()



