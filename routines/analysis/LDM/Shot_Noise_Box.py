import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Noise/"
plot_path = project_path + "plots/"
orig_path = project_path + "orig/"
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.bmp"
temp_path = project_path + "temp_box/"
csv_fstats = project_path + "frame_boxes.csv"
icaps.mk_folders([temp_path, plot_path, orig_path])


def main():
    batch_perc = 0.005
    start_frame = 0
    # batch_perc = 0.01
    # start_frame = 211025
    box_size = 128
    nboxrow = 8

    stop_frame = icaps.const.ldm_nframes - 1
    frames = np.arange(start_frame, stop_frame + 1)
    batch_size = int(round(batch_perc * len(frames)))
    print("Batch Size", batch_size)
    nbatches = int(np.ceil(len(frames) / batch_size))
    batch = 1
    while True:
        nframes = np.min([stop_frame + 1 - start_frame, batch_size])
        frames = np.arange(0, nframes) + start_frame
        imgs = np.zeros((nframes, *icaps.const.shape_ldm), dtype=np.uint8)
        pb = icaps.SimpleProgressBar(len(frames), f"Batch {batch}/{nbatches}")
        for i, frame in enumerate(frames):
            pb.set_stage(f"Loading ({frame})")
            file = icaps.generate_file(frame)
            img = icaps.load_img(icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame) + file, strip=True)
            imgs[i] = img
            pb.tick()
        pb.set_stage("Calculating Boxes...")
        means = np.zeros((len(frames), nboxrow * nboxrow), dtype=np.float)
        for f in range(len(frames)):
            for j in range(nboxrow):
                for i in range(nboxrow):
                    means[f, j * nboxrow + i] = np.mean(imgs[f,
                                                        j * box_size: (j + 1) * box_size,
                                                        i * box_size: (i + 1) * box_size])
        data = {"frame": frames}
        data = {**data, **{f"box_{j}_ {i}": means[:, j * nboxrow + 1] for i in range(nboxrow) for j in range(nboxrow)}}
        pb.set_stage("Getting Corners...")
        data_ = {"corner_tl": imgs[:, 0, 0],
                 "corner_tr": imgs[:, 0, icaps.const.shape_ldm[1] - 1],
                 "corner_bl": imgs[:, icaps.const.shape_ldm[0] - 1, 0],
                 "corner_br": imgs[:, icaps.const.shape_ldm[0] - 1, icaps.const.shape_ldm[1] - 1]}
        data = {**data, **data_}
        df = pd.DataFrame(data=data)
        pb.set_stage("Writing...")
        df.to_csv(temp_path + "temp_{:06d}_{:06d}.csv".format(start_frame, frames[-1]), index=False)
        pb.set_stage("Done")
        pb.close()
        batch += 1
        start_frame = frames[-1] + 1
        if start_frame >= stop_frame + 1:
            break

    icaps.merge_dataframes(temp_path, csv_fstats, silent=False, delete_source=False)


if __name__ == '__main__':
    main()





