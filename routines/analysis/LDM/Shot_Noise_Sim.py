import matplotlib.pyplot as plt
import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Noise/"
particle_path = drive_letter + ":/icaps/data/LDM_ROT6/particles_rot/"
csv_sim = project_path + "noise_sim.csv"


# sig_flux = 1
# sig_shot = 0.4
sig_flux = 0.63
sig_shot = 0.35
iterations = 10
show_sample = False


def plot():
    df = pd.read_csv(csv_sim)
    _, ax = plt.subplots()
    x = df[["dx", "dy"]].to_numpy().flatten() * icaps.const.px_ldm
    x = np.abs(x)
    x = np.sort(x)
    norm_x = np.linspace(1 / len(x), 1, len(x))
    ax.errorbar(x, norm_x, c="black", fmt=".", markersize=1)
    ax.set_xlabel("Absolute COM Offset in x and y (µm)")
    ax.set_ylabel("Normalized Cumulative Frequency")
    plt.show()

    _, ax = plt.subplots()
    x = df["dgyr"].to_numpy() * icaps.const.px_ldm
    x = np.abs(x)
    x = np.sort(x)
    norm_x = np.linspace(1 / len(x), 1, len(x))
    ax.errorbar(x, norm_x, c="black", fmt=".", markersize=1)
    ax.set_xlabel(r"Absolute $r_g$ Offset (µm)")
    ax.set_ylabel("Normalized Cumulative Frequency")
    plt.show()

    _, ax = plt.subplots()
    x = df["dangle"].to_numpy()
    x = np.abs(x)
    x = np.sort(x)
    norm_x = np.linspace(1 / len(x), 1, len(x))
    ax.errorbar(x, norm_x, c="black", fmt=".", markersize=1)
    ax.set_xlabel(r"Absolute $\theta$ Offset (rad)")
    ax.set_ylabel("Normalized Cumulative Frequency")
    plt.show()


def run():
    max_chisqr_trans = 40e-4
    max_chisqr_rot = 60e-4
    max_rel_err_trans_mass = 0.1
    max_rel_err_trans_tau = 0.1
    max_rel_err_rot_moi = 0.5
    max_rel_err_rot_tau = 0.5
    df_all = pd.read_csv(drive_letter + ":/icaps/data/LDM_ROT6/OF_Final_diff_drop1.csv")
    df_t1 = icaps.brownian.filter_chisqr(df_all, thresh=max_chisqr_trans, mode="trans")
    df_t1 = icaps.brownian.filter_rel_err(df_t1, mass=max_rel_err_trans_mass, mode="trans")
    df_r1 = df_t1.dropna(subset=["tau_rot"])
    df_r1 = icaps.brownian.filter_chisqr(df_r1, thresh=max_chisqr_rot, mode="rot")
    df_r1 = icaps.brownian.filter_rel_err(df_r1, mass=max_rel_err_rot_moi, mode="rot")
    df_r2 = icaps.brownian.filter_rel_err(df_r1, tau=max_rel_err_trans_tau, mode="trans")
    df_r2 = icaps.brownian.filter_rel_err(df_r2, tau=max_rel_err_rot_tau, mode="rot")
    particles = df_r1["particle"].unique()
    # particles = [720240]

    df_focus = pd.read_csv(drive_letter + ":/icaps/data/LDM_ROT6/focus_filter.csv")
    particles_focus = df_focus["particle"].unique()
    df = None
    for pidx, particle in enumerate(particles):
        ffc_path = particle_path + f"{particle}/ffc/"
        files = icaps.get_files(ffc_path)
        frames = [int(float(file.split(".")[0].split("_")[1])) for file in files]
        nan_arr = np.zeros(len(files) * iterations)
        nan_arr[:] = np.nan
        frames_rot = []
        if particle in particles_focus:
            df_fpart = pd.read_csv(particle_path + f"{particle}/focus.csv")
            min_focus = df_focus.loc[df_focus["particle"] == particle, "min_focus"].to_numpy()[0]
            frames_rot = df_fpart.loc[df_fpart["focus"] >= min_focus, "frame"].to_numpy()
        df_ = pd.DataFrame(data={
            "particle": np.repeat(particle, len(files) * iterations),
            "frame": np.repeat(frames, iterations),
            "iteration": np.tile(np.arange(iterations) + 1, len(files)),
            "flux": nan_arr,
            "dx": nan_arr,
            "dy": nan_arr,
            "dgyr": nan_arr,
            "dangle": nan_arr,
            "dmajor": nan_arr,
            "dminor": nan_arr
        })
        pb = icaps.SimpleProgressBar(len(files) * iterations, f"Particle {particle} ({pidx + 1}/{len(particles)})")
        for j, file in enumerate(files):
            img = icaps.load_img(ffc_path + file).astype(float)
            com = icaps.measure_com(img)
            gyrad = icaps.measure_gyrad(img, com=com)
            ax = None
            if show_sample:
                _, ax = plt.subplots(nrows=3, ncols=3)
                ax[0][0].imshow(img)
                ax[0][0].scatter(com[0], com[1], color="red", s=3, marker="x")
                ax[0][0].set_title(gyrad)
            frame = frames[j]
            box = None
            if frame in frames_rot:
                box = icaps.brownian.fit_ellipse(img.astype(np.uint8), thresh=20)
            for i in range(iterations):
                img_ = img.copy()
                # Flux Noise
                flux = np.random.normal(0, sig_flux, 1)[0]
                img_ += flux
                # Shot Noise
                gauss = np.random.normal(0, sig_shot, img.shape)
                gauss = gauss.reshape(img.shape)
                img_ += gauss
                # Thresholding
                img_[img_ < 0] = 0
                img_ = img_.astype(np.uint8)    # due to saving as bmp in between steps
                img_ = icaps.threshold(img_, 3, replace=(0, None))
                try:
                    com_ = icaps.measure_com(img_)
                except ZeroDivisionError:
                    continue
                gyrad_ = icaps.measure_gyrad(img_, com=com_)
                idx = (df_["frame"] == frame) & (df_["iteration"] == i + 1)
                df_.loc[idx, "flux"] = flux
                df_.loc[idx, "dx"] = com[0] - com_[0]
                df_.loc[idx, "dy"] = com[1] - com_[1]
                df_.loc[idx, "dgyr"] = gyrad - gyrad_
                if (frame in frames_rot) and (box is not None):
                    box_ = icaps.brownian.fit_ellipse(img_, thresh=20)
                    if box_ is not None:
                        df_.loc[idx, "dangle"] = np.deg2rad(box[2]) - np.deg2rad(box_[2])
                        df_.loc[idx, "dmajor"] = box[1][1] - box_[1][1]
                        df_.loc[idx, "dminor"] = box[1][0] - box_[1][0]
                if show_sample and (i + 1 <= 8):
                    ax[(i + 1) // 3][(i + 1) % 3].imshow(img_)
                    ax[(i + 1) // 3][(i + 1) % 3].scatter(com_[0], com_[1], color="red", s=3, marker="x")
                    ax[(i + 1) // 3][(i + 1) % 3].set_title(gyrad_)
                pb.tick()
            if show_sample:
                plt.show()
        pb.close()
        if df is None:
            df = df_
        else:
            df = df.append(df_, sort=False, ignore_index=True)
    df.to_csv(csv_sim, index=False)


if __name__ == '__main__':
    # run()
    plot()


