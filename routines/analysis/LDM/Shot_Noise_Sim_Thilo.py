import os
import cv2
import matplotlib.pyplot as plt
import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
thilo_path = project_path + "thilo/"
csv_sim = thilo_path + "noise_sim.csv"
particle_path = thilo_path + "images/"


# sig_flux = 1
# sig_shot = 0.4
sig_flux = 0.63
sig_shot = 0.35
iterations = 10
show_sample = False


def plot():
    df = pd.read_csv(csv_sim)
    _, ax = plt.subplots()
    x = df[["dx", "dy"]].to_numpy().flatten() * icaps.const.px_ldm
    x = np.abs(x)
    x = np.sort(x)
    norm_x = np.linspace(1 / len(x), 1, len(x))
    ax.errorbar(x, norm_x, c="black", fmt=".", markersize=1)
    ax.set_xlabel("Absolute COM Offset in x and y (µm)")
    ax.set_ylabel("Normalized Cumulative Frequency")
    plt.show()

    # _, ax = plt.subplots()
    # x = df["dgyr"].to_numpy() * icaps.const.px_ldm
    # x = np.abs(x)
    # x = np.sort(x)
    # norm_x = np.linspace(1 / len(x), 1, len(x))
    # ax.errorbar(x, norm_x, c="black", fmt=".", markersize=1)
    # ax.set_xlabel(r"Absolute $r_g$ Offset (µm)")
    # ax.set_ylabel("Normalized Cumulative Frequency")
    # plt.show()

    # _, ax = plt.subplots()
    # x = df["dangle"].to_numpy()
    # x = np.abs(x)
    # x = np.sort(x)
    # norm_x = np.linspace(1 / len(x), 1, len(x))
    # ax.errorbar(x, norm_x, c="black", fmt=".", markersize=1)
    # ax.set_xlabel(r"Absolute $\theta$ Offset (rad)")
    # ax.set_ylabel("Normalized Cumulative Frequency")
    # plt.show()


def run():
    particle = 0
    gen_dirs = os.listdir(particle_path)
    # gen_dirs = ["gen_09"]
    nparts = sum([len(os.listdir(particle_path + gen_dir)) for gen_dir in gen_dirs])
    df = None
    pb = icaps.SimpleProgressBar(nparts * 360 * iterations)
    for k, gen_dir in enumerate(gen_dirs):
        part_dirs = os.listdir(particle_path + gen_dir)
        # part_dirs = ["bcca_8192_0_gen_09_00001"]
        pb.set_stage(f"Gen {k + 1}")
        for j, part_dir in enumerate(part_dirs):
            pb.set_stage(f"Gen {k + 1}, Part {j + 1}, ID {particle}")
            part_path = particle_path + gen_dir + "/" + part_dir + "/"
            frames = np.arange(1, 361)
            nan_arr = np.zeros(len(frames) * iterations)
            nan_arr[:] = np.nan
            df_ = pd.DataFrame(data={
                "gen": np.repeat(k + 1, len(frames) * iterations),
                "particle": np.repeat(particle, len(frames) * iterations),
                "frame": np.repeat(frames, iterations),
                "iteration": np.tile(np.arange(iterations) + 1, len(frames)),
                "flux": nan_arr,
                "dx": nan_arr,
                "dy": nan_arr,
                # "dgyr": nan_arr,
                # "dangle": nan_arr,
                # "dmajor": nan_arr,
                # "dminor": nan_arr
            })
            for frame in frames:
                pb.set_stage(f"Gen {k + 1}, Part {j + 1}, ID {particle}, Frame {frame}")
                com = pd.read_csv(part_path + f"pxcenter_true_{frame}.csv", header=None)[0].to_numpy() - 0.5
                com = com[::-1]
                img = pd.read_csv(part_path + f"image_{frame}.csv", header=None).to_numpy().astype(float)
                # com2 = icaps.measure_com(img)
                # print(com, com2)
                # print(img)
                # _, ax = plt.subplots()
                # # plt.imshow(img, cmap=plt.get_cmap("binary_r"))
                # plt.pcolor(img, cmap=plt.get_cmap("binary_r"))
                # ax.invert_yaxis()
                # ax.scatter(*(com + 0.5), c="red", marker="x", s=50)
                # ax.scatter(*(com2 + 0.5), c="blue", marker="+", s=50)
                # plt.show()

                ax = None
                if show_sample:
                    _, ax = plt.subplots(nrows=3, ncols=3)
                    ax[0][0].imshow(img)
                    ax[0][0].scatter(com[0], com[1], color="red", s=3, marker="x")
                    # ax[0][0].set_title(gyrad)
                for i in range(iterations):
                    img_ = img.copy()
                    # Flux Noise
                    flux = np.random.normal(0, sig_flux, 1)[0]
                    img_ += flux
                    # Shot Noise
                    gauss = np.random.normal(0, sig_shot, img.shape)
                    gauss = gauss.reshape(img.shape)
                    img_ += gauss
                    # Thresholding
                    img_[img_ < 0] = 0
                    img_ = img_.astype(np.uint8)    # due to saving as bmp in between steps
                    img_ = icaps.threshold(img_, 3, replace=(0, None))
                    try:
                        com_ = icaps.measure_com(img_)
                    except ZeroDivisionError:
                        continue
                    idx = (df_["frame"] == frame) & (df_["iteration"] == i + 1)
                    df_.loc[idx, "flux"] = flux
                    df_.loc[idx, "dx"] = com[0] - com_[0]
                    df_.loc[idx, "dy"] = com[1] - com_[1]
                    if show_sample and (i + 1 <= 8):
                        ax[(i + 1) // 3][(i + 1) % 3].imshow(img_)
                        ax[(i + 1) // 3][(i + 1) % 3].scatter(com_[0], com_[1], color="red", s=3, marker="x")
                        # ax[(i + 1) // 3][(i + 1) % 3].set_title(gyrad_)
                    pb.tick()
                if show_sample:
                    plt.show()
            if df is None:
                df = df_
            else:
                df = df.append(df_, sort=False, ignore_index=True)
            particle += 1
    df.to_csv(csv_sim, index=False)
    pb.close()


if __name__ == '__main__':
    run()
    plot()


