import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Strange/"
particle_path = project_path + "particles/"
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
csv_track = drive_letter + ":/icaps/data/LDM_ROT6/Tracks_all.csv"
csv_focus_filter = project_path + "focus_filter.csv"
csv_rot = project_path + "Tracks_rot.csv"
icaps.mk_folder(particle_path)


def main():
    # df = pd.read_csv(csv_track)
    # particles = [747039, 847467]
    # df = df[df["particle"].isin(particles)]
    # # icaps.crop_df_particles(df[df["particle"].isin(particles)], ffc_path, particle_path,
    # #                         enlarge=1, subfolder="ffc", silent=False)
    # # icaps.measure_foci_bulk(particle_path, include_stats=True, detailed_progress=True)
    # df = icaps.collect_focus(df, particle_path, silent=False)
    # df_focus_filter = pd.read_csv(csv_focus_filter)
    # idcs = []
    # for particle in particles:
    #     focus_thresh = df_focus_filter.loc[df_focus_filter["particle"] == particle, "min_focus"].to_numpy()[0]
    #     idcs.extend(df.index[(df["particle"] == particle) & (df["focus"] < focus_thresh)].to_list())
    # df.drop(idcs, axis=0, inplace=True)
    # df.to_csv(csv_rot, index=False)

    # df = pd.read_csv(csv_rot)
    # df = icaps.brownian.fit_ellipses_bulk(df, particle_path, mark=True, mark_enlarge=4, silent=False,
    #                                       clear_out=True, thresh=20, mark_thresh=0, mark_thickness=3,
    #                                       mark_colors=[(0, 255, 0), (0, 0, 255)])
    # df.to_csv(csv_rot, index=False)

    icaps.mk_movie(particle_path + "847467/mark/", particle_path + "847467/movie.mp4",
                   fps=60)


if __name__ == "__main__":
    main()


