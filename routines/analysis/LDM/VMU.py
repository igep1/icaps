import pandas as pd
import icaps
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/VMU/"
vmu_log_csv = project_path + "icaps_virtual_motion.csv"
track_csv = project_path + "track_vmu.csv"
disp_csv = project_path + "disp_vmu.csv"
erf_csv = project_path + "erf_vmu.csv"
of_csv = project_path + "of_vmu.csv"
plot_path = project_path + "plots/"
icaps.mk_folders(plot_path)


def main():
    # df = pd.read_csv(vmu_log_csv)
    # df = df[["time_stamp", "x", "y", "z"]]
    # t0_vmu_xz = icaps.const.start_vmu_xz
    # t0_vmu_yz = icaps.const.start_vmu_yz
    # t0_xz = icaps.const.start_oos_xz
    # t0_yz = icaps.const.start_oos_yz + 2    # so that it aligns with vmu
    # t0_xz += 8     # to get roughly the middle of the interval, not the edges
    # t0_yz += 8
    # tmax = np.max(df["time_stamp"])
    # tf_xz = pd.Series(np.arange(t0_xz, tmax + 1, 20))
    # tf_yz = pd.Series(np.arange(t0_yz, tmax + 1, 20))
    # df = df[df["time_stamp"].isin(np.append(tf_xz, tf_yz))]
    # df.loc[df["time_stamp"].isin(tf_xz) & ~df["time_stamp"].isin(tf_yz), "y"] = np.nan
    # df.loc[df["time_stamp"].isin(tf_yz) & ~df["time_stamp"].isin(tf_xz), ["x", "z"]] = np.nan
    # df["frame"] = -1
    # df.loc[df["time_stamp"].isin(tf_xz), "frame"] = np.arange(0, len(tf_xz))
    # df.loc[df["time_stamp"].isin(tf_yz), "frame"] = np.arange(0, len(tf_yz))
    # df = df[df["time_stamp"] >= np.min([t0_vmu_xz, t0_vmu_yz])]
    # df.to_csv(track_csv, index=False)

    dts = (20, 40, 60, 80, 100, 120, 140, 160)
    df = pd.read_csv(track_csv)
    df_ = None
    for i in range(1, 13):
        stage = f"U{i}"
        start, stop = icaps.const.oos_stages[stage]
        if df_ is None:
            df_ = df[(df["frame"] >= start) & (df["frame"] <= stop)].copy()
        else:
            df_ = df_.append(df[(df["frame"] >= start) & (df["frame"] <= stop)].copy())
    df["particle"] = 0
    df.dropna(subset=["x"], inplace=True)
    df_disp = icaps.brownian.stitch_displacements(df, dts, add_leaps=False, time_column="time_stamp",
                                                  in_columns=["x", "z"], out_columns="d{}", silent=False)
    df_disp.to_csv(disp_csv, index=False)
    # icaps.brownian.plot_stitched_displacements(df_disp, dts, "x", "dx", silent=False, sub_folder=None,
    #                                            time_column="time_stamp",
    #                                            plot_path=plot_path + "disp_trans_x/", clear_out=False)
    # icaps.brownian.plot_stitched_displacements(df_disp, dts, "y", "dy", silent=False, sub_folder=None,
    #                                            time_column="time_stamp",
    #                                            plot_path=plot_path + "disp_trans_y/", clear_out=False)
    # icaps.brownian.plot_stitched_displacements(df_disp, dts, "z", "dz", silent=False, sub_folder=None,
    #                                            time_column="time_stamp",
    #                                            plot_path=plot_path + "disp_trans_z/", clear_out=False)


if __name__ == '__main__':
    main()

