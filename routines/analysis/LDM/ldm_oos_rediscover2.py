import cv2
import numpy as np
import pandas as pd
from scipy import ndimage
import icaps
import trackpy as tp
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# project_path = drive_letter + ":/icaps/data/OOS_YZ/"
# oos_path = project_path + "orig_strip/"
oos_path = drive_letter + ":/icaps/data/OOS_YZ/orig_strip/"
ldm_path1 = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_001/"
ldm_path2 = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_002/"
project_path = drive_letter + ":/icaps/data/LDM_OOS_Match/"
out_path = project_path
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.npy"
wacca_mark_path = out_path + "wacca/"
track_long_path = out_path + "Tracks_all.csv"
track_ldm_path = out_path + "Tracks_short.csv"
track_oos_path = out_path + "wacca.csv"

# (oos*1000/50) - 93 ms = ldm

particles = (445561, 446641, 465577, 672851, 681000, 666400, 744260, 747039, 847467, 851862, 880713, 849901, 862754,
             903408)
mark_frames = (20940, 54400, 56260, 63740, 74180, 80440)

oos_dim = (1024, 550, 11.9, 50)
ldm_dim = (1024, 1024, 1.0, 1000)
t_off = -93
x_off = -12
y_off = -12

phi = 39 * np.pi/180
theta = 90 - 39
ldm_h = int(round(ldm_dim[1] * (2 - np.cos(phi))))
edge_x = int(round(ldm_dim[0]/oos_dim[2]))
edge_y = int(round(ldm_h/oos_dim[2]))


# def draw_rect(img_oos):
#     height, width = img_oos.shape[:2]
#     img_oos_rect = icaps.draw_rect(img_oos,
#                                    (int(width / 2 - edge_x / 2) + x_off, int(height / 2 - edge_y / 2) + y_off),
#                                    (edge_x, edge_y), wh=True)
#     return img_oos_rect


def draw_rectangle(image, center, angle, width, height, color=(255, 0, 0), thickness=1):
    angle = np.radians(angle)
    c, s = np.cos(angle), np.sin(angle)
    rot = np.array([[c, -s], [s, c]])
    p1 = [+width / 2,  +height / 2]
    p2 = [-width / 2,  +height / 2]
    p3 = [-width / 2, -height / 2]
    p4 = [+width / 2,  -height / 2]
    p1_new = np.dot(p1, rot) + center
    p2_new = np.dot(p2, rot) + center
    p3_new = np.dot(p3, rot) + center
    p4_new = np.dot(p4, rot) + center
    img = cv2.line(image, (int(p1_new[0]), int(p1_new[1])), (int(p2_new[0]), int(p2_new[1])), color, thickness)
    img = cv2.line(img, (int(p2_new[0]), int(p2_new[1])), (int(p3_new[0]), int(p3_new[1])), color, thickness)
    img = cv2.line(img, (int(p3_new[0]), int(p3_new[1])), (int(p4_new[0]), int(p4_new[1])), color, thickness)
    img = cv2.line(img, (int(p4_new[0]), int(p4_new[1])), (int(p1_new[0]), int(p1_new[1])), color, thickness)
    # img = cv2.line(img, (int(p2_new[0]), int(p2_new[1])), (int(p4_new[0]), int(p4_new[1])), color, thickness)
    # img = cv2.line(img, (int(p1_new[0]), int(p1_new[1])), (int(p3_new[0]), int(p3_new[1])), color, thickness)

    return img


def crop_oos(img, f=(0.5, 0.5)):
    h, w = img.shape
    img = icaps.crop(img, (int(f[0]/2*w), int(f[1]/2*h)), (int((1-f[0])*w), int((1-f[1])*h)), wh=True)
    return img


def main():
    # df_long = pd.read_csv(track_long_path)
    # df = pd.DataFrame(columns=df_long.columns)
    # for particle in particles:
    #     df_ = df_long[df_long["particle"] == particle]
    #     df = df.append(df_)
    # df.to_csv(track_path, index=False)

    # icaps.mk_folder(wacca_mark_path)

    """
    df_ldm = pd.read_csv(track_ldm_path)
    # df_oos = pd.DataFrame(columns=["frame", "label", "x", "y", "area", "mass", "raw_mass", "gyrad", "n_sat",
    #                                "signal", "bx", "by", "bw", "bh", "tier"])
    df_oos = pd.read_csv(track_oos_path)

    frames = np.unique(df_ldm["frame"].to_numpy())
    frames = frames[(frames + 93) % 20 == 0]
    pb = icaps.ProgressBar(len(frames), "Progress")
    frames = np.array([56260, 74180]) - 93
    enlarge = 1
    # frames = (4906*20 - 93,)
    frames = (6365*20-93, 6500*20-93)
    for frame in frames:
        oos_frame = frame + 93
        if oos_frame % 20 == 0:
            oos_frame = int(oos_frame/20)
            file = icaps.generate_file(oos_frame, prefix=0, digits=5)
            img = icaps.load_img(oos_path + file)
            img = icaps.load_img(drive_letter + ":/icaps/data/OOS_YZ/orig_strip/" + file)
            # img = crop_oos(img, f=(0.8, 0.7))

            # df = icaps.wacca(img, frame=frame, thresh1=13, thresh2=100)
            df = df_oos[df_oos["frame"] == frame]

            mark_img = icaps.enlarge_pixels(img, enlarge)
            mark_img, color = icaps.apply_color(mark_img, (0, 0, 0))
            df0 = df[df["tier"] == 0].copy()
            # mark_img = icaps.draw_labelled_bbox_bulk(mark_img, df0[["bx", "by", "bw", "bh"]].to_numpy()*enlarge,
            #                                          labels=df0["label"].to_numpy(), color=(255, 0, 0))
            df1 = df[df["tier"] == 1].copy()
            # mark_img = icaps.draw_labelled_bbox_bulk(mark_img, df1[["bx", "by", "bw", "bh"]].to_numpy()*enlarge,
            #                                          labels=df1["label"].to_numpy(), color=(0, 0, 255))
            h, w = mark_img.shape[:2]
            # mark_img = draw_rectangle(mark_img, (int(w / 2) - 16*enlarge, int(h / 2)+3*enlarge), 90 - 39, int(1024),
            #                           int(1024 * (2 - np.cos(phi))), color=(0, 255, 0), thickness=1)
            mark_img = draw_rectangle(mark_img, (int(w / 2) - 16 * enlarge, int(h / 2) + 3 * enlarge), 90 - 39,
                                      int(1024/12),
                                      int(1024/12 * (2 - np.cos(phi))), color=(0, 255, 0), thickness=3)
            print(oos_frame)
            # if oos_frame == 4906:
            #     icaps.cvshow(mark_img)
            #     icaps.save_img(mark_img, "F:/Desktop/out.png")
            icaps.save_img(mark_img, wacca_mark_path + icaps.generate_file(frame + 93, prefix=None))
            # df_oos = df_oos.append(df, ignore_index=True, sort=False)
        pb.tick()
    pb.finish()
    # df_oos.to_csv(track_oos_path)
    """
    # df_oos = pd.read_csv(track_oos_path)
    # df_oos = icaps.link(df_oos, search_range=5, memory=3)
    # df_oos.to_csv(out_path + "wacca_link.csv")
    # df_oos = pd.read_csv(out_path + "wacca_link.csv")
    # df_oos = pd.read_csv(track_oos_path)
    # df_oos = df_oos[df_oos["frame"] == 80440 - 93]
    # df_oos = df_oos[df_oos["label"] == 46]
    # print(df_oos)
    # tp.plot_traj(df_oos)

    df_ldm = pd.read_csv(track_ldm_path)
    df_oos = pd.read_csv(out_path + "wacca_link.csv")
    df_match = pd.read_excel(out_path + "ldm_oos_match.ods", engine="odf")
    ldm_ids = df_match["ldm track id"].to_numpy()
    oos_ids = df_match["oos track id"].to_numpy()
    ldm_mass_max = np.zeros(len(ldm_ids))
    oos_mass_max = np.zeros(len(oos_ids))
    ldm_mass_mean = np.zeros(len(ldm_ids))
    oos_mass_mean = np.zeros(len(oos_ids))
    for i, oos_id in enumerate(oos_ids):
        track_ldm = df_ldm[df_ldm["particle"] == ldm_ids[i]].copy()
        track_oos = df_oos[df_oos["particle"] == oos_ids[i]].copy()
        ldm_mass_max[i] = np.max(track_ldm["mass"].to_numpy())
        oos_mass_max[i] = np.max(track_oos["mass"].to_numpy())
        ldm_mass_mean[i] = np.mean(track_ldm["mass"].to_numpy())
        oos_mass_mean[i] = np.mean(track_oos["mass"].to_numpy())

    ldm_mass_max = ldm_mass_max[ldm_ids != 445561]
    oos_mass_max = oos_mass_max[ldm_ids != 445561]
    ldm_mass_mean = ldm_mass_mean[ldm_ids != 445561]
    oos_mass_mean = oos_mass_mean[ldm_ids != 445561]
    oos_ids = oos_ids[ldm_ids != 445561]
    ldm_ids = ldm_ids[ldm_ids != 445561]

    _, ax = plt.subplots()
    ax.scatter(ldm_mass_mean, oos_mass_mean, color="black", s=10)
    for i, txt in enumerate(ldm_ids.astype("<U21")):
        ax.annotate(txt, (ldm_mass_mean[i], oos_mass_mean[i]), fontsize=6)
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Mean OOS Intensity")
    ax.set_xlabel("Mean LDM Intensity")

    popt, pcov = curve_fit(icaps.fit_lin0, ldm_mass_mean, oos_mass_mean)
    perr = np.sqrt(np.diag(pcov))
    xlim = ax.get_xlim()
    xspace = np.linspace(xlim[0], xlim[1], 1000)
    ax.plot(xspace, icaps.fit_lin0(xspace, *popt), color="red", label="y = ax\n"
                                                                      + "a = " + str(np.around(popt[0], 3)) + " \u00b1 "
                                                                      + str(np.around(perr[0], 3)))
    ax.legend()
    plt.savefig(out_path + "corr_int.png")
    plt.close()

    _, ax = plt.subplots()
    ldm_mass_mean = ldm_mass_mean/306
    ax.scatter(ldm_mass_mean, oos_mass_mean, color="black", s=10)
    for i, txt in enumerate(ldm_ids.astype("<U21")):
        ax.annotate(txt, (ldm_mass_mean[i], oos_mass_mean[i]), fontsize=6)
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Mean OOS Intensity")
    ax.set_xlabel("Mean LDM Mass (from Intensity) in Units of Monomer Mass")
    popt, pcov = curve_fit(icaps.fit_lin0, ldm_mass_mean, oos_mass_mean)
    perr = np.sqrt(np.diag(pcov))
    xlim = ax.get_xlim()
    xspace = np.linspace(xlim[0], xlim[1], 1000)
    ax.plot(xspace, popt[0] * xspace, color="red", label="y = ax\n"
                                                         + "a = " + str(np.around(popt[0], 3)) + " \u00b1 "
                                                         + str(np.around(perr[0], 3)))
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    ax.legend()
    plt.savefig(out_path + "corr_mass.png")


if __name__ == '__main__':
    main()

