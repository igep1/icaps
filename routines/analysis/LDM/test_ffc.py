import numpy as np
import icaps
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.npy"


def main():
    frame = 127500
    texus_path = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame)
    img = icaps.load_img(texus_path + icaps.generate_file(frame), strip=True)
    ff = icaps.load_img(ff_path)
    _, ax = plt.subplots()
    ax.imshow(img, cmap=plt.get_cmap("binary_r"))
    img_1 = ff - img
    _, ax = plt.subplots()
    ax.imshow(img_1, vmin=0, vmax=255, cmap=plt.get_cmap("binary_r"))
    img_2 = 255 - img * np.mean(ff)/ff
    _, ax = plt.subplots()
    ax.imshow(img_2, vmin=0, vmax=255, cmap=plt.get_cmap("binary_r"))
    # _, ax = plt.subplots()
    # ax.imshow(img_2 - img_1, cmap=plt.get_cmap("plasma"))
    # plt.colorbar(plt.cm.ScalarMappable(norm=plt.Normalize(vmin=np.min(img_2 - img_1),
    #                                                       vmax=np.max(img_2 - img_1)),
    #                                    cmap=plt.get_cmap("plasma")), label="Gray Value")
    plt.show()


if __name__ == '__main__':
    main()

