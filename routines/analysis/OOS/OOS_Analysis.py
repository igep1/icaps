import icaps
import numpy as np
import pandas as pd
import scipy.ndimage

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "XZ"
# plane = "YZ"
project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/"
orig_path = project_path + "orig_strip/"
plot_path = project_path + "plots/"
stages_path = project_path + "stages/"
volume_mask = project_path + "volume/mask_const.bmp"
csv_track = project_path + f"OOS_{plane}_U1-7.csv"
csv_masked = ".".join(csv_track.split(".")[:-1]) + "_masked.csv"
icaps.mk_folders([stages_path, plot_path])


def main():
    # ===================================================================================
    # MASKING
    # ===================================================================================
    mask_img = icaps.load_img(volume_mask)
    c_img = scipy.ndimage.convolve(mask_img, np.ones((3, 3)), mode="constant")
    c_img = c_img > 252
    my, mx = np.argwhere(c_img).transpose()

    df = pd.read_csv(csv_track)
    df["in_mask"] = 0
    df = df[df["bx"] <= np.max(mx)]
    df = df[df["bx"] + df["bw"] >= np.min(mx)]
    myu = np.unique(my)
    pb = icaps.SimpleProgressBar(len(myu), "Matching BBoxes")
    for my_ in myu:
        idx = (df["by"] == my_) | (df["by"] + df["bh"] == my_)
        df_ = df[idx]
        mx_ = mx[my == my_]
        inside = (mx_[0] <= df_["bx"]) & (df_["bx"] <= mx_[1])
        inside |= (mx_[0] <= df_["bx"] + df_["bw"]) & (df_["bx"] + df_["bw"] <= mx_[1])
        df.loc[idx, "in_mask"] = inside
        pb.tick()
    pb.close()
    df = df[df["in_mask"] == 1]
    df = df.drop(columns=["in_mask"])
    df.to_csv(csv_masked, index=False)

    # """
    # ===========================
    # Mark Examples
    df = pd.read_csv(csv_masked)
    frames = [1000, 2000, 3000, 4000, 5000, 6000, 7000]
    df = df[df["frame"].isin(frames)]
    pb = icaps.SimpleProgressBar(len(df), "Marking Examples")
    for frame in frames:
        pb.set_stage(frame)
        mark_img = icaps.load_img(orig_path + icaps.generate_file(frame, prefix=None, digits=5))
        mark_img, _ = icaps.apply_color(mark_img, (0, 0, 0))
        mark_img[c_img, 0] = 0
        mark_img[c_img, 1] = 255
        mark_img[c_img, 2] = 0
        df_ = df[df["frame"] == frame]
        for i, row in df_.iterrows():
            color = (0, 0, 255) if row["tier"] == 1 else (255, 0, 0)
            mark_img = icaps.draw_labelled_bbox(mark_img, row[["bx", "by", "bw", "bh"]], color=color)
            pb.tick()
        icaps.save_img(mark_img, plot_path + "marked_{:05d}.bmp".format(frame))
    pb.close()
    # """


if __name__ == '__main__':
    main()


