import icaps
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
texus_path = icaps.get_texus_path(drive_letter + ":/icaps/", camera="OOS_XZ")
project_path = drive_letter + ":/icaps/data/OOS_Growth/background/"
plot_path = project_path
orig_path = drive_letter + ":/icaps/data/OOS_Growth/{}/orig/"
orig_strip_path = drive_letter + ":/icaps/data/OOS_Growth/{}/orig_strip/"
ff_start = 11035
ff_stop = 13000
px_timelines = False
planes = [
    # "XZ",
    "XZ_mod",
    # "YZ"
]
strips = [
    True,
    # False
]

icaps.mk_folders([project_path, plot_path])
dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    bins_uint8 = np.arange(-0.5, 256, 1)
    for strip in strips:
        for plane in planes:
            mod = "mod" in plane
            if mod and not strip:
                continue
            plane = plane[:2]
            title = f"{plane}{'_strip' if strip else ''}{'_mod' if mod else ''}"
            print(title)
            plot_path_ = plot_path + f"{title}/"
            icaps.mk_folder(plot_path_)
            in_path = orig_strip_path.format(plane) if strip else orig_path.format(plane)
            print(f"\rLoading {ff_stop - ff_start + 1} images...", end="")
            imgs = np.array([icaps.load_img(in_path + file) for file in icaps.generate_files(ff_start, ff_stop,
                                                                                             prefix=None, digits=5)])
            print(f"\rLoading {ff_stop - ff_start + 1} images \u2713")
            ff = np.sum(imgs, axis=0) / len(imgs)
            img_shp = ff.shape
            # ff = icaps.average_ff(orig_strip_path.format(plane) if strip else orig_path.format(plane),
            #                       files=icaps.generate_files(ff_start, ff_stop, prefix=None, digits=5), silent=False)
            if mod:
                max_pos = np.argwhere(ff == np.max(ff))[0]
                ff[max_pos[0], max_pos[1]] = 0
                _, ax = plt.subplots()
                ax.errorbar(np.arange(0, ff_stop - ff_start + 1) / icaps.const.fps_oos,
                            imgs[:, max_pos[0], max_pos[1]], fmt=".", c="black", markersize=1)
                ax.set_xlabel("Time (s)")
                ax.set_ylabel("Counts")
                plt.savefig(plot_path_ + "DF_strange_pixel.png", dpi=dpi)
                plt.close()
                if px_timelines:
                    _, ax = plt.subplots()
                    _, ax2 = plt.subplots()
                    ax2.imshow(imgs[0], cmap=plt.get_cmap("binary_r"))
                    ax.set_xlabel("Time (s)")
                    ax.set_ylabel("Counts")
                    subdiv = 3, 6
                    for j in range(subdiv[0]):
                        y_pos = np.floor(img_shp[0] // subdiv[0] * (j + 0.5)).astype(int)
                        for i in range(subdiv[1]):
                            x_pos = np.floor(img_shp[1] // subdiv[1] * (i + 0.5)).astype(int)
                            ax.plot(np.arange(0, ff_stop - ff_start + 1) / icaps.const.fps_oos,
                                    imgs[:, y_pos, x_pos], c="black", ls="-")
                            ax2.scatter(x_pos, y_pos, c="red", marker="X", s=3)
                    plt.show()
                imgs[:, max_pos[0], max_pos[1]] = 0
            print("Min:", "{:.3f}".format(np.min(ff)),
                  "Max:", "{:.3f}".format(np.max(ff)),
                  "Mean:", "{:.3f}".format(np.mean(ff)),
                  "Median:", "{:.3f}".format(np.median(ff)))
            np.save(plot_path_ + f"DF_{title}_{ff_start}_{ff_stop}.npy", ff)
            icaps.save_img(np.round(ff).astype(np.uint8),
                           plot_path_ + f"DF_{title}_{ff_start}_{ff_stop}.bmp")
            _, ax = plt.subplots()
            cmap = plt.get_cmap("binary_r")
            ax.set_xlabel(r"$x \,\rm (px)$")
            ax.set_ylabel(r"$y \,\rm (px)$")
            plt.imshow(ff, cmap=cmap)
            plt.colorbar(plt.cm.ScalarMappable(norm=plt.Normalize(vmin=np.min(ff),
                                                                  vmax=np.max(ff)),
                                               cmap=cmap), label="Gray Value")
            plt.tight_layout()
            plt.savefig(plot_path_ + f"DF_{title}_{ff_start}_{ff_stop}.png", dpi=dpi)
            plt.close()

            _, ax = plt.subplots()
            ax.set_xlabel("Counts")
            ax.set_ylabel("Frequency")
            hist, _ = np.histogram(ff, bins=bins_uint8)
            ax.bar((bins_uint8[:-1] + 0.5), hist, width=0.9, color="black")
            ax.set_yscale("log")
            ax.set_xlim(-0.5, np.min([25.5, np.max(hist) + 0.5]))
            plt.tight_layout()
            plt.savefig(plot_path_ + f"DF_Hist_{title}_{ff_start}_{ff_stop}.png", dpi=dpi)
            plt.close()

            _, ax = plt.subplots()
            ax.set_xlabel("Counts")
            ax.set_ylabel("Normalized Frequency")
            ff_flat = np.sort(ff.flatten())
            ff_flat_norm = np.linspace(1 / len(ff_flat), 1, len(ff_flat))
            ax.errorbar(ff_flat, ff_flat_norm, fmt=".", markersize=1, color="black")
            ax.set_xlim(-0.5, np.min([25.5, np.max(ff_flat) + 0.5]))
            plt.tight_layout()
            plt.savefig(plot_path_ + f"DF_Dist_{title}_{ff_start}_{ff_stop}.png", dpi=dpi)
            plt.close()

            _, ax = plt.subplots(nrows=2)
            plt.subplots_adjust(hspace=0, wspace=0)
            ff_half_max = 0
            ax[1].set_xlabel("Counts")
            for i in range(2):
                ax[i].set_ylabel("Normalized Frequency")
                ff_half = ff[:, :img_shp[1] // 2 if i == 0 else img_shp[1] // 2:]
                ff_flat = np.sort(ff_half.flatten())
                ff_flat_norm = np.linspace(1 / len(ff_flat), 1, len(ff_flat))
                ax[i].errorbar(ff_flat, ff_flat_norm, fmt=".", markersize=1, color="black")
                ax[i].annotate("Left" if i == 0 else "Right", xy=(0.983, 0.05), xycoords="axes fraction",
                               size=15, ha="right", va="bottom", bbox=dict(boxstyle="square", fc="w"))
                ff_half_max = np.max([ff_half_max, np.max(ff_flat)])
            for i in range(2):
                ax[i].set_xlim(-0.5, np.min([25.5, ff_half_max + 0.5]))
            plt.savefig(plot_path_ + f"DF_Dist_LR_{title}_{ff_start}_{ff_stop}.png", dpi=dpi)
            plt.close()

            _, ax = plt.subplots(nrows=2)
            plt.subplots_adjust(hspace=0, wspace=0)
            ff_half_max = 0
            ax[1].set_xlabel("Counts")
            for i in range(2):
                ax[i].set_ylabel("Normalized Frequency")
                ff_half = ff[:img_shp[0] // 2 if i == 0 else img_shp[0] // 2:, :]
                ff_flat = np.sort(ff_half.flatten())
                ff_flat_norm = np.linspace(1 / len(ff_flat), 1, len(ff_flat))
                ax[i].errorbar(ff_flat, ff_flat_norm, fmt=".", markersize=1, color="black")
                ax[i].annotate("Top" if i == 0 else "Bottom", xy=(0.983, 0.05), xycoords="axes fraction",
                               size=15, ha="right", va="bottom", bbox=dict(boxstyle="square", fc="w"))
                ff_half_max = np.max([ff_half_max, np.max(ff_flat)])
            for i in range(2):
                ax[i].set_xlim(-0.5, np.min([25.5, ff_half_max + 0.5]))
            plt.savefig(plot_path_ + f"DF_Dist_TB_{title}_{ff_start}_{ff_stop}.png", dpi=dpi)
            plt.close()

            _, ax = plt.subplots()
            ax.set_xlabel("Counts")
            ax.set_ylabel("Normalized Frequency")
            norm = np.linspace(1 / len(imgs[0].flatten()), 1, len(imgs[0].flatten()))
            subdiv = 10
            max_val = 0
            ax.set_title(f"{len(imgs)} frames, {subdiv} subdivisions averaged over {len(imgs) // subdiv} frames each")
            for i in range(subdiv):
                j_start = len(imgs) // subdiv * i
                j_stop = len(imgs) // subdiv * (i + 1)
                # print(j_start, j_stop)
                img_flat = np.sort((np.sum(imgs[j_start: j_stop], axis=0) / (j_stop - j_start)).flatten())
                ax.errorbar(img_flat, norm, fmt=".", markersize=1, color=plt.get_cmap("tab10")(i))
                max_val = np.max([max_val, np.max(img_flat)])
            ax.set_xlim(-0.5, np.min([25.5, max_val + 0.5]))
            plt.savefig(plot_path_ + f"DF_Dist_Temp_{title}_{ff_start}_{ff_stop}.png", dpi=dpi)
            plt.close()


if __name__ == '__main__':
    main()

