import icaps
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# plane = "XZ"
plane = "YZ"
project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/"
orig_path = project_path + "orig_strip/"
out_path = project_path + "volume/"
# frame = 6270
frame = 7000
xoff = 350
if plane == "YZ":
    xoff += 100


def main():
    img = icaps.load_img(orig_path + icaps.generate_file(frame, prefix=None, digits=5))
    rect = icaps.rectangle((xoff, 220), (20, 150), wh=True)
    rect = icaps.scale_points(rect, 5, decimals=0)
    rect = icaps.scale_points(rect, [3, 1], decimals=0)
    img_m = icaps.draw_rect(img, rect, color=(255, 0, 0))
    rect = icaps.rotate_points(rect, 15, decimals=0)
    img_m = icaps.draw_rect(img_m, rect, color=(0, 255, 0))
    icaps.cvshow(img_m)
    mask = np.zeros(img.shape[:2], dtype=np.uint8)
    mask = icaps.draw_rect(mask, rect, color=255, thickness=-1)
    print(np.count_nonzero(mask))
    icaps.save_img(mask, out_path + "mask_const.bmp")


if __name__ == '__main__':
    main()


