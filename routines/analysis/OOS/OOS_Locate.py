import icaps
import pandas as pd
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# plane = "XZ"
plane = "YZ"
project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/"
orig_path = project_path + "orig_strip/"
plot_path = project_path + "plots/"
stages = [
    "E1",
    "E2",
    "E3",
    # "U1",
    # "U2",
    # "U3",
    # "U4",
    # "U5",
    # "U6",
    # "U7"
]
temp_batchsize = 100
mark = True
stages_path = project_path + "stages/"
volume_mask = project_path + "volume/mask_const.bmp"
df_rep = None
for stage in stages:
    try:
        df_rep_ = pd.DataFrame(data={"stage_type": [stage[0]], "stage_index": [int(float(stage[1]))]})
    except ValueError:
        df_rep_ = pd.DataFrame(data={"stage_type": [stage], "stage_index": [0]})
    if df_rep is None:
        df_rep = df_rep_
    else:
        df_rep = df_rep.append(df_rep_, ignore_index=True, sort=False)
rep = ""
for stage_type in df_rep["stage_type"].unique():
    idcs = df_rep.loc[df_rep["stage_type"] == stage_type, "stage_index"].to_numpy()
    rep += "_{}{}-{}".format(stage_type, np.min(idcs), np.max(idcs))
csv_loc = project_path + "OOS_{}{}.csv".format(plane, rep)
csv_track = ".".join(csv_loc.split(".")[:-1]) + "_linked.csv"
icaps.mk_folders([stages_path, plot_path])


def main():
    # """
    # ===================================================================================
    # LOCALIZATION (WACCA)
    # ===================================================================================
    for stage in stages:
        start, stop = icaps.const.oos_stages[stage]
        pb = icaps.SimpleProgressBar(stop - start + 1, stage)
        stage_path = stages_path + "{}_{:05d}_{:05d}/".format(stage, start, stop)
        mark_path = stage_path + "mark/"
        temp_path = stage_path + "temp/"
        icaps.mk_folders([stage_path, mark_path, temp_path])
        df_temp = None
        ntemp = 0
        temp_start_frame = 0
        mark_path_ = None
        for frame in range(start, stop + 1):
            pb.set_stage(frame)
            file = icaps.generate_file(frame, prefix=None, digits=5)
            img = icaps.load_img(orig_path + file)
            ntemp += 1
            if ntemp == 1:
                temp_start_frame = frame
            if frame == start:
                ntemp = temp_batchsize
            if (ntemp >= temp_batchsize) or (frame == stage):
                mark_path_ = mark_path + file
            df_ = icaps.wacca(img, frame=frame, tbase=15, tseed=100,
                              mark_path=mark_path_, mark_enlarge=2, mark_font_scale=0.5)
            if df_temp is None:
                df_temp = df_
            else:
                df_temp = df_temp.append(df_, ignore_index=True, sort=False)
            if frame == stage:
                mark_path_ = None
            if (ntemp >= temp_batchsize) or (frame == stop):
                mark_path_ = None
                ntemp = 0
                df_temp.to_csv(temp_path + "{:05d}_{:05d}.csv".format(temp_start_frame, frame), index=False)
                df_temp = None
            pb.tick()
        pb.close()
        icaps.merge_dataframes(temp_path, stage_path + "OOS_{}_{}_{:05d}_{:05d}.csv".format(plane, stage, start, stop),
                               silent=False)

    # Merge All
    df = None
    pb = icaps.SimpleProgressBar(len(stages), "Final Merge")
    for stage in stages:
        start, stop = icaps.const.oos_stages[stage]
        stage_path = stages_path + "{}_{:05d}_{:05d}/".format(stage, start, stop)
        df_ = pd.read_csv(stage_path + "OOS_{}_{}_{:05d}_{:05d}.csv".format(plane, stage, start, stop))
        if df is None:
            df = df_
        else:
            df = df.append(df_, ignore_index=True, sort=False)
        pb.tick()
    pb.close()
    df = df.sort_values(by=["frame", "label"])
    df.to_csv(csv_loc, index=False)
    # """

    """
    # ===================================================================================
    # TRACKING
    # ===================================================================================
    df = pd.read_csv(csv_loc)
    df = icaps.link(df, memory=3, search_range=5)
    df.to_csv(csv_track, index=False)
    """


if __name__ == '__main__':
    main()

