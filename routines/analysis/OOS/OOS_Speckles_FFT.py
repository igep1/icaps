import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_EScans/EScan2/"
csv_path = project_path + "OOS_XZ_08500_09000.csv"
dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df = pd.read_csv(csv_path)
    # df = df[df["frame"] < 419]
    # df = df[(df["frame"] >= 419) & (df["frame"] < 469)]
    # df = df[(df["frame"] >= 469) & (df["frame"] < 519)]
    # df = df[(df["frame"] >= 8530) & (df["frame"] < 8630)]
    df = df[(df["frame"] >= 8630) & (df["frame"] < 8730)]
    df = icaps.filter_track_length(df, 100)
    particles = df["particle"].unique()
    _, ax_sum = plt.subplots()
    ax_sum.set_yscale("log")
    ax_sum.set_xscale("log")
    for particle in particles:
        df_ = df[df["particle"] == particle]
        lum = df_["mass"].to_numpy()
        time = np.arange(0, len(df_)) / icaps.const.fps_oos
        # _, ax = plt.subplots()
        # ax.plot(time, lum, c="black")
        # plt.show()
        # from_ = float(input("From:"))
        # to_ = float(input("To:"))
        # lum_ = lum[(time <= to_) & (time >= from_)]
        lum_ = lum
        yf = fft(lum_)
        n = len(lum_)
        xf = fftfreq(n, 1 / icaps.const.fps_oos)[:n // 2]
        yfp = 1 / (n * icaps.const.fps_oos) * (np.abs(yf[0:n // 2])) ** 2
        # _, ax = plt.subplots()
        # ax.plot(time, lum, c="black")
        # ax.axvline(from_, c="red")
        # ax.axvline(to_, c="red")
        # _, ax2 = plt.subplots()
        # ax_sum.plot(xf, yfp, c="black", lw=2, alpha=1)
        ax_sum.scatter(xf, yfp, c="black", marker=".", s=1, alpha=1)
        # ax2.set_yscale("log")
        # ax2.set_xscale("log")
        # plt.show()
    plt.show()


if __name__ == '__main__':
    main()


