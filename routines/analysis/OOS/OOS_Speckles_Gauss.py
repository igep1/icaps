import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_Growth/speckles/"
csv_e1 = drive_letter + ":/icaps/data/OOS_EScans/EScan1/OOS_XZ_00300_00700.csv"
csv_e2 = drive_letter + ":/icaps/data/OOS_EScans/EScan2/OOS_XZ_08500_09000.csv"
csv_e3 = drive_letter + ":/icaps/data/OOS_EScans/EScan3/OOS_XZ_13300_13700.csv"
csv_dict = {"E1": csv_e1, "E2": csv_e2, "E3": csv_e3}
stage_params = {"E1": [419, 50], "E2": [8530, 100], "E3": [13424, 50]}
plot_path = project_path + "plots/"
icaps.mk_folder(plot_path)

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    for stage in ["E1", "E2", "E3"]:
        df = pd.read_csv(csv_dict[stage])

        start = stage_params[stage][0]
        step = stage_params[stage][1]
        substages = [[None, start, "0"],
                     [start, start + step, "1"],
                     [start + step, start + 2 * step, "2"],
                     [start + 2 * step, None, "3"]]
        for substage in substages:
            df_ = df.copy()
            if substage[0] is not None:
                df_ = df_[df_["frame"] >= substage[0]]
            if substage[1] is not None:
                df_ = df_[df_["frame"] < substage[1]]
            df_ = icaps.filter_track_length(df_, len(df_["frame"].unique()))
            particles = df_["particle"].unique()
            _, ax = plt.subplots()
            ax.set_xscale("log")
            ax.set_xlabel(r"Intensity / Median Intensity")
            ax.set_ylabel(r"Normalized Cumulative Frequency")
            lum = np.array([])
            if len(particles) == 0:
                continue
            medians = []
            for particle in particles:
                lum_ = df_.loc[df_["particle"] == particle, "mass"].to_numpy()
                medians.append(np.median(lum_))
                lum_ /= np.median(lum_)
                lum_ = np.sort(lum_)
                norm_ = np.linspace(1 / len(lum_), 1, len(lum_))
                ax.errorbar(lum_, norm_, c="gray", fmt="-", lw=1, alpha=0.2)
                lum = np.append(lum, lum_)
            medians = np.array(medians)
            lum = np.sort(lum)
            norm = np.linspace(1 / len(lum), 1, len(lum))
            ax.errorbar(lum, norm, c="black", fmt="-", markersize=1, lw=2, alpha=1)
            ax.set_xlim(4e-3, 2e1)
            out = icaps.fit_model(icaps.fit_erf, lum, norm, p0=[1, 0.4])
            xspace = np.linspace(*ax.get_xlim(), 1000)
            ax.plot(xspace, icaps.fit_erf(xspace, *out.beta), c="red", lw=1, ls="--", zorder=10,
                    label=icaps.get_param_label([[r"$\mu$", out.beta[0], out.sd_beta[0]],
                                                 [r"$\sigma$", out.beta[1], out.sd_beta[1]]]))
            ax.legend(fancybox=False)
            ax.set_title(f"{stage}-{substage[2]}, particles: {len(particles)}, "
                         f"tracklength: {len(df_['frame'].unique())}, " +
                         r"$\mu$: {:.2f}, $\sigma$: {:.2f}".format(np.mean(lum), np.std(lum)))
            plt.savefig(plot_path + f"{stage}_{substage[2]}.png", dpi=dpi)
            plt.close()

            sub_plot_path = plot_path + "median_filter/"
            icaps.mk_folder(sub_plot_path)
            particles1 = particles[medians <= 1 * np.median(medians)]
            particles2 = particles[medians > 1 * np.median(medians)]
            print(particles1)
            print(particles2)
            _, ax = plt.subplots()
            ax.set_xscale("log")
            ax.set_xlabel(r"Intensity / Median Intensity")
            ax.set_ylabel(r"Normalized Cumulative Frequency")
            for particle in particles1:
                lum_ = df_.loc[df_["particle"] == particle, "mass"].to_numpy()
                lum_ /= np.median(lum_)
                lum_ = np.sort(lum_)
                norm_ = np.linspace(1 / len(lum_), 1, len(lum_))
                ax.errorbar(lum_, norm_, c="gray", fmt="-", lw=1, alpha=0.2)
                lum = np.append(lum, lum_)
            lum = np.sort(lum)
            norm = np.linspace(1 / len(lum), 1, len(lum))
            ax.errorbar(lum, norm, c="black", fmt="-", markersize=1, lw=2, alpha=1)
            ax.set_xlim(4e-3, 2e1)
            ax.set_title(f"{stage}-{substage[2]}, particles: {len(particles1)}, "
                         f"tracklength: {len(df_['frame'].unique())}, " +
                         r"$\mu$: {:.2f}, $\sigma$: {:.2f}".format(np.mean(lum), np.std(lum)))
            plt.savefig(sub_plot_path + f"{stage}_{substage[2]}_small.png", dpi=dpi)
            plt.close()

            _, ax = plt.subplots()
            ax.set_xscale("log")
            ax.set_xlabel(r"Intensity / Median Intensity")
            ax.set_ylabel(r"Normalized Cumulative Frequency")
            for particle in particles2:
                lum_ = df_.loc[df_["particle"] == particle, "mass"].to_numpy()
                lum_ /= np.median(lum_)
                lum_ = np.sort(lum_)
                norm_ = np.linspace(1 / len(lum_), 1, len(lum_))
                ax.errorbar(lum_, norm_, c="gray", fmt="-", lw=1, alpha=0.2)
                lum = np.append(lum, lum_)
            lum = np.sort(lum)
            norm = np.linspace(1 / len(lum), 1, len(lum))
            ax.errorbar(lum, norm, c="black", fmt="-", markersize=1, lw=2, alpha=1)
            ax.set_xlim(4e-3, 2e1)
            ax.set_title(f"{stage}-{substage[2]}, particles: {len(particles2)}, "
                         f"tracklength: {len(df_['frame'].unique())}, " +
                         r"$\mu$: {:.2f}, $\sigma$: {:.2f}".format(np.mean(lum), np.std(lum)))
            plt.savefig(sub_plot_path + f"{stage}_{substage[2]}_big.png", dpi=dpi)
            plt.close()


if __name__ == '__main__':
    main()

