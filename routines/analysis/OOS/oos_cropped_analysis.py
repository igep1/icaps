import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# plane = "XZ"
# tl = (300, 300)
# br = (470, 470)
plane = "YZ"
tl = (420, 0)
br = (600, 200)
project_path = drive_letter + ":/icaps/data/OOS_{}/".format(plane)
out_path = project_path + "crop_analysis/"
orig_path = project_path + "orig_strip/"
mark_path = out_path + "mark/"
csv_path = out_path + "data.csv"
mark = True
fps = 50
px_size = 11.9
laser_depth = 891


def analyze():
    icaps.mk_folder(out_path)
    if mark:
        icaps.mk_folder(mark_path, clear=True)
    files = icaps.get_files(orig_path)
    files = files[:7400]
    df = pd.DataFrame(data={"frame": np.zeros(len(files), dtype=int),
                            "intensity": np.zeros(len(files), dtype=int)})
    pb = icaps.ProgressBar(len(files))
    for i, file in enumerate(files):
        frame = icaps.get_frame(file, prefix=None)
        img = icaps.load_img(orig_path + file)
        img_crop = icaps.crop(img, tl, br)
        df.loc[i, "frame"] = frame
        df.loc[i, "intensity"] = np.sum(img_crop)
        if mark:
            img = icaps.draw_rect(img, tl, br, color=(255, 0, 0), thickness=3)
            icaps.save_img(img, mark_path + file)
        pb.tick()
    pb.finish()
    df.to_csv(csv_path, index=False)


def plot():
    df = pd.read_csv(csv_path)
    _, ax = plt.subplots()
    ax.scatter(df["frame"] / fps, df["intensity"], color="black", s=1)
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Intensity of Cropped Area")
    ax.set_xlabel("Time in s")
    plt.savefig(out_path + "plot.png")
    plt.close()


def comparative_plot():
    df1 = pd.read_csv(drive_letter + ":/icaps/data/OOS_XZ/crop_analysis/data.csv")
    df2 = pd.read_csv(drive_letter + ":/icaps/data/OOS_YZ/crop_analysis/data.csv")
    volume1 = 170 * 170 * px_size ** 2 * laser_depth
    volume2 = 220 * 200 * px_size ** 2 * laser_depth
    _, ax = plt.subplots()
    ax.scatter(df1["frame"] / fps, df1["intensity"]/volume1*10**3, color="black", s=1, label="XZ")
    ax.scatter(df2["frame"] / fps, df2["intensity"]/volume2*10**3, color="gray", s=1, label="YZ")
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel(r"Brightness Density of Cropped Area ($10^{-3} \mu m^{-3}$)")
    ax.set_xlabel("Time (s)")
    ax.legend()
    arr = (df1["intensity"]/volume1).to_numpy()
    arr = np.append(arr, (df2["intensity"]/volume2).to_numpy())
    arr *= 10**3
    ax.set_ylim(0, 1.05*np.max(arr))
    plt.tight_layout()
    plt.savefig(out_path + "plot.png")
    # plt.close()
    plt.show()


def main():
    # analyze()
    # plot()
    # icaps.mk_movie(mark_path, out_path + "movie.mp4", fps=50)
    comparative_plot()


if __name__ == '__main__':
    main()

