import icaps
from scipy.ndimage.morphology import distance_transform_edt
import numpy as np
import cv2
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_XZ/"
strip_path = project_path + "orig_strip/"
out_path = project_path + "volume/"
fps = 50
px_size = 11.9
laser_depth = 891


def main():
    icaps.mk_folder(out_path)
    files = icaps.get_files(strip_path)
    files = files[:8801]
    df = pd.DataFrame(columns=["frame", "cloud_area", "cloud_intensity", "cloud_volume"])
    pb = icaps.ProgressBar(len(files), "Analysing Active Cloud Volume")
    for file in files:
        frame = icaps.get_frame(file, prefix=None)
        img = icaps.load_img(strip_path + file)
        thr = icaps.threshold(img, 13, replace=(0, 255))
        kernel = np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], dtype=np.uint8)
        dil = cv2.dilate(thr, kernel, iterations=7)
        _, labels, stats, _ = cv2.connectedComponentsWithStats(dil, 4, cv2.CV_16U)
        idx = np.argmax(stats[1:, 4]) + 1
        mask = np.zeros(dil.shape, dtype=np.uint8)
        mask[labels == idx] = 255
        mask = icaps.fill_holes(mask)
        res = img.copy()
        res[mask == 0] = 0
        df = df.append(pd.DataFrame(data={"frame": [frame],
                                          "cloud_area": [np.count_nonzero(mask)],
                                          "cloud_intensity": [np.sum(res)],
                                          "cloud_volume": [np.count_nonzero(mask)*px_size**2*laser_depth]}))
        conts, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        for i, cont in enumerate(conts):
            cv2.drawContours(img, conts, contourIdx=i, color=100, thickness=1)
        icaps.save_img(img, out_path + "contour/" + file)
        icaps.save_img(mask, out_path + "mask/" + file)
        pb.tick()
    pb.finish()
    df.to_csv(out_path + "cloud_data.csv", index=False)


def plot_volume():
    df = pd.read_csv(out_path + "cloud_data.csv")
    _, ax = plt.subplots()
    ax.scatter(df["frame"] / fps, df["cloud_area"] * px_size ** 2, color="black", s=1)
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Active Cloud Area in µm²")
    ax.set_xlabel("Time in s")
    plt.savefig(out_path + "Cloud_Area.png")
    plt.close()
    _, ax = plt.subplots()
    ax.scatter(df["frame"]/fps, df["cloud_area"]*px_size**2*laser_depth, color="black", s=1)
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Active Cloud Volume in µm³")
    ax.set_xlabel("Time in s")
    plt.savefig(out_path + "Cloud_Volume.png")
    plt.close()
    _, ax = plt.subplots()
    ax.scatter(df["frame"] / fps, df["cloud_intensity"], color="black", s=1)
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_ylabel("Active Cloud Intensity")
    ax.set_xlabel("Time in s")
    plt.savefig(out_path + "Cloud_Intensity.png")
    # plt.show()


if __name__ == '__main__':
    # main()
    plot_volume()
    # icaps.mk_movie(out_path + "contour/", out_path + "contour.mp4", fps=50)
