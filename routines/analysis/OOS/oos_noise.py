import icaps
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "XZ"
orig_path = drive_letter + ":/icaps/texus56_SRE_20191115/OOS/" + plane + "/"
orig_path = orig_path + ("000000000_10h_11m_09s_936ms/" if plane == "XZ" else "000000000_10h_11m_11s_462ms/")
project_path = drive_letter + ":/icaps/data/OOS/" + plane + "/"
in_path = project_path + "empty_strip/"
# thresh_out_path = project_path + "empty_thresh/"
hist_equal_out_path = project_path + "empty_strip_hist_equal/"
start = 11100
stop = 13000
spec = "(" + str(start) + ":" + str(stop) + ").bmp"
cut = ((750, 0), (1024, 100))
hot_pixel = (491, 314)


def main():
    # icaps.copy_files(orig_path + spec, in_path, silent=False, clear=True)
    # icaps.mk_folder(thresh_out_path, clear=False)
    # icaps.mk_folder(hist_equal_out_path, clear=False)
    bins = np.arange(0, 257, 1)

    # files, _ = icaps.get_files(in_path)
    # hists = None
    # pb = icaps.ProgressBar(len(files), title="Analysing Empty Frame Histograms")
    # for file in files:
    #     img = icaps.load_img(in_path + file)
    #     # img = icaps.draw_rect(img, tl=cut[0], br=cut[1], color=0, thickness=-1)
    #     hist, _ = np.histogram(img.flatten(), bins=bins)
    #     if hists is not None:
    #         hists = np.vstack((hists, hist))
    #     else:
    #         hists = hist
    #     # icaps.save_img(icaps.threshold(img, 1), thresh_out_path + file)
    #     img[hot_pixel[1], hot_pixel[0]] = 0
    #     # icaps.save_img(icaps.equalize_hist(img), hist_equal_out_path + file)
    #     pb.tick()
    # pb.finish()
    # np.save(project_path + "empty_frames.npy", hists)

    hists = np.load(project_path + "empty_frames.npy")
    total_hist = np.sum(hists, axis=0)
    print(total_hist)
    plt.bar((bins[:-1]).astype(int).astype(str), total_hist, align="center", width=1, color="gray")
    plt.gca().set_yscale("log")
    plt.gca().set_xlim(left=-0.5, right=30)
    plt.gca().set_xlabel("Gray Value")
    plt.gca().set_ylabel("Frequency from Frame " + str(start) + " to " + str(stop))
    plt.show()


if __name__ == '__main__':
    main()

