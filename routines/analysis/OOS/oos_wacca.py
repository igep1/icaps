import icaps

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# project_path = drive_letter + ":/icaps/data/OOS_XZ/"
project_path = drive_letter + ":/icaps/data/OOS_EScans/EScan1/"
in_path = project_path + "orig_strip/"
wacca_path = project_path + "wacca/"
show_progress = True
start = 300
stop = 700
step = 1
# use_frames = (1560, 5160)
use_frames = ()
thresh = 13
thresh_seeds = 100
interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)
# out_path = wacca_path + interval_string + "/"
out_path = wacca_path
temp_path = out_path + "wacca_temp/"
# contour_path = out_path + "contours/"
contour_path = None
mark_path = out_path + "mark/"
mask_path = project_path + "volume/mask/"


def main():
    icaps.mk_folder(out_path)
    files = icaps.get_files(in_path + "({start}:{stop}:{step}).bmp".format(start=start, stop=stop, step=step),
                            prefix=None)
    if mark_path is not None:
        icaps.mk_folder(mark_path)
    if contour_path is not None:
        icaps.mk_folder(contour_path)
    icaps.mk_folder(temp_path)

    pb = None
    if show_progress:
        pb = icaps.ProgressBar(len(files), title="Watershed Assisted Connected Component Analysis")
    for file in files:
        frame = icaps.get_frame(file, prefix=None)
        skip = False
        if len(use_frames) > 0:
            if frame not in use_frames:
                skip = True
        if not skip:
            img = icaps.load_img(in_path + file)
            mask = None
            if mask_path is not None:
                mask = icaps.load_img(mask_path + file)
            df = icaps.wacca(img, frame, thresh, thresh_seeds, mask=mask, connectivity=4,
                             mark_path=mark_path + file if mark_path is not None else None,
                             contour_path=contour_path + ".".join(file.split(".")[:-1]) + ".npy"
                             if contour_path is not None else None)
            df.to_csv(temp_path + "{num:0>{fill}}".format(fill=5, num=frame) + ".csv", index=False)
            if show_progress:
                pb.tick()
    if show_progress:
        pb.finish()
    icaps.merge_dataframes(temp_path, out_path + "wacca_" + interval_string + ".csv",
                           delete_source=True)


if __name__ == '__main__':
    main()


