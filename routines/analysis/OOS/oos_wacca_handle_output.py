import os
import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_XZ/wacca/"
csv_out = "wacca_xz_0to4.csv"
# temp_path = out_path + "temp/"


def main():
    # icaps.merge_dataframes(temp_path, out_path + "wacca_" + "00200_01170_001" + ".csv",
    #                        delete_source=False, silent=False)
    df = None
    files = os.listdir(project_path)
    pb = icaps.ProgressBar(5)
    for file in files:
        if (file[0] == "0") and os.path.isdir(project_path + file):
            files_ = os.listdir(project_path + file)
            for file_ in files_:
                if os.path.isfile(project_path + file + "/" + file_) and file_.endswith(".csv"):
                    df_ = pd.read_csv(project_path + file + "/" + file_)
                    if df is None:
                        df = df_
                    else:
                        df = df.append(df_, ignore_index=True, sort=False)
            pb.tick()
    df.to_csv(project_path + csv_out, index=False)


if __name__ == '__main__':
    main()

