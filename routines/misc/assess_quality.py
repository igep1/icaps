import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT/"
particle_path = project_path + "particles/"
csv_path = project_path + "Tracks_PS20L500.csv"


def main():
    files = icaps.get_files(particle_path)
    particles = np.array([int(float(file)) for file in files])
    particles = np.sort(particles)
    df = pd.read_csv(csv_path)
    for particle in particles:
        print(particle)
        df_ = df[df["particle"] == particle]
        max_mass = np.max(df_["mass"].to_numpy())
        frame = df_[df_["mass"] == max_mass]["frame"].to_numpy()[0]
        img = icaps.load_img(particle_path + "{}/{}_{:0>6}.bmp".format(particle, particle, frame))
        img = icaps.enlarge_pixels(img, 8)
        icaps.cvshow(img, t=0, name=str(particle))

        # max_area = np.max(df_["area"].to_numpy())
        # frame = df_[df_["area"] == max_area]["frame"].to_numpy()[0]
        # img = icaps.load_img(particle_path + "{}/{}_{:0>6}.bmp".format(particle, particle, frame))
        # img = icaps.enlarge_pixels(img, 8)
        # icaps.cvshow(img, t=0, name=str(particle))


if __name__ == '__main__':
    main()

