import icaps
import pandas as pd
import matplotlib.pyplot as plt

project = "LDM_ROT5"
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/{}/".format(project)
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
particle_rot_path = project_path + "particles_rot/"
plot_path = project_path + "plots/"
plot_dg_path = project_path + "plots_dg/"
thilo_path = project_path + "thilo/"
thilo_fname = "distribution_values_center_uncertainty_gen_{}.txt"

csv_phases = project_path + "Phases_Hand.csv"
csv_track_all = project_path + "Tracks_all.csv"
csv_track = project_path + "Tracks_PE1L100.csv"
csv_rot_of = project_path + "OF_Rot.csv"
csv_rot_usable = project_path + "usable_rot_OF_particles.txt"
csv_trans_disp = project_path + "DISP_Trans.csv"
csv_trans_erf = project_path + "ERF_Trans.csv"
csv_trans_erf_flagged = project_path + "ERF_Trans_flagged.csv"
csv_trans_of_sg = project_path + "OF_Trans_SG.csv"
csv_final_sg = project_path + "OF_Final_SG.csv"
csv_trans_erf_dg = project_path + "ERF_Trans_DG.csv"
csv_trans_erf_noise = project_path + "ERF_Trans_Noise.csv"
csv_trans_of_dg = project_path + "OF_Trans_DG.csv"
csv_final_dg = project_path + "OF_Final_DG.csv"

df = pd.read_csv(csv_trans_erf_dg)
df_erf = pd.read_csv(csv_trans_erf)
df_of_sg = pd.read_csv(csv_trans_of_sg)
df_of_dg = pd.read_csv(csv_trans_of_dg)
pd.set_option("display.max_columns", None)
pd.set_option("display.max_rows", None)
pd.set_option("display.width", None)
print(df.columns)
print(df_erf.columns)
df = df[df["sigma_type"].isin(["high double", "low double"])]
df = df.drop(columns=["chisqr"])
df = pd.merge(df, df_erf[["particle", "dt", "axis", "chisqr", "chisqr_dg"]], how="left", on=["particle", "dt", "axis"])
print(df.columns)
particles = df["particle"].unique()
df_of_sg = df_of_sg[df_of_sg["particle"].isin(particles)]
df_of_dg = df_of_dg[df_of_dg["particle"].isin(particles)]
df_of = pd.merge(df_of_sg[["particle", "axis", "chisqr"]], df_of_dg[["particle", "axis", "chisqr"]],
                 how="left", on=["particle", "axis"])
print(df_of.columns)

_, ax = plt.subplots()
ax.scatter(df_of["chisqr_x"], df_of["chisqr_y"] / df_of["chisqr_x"], c="black", s=3)
ax.set_xlabel(r"$\chi_{\rm single}^2$")
ax.set_ylabel(r"$\chi_{\rm double}^2 / \chi_{\rm single}^2$")
ax.set_yscale("log")
# ax.plot([0, 1], [0, 1], c="gray", zorder=-1, lw=2)
plt.show()


_, ax = plt.subplots()
ax.scatter(df["chisqr"], df["chisqr_dg"] / df["chisqr"], c="black", s=3)
ax.set_xlabel(r"$\chi_{\rm single}^2$")
ax.set_ylabel(r"$\chi_{\rm double}^2 / \chi_{\rm single}^2$")
# ax.plot([0, 1], [0, 1], c="gray", zorder=-1, lw=2)
plt.show()





