import matplotlib as mpl
import numpy as np
import pandas as pd
from icaps.io import mk_folders, mk_folder
from icaps.processing.tables import move_col
from icaps.analysis.fitting import fit_model, fit_of_log
from icaps.OldProgressBar import ProgressBar
import icaps.const as const
import matplotlib.pyplot as plt
import icaps
from inspect import signature
import os


def calc_ornstein_fuerth_adapt(df_erf, dts=None, axes=None, particles=None, mode="trans",
                               id_column="particle", mass_column="mass",
                               units=None, fit=None, plot=None, dg=None,
                               temp_path=None, temp_interval=0.1, ret_erf=False, silent=True):
    # =====================
    # Parameter Setup
    # =====================
    units_ = {"sigma": "m",
              "time": "s",
              "time_cvt": 1e-3,
              "mass_mag": 14}
    if mode == "rot":
        units_ = icaps.merge_dicts(units_, {"sigma": "rad", "mass_mag": 25})
        if mass_column == "mass":
            mass_column = "inertia"
    mass_err_column = mass_column + "_err"
    units = icaps.merge_dicts(units_, {} if units is None else units)
    fit_ = {"p0": None,
            "maxit": None,
            "fit_type": 0,
            "fit_err": True,
            "bounds": (None, None),
            "adapt_margins": (3, 3),
            "grade_weights": (1., 1.),
            "n_min": 4,
            "quality_type": "all",
            "quantity_type": "count",
            "max_sigsqr": None,
            "max_err_ratio": 1.,
            "grade_type": "gaussian",
            "min_tau": None}
    fit = icaps.merge_dicts(fit_, {} if fit is None else fit)
    plot_ = {"path": None,
             "dpi": 600,
             "clear_out": True,
             "errors": True,
             "attempts": False,
             "crop_to_max": True}
    plot = icaps.merge_dicts(plot_, {} if plot is None else plot)
    dg_ = {"apply": False,
           "min_chi_ratio": 3,
           "max_var": 1,
           "min_weight": 0.2,
           "max_dt": 5}
    dg = icaps.merge_dicts(dg_, {} if dg is None else dg)
    if plot["path"] is not None:
        mpl.use("Agg")
    n_temp = 0
    if id_column is None:
        df_erf["particle"] = 0
        id_column = "particle"
    if particles is None:
        particles = np.unique(df_erf[id_column].to_numpy())
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_raw = dts.copy()
    dts = dts.astype(np.float64) * units["time_cvt"]
    dt_space = np.linspace(np.min(dts), np.max(dts), 1000)
    if not ret_erf:
        df_erf = df_erf[df_erf["dt"].isin(dts_raw)]
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    elif isinstance(axes, str):
        axes = [axes]
    if plot["path"] is None:
        plot["attempts"] = False
    else:
        if "{}" in plot["path"]:
            plot["path"] = [plot["path"].format(axis) for axis in axes]
        else:
            plot["path"] = [plot["path"] for axis in axes]
        mk_folders(plot["path"], clear=plot["clear_out"])
    if fit["p0"] is None:
        fit["p0"] = (None, None)
    fit["p0"] = list(fit["p0"])
    if fit["p0"][0] is None:
        fit["p0"][0] = np.log10(2 * const.k_B * const.T / (const.m_0 * const.r_0 ** 2))
    if fit["p0"][1] is None:
        fit["p0"][1] = 0.001
    nan_arr = np.zeros(len(particles) * len(axes))
    nan_arr[:] = np.nan
    df_out = pd.DataFrame(data={id_column: np.repeat(particles, len(axes)),
                                "axis": np.tile(axes, len(particles)),
                                mass_column: nan_arr,
                                mass_err_column: nan_arr,
                                "tau": nan_arr,
                                "tau_err": nan_arr,
                                "chisqr": nan_arr,
                                "n_dots": np.zeros(len(particles) * len(axes), dtype=int),
                                "t_diff": nan_arr})
    # =====================
    # Start of Loop
    # =====================
    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(axes), "Calculating OF Fits")
    for p, particle in enumerate(particles):
        df_erf_ = df_erf[(df_erf[id_column] == particle) & (df_erf["dt"].isin(dts_raw))]
        for a, axis in enumerate(axes):
            ax = None
            if plot["path"] is not None:
                _, ax = plt.subplots()
                if fit["max_sigsqr"] is not None:
                    ax.axhline(fit["max_sigsqr"], lw=2, ls="--", c="black")
            sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma"].to_numpy())
            sig_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_err"].to_numpy())
            sigsqr = sig**2
            sigsqr_err = 2 * sig_err * sig
            chisqr_erf = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "chisqr"].to_numpy())
            # =====================
            # Adaptive Fitting
            # =====================
            candidates = parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["bounds"],
                                          adapt_margins=fit["adapt_margins"], n_min=fit["n_min"],
                                          max_sigsqr=fit["max_sigsqr"], max_err_ratio=fit["max_err_ratio"])
            nan_arr = np.zeros(len(candidates))
            nan_arr[:] = np.nan
            df_fit = pd.DataFrame(data={
                "beta0": nan_arr,
                "sd_beta0": nan_arr,
                "beta1": nan_arr,
                "sd_beta1": nan_arr,
                "chisqr": nan_arr,
                "quality": nan_arr,
                "quantity": nan_arr
            })
            if not candidates:
                if not silent:
                    pb.tick()
                plt.close()
                continue
            for ci, cidx in enumerate(candidates):
                log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None
                try:
                    out = fit_model(fit_of_log, dts[cidx], np.log10(sigsqr[cidx]),
                                    p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err)
                except RuntimeError:
                    continue
                if out.res_var == 0:
                    continue
                if fit["min_tau"] is not None:
                    if out.beta[1] + out.sd_beta[1] < fit["min_tau"]:
                        continue
                df_fit.loc[ci, "beta0"] = out.beta[0]
                df_fit.loc[ci, "beta1"] = out.beta[1]
                df_fit.loc[ci, "sd_beta0"] = out.sd_beta[0]
                df_fit.loc[ci, "sd_beta1"] = out.sd_beta[1]
                chisqr = np.sum((np.log10(sigsqr[cidx]) - fit_of_log(dts[cidx], *out.beta)) ** 2) / len(cidx)
                df_fit.loc[ci, "chisqr"] = chisqr
                df_fit.loc[ci, "quality"] = 1 / chisqr
                if fit["quantity_type"] == "time":
                    df_fit.loc[ci, "quantity"] = np.max(dts[cidx]) - np.min(dts[cidx])
                else:
                    df_fit.loc[ci, "quantity"] = len(cidx)
                if plot["attempts"] and plot["path"] is not None:
                    ax.plot(dt_space, 10 ** fit_of_log(dt_space, *out.beta),
                            c="gray", lw=1, ls="-.", alpha=0.3)
            if np.max(df_fit["quality"]) == 0:
                if not silent:
                    pb.tick()
                continue
            df_fit["quality"] = df_fit["quality"] / np.max(df_fit["quality"])
            df_fit["quantity"] = df_fit["quantity"] / np.max(df_fit["quantity"])
            if fit["grade_type"] == "linear":
                df_fit["grade"] = df_fit["quality"] * fit["grade_weights"][0]\
                                  + df_fit["quantity"] * fit["grade_weights"][1]
            else:
                df_fit["grade"] = np.sqrt((df_fit["quality"] * fit["grade_weights"][0]) ** 2
                                          + (df_fit["quantity"] * fit["grade_weights"][1]) ** 2)
            best_idx = np.argmax(df_fit["grade"].to_numpy())
            beta = np.squeeze(df_fit.loc[best_idx, ["beta0", "beta1"]])
            sd_beta = np.squeeze(df_fit.loc[best_idx, ["sd_beta0", "sd_beta1"]])
            chisqr = np.squeeze(df_fit.loc[best_idx, "chisqr"])
            cidx = candidates[best_idx]
            mass = 2 * const.k_B * const.T / (10 ** beta[0])
            mass_err = 2 * const.k_B * const.T * np.log(10) / (10 ** beta[0]) * sd_beta[0]
            tau = abs(beta[1])
            tau_err = sd_beta[1]
            t_diff = np.max(dts[cidx]) - np.min(dts[cidx])
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), mass_column] = mass
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), mass_err_column] = mass_err
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau"] = tau
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau_err"] = tau_err
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "chisqr"] = chisqr
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "n_dots"] = len(cidx)
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "t_diff"] = t_diff
            if ret_erf:
                use = np.zeros(len(sigsqr), dtype=bool)
                use[cidx] = True
                df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                           (df_erf["axis"] == axis), "of_used"] = use
            # =====================
            # Plotting
            # =====================
            if plot["path"] is not None:
                if dg["apply"]:
                    sigma_type = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigsqr[sigma_type == "single"],
                                yerr=sigsqr_err[sigma_type == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[sigma_type == "low double"], sigsqr[sigma_type == "low double"],
                                yerr=sigsqr_err[sigma_type == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[sigma_type == "high double"], sigsqr[sigma_type == "high double"],
                                yerr=sigsqr_err[sigma_type == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigsqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigsqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigsqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                else:
                    ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[cidx], sigsqr[cidx], yerr=sigsqr_err[cidx] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr * 1e4, r"$10^{-4}$") + "\n"
                if mode == "trans":
                    label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                                                                                 mass_err * 10 ** units["mass_mag"],
                                                                                 r"$10^{{{}}}$".format(
                                                                                     -units["mass_mag"])) + "\n"
                else:
                    label += r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                                                                                    mass_err * 10 ** units["mass_mag"],
                                                                                    r"$10^{{{}}}$".format(
                                                                                        -units["mass_mag"])) + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(tau * 1e3, tau_err * 1e3)
                ax.plot(dt_space, 10 ** (fit_of_log(dt_space, *beta)), c="red", lw=2, ls="-", zorder=20, label=label)
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.legend(loc="lower right", framealpha=1)
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot["path"][a] + "sigma_OF_{}_{}_{}.png".format(mode, axis, particle), dpi=plot["dpi"])
                plt.close()
            if not silent:
                pb.tick()
        if temp_path is not None:
            if p/len(particles) > n_temp * temp_interval:
                df_prelim = move_col(df_out, id_column, 0)
                df_prelim.to_csv(temp_path, index=False)
                n_temp += 1
    if temp_path is not None:
        os.remove(temp_path)
    if not silent:
        pb.finish()
    if (len(df_out[id_column].unique()) == 1) and (df_out[id_column].unique()[0] == 0):
        df_out.drop(columns=[id_column], inplace=True)
    if ret_erf:
        return df_out, df_erf
    else:
        return df_out



# ============================================================
# SG/DG Decision
# ============================================================
# filters = {
#     "max_chisqr": 5e-3,
#     "excluded": (8781427,),
#     "max_ns_ratio": 0.2,
#     "min_weight": 0.9,
#     "min_chisqr_ratio": 3,
#     "max_sigma_ratio": 2,
#     # "max_sigma": 9e5,
#     # "min_sigma": 1e-7,
#     "max_sigma": 1e10,
#     "min_sigma": 0,
#     "operator": "or"
# }
#
# plot_xerr = False
# dg_of_used = False
# df_erf = pd.read_csv(csv_trans_erf_flagged)
# df = pd.read_csv(csv_final_sg)
# print("all:\t\t\t\t", len(df["particle"].unique()))
# df = df[df["chisqr_x"] <= filters["max_chisqr"]]
# df = df[df["chisqr_y"] <= filters["max_chisqr"]]
# print("max_chisqr ({}):\t".format(filters["max_chisqr"]), len(df["particle"].unique()))
# df = df.dropna(subset=["mass_x", "mass_y"])
# print("xy clause:\t\t\t", len(df["particle"].unique()))
# df = df[~df["particle"].isin(filters["excluded"])]
# print("excluded:\t\t\t", len(df["particle"].unique()))
# df = df[df["mass_x_err"] / df["mass_x"] <= filters["max_ns_ratio"]]
# df = df[df["mass_y_err"] / df["mass_y"] <= filters["max_ns_ratio"]]
# df = df[df["tau_x_err"] / df["tau_x"] <= filters["max_ns_ratio"]]
# df = df[df["tau_y_err"] / df["tau_y"] <= filters["max_ns_ratio"]]
# print("max_ns_ratio ({}):\t".format(filters["max_ns_ratio"]), len(df["particle"].unique()))
# df = df.dropna(subset=["mass_x", "mass_y"])
# print("xy clause:\t\t\t", len(df["particle"].unique()))
#
# particles = df["particle"].unique()
# df_erf_dg = df_erf[["particle", "dt", "axis", "sigma", "sigma_err", "chisqr", "of_used"]].copy()
# df_erf_dg = df_erf_dg[df_erf_dg["particle"].isin(particles)]
# df_erf_dg["sigma_type"] = "single"
# df_noise = None
# start_frame = np.zeros(len(particles), dtype=int)
# for i, particle in enumerate(particles):
#     start_frame[i] = df.loc[df["particle"] == particle, "start_frame"].to_numpy()[0]
# particles1 = particles[start_frame < icaps.const.end_ff]
# print("injection 1:\t\t", len(particles1))
# particles2 = particles[start_frame >= icaps.const.end_ff]
# print("injection 2:\t\t", len(particles2))
# particles = np.array([particles1, particles2], dtype=object)
# axes = ["x", "y"]
# pb = icaps.ProgressBar(len(particles) * len(axes) * len(dts_dg), "Trans Double Gauss Correction")
# extra_plots = [plt.subplots() for i in range(len(dts))]
# # icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_extra/", clear=True)
# icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_extra/", clear=False)
# for i, xax in enumerate(extra_plots):
#     xax[1].set_xlabel(r"$\langle E \rangle$ (arb. units)")
#     xax[1].set_ylabel(r"$\sigma_{\rm trans}$ (m)")
#     xax[1].set_yscale("log")
#     xax[1].set_xscale("log")
#     x_mean_ex = df["mean_ex"].to_numpy()
#     xax[1].set_xlim(0.8 * np.min(x_mean_ex), 1.2 * np.max(x_mean_ex))
#     xax[1].set_ylim(9e-9, 9e-5)
#     xax[1].set_title(r"$\Delta t$" + " = {} ms".format(dts[i]))
# for inject, parts in enumerate(particles):
#     df_ = df[df["particle"].isin(parts)]
#     mean_ex = df_["mean_ex"].to_numpy()
#     mean_ex_err = df_["mean_ex_err"].to_numpy()
#     df_erf_inj = df_erf[df_erf["particle"].isin(parts)]
#     # icaps.mk_folder(plot_dg_path + "sigma_mass_trans_i{}/".format(inject+1), clear=True)
#     # icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_i{}/".format(inject + 1), clear=True)
#     icaps.mk_folder(plot_dg_path + "sigma_mass_trans_i{}/".format(inject+1), clear=True)
#     icaps.mk_folder(plot_dg_path + "sigma_mass_noise_trans_i{}/".format(inject + 1), clear=True)
#     for axis in axes:
#         for idt, dt in enumerate(dts_dg):
#             df_erf_ = df_erf_inj[df_erf_inj["axis"] == axis]
#             df_erf_ = df_erf_[df_erf_["dt"] == dt]
#             parts_ = parts[~np.isnan(df_erf_["sigma"].to_numpy())]
#             mean_ex_ = mean_ex[~np.isnan(df_erf_["sigma"].to_numpy())]
#             mean_ex_err_ = mean_ex_err[~np.isnan(df_erf_["sigma"].to_numpy())]
#             df_erf_ = df_erf_[~np.isnan(df_erf_["sigma"])]
#             of_used = np.squeeze(df_erf_["of_used"].to_numpy())
#             sig0 = np.squeeze(df_erf_["sigma0"].to_numpy())
#             sig1 = np.squeeze(df_erf_["sigma1"].to_numpy())
#             sig = np.squeeze(df_erf_["sigma"].to_numpy())
#             sig_err = np.squeeze(df_erf_["sigma_err"].to_numpy())
#             sig0_err = np.squeeze(df_erf_["sigma0_err"].to_numpy())
#             sig1_err = np.squeeze(df_erf_["sigma1_err"].to_numpy())
#             chisqr_erf = np.squeeze(df_erf_["chisqr"].to_numpy())
#             chisqr_erf_dg = np.squeeze(df_erf_["chisqr_dg"].to_numpy())
#             w0 = np.squeeze(df_erf_["w0"].to_numpy())
#             w1 = 1. - w0
#             sig0_err[sig0 <= 0] = sig_err[sig0 <= 0]
#             sig1_err[sig1 <= 0] = sig_err[sig1 <= 0]
#             sig0[sig0 <= 0] = sig[sig0 <= 0]
#             sig1[sig1 <= 0] = sig[sig1 <= 0]
#             sigmax = np.amax([sig0, sig1], axis=0)
#             sigmin = np.amin([sig0, sig1], axis=0)
#             sigmax_err = np.array([sig0_err[i] if sig0[i] > sig1[i] else sig1_err[i] for i in range(len(sig0))])
#             sigmin_err = np.array([sig0_err[i] if sig0[i] <= sig1[i] else sig1_err[i] for i in range(len(sig0))])
#             wmax = np.array([w0[i] if w0[i] > w1[i] else w1[i] for i in range(len(w0))])
#
#             fig, ax = plt.subplots()
#             ax.set_xlabel(r"$\langle E \rangle$ (arb. units)")
#             ax.set_ylabel(r"$\sigma_{\rm trans}$ (m)")
#             ax.set_yscale("log")
#             ax.set_xscale("log")
#             ax.set_xlim(0.8 * np.min(mean_ex_), 1.2 * np.max(mean_ex_))
#             ax.set_ylim(9e-9, 9e-5)
#             ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))
#
#             f1 = (sigmax / sigmin < filters["max_sigma_ratio"])
#             f2 = (chisqr_erf / chisqr_erf_dg < filters["min_chisqr_ratio"])
#             f3 = (wmax >= filters["min_weight"])
#             fsum = None
#             if filters["operator"] == "or":
#                 fsum = f1 | f2 | f3
#             elif filters["operator"] == "and":
#                 fsum = f1 & f2 & f3
#             else:
#                 print("Bad Filter Type!")
#             if dg_of_used:
#                 fsum = fsum | of_used
#
#             parts_ = parts_[sig < filters["max_sigma"]].copy()
#             fsum = fsum[sig < filters["max_sigma"]]
#             sigmax = sigmax[sig < filters["max_sigma"]]
#             sigmin = sigmin[sig < filters["max_sigma"]]
#             sigmax_err = sigmax_err[sig < filters["max_sigma"]]
#             sigmin_err = sigmin_err[sig < filters["max_sigma"]]
#             mean_ex_ = mean_ex_[sig < filters["max_sigma"]]
#             mean_ex_err_ = mean_ex_err_[sig < filters["max_sigma"]]
#             sig_err = sig_err[sig < filters["max_sigma"]]
#             sig = sig[sig < filters["max_sigma"]]
#
#             parts_ = parts_[sig >= filters["min_sigma"]].copy()
#             fsum = fsum[sig >= filters["min_sigma"]]
#             sigmax = sigmax[sig >= filters["min_sigma"]]
#             sigmin = sigmin[sig >= filters["min_sigma"]]
#             sigmax_err = sigmax_err[sig >= filters["min_sigma"]]
#             sigmin_err = sigmin_err[sig >= filters["min_sigma"]]
#             mean_ex_ = mean_ex_[sig >= filters["min_sigma"]]
#             mean_ex_err_ = mean_ex_err_[sig >= filters["min_sigma"]]
#             sig_err = sig_err[sig >= filters["min_sigma"]]
#             sig = sig[sig >= filters["min_sigma"]]
#
#             sigmax_ = sigmax[~fsum]
#             sigmin_ = sigmin[~fsum]
#             sigmax_err_ = sigmax_err[~fsum]
#             sigmin_err_ = sigmin_err[~fsum]
#             sig_ = sig[fsum]
#             sig_err_ = sig_err[fsum]
#             ax.errorbar(mean_ex_[~fsum], sigmax_,
#                         xerr=mean_ex_err_[~fsum] if plot_xerr else None, yerr=sigmax_err_,
#                         fmt="^", markerfacecolor="none", markeredgecolor="red", markersize=5, alpha=0.3)
#             ax.errorbar(mean_ex_[~fsum], sigmin_,
#                         xerr=mean_ex_err_[~fsum] if plot_xerr else None, yerr=sigmin_err_,
#                         fmt="v", markerfacecolor="none", markeredgecolor="blue", markersize=5, alpha=0.3)
#             ax.errorbar(mean_ex_[fsum], sig_,
#                         xerr=mean_ex_err_[fsum] if plot_xerr else None, yerr=sig_err_,
#                         fmt="x", color="black", label=r"Single Gauss", markersize=5, alpha=1.)
#             # print("single", len(mean_ex_[fsum]))
#             # print("double", len(mean_ex_[~fsum]))
#             out = icaps.fit_model(icaps.fit_lin, np.log10(mean_ex_[fsum]), np.log10(sig_),
#                                   xerr=(mean_ex_err_[fsum] / mean_ex_[fsum]) / np.log(10),
#                                   yerr=(sig_err_ / sig_) / np.log(10),
#                                   p0=[-0.5, -3], fit_type=2, maxit=10000)
#             xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
#             ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="black", ls="-", lw=2,
#                     label="Single Gauss Fit\nSlope = {:.2f} \u00b1 {:.2f}".format(out.beta[0], out.sd_beta[0]))
#             distmin = np.abs(np.log10(sigmin_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
#             distmax = np.abs(np.log10(sigmax_) - icaps.fit_lin(np.log10(mean_ex_[~fsum]), *out.beta))
#             sigmin_use = sigmin_[distmin <= distmax]
#             sigmax_use = sigmax_[distmin > distmax]
#             sigmin_use_err = sigmin_err_[distmin <= distmax]
#             sigmax_use_err = sigmax_err_[distmin > distmax]
#             ax.errorbar(mean_ex_[~fsum][distmin > distmax], sigmax_use,
#                         xerr=mean_ex_err_[~fsum][distmin > distmax] if plot_xerr else None, yerr=sigmax_use_err,
#                         fmt="^", c="red", label=r"Major Double Gauss", markersize=5, alpha=1.)
#             ax.errorbar(mean_ex_[~fsum][distmin <= distmax], sigmin_use,
#                         xerr=mean_ex_err_[~fsum][distmin <= distmax] if plot_xerr else None, yerr=sigmin_use_err,
#                         fmt="v", c="blue", label=r"Minor Double Gauss", markersize=5, alpha=1.)
#
#             # x_full = np.hstack(
#             #     [mean_ex_[fsum], mean_ex_[~fsum][distmin <= distmax], mean_ex_[~fsum][distmin > distmax]]).ravel()
#             # y_full = np.hstack([sig_, sigmin_use, sigmax_use]).ravel()
#             # out = icaps.fit_model(icaps.fit_lin, x=np.log10(x_full), y=np.log10(y_full),
#             #                       p0=[-0.5, -5], fit_type=2)
#             # ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="magenta", ls="--", lw=2,
#             #         label="Single and Double Gauss Fit\nSlope = {:.2f} \u00b1 {:.2f}".format(out.beta[0],
#             #                                                                                  out.sd_beta[0]))
#             ax.legend()
#             plt.savefig(plot_dg_path + "sigma_mass_trans_i{}/".format(inject + 1) +
#                         "sigma_mass_trans_i{}_{}_{}_{}_{}_{}.png".format(inject + 1, axis, dt,
#                                                                          filters["min_weight"],
#                                                                          filters["min_chisqr_ratio"],
#                                                                          filters["max_sigma_ratio"]),
#                         dpi=600)
#             plt.close(fig)
#
#             fig, ax = plt.subplots()
#             ax.set_xlabel(r"$\langle E \rangle$ (arb. units)")
#             ax.set_ylabel(r"$\sigma_{\rm trans}$ (m)")
#             ax.set_yscale("log")
#             ax.set_xscale("log")
#             ax.set_xlim(0.8 * np.min(mean_ex), 1.2 * np.max(mean_ex))
#             ax.set_ylim(9e-9, 9e-5)
#             ax.set_title(r"$\Delta t$" + " = {} ms".format(dt))
#             ax.scatter(mean_ex_[~fsum][distmin <= distmax], sigmax_[distmin <= distmax], marker="^", c="red",
#                        label=r"Major Double Gauss", s=5, alpha=1.)
#             ax.scatter(mean_ex_[~fsum][distmin > distmax], sigmin_[distmin > distmax], marker="v", c="blue",
#                        label=r"Minor Double Gauss", s=5, alpha=1.)
#             ax.legend()
#             plt.savefig(plot_dg_path + "sigma_mass_noise_trans_i{}/".format(inject + 1) +
#                         "sigma_mass_noise_trans_i{}_{}_{}_{}_{}_{}.png".format(inject + 1, axis, dt,
#                                                                                filters["min_weight"],
#                                                                                filters["min_chisqr_ratio"],
#                                                                                filters["max_sigma_ratio"]),
#                         dpi=600)
#             plt.close(fig)
#
#             extra_plots[idt][1].errorbar(mean_ex_[~fsum][distmin <= distmax], sigmax_[distmin <= distmax],
#                                          fmt="^", color="red", markersize=5, alpha=1.)
#             extra_plots[idt][1].errorbar(mean_ex_[~fsum][distmin > distmax], sigmin_[distmin > distmax],
#                                          fmt="v", color="blue", markersize=5, alpha=1.)
#
#             if df_noise is None:
#                 p = parts_[~fsum][distmin <= distmax]
#                 df_noise = pd.DataFrame(data={"particle": p,
#                                               "dt": np.repeat(dt, len(p)),
#                                               "axis": np.repeat(axis, len(p)),
#                                               "inject": np.repeat(inject, len(p)),
#                                               "sigma": sigmax_[distmin <= distmax],
#                                               "sigma_err": sigmax_err_[distmin <= distmax],
#                                               "sigma_type": np.repeat("high double", len(p))})
#                 p = parts_[~fsum][distmin > distmax]
#                 df_noise_ = pd.DataFrame(data={"particle": p,
#                                                "dt": np.repeat(dt, len(p)),
#                                                "axis": np.repeat(axis, len(p)),
#                                                "inject": np.repeat(inject, len(p)),
#                                                "sigma": sigmin_[distmin > distmax],
#                                                "sigma_err": sigmin_err_[distmin > distmax],
#                                                "sigma_type": np.repeat("low double", len(p))})
#                 df_noise = df_noise.append(df_noise_, sort=False)
#             else:
#                 p = parts_[~fsum][distmin <= distmax]
#                 df_noise_ = pd.DataFrame(data={"particle": p,
#                                                "dt": np.repeat(dt, len(p)),
#                                                "axis": np.repeat(axis, len(p)),
#                                                "inject": np.repeat(inject, len(p)),
#                                                "sigma": sigmax_[distmin <= distmax],
#                                                "sigma_err": sigmax_err_[distmin <= distmax],
#                                                "sigma_type": np.repeat("high double", len(p))})
#                 df_noise = df_noise.append(df_noise_, sort=False)
#                 p = parts_[~fsum][distmin > distmax]
#                 df_noise_ = pd.DataFrame(data={"particle": p,
#                                                "dt": np.repeat(dt, len(p)),
#                                                "axis": np.repeat(axis, len(p)),
#                                                "inject": np.repeat(inject, len(p)),
#                                                "sigma": sigmin_[distmin > distmax],
#                                                "sigma_err": sigmin_err_[distmin > distmax],
#                                                "sigma_type": np.repeat("low double", len(p))})
#                 df_noise = df_noise.append(df_noise_, sort=False)
#             for i, part in enumerate(parts_[fsum]):
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma"] = sig_[i]
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma_err"] = sig_err_[i]
#             for i, part in enumerate(parts_[~fsum][distmin <= distmax]):
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma"] = sigmin_use[i]
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma_err"] = sigmin_use_err[i]
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma_type"] = "low double"
#             for i, part in enumerate(parts_[~fsum][distmin > distmax]):
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma"] = sigmax_use[i]
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma_err"] = sigmax_use_err[i]
#                 df_erf_dg.loc[(df_erf_dg["axis"] == axis) & (df_erf_dg["particle"] == part) &
#                               (df_erf_dg["dt"] == dt), "sigma_type"] = "high double"
#             pb.tick()
# pb.finish()
# for i, xax in enumerate(extra_plots):
#     xax[0].savefig(plot_dg_path + "sigma_mass_noise_trans_extra/" +
#                    "sigma_mass_noise_trans_extra_{}.png".format(dts[i]), dpi=600)
#     plt.close(xax[0])
# plt.close("all")
#
# df_erf_dg.to_csv(csv_trans_erf_dg, index=False)
# df_noise.to_csv(csv_trans_erf_noise, index=False)






# ============================================================
# CALC & PLOT TRANS OF FITS (OLD!)
# ============================================================
# df_erf = pd.read_csv(csv_erf_trans_path)
# plot = False
# df, df_erf2 = icaps.brownian.calc_ornstein_fuerth(
#     df_erf, dts, axes=["x", "y"], mode="trans",
#     temp_path=project_path + "OF_Trans_prelim.csv", silent=False, ret_erf=True,
#     plot={"path": plot_path + "of_trans_{}/" if plot else None,
#           "attempts": False,
#           "error": True,
#           "dpi": 600,
#           "clear_out": True,
#           "crop_to_max": True},
#     fit={"fit_type": 2,
#          "fit_err": True,
#          "bounds": (None, None),
#          "adapt_margins": (5, 13),
#          "grade_weights": (1., 1.),
#          "grade_type": "gaussian",
#          "n_min": 10,
#          "quality_type": "all",
#          "quantity_type": "count",
#          "max_sigsqr": 1e-6,
#          "max_err_ratio": 1,
#          "min_tau": 4.8e-3},
#     dg={"apply": False,
#         "max_dt": 5,
#         "min_chi_ratio": 3})
# df.to_csv(csv_of_trans_path, index=False)
# df_erf2.to_csv(project_path + "ERF_Trans_2.csv", index=False)


def plot_erf(df_disp, df_erf, axes, disp_columns, out_paths, particle=None, id_column="particle",
             disp_names="d{}", disp_full_name="Displacement", disp_convert=const.px_ldm, erf_convert=1e6,
             disp_unit="µm", dpi=300, double_gauss=True, dg_func=icaps.fit_erf_dg):
    if isinstance(axes, str):
        axes = [axes]
    if isinstance(disp_columns, str):
        disp_columns = [disp_columns]
    assert len(axes) == len(disp_columns)
    assert len(axes) == len(out_paths)
    if (id_column is not None) and (particle is not None):
        df_disp = df_disp[df_disp[id_column] == particle]
        df_erf = df_erf[df_erf[id_column] == particle]
    df_erf = df_erf[df_erf["axis"].isin(axes)]
    if disp_names is None:
        disp_names = disp_columns
    if isinstance(disp_names, str):
        if "{}" in disp_names:
            disp_names = [disp_names.format(a) for a in axes]
        else:
            disp_names = [disp_names for a in axes]
    elif isinstance(disp_names, str):
        disp_names = [disp_names for i in range(len(disp_columns))]

    fig, ax = plt.subplots(nrows=2)
    fig.subplots_adjust(hspace=.001, wspace=.001)
    for i in range(len(axes)):
        # _, ax = plt.subplots(nrows=2)
        # plt.subplots_adjust(hspace=.001, wspace=.001)
        disp = np.sort(df_disp[disp_columns[i]].dropna().to_numpy()) * disp_convert
        norm_disp = np.linspace(1 / len(disp), 1, len(disp))
        ax[0].scatter(disp, norm_disp, c="black", s=2, marker="o")
        ax[1].set_xlabel("{} ({})".format(disp_names[i], disp_unit))
        ax[0].set_ylabel("Normalized {}".format(disp_names[i]))
        ax[1].set_ylabel("Residual")
        ax[0].set_ylim(-0.02, 1.02)
        ax[1].set_ylim(-0.1, 0.1)
        ax[1].axhline(0, c="gray", zorder=0, lw=2, ls="--")
        x_low = 0.95*min([ax[0].get_xlim()[0] for i in range(len(axes))])
        x_high = 1.05*max([ax[0].get_xlim()[1] for i in range(len(axes))])
        x_lim = np.max(np.abs([x_low, x_high]))
        x_space = np.linspace(x_low, x_high, 1000)
        s_erf = icaps.row2dict(df_erf[df_erf["axis"] == axes[i]])
        if x_lim != 0.:
            ax[0].set_xlim(-x_lim, x_lim)
            ax[1].set_xlim(-x_lim, x_lim)
        ax[0].set_xticks([])
        sigma = s_erf["sigma"] * erf_convert
        sigma_err = s_erf["sigma_err"] * erf_convert
        drift = s_erf["drift"] * erf_convert
        drift_err = s_erf["drift_err"] * erf_convert
        if (not np.isnan(sigma)) and (not np.isnan(drift)) and (not sigma_err == 0.) and (not drift_err == 0.):
            ax[0].plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="blue", linewidth=1,
                       label="Single Gauss"
                       # label=r"$\mu$" + " = {:.2f} ".format(drift) + r"$\pm$"
                       #       + " {:.2f} {}\n".format(drift_err, disp_unit)
                       #       + r"$\sigma$" + " = {:.2f} ".format(sigma) + r"$\pm$"
                       #       + " {:.2f} {}".format(sigma_err, disp_unit)
                       )
            # ax[0].legend(loc="upper left")

            norm_disp = np.linspace(1 / len(disp), 1, len(disp))
            ax[1].scatter(disp, icaps.fit_erf(disp, drift, sigma) - norm_disp, c="blue", s=2, zorder=1)

        if double_gauss:
            dg_centered = False
            if "drift01" in s_erf.keys():
                dg_centered = True
            w0 = s_erf["w0"]
            w0_err = s_erf["w0_err"]
            sigma0 = s_erf["sigma0"] * erf_convert
            sigma0_err = s_erf["sigma0_err"] * erf_convert
            sigma1 = s_erf["sigma1"] * erf_convert
            sigma1_err = s_erf["sigma1_err"] * erf_convert
            if dg_centered:
                drift0 = s_erf["drift01"] * erf_convert
                drift0_err = s_erf["drift01_err"] * erf_convert
                drift1 = s_erf["drift01"] * erf_convert
                drift1_err = s_erf["drift01_err"] * erf_convert
                params = (w0, drift0, sigma0, sigma1)
            else:
                drift0 = s_erf["drift0"] * erf_convert
                drift0_err = s_erf["drift0_err"] * erf_convert
                drift1 = s_erf["drift1"] * erf_convert
                drift1_err = s_erf["drift1_err"] * erf_convert
                params = (w0, drift0, drift1, sigma0, sigma1)
            if (not np.isnan(sigma)) and (not np.isnan(drift)) and (not sigma_err == 0.) and (not drift_err == 0.):
                ax[0].plot(x_space, dg_func(x_space, *params), c="red", linewidth=1,
                           label="Double Gauss\n"
                                 + r"$w_0$" + " = {:.2f} ".format(w0) + r"$\pm$"
                                 + " {:.2f}\n".format(w0_err)
                                 + (r"$\mu_0$" if not dg_centered else r"$\mu_{01}$")
                                 + " = {:.2f} ".format(drift0) + r"$\pm$"
                                 + " {:.2f} {}\n".format(drift0_err, disp_unit)
                                 + r"$\sigma_0$" + " = {:.2f} ".format(sigma0) + r"$\pm$"
                                 + " {:.2f} {}\n".format(sigma0_err, disp_unit)
                                 + (r"$\mu_1$" + " = {:.2f} ".format(drift1) + r"$\pm$"
                                    + " {:.2f} {}\n".format(drift1_err, disp_unit) if not dg_centered else "")
                                 + r"$\sigma_1$" + " = {:.2f} ".format(sigma1) + r"$\pm$"
                                 + " {:.2f} {}".format(sigma1_err, disp_unit)
                           )
                ax[0].legend(loc="lower right")

                ax[1].scatter(disp, dg_func(disp, *params) - norm_disp, c="red", s=2,
                              zorder=2)

        # plt.tight_layout()
        # mpl.use("Agg")
        if i == 0:
            fig.tight_layout()
        plt.savefig(out_paths[i], dpi=dpi)
        ax[0].cla()
        ax[1].cla()
        # plt.close()
    plt.close()


def plot_erfs(df_disp, df_erf, axes, dts, disp_columns="d{}", id_column="particle", append_dts=True, plot_path=None,
              sub_folder="{}/plots/erf_trans/", disp_names="d{}", disp_unit="µm", disp_convert=const.px_ldm,
              erf_convert=1e6, silent=True, clear_out=False, dpi=300, double_gauss=True, dg_func=icaps.fit_erf_dg):
    mpl.use("Agg")
    if id_column is not None:
        particles = np.unique(df_disp[id_column].to_numpy())
    else:
        particles = [0]
    if isinstance(disp_columns, str):
        if append_dts:
            if "{}" in disp_columns:
                disp_columns = ["{}_{}".format(disp_columns.format(a), dt) for dt in dts for a in axes]
            else:
                disp_columns = ["{}_{}".format(disp_columns, dt) for dt in dts for a in axes]
        elif "{}" in disp_columns:
            disp_columns = [disp_columns.format(a) for dt in dts for a in axes]
        else:
            disp_columns = [disp_columns]
    assert len(dts) * len(axes) == len(disp_columns)
    if sub_folder is None:
        sub_folder = ""
        if clear_out:
            mk_folder(plot_path, clear=clear_out)
            clear_out = False

    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(dts), "Plotting Erf Fits")
    for particle in particles:
        if "{}" in sub_folder:
            sub_folder = sub_folder.format(particle)
        out_path = plot_path + sub_folder
        mk_folder(out_path, clear=clear_out)
        for i in range(int(len(disp_columns)/len(axes))):
            df_erf_ = df_erf[df_erf["dt"] == dts[i]]
            disp_column = disp_columns[len(axes)*i:len(axes)*(i+1)]
            out_paths = [out_path + "{}_{}_{}.png".format(particle, dts[i], axis) for axis in axes]
            plot_erf(df_disp, df_erf_, axes, disp_column, out_paths,
                     particle=particle, id_column=id_column,
                     disp_names=disp_names, disp_convert=disp_convert, erf_convert=erf_convert, disp_unit=disp_unit,
                     dpi=dpi, double_gauss=double_gauss, dg_func=dg_func)
            if not silent:
                pb.tick()
    if not silent:
        pb.finish()


def calc_erf(df, dts, axes, disp_columns="d{}", id_column="particle", column_frmt="{}_{}", p0=None, maxit=None,
             fit_type=2, n_min=10, disp_convert=const.px_ldm*1e-6, silent=True, max_sigma=1, plot_path=None,
             sub_folder="{}/plots/erf_trans/", disp_names="d{}", disp_unit="µm", disp_convert_plot=const.px_ldm,
             erf_convert=1e6, clear_out=False, dpi=300, double_gauss=True, dg_func=icaps.fit_erf_dg, fickian=True):
    dg_version = len(signature(dg_func).parameters) - 4
    if id_column is not None:
        particles = np.unique(df[id_column].to_numpy())
    else:
        particles = [0]
    if isinstance(axes, str):
        axes = [axes]
    disp_columns_param = disp_columns
    if isinstance(disp_columns, str):
        if "{}" in disp_columns:
            disp_columns = [disp_columns.format(a) for a in axes]
        else:
            disp_columns = [disp_columns]
    assert len(axes) == len(disp_columns)
    if p0 is None:
        p0 = [0.0, 1.0]
    if plot_path is not None:
        icaps.mk_folder(plot_path, clear=clear_out)
    nan_arr = np.zeros(len(axes) * len(dts))
    nan_arr[:] = np.nan
    if not double_gauss:
        df_template = pd.DataFrame(data={"dt": np.repeat(dts, len(axes)),
                                         "axis": np.tile(axes, len(dts)),
                                         "sigma": nan_arr,
                                         "sigma_err": nan_arr,
                                         "drift": nan_arr,
                                         "drift_err": nan_arr,
                                         "chisqr": nan_arr,
                                         "n_dots": nan_arr
                                         })
    else:
        df_template = pd.DataFrame(data={"dt": np.repeat(dts, len(axes)),
                                         "axis": np.tile(axes, len(dts)),
                                         "sigma": nan_arr,
                                         "sigma_err": nan_arr,
                                         "drift": nan_arr,
                                         "drift_err": nan_arr,
                                         "chisqr": nan_arr,
                                         "w0": nan_arr,
                                         "w0_err": nan_arr,
                                         "sigma0": nan_arr,
                                         "sigma0_err": nan_arr,
                                         "sigma1": nan_arr,
                                         "sigma1_err": nan_arr,
                                         "drift0": nan_arr,
                                         "drift0_err": nan_arr,
                                         "drift1": nan_arr,
                                         "drift1_err": nan_arr,
                                         "chisqr_double": nan_arr,
                                         "n_dots": nan_arr
                                         })
        if dg_version < 2:
            df_template.drop(columns=["drift1", "drift1_err"], inplace=True)
            df_template.rename(columns={"drift0": "drift01", "drift0_err": "drift01_err"}, inplace=True)
    df_out = None
    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(axes) * len(dts), "Calculating Erf Fits")
    for particle in particles:
        df_out_ = df_template.copy()
        if id_column is not None:
            df_out_[id_column] = particle
            df_ = df[df["particle"] == particle]
        else:
            df_ = df
        for i, disp_column in enumerate(disp_columns):
            for dt in dts:
                axis_label = column_frmt.format(disp_column, dt)
                disp = np.sort(df_[axis_label].dropna().to_numpy()) * disp_convert
                if len(disp) < n_min:
                    if not silent:
                        pb.tick()
                    continue
                norm_disp = np.linspace(1 / len(disp), 1, len(disp))
                out_single = fit_model(icaps.fit_erf, disp, norm_disp, p0=p0, fit_type=fit_type, maxit=maxit)
                if max_sigma is not None:
                    if out_single.beta[0] >= max_sigma:
                        if not silent:
                            pb.tick()
                        continue
                df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "drift"] = out_single.beta[0]
                df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "drift_err"] = out_single.sd_beta[0]
                df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "sigma"] = out_single.beta[1]
                df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "sigma_err"] = out_single.sd_beta[1]
                df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "chisqr"] = out_single.res_var
                df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "n_dots"] = len(disp)
                if double_gauss:
                    p0_ = [0.8, out_single.beta[0], out_single.beta[0], out_single.beta[1], out_single.beta[1]]
                    if dg_version < 2:
                        del p0_[1]
                        if dg_version == 0:
                            del p0_[0]
                    out = fit_model(dg_func, disp, norm_disp, p0=p0_, fit_type=fit_type, maxit=maxit)
                    if dg_version > 0:
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "w0"] = out.beta[0]
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "w0_err"] = out.sd_beta[0]
                    else:
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "w0"] = 0.5
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "w0_err"] = 0
                    if dg_version == 2:
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "drift0"] = out.beta[1]
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]),
                                    "drift0_err"] = out.sd_beta[1]
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "drift1"] = out.beta[2]
                        df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]),
                                    "drift1_err"] = out.sd_beta[2]
                    else:
                        df_out_.loc[(df_out_["dt"] == dt) &
                                    (df_out_["axis"] == axes[i]), "drift01"] = out.beta[dg_version]
                        df_out_.loc[(df_out_["dt"] == dt) &
                                    (df_out_["axis"] == axes[i]), "drift01_err"] = out.sd_beta[dg_version]

                    df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "sigma0"] = out.beta[dg_version+1]
                    df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]),
                                "sigma0_err"] = out.sd_beta[dg_version+1]
                    df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "sigma1"] = out.beta[dg_version+2]
                    df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]),
                                "sigma1_err"] = out.sd_beta[dg_version+2]
                    df_out_.loc[(df_out_["dt"] == dt) & (df_out_["axis"] == axes[i]), "chisqr_double"] = out.res_var
                if not silent:
                    pb.tick()
        if df_out is None:
            df_out = df_out_
        else:
            df_out = df_out.append(df_out_)
        if plot_path is not None:
            icaps.plot_erfs(df_, df_out_, axes=axes, dts=dts, disp_columns=disp_columns_param, id_column=id_column,
                      plot_path=plot_path, sub_folder=sub_folder, disp_names=disp_names, disp_unit=disp_unit,
                      disp_convert=disp_convert_plot, erf_convert=erf_convert, dpi=dpi,
                      dg_func=dg_func if dg_version > 0 else icaps.fit_double_erf_c,
                      double_gauss=double_gauss)
    if not silent:
        pb.finish()
    if id_column is not None:
        df_out = move_col(df_out, id_column, 0)
    return df_out


def calc_ornstein_fuerth_adapt(df_erf, dts=None, axes=None, particles=None, mode="trans",
                               id_column="particle", mass_column="mass",
                               units=None, fit=None, plot=None, dg=None,
                               temp_path=None, temp_interval=0.1, ret_erf=False, silent=True):
    # =====================
    # Parameter Setup
    # =====================
    units_ = {"sigma": "m",
              "time": "s",
              "time_cvt": 1e-3,
              "mass_mag": 14}
    if mode == "rot":
        units_ = icaps.merge_dicts(units_, {"sigma": "rad", "mass_mag": 25})
        if mass_column == "mass":
            mass_column = "inertia"
    mass_err_column = mass_column + "_err"
    units = icaps.merge_dicts(units_, {} if units is None else units)
    fit_ = {"p0": None,
            "maxit": None,
            "fit_type": 0,
            "fit_err": True,
            "bounds": (None, None),
            "adapt_margins": (3, 3),
            "grade_weights": (1., 1.),
            "n_min": 4,
            "quality_type": "all",
            "quantity_type": "count",
            "max_sigsqr": None,
            "max_err_ratio": 1.,
            "grade_type": "gaussian",
            "min_tau": 4.8e-3}
    fit = icaps.merge_dicts(fit_, {} if fit is None else fit)
    plot_ = {"path": None,
             "dpi": 600,
             "clear_out": True,
             "errors": True,
             "attempts": False,
             "crop_to_max": True}
    plot = icaps.merge_dicts(plot_, {} if plot is None else plot)
    dg_ = {"apply": False,
           "min_chi_ratio": 3,
           "max_var": 1,
           "min_weight": 0.2,
           "max_dt": 5}
    dg = icaps.merge_dicts(dg_, {} if dg is None else dg)
    if plot["path"] is not None:
        mpl.use("Agg")
    n_temp = 0
    if id_column is None:
        df_erf["particle"] = 0
        id_column = "particle"
    if particles is None:
        particles = np.unique(df_erf[id_column].to_numpy())
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_raw = dts.copy()
    dts = dts.astype(np.float64) * units["time_cvt"]
    dt_space = np.linspace(np.min(dts), np.max(dts), 1000)
    if not ret_erf:
        df_erf = df_erf[df_erf["dt"].isin(dts_raw)]
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    elif isinstance(axes, str):
        axes = [axes]
    if plot["path"] is None:
        plot["attempts"] = False
    else:
        if "{}" in plot["path"]:
            plot["path"] = [plot["path"].format(axis) for axis in axes]
        else:
            plot["path"] = [plot["path"] for axis in axes]
        mk_folders(plot["path"], clear=plot["clear_out"])
    if fit["p0"] is None:
        fit["p0"] = (None, None)
    fit["p0"] = list(fit["p0"])
    if fit["p0"][0] is None:
        fit["p0"][0] = np.log10(2 * const.k_B * const.T / (const.m_0 * const.r_0 ** 2))
    if fit["p0"][1] is None:
        fit["p0"][1] = 0.001
    nan_arr = np.zeros(len(particles) * len(axes))
    nan_arr[:] = np.nan
    df_out = pd.DataFrame(data={id_column: np.repeat(particles, len(axes)),
                                "axis": np.tile(axes, len(particles)),
                                mass_column: nan_arr,
                                mass_err_column: nan_arr,
                                "tau": nan_arr,
                                "tau_err": nan_arr,
                                "chisqr": nan_arr,
                                "n_dots": np.zeros(len(particles) * len(axes), dtype=int),
                                "t_diff": nan_arr})
    # =====================
    # Start of Loop
    # =====================
    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(axes), "Calculating OF Fits")
    for p, particle in enumerate(particles):
        df_erf_ = df_erf[(df_erf[id_column] == particle) & (df_erf["dt"].isin(dts_raw))]
        for a, axis in enumerate(axes):
            ax = None
            if plot["path"] is not None:
                _, ax = plt.subplots()
                if fit["max_sigsqr"] is not None:
                    ax.axhline(fit["max_sigsqr"], lw=2, ls="--", c="black")
            sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma"].to_numpy())
            sig_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_err"].to_numpy())
            sigsqr = sig**2
            sigsqr_err = 2 * sig_err * sig
            chisqr_erf = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "chisqr"].to_numpy())
            # =====================
            # Adaptive Fitting
            # =====================
            candidates = icaps.parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["bounds"],
                                          adapt_margins=fit["adapt_margins"], n_min=fit["n_min"],
                                          max_sigsqr=fit["max_sigsqr"], max_err_ratio=fit["max_err_ratio"])
            nan_arr = np.zeros(len(candidates))
            nan_arr[:] = np.nan
            df_fit = pd.DataFrame(data={
                "beta0": nan_arr,
                "sd_beta0": nan_arr,
                "beta1": nan_arr,
                "sd_beta1": nan_arr,
                "chisqr": nan_arr,
                "quality": nan_arr,
                "quantity": nan_arr
            })
            if not candidates:
                if not silent:
                    pb.tick()
                plt.close()
                continue
            for ci, cidx in enumerate(candidates):
                log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None
                try:
                    out = fit_model(fit_of_log, dts[cidx], np.log10(sigsqr[cidx]),
                                    p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err)
                except RuntimeError:
                    continue
                if out.res_var == 0:
                    continue
                if fit["min_tau"] is not None:
                    if out.beta[1] + out.sd_beta[1] < fit["min_tau"]:
                        continue
                df_fit.loc[ci, "beta0"] = out.beta[0]
                df_fit.loc[ci, "beta1"] = out.beta[1]
                df_fit.loc[ci, "sd_beta0"] = out.sd_beta[0]
                df_fit.loc[ci, "sd_beta1"] = out.sd_beta[1]
                chisqr = np.sum((np.log10(sigsqr[cidx]) - fit_of_log(dts[cidx], *out.beta)) ** 2) / len(cidx)
                df_fit.loc[ci, "chisqr"] = chisqr
                df_fit.loc[ci, "quality"] = 1 / chisqr
                if fit["quantity_type"] == "time":
                    df_fit.loc[ci, "quantity"] = np.max(dts[cidx]) - np.min(dts[cidx])
                else:
                    df_fit.loc[ci, "quantity"] = len(cidx)
                if plot["attempts"] and plot["path"] is not None:
                    ax.plot(dt_space, 10 ** fit_of_log(dt_space, *out.beta),
                            c="gray", lw=1, ls="-.", alpha=0.3)
            if np.max(df_fit["quality"]) == 0:
                if not silent:
                    pb.tick()
                continue
            df_fit["quality"] = df_fit["quality"] / np.max(df_fit["quality"])
            df_fit["quantity"] = df_fit["quantity"] / np.max(df_fit["quantity"])
            if fit["grade_type"] == "linear":
                df_fit["grade"] = df_fit["quality"] * fit["grade_weights"][0]\
                                  + df_fit["quantity"] * fit["grade_weights"][1]
            else:
                df_fit["grade"] = np.sqrt((df_fit["quality"] * fit["grade_weights"][0]) ** 2
                                          + (df_fit["quantity"] * fit["grade_weights"][1]) ** 2)
            best_idx = np.argmax(df_fit["grade"].to_numpy())
            beta = np.squeeze(df_fit.loc[best_idx, ["beta0", "beta1"]])
            sd_beta = np.squeeze(df_fit.loc[best_idx, ["sd_beta0", "sd_beta1"]])
            chisqr = np.squeeze(df_fit.loc[best_idx, "chisqr"])
            cidx = candidates[best_idx]
            mass = 2 * const.k_B * const.T / (10 ** beta[0])
            mass_err = 2 * const.k_B * const.T * np.log(10) / (10 ** beta[0]) * sd_beta[0]
            tau = abs(beta[1])
            tau_err = sd_beta[1]
            t_diff = np.max(dts[cidx]) - np.min(dts[cidx])
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), mass_column] = mass
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), mass_err_column] = mass_err
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau"] = tau
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau_err"] = tau_err
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "chisqr"] = chisqr
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "n_dots"] = len(cidx)
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "t_diff"] = t_diff
            if ret_erf:
                use = np.zeros(len(sigsqr), dtype=bool)
                use[cidx] = True
                df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                           (df_erf["axis"] == axis), "of_used"] = use
            # =====================
            # Plotting
            # =====================
            if plot["path"] is not None:
                if dg["apply"]:
                    sigma_type = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigsqr[sigma_type == "single"],
                                yerr=sigsqr_err[sigma_type == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[sigma_type == "low double"], sigsqr[sigma_type == "low double"],
                                yerr=sigsqr_err[sigma_type == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[sigma_type == "high double"], sigsqr[sigma_type == "high double"],
                                yerr=sigsqr_err[sigma_type == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigsqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigsqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigsqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                else:
                    ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[cidx], sigsqr[cidx], yerr=sigsqr_err[cidx] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr * 1e4, r"$10^{-4}$") + "\n"
                if mode == "trans":
                    label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                                                                                 mass_err * 10 ** units["mass_mag"],
                                                                                 r"$10^{{{}}}$".format(
                                                                                     -units["mass_mag"])) + "\n"
                else:
                    label += r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                                                                                    mass_err * 10 ** units["mass_mag"],
                                                                                    r"$10^{{{}}}$".format(
                                                                                        -units["mass_mag"])) + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(tau * 1e3, tau_err * 1e3)
                ax.plot(dt_space, 10 ** (fit_of_log(dt_space, *beta)), c="red", lw=2, ls="-", zorder=20, label=label)
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.legend(loc="lower right", framealpha=1)
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot["path"][a] + "sigma_OF_{}_{}_{}.png".format(mode, axis, particle), dpi=plot["dpi"])
                plt.close()
            if not silent:
                pb.tick()
        if temp_path is not None:
            if p/len(particles) > n_temp * temp_interval:
                df_prelim = move_col(df_out, id_column, 0)
                df_prelim.to_csv(temp_path, index=False)
                n_temp += 1
    if temp_path is not None:
        os.remove(temp_path)
    if not silent:
        pb.finish()
    if (len(df_out[id_column].unique()) == 1) and (df_out[id_column].unique()[0] == 0):
        df_out.drop(columns=[id_column], inplace=True)
    if ret_erf:
        return df_out, df_erf
    else:
        return df_out


def calc_ornstein_fuerth_old(df_erf, dts=None, particles=None, plot_path=None, p0=None, maxit=None, fit_type=0,
                             id_column="particle", axes=None, mass_column="mass", sigma_column="sigma",
                             fit_error=True, fit_bounds=(None, None), adapt_margins=(3, 3),
                             grade_weights=(1., 0.), plot_attempts=False, n_min=4, quality_type="fixed",
                             quantity_type="time", max_sigma_sqr=None, max_err_ratio=1, silent=True,
                             plot_error=True, sigma_unit="m", mode="trans", time_units="s", time_convert=1e-3,
                             dpi=300, clear_out=False, grade_type="linear", prelim_save=None, prelim_cutoff=0.1,
                             crop_to_max=True, double_gauss="fitted", dg_min_chi_factor=3, dg_max_dt=5, dg_max_var=1,
                             dg_min_weight=0.2, dg_as_error=False, ret_erf=False, min_tau=4.8e-3):
    mpl.use("Agg")
    sigma_err_column = sigma_column + "_err"
    n_prelim_saves = 0
    if particles is None:
        if id_column is not None:
            particles = np.unique(df_erf[id_column].to_numpy())
        else:
            particles = [0]
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_ms = dts.copy()
    dts = dts.astype(np.float64) * time_convert
    dt_range = np.linspace(np.min(dts), np.max(dts), 1000)
    # df_erf = df_erf[df_erf["dt"].isin(dts_ms)]
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    if isinstance(axes, str):
        axes = [axes]
    if plot_path is None:
        plot_attempts = False
    else:
        if "{}" in plot_path:
            plot_path = [plot_path.format(a) for a in axes]
        else:
            plot_path = [plot_path for a in axes]
        mk_folders(plot_path, clear=clear_out)
    if p0 is None:
        p0 = (None, None)
    p0 = list(p0)
    if p0[0] is None:
        p0[0] = np.log10(2 * const.k_B * const.T / (const.m_0 * const.r_0 ** 2))
    if p0[1] is None:
        p0[1] = 0.001
    nan_arr = np.zeros(len(axes))
    nan_arr[:] = np.nan
    df_template = pd.DataFrame(data={"axis": axes,
                                     mass_column: nan_arr,
                                     "{}_err".format(mass_column): nan_arr,
                                     "tau": nan_arr,
                                     "tau_err": nan_arr,
                                     "chi_sqr": nan_arr,
                                     "n_dots": np.zeros(len(axes), dtype=int),
                                     "t_diff": nan_arr
                                     })
    df_out = None
    if ret_erf:
        df_erf["of_used"] = False
    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(axes), "Calculating OF Fits")

    for part_i, particle in enumerate(particles):
        df_out_ = df_template.copy()
        if id_column is not None:
            df_out_[id_column] = particle
            df_ = df_erf[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_ms))]
        else:
            df_ = df_erf[df_erf["dt"].isin(dts_ms)]
        for i, a in enumerate(axes):
            ax = None
            if plot_path is not None:
                _, ax = plt.subplots()
                if max_sigma_sqr:
                    ax.axhline(max_sigma_sqr, linewidth=2, linestyle="--", color="black")
            sigma = np.squeeze(df_.loc[df_["axis"] == a, sigma_column].to_numpy())
            sigma_sqr = sigma**2
            sigma_sqr_err = 2 * np.squeeze(df_.loc[df_["axis"] == a, [sigma_err_column]].to_numpy()) * sigma
            chisqr_erf = np.squeeze(df_.loc[df_["axis"] == a, "chi_sqr"].to_numpy())

            if double_gauss == "adapt":
                sigma2_sqr = sigma_sqr.copy()
                sigma2_sqr_err = sigma_sqr_err.copy()
                sigma0 = np.squeeze(df_.loc[df_["axis"] == a, "sigma0"].to_numpy())
                sigma0_sqr = sigma0 ** 2
                sigma0_sqr_err = 2 * np.squeeze(df_.loc[df_["axis"] == a, "sigma0_err"].to_numpy()) * sigma0
                sigma1 = np.squeeze(df_.loc[df_["axis"] == a, "sigma1"].to_numpy())
                sigma1_sqr = sigma1 ** 2
                sigma1_sqr_err = 2 * np.squeeze(df_.loc[df_["axis"] == a, "sigma1_err"].to_numpy()) * sigma1
                w0 = np.squeeze(df_.loc[df_["axis"] == a, "w0"].to_numpy())
                w1 = 1. - w0
                chisqr_erf_double = np.squeeze(df_.loc[df_["axis"] == a, "chi_sqr_double"].to_numpy())
                sigs = np.transpose([sigma0_sqr, sigma1_sqr])
                sigs_err = np.transpose([sigma0_sqr_err, sigma1_sqr_err])
                ws = np.transpose([w0, w1])
                if dg_max_dt is None:
                    dg_max_dt = dts_ms[-1]
                if dg_as_error:
                    sigma_sqr_err_low = sigma_sqr_err.copy()
                    sigma_sqr_err_high = sigma_sqr_err.copy()
                    for j in range(np.where(dts_ms == dg_max_dt)[0][0] + 1):
                        max_idx = np.where(sigs[j] == np.max(sigs[j]))[0][0]
                        min_idx = max_idx - 1
                        sigma_sqr_err_low[j] = sigs[j, min_idx]
                        sigma_sqr_err_high[j] = sigs[j, max_idx]
                        if ws[j, min_idx] > ws[j, max_idx]:
                            sigma_sqr_err_low[j] *= ws[j, min_idx]
                            sigma_sqr_err_high[j] *= ws[j, max_idx] / ws[j, min_idx]
                        else:
                            sigma_sqr_err_low[j] *= ws[j, min_idx] / ws[j, max_idx]
                            sigma_sqr_err_high[j] *= ws[j, max_idx]
                    sigma_sqr_err = np.array([sigma_sqr_err_low, sigma_sqr_err_high])
                else:
                    for j in range(np.where(dts_ms == dg_max_dt)[0][0] + 1):
                        if np.any(np.isnan(sigs[j])):
                            continue
                        if chisqr_erf[j]/chisqr_erf_double[j] < dg_min_chi_factor:
                            continue
                        if sigs_err[j][0]/sigs[j][0] > dg_max_var:
                            continue
                        if sigs_err[j][1]/sigs[j][1] > dg_max_var:
                            continue
                        max_idx = np.where(sigs[j] == np.max(sigs[j]))[0][0]
                        min_idx = max_idx - 1
                        if ws[j, min_idx] >= dg_min_weight:
                            sigma_sqr[j] = sigs[j, min_idx]
                            sigma_sqr_err[j] = sigs_err[j, min_idx]
                        else:
                            sigma_sqr[j] = sigs[j, max_idx]
                            sigma_sqr_err[j] = sigs_err[j, max_idx]
            if plot_path is not None:
                if double_gauss == "fitted":
                    sigma_type = np.squeeze(df_.loc[df_["axis"] == a, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigma_sqr[sigma_type == "single"],
                                yerr=sigma_sqr_err[sigma_type == "single"] if plot_error else None, fmt="o",
                                c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[sigma_type == "low double"], sigma_sqr[sigma_type == "low double"],
                                yerr=sigma_sqr_err[sigma_type == "low double"] if plot_error else None, fmt="v",
                                c="black", markersize=7, markerfacecolor="none")
                    ax.errorbar(dts[sigma_type == "high double"], sigma_sqr[sigma_type == "high double"],
                                yerr=sigma_sqr_err[sigma_type == "high double"] if plot_error else None, fmt="^",
                                c="black", markersize=7, markerfacecolor="none")
                elif (double_gauss == "none") or dg_as_error:
                    ax.errorbar(dts, sigma_sqr, yerr=sigma_sqr_err if plot_error else None, fmt=".", color="gray",
                                markersize=7, markerfacecolor="gray", alpha=0.7)
                else:
                    ax.errorbar(dts, sigma_sqr, yerr=sigma_sqr_err, fmt=".", markersize=7, c="blue",
                                marker="o", markerfacecolor="none")
                    dg_max_idx = np.where(dts_ms == dg_max_dt)[0][0] + 1
                    # ax.errorbar(dts[:dg_max_idx], sigma0_sqr[:dg_max_idx], yerr=None, fmt=".",
                    #             markersize=7, c="green",
                    #             marker="x", markerfacecolor="none", zorder=101)
                    # ax.errorbar(dts[:dg_max_idx], sigma1_sqr[:dg_max_idx], yerr=None, fmt=".",
                    #             markersize=7, c="red",
                    #             marker="+", markerfacecolor="none", zorder=100)
                    # ax.errorbar(dts[:dg_max_idx], sigma2_sqr[:dg_max_idx], yerr=None, fmt=".",
                    #             markersize=7, c="magenta",
                    #             marker="d", markerfacecolor="none", zorder=99)
                    ax.errorbar(dts, sigma0_sqr, yerr=None, fmt=".",
                                markersize=7, c="green",
                                marker="x", markerfacecolor="none", zorder=101)
                    ax.errorbar(dts, sigma1_sqr, yerr=None, fmt=".",
                                markersize=7, c="red",
                                marker="+", markerfacecolor="none", zorder=100)
                    ax.errorbar(dts, sigma2_sqr, yerr=None, fmt=".",
                                markersize=7, c="magenta",
                                marker="d", markerfacecolor="none", zorder=99)
                    ylim_min = np.nanmin([sigma0_sqr[0], sigma1_sqr[0], sigma2_sqr[0]])
                    ylim_max = np.nanmax([sigma0_sqr[~np.isnan(sigma0_sqr)][-1],
                                          sigma1_sqr[~np.isnan(sigma1_sqr)][-1],
                                          sigma2_sqr[~np.isnan(sigma2_sqr)][-1]])
                    ax.set_ylim(0.8 * ylim_min,
                                1.2 * ylim_max)
            popts = []
            perrs = []
            chisqrs = []
            candidates = icaps.parse_candidates(dts_ms, sigma_sqr, sigma_sqr_err, fit_bounds=fit_bounds,
                                          adapt_margins=adapt_margins, n_min=n_min, max_sigma_sqr=max_sigma_sqr,
                                          max_err_ratio=max_err_ratio)
            unalterable = np.arange(0, len(dts_ms), 1)
            scut = sigma_sqr[adapt_margins[0]:len(unalterable) - adapt_margins[1]]
            serrcut = sigma_sqr_err[adapt_margins[0]:len(unalterable) - adapt_margins[1]]
            unalterable = unalterable[adapt_margins[0]:len(unalterable) - adapt_margins[1]]
            unalterable = unalterable[(scut <= max_sigma_sqr) & (serrcut/scut <= max_err_ratio)]
            if not candidates:
                if not silent:
                    pb.tick()
                plt.close()
                continue
            quality = np.zeros(len(candidates), dtype=float)
            quantity = np.zeros(len(candidates), dtype=float)
            for ci, cidx in enumerate(candidates):
                popts.append(None)
                perrs.append(None)
                chisqrs.append(None)
                try:
                    try:
                        if dg_as_error:
                            # def fit_of_log_weighted(x, a, b):
                            #     yfit = fit_of_log(x, a, b)
                            #     y = sigma_sqr[cidx]
                            #     weight = np.ones_like(yfit)
                            #     weight[10**yfit > y] = sigma_sqr_err_high[cidx][10**yfit > y]
                            #     weight[10**yfit <= y] = sigma_sqr_err_low[cidx][10**yfit <= y]
                            #     return (yfit - np.log10(y))**2/np.log10(weight)**2
                            #
                            # out = fit_model(fit_of_log_weighted if fit_error else fit_of_log, dts[cidx],
                            #                 np.log10(sigma_sqr[cidx]), p0=p0, fit_type=fit_type, maxit=maxit)

                            out = fit_model(fit_of_log, dts[cidx], np.log10(sigma_sqr[cidx]), p0=p0, fit_type=fit_type,
                                            maxit=maxit)
                            yfit = 10**fit_of_log(dts[cidx], *out.beta)
                            weight = np.ones_like(yfit)
                            weight[yfit > sigma_sqr[cidx]] = sigma_sqr_err_high[cidx][yfit > sigma_sqr[cidx]]
                            weight[yfit <= sigma_sqr[cidx]] = sigma_sqr_err_low[cidx][yfit <= sigma_sqr[cidx]]
                            out = fit_model(fit_of_log, dts[cidx], np.log10(sigma_sqr[cidx]), p0=p0, fit_type=fit_type,
                                            maxit=maxit, yerr=weight)

                        else:
                            out = fit_model(fit_of_log, dts[cidx], np.log10(sigma_sqr[cidx]), p0=p0,
                                            fit_type=fit_type, maxit=maxit,
                                            yerr=np.log10(sigma_sqr_err[cidx]) if fit_error else None)
                    except RuntimeError:
                        continue
                    if out.res_var == 0.:
                        continue
                    if min_tau is not None:
                        if out.beta[1] + out.sd_beta[1] < min_tau:
                            continue
                    popts[ci] = out.beta
                    perrs[ci] = out.sd_beta
                    # chisqrs[ci] = out.res_var
                    # chisqrs[ci] = out.sum_square
                    # print(cidx, len(cidx))
                    chisqrs[ci] = np.sum((np.log10(sigma_sqr[cidx]) - fit_of_log(dts[cidx], *out.beta))**2) / len(cidx)
                    if (quality_type == "fixed") and len(unalterable) > 0:
                        quality[ci] = float(len(unalterable)) / np.sum((np.log10(sigma_sqr[unalterable]) -
                                                                        fit_of_log(dts[unalterable], *out.beta))**2)
                    else:
                        quality[ci] = 1. / chisqrs[ci]
                    if quantity_type == "time":
                        quantity[ci] = np.max(dts[cidx]) - np.min(dts[cidx])
                    else:
                        quantity[ci] = len(cidx)
                    if plot_attempts:
                        ax.plot(dt_range, 10 ** (fit_of_log(dt_range, *out.beta)), color="gray", lw=1, linestyle="-.",
                                alpha=0.3)
                except ValueError or RuntimeError:
                    continue
            if np.max(quality) == 0:
                if not silent:
                    pb.tick()
                continue
            quality = quality / np.max(quality)
            quantity = quantity / np.max(quantity)
            if grade_type == "linear":
                grade = quality * grade_weights[0] + quantity * grade_weights[1]
            else:
                grade = np.sqrt((quality * grade_weights[0])**2 + (quantity * grade_weights[1])**2)
            best_idx = np.argmax(grade)
            popt = popts[best_idx]
            perr = perrs[best_idx]
            chisqr = chisqrs[best_idx]
            if popt is None:
                if not silent:
                    pb.tick()
                continue
            cidx = candidates[best_idx]
            if plot_path is not None:
                if double_gauss == "fitted":
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigma_sqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigma_sqr_err[cidx][sigma_type[cidx] == "single"] if plot_error else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigma_sqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigma_sqr_err[cidx][sigma_type[cidx] == "low double"] if plot_error else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigma_sqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigma_sqr_err[cidx][sigma_type[cidx] == "high double"] if plot_error else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                elif double_gauss == "none":
                    ax.errorbar(dts[cidx], sigma_sqr[cidx], yerr=sigma_sqr_err[cidx] if plot_error else None, fmt=".",
                                c="black", markersize=7, alpha=1)
                else:
                    if dg_as_error:
                        yerr = sigma_sqr_err[:, cidx]
                    else:
                        yerr = sigma_sqr_err[cidx]
                    ax.errorbar(dts[cidx], sigma_sqr[cidx], yerr=yerr,
                                fmt=".", markersize=7, c="blue",
                                marker="o")
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr*1e4, r"$10^{-4}$") + "\n"
                if mode == "trans":
                    label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(2 * const.k_B * const.T /
                                                                                 (10 ** popt[0]) * 1e14,
                                                                                 2 * const.k_B * const.T *
                                                                                 np.log(10) / (10 ** popt[0])
                                                                                 * perr[0] * 1e14,
                                                                                 r"$10^{-14}$") + "\n"
                else:
                    label += r"$I$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm$\rm ^2$".format(2 * const.k_B * const.T /
                                                                                          (10 ** popt[0]) * 1e25,
                                                                                          2 * const.k_B * const.T *
                                                                                          np.log(10) / (10 ** popt[0])
                                                                                          * perr[0] * 1e25,
                                                                                          r"$10^{-25}$") + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(abs(popt[1]) * 1e3, abs(perr[1]) * 1e3)
                ax.plot(dt_range, 10 ** (fit_of_log(dt_range, *popt)), color="red", lw=2, linestyle="-", zorder=20,
                        label=label)

            df_out_.loc[df_out_["axis"] == a, mass_column] = 2 * const.k_B * const.T / (10 ** popt[0])
            df_out_.loc[df_out_["axis"] == a, "{}_err".format(mass_column)] =\
                2 * const.k_B * const.T * np.log(10) / (10 ** popt[0]) * perr[0]
            df_out_.loc[df_out_["axis"] == a, "tau"] = abs(popt[1])
            df_out_.loc[df_out_["axis"] == a, "tau_err"] = perr[1]
            df_out_.loc[df_out_["axis"] == a, "chi_sqr"] = chisqr
            df_out_.loc[df_out_["axis"] == a, "n_dots"] = len(cidx)
            df_out_.loc[df_out_["axis"] == a, "t_diff"] = np.max(dts[cidx]) - np.min(dts[cidx])
            if ret_erf:
                use = np.zeros(len(sigma_sqr), dtype=bool)
                use[cidx] = True
                if id_column is not None:
                    df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_ms)) &
                               (df_erf["axis"] == a), "of_used"] = use
                else:
                    df_erf.loc[df_erf["dt"].isin(dts_ms) & (df_erf["axis"] == a), "of_used"] = use

            if plot_path is not None:
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(sigma_unit) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + a,
                                                                   "{}".format(sigma_unit) + r"$^{\rm 2}$"))
                ax.legend(loc="lower right", framealpha=1)
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(time_units))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigma_sqr[~np.isnan(sigma_sqr)]) * 1.1)
                if crop_to_max:
                    top_edge = sigma_sqr[~np.isnan(sigma_sqr)]
                    top_edge = np.max(top_edge[top_edge < max_sigma_sqr])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot_path[i] + "sigma_OF_{}_{}_{}.png".format(mode, a, particle), dpi=dpi)
                plt.close()

            if not silent:
                pb.tick()
        if df_out is None:
            df_out = df_out_
        else:
            df_out = df_out.append(df_out_)
        if prelim_save is not None:
            if part_i/len(particles) > n_prelim_saves * prelim_cutoff:
                df_prelim = move_col(df_out, id_column, 0)
                df_prelim.to_csv(prelim_save, index=False)
                n_prelim_saves += 1
    if prelim_save is not None:
        os.remove(prelim_save)
    if not silent:
        pb.finish()
    if id_column is not None:
        df_out = move_col(df_out, id_column, 0)
    if ret_erf:
        return df_out, df_erf
    return df_out


# Jan 2022
def calc_ornstein_fuerth_old2(df_erf, dts=None, axes=None, particles=None, mode="trans",
                         id_column="particle", mass_column="mass",
                         units=None, fit=None, plot=None, dg=None,
                         temp_path=None, temp_interval=0.1, ret_erf=False, silent=True):
    # =====================
    # Parameter Setup
    # =====================
    units_ = {"sigma": "m",
              "time": "s",
              "time_cvt": 1e-3,
              "mass_mag": 14}
    if mode == "rot":
        units_ = icaps.merge_dicts(units_, {"sigma": "rad", "mass_mag": 25})
        if mass_column == "mass":
            mass_column = "inertia"
    mass_err_column = mass_column + "_err"
    units = icaps.merge_dicts(units_, {} if units is None else units)
    fit_ = {"p0": None,
            "maxit": None,
            "fit_type": 0,
            "fit_err": True,
            "ballistic_bounds": (None, 5),
            "ballistic_fit": 0,
            "max_sigsqr": None,
            "max_err_ratio": 1.,
            "min_tau": None}
    fit = icaps.merge_dicts(fit_, {} if fit is None else fit)
    plot_ = {"path": None,
             "dpi": 600,
             "clear_out": True,
             "errors": True,
             "attempts": False,
             "crop_to_max": True}
    plot = icaps.merge_dicts(plot_, {} if plot is None else plot)
    dg_ = {"apply": False,
           "min_chi_ratio": 3,
           "max_var": 1,
           "min_weight": 0.2,
           "max_dt": 5}
    dg = icaps.merge_dicts(dg_, {} if dg is None else dg)
    if plot["path"] is not None:
        mpl.use("Agg")
    n_temp = 0
    if id_column is None:
        df_erf["particle"] = 0
        id_column = "particle"
    if particles is None:
        particles = np.unique(df_erf[id_column].to_numpy())
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_raw = dts.copy()
    dts = dts.astype(np.float64) * units["time_cvt"]
    dt_space = np.linspace(np.min(dts), np.max(dts), 1000)
    if not ret_erf:
        df_erf = df_erf[df_erf["dt"].isin(dts_raw)]
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    elif isinstance(axes, str):
        axes = [axes]
    if plot["path"] is None:
        plot["attempts"] = False
    else:
        if "{}" in plot["path"]:
            plot["path"] = [plot["path"].format(axis) for axis in axes]
        else:
            plot["path"] = [plot["path"] for axis in axes]
        mk_folders(plot["path"], clear=plot["clear_out"])
    if fit["p0"] is None:
        fit["p0"] = [None, None]
    fit["p0"] = list(fit["p0"])
    if fit["ballistic_fit"] == 0:
        p0_default = [np.log10(2 * const.k_B * const.T / (const.m_0 * const.r_0 ** 2)), 4.8e-3]
    elif fit["ballistic_fit"] == 1:
        p0_default = [np.log10(const.k_B * const.T / (const.m_0 * const.r_0 ** 2)), 4.8e-3]
    else:
        p0_default = [const.k_B * const.T / (const.m_0 * const.r_0 ** 2), 4.8e-3]
    # if fit["p0"][0] is None:
    #     fit["p0"][0] = np.log10(2 * const.k_B * const.T / (const.m_p * const.r_p ** 2))
    # if fit["p0"][1] is None:
    #     fit["p0"][1] = 0.001
    nan_arr = np.zeros(len(particles) * len(axes))
    nan_arr[:] = np.nan
    df_out = pd.DataFrame(data={id_column: np.repeat(particles, len(axes)),
                                "axis": np.tile(axes, len(particles)),
                                mass_column: nan_arr,
                                mass_err_column: nan_arr,
                                "tau": nan_arr,
                                "tau_err": nan_arr,
                                "chisqr_bal": nan_arr,
                                "chisqr": nan_arr,
                                "n_dots": np.zeros(len(particles) * len(axes), dtype=int),
                                "t_diff": nan_arr,
                                "tau0": nan_arr,
                                "chisqr_lin": nan_arr
                                })
    # =====================
    # Start of Loop
    # =====================
    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(axes), "Calculating OF Fits")
    for p, particle in enumerate(particles):
        df_erf_ = df_erf[(df_erf[id_column] == particle) & (df_erf["dt"].isin(dts_raw))]
        for a, axis in enumerate(axes):
            ax = None
            if plot["path"] is not None:
                _, ax = plt.subplots()
                if fit["max_sigsqr"] is not None:
                    ax.axhline(fit["max_sigsqr"], lw=2, ls="--", c="black")
            sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma"].to_numpy())
            sig_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_err"].to_numpy())
            sigsqr = sig**2
            sigsqr_err = 2 * sig_err * sig
            chisqr_erf = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "chisqr"].to_numpy())
            # =====================
            # Sequential Fitting
            # =====================
            candidates = icaps.brownian.parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["ballistic_bounds"],
                                          adapt_margins=(0, 0), n_min=1, max_sigsqr=fit["max_sigsqr"],
                                          max_err_ratio=fit["max_err_ratio"])
            if len(candidates) == 0:
                pb.tick()
                continue
            cidx = candidates[0]
            log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None
            p0 = [None, None]
            p0[1] = fit["p0"][1] if fit["p0"][1] is not None else p0_default[1]
            if fit["ballistic_fit"] == 0:
                p0[0] = 2 * np.log10(const.k_B * const.T / fit["p0"][0]) if fit["p0"][0] is not None else p0_default[0]
                out = fit_model(fit_of_log, dts[cidx], np.log10(sigsqr[cidx]), p0=p0, fit_type=fit["fit_type"],
                                maxit=fit["maxit"], yerr=log_err)
                tau0 = abs(out.beta[1])
                mass = 2 * const.k_B * const.T / (10 ** out.beta[0])
                mass_err = 2 * const.k_B * const.T * np.log(10) / (10 ** out.beta[0]) * out.sd_beta[0]
                chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - fit_of_log(dts[cidx], *out.beta)) ** 2) / len(cidx)
            elif fit["ballistic_fit"] == 1:
                p0[0] = np.log10(const.k_B * const.T / fit["p0"][0]) if fit["p0"][0] is not None else p0_default[0]
                out = fit_model(icaps.fit_slope2, np.log10(dts[cidx]), np.log10(sigsqr[cidx]),
                                p0=[p0[0]], fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err)
                tau0 = 0
                mass = const.k_B * const.T / (10 ** out.beta[0])
                mass_err = const.k_B * const.T * np.log(10) / (10 ** out.beta[0]) * out.sd_beta[0]
                chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - icaps.fit_slope2(np.log10(dts[cidx]), *out.beta)) ** 2) / len(cidx)
            else:
                p0[0] = const.k_B * const.T / fit["p0"][0] if fit["p0"][0] is not None else p0_default[0]
                out = fit_model(icaps.fit_taylor4, dts[cidx], sigsqr[cidx], p0=(p0[0], p0[1]), fit_type=fit["fit_type"],
                                maxit=fit["maxit"], yerr=sigsqr_err[cidx])
                tau0 = abs(out.beta[1])
                mass = const.k_B * const.T / out.beta[0]
                mass_err = mass * out.sd_beta[0] / out.beta[0]
                chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - np.log10(icaps.fit_taylor4(dts[cidx], *out.beta))) ** 2)

            candidates = icaps.brownian.parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=(None, None),
                                          adapt_margins=(0, 0), n_min=1, max_sigsqr=fit["max_sigsqr"],
                                          max_err_ratio=fit["max_err_ratio"])
            # print(dts_raw[candidates[0]])
            cidx = candidates[0]
            log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None

            def fit_of_here(x0, x1):
                return fit_of_log(x0, np.log10(2 * const.k_B * const.T / mass), x1)

            # print(p0)
            out = fit_model(fit_of_here, dts[cidx], np.log10(sigsqr[cidx]),
                            p0=[p0[1]], fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err)
            tau = abs(out.beta[0])
            tau_err = out.sd_beta[0]
            chisqr_tau = np.sum((np.log10(sigsqr[cidx]) - fit_of_here(dts[cidx], *out.beta)) ** 2) / len(cidx)
            t_diff = np.max(dts[cidx]) - np.min(dts[cidx])

            # Investigate linearity of all data to check for "strangeness"
            out = fit_model(icaps.fit_slope2, np.log10(dts[cidx]), np.log10(sigsqr[cidx]),
                            p0=[p0_default[0] / 2],
                            fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_lin = np.sum((np.log10(sigsqr[cidx]) - icaps.fit_slope2(np.log10(dts[cidx]), *out.beta)) ** 2) / len(cidx)
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "chisqr_lin"] = chisqr_lin

            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau0"] = tau0
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), mass_column] = mass
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), mass_err_column] = mass_err
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau"] = tau
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "tau_err"] = tau_err
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "chisqr_bal"] = chisqr_mass
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "chisqr"] = chisqr_tau
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "n_dots"] = len(cidx)
            df_out.loc[(df_out[id_column] == particle) & (df_out["axis"] == axis), "t_diff"] = t_diff
            if ret_erf:
                use = np.zeros(len(sigsqr), dtype=bool)
                use[cidx] = True
                df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                           (df_erf["axis"] == axis), "of_used"] = use
            # =====================
            # Plotting
            # =====================
            if plot["path"] is not None:
                if dg["apply"]:
                    sigma_type = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigsqr[sigma_type == "single"],
                                yerr=sigsqr_err[sigma_type == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "low double"], sigsqr[sigma_type == "low double"],
                                yerr=sigsqr_err[sigma_type == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "high double"], sigsqr[sigma_type == "high double"],
                                yerr=sigsqr_err[sigma_type == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigsqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigsqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigsqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                else:
                    ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx], sigsqr[cidx], yerr=sigsqr_err[cidx] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_mass * 1e4, r"$10^{-4}$") + "\n"
                if mode == "trans":
                    label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                                                                                 mass_err * 10 ** units["mass_mag"],
                                                                                 r"$10^{{{}}}$".format(
                                                                                     -units["mass_mag"]))
                else:
                    label += r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                                                                                    mass_err * 10 ** units["mass_mag"],
                                                                                    r"$10^{{{}}}$".format(
                                                                                        -units["mass_mag"]))
                # ax.plot(dt_space, 10 ** fit_slope2(np.log10(dt_space), np.log10(const.k_B * const.T / mass)),
                #         c="red", lw=2, ls="--", zorder=20, label=label)
                if fit["ballistic_fit"] == 0:
                    ax.plot(dt_space, 10 ** fit_of_log(dt_space, np.log10(2 * const.k_B * const.T / mass), tau0),
                            c="red", lw=2, ls="--", zorder=20, label=label)
                elif fit["ballistic_fit"] == 2:
                    ax.plot(dt_space, icaps.fit_taylor4(dt_space, const.k_B * const.T / mass, tau0),
                            c="red", lw=2, ls="--", zorder=20, label=label)
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_tau * 1e4, r"$10^{-4}$") + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(tau * 1e3, tau_err * 1e3)
                ax.plot(dt_space, 10 ** fit_of_log(dt_space, np.log10(2 * const.k_B * const.T / mass), tau),
                        c="red", lw=2, ls="-", zorder=20, label=label)
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.legend(loc="lower right", framealpha=1)
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot["path"][a] + "sigma_OF_{}_{}_{}.png".format(mode, axis, particle), dpi=plot["dpi"])
                plt.close()
            if not silent:
                pb.tick()
        if temp_path is not None:
            if p/len(particles) > n_temp * temp_interval:
                df_prelim = move_col(df_out, id_column, 0)
                df_prelim.to_csv(temp_path, index=False)
                n_temp += 1
    if temp_path is not None:
        os.remove(temp_path)
    if not silent:
        pb.finish()
    if (len(df_out[id_column].unique()) == 1) and (df_out[id_column].unique()[0] == 0):
        df_out.drop(columns=[id_column], inplace=True)
    if ret_erf:
        return df_out, df_erf
    else:
        return df_out


# Feb 2022 (current)
def calc_ornstein_fuerth_old3(df_erf, dts=None, axes=None, particles=None, mode="trans",
                              id_column="particle", mass_column="mass",
                              units=None, fit=None, plot=None, double_gauss=False, sigma_diff=False,
                              temp_path=None, temp_interval=0.1, ret_erf=False, silent=True):
    # =====================
    # Parameter Setup
    # =====================
    units_ = {"sigma": "m",
              "time": "s",
              "time_cvt": 1e-3,
              "mass_mag": 14}
    if mode == "rot":
        units_ = icaps.merge_dicts(units_, {"sigma": "rad", "mass_mag": 25})
        if mass_column == "mass":
            mass_column = "inertia"
    mass_err_column = mass_column + "_err"
    units = icaps.merge_dicts(units_, {} if units is None else units)
    fit_ = {"p0": None,
            "maxit": None,
            "fit_type": 0,
            "fit_err": True,
            "bounds": (None, None),
            "ballistic_bounds": (None, 5),
            "ballistic_fit": 0,
            "max_sigsqr": None,
            "max_err_ratio": 1.,
            "min_tau": None}
    fit = icaps.merge_dicts(fit_, {} if fit is None else fit)
    plot_ = {"path": None,
             "dpi": 600,
             "clear_out": True,
             "errors": True,
             "attempts": False,
             "crop_to_max": True}
    plot = icaps.merge_dicts(plot_, {} if plot is None else plot)
    if plot["path"] is not None:
        mpl.use("Agg")
    n_temp = 0
    if id_column is None:
        df_erf["particle"] = 0
        id_column = "particle"
    if particles is None:
        particles = np.unique(df_erf[id_column].to_numpy())
    if dts is None:
        dts = np.unique(df_erf["dt"].to_numpy())
    dts_raw = dts.copy()
    dts = dts.astype(np.float64) * units["time_cvt"]
    dt_space = np.linspace(np.min(dts), np.max(dts), 1000)
    if not ret_erf:
        df_erf = df_erf[df_erf["dt"].isin(dts_raw)]
    df_erf["of_used"] = False
    if axes is None:
        axes = np.unique(df_erf["axis"].to_numpy())
    elif isinstance(axes, str):
        axes = [axes]
    if plot["path"] is None:
        plot["attempts"] = False
    else:
        if "{}" in plot["path"]:
            plot["path"] = [plot["path"].format(axis) for axis in axes]
        else:
            plot["path"] = [plot["path"] for axis in axes]
        mk_folders(plot["path"], clear=plot["clear_out"])
    if fit["p0"] is None:
        fit["p0"] = [None, None]
    fit["p0"] = list(fit["p0"])
    p0_m = const.m_0
    if mode == "rot":
        p0_m *= const.r_0 ** 2
    fit["p0"][0] = p0_m if fit["p0"][0] is None else fit["p0"][0]
    fit["p0"][1] = 4.8e-3 if fit["p0"][1] is None else fit["p0"][1]

    nan_arr = np.zeros(len(particles) * len(axes))
    nan_arr[:] = np.nan
    df_out = pd.DataFrame(data={id_column: np.repeat(particles, len(axes)),
                                "axis": np.tile(axes, len(particles)),
                                mass_column: nan_arr,
                                mass_err_column: nan_arr,
                                "tau": nan_arr,
                                "tau_err": nan_arr,
                                "chisqr_bal": nan_arr,
                                "chisqr": nan_arr,
                                "n_dots": np.zeros(len(particles) * len(axes), dtype=int),
                                "t_diff": nan_arr,
                                "tau0": nan_arr,
                                "chisqr_lin": nan_arr
                                })
    # =====================
    # Start of Loop
    # =====================
    pb = None
    if not silent:
        pb = ProgressBar(len(particles) * len(axes), "Calculating OF Fits")
    for p, particle in enumerate(particles):
        df_erf_ = df_erf[(df_erf[id_column] == particle) & (df_erf["dt"].isin(dts_raw))]
        for a, axis in enumerate(axes):
            idx_out = df_out.index[(df_out[id_column] == particle) & (df_out["axis"] == axis)]
            ax = None
            if plot["path"] is not None:
                _, ax = plt.subplots()
                if fit["max_sigsqr"] is not None:
                    ax.axhline(fit["max_sigsqr"], lw=2, ls="--", c="black")
            if sigma_diff:
                sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_diff"].to_numpy())
                sig_err = np.zeros_like(sig)
            else:
                sig = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma"].to_numpy())
                sig_err = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_err"].to_numpy())
            sigsqr = sig**2
            sigsqr_err = 2 * sig_err * sig
            chisqr_erf = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "chisqr"].to_numpy())
            # =====================
            # Sequential Fitting
            # =====================
            candidates = icaps.brownian.parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["ballistic_bounds"],
                                          adapt_margins=(0, 0), n_min=1, max_sigsqr=fit["max_sigsqr"],
                                          max_err_ratio=fit["max_err_ratio"])
            if len(candidates) == 0:
                pb.tick()
                continue
            cidx = candidates[0]
            log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None
            if fit["ballistic_fit"] == 0:
                out = fit_model(fit_of_log, dts[cidx], np.log10(sigsqr[cidx]),
                                yerr=log_err, p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
                tau0 = abs(out.beta[1])
                mass = out.beta[0]
                mass_err = out.sd_beta[0]
                chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - fit_of_log(dts[cidx], *out.beta)) ** 2) / len(cidx)
            elif fit["ballistic_fit"] == 1:
                out = fit_model(icaps.fit_slope2, np.log10(dts[cidx]), np.log10(sigsqr[cidx]),
                                yerr=log_err, p0=[fit["p0"][0]], fit_type=fit["fit_type"], maxit=fit["maxit"])
                tau0 = 0
                mass = out.beta[0]
                mass_err = out.sd_beta[0]
                chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - icaps.fit_slope2(np.log10(dts[cidx]), *out.beta)) ** 2) / len(cidx)
            else:
                out = fit_model(icaps.fit_of_taylor4, dts[cidx], sigsqr[cidx],
                                yerr=sigsqr_err[cidx], p0=fit["p0"], fit_type=fit["fit_type"], maxit=fit["maxit"])
                tau0 = abs(out.beta[1])
                mass = out.beta[0]
                mass_err = out.sd_beta[0]
                chisqr_mass = np.sum((np.log10(sigsqr[cidx]) - np.log10(icaps.fit_of_taylor4(dts[cidx], *out.beta))) ** 2)

            candidates = icaps.brownian.parse_candidates(dts_raw, sigsqr, sigsqr_err, fit_bounds=fit["bounds"],
                                          adapt_margins=(0, 0), n_min=1, max_sigsqr=fit["max_sigsqr"],
                                          max_err_ratio=fit["max_err_ratio"])
            # print(dts_raw[candidates[0]])
            cidx = candidates[0]
            log_err = np.log10(sigsqr_err[cidx]) / np.log10(sigsqr[cidx]) / np.log(10) if fit["fit_err"] else None

            def fit_of_here(x0, x1):
                return fit_of_log(x0, mass, x1)

            out = fit_model(fit_of_here, dts[cidx], np.log10(sigsqr[cidx]),
                            p0=[fit["p0"][1]], fit_type=fit["fit_type"], maxit=fit["maxit"], yerr=log_err)
            tau = abs(out.beta[0])
            tau_err = out.sd_beta[0]
            chisqr_tau = np.sum((np.log10(sigsqr[cidx]) - fit_of_here(dts[cidx], *out.beta)) ** 2) / len(cidx)
            t_diff = np.max(dts[cidx]) - np.min(dts[cidx])

            # Investigate linearity of all data to check for "strangeness"
            out = fit_model(icaps.fit_slope2, np.log10(dts[cidx]), np.log10(sigsqr[cidx]),
                            fit_type=fit["fit_type"], maxit=fit["maxit"])
            chisqr_lin = np.sum((np.log10(sigsqr[cidx]) - icaps.fit_slope2(np.log10(dts[cidx]), *out.beta)) ** 2) / len(cidx)
            df_out.loc[idx_out, "chisqr_lin"] = chisqr_lin

            df_out.loc[idx_out, "tau0"] = tau0
            df_out.loc[idx_out, mass_column] = mass
            df_out.loc[idx_out, mass_err_column] = mass_err
            df_out.loc[idx_out, "tau"] = tau
            df_out.loc[idx_out, "tau_err"] = tau_err
            df_out.loc[idx_out, "chisqr_bal"] = chisqr_mass
            df_out.loc[idx_out, "chisqr"] = chisqr_tau
            df_out.loc[idx_out, "n_dots"] = len(cidx)
            df_out.loc[idx_out, "t_diff"] = t_diff
            if ret_erf:
                use = np.zeros(len(sigsqr), dtype=bool)
                use[cidx] = True
                df_erf.loc[(df_erf["particle"] == particle) & (df_erf["dt"].isin(dts_raw)) &
                           (df_erf["axis"] == axis), "of_used"] = use
            # =====================
            # Plotting
            # =====================
            if plot["path"] is not None:
                if double_gauss:
                    sigma_type = np.squeeze(df_erf_.loc[df_erf_["axis"] == axis, "sigma_type"].to_numpy())
                    ax.errorbar(dts[sigma_type == "single"], sigsqr[sigma_type == "single"],
                                yerr=sigsqr_err[sigma_type == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "low double"], sigsqr[sigma_type == "low double"],
                                yerr=sigsqr_err[sigma_type == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[sigma_type == "high double"], sigsqr[sigma_type == "high double"],
                                yerr=sigsqr_err[sigma_type == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "single"],
                                sigsqr[cidx][sigma_type[cidx] == "single"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "single"] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "low double"],
                                sigsqr[cidx][sigma_type[cidx] == "low double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "low double"] if plot["error"] else None,
                                fmt="v", c="black", markersize=7, alpha=1)
                    ax.errorbar(dts[cidx][sigma_type[cidx] == "high double"],
                                sigsqr[cidx][sigma_type[cidx] == "high double"],
                                yerr=sigsqr_err[cidx][sigma_type[cidx] == "high double"] if plot["error"] else None,
                                fmt="^", c="black", markersize=7, alpha=1)
                else:
                    ax.errorbar(dts, sigsqr, yerr=sigsqr_err if plot["error"] else None,
                                fmt="o", c="black", markersize=7, markerfacecolor="none", alpha=0.5)
                    ax.errorbar(dts[cidx], sigsqr[cidx], yerr=sigsqr_err[cidx] if plot["error"] else None,
                                fmt="o", c="black", markersize=7, alpha=1)
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_mass * 1e4, r"$10^{-4}$") + "\n"
                if mode == "trans":
                    label += r"$m$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kg".format(mass * 10 ** units["mass_mag"],
                                                                                 mass_err * 10 ** units["mass_mag"],
                                                                                 r"$10^{{{}}}$".format(
                                                                                     -units["mass_mag"]))
                else:
                    label += r"$MoI$ = ({:.2f} $\pm$ {:.2f}) $\cdot$ {} kgm".format(mass * 10 ** units["mass_mag"],
                                                                                    mass_err * 10 ** units["mass_mag"],
                                                                                    r"$10^{{{}}}$".format(
                                                                                        -units["mass_mag"]))
                if fit["ballistic_fit"] == 0:
                    ax.plot(dt_space, 10 ** fit_of_log(dt_space, mass, tau0),
                            c="red", lw=2, ls="--", zorder=20, label=label)
                elif fit["ballistic_fit"] == 2:
                    ax.plot(dt_space, icaps.fit_of_taylor4(dt_space, mass, tau0),
                            c="red", lw=2, ls="--", zorder=20, label=label)
                label = r"$\chi^2$ = {:.2f} $\cdot$ {}".format(chisqr_tau * 1e4, r"$10^{-4}$") + "\n"
                label += r"$\tau$ = ({:.2f} $\pm$ {:.2f}) ms".format(tau * 1e3, tau_err * 1e3)
                ax.plot(dt_space, 10 ** fit_of_log(dt_space, mass, tau),
                        c="red", lw=2, ls="-", zorder=20, label=label)
                if mode == "rot":
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0], "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                else:
                    ax.set_ylabel(r"$\sigma_{{{}}}^2$ ({})".format(mode[0] + ", " + axis,
                                                                   "{}".format(units["sigma"]) + r"$^{\rm 2}$"))
                ax.legend(loc="lower right", framealpha=1)
                ax.set_xlabel(r"$\Delta t$" + " ({})".format(units["time"]))
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(left=np.min(dts) * 0.95)
                ax.set_ylim(top=np.max(sigsqr[~np.isnan(sigsqr)]) * 1.1)
                if plot["crop_to_max"]:
                    top_edge = sigsqr[~np.isnan(sigsqr)]
                    top_edge = np.max(top_edge[top_edge < fit["max_sigsqr"]])
                    ax.set_ylim(top=top_edge * 1.1)
                plt.savefig(plot["path"][a] + "sigma_OF_{}_{}_{}.png".format(mode, axis, particle), dpi=plot["dpi"])
                plt.close()
            if not silent:
                pb.tick()
        if temp_path is not None:
            if p/len(particles) > n_temp * temp_interval:
                df_prelim = move_col(df_out, id_column, 0)
                df_prelim.to_csv(temp_path, index=False)
                n_temp += 1
    if temp_path is not None:
        os.remove(temp_path)
    if not silent:
        pb.finish()
    if (len(df_out[id_column].unique()) == 1) and (df_out[id_column].unique()[0] == 0):
        df_out.drop(columns=[id_column], inplace=True)
    if ret_erf:
        return df_out, df_erf
    else:
        return df_out
