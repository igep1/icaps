import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"


def main():
    df_erf_old = pd.read_csv(project_path + "plots/_backup/ERF_Rot_flagged.csv")
    df_erf_new = pd.read_csv(project_path + "ERF_Rot_flagged.csv")
    df_erf_new["upper_bound"] = df_erf_old["upper_bound"]
    df_erf_new.to_csv(project_path + "ERF_Rot_flagged.csv", index=False)


if __name__ == '__main__':
    main()


