import icaps
import fnmatch
import os


def main():
    # path = "E:/icaps/data/Scan6/OS-1 Camera094582.bmp"
    # path = "E:/icaps/data/Scan6/OS-1 Camera*.bmp"
    # path = "E:/icaps/data/Scan6/*.bmp"
    # path = "E:/icaps/data/Scan6/OS-1 Camera(005000:).bmp"
    # path = "E:/icaps/data/Scan6/OS-1 Camera[90:100].bmp"
    # path = "E:/icaps/data/Scan6/*(8938:9900).bmp"
    # path = "E:/icaps/data/Scan6/ffc/"
    # path = "E:/icaps/data/Scan6/ffc/" + "Os7-S1 Camera(130430:130432).bmp"
    # path = "E:/icaps/data/Scan6/ffc/" + "Os7-S1 Camera(130430:130432).bmp"
    path = "E:/icaps/data/Scan6/ffc/" + "(:133000).bmp"
    # path = "E:/icaps/data/Scan6/ffc/" + "Os7-S1 Camera[30:].bmp"
    # path = "E:/icaps/texus56_SRE_20191115/LDM/Flight1511_002/"
    # ret = icaps.parse_formatted_path(path)
    # # print(ret)
    # ret2 = icaps.strip_files(os.listdir(ret[0]), start=ret[4], stop=ret[5], frmt=ret[3], use_index=ret[6], prefix=ret[1], suffix=ret[2])
    # print(ret2)
    # print(fnmatch.fnmatch("prefixblasuffix.bmp", "prefix*"))
    # print(os.listdir(path))
    res = icaps.get_files(path, omit_blanks=True)
    print(res)
    print(len(res))


if __name__ == '__main__':
    main()





