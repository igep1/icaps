import icaps
import os
import cv2

project_path = "data/Agg1/"
in_path1 = project_path + "diff/"
in_path2 = project_path + "diff_ent/"
out_path_imgs = project_path + "diff_ent_comp/"
out_path_mov = project_path + "movies/diff_ent_comp.mp4"

icaps.mk_folder(out_path_imgs, clear=True)

files1 = os.listdir(in_path1)
files2 = os.listdir(in_path2)
for i, file in enumerate(files1):
    img1 = icaps.load_img(in_path1 + files1[i], flags=3)
    img2 = icaps.load_img(in_path2 + files2[i], flags=3)
    img = icaps.arrage_imgs_in_grid([img1, img2])
    cv2.imwrite(out_path_imgs + file, img)

