import icaps


plane = "XZ"
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
texus_path = drive_letter + ":/icaps/texus56_SRE_20191115/OOS/" + plane + "/"
texus_path = texus_path + ("000000000_10h_11m_09s_936ms/" if plane == "XZ" else "000000000_10h_11m_11s_462ms/")
project_path = drive_letter + ":/icaps/data/OOS_" + plane + "/"
orig_path = project_path + "orig/"
strip_path = project_path + "orig_strip/"


def main():
    # icaps.copy_files(texus_path, orig_path, show_progress=True)
    # files, _ = icaps.get_files(orig_path)
    # files = icaps.generate_files(start=0, stop=18436, prefix=None, frmt="bmp", digits=5)
    # pb = icaps.ProgressBar(len(files), title="Stripping OOS Images")
    # for file in files:
    #     img = icaps.load_img(orig_path + file)
    #     img = icaps.crop(img, tl=(0, 120), br=(1024, 670))
    #     icaps.save_img(img, strip_path + file)
    #     pb.tick()
    # pb.finish()

    movie_path = project_path + "supercluster3/"
    icaps.mk_folder(movie_path)
    files = icaps.generate_files(start=6828, stop=7460, prefix=None, frmt="bmp", digits=5)
    for file in files:
        img = icaps.load_img(strip_path + file)
        img = icaps.crop(img, tl=(250, 430), br=(410, 550))
        img = icaps.enlarge_pixels(img, 8)
        icaps.save_img(img, movie_path + file)
    icaps.mk_movie(movie_path, project_path + "supercluster3.mp4", fps=30)


if __name__ == '__main__':
    main()

