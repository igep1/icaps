import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# oos_plane = "XZ"
oos_plane = "YZ"
collision_id = 2
orig_path = icaps.get_texus_path(drive_letter + ":/icaps/", "OOS" + oos_plane)
project_path = drive_letter + ":/icaps/data/Collisions/OOS/"
coll_path = project_path + "coll{}/".format(collision_id)
mark_path = coll_path + "mark_{}/".format(oos_plane)
part_path = coll_path + "part_{}/".format(oos_plane)
csv_track_path = coll_path + "wacca_{}.csv".format(oos_plane)
csv_part_path = coll_path + "wacca_part_{}.csv".format(oos_plane)
info_path = coll_path + "info_{}.txt".format(oos_plane)
icaps.mk_folders([coll_path, mark_path, part_path])

with open(info_path) as info:
    lines = info.readlines()
    start_frame = int(float(lines[2].split("\t")[1]))
    stop_frame = int(float(lines[3].split("\t")[1]))
    center_pos = lines[4].split("\t")[1].split(", ")
    center_pos = np.array((int(float(center_pos[0])), int(float(center_pos[1]))))
    buffer_size = int(float(lines[5].split("\t")[1]))
    enlarge_mark = int(float(lines[6].split("\t")[1]))

    partners = []
    for line in lines[8:]:
        lsplit = line.replace("\n", "").split(", ")
        partner = [int(float(lsplit[0])), lsplit[1], int(float(lsplit[2])), lsplit[3]]
        partners.append(partner)

df = None
pb = icaps.ProgressBar(stop_frame - start_frame + 1, "WACCA")
for frame in range(start_frame, stop_frame + 1):
    img = icaps.load_img(orig_path + icaps.generate_file(frame, prefix=None, digits=5))
    img = icaps.crop(img, tl=center_pos-buffer_size, br=center_pos+buffer_size)
    df_ = icaps.wacca(img, frame, thresh1=50, thresh2=120)
    if df is None:
        df = df_.copy()
    else:
        df = df.append(df_)
    pb.tick()
pb.finish()
df = icaps.link(df, memory=3, search_range=3)
df = icaps.filter_edge(df, shape=(buffer_size*2, buffer_size*2))
df.to_csv(csv_track_path, index=False)

df = pd.read_csv(csv_track_path)
pb = icaps.ProgressBar(stop_frame - start_frame + 1, "Marking")
for frame in range(start_frame, stop_frame + 1):
    img = icaps.load_img(orig_path + icaps.generate_file(frame, prefix=None, digits=5))
    img = icaps.crop(img, tl=center_pos - buffer_size, br=center_pos + buffer_size)
    mark_img = icaps.draw_labelled_bbox_bulk(img, df.loc[df["frame"] == frame, ["bx", "by", "bw", "bh"]].to_numpy(),
                                             labels=df.loc[df["frame"] == frame, "particle"].to_numpy(),
                                             font_scale=1, enlarge=enlarge_mark)
    icaps.save_img(mark_img, mark_path + icaps.generate_file(frame, prefix=None, digits=5))
    pb.tick()
pb.finish()
icaps.mk_movie(mark_path, coll_path + "mark_{}.mp4".format(oos_plane))

df = pd.read_csv(csv_track_path)
partner_ids = np.unique([partner[0] for partner in partners])
df = df[df["particle"].isin(partner_ids)]
for partner in partners:
    if partner[1] == "<":
        df.loc[(df["particle"] == partner[0]) & (df["frame"] < partner[2]), "partner"] = partner[3]
    else:
        df.loc[(df["particle"] == partner[0]) & (df["frame"] >= partner[2]), "partner"] = partner[3]
df.to_csv(csv_part_path, index=False)

pb = icaps.ProgressBar(stop_frame - start_frame + 1, "Partners")
for frame in range(start_frame, stop_frame + 1):
    img = icaps.load_img(orig_path + icaps.generate_file(frame, prefix=None, digits=5))
    img = icaps.crop(img, tl=center_pos - buffer_size, br=center_pos + buffer_size)
    part_img = icaps.draw_labelled_bbox_bulk(img, df.loc[df["frame"] == frame, ["bx", "by", "bw", "bh"]].to_numpy(),
                                             labels=df.loc[df["frame"] == frame, "partner"].to_numpy(),
                                             font_scale=1, enlarge=enlarge_mark)
    icaps.save_img(part_img, part_path + icaps.generate_file(frame, prefix=None, digits=5))
    pb.tick()
pb.finish()
icaps.mk_movie(part_path, coll_path + "part_{}.mp4".format(oos_plane))


