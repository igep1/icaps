import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os

project = "LDM_ROT5"
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/{}/".format(project)
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
particle_rot_path = project_path + "particles_rot/"
plot_path = project_path + "plots/"
plot_dg_path = project_path + "plots_dg/"
thilo_path = project_path + "thilo/"
thilo_fname = "distribution_values_center_uncertainty_gen_{}.txt"

csv_phases = project_path + "Phases_Hand.csv"
csv_track_all = project_path + "Tracks_all.csv"
csv_track = project_path + "Tracks_PE1L100.csv"
csv_rot_of = project_path + "OF_Rot.csv"
csv_rot_usable = project_path + "usable_rot_OF_particles.txt"
csv_trans_disp = project_path + "DISP_Trans.csv"
csv_trans_erf = project_path + "ERF_Trans.csv"
csv_trans_erf_flagged = project_path + "ERF_Trans_flagged.csv"
csv_trans_of_sg = project_path + "OF_Trans_SG.csv"
csv_final_sg = project_path + "OF_Final_SG.csv"
csv_trans_erf_dg = project_path + "ERF_Trans_DG.csv"
csv_trans_erf_noise = project_path + "ERF_Trans_Noise.csv"
csv_trans_of_dg = project_path + "OF_Trans_DG.csv"
csv_final_dg = project_path + "OF_Final_DG.csv"

df_erf = pd.read_csv(csv_trans_erf)
print(len(df_erf))
print(len(df_erf["sigma"]))
print(len(df_erf["sigma_diff"]))
_, ax = plt.subplots()
ax.scatter(df_erf.loc[df_erf["dt"] == 1, "sigma"], df_erf.loc[df_erf["dt"] == 1, "sigma_diff"] / 2, c="black", s=1)
print(len(df_erf.loc[df_erf["dt"] == 1, "sigma"].dropna()))
print(len(df_erf.loc[df_erf["dt"] == 1, "sigma_diff"].dropna()))
plt.show()

_, ax = plt.subplots()
y = df_erf.loc[df_erf["dt"] == 1, "sigma_diff"] / 2 / df_erf.loc[df_erf["dt"] == 1, "sigma"]
bin_size = 0.02 / 10
bin_lim = np.ceil(np.max(np.abs(y)) * 100) / 100
bins = np.arange(bin_size/2, bin_lim + bin_size, bin_size)
bins = np.append(-bins[::-1], bins)
hist, _ = np.histogram(y, bins=bins)
# hist = hist / np.max(hist)
ax.bar(bins[:-1] + bin_size/2, hist, width=bin_size*0.9, color="black")
# ax.set_xlim(-bin_lim - bin_size, bin_lim + bin_size)
# ax.set_ylim(0, 1.05 * np.max(hist))
ax.set_ylim(0, 1.05 * np.max(hist))
plt.tight_layout()
plt.show()
# plt.savefig(plot_path + "Mass_RelErr_Hist.png", dpi=dpi)
# plt.close()



