import icaps
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")

project_path = drive_letter + ":/icaps/data/LDM_ROT5/"
csv1_path = project_path + "OF_Chi0.005Ex_BallisticOF.csv"
csv2_path = project_path + "OF_Chi0.005Ex_BallisticT2.csv"
df1 = pd.read_csv(csv1_path)
df2 = pd.read_csv(csv2_path)

df1["mass"] = np.mean([df1["mass_x"], df1["mass_y"]], axis=0)
df1["mass_err"] = 1 / 2 * np.sqrt(df1["mass_x_err"] ** 2 + df1["mass_y_err"] ** 2)
df1["tau"] = np.mean([df1["tau_x"], df1["tau_y"]], axis=0)
df1["tau_err"] = 1 / 2 * np.sqrt(df1["tau_x_err"] ** 2 + df1["tau_y_err"] ** 2)
print(len(df1["particle"].unique()))
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df1 = df1[df1["mass_x_err"] / df1["mass_x"] <= max_ratio]
    df1 = df1[df1["mass_y_err"] / df1["mass_y"] <= max_ratio]
    print(len(df1["particle"].unique()))
    df1 = df1[df1["tau_x_err"] / df1["tau_x"] <= max_ratio]
    df1 = df1[df1["tau_y_err"] / df1["tau_y"] <= max_ratio]
    print(len(df1["particle"].unique()))

df2["mass"] = np.mean([df2["mass_x"], df2["mass_y"]], axis=0)
df2["mass_err"] = 1 / 2 * np.sqrt(df2["mass_x_err"] ** 2 + df2["mass_y_err"] ** 2)
df2["tau"] = np.mean([df2["tau_x"], df2["tau_y"]], axis=0)
df2["tau_err"] = 1 / 2 * np.sqrt(df2["tau_x_err"] ** 2 + df2["tau_y_err"] ** 2)
print(len(df2["particle"].unique()))
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df2 = df2[df2["mass_x_err"] / df2["mass_x"] <= max_ratio]
    df2 = df2[df2["mass_y_err"] / df2["mass_y"] <= max_ratio]
    print(len(df2["particle"].unique()))
    df2 = df2[df2["tau_x_err"] / df2["tau_x"] <= max_ratio]
    df2 = df2[df2["tau_y_err"] / df2["tau_y"] <= max_ratio]
    print(len(df2["particle"].unique()))

plot_path = project_path + "/plots/mixed_results/"
icaps.mk_folder(plot_path)

particles1 = df1["particle"].unique()
particles2 = df2["particle"].unique()
particles = particles1[np.isin(particles1, particles2)]
df1 = df1[df1["particle"].isin(particles)]
df2 = df2[df2["particle"].isin(particles)]

_, ax = plt.subplots()
ax.errorbar(df1["mass_x"], df2["mass_x"], xerr=df1["mass_x_err"], yerr=df2["mass_x_err"], markersize=5,
            fmt=".", c="black")
ax.set_xlabel(r"$m_{\rm x,\,OF}$ (kg)")
ax.set_ylabel(r"$m_{\rm x,\,T2}$ (kg)")
xlim = ax.get_xlim()
ylim = ax.get_ylim()
xylim = (np.min([xlim[0], ylim[0]]), np.max([xlim[1], ylim[1]]))
ax.plot(xylim, xylim, ls="--", lw=1, color="gray", zorder=-1)
ax.set_xlim(xylim)
ax.set_ylim(xylim)
plt.savefig(plot_path + "MassX_OF_T2.png", dpi=600)

_, ax = plt.subplots()
ax.errorbar(df1["mass_y"], df2["mass_y"], xerr=df1["mass_y_err"], yerr=df2["mass_y_err"], markersize=5,
            fmt=".", c="black")
ax.set_xlabel(r"$m_{\rm y,\,OF}$ (kg)")
ax.set_ylabel(r"$m_{\rm y,\,T2}$ (kg)")
xlim = ax.get_xlim()
ylim = ax.get_ylim()
xylim = (np.min([xlim[0], ylim[0]]), np.max([xlim[1], ylim[1]]))
ax.plot(xylim, xylim, ls="--", lw=1, color="gray", zorder=-1)
ax.set_xlim(xylim)
ax.set_ylim(xylim)
plt.savefig(plot_path + "MassY_OF_T2.png", dpi=600)

_, ax = plt.subplots()
ax.errorbar(df1["tau_x"] * 1e3, df2["tau_x"] * 1e3,
            xerr=df1["tau_x_err"] * 1e3, yerr=df2["tau_x_err"] * 1e3,
            markersize=5, fmt=".", c="black")
ax.set_xlabel(r"$\tau_{\rm x,\,OF}$ (ms)")
ax.set_ylabel(r"$\tau_{\rm x,\,T2}$ (ms)")
xlim = ax.get_xlim()
ylim = ax.get_ylim()
xylim = (np.max([np.min([xlim[0], ylim[0]]), 0]), np.min([np.max([xlim[1], ylim[1]]), 30]))
ax.plot(xylim, xylim, ls="--", lw=1, color="gray", zorder=-1)
ax.set_xlim(xylim)
ax.set_ylim(xylim)
plt.savefig(plot_path + "TauX_OF_T2.png", dpi=600)

_, ax = plt.subplots()
ax.errorbar(df1["tau_y"] * 1e3, df2["tau_y"] * 1e3,
            xerr=df1["tau_y_err"] * 1e3, yerr=df2["tau_y_err"] * 1e3,
            markersize=5, fmt=".", c="black")
ax.set_xlabel(r"$\tau_{\rm y,\,OF}$ (ms)")
ax.set_ylabel(r"$\tau_{\rm y,\,T2}$ (ms)")
xlim = ax.get_xlim()
ylim = ax.get_ylim()
xylim = (np.max([np.min([xlim[0], ylim[0]]), 0]), np.min([np.max([xlim[1], ylim[1]]), 30]))
ax.plot(xylim, xylim, ls="--", lw=1, color="gray", zorder=-1)
ax.set_xlim(xylim)
ax.set_ylim(xylim)
plt.savefig(plot_path + "TauY_OF_T2.png", dpi=600)

_, ax = plt.subplots()
ax.errorbar(df1["chisqr_x"], df2["chisqr_x"], markersize=5,
            fmt=".", c="black")
ax.set_xlabel(r"$\chi^2_{\rm x,\,OF}$")
ax.set_ylabel(r"$\chi^2_{\rm x,\,T2}$")
xlim = ax.get_xlim()
ylim = ax.get_ylim()
xylim = (np.min([xlim[0], ylim[0]]), np.max([xlim[1], ylim[1]]))
ax.plot(xylim, xylim, ls="--", lw=1, color="gray", zorder=-1)
ax.set_xlim(xylim)
ax.set_ylim(xylim)
plt.savefig(plot_path + "ChiSqrX_OF_T2.png", dpi=600)

