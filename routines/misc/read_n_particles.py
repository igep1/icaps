import pandas as pd
import icaps

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
# csv_path = project_path + "Tracks_PE1L100CFadaptL100.csv"
csv_path = project_path + "Tracks_all.csv"
# csv_path = project_path + "Tracks_P.csv"

# csv_path = drive_letter + ":/icaps/data/LDM_ROT2/Tracks_PA50S20L100.csv"

df = pd.read_csv(csv_path)
print(len(df["particle"].unique()))
df = df[df["frame"] >= 4000]
print(len(df["particle"].unique()))
df = df[(df["frame"] >= 264000) | (df["frame"] < 260300)]
print(len(df["particle"].unique()))
df = df[df["area"] > 1]
print(len(df["particle"].unique()))
df = icaps.filter_track_length(df, 2)
print(len(df["particle"].unique()))








