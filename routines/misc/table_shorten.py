import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "YZ"
project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/"
csv_path = project_path + f"OOS_{plane}_E1-3.csv"
out_path = project_path + f"OOS_{plane}_E1_Dist.csv"
out_short_path = project_path + f"OOS_{plane}_E1_Dist_Short.csv"
plot_path = project_path + "plots/E1_Dist/"
icaps.mk_folders(plot_path)

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df = pd.read_csv(csv_path)
    df = df[df["frame"] <= icaps.const.oos_stages["E1"][1]]
    df_out = df[["frame", "x", "y", "area", "raw_ex", "ex", "gyrad", "tier"]].copy()
    df_out["mass"] = df_out["ex"] / icaps.const.oos_ex0
    df_out["mass_low"] = df_out["mass"] - df_out["ex"] / (icaps.const.oos_ex0 + icaps.const.oos_ex0_err)
    df_out["mass_high"] = df_out["ex"] / (icaps.const.oos_ex0 - icaps.const.oos_ex0_err) - df_out["mass"]
    df_out.to_csv(out_path, index=False)

    df = pd.read_csv(out_path)
    df = df[["frame", "mass"]]
    df.to_csv(out_short_path, index=False)

    df = pd.read_csv(out_short_path)
    mass = df["mass"].to_numpy()
    mass = np.sort(mass)
    _, ax = plt.subplots()
    cumsum = np.cumsum(mass)
    ax.scatter(mass, cumsum / np.max(cumsum), s=1, c="black", marker="o")
    ax.set_xlabel(r"$m (m_0)$")
    ax.set_ylabel("Normalized Cumulative Mass")
    ax.minorticks_on()
    icaps.mirror_axis(ax)
    ax.axhline(0.5, c="gray", ls="--", lw=1, zorder=-1)
    plt.savefig(plot_path + f"OOS_{plane}_MassDist_Lin.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(mass, np.linspace(1 / len(mass), 1, len(mass)), s=1, c="black", marker="o")
    ax.set_xlabel(r"$m (m_0)$")
    ax.set_ylabel("Normalized Cumulative Frequency")
    ax.minorticks_on()
    icaps.mirror_axis(ax)
    ax.axhline(0.5, c="gray", ls="--", lw=1, zorder=-1)
    plt.savefig(plot_path + f"OOS_{plane}_FreqDist_Lin.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    cumsum = np.cumsum(mass)
    ax.scatter(mass, cumsum / np.max(cumsum), s=1, c="black", marker="o")
    ax.set_xlabel(r"$m (m_0)$")
    ax.set_ylabel("Normalized Cumulative Mass")
    ax.minorticks_on()
    ax.set_xscale("log")
    icaps.mirror_axis(ax)
    ax.axhline(0.5, c="gray", ls="--", lw=1, zorder=-1)
    plt.savefig(plot_path + f"OOS_{plane}_MassDist_Log.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(mass, np.linspace(1 / len(mass), 1, len(mass)), s=1, c="black", marker="o")
    ax.set_xlabel(r"$m (m_0)$")
    ax.set_ylabel("Normalized Cumulative Frequency")
    ax.minorticks_on()
    ax.set_xscale("log")
    icaps.mirror_axis(ax)
    ax.axhline(0.5, c="gray", ls="--", lw=1, zorder=-1)
    plt.savefig(plot_path + f"OOS_{plane}_FreqDist_Log.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()


