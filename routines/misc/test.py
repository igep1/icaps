import pandas as pd
import icaps
import numpy as np
import cv2


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/Test/"
particle = 371177
particle_path = project_path + "{}/".format(particle)
sharp_path = particle_path + "ffc/"


def fit_ellipse(img, thresh, mark_enlarge=1, ret_marked=False, mark_colors=None, silent=True, angle_corr=0):
    thr_img = icaps.threshold(img, thresh, replace=(0, None))
    contour = cv2.findContours(thr_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    try:
        contour = contour[0][0]
    except IndexError:
        if not silent:
            print("Contour not found.")
        if ret_marked:
            return None, None
        else:
            return None
    contour = np.squeeze(contour)
    try:
        box = cv2.fitEllipse(contour)
    except cv2.error:
        if not silent:
            print("Could not fit ellipse.")
        if ret_marked:
            return None, None
        else:
            return None

    if not ret_marked:
        return box
    else:
        if mark_colors is None:
            mark_colors = [(255, 0, 0), (0, 0, 255)]
        box_large = (np.array(box[0]) * mark_enlarge, np.array(box[1]) * mark_enlarge, np.array(box[2]))
        mask = np.zeros(img.shape, np.uint8)
        mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
        mask = icaps.fill_holes(mask)
        img[mask == 0] = 0
        mark_img = icaps.enlarge_pixels(img, mark_enlarge)
        mark_img = cv2.cvtColor(mark_img, cv2.COLOR_GRAY2BGR)
        mark_img = cv2.drawContours(mark_img, [contour * mark_enlarge], contourIdx=0, color=mark_colors[0])
        mark_img = icaps.draw_ellipse(mark_img, box_large, color=mark_colors[1])
        return box, mark_img


def main():
    thresh = 30
    files = icaps.get_files(sharp_path)
    for file in files:
        img = icaps.load_img(sharp_path + file)
        box, mark_img = fit_ellipse(img, thresh, mark_enlarge=4, ret_marked=True, angle_corr=0)


if __name__ == '__main__':
    main()

