import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from timeit import default_timer as timer


def main():
    # df = pd.DataFrame(data={"frame": np.arange(0, 100, 1), "x": np.random.randint(1, 20, 100)})
    # df.to_csv("F:/Desktop/test.csv", index=False)

    dt = 3
    df = pd.read_csv("F:/Desktop/test.csv")
    t0 = timer()
    df["dx"] = np.nan
    idx = df.index
    j = 0
    i = 1
    t_off = 0
    x_off = 0
    while i < len(idx):
        dt_ = df.loc[idx[i], "frame"] - df.loc[idx[j], "frame"] - t_off
        if dt_ < dt:
            i += 1
        elif dt_ > dt:
            t_off = df.loc[idx[i], "frame"] - df.loc[idx[i-1], "frame"]
            x_off = df.loc[idx[i], "x"] - df.loc[idx[i-1], "x"]
            i += 1
        else:
            df.loc[idx[j], "dx"] = df.loc[idx[i], "x"] - df.loc[idx[j], "x"] - x_off
            t_off = 0
            x_off = 0
            j = i
            i += 1
    print("method1", timer() - t0)
    df.to_csv("F:/Desktop/test_res.csv", index=False)

    t0 = timer()
    df["tdiff"] = df["frame"].diff().shift(periods=-1)
    # df["stitch"] = 0
    # df.loc[df["tdiff"] > dt, "stitch"] = 1
    idx = df.index
    j = 0
    i = 1
    t_off = 0
    df["x_"] = df["x"].astype(np.float64)
    while i < len(idx):
        dt_ = df.loc[idx[i], "frame"] - df.loc[idx[j], "frame"] - t_off
        # sum instead?
        if dt_ < dt:
            i += 1
        elif dt_ > dt:
            t_off = df.loc[idx[i], "frame"] - df.loc[idx[i - 1], "frame"]
            # df.loc[idx[i], "stitch"] = 1
            df.loc[idx[i]:, "x_"] -= df.loc[idx[i], "x"] - df.loc[idx[i - 1], "x"]
            i += 1
        else:
            # df.loc[idx[j], "stitch"] = 2
            t_off = 0
            j = i
            i += 1

    # df["x_"] = df["x"].astype(np.float64)
    # s_idx = df[df["stitch"] == 1].index
    # for s_i in s_idx:
    #     p_i = idx[np.squeeze(np.argwhere(idx == s_i)) - 1]
    #     diff = df.loc[s_i, "x"] - df.loc[p_i, "x"]
    #     df.loc[s_i:, "x_"] -= diff
    # df.loc[df["stitch"] == 1, "x_"] = np.nan

    print("method2", timer() - t0)
    df.to_csv("F:/Desktop/test_res2.csv", index=False)


if __name__ == '__main__':
    main()

