import os
import shutil
import pandas as pd


path = "H:/icaps/data/LDM_ROT3/particles/"
csv_path = "H:/icaps/data/LDM_ROT3/new_candidates_plus.txt"
candidates = pd.read_csv(csv_path, header=None)[0].to_numpy()

files = os.listdir(path)
for file in files:
    particle = int(float(file))
    if particle not in candidates:
        shutil.rmtree(path + file)
        print(file, "removed.")
    else:
        if os.path.exists(path + file + "/plots"):
            shutil.rmtree(path + file + "/plots")
            print(file + "/plots", "removed.")



