import cv2
import numpy as np
import icaps


def main():
    img = np.zeros((1000, 1000), dtype=np.uint8)
    rect = ((500, 500), (100, 20), 60)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    print(box)
    cv2.drawContours(img, [box], 0, 255, 2)
    img = icaps.fill_holes(img)
    cnt = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0][0]
    print(cnt)
    ellipse = cv2.fitEllipse(cnt)
    img = icaps.draw_ellipse(img, ellipse, 100, 2)
    print(ellipse)
    icaps.cvshow(img)


if __name__ == '__main__':
    main()

