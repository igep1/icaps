import numpy as np
import icaps
import pandas as pd
import matplotlib.pyplot as plt

csv_cloud_path = "H:/icaps/data/OOS_XZ/volume/cloud_data.csv"
csv_phase_path = "H:/icaps/data/LDM_ROT3/Phases_Hand.csv"
csv_out_path = "H:/icaps/data/OOS_XZ/volume/cloud_data_undisturbed.csv"


def main():
    df = pd.read_csv(csv_cloud_path)
    df_phase = pd.read_csv(csv_phase_path)
    frames = df_phase.loc[df_phase["phase"] == 0, "frame"].to_numpy()
    frames = np.unique(icaps.cvt_frame(frames, to="oos"))
    _, ax = plt.subplots()
    ax.scatter(df["frame"], df["cloud_volume"], s=1, c="black")
    df = df[df["frame"].isin(frames)]
    _, ax = plt.subplots()
    ax.scatter(df["frame"], df["cloud_volume"], s=1, c="black")
    plt.show()
    df.to_csv(csv_out_path, index=False)


if __name__ == '__main__':
    main()

