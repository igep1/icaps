import icaps
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")

path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.npy"

img = icaps.load_img(path)
print(np.mean(img))
