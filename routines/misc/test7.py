# path = drive_letter + ":/icaps/data/LDM_ROT4/Displacements_Trans.csv"
#
# df = pd.read_csv(path)
# particles = df["particle"].unique()
# num = np.zeros(len(particles), dtype=int)
# pb = icaps.ProgressBar(len(particles), "P")
# for i, particle in enumerate(particles):
#     num[i] = len(df[df["particle"] == particle])
#
# num = np.sort(num)
# norm_num = np.linspace(1 / len(num), 1, len(num))
#
# _, ax = plt.subplots()
# ax.scatter(num, norm_num, c="black", s=5)
# plt.show()

# path = drive_letter + ":/icaps/data/LDM_ROT4/Erf_Trans.csv"
#
# df = pd.read_csv(path)
# dt = 10
# df = df[df["dt"] == 10]
# num = df["drift"].to_numpy() * 1e6
# num = np.sort(num)
# norm_num = np.linspace(1 / len(num), 1, len(num))
#
# num_min = df.loc[df["sigma0"] < df["sigma1"], "drift0"].to_numpy() * 1e6
# num_min = np.append(num_min, df.loc[df["sigma0"] >= df["sigma1"], "drift1"].to_numpy() * 1e6)
# num_min = np.sort(num_min)
# norm_num_min = np.linspace(1 / len(num_min), 1, len(num_min))
#
# num_max = df.loc[df["sigma0"] >= df["sigma1"], "drift0"].to_numpy() * 1e6
# num_max = np.append(num_max, df.loc[df["sigma0"] < df["sigma1"], "drift1"].to_numpy() * 1e6)
# num_max = np.sort(num_max)
# norm_num_max = np.linspace(1 / len(num_max), 1, len(num_max))
#
# _, ax = plt.subplots()
# ax.scatter(num, norm_num, c="black", s=5)
# ax.set_xlim(-dt, dt)
#
# _, ax = plt.subplots()
# ax.scatter(num_min, norm_num_min, c="black", s=5)
# ax.set_xlim(-dt, dt)
#
# _, ax = plt.subplots()
# ax.scatter(num_max, norm_num_max, c="black", s=5)
# ax.set_xlim(-dt, dt)
# plt.show()

