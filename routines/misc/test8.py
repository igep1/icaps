import icaps
import pandas as pd
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT4/"
csv_noise_path = project_path + "ERF_Trans_Noise.csv"
csv_chi_path = project_path + "OF_Chi0.002Ex_DG.csv"

df = pd.read_csv(csv_noise_path)
df = df[df["dt"] == 1]
df = df[df["axis"] == "x"]
df = df[df["sigma_type"] == "low double"]
particles = df["particle"].unique()
df2 = pd.read_csv(csv_chi_path)
df2 = df2[df2["particle"].isin(particles)]
mean_ex = np.squeeze(df2["mean_ex"])
particles = particles[np.argsort(mean_ex)]

print(particles)


