import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/Rainer/"
csv_bottom_path = project_path + "wacca/50_255_bottom/wacca_00200_09000_001.csv"
csv_top_path = project_path + "wacca/50_255_top/wacca_00200_09000_001.csv"
id_bottom = 0
id_top = 169


def main():
    df_bottom = pd.read_csv(csv_bottom_path)
    df_top = pd.read_csv(csv_top_path)
    df_bottom = df_bottom[df_bottom["label"] == id_bottom]
    df_top = df_top[df_top["label"] == id_top]

    icaps.crop_df_particle(df_bottom, id_bottom, project_path + "orig_bottom/", project_path + "part_bottom/",
                           threshold=50, id_column="label", gen_kwargs={"prefix": None, "digits": 5})
    icaps.crop_df_particle(df_top, id_top, project_path + "orig_top/", project_path + "part_top/",
                           threshold=50, id_column="label", gen_kwargs={"prefix": None, "digits": 5})

    img_bottom = icaps.load_img(project_path + "part_bottom/000000/000000_007231.bmp")
    img_top = icaps.load_img(project_path + "part_top/000169/000169_007231.bmp")

    print(np.mean(img_bottom[img_bottom > 0]))
    print(np.mean(img_top[img_top > 0]))


if __name__ == '__main__':
    main()

