import numpy as np
import matplotlib.pyplot as plt
from scipy.special import erf
import icaps


def fit_erf(x, mu, sigma):
    return 0.5 * (1 + erf((x - mu) / (sigma * np.sqrt(2))))


def fit_exp(x, mu, lamda):
    res = 0.5 * (1 + erf(np.sqrt(np.abs((x - mu)) / lamda)))
    res[x - mu < 0] = 1 - res[x - mu < 0]
    # res = np.zeros_like(x)
    # res[x - mu >= 0] = 0.5 * (1 + erf(np.sqrt(np.abs((x[x - mu >= 0] - mu)) / lamda)))
    # res[x - mu < 0] = 0.5 * (1 - erf(np.sqrt(np.abs((x[x - mu < 0] - mu)) / lamda)))
    return res


def fit_exp_solved(dx, mu, lamb):
    res = np.zeros_like(dx)
    idx_p = dx - mu >= 0
    idx_n = dx - mu < 0
    res[idx_n] += 1 / 2 * np.exp((dx[idx_n] - mu) / lamb)
    res[idx_p] += 1 / 2 * (2 - np.exp(-(dx[idx_p] - mu) / lamb))
    return res


def fit_tanh(dx, mu):
    return 1 * (np.tanh((dx - mu)) + 1) * 0.5


xspace = np.linspace(-2, 2, 1000)

_, ax = plt.subplots()
ax.scatter(xspace, fit_tanh(xspace, 0), c="green", s=2)


_, ax = plt.subplots()
ax.scatter(xspace, fit_exp(xspace, 0, 1.5e-1), c="gray", s=2)

# _, ax = plt.subplots()
ax.scatter(xspace, fit_exp_solved(xspace, 0, 1.5e-1), c="black", s=2)

# _, ax = plt.subplots()
# ax.scatter(xspace, icaps.fit_erf_exp_solved(xspace, 0, 1.5e-1, 0.1, 1.5e-1), c="black", s=2)

# _, ax = plt.subplots()
# ax.scatter(xspace, icaps.fit_erf_exp(xspace, 0, 1.5e-1, 0.2, 1.5e-1), c="red", s=2)
plt.show()

