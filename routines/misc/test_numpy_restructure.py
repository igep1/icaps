import numpy as np
import pandas as pd

arr = np.array([
    [[11, 12, 13, 14], [15, 16, 17, 18]],
    [[21, 22, 23, 24], [25, 26, 27, 28]],
    [[31, 32, 33, 34], [35, 36, 37, 38]]
])

print(arr)

arr_ = arr.T
print(arr_)
for j in range(arr[0].shape[1]):
    for i in range(arr[0].shape[0]):
        print(j, i, arr_[j, i])


coords = np.argwhere(arr[0])
print(coords)
data = {"y": coords[:, 0], "x": coords[:, 1]}
print(data)
d_ = {f"{key}": np.zeros(len(coords), dtype=np.uint64) for key in np.unique(arr)}
df = pd.DataFrame(data={**data, **d_})
print(df)
arr2 = arr_.reshape(-1, arr_.shape[-1])
print(arr2)
# df[np.unique(arr)]






