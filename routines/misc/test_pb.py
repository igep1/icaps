import icaps
import time
import threading


arr = [234, 245, 208, 133, 123, 991, 873]
scrubbing = icaps.Stage("Scrubbing", -1)
stage = icaps.Stage("Total", length=5, subs=icaps.Stage("SubStage", arr, subs=[icaps.Stage("SubSubStage", [2, 3]), scrubbing]))
# for i in range(80):
#     print(i + 1, stage, stage.idx, stage.active_idx)
#     if not stage.tick():
#         break

# print(stage)
fpb = icaps.ProgressBar(stage, gui=True, close_on_completion=True,
                        color=("red", "green", "yellow"), show_val=True
                        )

i = 0
while True:
    # print(i)
    time.sleep(0.1)
    if not fpb.tick():
        break
    i += 1

# for i in fpb.__iter__():
#     print(i)
#     time.sleep(0.2)
#     fpb.tick()
#     # print(fpb.window.stage)
# print(threading.enumerate())


pb = icaps.ProgressBar(arr, gui=True, close_on_completion=True)
for i, num in enumerate(pb):
    time.sleep(1)
    print(i, num)
    time.sleep(1)
    pb.tick()
print([thread.name for thread in threading.enumerate()])
# pb.close()
# print([thread.name for thread in threading.enumerate()])


