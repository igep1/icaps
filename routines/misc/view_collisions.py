import icaps
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/Collisions/LDM/"
out_path = project_path + "identified/"
mov_out_path = project_path + "movies/"
csv_coll_path = project_path + "collisions.csv"
csv_all_path = project_path + "Tracks_all.csv"
csv_candidates_path = project_path + "Tracks_CollisionCandidates.csv"
icaps.mk_folders([out_path, mov_out_path], clear=True)

# df = pd.read_csv(csv_coll_path)
# df_all = pd.read_csv(csv_all_path)
# df = pd.merge(df_all, df[["particle", "frame", "collision"]], how="left", on=["frame", "particle"])
# df["collision"] = df["collision"].fillna(-1).astype(np.int64)
# df.to_csv(csv_candidates_path, index=False)

df = pd.read_csv(csv_candidates_path)
df_coll = df[df["collision"] > -1]
particles = np.unique(np.append(df_coll["particle"].unique(), df_coll["collision"].unique()))
frame_buff = 150
search_range = 3
memory = 2
# pb = icaps.ProgressBar(len(particles), "Progress")
done_particles = []
for particle in particles:
    # pb.set_phase(particle)
    partners = df_coll[df_coll["particle"] == particle]["collision"].unique()
    done_particles.append(particle)
    for partner in partners:
        if partner in done_particles:
            break
        dft = df[df["particle"].isin([particle, partner])].copy()
        dft = dft[dft["frame"] >= np.min(dft.loc[dft["particle"] == particle, "frame"])]
        dft = dft[dft["frame"] >= np.min(dft.loc[dft["particle"] == partner, "frame"])]
        dft = dft[dft["frame"] <= np.max(dft.loc[dft["particle"] == particle, "frame"])]
        dft = dft[dft["frame"] <= np.max(dft.loc[dft["particle"] == partner, "frame"])]
        frames = dft["frame"].unique()
        dft["dist"] = np.nan
        for frame in frames:
            p0 = np.squeeze(dft.loc[(dft["particle"] == particle) & (dft["frame"] == frame), ["x", "y"]].to_numpy())
            p1 = np.squeeze(dft.loc[(dft["particle"] == partner) & (dft["frame"] == frame), ["x", "y"]].to_numpy())
            if (len(p0) > 1) & (len(p1) > 1):
                dist = np.sqrt((p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2)
                dft.loc[dft["frame"] == frame, "dist"] = dist
        dft = dft.dropna(subset=["dist"])
        # cframe = dft.sort_values(by="dist")["frame"].to_numpy()[0]
        # print(cframe)
        cframe = np.max(dft["frame"].unique())
        # print(cframe)
        df_mid = np.squeeze(dft.loc[dft["frame"] == cframe, ["x", "y", "mass"]].to_numpy())
        com_x = (df_mid[0, 0] * df_mid[0, 2] + df_mid[1, 0] * df_mid[1, 2]) / (df_mid[0, 2] + df_mid[1, 2])
        com_y = (df_mid[0, 1] * df_mid[0, 2] + df_mid[1, 1] * df_mid[1, 2]) / (df_mid[0, 2] + df_mid[1, 2])
        com = np.array([com_x, com_y])
        product = None
        dfc = df[df["frame"] >= cframe - frame_buff]
        dfc = dfc[dfc["frame"] <= cframe + frame_buff]
        df_ = dfc[dfc["frame"] > cframe]
        df_ = df_[df_["frame"] <= cframe + memory]
        df_["dist"] = np.nan
        search_frames = df_["frame"].unique()
        parts = [particle, partner]
        for frame in search_frames:
            x = df_.loc[df_["frame"] == frame, "x"].to_numpy()
            y = df_.loc[df_["frame"] == frame, "y"].to_numpy()
            df_.loc[df_["frame"] == frame, "dist"] = np.sqrt((x - com[0]) ** 2 + (y - com[1]) ** 2)
            min_dist = np.min(df_.loc[df_["frame"] == frame, "dist"])
            if min_dist < search_range:
                id = df_[df_["frame"] == frame].sort_values(by="dist")["particle"].to_numpy()[0]
                if id not in parts:
                    product = id
            # df_.loc[df_["frame"] == frame, "dist"]
            # print(df_.sort_values(by="dist").loc[df_["frame"] == frame, "dist"])
            # print(min_dist)
        # print(product)
        if product is not None:
            parts.append(product)
        parts_string = "{}_{}_{}".format(particle, partner, product)
        print(parts_string)
        this_out_path = out_path + "{}/".format(parts_string)
        min_tlx = np.min(dfc.loc[dfc["particle"].isin(parts), "bx"].to_numpy())
        min_tly = np.min(dfc.loc[dfc["particle"].isin(parts), "by"].to_numpy())
        max_brx = np.max(dfc.loc[dfc["particle"].isin(parts), "bx"].to_numpy()
                         + dfc.loc[dfc["particle"].isin(parts), "bw"].to_numpy())
        max_bry = np.max(dfc.loc[dfc["particle"].isin(parts), "by"].to_numpy()
                         + dfc.loc[dfc["particle"].isin(parts), "bh"].to_numpy())
        frames_coll = dfc["frame"].unique()
        for frame in frames_coll:
            file = icaps.generate_file(frame)
            orig_path = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame=frame)
            img = icaps.load_img(orig_path + file, strip=True)
            img = icaps.crop(img, tl=(min_tlx, min_tly), br=(max_brx, max_bry))
            # icaps.cvshow(img, t=1)
            icaps.save_img(img, this_out_path + parts_string + "_{}.bmp".format(frame))
        icaps.mk_movie(this_out_path, mov_out_path + parts_string + ".mp4")

    # print(df_mid)
        # print(com)
        # minbx = df[df["particle"]]
        # center = dft.loc[dft["frame"] == cframe, ["x", "y"]]
    # pb.tick()

# print(np.unique(particles))
# while True:
#     try:
#         particle = particles[0]
#     except IndexError:
#         break
#     partners = df.loc[df["particle"] == particle, "collision"]
#     print(particle)
#     print(partners)
#     particles = particles[1:]

# frames = df["frame"].unique()
# for frame in frames:
#     df_ = df[df["frame"] == frame]
#     stats = df_[["bx", "by", "bw", "bh"]].to_numpy()
#     particles = df_["particle"].unique()
#     file = icaps.generate_file(frame)
#     orig_path = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame=frame)
#     img = icaps.load_img(orig_path + file, strip=True)
#     img = icaps.draw_labelled_bbox_bulk(img, stats, labels=particles)
#     icaps.cvshow(img, size=1, t=1)

