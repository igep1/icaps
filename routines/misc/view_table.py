import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import icaps

project_path = "I:/icaps/data/LDM_ROT5/"
csv_path = project_path + "OF_Trans.csv"
pd.set_option("display.max_columns", None)
pd.set_option("display.max_rows", None)
pd.set_option("display.width", None)

df = pd.read_csv(csv_path)
df2 = df[df["n_dots"] > 15].sort_values(axis=0, ascending=False, by=["tau"])
df2["flag"] = 0

dt = 15
for i, row in df2.iterrows():
    val0 = icaps.fit_of_lin(dt * 1e-3, icaps.const.k_B * icaps.const.T / row["mass"], row["tau0"])
    val1 = icaps.fit_of_lin(dt * 1e-3, icaps.const.k_B * icaps.const.T / row["mass"], row["tau"])
    if val0 > val1:
        df2.loc[i, "flag"] = 1
# df2.to_csv("F:/Desktop/T1.csv", index=False)

print(df2[df2["flag"] == 1].head(10))
print("==================================================================")

df3 = df[df["n_dots"] > 15].sort_values(axis=0, ascending=True, by=["chisqr_lin"])
# df3.to_csv("F:/Desktop/T2.csv", index=False)
print(df3.head(10))
print("==================================================================")

# df4 = df[df["n_dots"] > 15].copy()
df4 = df2[df2["n_dots"] > 15].copy()
df4["mass_disc"] = np.nan
particles = df4["particle"].unique()
for i, particle in enumerate(particles):
    df_ = df4[df4["particle"] == particle]
    if len(df_.dropna(subset=["mass"])) < 2:
        continue
    mx = df_.loc[df_["axis"] == "x", "mass"].to_numpy()[0]
    my = df_.loc[df_["axis"] == "y", "mass"].to_numpy()[0]
    df4.loc[df4["particle"] == particle, "mass_disc"] = 1 - min([mx, my]) / max([mx, my])

df4 = df4.sort_values(axis=0, ascending=False, by=["mass_disc"])
df4.to_csv("F:/Desktop/T.csv", index=False)
print(df4.head(10))
