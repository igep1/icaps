import numpy as np
import pandas as pd
import icaps
import matplotlib.pyplot as plt
from itertools import cycle

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT4/"
plot_path = project_path + "plots_dg/"
chi_sqr_thresh = 2e-3    # least squares
# csv_of_chiex_path = project_path + "OF_Chi{}Ex.csv".format(chi_sqr_thresh)
csv_of_chiex_dg_path = project_path + "OF_Chi{}Ex_DG.csv".format(chi_sqr_thresh)
csv_disp_rot_path = project_path + "Displacement_Rot.csv"
csv_erf_rot_path = project_path + "Erf_Rot.csv"
csv_of_rot_path = project_path + "OF_Rot.csv"
csv_disp_trans_path = project_path + "Displacements_Trans.csv"
csv_erf_trans_path = project_path + "Erf_Trans.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
csv_vmu_of_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/particles/0/of.csv"
plt.rcParams.update({"font.size": 10})
dpi = 600
df = pd.read_csv(csv_of_chiex_dg_path)
print(len(df["particle"].unique()))
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df = df[df["mass_x_err"]/df["mass_x"] <= max_ratio]
    df = df[df["mass_y_err"] / df["mass_y"] <= max_ratio]
    print(len(df["particle"].unique()))
    df = df[df["tau_x_err"]/df["tau_x"] <= max_ratio]
    df = df[df["tau_y_err"] / df["tau_y"] <= max_ratio]
    print(len(df["particle"].unique()))

# ============================================================
# VMU COMPARATIVE PLOT
# ============================================================
df_vmu = pd.read_csv(csv_vmu_of_path)
_, ax = plt.subplots()
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel(r"$\Delta t$ (s)")
ax.set_ylabel(r"$\left<\Delta x ^2\right>$ (m$^2$)")
ax.set_xlim(0.9e-3, 60e-3)

particles = df["particle"].to_numpy()
df_erf = pd.read_csv(csv_erf_trans_path)
axis = "x"
df_erf = df_erf[df_erf["axis"] == axis]
df_erf = df_erf[df_erf["particle"].isin(particles)]
max_sigma_sqr = 1e-6
for i, particle in enumerate(particles):
    df_ = df_erf[df_erf["particle"] == particle]
    df_ = df_.dropna(subset=["sigma"])
    dt = df_["dt"].to_numpy()
    sigma = df_["sigma"].to_numpy()
    sigma_err = df_["sigma_err"].to_numpy()
    sigma_sqr = sigma ** 2
    sigma_sqr_err = 2 * sigma_err * sigma
    dt = dt[sigma_sqr <= max_sigma_sqr]
    sigma_sqr_err = sigma_sqr_err[sigma_sqr <= max_sigma_sqr]
    sigma_sqr = sigma_sqr[sigma_sqr <= max_sigma_sqr]
    idx = np.where(sigma_sqr_err/sigma_sqr < 10)
    dt = dt[idx]
    sigma_sqr = sigma_sqr[idx]
    sigma_sqr_err = sigma_sqr_err[idx]
    ax.errorbar(dt*1e-3, sigma_sqr, yerr=sigma_sqr_err, fmt=".", color="black", markersize=2,
                label="Measured Data in {}".format(axis) if i == 0 else None)

t_range = np.linspace(0.9e-3, 60e-3, 1000)
linecycler = cycle(["-", "--", ":", "-."])
for i, a in enumerate(df_vmu["axis"].to_numpy()):
    mass = df_vmu.loc[df_vmu["axis"] == a, "mass"].to_numpy()[0]
    tau = df_vmu.loc[df_vmu["axis"] == a, "tau"].to_numpy()[0]
    ax.plot(t_range, icaps.fit_of_lin(t_range, icaps.const.k_B * icaps.const.T / mass, tau),
            c="red", linestyle=next(linecycler), label="Cloud Track in {}".format(a.upper()))
ax.legend(loc="lower right")
plt.savefig(plot_path + "VMU_Drift_Influence.png", dpi=dpi)
plt.close()
