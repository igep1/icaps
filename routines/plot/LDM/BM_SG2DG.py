import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as mc

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")

project_path = drive_letter + ":/icaps/data/LDM_ROT5/"
plot_path = project_path + "plots_dg/"
csv_erf_trans_sg_path = project_path + "ERF_Trans.csv"

df_erf = pd.read_csv(csv_erf_trans_sg_path)

# ============================================================
# TABLE PREPARATION
# ============================================================
dpi = 600
chisqr_thresh = 5e-3
# path = drive_letter + ":/icaps/data/LDM_ROT4/OF_Trans.csv"
csv_of_chiex_dg_path = drive_letter + ":/icaps/data/LDM_ROT5/OF_Chi{}Ex_DG.csv".format(chisqr_thresh)
csv_of_chiex_sg_path = drive_letter + ":/icaps/data/LDM_ROT5/OF_Chi{}Ex.csv".format(chisqr_thresh)
df_dg = pd.read_csv(csv_of_chiex_dg_path)
df_sg = pd.read_csv(csv_of_chiex_sg_path)
df = pd.merge(df_dg, df_sg[["particle", "inertia", "inertia_err", "tau_rot", "tau_rot_err", "chisqr_rot",
                            "n_dots_rot", "t_diff_rot"]], how="left", on="particle")
df = df_sg.copy()  # just use SG data
df = df.dropna(subset=["mass_x", "mass_y"])
df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
df["tau"] = np.mean([df["tau_x"], df["tau_y"]], axis=0)
df["tau_err"] = 1 / 2 * np.sqrt(df["tau_x_err"] ** 2 + df["tau_y_err"] ** 2)
print(len(df["particle"].unique()))
print(len(df.dropna(subset=["tau_rot"])["particle"].unique()))
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df = df[df["mass_x_err"] / df["mass_x"] <= max_ratio]
    df = df[df["mass_y_err"] / df["mass_y"] <= max_ratio]
    print(len(df["particle"].unique()))
    df = df[df["tau_x_err"] / df["tau_x"] <= max_ratio]
    df = df[df["tau_y_err"] / df["tau_y"] <= max_ratio]
    print(len(df["particle"].unique()))
print(len(df.dropna(subset=["tau_rot"])["particle"].unique()))


# ============================================================
# COLLECTIVE PARAM PLOTS (Single vs. Double)
# ============================================================
axes = ["x", "y"]
dts = [1, 5, 25]
particles = df["particle"].unique()
df_erf = df_erf[df_erf["particle"].isin(particles)]
w_thresh = 0.9
dg_min_chi_factor = 3
max_min_thresh = 2
for axis in axes:
    fig, ax = plt.subplots(nrows=3, ncols=3)
    fig.subplots_adjust(hspace=.001, wspace=.001)
    fig.subplots_adjust(left=0.1, right=0.88)
    for j, dt in enumerate(dts):
        df_ = df_erf[(df_erf["axis"] == axis) & (df_erf["dt"] == dt)]
        df_ = df_[~np.isnan(df_["sigma"])]
        sig0 = np.squeeze(df_["sigma0"].to_numpy())
        sig1 = np.squeeze(df_["sigma1"].to_numpy())
        sig = np.squeeze(df_["sigma"].to_numpy())
        sig0[sig0 <= 0] = sig[sig0 <= 0]
        sig1[sig1 <= 0] = sig[sig1 <= 0]
        chisqr_dg = np.squeeze(df_["chisqr_dg"].to_numpy())
        chisqr_sg = np.squeeze(df_["chisqr"].to_numpy())
        sigmax = np.amax([sig0, sig1], axis=0)
        sigmin = np.amin([sig0, sig1], axis=0)
        w0 = np.squeeze(df_["w0"].to_numpy())
        w1 = 1. - w0
        wmax = np.array([w0[i] if w0[i] > w1[i] else w1[i] for i in range(len(w0))])
        chisqr_ratio = chisqr_sg / chisqr_dg
        sig_ratio = sigmax/sigmin
        ms = 2
        mew = 0.5
        ax[0][j].scatter(chisqr_ratio, sig_ratio, ec="black", s=ms, alpha=1, fc="none", lw=mew)
        if j == 2:
            ax[1][j].scatter(chisqr_ratio, wmax, ec="black", s=ms, alpha=1, fc="none", lw=mew,
                             label="Double Gauss")
        else:
            ax[1][j].scatter(chisqr_ratio, wmax, ec="black", s=ms, alpha=1, fc="none", lw=mew)
        ax[2][j].scatter(wmax, sig_ratio, ec="gray", s=ms, alpha=1, fc="none", lw=mew)
        f1 = (sig_ratio < max_min_thresh)
        f2 = (chisqr_ratio < dg_min_chi_factor)
        f3 = (wmax >= w_thresh)
        fsum = f1 | f2 | f3
        ax[0][j].scatter(chisqr_ratio[fsum], sig_ratio[fsum], c="black", s=ms, alpha=1, marker="o", lw=mew)
        if j == 2:
            ax[1][j].scatter(chisqr_ratio[fsum], wmax[fsum], c="black", s=ms, alpha=1, marker="o", lw=mew,
                             label="Single Gauss")
            ax[1][j].legend(loc="lower right", fontsize=8, ncol=2, bbox_to_anchor=(1.3, -1.42), columnspacing=0.8,
                            handletextpad=0.3)
        else:
            ax[1][j].scatter(chisqr_ratio[fsum], wmax[fsum], c="black", s=ms, alpha=1, marker="o", lw=mew)
        ax[2][j].scatter(wmax[fsum], sig_ratio[fsum], c="gray", s=ms, alpha=1, marker="o", lw=mew)

        lw = 1
        ax[0][j].axvline(dg_min_chi_factor, ls="--", lw=lw, c="red", alpha=0.5)
        ax[1][j].axvline(dg_min_chi_factor, ls="--", lw=lw, c="red", alpha=0.5)
        ax[2][j].axvline(w_thresh, ls="--", lw=lw, c="red", alpha=0.5)
        ax[0][j].axhline(max_min_thresh, ls="--", lw=lw, c="red", alpha=0.5)
        ax[1][j].axhline(w_thresh, ls="--", lw=lw, c="red", alpha=0.5)
        ax[2][j].axhline(max_min_thresh, ls="--", lw=lw, c="red", alpha=0.5)

        falpha = 0.1
        ax[0][j].add_patch(patches.Rectangle((dg_min_chi_factor, max_min_thresh), -20, -20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))
        ax[1][j].add_patch(patches.Rectangle((dg_min_chi_factor, w_thresh), -20, +20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))
        ax[2][j].add_patch(patches.Rectangle((w_thresh, max_min_thresh), +20, -20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))

        ax[0][j].add_patch(patches.Rectangle((dg_min_chi_factor, max_min_thresh), -20, +20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))
        ax[1][j].add_patch(patches.Rectangle((dg_min_chi_factor, w_thresh), -20, -20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))
        ax[2][j].add_patch(patches.Rectangle((w_thresh, max_min_thresh), +20, +20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))

        ax[0][j].add_patch(patches.Rectangle((dg_min_chi_factor, max_min_thresh), +20, -20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))
        ax[1][j].add_patch(patches.Rectangle((dg_min_chi_factor, w_thresh), +20, +20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))
        ax[2][j].add_patch(patches.Rectangle((w_thresh, max_min_thresh), -20, -20,
                                             lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', falpha)))

        # ax[0][j].add_patch(patches.Rectangle((dg_min_chi_factor, max_min_thresh), -20, -20,
        #                                      lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', 0.2)))
        # ax[1][j].add_patch(patches.Rectangle((dg_min_chi_factor, w_thresh), -20, +20,
        #                                      lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', 0.2)))
        # ax[2][j].add_patch(patches.Rectangle((w_thresh, max_min_thresh), +20, -20,
        #                                      lw=lw, ls="--", ec="none", fc=mc.to_rgba('red', 0.2)))

        ax[2][j].tick_params(axis="x", colors="gray")
        ax[2][j].xaxis.label.set_color("gray")
        if j != 0:
            ax[0][j].set_yticks([])
            ax[1][j].set_yticks([])
            ax[2][j].set_yticks([])
        else:
            ax[0][j].set_yticks([2, 4, 6, 8, 10, 12])
            ax[2][j].set_yticks([2, 4, 6, 8, 10, 12])
        ax[1][j].set_xticks([])
        ax[0][j].xaxis.tick_top()
        ax[0][j].xaxis.set_label_position("top")
        ax[0][j].set_xticks([0, 4, 8, 12, 16])
        ax[0][j].set_xlim(0, 20.5)
        ax[0][j].set_ylim(1, 12.5)
        ax[1][j].set_xlim(0, 20.5)
        ax[1][j].set_ylim(0.48, 1.02)
        ax[2][j].set_xlim(0.48, 1.02)
        ax[2][j].set_ylim(1, 12.5)
        ax[0][j].minorticks_on()
        ax[1][j].minorticks_on()
        ax[2][j].minorticks_on()
        ax[0][j].annotate(r"$\Delta t$ = {} ms".format(dt), xy=(20, 12), xycoords="data",
                          size=10, ha="right", va="top", bbox=dict(boxstyle="square", fc="w"))
    ax[0][1].set_xlabel(r"$\chi_{\rm single}^2/\chi_{\rm double}^2$")
    ax[0][0].set_ylabel(r"$\sigma_{\rm max}/\sigma_{\rm min}$")
    ax[1][0].set_ylabel(r"$w_{\rm max}$")
    ax[2][0].set_ylabel(r"$\sigma_{\rm max}/\sigma_{\rm min}$")
    ax[2][1].set_xlabel(r"$w_{\rm max}$")
    plt.savefig(plot_path + "Paramspace_{}.png".format(axis), dpi=dpi)
    plt.close()

