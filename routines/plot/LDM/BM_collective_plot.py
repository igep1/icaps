import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import icaps


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT5/"
# plot_path = project_path + "plots/"
plot_path = project_path + "plots_dg/"
# chisqr_thresh = 2e-3    # least squares
chisqr_thresh = 5e-3    # least squares
# excluded_particles = (8781427,)
csv_of_chiex_sg_path = project_path + "OF_Chi{}Ex.csv".format(chisqr_thresh)
csv_of_chiex_dg_path = project_path + "OF_Chi{}Ex_DG.csv".format(chisqr_thresh)
csv_disp_trans_path = project_path + "DISP_Trans.csv"
csv_erf_trans_path = project_path + "ERF_Trans.csv"
csv_erf_trans_2_path = project_path + "ERF_Trans_2.csv"
csv_erf_trans_dg_path = project_path + "ERF_Trans_DG.csv"
csv_erf_rot_path = project_path + "ERF_Rot.csv"
csv_of_trans_path = project_path + "OF_Trans.csv"
csv_vmu_of_path = drive_letter + ":/icaps/data/LDM_BM_Virtual/particles/0/of.csv"
dpi = 600
plt.rcParams.update({"font.size": 10})


# ============================================================
# TABLE PREPARATION
# ============================================================
df = pd.read_csv(csv_of_chiex_dg_path)
df_sg = pd.read_csv(csv_of_chiex_sg_path)
df = pd.merge(df, df_sg[["particle", "inertia", "inertia_err", "tau_rot", "tau_rot_err", "chisqr_rot",
                         "n_dots_rot", "t_diff_rot"]], how="left", on="particle")
print("chi² {}:\t\t".format(chisqr_thresh), len(df["particle"].unique()))
df["mass"] = np.mean([df["mass_x"], df["mass_y"]], axis=0)
df["mass_err"] = 1 / 2 * np.sqrt(df["mass_x_err"] ** 2 + df["mass_y_err"] ** 2)
df["tau"] = np.mean([df["tau_x"], df["tau_y"]], axis=0)
df["tau_err"] = 1 / 2 * np.sqrt(df["tau_x_err"] ** 2 + df["tau_y_err"] ** 2)
# max_ratio = -1
max_ratio = .2
if max_ratio > 0:
    df = df[df["mass_x_err"] / df["mass_x"] <= max_ratio]
    df = df[df["mass_y_err"] / df["mass_y"] <= max_ratio]
    print(len(df["particle"].unique()))
    df = df[df["tau_x_err"] / df["tau_x"] <= max_ratio]
    df = df[df["tau_y_err"] / df["tau_y"] <= max_ratio]
    print(len(df["particle"].unique()))


# # ============================================================
# # COLLECTIVE ERF PLOT (NORM)
# # ============================================================
# particles = df["particle"].unique()
# df_disp = pd.read_csv(csv_disp_trans_path)
# df_disp = df_disp[df_disp["particle"].isin(particles)]
# df_erf = pd.read_csv(csv_erf_trans_path)
# df_erf = df_erf[df_erf["particle"].isin(particles)]
# axis = "x"
# dts = np.arange(1, 26, 1)
# dts = np.append(dts, np.arange(30, 51, 5))
# xlim = 5
# icaps.mk_folder(plot_path + "erf_coll_norm/", clear=True)
# pb = icaps.ProgressBar(len(dts) * len(particles), "Norm")
# for dt in dts:
#     dx_label = "d{}_{}".format(axis, dt)
#     df_disp_ = df_disp[["particle", dx_label]].dropna()
#     df_erf_ = df_erf[df_erf["dt"] == dt]
#     df_erf_ = df_erf_[df_erf_["axis"] == axis]
#     xi = []
#     for particle in particles:
#         drift = df_erf_.loc[df_erf_["particle"] == particle, "drift"].to_numpy()[0] * 1e6
#         sigma = df_erf_.loc[df_erf_["particle"] == particle, "sigma"].to_numpy()[0] * 1e6
#         dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
#         xi_ = (dx - drift) / sigma
#         xi.extend(xi_)
#         pb.tick()
#     xi = np.array(xi)
#
#     _, ax = plt.subplots(nrows=2)
#     x = np.sort(xi)
#     y = np.linspace(1 / len(x), 1, len(x))
#     ax[0].scatter(x, y, s=1, c="black")
#
#     out = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.0, 1.0], fit_type=2)
#     x_space = np.linspace(np.min(x), np.max(x), 1000)
#     ax[0].plot(x_space, icaps.fit_erf(x_space, *out.beta), c="blue", alpha=0.6, lw=2,
#                label="Single Gauss Fit"
#                # label=r"$\mu$" + " = {:.3f} ".format(out.beta[0]) + r"$\pm$"
#                #       + " {:.3f}\n".format(out.sd_beta[0])
#                #       + r"$\sigma$" + " = {:.3f} ".format(out.beta[1]) + r"$\pm$"
#                #       + " {:.3f}\n".format(out.sd_beta[1])
#                )
#     ax[1].scatter(x, y - icaps.fit_erf(x, *out.beta), c="blue", s=1, marker="o", zorder=1)
#
#     out = icaps.fit_model(icaps.fit_double_erf_center, x, y, p0=[0.9, 0.0, out.beta[1], out.beta[1]],
#                           fit_type=2)
#     x_space = np.linspace(np.min(x), np.max(x), 1000)
#     ax[0].plot(x_space, icaps.fit_double_erf_center(x_space, *out.beta), c="red", alpha=0.6, lw=2,
#                label="Double Gauss Fit\n"
#                      + r"$w_0$" + " = {:.3f} ".format(out.beta[0]) + r"$\pm$"
#                      + " {:.3f}\n".format(out.sd_beta[0])
#                      + r"$\mu_{01}$" + " = {:.3f} ".format(out.beta[1]) + r"$\pm$"
#                      + " {:.3f}\n".format(out.sd_beta[1])
#                      + r"$\sigma_0$" + " = {:.3f} ".format(out.beta[2]) + r"$\pm$"
#                      + " {:.3f}\n".format(out.sd_beta[2])
#                      + r"$\sigma_1$" + " = {:.3f} ".format(out.beta[3]) + r"$\pm$"
#                      + " {:.3f}".format(out.sd_beta[3])
#                )
#     ax[1].scatter(x, y - icaps.fit_double_erf_center(x, *out.beta), c="red", s=1, marker="o", zorder=1)
#     ax[0].set_title(r"$\Delta t$ = {} ms".format(dt))
#     ax[0].set_ylabel("Normalized Cumulative Frequency")
#     ax[0].set_xlim(-xlim, xlim)
#     ax[0].legend(loc="upper left")
#     ax[1].set_xlim(-xlim, xlim)
#     ax[1].set_ylabel("Residual")
#     ax[1].set_ylim(-0.017, 0.017)
#     ax[1].axhline(0, c="gray", lw=2, ls="--", zorder=0)
#     ax[1].set_xlabel(r"$\Delta\xi_{}$".format(axis))
#     plt.savefig(plot_path + "erf_coll_norm/erf_coll_{}_{}.png".format(axis, dt), dpi=dpi)
#     plt.close()
# pb.finish()


# # ============================================================
# # COLLECTIVE ERF PLOT (SINGLE)
# # ============================================================
# particles = df["particle"].unique()
# df_disp = pd.read_csv(csv_disp_trans_path)
# df_disp = df_disp[df_disp["particle"].isin(particles)]
# df_erf = pd.read_csv(csv_erf_trans_path)
# df_erf = df_erf[df_erf["particle"].isin(particles)]
# axis = "x"
# dts = np.arange(1, 26, 1)
# dts = np.append(dts, np.arange(30, 51, 5))
# xlim = 5
# icaps.mk_folder(plot_path + "erf_coll_single/", clear=True)
# pb = icaps.ProgressBar(len(dts) * len(particles), "Single")
# for dt in dts:
#     dx_label = "d{}_{}".format(axis, dt)
#     df_disp_ = df_disp[["particle", dx_label]].dropna()
#     df_erf_ = df_erf[df_erf["dt"] == dt]
#     df_erf_ = df_erf_[df_erf_["axis"] == axis]
#     fig, ax = plt.subplots(nrows=2)
#     for particle in particles:
#         drift = df_erf_.loc[df_erf_["particle"] == particle, "drift"].to_numpy()[0] * 1e6
#         sigma = df_erf_.loc[df_erf_["particle"] == particle, "sigma"].to_numpy()[0] * 1e6
#         dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
#         xi_ = (dx - drift) / sigma
#         x = np.sort(xi_)
#         y = np.linspace(1 / len(x), 1, len(x))
#         out = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.1, 0.1], fit_type=2)
#         ax[0].scatter(x, y, s=1, c="black")
#         x_space = np.linspace(np.min(x), np.max(x), 1000)
#         ax[0].plot(x_space, icaps.fit_erf(x_space, *out.beta), c="red", alpha=0.6, lw=2)
#         res = y - icaps.fit_erf(x, *out.beta)
#         ax[1].scatter(x, res, c="black", s=1, marker="o", zorder=1, alpha=0.005)
#         pb.tick()
#
#     # ax[0].legend(loc="upper left")
#     ax[0].set_ylabel("Normalized Cumulative Frequency")
#     ax[0].set_xlim(-xlim, xlim)
#     ax[1].set_xlim(-xlim, xlim)
#     ax[1].set_ylabel("Residual")
#     ax[1].axhline(0, c="gray", lw=2, ls="--", zorder=0)
#     ax[1].set_xlabel(r"$\Delta\xi_G$")
#     ax[0].set_xticks([])
#     # ax[1].set_yticks([-0.075, -0.05, -0.025, 0, 0.025, 0.05, 0.075])
#     ax[1].set_ylim(-0.10, 0.10)
#     ax[1].yaxis.tick_right()
#     ax[1].yaxis.set_label_position("right")
#     # fig.subplots_adjust(bottom=0.08, top=0.95)
#     fig.subplots_adjust(hspace=.001, wspace=.001)
#     fig.subplots_adjust(left=0.1, right=0.88)
#     plt.savefig(plot_path + "erf_coll_single/erf_coll_{}_{}.png".format(axis, dt), dpi=dpi)
#     plt.close()
# pb.finish()


# ============================================================
# COLLECTIVE PRE ERF PLOT (SINGLE, COLLAGE)
# ============================================================
df_disp = pd.read_csv(csv_disp_trans_path)
df_erf = pd.read_csv(csv_erf_trans_path)
df_erf_dg = pd.read_csv(csv_erf_trans_dg_path)
# particles = df["particle"].unique()
particles = df_erf_dg["particle"].unique()
df_erf_dg = df_erf_dg[df_erf_dg["particle"].isin(particles)]
df_erf = df_erf[df_erf["particle"].isin(particles)]
df_disp = df_disp[df_disp["particle"].isin(particles)]
axis = "x"
dts = [1, 6]
xlim = 5
pb = icaps.ProgressBar(len(dts) * len(particles), "Single PRE Collage")
fig, ax = plt.subplots(nrows=2, ncols=2, gridspec_kw={"height_ratios": [2, 1]}, figsize=[6.4, 4.8*0.9])
for j, dt in enumerate(dts):
    dx_label = "d{}_{}".format(axis, dt)
    df_disp_ = df_disp[["particle", dx_label]].dropna()
    df_erf_ = df_erf[(df_erf["dt"] == dt) & (df_erf["axis"] == axis)]
    df_erf_dg_ = df_erf_dg[(df_erf_dg["dt"] == dt) & (df_erf_dg["axis"] == axis)]
    x_space = np.linspace(-10, 10, 1000)
    # ax[0][j].plot(x_space, icaps.fit_erf(x_space, 0, 1), c="red", alpha=0.6, lw=2, ls="-", zorder=1000)
    ax[0][j].plot(x_space, icaps.fit_erf_old(x_space, 0, 1), c="red", alpha=0.6, lw=2, ls="-", zorder=1000)
    for particle in particles:
        sigma_type = df_erf_dg_.loc[df_erf_dg_["particle"] == particle, "sigma_type"].to_numpy()[0]
        if sigma_type == "single":
            pb.tick()
            continue
        drift = df_erf_.loc[df_erf_["particle"] == particle, "drift"].to_numpy()[0] * 1e6
        sigma = df_erf_.loc[df_erf_["particle"] == particle, "sigma"].to_numpy()[0] * 1e6
        dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
        xi_ = (dx - drift) / sigma
        x = np.sort(xi_)
        y = np.linspace(1 / len(x), 1, len(x))
        # out = icaps.fit_model(icaps.fit_erf, x, y, p0=[0.1, 0.1], fit_type=2)
        ax[0][j].scatter(x, y, s=1, c="black", alpha=0.005)
        # res = y - icaps.fit_erf(x, 0, 1)
        res = y - icaps.fit_erf_old(x, 0, 1)
        ax[1][j].scatter(x, res, c="black", s=1, marker="o", zorder=1, alpha=0.005)
        pb.tick()
    ax[0][j].set_xlim(-xlim, xlim)
    ax[0][j].minorticks_on()
    ax[1][j].minorticks_on()
    ax[1][j].set_xlim(-xlim, xlim)
    ax[1][j].axhline(0, c="gray", lw=2, ls="--", zorder=10, alpha=0.5)
    ax[0][j].annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-4.5, 1), xycoords="data",
                      size=10, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))

    # ax[0].legend(loc="upper left")
ax[0][0].set_ylabel("Normalized Cumulative Frequency")
ax[1][1].set_ylabel("Residual")
fig.text(0.50, 0.02, r"$\Delta\xi_{{\rm {}}}$".format("G, {}".format(axis)), ha="center")
ax[1][0].set_yticks([])
ax[0][1].set_yticks([])
ax[0][0].set_ylim(-0.05, 1.05)
ax[0][1].set_ylim(-0.05, 1.05)
ax[1][1].set_yticks([-0.1, -0.05, 0, 0.05, 0.1])
# ax[1].set_yticks([-0.075, -0.05, -0.025, 0, 0.025, 0.05, 0.075])
ax[1][0].set_ylim(-0.10, 0.10)
ax[1][1].set_ylim(-0.10, 0.10)
ax[1][1].yaxis.tick_right()
ax[1][1].yaxis.set_label_position("right")
# fig.subplots_adjust(bottom=0.08, top=0.95)
fig.subplots_adjust(hspace=.001, wspace=.001)
fig.subplots_adjust(left=0.1, right=0.88)
plt.savefig(plot_path + "erf_coll_pre_{}_collage.png".format(axis), dpi=dpi)
plt.close()
pb.finish()


# ============================================================
# COLLECTIVE POST ERF PLOT (SINGLE, COLLAGE)
# ============================================================
df_disp = pd.read_csv(csv_disp_trans_path)
df_erf = pd.read_csv(csv_erf_trans_path)
df_erf_dg = pd.read_csv(csv_erf_trans_dg_path)
# particles = df["particle"].unique()
particles = df_erf_dg["particle"].unique()
df_erf = df_erf[df_erf["particle"].isin(particles)]
df_erf_dg = df_erf_dg[df_erf_dg["particle"].isin(particles)]
df_disp = df_disp[df_disp["particle"].isin(particles)]
axis = "x"
dts = [1, 6]
xlim = 5
pb = icaps.ProgressBar(len(dts) * len(particles), "Single POST Collage")
fig, ax = plt.subplots(nrows=2, ncols=2, gridspec_kw={"height_ratios": [2, 1]}, figsize=[6.4, 4.8*0.9])
for j, dt in enumerate(dts):
    dx_label = "d{}_{}".format(axis, dt)
    df_disp_ = df_disp[["particle", dx_label]].dropna()
    df_erf_ = df_erf[(df_erf["dt"] == dt) & (df_erf["axis"] == axis)]
    df_erf_dg_ = df_erf_dg[(df_erf_dg["dt"] == dt) & (df_erf_dg["axis"] == axis)]
    x_space = np.linspace(-10, 10, 1000)
    # ax[0][j].plot(x_space, icaps.fit_erf(x_space, 0, 1), c="red", alpha=0.6, lw=2, ls="-", zorder=1000)
    ax[0][j].plot(x_space, icaps.fit_erf_old(x_space, 0, 1), c="red", alpha=0.6, lw=2, ls="-", zorder=1000)
    for particle in particles:
        sigma_type = df_erf_dg_.loc[df_erf_dg_["particle"] == particle, "sigma_type"].to_numpy()[0]
        if sigma_type == "single":
            pb.tick()
            continue
        drift = df_erf_.loc[df_erf_["particle"] == particle, "drift"].to_numpy()[0] * 1e6
        sigma = df_erf_.loc[df_erf_["particle"] == particle, "sigma"].to_numpy()[0] * 1e6
        drift01 = df_erf_.loc[df_erf_["particle"] == particle, "drift01"].to_numpy()[0] * 1e6
        sigma0 = df_erf_.loc[df_erf_["particle"] == particle, "sigma0"].to_numpy()[0] * 1e6
        sigma1 = df_erf_.loc[df_erf_["particle"] == particle, "sigma1"].to_numpy()[0] * 1e6
        w0 = np.squeeze(df_erf_.loc[df_erf_["particle"] == particle, "w0"].to_numpy())
        w1 = 1. - w0
        if sigma0 <= 0:
            sigma0 = sigma
        if sigma1 <= 0:
            sigma1 = sigma
        sigmax = np.amax([sigma0, sigma1])
        sigmin = np.amin([sigma0, sigma1])
        w = np.array([w0, w1])
        w_sigmax = w[np.argmax([sigma0, sigma1])]
        w_sigmin = w[np.argmin([sigma0, sigma1])]
        if sigma_type == "low double":
            sigma = sigmin
        elif sigma_type == "high double":
            sigma = sigmax
        if sigma_type != "single":
            drift = drift01
        dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
        x = np.sort(dx)
        y = np.linspace(1 / len(x), 1, len(x))
        if sigma_type == "low double":
            # y -= w_sigmax * icaps.fit_erf(x, drift01, sigmax)
            y -= w_sigmax * icaps.fit_erf_old(x, drift01, sigmax)
            y = y / w_sigmin
            # print(min(y), max(y))
        elif sigma_type == "high double":
            # y -= w_sigmin * icaps.fit_erf(x, drift01, sigmin)
            y -= w_sigmin * icaps.fit_erf_old(x, drift01, sigmin)
            y = y / w_sigmax
            # print(min(y), max(y))
        xi_ = (dx - drift) / sigma
        x = np.sort(xi_)
        ax[0][j].scatter(x, y, s=1, c="black", alpha=0.005)
        # res = y - icaps.fit_erf(x, 0, 1)
        res = y - icaps.fit_erf_old(x, 0, 1)
        ax[1][j].scatter(x, res, c="black", s=1, marker="o", zorder=1, alpha=0.005)
        pb.tick()
    ax[0][j].set_xlim(-xlim, xlim)
    ax[0][j].minorticks_on()
    ax[1][j].minorticks_on()
    ax[1][j].set_xlim(-xlim, xlim)
    ax[1][j].axhline(0, c="gray", lw=2, ls="--", zorder=10, alpha=0.5)
    ax[0][j].annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-4.5, 1), xycoords="data",
                      size=10, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))

    # ax[0].legend(loc="upper left")
ax[0][0].set_ylabel("Normalized Cumulative Frequency")
ax[1][1].set_ylabel("Residual")
fig.text(0.50, 0.02, r"$\Delta\xi_{{\rm {}}}$".format("G, {}".format(axis)), ha="center")
ax[1][0].set_yticks([])
ax[0][1].set_yticks([])
ax[1][1].set_yticks([-0.1, -0.05, 0, 0.05, 0.1])
ax[0][0].set_ylim(-0.05, 1.05)
ax[0][1].set_ylim(-0.05, 1.05)
# ax[1].set_yticks([-0.075, -0.05, -0.025, 0, 0.025, 0.05, 0.075])
ax[1][0].set_ylim(-0.10, 0.10)
ax[1][1].set_ylim(-0.10, 0.10)
ax[1][1].yaxis.tick_right()
ax[1][1].yaxis.set_label_position("right")
# fig.subplots_adjust(bottom=0.08, top=0.95)
fig.subplots_adjust(hspace=.001, wspace=.001)
fig.subplots_adjust(left=0.1, right=0.88)
plt.savefig(plot_path + "erf_coll_post_{}_collage.png".format(axis), dpi=dpi)
plt.close()
pb.finish()


# ============================================================
# COLLECTIVE OF PLOT (TRANS)
# ============================================================
particles = df["particle"].unique()
df_erf = pd.read_csv(csv_erf_trans_2_path)
df_erf = df_erf[df_erf["particle"].isin(particles)]
axes = ["x", "y"]
dts = np.arange(1, 26, 1)
dts = np.append(dts, np.arange(30, 51, 5))
pb = icaps.ProgressBar(2 * len(particles), "OF TRANS")
for axis in axes:
    fig, ax = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]})
    ax[1].set_xscale("log")
    for particle in particles:
        sigma = df_erf.loc[(df_erf["particle"] == particle) & (df_erf["axis"] == axis), "sigma"].to_numpy()
        mass = df.loc[df["particle"] == particle, "mass_{}".format(axis)].to_numpy()[0]
        tau = df.loc[df["particle"] == particle, "tau_{}".format(axis)].to_numpy()[0]
        xi = sigma**2/(2 * icaps.const.k_B * icaps.const.T / mass * tau**2)
        theta = dts/(tau * 1e3)
        xi = xi[df_erf.loc[(df_erf["particle"] == particle) & (df_erf["axis"] == axis), "of_used"].to_numpy()]
        theta = theta[df_erf.loc[(df_erf["particle"] == particle) & (df_erf["axis"] == axis), "of_used"].to_numpy()]
        ax[0].scatter(theta, xi, s=1, c="black", alpha=0.25)
        res = (xi - (theta - 1 + np.exp(-theta)))/(theta - 1 + np.exp(-theta))
        ax[1].scatter(theta, res, c="black", s=1, alpha=0.25)
        pb.tick()
    xspace = np.linspace(ax[0].get_xlim()[0], ax[0].get_xlim()[1], 1000)
    ax[1].axhline(0, color="gray", ls="--", zorder=10, alpha=0.5)
    ax[0].plot(xspace, xspace - 1 + np.exp(-xspace), color="red", ls="--", lw=2, alpha=0.5)
    ax[0].set_yscale("log")
    ax[0].set_xscale("log")
    ax[0].set_xlim(0.05, 25)
    ax[0].set_ylim(1e-3, 40)
    ax[1].set_ylim(-0.4, 0.4)
    ax[1].set_xlim(0.05, 25)
    ax[0].minorticks_on()
    ax[1].minorticks_on()
    ax[0].set_xticks([])
    ax[1].yaxis.tick_right()
    ax[1].yaxis.set_label_position("right")
    ax[0].set_ylabel(r"$\left<\Delta \xi_{{\rm {}}} ^2\right>$".format("OF, {}".format(axis)))
    ax[1].set_xlabel(r"$\vartheta_{{\rm {}}}$".format("t, {}".format(axis)))
    ax[1].set_ylabel("Residual")
    fig.subplots_adjust(hspace=.001, wspace=.001)
    fig.subplots_adjust(left=0.12, right=0.89)
    plt.savefig(plot_path + "of_coll_{}.png".format(axis), dpi=dpi)
    plt.close()
pb.finish()


# ============================================================
# COLLECTIVE OF PLOT (ROT)
# ============================================================
df_ = df.dropna(subset=["inertia"])
df_ = df_[df_["inertia_err"]/df_["inertia"] < 0.2]
particles = df_["particle"].unique()
df_erf = pd.read_csv(csv_erf_rot_path)
df_erf = df_erf[df_erf["particle"].isin(particles)]
axes = ["x", "y"]
dts = np.arange(1, 26, 1)
dts = np.append(dts, np.arange(30, 51, 5))
pb = icaps.ProgressBar(len(particles), "OF ROT")
fig, ax = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]})
ax[1].set_xscale("log")
for particle in particles:
    sigma = df_erf.loc[df_erf["particle"] == particle, "sigma"].to_numpy()
    inertia = df.loc[df["particle"] == particle, "inertia"].to_numpy()[0]
    tau = df.loc[df["particle"] == particle, "tau_rot"].to_numpy()[0]
    # tau = tau[~np.isnan(inertia)]
    # inertia = inertia[~np.isnan(inertia)]
    xi = sigma**2/(2 * icaps.const.k_B * icaps.const.T / inertia * tau**2)
    theta = dts/(tau * 1e3)
    # xi = xi[df_erf.loc[(df_erf["particle"] == particle) & (df_erf["axis"] == axis), "of_used"].to_numpy()]
    # theta = theta[df_erf.loc[(df_erf["particle"] == particle) & (df_erf["axis"] == axis), "of_used"].to_numpy()]
    ax[0].scatter(theta, xi, s=1, c="black", alpha=0.25)
    res = (xi - (theta - 1 + np.exp(-theta)))/(theta - 1 + np.exp(-theta))
    ax[1].scatter(theta, res, c="black", s=1, alpha=0.25)
    pb.tick()
xspace = np.linspace(ax[0].get_xlim()[0], ax[0].get_xlim()[1], 1000)
ax[1].axhline(0, color="gray", ls="--", zorder=10, alpha=0.5)
ax[0].plot(xspace, xspace - 1 + np.exp(-xspace), color="red", ls="--", lw=2, alpha=0.5)
ax[0].set_yscale("log")
ax[0].set_xscale("log")
ax[0].set_xlim(0.05, 25)
ax[0].set_ylim(1e-3, 40)
ax[1].set_xlim(0.05, 25)
ax[0].minorticks_on()
ax[1].minorticks_on()
ax[1].set_ylim(-1, 1)
ax[0].set_xticks([])
ax[1].yaxis.tick_right()
ax[1].yaxis.set_label_position("right")
ax[0].set_ylabel(r"$\left<\Delta \xi_{\rm OF, r} ^2\right>$")
ax[1].set_xlabel(r"$\vartheta_{\rm r}$")
ax[1].set_ylabel("Residual")
fig.subplots_adjust(hspace=.001, wspace=.001)
fig.subplots_adjust(left=0.12, right=0.89)
plt.savefig(plot_path + "of_coll_rot.png", dpi=dpi)
plt.close()
pb.finish()



