import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")

dpi = 600
chisqr_thresh = 5e-3
project_path = drive_letter + ":/icaps/data/LDM_ROT5/"
plot_path = project_path + "plots/compare_methods/"

fits = ("T4", "OF")
sigmas = ("diff", "fit")
combinations = []
for fit in fits:
    combination = (fit, fit, *sigmas)
    combinations.append(combination)
for sigma in sigmas:
    combination = (*fits, sigma, sigma)
    combinations.append(combination)
print(combinations)

icaps.mk_folders([plot_path], clear=True)

for combination in combinations:
    method1 = combination[0], combination[2]
    method2 = combination[1], combination[3]
    csv_of_final_sg1 = project_path + "OF_Final_SG_{}_sigma{}.csv".format(*method1)
    csv_of_final_sg2 = project_path + "OF_Final_SG_{}_sigma{}.csv".format(*method2)

    df_sg1 = pd.read_csv(csv_of_final_sg1)
    df_sg1 = df_sg1[df_sg1["chisqr_x"] <= chisqr_thresh]
    df_sg1 = df_sg1[df_sg1["chisqr_y"] <= chisqr_thresh]
    particles = df_sg1["particle"].unique()
    df_sg2 = pd.read_csv(csv_of_final_sg2)
    df_sg2 = df_sg2[df_sg2["particle"].isin(particles)]

    df_sg1["mass"] = np.mean([df_sg1["mass_x"], df_sg1["mass_y"]], axis=0)
    df_sg1["mass_err"] = 1 / 2 * np.sqrt(df_sg1["mass_x_err"] ** 2 + df_sg1["mass_y_err"] ** 2)
    df_sg1["tau"] = np.mean([df_sg1["tau_x"], df_sg1["tau_y"]], axis=0)
    df_sg1["tau_err"] = 1 / 2 * np.sqrt(df_sg1["tau_x_err"] ** 2 + df_sg1["tau_y_err"] ** 2)
    print(len(df_sg1["particle"].unique()))
    print(len(df_sg1.dropna(subset=["tau_rot"])["particle"].unique()))
    # max_ratio = -1
    max_ratio = .2
    if max_ratio > 0:
        df_sg1 = df_sg1[df_sg1["mass_x_err"] / df_sg1["mass_x"] <= max_ratio]
        df_sg1 = df_sg1[df_sg1["mass_y_err"] / df_sg1["mass_y"] <= max_ratio]
        print(len(df_sg1["particle"].unique()))
        df_sg1 = df_sg1[df_sg1["tau_x_err"] / df_sg1["tau_x"] <= max_ratio]
        df_sg1 = df_sg1[df_sg1["tau_y_err"] / df_sg1["tau_y"] <= max_ratio]
        print(len(df_sg1["particle"].unique()))
    print(len(df_sg1.dropna(subset=["tau_rot"])["particle"].unique()))

    df_sg2 = df_sg2[["particle", "mass_x", "tau_x", "chisqr_x"]]
    df_sg2.rename(columns={"mass_x": "mass_x2", "tau_x": "tau_x2", "chisqr_x": "chisqr_x2"}, inplace=True)
    df_sg12 = pd.merge(df_sg1, df_sg2, how="left", on="particle")
    print(df_sg12.columns)


    # with open(plot_path + "info.txt", "w") as f:
    #     f.write("trans table\t" + csv_of_final_dg.split("/")[-1] + "\n")
    #     f.write("rot table\t" + csv_of_final_sg.split("/")[-1] + "\n")
    #     f.write("n_parts\t\t" + str(len(df["particle"].unique())) + "\n")
    #     f.write("max_ratio\t" + str(max_ratio))

    # ============================================================
    # RESULT PLOTS
    # ============================================================
    _, ax = plt.subplots()
    ax.scatter(df_sg12["mass_x"], df_sg12["mass_x2"]/df_sg12["mass_x"], s=5, c="black")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(2e-15, 2e-12)
    ax.set_ylim(5e-1, 2e0)
    ax.set_xlabel("m_x_{}_{}".format(*method1))
    ax.set_ylabel("m_x_{}_{}".format(*method2) + "/" + "m_x_{}_{}".format(*method1))
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_X_{}-{}_{}-{}.png".format(method1[0], method2[0], method1[1], method2[1]), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df_sg12["tau_x"], df_sg12["tau_x2"]/df_sg12["tau_x"], s=5, c="black")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(3e-3, 3e-2)
    ax.set_ylim(5e-1, 2e0)
    ax.set_xlabel("tau_x_{}_{}".format(*method1))
    ax.set_ylabel("tau_x_{}_{}".format(*method2) + "/" + "tau_x_{}_{}".format(*method1))
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_X_{}-{}_{}-{}.png".format(method1[0], method2[0], method1[1], method2[1]), dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    ax.scatter(df_sg12["chisqr_x"], df_sg12["chisqr_x2"]/df_sg12["chisqr_x"], s=5, c="black")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(9e-5, 6e-3)
    ax.set_ylim(2e-1, 2e1)
    ax.set_xlabel("chisqr_x_{}_{}".format(*method1))
    ax.set_ylabel("chisqr_x_{}_{}".format(*method2) + "/" + "chisqr_x_{}_{}".format(*method1))
    plt.tight_layout()
    plt.savefig(plot_path + "Chisqr_X_{}-{}_{}-{}.png".format(method1[0], method2[0], method1[1], method2[1]), dpi=dpi)
    plt.close()





