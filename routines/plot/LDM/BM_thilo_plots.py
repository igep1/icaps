import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT4/"
plot_dg_path = project_path + "plots_dg/"
thilo_path = project_path + "thilo/"
thilo_fname = "distribution_values_center_uncertainty_gen_{}.txt"
csv_erf_trans_noise_path = project_path + "ERF_Trans_Noise.csv"

df_noise = pd.read_csv(csv_erf_trans_noise_path)
fig, ax = plt.subplots()
data = df_noise.loc[df_noise["dt"] == 1, "sigma"].to_numpy()
data_err = df_noise.loc[df_noise["dt"] == 1, "sigma_err"].to_numpy()
data_err = data_err[np.argsort(data)]
data = np.sort(data)
norm_data = np.linspace(1 / len(data), 1, len(data))
ax.errorbar(data, norm_data, xerr=data_err, color="red", fmt="o", markersize=1, label="Measured Data")
gens = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for i, gen in enumerate(gens):
    df_thilo = pd.read_csv(thilo_path + thilo_fname.format(gen), header=None)
    data = df_thilo[0]
    data = np.sort(data) / icaps.const.r_0 * 1e-12
    norm_data = np.linspace(1 / len(data), 1, len(data))
    ax.scatter(data, norm_data, marker="x", s=2,
               color=np.repeat(0.75 / (len(gens)) * (len(gens) - i + 1), 3).tolist(),
               label="Simulated Data (BCCA)\n({} - {} Particles)".format(2**gens[0], 2**gens[-1])
               if i == int(len(gens) / 2) else None)
ax.set_xscale("log")
ax.set_xlabel(r"$\Delta s$ (m)")
ax.set_ylabel("Normalized Cumulative Frequency")
ax.annotate("Number of Particles", xy=(0.35, 0.907), xycoords="figure fraction", xytext=(0.55, 0.9),
            arrowprops=dict(arrowstyle="<-", color="black", linewidth=2), clip_on=False)
ax.minorticks_on()
ax.set_xlim(3e-8, 7e-6)
ax.legend()
plt.savefig(plot_dg_path + "Noise_Sim.png", dpi=600)
plt.close()

