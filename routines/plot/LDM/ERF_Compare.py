import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
csv_test = project_path + "ERF_Trans_Exp_0.csv"
csv_final = project_path + "OF_Final.csv"
plot_path = project_path + "plots/erf_compare/"
icaps.mk_folders(plot_path)
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
max_chisqr_trans = 40e-4
max_rel_err_trans_mass = 0.1

dpi = 600
fancybox = False
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df = pd.read_csv(csv_test)
    df_all = pd.read_csv(csv_final)
    df_m = icaps.brownian.filter_chisqr(df_all, thresh=max_chisqr_trans, mode="trans")
    df_m = icaps.brownian.filter_rel_err(df_m, mass=max_rel_err_trans_mass, mode="trans")
    df = df[df["particle"].isin(df_m["particle"].unique())]
    axis = "x"
    dts = (1, 10)
    fig, ax = plt.subplots(nrows=2)
    for i, dt in enumerate(dts):
        df_ = df[(df["axis"] == axis) & (df["dt"] == dt)]
        chisqr_dg = df_["chisqr_dg"].to_numpy()
        chisqr_exp = df_["chisqr_exp"].to_numpy()
        chisqr = df_["chisqr"].to_numpy()
        ax[i].errorbar(np.sort(chisqr_dg), np.linspace(1 / len(chisqr_dg), 1, len(chisqr_dg)),
                       fmt="o", c="black", markersize=1, label=r"$\Phi_2$")
        ax[i].errorbar(np.sort(chisqr_exp), np.linspace(1 / len(chisqr_exp), 1, len(chisqr_exp)),
                       fmt="o", c="gray", markersize=1, label=r"$\Phi_2'$")
        ax[i].errorbar(np.sort(chisqr), np.linspace(1 / len(chisqr), 1, len(chisqr)),
                       fmt="o", c="lightgray", markersize=1, label=r"$\Phi$")
        ax[i].set_xscale("log")
        # ax[i].set_xlim(1e-6, 2e-2)
        ax[i].set_ylim(-0.02, 1.02)
        ax[i].set_yticks([0.1, 0.3, 0.5, 0.7, 0.9])
        ax[i].annotate(r"$\Delta t$ = {} ms".format(dt), xy=(1.12e-6, 0.986), xycoords="data",
                       size=13, ha="left", va="top",
                       bbox=dict(boxstyle="square", fc="w"))
    ax[0].legend(loc=(0., 0.), fancybox=fancybox, bbox_to_anchor=(0., 1.01), ncol=3, columnspacing=1.)
    ax[0].set_xticks([])
    ax[1].set_xlabel(r"$\chi_{\Phi}^2$")
    fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    fig.subplots_adjust(hspace=.001, wspace=.001)
    fig.subplots_adjust(bottom=0.15)
    plt.savefig(plot_path + "ERF_Compare.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()

