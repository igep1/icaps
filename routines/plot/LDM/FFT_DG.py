import numpy as np
import pandas as pd
from scipy.fft import fft, fftfreq
from scipy.ndimage.filters import uniform_filter1d
import icaps
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
plot_path = project_path + "plots/fft/"
sub_trans_plot_path = plot_path + "single_trans/"
sub_rot_plot_path = plot_path + "single_rot/"
csv_disp_t = project_path + "DISP_Trans.csv"
csv_disp_r = project_path + "DISP_Rot.csv"
csv_of = project_path + "OF_Final_diff_drop1.csv"
csv_fft_stats = project_path + "FFT_stats.csv"

max_chisqr_trans = 40e-4
max_chisqr_rot = 60e-4
max_rel_err_trans_mass = 0.1
max_rel_err_trans_tau = 0.1
max_rel_err_rot_moi = 0.5
max_rel_err_rot_tau = 0.5

icaps.mk_folders([plot_path, sub_trans_plot_path, sub_rot_plot_path])
axes = ["x", "y"]
dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df_all = pd.read_csv(csv_of)
    df_t1 = icaps.brownian.filter_chisqr(df_all, thresh=max_chisqr_trans, mode="trans")
    df_t1 = icaps.brownian.filter_rel_err(df_t1, mass=max_rel_err_trans_mass, mode="trans")
    df_t2 = icaps.brownian.filter_rel_err(df_t1, tau=max_rel_err_trans_tau, mode="trans")
    df_r1 = df_t1.dropna(subset=["tau_rot"])
    df_r1 = icaps.brownian.filter_chisqr(df_r1, thresh=max_chisqr_rot, mode="rot")
    df_r1 = icaps.brownian.filter_rel_err(df_r1, mass=max_rel_err_rot_moi, mode="rot")
    df_r2 = icaps.brownian.filter_rel_err(df_r1, tau=max_rel_err_trans_tau, mode="trans")
    df_r2 = icaps.brownian.filter_rel_err(df_r2, tau=max_rel_err_rot_tau, mode="rot")
    df_disp_t = pd.read_csv(csv_disp_t)
    df_disp_r = pd.read_csv(csv_disp_r)

    # # ===================================================================================
    # # TEST METHOD
    # # ===================================================================================
    # df_s = df_t2.sort_values(by=["n_frames"], ascending=False)
    # particle = df_s.dropna(subset=["inertia"])["particle"].to_numpy()[0]
    # y = df_disp_t.loc[df_disp_t["particle"] == particle,
    #                   f"dx_1"].dropna().to_numpy() * icaps.const.px_ldm * 1e-3
    # y = np.cumsum(y)
    # y = np.append(np.array([0.]), y)
    # yf = fft(y)
    # n = len(y)
    # xf = fftfreq(n, 1 / icaps.const.fps_ldm)
    # xf = xf[:n // 2]
    # yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[:n // 2])) ** 2
    # tau = df_all.loc[df_all["particle"] == particle, "tau_x"].to_numpy()[0]
    # mass = df_all.loc[df_all["particle"] == particle, "mass_x"].to_numpy()[0]
    # yfp /= (icaps.const.k_B * icaps.const.T * tau / mass)
    # fig, ax = plt.subplots()
    # ax.scatter(xf, yfp, s=1, c="black")
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    # ax.set_xlabel(r"$f \,\rm(Hz)$")
    # ax.set_ylabel(r"$P_\theta/D_x \,\rm(a.u.)$")
    # plt.tight_layout()
    #
    # y = df_disp_r.loc[df_disp_r["particle"] == particle, "omega_1"].dropna().to_numpy()
    # y = np.cumsum(y)
    # y = np.append(np.array([0.]), y)
    # yf = fft(y)
    # n = len(y)
    # xf = fftfreq(n, 1 / icaps.const.fps_ldm)
    # xf = xf[:n // 2]
    # yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[:n // 2])) ** 2
    # tau = df_all.loc[df_all["particle"] == particle, "tau_rot"].to_numpy()[0]
    # mass = df_all.loc[df_all["particle"] == particle, "inertia"].to_numpy()[0]
    # yfp /= (icaps.const.k_B * icaps.const.T * tau / mass)
    # fig, ax = plt.subplots()
    # ax.scatter(xf, yfp, s=1, c="black")
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    # ax.set_xlabel(r"$f \,\rm(Hz)$")
    # ax.set_ylabel(r"$P_\theta/D_\theta \,\rm(a.u.)$")
    # plt.tight_layout()
    # plt.show()

    # # ===================================================================================
    # # QUICK EXAMPLES
    # # ===================================================================================
    # df_s = df_t1.sort_values(by=["n_frames"], ascending=False)
    # # print(df_s[["n_frames", "particle", "tau", "mass"]])
    # # particles = [720240]
    # particles = df_s["particle"].to_numpy()
    # particles = [particles[0]]
    # for particle in particles:
    #     fig, ax = plt.subplots(nrows=2)
    #     ylims = [1e100, 1e-100]
    #     for i, axis in enumerate(axes):
    #         y = df_disp_t.loc[df_disp_t["particle"] == particle,
    #                           f"d{axis}_1"].dropna().to_numpy() * icaps.const.px_ldm * 1e-3
    #         y = np.cumsum(y)
    #         y = np.append(np.array([0.]), y)
    #         yf = fft(y)
    #         n = len(y)
    #         xf = fftfreq(n, 1/icaps.const.fps_ldm)[:n//2]
    #         # yfp = 1./(n * icaps.const.fps_ldm) * np.abs(yf[0:n//2])
    #         yfp = 1/(n * icaps.const.fps_ldm) * (np.abs(yf[0:n//2])) ** 2
    #         # yfp *= 1000
    #
    #         ax[i].scatter(xf, yfp, c="black", marker="o", s=2, alpha=0.2)
    #         movavrg = uniform_filter1d(yfp, size=20)
    #         ax[i].scatter(xf, movavrg, c="black", marker="o", s=2)
    #         # idx_lmax = argrelmax(yfp, order=20)
    #         # ax[i].plot(xf[idx_lmax], yfp[idx_lmax], c="red", lw=2, ls="-")
    #         ax[i].set_ylabel(r"$P_{{{}}} \,\rm (a. u.)$".format(axis))
    #         ax[i].set_xscale("log")
    #         ax[i].set_yscale("log")
    #         ax[i].set_xlim(7e-2, 5.5e2)
    #         ylims[0] = np.min([ylims[0], np.min(yfp)])
    #         ylims[1] = np.max([ylims[1], np.max(yfp)])
    #     xspace = np.linspace(10, 100, 1000)
    #     for i in range(len(axes)):
    #         ax[i].set_ylim(ylims[0] * 1000, ylims[1])
    #         ax[i].plot(xspace, 1e-4 * xspace**(-2), c="red", ls="--", lw="2")
    #     ax[0].set_xticks([])
    #     ax[1].set_xlabel(r"$f \,\rm (Hz)$")
    #     ax[0].set_title(str(particle))
    #     fig.subplots_adjust(hspace=0.001)
    #     fig.subplots_adjust(bottom=0.15, top=0.93)
    #     fig.subplots_adjust(left=0.15, right=0.93)
    #     plt.show()

    # ===================================================================================
    # STATISTICAL ANALYSIS (Trans)
    # ===================================================================================
    plot_perc = 0
    roller = "log"
    particles = df_t2["particle"].unique()
    xf_all = np.array([])
    yfp_all = np.array([])
    pb = icaps.SimpleProgressBar(len(particles) * 2, "Progress")
    for axis in axes:
        xf_axis = []
        yfp_axis = []
        for i, particle in enumerate(particles):
            pb.set_stage(particle)
            y = df_disp_t.loc[df_disp_t["particle"] == particle,
                              f"d{axis}_1"].dropna().to_numpy() * icaps.const.px_ldm * 1e-3
            y = np.cumsum(y)
            y = np.append(np.array([0.]), y)
            yf = fft(y)
            n = len(y)
            xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
            yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
            tau = df_all.loc[df_all["particle"] == particle, f"tau_{axis}"].to_numpy()[0]
            mass = df_all.loc[df_all["particle"] == particle, f"mass_{axis}"].to_numpy()[0]
            yfp_ = yfp
            # yfp_ = yfp / (icaps.const.k_B * icaps.const.T * tau / mass)
            xf_axis = np.append(xf_axis, xf)
            yfp_axis = np.append(yfp_axis, yfp_)
            xf_all = np.append(xf_all, xf)
            yfp_all = np.append(yfp_all, yfp_)
            if (i + 1) / len(particles) <= plot_perc:
                fig, ax = plt.subplots()
                # ax.scatter(xf, yfp, c="black", marker="o", s=2, alpha=0.2)
                ax.scatter(xf, yfp, c="black", marker="o", s=1, alpha=1)
                if roller == "lin":
                    movavrg = pd.Series(yfp).rolling(20, center=True).mean().to_numpy()
                else:
                    movavrg = 10 ** pd.Series(np.log10(yfp)).rolling(20, center=True).mean().to_numpy()
                # ax.scatter(xf, movavrg, c="black", marker="o", s=2)
                ax.set_ylabel(r"$P_{{{}}} \,\rm (a. u.)$".format(axis))
                ax.set_xlabel(r"$f \,\rm (Hz)$")
                ax.set_xscale("log")
                ax.set_yscale("log")
                ax.set_xlim(7e-2, 5.5e2)
                ax.set_ylim(3e-11, 3e-2)
                xspace = np.linspace(2e-1, 400, 1000)
                ax.plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
                ax.set_title(f"{particle:06d}")
                plt.tight_layout()
                plt.savefig(sub_trans_plot_path + f"{particle:06d}_{axis}.png", dpi=dpi)
                plt.close()
            pb.tick()
        idx = np.argsort(xf_axis)
        xf_axis = xf_axis[idx]
        yfp_axis = yfp_axis[idx]
        fig, ax = plt.subplots()
        ax.errorbar(xf_axis, yfp_axis, c="lightgray", fmt=".", markersize=1, alpha=1, zorder=0)
        if roller == "lin":
            movavrg = pd.Series(yfp_axis).rolling(len(particles), center=True).mean().to_numpy()
        else:
            movavrg = 10 ** pd.Series(np.log10(yfp_axis)).rolling(len(particles), center=True).mean().to_numpy()
        ax.errorbar(xf_axis, movavrg, c="black", fmt=".", markersize=1, alpha=1, zorder=1)
        ax.set_ylabel(r"$P_{{{}}} \,\rm (a. u.)$".format(axis))
        # ax.set_ylabel(r"$P_{{{}}} / D_{{{}}} \,\rm (a. u.)$".format(axis, axis))
        ax.set_xlabel(r"$f \,\rm (Hz)$")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(7e-2, 5.5e2)
        ax.set_ylim(3e-11, 3e-2)
        # ax.set_ylim(3e-2, 3e8)
        xspace = np.linspace(2e-1, 400, 1000)
        ax.plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
        ax.text(100, 2.4e-8, r"$\sim f^{-2}$", color="red",
                bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
        # ax.plot(xspace, 7e5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
        # ax.text(100, 2.4e6, r"$\sim f^{-2}$", color="red",
        #         bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
        icaps.mirror_axis(ax)
        fig.subplots_adjust(bottom=0.12, left=0.16)
        plt.savefig(plot_path + f"FFT_coll_{axis}.png", dpi=dpi)
        plt.close()
    pb.close()
    idx = np.argsort(xf_all)
    xf_all = xf_all[idx]
    yfp_all = yfp_all[idx]
    fig, ax = plt.subplots()
    ax.errorbar(xf_all, yfp_all, c="lightgray", fmt=".", markersize=1, alpha=1, zorder=0)
    if roller == "lin":
        movavrg = pd.Series(yfp_all).rolling(len(particles), center=True).mean().to_numpy()
    else:
        movavrg = 10 ** pd.Series(np.log10(yfp_all)).rolling(len(particles), center=True).mean().to_numpy()
    ax.errorbar(xf_all, movavrg, c="black", fmt=".", markersize=1, alpha=1, zorder=1)
    ax.set_ylabel(r"$P_{x, y} \,\rm (a. u.)$")
    # ax.set_ylabel(r"$P_{x, y} / D_{x,y} \,\rm (a. u.)$")
    ax.set_xlabel(r"$f \,\rm (Hz)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(7e-2, 5.5e2)
    ax.set_ylim(3e-11, 3e-2)
    # ax.set_ylim(3e-2, 3e8)
    xspace = np.linspace(2e-1, 400, 1000)
    ax.plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax.text(100, 2.4e-8, r"$\sim f^{-2}$", color="red",
            bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    # ax.plot(xspace, 7e5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    # ax.text(100, 2.4e6, r"$\sim f^{-2}$", color="red",
    #         bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    icaps.mirror_axis(ax)
    fig.subplots_adjust(bottom=0.12, left=0.16)
    # plt.show()
    plt.savefig(plot_path + "FFT_coll_trans.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # STATISTICAL ANALYSIS (Rot)
    # ===================================================================================
    plot_perc = 1
    roller = "log"
    particles = df_r2["particle"].unique()
    print(len(particles))
    pd.DataFrame(data={"particle": particles}).to_csv(project_path + "good_rot_particles.csv", index=False)
    xf_all = np.array([])
    yfp_all = np.array([])
    pb = icaps.SimpleProgressBar(len(particles), "Progress")
    for i, particle in enumerate(particles):
        pb.set_stage(particle)
        y = df_disp_r.loc[df_disp_r["particle"] == particle, "omega_1"].dropna().to_numpy()
        y = np.cumsum(y)
        y = np.append(np.array([0.]), y)
        yf = fft(y)
        n = len(y)
        xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
        yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
        # yfp = 1 / n * (2 * np.abs(yf[0:n // 2])) ** 2
        tau = df_all.loc[df_all["particle"] == particle, "tau_rot"].to_numpy()[0]
        mass = df_all.loc[df_all["particle"] == particle, "inertia"].to_numpy()[0]
        yfp_ = yfp
        # yfp_ = yfp / (icaps.const.k_B * icaps.const.T * tau / mass)
        xf_all = np.append(xf_all, xf)
        yfp_all = np.append(yfp_all, yfp_)
        if ((i + 1) / len(particles) <= plot_perc) or (particle == 720240):
            fig, ax = plt.subplots()
            # ax.scatter(xf, yfp, c="black", marker="o", s=2, alpha=0.2)
            ax.scatter(xf, yfp, c="black", marker="o", s=1, alpha=1)
            if roller == "lin":
                movavrg = pd.Series(yfp).rolling(20, center=True).mean().to_numpy()
            else:
                movavrg = 10 ** pd.Series(np.log10(yfp)).rolling(20, center=True).mean().to_numpy()
            # ax.scatter(xf, movavrg, c="black", marker="o", s=2)
            ax.set_ylabel(r"$P_\theta \,\rm (a. u.)$")
            ax.set_xlabel(r"$f \,\rm (Hz)$")
            # ax.set_xscale("log")
            ax.set_yscale("log")
            ax.set_xlim(7e-2, 5.5e2)
            ax.set_ylim(3e-9, 3e1)
            xspace = np.linspace(2e-1, 400, 1000)
            ax.plot(xspace, 1e0 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
            ax.set_title(f"{particle:06d}")
            plt.tight_layout()
            plt.savefig(sub_rot_plot_path + f"{particle:06d}_rot.png", dpi=dpi)
            plt.close()
        pb.tick()
    pb.close()
    idx = np.argsort(xf_all)
    xf_all = xf_all[idx]
    yfp_all = yfp_all[idx]
    fig, ax = plt.subplots()
    ax.errorbar(xf_all, yfp_all, c="lightgray", fmt=".", markersize=1, alpha=1, zorder=0)
    if roller == "lin":
        movavrg = pd.Series(yfp_all).rolling(len(particles), center=True).mean().to_numpy()
    else:
        movavrg = 10 ** pd.Series(np.log10(yfp_all)).rolling(len(particles), center=True).mean().to_numpy()
    ax.errorbar(xf_all, movavrg, c="black", fmt=".", markersize=1, alpha=1, zorder=1)
    ax.set_ylabel(r"$P_\theta \,\rm (a. u.)$")
    # ax.set_ylabel(r"$P_\theta / D_\theta \,\rm (a. u.)$")
    ax.set_xlabel(r"$f \,\rm (Hz)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(7e-2, 5.5e2)
    ax.set_ylim(3e-7, 3e1)
    # ax.set_ylim(2e-10, 1e1)
    xspace = np.linspace(2e-1, 400, 1000)
    ax.plot(xspace, 1e0 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax.text(100, 5e-4, r"$\sim f^{-2}$", color="red",
            bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    # ax.plot(xspace, 5e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    # ax.text(100, 1.5e-3, r"$\sim f^{-2}$", color="red",
    #         bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    icaps.mirror_axis(ax)
    fig.subplots_adjust(bottom=0.12, left=0.16)
    # plt.show()
    plt.savefig(plot_path + "FFT_coll_rot.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()

