import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import matplotlib.patches as patches
from scipy.ndimage import uniform_filter1d
from scipy.fft import fft, fftfreq

# Paths
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM/"
plot_path = project_path + "plots/"
csv_of_final = project_path + "OF_Final.csv"
csv_disp_t = project_path + "DISP_Trans.csv"
csv_erf_t = project_path + "ERF_Trans.csv"
csv_erf_t_flagged = project_path + "ERF_Trans_flagged.csv"
csv_disp_r = project_path + "DISP_Rot.csv"
csv_erf_r = project_path + "ERF_Rot.csv"
csv_erf_r_flagged = project_path + "ERF_Rot_flagged.csv"

# Filters
dts = np.arange(1, 11, 1)
dts = np.append(dts, np.arange(11, 26, 2))
dts = np.append(dts, np.arange(30, 51, 5))
dx_max = 1.75
omega_max = 0.175
res_max = 0.029

# Plots
fancybox = False
dpi = 600
particle = 720240
plot_path += "{}/".format(particle)
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    icaps.mk_folder(plot_path)
    df_disp_t = pd.read_csv(csv_disp_t)
    df_disp_t = df_disp_t[df_disp_t["particle"] == particle]
    # df_erf_t_exp = pd.read_csv(project_path + "ERF_Trans_Exp_2.csv")
    # df_erf_t_exp = df_erf_t_exp[df_erf_t_exp["particle"] == particle]
    df_erf_t = pd.read_csv(csv_erf_t_flagged)
    df_erf_t = df_erf_t[df_erf_t["particle"] == particle]
    df_disp_r = pd.read_csv(csv_disp_r)
    df_disp_r = df_disp_r[df_disp_r["particle"] == particle]
    df_erf_r = pd.read_csv(csv_erf_r_flagged)
    df_erf_r = df_erf_r[df_erf_r["particle"] == particle]
    df_all = pd.read_csv(csv_of_final)
    df_all = df_all[df_all["particle"] == particle]
    pd.set_option("display.max_rows", 500)
    pd.set_option("display.max_columns", 500)
    pd.set_option("display.width", 1000)
    print(df_all)

    # # ===================================================================================
    # # TEST ERF
    # # ===================================================================================
    # dt = 1
    # axis = "x"
    # _, ax = plt.subplots()
    # ax.minorticks_on()
    # x = np.sort(df_disp_t[f"d{axis}_{dt}"].dropna()) * icaps.const.px_ldm
    # y = np.linspace(1 / len(x), 1, len(x))
    # ax.scatter(x, y, c="black", s=1)
    # lim = np.max(np.abs(ax.get_xlim()))
    # ax.set_xlim(-lim, lim)
    #
    # def test_erf(dx, mu, sig, a, lamb_):
    #     res = (1 - a) * icaps.fit_erf(dx, mu, sig)
    #     for k in range(len(dx)):
    #         if dx[k] - mu < 0:
    #             res[k] += a / 2 * np.exp((dx[k] - mu) / lamb_)
    #         else:
    #             res[k] += a / 2 * (2 - np.exp(-(dx[k] - mu) / lamb_))
    #     return res
    #
    # xspace = np.linspace(-lim, lim, 1000)
    # out = icaps.fit_model(icaps.fit_erf, x, y, fit_type=2)
    # print(out.beta)
    # p0 = [out.beta[0], out.beta[1], 0.3, 0.2]
    # ax.plot(xspace, icaps.fit_erf(xspace, *out.beta), c="gray", ls="--")
    # out = icaps.fit_model(test_erf, x, y, fit_type=2)
    # print(out.beta)
    # ax.plot(xspace, test_erf(xspace, *out.beta), c="red", ls="-")
    # out = icaps.fit_model(icaps.fit_erf_dg, x, y, fit_type=2)
    # print(out.beta)
    # ax.plot(xspace, icaps.fit_erf_dg(xspace, *out.beta), c="blue", ls="-.")
    # plt.show()

    # ===================================================================================
    # ERF 3 x 2
    # ===================================================================================
    # ===========================
    # Trans 3 x 2
    dts_ = [1, 5, 10, 21, 35, 50]
    for axis in ["x", "y"]:
        fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={"height_ratios": np.tile([2, 1], len(dts_) // 2)},
                               figsize=[5.2, 6.4])
        plt.subplots_adjust(hspace=.001, wspace=.001)
        xspace = np.linspace(-11.5, 11.5, 1000)
        for j, dt in enumerate(dts_):
            col = j % 2
            row0 = 2 * (j // 2)
            row1 = 2 * (j // 2) + 1
            ax0 = ax[row0][col]
            ax1 = ax[row1][col]
            ax0.set_xlim(-11.5, 11.5)
            ax1.set_xlim(-11.5, 11.5)
            ax0.set_ylim(-0.02, 1.02)
            ax1.set_ylim(-0.1, 0.1)
            ax0.minorticks_on()
            ax1.minorticks_on()
            ax1.axhline(0, c="gray", lw=2, ls="--", zorder=1000)
            ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.027, 0.95), xycoords="axes fraction",
                         size=13, ha="left", va="top",
                         bbox=dict(boxstyle="square", fc="w"))
            if row0 == 0:
                ax0.xaxis.tick_top()
                ax0.set_xticks([-8, 0, 8])
                ax0.xaxis.set_ticklabels([])
                ax1.set_xticks([])
            else:
                ax0.set_xticks([])
                ax1.set_xticks([])
            if row0 == 4:
                ax1.set_xticks([-8, 0, 8])
            if col == 1:
                ax0.set_yticks([])
                ax1.yaxis.tick_right()
            else:
                ax1.set_yticks([])
            df_disp_t_ = df_disp_t[["particle", f"d{axis}_{dt}"]].dropna()
            df_erf_t_ = df_erf_t[(df_erf_t["dt"] == dt) & (df_erf_t["axis"] == axis)]
            dx = df_disp_t_.loc[df_disp_t_["particle"] == particle, f"d{axis}_{dt}"].to_numpy() * icaps.const.px_ldm
            idx = (df_erf_t_["particle"] == particle)
            drift = df_erf_t_.loc[idx, "drift"].to_numpy()[0] * 1e6
            sigma = df_erf_t_.loc[idx, "sigma"].to_numpy()[0] * 1e6
            if sigma > 1e3:
                continue
            ax0.plot(xspace, icaps.fit_erf(xspace, drift, sigma), c="red", lw=2, zorder=2, alpha=0.6)
            x = np.sort(dx)
            y = np.linspace(1 / len(x), 1, len(x))
            ax0.errorbar(x, y,
                         markerfacecolor="black", markeredgecolor="none", markersize=4, fmt=".", zorder=1)
            ax1.errorbar(x, y - icaps.fit_erf(x, drift, sigma),
                         markerfacecolor="black", markeredgecolor="none", markersize=4, fmt=".", zorder=1)

        fig.text(0.50, 0.01, r"$\Delta {{{}}} \,\rm (\mu m)$".format(axis), ha="center")
        fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
        fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
        fig.subplots_adjust(bottom=0.08, top=0.95)
        fig.subplots_adjust(left=0.09, right=0.87)
        plt.savefig(plot_path + f"erf_{particle}_coll_{axis}.png", dpi=dpi)
        plt.close()

    # ===========================
    # Rot 3 x 2
    dts_ = [1, 5, 10, 21, 35, 50]
    fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={"height_ratios": np.tile([2, 1], len(dts_) // 2)},
                           figsize=[5.2, 6.4])
    plt.subplots_adjust(hspace=.001, wspace=.001)
    xspace = np.linspace(-1.15, 1.15, 1000)
    for j, dt in enumerate(dts_):
        col = j % 2
        row0 = 2 * (j // 2)
        row1 = 2 * (j // 2) + 1
        ax0 = ax[row0][col]
        ax1 = ax[row1][col]
        ax0.set_xlim(-1.15, 1.15)
        ax1.set_xlim(-1.15, 1.15)
        ax0.set_ylim(-0.02, 1.02)
        ax1.set_ylim(-0.1, 0.1)
        ax0.minorticks_on()
        ax1.minorticks_on()
        ax1.axhline(0, c="gray", lw=2, ls="--", zorder=1000)
        ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.027, 0.95), xycoords="axes fraction",
                     size=13, ha="left", va="top",
                     bbox=dict(boxstyle="square", fc="w"))
        if row0 == 0:
            ax0.xaxis.tick_top()
            ax0.set_xticks([-.8, 0, .8])
            ax0.xaxis.set_ticklabels([])
            ax1.set_xticks([])
        else:
            ax0.set_xticks([])
            ax1.set_xticks([])
        if row0 == 4:
            ax1.set_xticks([-.8, 0, .8])
        if col == 1:
            ax0.set_yticks([])
            ax1.yaxis.tick_right()
        else:
            ax1.set_yticks([])
        df_disp_r_ = df_disp_r[["particle", f"omega_{dt}"]].dropna()
        df_erf_r_ = df_erf_r[df_erf_r["dt"] == dt]
        dx_label = "omega_{}".format(dt)
        dx = df_disp_r_.loc[df_disp_r_["particle"] == particle, dx_label].to_numpy()
        idx = df_erf_r_["particle"] == particle
        drift = df_erf_r_.loc[idx, "drift"].to_numpy()[0]
        sigma = df_erf_r_.loc[idx, "sigma"].to_numpy()[0]
        if sigma > np.sqrt(np.pi ** 2 / 4):
            continue
        ax0.plot(xspace, icaps.fit_erf(xspace, drift, sigma), c="red", lw=2, zorder=2, alpha=0.6)
        x = np.sort(dx)
        y = np.linspace(1 / len(x), 1, len(x))
        ax0.errorbar(x, y,
                     markerfacecolor="black", markeredgecolor="none", markersize=4, fmt=".", zorder=1)
        ax1.errorbar(x, y - icaps.fit_erf(x, drift, sigma),
                     markerfacecolor="black", markeredgecolor="none", markersize=4, fmt=".", zorder=1)

    fig.text(0.50, 0.01, r"$\Delta\theta\,\rm(rad)$", ha="center")
    fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
    fig.subplots_adjust(bottom=0.08, top=0.95)
    fig.subplots_adjust(left=0.09, right=0.87)
    plt.savefig(plot_path + f"erf_{particle}_coll_rot.png", dpi=dpi)
    plt.close()

    # # ===================================================================================
    # # ERF COLLAGE
    # # ===================================================================================
    # # Trans
    # dts_ = [(1, 5), (10, 21), (35, 50)]
    # axes = ["x", "y"]
    # for axis in axes:
    #     df_erf_t_ = df_erf_t[df_erf_t["axis"] == axis]
    #     fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={"height_ratios": np.tile([2, 1], len(dts_))},
    #                            figsize=[5.2, 6.4])
    #     plt.subplots_adjust(hspace=.001, wspace=.001)
    #     for j in range(len(dts_) * 2):
    #         if j % 2 == 0:
    #             for i in range(len(dts_[int(j / 2)])):
    #                 dt = dts_[int(j / 2)][i]
    #                 ax_ = ax[j][i]
    #                 ax_.minorticks_on()
    #                 disp_column = "d{}_{}".format(axis, dt)
    #                 disp = np.sort(df_disp_t[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
    #                 norm_disp = np.linspace(1 / len(disp), 1, len(disp))
    #                 ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
    #                 sigma = df_erf_t_.loc[df_erf_t_["dt"] == dt, "sigma_diff"].to_numpy()[0] * 1e6
    #                 drift = df_erf_t_.loc[df_erf_t_["dt"] == dt, "drift01"].to_numpy()[0] * 1e6
    #                 # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
    #                 x_space = np.linspace(-11.5, 11.5, 1000)
    #                 ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
    #                 ax_.set_xlim(-11.5, 11.5)
    #                 if int(j / 2) < len(dts_) - 1:
    #                     ax_.set_xticks([])
    #                 if i == 1:
    #                     ax_.set_yticks([])
    #                 # else:
    #                 #     if j == 2:
    #                 #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
    #                 ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-10.8, 1.005), xycoords="data",
    #                              size=13, ha="left", va="top",
    #                              bbox=dict(boxstyle="square", fc="w"))
    #         else:
    #             for i in range(len(dts_[int(j / 2)])):
    #                 dt = dts_[int(j / 2)][i]
    #                 ax_ = ax[j][i]
    #                 ax_.minorticks_on()
    #                 disp_column = "d{}_{}".format(axis, dt)
    #                 disp = np.sort(df_disp_t[disp_column].dropna().to_numpy()) * icaps.const.px_ldm
    #                 norm_disp = np.linspace(1 / len(disp), 1, len(disp))
    #                 sigma = df_erf_t_.loc[df_erf_t_["dt"] == dt, "sigma_diff"].to_numpy()[0] * 1e6
    #                 drift = df_erf_t_.loc[df_erf_t_["dt"] == dt, "drift01"].to_numpy()[0] * 1e6
    #                 disp_res = norm_disp - icaps.fit_erf(disp, drift, sigma)
    #                 ax_.scatter(disp, disp_res, c="black", s=1, marker="o", zorder=1)
    #                 ax_.set_xticks([-8, 0, 8])
    #                 ax_.set_xlim(-11.5, 11.5)
    #                 ax_.set_ylim(-0.06, 0.06)
    #                 ax_.axhline(0, c="grey", zorder=0, lw=1, linestyle="--")
    #                 ax_.yaxis.tick_right()
    #                 # ax_.yaxis.set_axis_label("right")
    #                 if int(j / 2) < len(dts_) - 1:
    #                     ax_.set_xticks([])
    #                 # else:
    #                 #     ax_.set_xlabel(r"$dx$ (µm)")
    #                 if i == 0:
    #                     ax_.set_yticks([])
    #                 # else:
    #                 #     ax_.set_ylabel(r"Residual")
    #     fig.text(0.50, 0.01, r"$\Delta {{{}}}$ (µm)".format(axis), ha="center")
    #     fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    #     fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
    #     fig.subplots_adjust(bottom=0.08, top=0.95)
    #     fig.subplots_adjust(left=0.09, right=0.87)
    #     plt.savefig(plot_path + "erf_{}_coll_{}.png".format(axis, particle), dpi=dpi)
    #     plt.close()
    #
    # # ===========================
    # # Rot
    # dts_ = [(1, 5), (10, 21), (35, 50)]
    # df_erf_r_ = df_erf_r[df_erf_r["axis"] == "rot"]
    # fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={"height_ratios": np.tile([2, 1], len(dts_))},
    #                        figsize=[5.2, 6.4])
    # plt.subplots_adjust(hspace=.001, wspace=.001)
    # for j in range(len(dts_) * 2):
    #     if j % 2 == 0:
    #         for i in range(len(dts_[int(j / 2)])):
    #             dt = dts_[int(j / 2)][i]
    #             ax_ = ax[j][i]
    #             ax_.minorticks_on()
    #             disp_column = "omega_{}".format(dt)
    #             disp = np.sort(df_disp_r[disp_column].dropna().to_numpy())
    #             norm_disp = np.linspace(1 / len(disp), 1, len(disp))
    #             ax_.scatter(disp, norm_disp, c="black", s=3, marker="o", zorder=1)
    #             sigma = df_erf_r_.loc[df_erf_r_["dt"] == dt, "sigma_diff"].to_numpy()[0]
    #             drift = df_erf_r_.loc[df_erf_r_["dt"] == dt, "drift01"].to_numpy()[0]
    #             # x_space = np.linspace(-1.05 * np.max(np.abs(disp)), 1.05 * np.max(np.abs(disp)), 1000)
    #             x_space = np.linspace(-1.15, 1.15, 1000)
    #             ax_.plot(x_space, icaps.fit_erf(x_space, drift, sigma), c="red", linewidth=2, zorder=2, alpha=0.6)
    #             ax_.set_xlim(-1.15, 1.15)
    #             if int(j / 2) < len(dts_) - 1:
    #                 ax_.set_xticks([])
    #             if i == 1:
    #                 ax_.set_yticks([])
    #             # else:
    #             #     if j == 2:
    #             #         ax_.set_ylabel(r"Normalized Cumulative Frequency")
    #             ax_.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(-1.08, 1.005), xycoords="data",
    #                          size=13, ha="left", va="top",
    #                          bbox=dict(boxstyle="square", fc="w"))
    #     else:
    #         for i in range(len(dts_[int(j / 2)])):
    #             dt = dts_[int(j / 2)][i]
    #             ax_ = ax[j][i]
    #             ax_.minorticks_on()
    #             disp_column = "omega_{}".format(dt)
    #             disp = np.sort(df_disp_r[disp_column].dropna().to_numpy())
    #             norm_disp = np.linspace(1 / len(disp), 1, len(disp))
    #             sigma = df_erf_r_.loc[df_erf_r_["dt"] == dt, "sigma_diff"].to_numpy()[0]
    #             drift = df_erf_r_.loc[df_erf_r_["dt"] == dt, "drift01"].to_numpy()[0]
    #             disp_res = norm_disp - icaps.fit_erf(disp, drift, sigma)
    #             ax_.scatter(disp, disp_res, c="black", s=1, marker="o", zorder=1)
    #             ax_.set_xlim(-1.15, 1.15)
    #             ax_.set_xticks([-.8, 0, .8])
    #             ax_.set_ylim(-0.06, 0.06)
    #             ax_.axhline(0, c="grey", zorder=0, lw=1, linestyle="--")
    #             ax_.yaxis.tick_right()
    #             # ax_.yaxis.set_axis_label("right")
    #             if int(j / 2) < len(dts_) - 1:
    #                 ax_.set_xticks([])
    #             # else:
    #             #     ax_.set_xlabel(r"$dx$ (µm)")
    #             if i == 0:
    #                 ax_.set_yticks([])
    #             # else:
    #             #     ax_.set_ylabel(r"Residual")
    # fig.text(0.50, 0.01, r"$\Delta \theta$ (rad)", ha="center")
    # fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    # fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
    # fig.subplots_adjust(bottom=0.08, top=0.95)
    # fig.subplots_adjust(left=0.09, right=0.87)
    # plt.savefig(plot_path + "erf_rot_coll_{}.png".format(particle), dpi=dpi)
    # plt.close()

    # ===================================================================================
    # TRACK
    # ===================================================================================
    # Trans
    fig, ax = plt.subplots()
    # df = df_disp_t[(df_disp_t["frame"] - np.min(df_disp_t["frame"])) / 1000 < 1.82]
    df = df_disp_t.dropna(subset=["x", "y"])
    time = (df["frame"].to_numpy() - np.min(df["frame"])) / 1000
    x = df["x"] - np.min(df["x"])
    y = df["y"] - np.min(df["y"])
    y = np.max(y) - y
    focus = (df["ex"] / df["area"]).to_numpy()
    focus = focus / np.max(focus) + 0.1
    sct = ax.scatter(x, y, c=time, s=np.round(focus * 10).astype(int), cmap=plt.get_cmap("plasma"))
    ax.plot(x, y, c="black", lw=1)
    max_xy = np.max([np.max(x), np.max(y)])
    ax.set_xlim(-3, max_xy + 3)
    ax.set_ylim(-3, max_xy + 3)
    ax.set_xlabel(r"Relative $x$ Position (µm)")
    ax.set_ylabel(r"Relative $y$ Position (µm)")
    icaps.mirror_axis(ax)
    fig.subplots_adjust(bottom=0.12, top=0.92)
    fig.colorbar(sct, ax=ax, label="Time (s)", pad=0.07)
    plt.savefig(plot_path + f"track_trans_{particle}.png", dpi=dpi)
    plt.close()

    # ===========================
    # Rot
    dt = 1
    df = df_disp_r[(df_disp_r["frame"] - np.min(df_disp_r["frame"])) / 1000 < 1.82]
    df = df[["frame", "gyrad", "q84_grad", "ellipse_h", "ellipse_angle", "omega_{}".format(dt)]].dropna()
    # start_angle = np.deg2rad(360 - df["ellipse_angle"].to_numpy()[0] + 90)
    start_angle = np.deg2rad(df["ellipse_angle"].to_numpy()[0] + 180)
    disp = df["omega_{}".format(dt)].to_numpy() * -1
    angle = np.cumsum(np.append([start_angle], disp))
    # time = np.arange(dt, len(angle) + dt, dt) / 1000
    time = np.append([0], (df["frame"].to_numpy() - np.min(df["frame"]) + 1) / 1000)
    ckey = df["q84_grad"].to_numpy()
    ckey = np.append(ckey[0], ckey)
    fig, ax = plt.subplots(subplot_kw={"projection": "polar"})
    sct = ax.scatter(angle, time, c=ckey, s=4, cmap=plt.get_cmap("plasma"))
    ax.plot(angle, time, c="black", lw=1, ls="-", alpha=0.5)
    ax.set_rlabel_position(270)
    ax.yaxis.set_ticklabels(["", "", 0.75, 1.0, 1.25, 1.50, 1.75])
    label_position = ax.get_rlabel_position()
    ax.text(np.radians(label_position - 10), ax.get_rmax() / 2., r"Time (s)", rotation=90,
            # rotation=label_position,
            ha="center", va="center")
    ax.set_rlim(0, 1.05 * np.max(time))
    ax.minorticks_on()
    # pos = ax.imshow(ckey, cmap="plasma", interpolation="none")
    # fig.colorbar(pos, ax=ax)
    fig.colorbar(sct, ax=ax, label=r"Sharpness (arb. units)", pad=0.07)
    plt.savefig(plot_path + f"track_rot_{particle}.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # ERF TRANS X
    # ===================================================================================
    # todo: Why is the test fit better than the curve in this plot?
    dt = 1
    axis = "x"
    df_erf_t_ = df_erf_t
    fig, axes = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]}, figsize=[6.4, 4.8])
    x = np.sort(df_disp_t[f"d{axis}_{dt}"]) * icaps.const.px_ldm
    y = np.linspace(1 / len(x), 1, len(x))
    ax = axes[0]
    ax.errorbar(x, y, fmt="o", c="black", markersize=1)
    xlim = np.max(np.abs(ax.get_xlim()))
    # ax.set_xlim(-xlim, xlim)
    ax.set_xlim(-dx_max, dx_max)
    xspace = np.linspace(*ax.get_xlim(), 1000)
    drift = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "drift"].to_numpy()[0]
    sigma = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma"].to_numpy()[0]
    rss_sg = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "rss"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf(xspace * 1e-6, drift, sigma),
            lw=1, c="red", label=r"$\Phi$", zorder=10, alpha=0.7)
    w0 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "w0"].to_numpy()[0]
    sigma0 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma0"].to_numpy()[0]
    sigma1 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma1"].to_numpy()[0]
    drift01 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "drift01"].to_numpy()[0]
    rss_dg = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "rss_dg"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf_dg(xspace * 1e-6, w0, drift01, sigma0, sigma1),
            lw=1, c="blue", label=r"$\Phi_2$", zorder=11, alpha=0.7)
    y = y[~np.isnan(x)]
    x = x[~np.isnan(x)]
    # out_exp = icaps.fit_model(icaps.fit_erf_exp, x * 1e-6, y, fit_type=2,
    #                           p0=[drift, sigma, 0.1, 0.001],
    #                           # p0=[drift, sigma],
    #                           # maxit=10000
    #                           )
    # print(out_exp.beta)
    # print(out_exp.sd_beta)
    # rss_exp = np.sum((y - icaps.fit_erf_exp(x, *out_exp.beta)) ** 2) / len(x)
    # print(rss_sg, rss_dg, rss_exp)
    w_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "w_exp"].to_numpy()[0]
    sigma_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma_exp"].to_numpy()[0]
    lamb_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "lambda_exp"].to_numpy()[0]
    drift_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "drift_exp"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf_exp(xspace * 1e-6, drift_exp, sigma_exp, w_exp, lamb_exp),
            lw=1, c="green", label=r"$\Phi_2'$", zorder=11, alpha=0.7)
    ax.set_ylabel(r"Normalized Cumulative Frequency")
    var_left = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "var_left"].to_numpy()[0]
    var_right = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "var_right"].to_numpy()[0]
    ax.axvline(var_left * icaps.const.px_ldm * 1e6,
               c="black", lw=1, ls="-.", alpha=0.5, label=r"1$\sigma$ Range", zorder=1000)
    ax.axvline(var_right * icaps.const.px_ldm * 1e6,
               c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    icaps.mirror_axis(ax)
    ax.legend(ncol=4, loc=[0, 1.05], columnspacing=0.9, fancybox=fancybox)

    ins_bbox = [0.1, 0.85, 0.5, 0.17]
    ins_pos = [0.59, 0.11, 0.43 * 4.8 / 6.4, 0.43]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(x, y, fmt="o", c="black", markersize=1)
    ins.plot(xspace, icaps.fit_erf(xspace * 1e-6, drift, sigma),
             lw=1, c="red", zorder=10, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_dg(xspace * 1e-6, w0, drift01, sigma0, sigma1),
             lw=1, c="blue", zorder=11, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_exp(xspace * 1e-6, drift_exp, sigma_exp, w_exp, lamb_exp),
             lw=1, c="green", zorder=12, alpha=0.7)
    ins.axvline(var_right * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    lims = [ins_bbox[0], ins_bbox[1], ins_bbox[0] + ins_bbox[2], ins_bbox[1] + ins_bbox[3]]
    ins.set_xlim(lims[0], lims[2])
    ins.set_ylim(lims[1], lims[3])
    ins.annotate("(b)", (1 - 0.009, 0.22), xycoords="axes fraction", c="black", ha="right", va="top")
    ins_twinx = ins.twinx()
    ins_twinx.set_ylim(*ins.get_ylim())
    ins_twinx.minorticks_on()
    ins_twinx.set_axes_locator(InsetPosition(ax, ins_pos))
    ins.yaxis.set_ticklabels([])
    ins.minorticks_on()
    ins.yaxis.set_visible(False)
    rect = patches.Rectangle((lims[0], lims[1]), ins_bbox[2], ins_bbox[3],
                             linewidth=1, edgecolor="black", facecolor="none", alpha=0.5)
    ax.add_patch(rect)
    ax.annotate("(b)", (lims[0] + 0.62 * ins_bbox[2], lims[1] + 0.19 * ins_bbox[3]), c="black", alpha=0.5)
    ax.xaxis.set_visible(False)
    # ax.indicate_inset_zoom(ins, edgecolor="black")

    ins_pos = [0.09, 0.5, 0.43 * 4.8 / 6.4, 0.43]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(x, y, fmt="o", c="black", markersize=1)
    ins.plot(xspace, icaps.fit_erf(xspace * 1e-6, drift, sigma),
             lw=1, c="red", zorder=10, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_dg(xspace * 1e-6, w0, drift01, sigma0, sigma1),
             lw=1, c="blue", zorder=11, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_exp(xspace * 1e-6, drift_exp, sigma_exp, w_exp, lamb_exp),
             lw=1, c="green", zorder=11, alpha=0.7)
    ins.axvline(var_left * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    lims = [- ins_bbox[0] - ins_bbox[2], 1 - ins_bbox[1] - ins_bbox[3], -ins_bbox[0], 1 - ins_bbox[1]]
    ins.set_xlim(lims[0], lims[2])
    ins.set_ylim(lims[1], lims[3])
    ins.annotate("(a)", (0.03, 0.82), xycoords="axes fraction", c="black")
    ins.minorticks_on()
    rect = patches.Rectangle((lims[0], lims[1]), ins_bbox[2], ins_bbox[3],
                             linewidth=1, edgecolor="black", facecolor="none", alpha=0.5)
    ax.add_patch(rect)
    ax.annotate("(a)", (lims[0] + 0.04 * ins_bbox[2], lims[1] + 0.53 * ins_bbox[3]), c="black", alpha=0.5)
    # ax.indicate_inset_zoom(ins, edgecolor="black")

    ax = axes[1]
    ax.set_xlim(*axes[0].get_xlim())
    ax.set_ylim(-res_max, res_max)
    ax.axhline(0, c="gray", lw=1, ls="--", zorder=1000)
    ax.errorbar(x, y - icaps.fit_erf(x * 1e-6, drift, sigma),
                c="red", fmt="o", markersize=1, alpha=0.7, zorder=1)
    ax.errorbar(x, y - icaps.fit_erf_dg(x * 1e-6, w0, drift01, sigma0, sigma1),
                c="blue", fmt="o", markersize=1, alpha=0.7, zorder=2)
    ax.errorbar(x, y - icaps.fit_erf_exp(x * 1e-6, drift_exp, sigma_exp, w_exp, lamb_exp),
                c="green", fmt="o", markersize=1, alpha=0.7, zorder=0)
    ax.axvline(var_left * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    ax.axvline(var_right * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    ax.set_xlabel(r"$\Delta x \, (\mathrm{\mu m})$")
    ylim = np.max(np.abs(ax.get_ylim()))
    ax.set_ylim(-ylim, ylim)
    twinx = ax.twinx()
    twinx.set_ylim(*ax.get_ylim())
    twinx.minorticks_on()
    twinx.set_ylabel("Residual")
    ax.yaxis.set_ticklabels([])
    ax.minorticks_on()
    fig.subplots_adjust(hspace=.001, wspace=.001)
    fig.subplots_adjust(left=0.11, right=0.85)
    # plt.tight_layout()
    plt.savefig(plot_path + "erf_{}_{}.png".format(axis, particle), dpi=dpi)
    plt.close()

    # ===================================================================================
    # ERF TRANS Y
    # ===================================================================================
    dt = 1
    axis = "y"
    df_erf_t_ = df_erf_t
    fig, axes = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]}, figsize=[6.4, 4.8])
    x = np.sort(df_disp_t[f"d{axis}_{dt}"]) * icaps.const.px_ldm
    y = np.linspace(1 / len(x), 1, len(x))
    ax = axes[0]
    ax.errorbar(x, y, fmt="o", c="black", markersize=1)
    xlim = np.max(np.abs(ax.get_xlim()))
    # ax.set_xlim(-xlim, xlim)
    ax.set_xlim(-dx_max, dx_max)
    xspace = np.linspace(*ax.get_xlim(), 1000)
    drift = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "drift"].to_numpy()[0]
    sigma = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf(xspace * 1e-6, drift, sigma),
            lw=1, c="red", label=r"$\Phi$", zorder=10, alpha=0.7)
    w0 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "w0"].to_numpy()[0]
    sigma0 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma0"].to_numpy()[0]
    sigma1 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma1"].to_numpy()[0]
    drift01 = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "drift01"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf_dg(xspace * 1e-6, w0, drift01, sigma0, sigma1),
            lw=1, c="blue", label=r"$\Phi_2$", zorder=11, alpha=0.7)
    w_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "w_exp"].to_numpy()[0]
    sigma_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "sigma_exp"].to_numpy()[0]
    lamb_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "lambda_exp"].to_numpy()[0]
    drift_exp = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "drift_exp"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf_exp(xspace * 1e-6, drift_exp, sigma_exp, w_exp, lamb_exp),
            lw=1, c="green", label=r"$\Phi_2'$", zorder=11, alpha=0.7)

    ax.set_ylabel(r"Normalized Cumulative Frequency")
    var_left = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "var_left"].to_numpy()[0]
    var_right = df_erf_t_.loc[(df_erf_t_["dt"] == dt) & (df_erf_t_["axis"] == axis), "var_right"].to_numpy()[0]
    ax.axvline(var_left * icaps.const.px_ldm * 1e6,
               c="black", lw=1, ls="-.", alpha=0.5, label=r"1$\sigma$ Range", zorder=1000)
    ax.axvline(var_right * icaps.const.px_ldm * 1e6,
               c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    icaps.mirror_axis(ax)
    ax.legend(ncol=3, loc=[0, 1.05], columnspacing=0.9, fancybox=fancybox)

    ins_bbox = [0.1, 0.85, 0.5, 0.17]
    ins_pos = [0.59, 0.11, 0.43 * 4.8 / 6.4, 0.43]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(x, y, fmt="o", c="black", markersize=1)
    ins.plot(xspace, icaps.fit_erf(xspace * 1e-6, drift, sigma), lw=1, c="red", zorder=10, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_dg(xspace * 1e-6, w0, drift01, sigma0, sigma1), lw=1, c="blue", zorder=11, alpha=0.7)
    ins.axvline(var_right * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    lims = [ins_bbox[0], ins_bbox[1], ins_bbox[0] + ins_bbox[2], ins_bbox[1] + ins_bbox[3]]
    ins.set_xlim(lims[0], lims[2])
    ins.set_ylim(lims[1], lims[3])
    ins.annotate("(b)", (1 - 0.009, 0.22), xycoords="axes fraction", c="black", ha="right", va="top")
    ins_twinx = ins.twinx()
    ins_twinx.set_ylim(*ins.get_ylim())
    ins_twinx.minorticks_on()
    ins_twinx.set_axes_locator(InsetPosition(ax, ins_pos))
    ins.yaxis.set_ticklabels([])
    ins.minorticks_on()
    ins.yaxis.set_visible(False)
    rect = patches.Rectangle((lims[0], lims[1]), ins_bbox[2], ins_bbox[3],
                             linewidth=1, edgecolor="black", facecolor="none", alpha=0.5)
    ax.add_patch(rect)
    ax.annotate("(b)", (lims[0] + 0.62 * ins_bbox[2], lims[1] + 0.19 * ins_bbox[3]), c="black", alpha=0.5)
    # ax.indicate_inset_zoom(ins, edgecolor="black")

    ins_pos = [0.09, 0.5, 0.43 * 4.8 / 6.4, 0.43]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(x, y, fmt="o", c="black", markersize=1)
    ins.plot(xspace, icaps.fit_erf(xspace * 1e-6, drift, sigma), lw=1, c="red", zorder=10, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_dg(xspace * 1e-6, w0, drift01, sigma0, sigma1), lw=1, c="blue", zorder=11, alpha=0.7)
    ins.axvline(var_left * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    lims = [- ins_bbox[0] - ins_bbox[2], 1 - ins_bbox[1] - ins_bbox[3], -ins_bbox[0], 1 - ins_bbox[1]]
    ins.set_xlim(lims[0], lims[2])
    ins.set_ylim(lims[1], lims[3])
    ins.annotate("(a)", (0.03, 0.82), xycoords="axes fraction", c="black")
    ins.minorticks_on()
    rect = patches.Rectangle((lims[0], lims[1]), ins_bbox[2], ins_bbox[3],
                             linewidth=1, edgecolor="black", facecolor="none", alpha=0.5)
    ax.add_patch(rect)
    ax.annotate("(a)", (lims[0] + 0.04 * ins_bbox[2], lims[1] + 0.53 * ins_bbox[3]), c="black", alpha=0.5)
    ax.xaxis.set_visible(False)
    # ax.indicate_inset_zoom(ins, edgecolor="black")

    ax = axes[1]
    ax.set_xlim(*axes[0].get_xlim())
    ax.set_ylim(-res_max, res_max)
    ax.axhline(0, c="gray", lw=1, ls="--", zorder=1000)
    ax.errorbar(x, y - icaps.fit_erf(x * 1e-6, drift, sigma),
                c="red", fmt="o", markersize=1, alpha=0.7, zorder=1)
    ax.errorbar(x, y - icaps.fit_erf_dg(x * 1e-6, w0, drift01, sigma0, sigma1),
                c="blue", fmt="o", markersize=1, alpha=0.7, zorder=2)
    ax.axvline(var_left * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    ax.axvline(var_right * icaps.const.px_ldm * 1e6, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    ax.set_xlabel(r"$\Delta y \, (\mathrm{\mu m})$")
    ylim = np.max(np.abs(ax.get_ylim()))
    ax.set_ylim(-ylim, ylim)
    twinx = ax.twinx()
    twinx.set_ylim(*ax.get_ylim())
    twinx.minorticks_on()
    twinx.set_ylabel("Residual")
    ax.yaxis.set_ticklabels([])
    ax.minorticks_on()
    fig.subplots_adjust(hspace=.001, wspace=.001)
    fig.subplots_adjust(left=0.11, right=0.85)
    # plt.tight_layout()
    plt.savefig(plot_path + "erf_{}_{}.png".format(axis, particle), dpi=dpi)
    plt.close()

    # ===================================================================================
    # ERF ROT
    # ===================================================================================
    dt = 1
    axis = "rot"
    fig, axes = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]}, figsize=[6.4, 4.8])
    x = np.sort(df_disp_r[f"omega_{dt}"])
    y = np.linspace(1 / len(x), 1, len(x))
    ax = axes[0]
    ax.errorbar(x, y, fmt="o", c="black", markersize=1)
    xlim = np.max(np.abs(ax.get_xlim()))
    # ax.set_xlim(-xlim, xlim)
    ax.set_xlim(-omega_max, omega_max)
    xspace = np.linspace(*ax.get_xlim(), 1000)
    drift = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "drift"].to_numpy()[0]
    sigma = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "sigma"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf(xspace, drift, sigma),
            lw=1, c="red", label=r"$\Phi$", zorder=10, alpha=0.7)
    w0 = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "w0"].to_numpy()[0]
    sigma0 = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "sigma0"].to_numpy()[0]
    sigma1 = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "sigma1"].to_numpy()[0]
    drift01 = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "drift01"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf_dg(xspace, w0, drift01, sigma0, sigma1),
            lw=1, c="blue", label=r"$\Phi_2$", zorder=11, alpha=0.7)
    w_exp = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "w_exp"].to_numpy()[0]
    sigma_exp = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "sigma_exp"].to_numpy()[0]
    lamb_exp = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "lambda_exp"].to_numpy()[0]
    drift_exp = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "drift_exp"].to_numpy()[0]
    ax.plot(xspace, icaps.fit_erf_exp(xspace, drift_exp, sigma_exp, w_exp, lamb_exp),
            lw=1, c="green", label=r"$\Phi_2'$", zorder=11, alpha=0.7)

    ax.set_ylabel(r"Normalized Cumulative Frequency")
    var_left = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "var_left"].to_numpy()[0]
    var_right = df_erf_r.loc[(df_erf_r["dt"] == dt) & (df_erf_r["axis"] == axis), "var_right"].to_numpy()[0]
    ax.axvline(var_left, c="black", lw=1, ls="-.", alpha=0.5, label=r"1$\sigma$ Range", zorder=1000)
    ax.axvline(var_right, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    icaps.mirror_axis(ax)
    ax.legend(ncol=4, loc=[0, 1.05], columnspacing=0.9, fancybox=fancybox)

    ins_bbox = [0.01, 0.85, 0.05, 0.17]
    ins_pos = [0.59, 0.11, 0.43 * 4.8 / 6.4, 0.43]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(x, y, fmt="o", c="black", markersize=1)
    ins.plot(xspace, icaps.fit_erf(xspace, drift, sigma), lw=1, c="red", zorder=10, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_dg(xspace, w0, drift01, sigma0, sigma1), lw=1, c="blue", zorder=11, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_exp(xspace, drift_exp, sigma_exp, w_exp, lamb_exp),
             lw=1, c="green", zorder=11, alpha=0.7)
    ins.axvline(var_right, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    lims = [ins_bbox[0], ins_bbox[1], ins_bbox[0] + ins_bbox[2], ins_bbox[1] + ins_bbox[3]]
    ins.set_xlim(lims[0], lims[2])
    ins.set_ylim(lims[1], lims[3])
    ins.annotate("(b)", (1 - 0.009, 0.22), xycoords="axes fraction", c="black", ha="right", va="top")
    ins_twinx = ins.twinx()
    ins_twinx.set_ylim(*ins.get_ylim())
    ins_twinx.minorticks_on()
    ins_twinx.set_axes_locator(InsetPosition(ax, ins_pos))
    ins.yaxis.set_ticklabels([])
    ins.minorticks_on()
    ins.yaxis.set_visible(False)
    rect = patches.Rectangle((lims[0], lims[1]), ins_bbox[2], ins_bbox[3],
                             linewidth=1, edgecolor="black", facecolor="none", alpha=0.5)
    ax.add_patch(rect)
    ax.annotate("(b)", (lims[0] + 0.62 * ins_bbox[2], lims[1] + 0.19 * ins_bbox[3]), c="black", alpha=0.5)
    # ax.indicate_inset_zoom(ins, edgecolor="black")

    ins_pos = [0.09, 0.5, 0.43 * 4.8 / 6.4, 0.43]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(x, y, fmt="o", c="black", markersize=1)
    ins.plot(xspace, icaps.fit_erf(xspace, drift, sigma), lw=1, c="red", zorder=10, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_dg(xspace, w0, drift01, sigma0, sigma1), lw=1, c="blue", zorder=11, alpha=0.7)
    ins.plot(xspace, icaps.fit_erf_exp(xspace, drift_exp, sigma_exp, w_exp, lamb_exp),
             lw=1, c="green", zorder=11, alpha=0.7)
    ins.axvline(var_left, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    lims = [- ins_bbox[0] - ins_bbox[2], 1 - ins_bbox[1] - ins_bbox[3], -ins_bbox[0], 1 - ins_bbox[1]]
    ins.set_xlim(lims[0], lims[2])
    ins.set_ylim(lims[1], lims[3])
    ins.annotate("(a)", (0.03, 0.82), xycoords="axes fraction", c="black")
    ins.minorticks_on()
    rect = patches.Rectangle((lims[0], lims[1]), ins_bbox[2], ins_bbox[3],
                             linewidth=1, edgecolor="black", facecolor="none", alpha=0.5)
    ax.add_patch(rect)
    ax.annotate("(a)", (lims[0] + 0.04 * ins_bbox[2], lims[1] + 0.53 * ins_bbox[3]), c="black", alpha=0.5)
    ax.xaxis.set_visible(False)
    # ax.indicate_inset_zoom(ins, edgecolor="black")

    ax = axes[1]
    ax.set_xlim(*axes[0].get_xlim())
    ax.set_ylim(-res_max, res_max)
    ax.axhline(0, c="gray", lw=1, ls="--", zorder=1000)
    ax.errorbar(x, y - icaps.fit_erf(x, drift, sigma),
                c="red", fmt="o", markersize=1, alpha=0.7, zorder=1)
    ax.errorbar(x, y - icaps.fit_erf_dg(x, w0, drift01, sigma0, sigma1),
                c="blue", fmt="o", markersize=1, alpha=0.7, zorder=2)
    ax.errorbar(x, y - icaps.fit_erf_exp(x, drift_exp, sigma_exp, w_exp, lamb_exp),
                c="green", fmt="o", markersize=1, alpha=0.7, zorder=0)
    ax.axvline(var_left, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    ax.axvline(var_right, c="black", lw=1, ls="-.", alpha=0.5, zorder=1000)
    ax.set_xlabel(r"$\Delta \theta \, (\mathrm{rad})$")
    ylim = np.max(np.abs(ax.get_ylim()))
    ax.set_ylim(-ylim, ylim)
    twinx = ax.twinx()
    twinx.set_ylim(*ax.get_ylim())
    twinx.minorticks_on()
    twinx.set_ylabel("Residual")
    ax.yaxis.set_ticklabels([])
    ax.minorticks_on()
    fig.subplots_adjust(hspace=.001, wspace=.001)
    fig.subplots_adjust(left=0.11, right=0.85)
    # plt.tight_layout()
    plt.savefig(plot_path + "erf_{}_{}.png".format(axis, particle), dpi=dpi)
    plt.close()

    # ===================================================================================
    # OF TRANS X
    # ===================================================================================
    axis = "x"
    fig, ax = plt.subplots()
    sigma_sqr = df_erf_t.loc[(df_erf_t["axis"] == axis) & (df_erf_t["dt"].isin(dts)), "sigma_diff"].to_numpy() ** 2
    of_used = df_erf_t.loc[(df_erf_t["axis"] == axis) & (df_erf_t["dt"].isin(dts)), "of_used"].to_numpy()
    ax.errorbar(dts[~of_used] * 1e-3, sigma_sqr[~of_used], fmt="o",
                markerfacecolor="none", markeredgecolor="black", markersize=5, label="Dropped Data")
    ax.errorbar(dts[of_used] * 1e-3, sigma_sqr[of_used], fmt="o",
                markerfacecolor="black", markeredgecolor="black", markersize=5, label="Used Data")
    ax.set_xlabel(r"$\Delta t \,\rm (s)$")
    ax.set_ylabel(r"$\langle \Delta x^2 \rangle \, (\mathrm{m}^{\rm 2})$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    xspace = np.linspace(*ax.get_xlim(), 1000)
    mass = df_all[f"mass_{axis}"].to_numpy()[0]
    tau = df_all[f"tau_{axis}"].to_numpy()[0]
    ax.plot(xspace, 10 ** icaps.fit_of_log(xspace, mass, tau), c="red", lw=2)
    xspace_ = xspace[xspace < 5e-3]
    ax.plot(xspace_, icaps.fit_para0(xspace_, 2 * icaps.const.T * icaps.const.k_B / mass),
            c="black", lw=2, ls="-.")
    # ax.text(1.2e-3, 8e-14, "Ballistic Slope", rotation=40.5)
    ax.text(1.5e-3, 1.6e-13, "Ballistic Slope\n" + r"$(\sim \Delta t^2)$", ha="center")
    xspace_ = xspace[xspace > 5e-3]
    ax.plot(xspace_, icaps.fit_lin0(xspace_, 2 * icaps.const.T * icaps.const.k_B * tau / mass),
            c="black", lw=2, ls="--")
    # ax.text(0.9e-2, 4e-12, "Diffusive Slope", rotation=23)
    ax.text(1.2e-2, 6e-12, "Diffusive Slope\n" + r"$(\sim \Delta t)$", ha="center")
    ax.legend(loc="lower right", fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + f"of_{axis}_{particle}.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # OF TRANS Y
    # ===================================================================================
    axis = "y"
    fig, ax = plt.subplots()
    sigma_sqr = df_erf_t.loc[(df_erf_t["axis"] == axis) & (df_erf_t["dt"].isin(dts)), "sigma_diff"].to_numpy() ** 2
    of_used = df_erf_t.loc[(df_erf_t["axis"] == axis) & (df_erf_t["dt"].isin(dts)), "of_used"].to_numpy()
    ax.errorbar(dts[~of_used] * 1e-3, sigma_sqr[~of_used], fmt="o",
                markerfacecolor="none", markeredgecolor="black", markersize=5, label="Dropped Data")
    ax.errorbar(dts[of_used] * 1e-3, sigma_sqr[of_used], fmt="o",
                markerfacecolor="black", markeredgecolor="black", markersize=5, label="Used Data")
    ax.set_xlabel(r"$\Delta t \,\rm (s)$")
    ax.set_ylabel(r"$\langle \Delta y^2 \rangle \, (\mathrm{m}^{\rm 2})$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    xspace = np.linspace(*ax.get_xlim(), 1000)
    mass = df_all[f"mass_{axis}"].to_numpy()[0]
    tau = df_all[f"tau_{axis}"].to_numpy()[0]
    ax.plot(xspace, 10 ** icaps.fit_of_log(xspace, mass, tau), c="red", lw=2)
    xspace_ = xspace[xspace < 5e-3]
    ax.plot(xspace_, icaps.fit_para0(xspace_, 2 * icaps.const.T * icaps.const.k_B / mass),
            c="black", lw=2, ls="-.")
    # ax.text(1.2e-3, 8e-14, "Ballistic Slope", rotation=40.5)
    ax.text(1.5e-3, 1.6e-13, "Ballistic Slope\n" + r"$(\sim \Delta t^2)$", ha="center")
    xspace_ = xspace[xspace > 5e-3]
    ax.plot(xspace_, icaps.fit_lin0(xspace_, 2 * icaps.const.T * icaps.const.k_B * tau / mass),
            c="black", lw=2, ls="--")
    # ax.text(0.9e-2, 4e-12, "Diffusive Slope", rotation=23)
    ax.text(1.2e-2, 6e-12, "Diffusive Slope\n" + r"$(\sim \Delta t)$", ha="center")
    ax.legend(loc="lower right", fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + f"of_{axis}_{particle}.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # OF ROT
    # ===================================================================================
    axis = "rot"
    fig, ax = plt.subplots()
    sigma_sqr = df_erf_r.loc[(df_erf_r["axis"] == axis) & (df_erf_r["dt"].isin(dts)), "sigma_diff"].to_numpy() ** 2
    of_used = df_erf_r.loc[(df_erf_r["axis"] == axis) & (df_erf_r["dt"].isin(dts)), "of_used"].to_numpy()
    ax.errorbar(dts[~of_used] * 1e-3, sigma_sqr[~of_used], fmt="o",
                markerfacecolor="none", markeredgecolor="black", markersize=5, label="Dropped Data")
    ax.errorbar(dts[of_used] * 1e-3, sigma_sqr[of_used], fmt="o",
                markerfacecolor="black", markeredgecolor="black", markersize=5, label="Used Data")
    ax.set_xlabel(r"$\Delta t \,\rm (s)$")
    ax.set_ylabel(r"$\langle \Delta \theta^2 \rangle \, (\mathrm{rad}^{\rm 2})$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    xspace = np.linspace(*ax.get_xlim(), 1000)
    mass = df_all["inertia"].to_numpy()[0]
    tau = df_all[f"tau_{axis}"].to_numpy()[0]
    ax.plot(xspace, 10 ** icaps.fit_of_log(xspace, mass, tau), c="red", lw=2)
    xspace_ = xspace[xspace < 5e-3]
    ax.plot(xspace_, icaps.fit_para0(xspace_, 2 * icaps.const.T * icaps.const.k_B / mass),
            c="black", lw=2, ls="-.")
    # ax.text(1.2e-3, 8e-14, "Ballistic Slope", rotation=40.5)
    ax.text(1.5e-3, 2.7e-3, "Ballistic Slope\n" + r"$(\sim \Delta t^2)$", ha="center")
    xspace_ = xspace[xspace > 5e-3]
    ax.plot(xspace_, icaps.fit_lin0(xspace_, 2 * icaps.const.T * icaps.const.k_B * tau / mass),
            c="black", lw=2, ls="--")
    # ax.text(0.9e-2, 4e-12, "Diffusive Slope", rotation=23)
    ax.text(1.2e-2, 1.3e-1, "Diffusive Slope\n" + r"$(\sim \Delta t)$", ha="center")
    ax.legend(loc="lower right", fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + f"of_{axis}_{particle}.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # FFT
    # ===================================================================================
    xf_all = np.array([])
    yfp_all = np.array([])
    xf_trans = []
    yfp_trans = []
    axes = ["x", "y"]
    roller = "log"
    for axis in axes:
        y = df_disp_t.loc[df_disp_t["particle"] == particle,
                          f"d{axis}_1"].dropna().to_numpy() * icaps.const.px_ldm * 1e-3
        y = np.cumsum(y)
        y = np.append(np.array([0.]), y)
        yf = fft(y)
        n = len(y)
        xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
        yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
        xf_all = np.append(xf_all, xf)
        yfp_all = np.append(yfp_all, yfp)
        xf_trans.append(xf)
        yfp_trans.append(yfp)
        fig, ax = plt.subplots()
        # ax.scatter(xf, yfp, c="black", marker="o", s=2, alpha=0.2)
        ax.scatter(xf, yfp, c="black", marker="o", s=1, alpha=1)
        if roller == "lin":
            movavrg = pd.Series(yfp).rolling(20, center=True).mean().to_numpy()
        else:
            movavrg = 10 ** pd.Series(np.log10(yfp)).rolling(20, center=True).mean().to_numpy()
        # ax.scatter(xf, movavrg, c="black", marker="o", s=2)
        ax.set_ylabel(r"$P_{{{}}} \,\rm (arb. units)$".format(axis))
        ax.set_xlabel(r"$f \,\rm (Hz)$")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(7e-2, 5.5e2)
        ax.set_ylim(3e-13, 3e-2)
        xspace = np.linspace(2e-1, 400, 1000)
        ax.plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
        ax.text(100, 2.4e-8, r"$\sim f^{-2}$", color="red",
                bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
        plt.tight_layout()
        plt.savefig(plot_path + f"fft_{particle:06d}_{axis}.png", dpi=dpi)
        plt.close()
    idx = np.argsort(xf_all)
    xf_all = xf_all[idx]
    yfp_all = yfp_all[idx]
    fig, ax = plt.subplots()
    ax.scatter(xf_all, yfp_all, c="black", marker="o", s=1, alpha=1)
    if roller == "lin":
        movavrg = pd.Series(yfp_all).rolling(20, center=True).mean().to_numpy()
    else:
        movavrg = 10 ** pd.Series(np.log10(yfp_all)).rolling(20, center=True).mean().to_numpy()
    data_trans = [xf_all, yfp_all, movavrg]
    # ax.scatter(xf_all, movavrg, c="black", marker="o", s=2)
    ax.set_ylabel(r"$P_{x,y} \,\rm (arb. units)$")
    ax.set_xlabel(r"$f \,\rm (Hz)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(7e-2, 5.5e2)
    ax.set_ylim(3e-13, 3e-2)
    xspace = np.linspace(2e-1, 400, 1000)
    ax.plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax.text(100, 2.4e-8, r"$\sim f^{-2}$", color="red",
            bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    plt.tight_layout()
    plt.savefig(plot_path + f"fft_{particle:06d}_xy.png", dpi=dpi)
    plt.close()

    y = df_disp_r.loc[df_disp_r["particle"] == particle, "omega_1"].dropna().to_numpy()
    y = np.cumsum(y)
    y = np.append(np.array([0.]), y)
    yf = fft(y)
    n = len(y)
    xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
    yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
    fig, ax = plt.subplots()
    # ax.scatter(xf, yfp, c="black", marker="o", s=2, alpha=0.2)
    ax.scatter(xf, yfp, c="black", marker="o", s=1, alpha=1)
    roller = "log"
    if roller == "lin":
        movavrg = pd.Series(yfp).rolling(20, center=True).mean().to_numpy()
    else:
        movavrg = 10 ** pd.Series(np.log10(yfp)).rolling(20, center=True).mean().to_numpy()
    data_rot = [xf, yfp, movavrg]
    # ax.scatter(xf, movavrg, c="black", marker="o", s=2)
    ax.set_ylabel(r"$P_\theta \,\rm (arb. units)$")
    ax.set_xlabel(r"$f \,\rm (Hz)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(7e-2, 5.5e2)
    ax.set_ylim(3e-9, 3e1)
    xspace = np.linspace(2e-1, 400, 1000)
    ax.plot(xspace, 1e0 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax.text(100, 5e-4, r"$\sim f^{-2}$", color="red",
            bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    plt.tight_layout()
    plt.savefig(plot_path + f"fft_{particle:06d}_rot.png", dpi=dpi)
    plt.close()

    fig, ax = plt.subplots(nrows=2, figsize=(8, 6 * 1.0))
    fig.subplots_adjust(hspace=0.0, wspace=0.0, top=0.95, bottom=0.1)
    # ax[0].errorbar(data_trans[0], data_trans[1], color="black", fmt=".", markersize=2)
    ax[0].errorbar(xf_trans[0], yfp_trans[0], color="black", fmt=".", markersize=2, label="$x$ (black), $y$ (gray)")
    ax[0].errorbar(xf_trans[1], yfp_trans[1], color="gray", fmt=".", markersize=2)
    ax[0].legend(loc="lower left", fancybox=False)
    ax[1].errorbar(data_rot[0], data_rot[1], color="black", fmt=".", markersize=2)
    # ax[0].errorbar(data_trans[0], data_trans[2], color="black", fmt=".", markersize=2)
    # ax[1].errorbar(data_rot[0], data_rot[2], color="black", fmt=".", markersize=2)
    xspace = np.linspace(2e-1, 400, 1000)
    ax[0].set_ylim(3e-13, 3e-2)
    ax[0].plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax[0].text(100, 3.4e-8, r"$\sim f^{-2}$", color="red",
               bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    ax[1].plot(xspace, 1e0 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax[1].text(100, 5e-4, r"$\sim f^{-2}$", color="red",
               bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    ax[1].set_ylim(3e-9, 3e1)
    for i in range(2):
        ax[i].set_xscale("log")
        ax[i].set_yscale("log")
        ax[i].set_xlim(7e-2, 5.5e2)
    ax[0].xaxis.tick_top()
    ax[0].xaxis.set_ticklabels([])
    icaps.mirror_axis(ax[0], "y")
    icaps.mirror_axis(ax[1], "y")
    ax[0].set_ylabel(r"$P_{x,y} \,\rm (arb. units)$")
    ax[1].set_ylabel(r"$P_\theta \,\rm (arb. units)$")
    ax[1].set_xlabel(r"$f \,\rm (Hz)$")
    plt.savefig(plot_path + f"fft_{particle:06d}_both.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()
