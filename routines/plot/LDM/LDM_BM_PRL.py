import icaps
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.optimize import fsolve

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM/"
csv_of_final = project_path + "OF_Final.csv"
csv_erf_t_flagged = project_path + "ERF_Trans_flagged.csv"
csv_erf_r_flagged = project_path + "ERF_Rot_flagged.csv"
plot_path = project_path + "plots/PRL/"
icaps.mk_folders([plot_path], clear=False)

# Filters
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
max_rss_trans = 40e-4
max_rss_rot = 60e-4
max_rel_err_trans_mass = 0.1
max_rel_err_trans_tau = 0.1
max_rel_err_rot_moi = 0.5
max_rel_err_rot_tau = 0.5
ex_col = "max_ex"
ex_err_col = "median_ex_err_median"
gyrad_col = "mean_gyrad"
gyrad_err_col = "mean_gyrad_err"
lh_link = np.mean

# Plots
mass_lims = 2e-15, 1e-11
moi_lims = 5e-27, 1e-21
n_lims = 1e0, 1e3
z_lims = 1e0, 1e7
g_lims = 0.1, 60
n_lims_c = 6e0, 1e3
z_lims_c = 3e1, 1e7
g_lims_c = 1e0, 60
dpi = 600
residuals_right = False
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)
plt.rc("text.latex", preamble=r"\usepackage{amsmath}")


def main():
    # ===================================================================================
    # Needed Tables:
    # - df_t1                        rss_trans, mass_rel_err
    # - df_t2       from df_t1       tau_rel_err
    # - df_r1      from df_t1       rot_notnan, rss_rot, moi_rel_err
    # - df_r2       from df_r1     tau_rel_err, tau_rot_rel_err
    # ===================================================================================
    df_all = pd.read_csv(csv_of_final)
    log = icaps.OutLog().add("all", 0, len(df_all["particle"].unique()), op="none")
    df_t1, log = icaps.brownian.filter_rss(df_all, thresh=max_rss_trans, mode="trans", log=log)
    df_t1, log = icaps.brownian.filter_rel_err(df_t1, mass=max_rel_err_trans_mass, mode="trans", log=log)
    df_t2, log = icaps.brownian.filter_rel_err(df_t1, tau=max_rel_err_trans_tau, mode="trans", log=log)

    df_r1 = df_t1.dropna(subset=["tau_rot"])
    log.add("rot_notnan", 0, len(df_r1["particle"].unique()), op="none")
    df_r1, log = icaps.brownian.filter_rss(df_r1, thresh=max_rss_rot, mode="rot", log=log)
    df_r1, log = icaps.brownian.filter_rel_err(df_r1, mass=max_rel_err_rot_moi, mode="rot", log=log)

    df_r2, log = icaps.brownian.filter_rel_err(df_r1, tau=max_rel_err_trans_tau, mode="trans", log=log)
    df_r2, log = icaps.brownian.filter_rel_err(df_r2, tau=max_rel_err_rot_tau, mode="rot", log=log)
    print(log)
    log.to_csv(plot_path + "info.csv")
    table = {"T1": df_t1, "T2": df_t2, "R1": df_r1, "R2": df_r2, "BM_ALL": df_all}

    # ===================================================================================
    # Calibration | Extinction vs. Mass
    # ===================================================================================
    out_ex0 = None
    out_ex0_s1 = None
    for tab in ["T1", "T2", "R1"]:
        df_ = table[tab]
        fig, ax = plt.subplots(nrows=3, gridspec_kw={"height_ratios": [2, 1, 1]}, figsize=(6.4, 4.8 * 1.2))
        fig.subplots_adjust(hspace=0.)
        ax[2].set_xlabel(r"$m_\mathrm{BM} / m_0$")
        ax[0].set_ylabel(r"$\mu_{\rm opt}$ (counts)")
        ax[1].set_ylabel(r"Residual (dex)")
        ax[1].yaxis.set_label_coords(-0.08, 0)
        if residuals_right:
            ax[1].yaxis.tick_right()
            ax[1].yaxis.set_label_position("right")
            ax[2].yaxis.tick_right()
            ax[2].yaxis.set_label_position("right")
            ax[1].yaxis.set_label_coords(1.1, 0)
        for i in range(3):
            ax[i].set_xscale("log")
            ax[i].set_xlim(4e-1, 4e3)
            ax[i].minorticks_on()
        ax[0].set_yscale("log")
        ax[0].set_ylim(2e2, 3e5)
        ax[1].set_ylim(-0.4, 0.4)
        ax[2].set_ylim(-0.4, 0.4)
        ax[1].set_yticks([-0.4, -0.2, 0, 0.2, 0.4])
        ax[2].set_yticks([-0.4, -0.2, 0, 0.2, 0.4])
        ax[1].yaxis.set_ticklabels(["", -0.2, 0, 0.2, ""])
        ax[2].yaxis.set_ticklabels(["", -0.2, 0, 0.2, ""])
        mass_x_brown = df_["mass_x"].to_numpy()
        mass_x_brown_err = df_["mass_x_err"].to_numpy()
        mass_y_brown = df_["mass_y"].to_numpy()
        mass_y_brown_err = df_["mass_y_err"].to_numpy()
        n_x_brown = mass_x_brown / icaps.const.m_0
        n_x_brown_low = n_x_brown - (mass_x_brown - mass_x_brown_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_x_brown_high = (mass_x_brown + mass_x_brown_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_x_brown
        n_y_brown = mass_y_brown / icaps.const.m_0
        n_y_brown_low = n_y_brown - (mass_y_brown - mass_y_brown_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_y_brown_high = (mass_y_brown + mass_y_brown_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_y_brown

        ex = df_[ex_col].to_numpy()
        ex_err = df_[ex_err_col]
        ax[0].errorbar(n_x_brown, ex, xerr=[n_x_brown_low, n_x_brown_high], yerr=ex_err,
                       fmt="o", c="darkgray", markersize=2)
        ax[0].errorbar(n_x_brown, ex, fmt="o", c="black", markersize=2)
        ax[0].errorbar(n_y_brown, ex, xerr=[n_y_brown_low, n_y_brown_high], yerr=ex_err,
                       fmt="o", c="darkgray", markersize=2)
        ax[0].errorbar(n_y_brown, ex, fmt="o", c="black", markersize=2)
        n_brown = np.append(n_x_brown, n_y_brown)
        n_brown_low = np.append(n_x_brown_low, n_y_brown_low)
        n_brown_high = np.append(n_x_brown_high, n_y_brown_high)
        n_brown_err = lh_link([n_brown_low, n_brown_high], axis=0)
        ex = np.tile(ex, 2)
        ex_err = np.tile(ex_err, 2)

        xspace = np.linspace(*ax[0].get_xlim(), 1000)
        out_s1 = icaps.fit_model(icaps.fit_slope1, np.log10(n_brown), np.log10(ex),
                                 icaps.log_err(n_brown, n_brown_err), icaps.log_err(ex, ex_err),
                                 fit_type=0, p0=[300.])
        label = icaps.get_param_label([
            [r"$\mu_{\rm opt} = \mu'_0 m_\mathrm{BM} / m_0$"],
            [r"$\mu'_0$", 10 ** out_s1.beta[0], 10 ** out_s1.sd_beta[0]]
        ])
        ax[0].plot(xspace, 10 ** icaps.fit_slope1(np.log10(xspace), *out_s1.beta), c="blue", ls="--", lw=2,
                   label=label, alpha=1, zorder=10)

        chisqr = icaps.chisqr(np.log10(ex), icaps.fit_slope1(np.log10(n_brown), *out_s1.beta))
        rss = icaps.rss(np.log10(ex), icaps.fit_slope1(np.log10(n_brown), *out_s1.beta))
        ax[1].annotate(icaps.get_param_label([[r"$\chi^2$", chisqr]]), xy=(0.987, 0.94), xycoords="axes fraction",
                       size=15, ha="right", va="top", bbox=dict(boxstyle="square", fc="w"))
        res = np.log10(ex) - icaps.fit_slope1(np.log10(n_brown), *out_s1.beta)
        ax[1].errorbar(n_brown, res, mfc="black", mec="black", fmt="o", markersize=2)

        out_lin = icaps.fit_model(icaps.fit_lin, np.log10(n_brown), np.log10(ex),
                                  icaps.log_err(n_brown, n_brown_err), icaps.log_err(ex, ex_err),
                                  fit_type=0, p0=[1., 300.])
        label = icaps.get_param_label([
            [r"$\mu_{\rm opt} = \kappa_0 \left(m_\mathrm{BM} / m_0 \right)^{\kappa_1}$"],
            [r"$\kappa_0$", 10 ** out_lin.beta[1], 10 ** out_lin.sd_beta[1]],
            [r"$\kappa_1$", out_lin.beta[0], out_lin.sd_beta[0]]
        ])
        ax[0].plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_lin.beta), c="red", ls="-", lw=2,
                   label=label, alpha=1, zorder=10)
        chisqr = icaps.chisqr(np.log10(ex), icaps.fit_lin(np.log10(n_brown), *out_lin.beta))
        rss = icaps.rss(np.log10(ex), icaps.fit_lin(np.log10(n_brown), *out_lin.beta))
        ax[2].annotate(icaps.get_param_label([[r"$\chi^2$", chisqr]]), xy=(0.987, 0.94), xycoords="axes fraction",
                       size=15, ha="right", va="top", bbox=dict(boxstyle="square", fc="w"))
        res = np.log10(ex) - icaps.fit_lin(np.log10(n_brown), *out_lin.beta)
        ax[2].errorbar(n_brown, res, mfc="black", mec="black", fmt="d", markersize=2)

        if tab == "T1":
            out_ex0 = out_lin
            out_ex0_s1 = out_s1
        ax[0].legend(loc=(0, 1.05), ncol=2, fancybox=False, fontsize=13)
        if residuals_right:
            ax[0].errorbar(6.43e-1, 1.05e6, mfc="black", mec="black", fmt="o", clip_on=False, zorder=1000, markersize=2)
            ax[0].errorbar(4.35e1, 1.05e6, mfc="black", mec="black", fmt="d", clip_on=False, zorder=1000, markersize=2)
        else:
            ax[0].errorbar(6.2e-1, 1.05e6, mfc="black", mec="black", fmt="o", clip_on=False, zorder=1000, markersize=2)
            ax[0].errorbar(2.5e1, 1.05e6, mfc="black", mec="black", fmt="d", clip_on=False, zorder=1000, markersize=2)
        fig.subplots_adjust(bottom=0.1, top=0.85, left=0.105, right=0.885 if residuals_right else 0.99)
        ax[0].xaxis.tick_top()
        ax[0].xaxis.set_ticklabels([])
        ax[1].axhline(0, c="gray", ls="--", lw=2, zorder=1000)
        ax[2].axhline(0, c="gray", ls="--", lw=2, zorder=1000)
        plt.savefig(plot_path + f"Ex0_{tab}.png", dpi=dpi)
        plt.close()

    print("n_opt fit params", out_ex0.beta, out_ex0.sd_beta)

    def adapt_n_opt(ex_, ex_err_=None):
        n_ = (ex_ / (10 ** out_ex0.beta[1])) ** (1 / (out_ex0.beta[0]))
        if ex_err_ is not None:
            n_low_ = n_ - ((ex_ - ex_err_) / (10 ** (out_ex0.beta[1]
                                                     + out_ex0.sd_beta[1]))) ** (1 / (out_ex0.beta[0]
                                                                                      + out_ex0.sd_beta[0]))
            n_high_ = ((ex_ + ex_err_) / (10 ** (out_ex0.beta[1]
                                                 - out_ex0.sd_beta[1]))) ** (1 / (out_ex0.beta[0]
                                                                                  - out_ex0.sd_beta[0])) - n_
            return n_, n_low_, n_high_
        return n_

    # ===================================================================================
    # Calibration | MoI Optical vs. MoI Brownian
    # ===================================================================================
    for plot_half in (False, True):
        df_ = table["R1"]
        if plot_half:
            nrows = 3
            fig, ax = plt.subplots(nrows=nrows, gridspec_kw={"height_ratios": [2, 1, 1]}, figsize=(6.4, 4.8 * 1.25))
        else:
            nrows = 2
            fig, ax = plt.subplots(nrows=nrows, gridspec_kw={"height_ratios": [2, 1]}, figsize=(6.4, 4.8 * 1.1))
        fig.subplots_adjust(hspace=0.)
        fig.subplots_adjust(bottom=0.124, top=0.82, left=0.13, right=0.875 if residuals_right else 0.99)
        ax[nrows - 1].set_xlabel(r"$I_\mathrm{BM} / I_0$")
        ax[0].set_ylabel(r"$i_\mathrm{opt}\, (\mathrm{counts}\cdot \mathrm{m}^2)$")
        for i in range(nrows):
            ax[i].set_xscale("log")
            ax[i].set_xlim(7e1, 7e5)
            ax[i].minorticks_on()
        ax[0].set_yscale("log")
        if residuals_right:
            ax[1].yaxis.tick_right()
            ax[1].yaxis.set_label_position("right")
        else:
            ax[1].set_yticks([-0.4, 0, 0.4])
        ax[1].set_ylabel("Residual (dex)")
        if plot_half:
            ax[2].yaxis.tick_right()
            ax[1].yaxis.set_label_coords(1.12, 0)
            ax[0].errorbar(1.15e2, 7.05e-4, mfc="black", mec="black", fmt="o", clip_on=False, zorder=1000, markersize=2)
            ax[0].errorbar(1.45e4, 7.05e-4, mfc="black", mec="black", fmt="d", clip_on=False, zorder=1000, markersize=2)
            ax[2].set_ylim(-0.6, 0.6)
            ax[2].set_yticks([-0.4, 0, 0.4])
            ax[1].set_yticks([-0.4, 0, 0.4])
        ax[0].xaxis.tick_top()
        ax[0].xaxis.set_ticklabels([])
        ax[1].set_ylim(-0.6, 0.6)
        ax[0].set_ylim(1e-8, 1e-4)
        ex = df_[ex_col].to_numpy()
        ex_err = df_[ex_err_col].to_numpy()
        gyrad = df_[gyrad_col].to_numpy() * icaps.const.px_ldm / 1e6
        gyrad_err = df_[gyrad_err_col].to_numpy() * icaps.const.px_ldm / 1e6
        i_opt = ex * gyrad ** 2
        i_opt_err = i_opt * np.sqrt((ex_err / ex) ** 2 + (2 * gyrad_err / gyrad) ** 2)

        moi_brown = df_["inertia"].to_numpy()
        moi_brown_err = df_["inertia_err"].to_numpy()
        z_brown = moi_brown / icaps.const.moi_0
        z_brown_low = z_brown - (moi_brown - moi_brown_err) / (icaps.const.moi_0 + icaps.const.moi_0_err)
        z_brown_high = (moi_brown + moi_brown_err) / (icaps.const.moi_0 - icaps.const.moi_0_err) - z_brown
        z_brown_err = lh_link([z_brown_low, z_brown_high], axis=0)
        ax[0].errorbar(z_brown, i_opt, xerr=[z_brown_low, z_brown_high], yerr=i_opt_err,
                       fmt="o", c="darkgray", markersize=3)
        ax[0].errorbar(z_brown, i_opt, fmt="o", c="black", markersize=3)

        # ax[0].set_ylim(1e-25, 1e-21)
        # ax[0].set_xlim(*ax.get_ylim())
        xspace = np.linspace(*ax[0].get_xlim(), 1000)
        # xlogpspace = np.logspace(-25, -21, 1000)

        out_half = icaps.fit_model(icaps.fit_moi_log_half, z_brown, np.log10(i_opt), fit_type=0,
                                   xerr=z_brown_err, yerr=icaps.log_err(i_opt, i_opt_err),
                                   p0=[5e-11, 5])
        if plot_half:
            l_align = r"\begin{align*} " \
                      + r"\lambda_0 = \,& ({{{}}} \pm {{{}}})\cdot 10^{{-11}}".format(icaps.val2str(out_half.beta[0] * 1e11, 2),
                                                                                      icaps.val2str(out_half.sd_beta[0] * 1e11,
                                                                                                    2)) \
                      + r"\\[-5pt]& \mathrm{{counts\cdot m^2}}" \
                      + r"\end{align*}"
            label = icaps.get_param_label([
                [l_align],
                # [r"$\lambda_0$", out_half.beta[0] * 1e11, out_half.sd_beta[0] * 1e11,
                #  r"$\cdot\, 10^{-11}\\ \mathrm{counts}\cdot\mathrm{m}^2$"],
                [r"$\lambda_1$", out_half.beta[1], out_half.sd_beta[1]],
                [r"$\lambda_2 \equiv 1/2$"]
            ])
            ax[0].plot(xspace, 10 ** icaps.fit_moi_log_half(xspace, *out_half.beta), c="blue", ls="--", lw=2, label=label)
            res = np.log10(i_opt) - icaps.fit_moi_log_half(z_brown, *out_half.beta)
            ax[1].errorbar(z_brown, res, fmt="o", c="black", markersize=2)
            ax[1].axhline(0, lw=2, ls="--", c="gray", zorder=-1)
            chisqr = icaps.chisqr(np.log10(i_opt), icaps.fit_moi_log_half(np.log10(z_brown), *out_half.beta))
            ax[1].annotate(icaps.get_param_label([[r"$\chi^2$", chisqr]]), xy=(0.987, 0.94), xycoords="axes fraction",
                           size=15, ha="right", va="top", bbox=dict(boxstyle="square", fc="w"))

        out_log = icaps.fit_model(icaps.fit_moi_log, z_brown, np.log10(i_opt), fit_type=0,
                                  xerr=z_brown_err, yerr=icaps.log_err(i_opt, i_opt_err),
                                  p0=[5e-11, 5, 0.25])
        label = []
        if plot_half:
            l_align = r"\begin{align*} "\
                      + r"\lambda_0 = \,& ({{{}}} \pm {{{}}})\cdot 10^{{-11}}".format(icaps.val2str(out_log.beta[0] * 1e11, 2),
                                                                                      icaps.val2str(out_log.sd_beta[0] * 1e11,
                                                                                                    2))\
                      + r"\\[-5pt]& \mathrm{{counts\cdot m^2}}"\
                      + r"\end{align*}"
            label.append([l_align])
        else:
            label.append([r"$\lambda_0$", out_log.beta[0] * 1e11, out_log.sd_beta[0] * 1e11,
                          r"$\cdot\, 10^{-11} \mathrm{counts}\cdot\mathrm{m}^2$"])
        label.append([r"$\lambda_1$", out_log.beta[1], out_log.sd_beta[1]])
        label.append([r"$\lambda_2$", out_log.beta[2], out_log.sd_beta[2]])
        label = icaps.get_param_label(label)
        ax[0].plot(xspace, 10 ** icaps.fit_moi_log(xspace, *out_log.beta), c="red", ls="-", lw=2, label=label)
        res = np.log10(i_opt) - icaps.fit_moi_log(z_brown, *out_log.beta)
        ax[nrows - 1].errorbar(z_brown, res, fmt="d" if plot_half else "o", c="black", markersize=2)
        ax[nrows - 1].axhline(0, lw=2, ls="--", c="gray", zorder=-1)
        if plot_half:
            chisqr = icaps.chisqr(np.log10(i_opt), icaps.fit_moi_log(np.log10(z_brown), *out_log.beta))
            ax[nrows - 1].annotate(icaps.get_param_label([[r"$\chi^2$", chisqr]]), xy=(0.987, 0.94),
                                   xycoords="axes fraction", size=15, ha="right", va="top",
                                   bbox=dict(boxstyle="square", fc="w"))

        ax[0].plot(xspace, xspace * icaps.const.moi_0 * 10 ** out_ex0_s1.beta[0] / icaps.const.m_0,
                   c="gray", ls="-.", lw=2, zorder=-1,
                   label=r"$\frac{I}{I_0} \cdot \frac{I_0 \mu'_0}{m_0}$"
                         # + "\n" + r"$(\mu'_0 \approx {{{}}}$".format(
                         # round(10 ** out_ex0_s1.beta[0])) + r"$\, \mathrm{counts})$"
                   )
        handles, labels = ax[0].get_legend_handles_labels()
        leg1 = ax[0].legend(handles=handles[:2], labels=labels[:2],
                            loc=(0, 1.05), ncol=2, fancybox=False, fontsize=13, columnspacing=0.5)
        if plot_half:
            ax[0].legend(handles=[handles[2]], labels=[labels[2]], loc="lower right", fancybox=False, fontsize=13)
        ax[0].add_artist(leg1)
        # icaps.mirror_axis(ax)
        plt.savefig(plot_path + "MoI_Decline_R1{}.png".format("_half" if plot_half else ""), dpi=dpi)
        plt.close()

    def adapt_z_opt(i_opt_, ret_fails_=False):
        import warnings
        result = np.zeros_like(i_opt_)
        fails_ = np.zeros_like(i_opt_).astype(bool)
        for j, i_ in enumerate(i_opt_):
            x0 = (np.sqrt(i_ / out_half.beta[0]) - out_half.beta[1]) ** 2
            with warnings.catch_warnings(record=True) as w:
                result[j] = fsolve(lambda q: 10 ** icaps.fit_moi_log(q, *out_log.beta) - i_, x0=x0)
                if len(w) > 0:
                    fails_[j] = True
            if fails_[j]:
                fails_[j] = False
                x0 = i_ * icaps.const.m_0 / (10 ** out_ex0_s1.beta[0] * icaps.const.moi_0)
                with warnings.catch_warnings(record=True) as w:
                    result[j] = fsolve(lambda q: 10 ** icaps.fit_moi_log(q, *out_log.beta) - i_, x0=x0)
                    if len(w) > 0:
                        fails_[j] = True
        if ret_fails_:
            return result, fails_
        return result

    # ===================================================================================
    # Calibration | Gyrad Optical vs. Gyrad Brownian
    # ===================================================================================
    df_ = table[tab]
    fig, ax = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]}, figsize=(6.4, 4.8 * 1.1))
    ax[1].set_xlabel(r"$r_\mathrm{g, BM} \,(\mathrm{m})$")
    ax[0].set_ylabel(r"$r_\mathrm{g, opt} \,(\mathrm{m})$")
    for i in range(2):
        ax[i].set_xscale("log")
        ax[i].set_xlim(1e-6, 3e-5)
        ax[i].minorticks_on()
    ax[0].set_ylim(1e-6, 3e-5)
    ax[0].set_yscale("log")
    ax[0].xaxis.tick_top()
    ax[0].xaxis.set_ticklabels([])
    if residuals_right:
        ax[1].yaxis.tick_right()
        ax[1].yaxis.set_label_position("right")
    fig.subplots_adjust(hspace=0.)
    fig.subplots_adjust(bottom=0.124, top=0.82, left=0.13, right=0.875 if residuals_right else 0.99)
    mass_x = df_["mass_x"].to_numpy()
    mass_x_err = df_["mass_x_err"].to_numpy()
    mass_y = df_["mass_y"].to_numpy()
    mass_y_err = df_["mass_y_err"].to_numpy()
    moi_brown = df_["inertia"].to_numpy()
    moi_brown_err = df_["inertia_err"].to_numpy()
    gyrad_x = np.sqrt(moi_brown / mass_x)
    gyrad_x_low = gyrad_x - np.sqrt((moi_brown - moi_brown_err) / (mass_x + mass_x_err))
    gyrad_x_high = np.sqrt((moi_brown + moi_brown_err) / (mass_x - mass_x_err)) - gyrad_x
    gyrad_y = np.sqrt(moi_brown / mass_y)
    gyrad_y_low = gyrad_y - np.sqrt((moi_brown - moi_brown_err) / (mass_y + mass_y_err))
    gyrad_y_high = np.sqrt((moi_brown + moi_brown_err) / (mass_y - mass_y_err)) - gyrad_y
    gyrad_opt = df_[gyrad_col].to_numpy() * icaps.const.px_ldm / 1e6
    gyrad_opt_err = df_[gyrad_err_col].to_numpy() * icaps.const.px_ldm / 1e6
    ax[0].errorbar(gyrad_x, gyrad_opt, xerr=[gyrad_x_low, gyrad_x_high], yerr=gyrad_opt_err,
                   c="darkgray", fmt="o", markersize=2)
    ax[0].errorbar(gyrad_x, gyrad_opt, c="black", fmt="o", markersize=2)
    ax[0].errorbar(gyrad_y, gyrad_opt, xerr=[gyrad_y_low, gyrad_y_high], yerr=gyrad_opt_err,
                   c="darkgray", fmt="o", markersize=2)
    ax[0].errorbar(gyrad_y, gyrad_opt, c="black", fmt="o", markersize=2)
    gyrad_brown = np.append(gyrad_x, gyrad_y)
    gyrad_brown_low = np.append(gyrad_x_low, gyrad_y_low)
    gyrad_brown_high = np.append(gyrad_x_high, gyrad_y_high)
    gyrad_brown_err = lh_link([gyrad_brown_low, gyrad_brown_high], axis=0)
    gyrad_opt2 = np.tile(gyrad_opt, 2)
    gyrad_opt2_err = np.tile(gyrad_opt_err, 2)
    xspace = np.linspace(*ax[0].get_xlim(), 1000)
    out_gyr = icaps.fit_model(icaps.fit_slope1, gyrad_brown, gyrad_opt2, xerr=gyrad_brown_err, yerr=gyrad_opt2_err)
    label = icaps.get_param_label([
        [r"$r_\mathrm{g,opt} = r_\mathrm{g,BM} + \delta r_\mathrm{g}$"],
        [r"$\delta r_\mathrm{g}$", out_gyr.beta[0] * 1e6, out_gyr.sd_beta[0] * 1e6, r"$\cdot 10^{-6} \,\mathrm{m}$"]
    ])
    ax[0].plot(xspace, icaps.fit_slope1(xspace, *out_gyr.beta), c="red", ls="-", lw=2, label=label, zorder=1000)
    res = np.log10(gyrad_opt2) - np.log10(icaps.fit_slope1(gyrad_brown, *out_gyr.beta))
    ax[1].errorbar(gyrad_brown, res, c="black", fmt="o", markersize=2)
    # ax[1].set_ylim(np.array([-1, 1]) * np.repeat(np.max(np.abs(ax[1].get_ylim())), 2))
    ax[1].axhline(0, c="gray", zorder=-1, lw=2, ls="--")
    ax[1].set_ylabel("Residual (dex)")
    ax[1].set_yticks([-0.2, 0, 0.2])
    ax[1].set_ylim(-0.28, 0.28)

    out_gyr_pow = icaps.fit_model(icaps.fit_lin, np.log10(gyrad_brown), np.log10(gyrad_opt2),
                                  xerr=icaps.log_err(gyrad_brown, gyrad_brown_err),
                                  yerr=icaps.log_err(gyrad_opt2, gyrad_opt2_err))
    # label = icaps.get_param_label([
    #     [r"$r_\mathrm{g,opt} = a * r_\mathrm{g,BM}^b$"],
    #     [r"$a$", 10 ** out_gyr_pow.beta[1], 10 ** out_gyr_pow.sd_beta[1]],
    #     [r"$b$", out_gyr_pow.beta[0], out_gyr_pow.sd_beta[0]]
    # ])
    # ax[0].plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_gyr_pow.beta),
    #            c="blue", ls="--", lw=2, label=label, zorder=1000)

    ax[0].plot(xspace, xspace, c="gray", ls="-.", lw=2, zorder=-1, label=r"$r_\mathrm{g, opt} = r_\mathrm{g, BM}$")
    # ax[0].legend(fancybox=False, loc="lower right", fontsize=13)
    # icaps.mirror_axis(ax)
    ax[0].legend(loc=(0, 1.05), ncol=2, fancybox=False, fontsize=13, columnspacing=0.5)
    plt.savefig(plot_path + f"Gyrad_BM_vs_Opt_{tab}.png", dpi=dpi)
    plt.close()

    def adapt_gyrad_opt(gyrad_opt_, gyrad_opt_err_):
        gyrad_ = gyrad_opt_ - out_gyr.beta[0]
        gyrad_err_ = np.sqrt(gyrad_opt_err_**2 + out_gyr.sd_beta[0]**2)
        return gyrad_, gyrad_err_

    def adapt_gyrad_opt_pow(gyrad_opt_, gyrad_opt_err_):
        gyrad_ = (gyrad_opt_ / (10 ** out_gyr_pow.beta[1]))**(1/out_gyr_pow.beta[0])
        gyrad_low_ = ((gyrad_opt_ - gyrad_opt_err_) / (10 ** (
                out_gyr_pow.beta[1] + out_gyr_pow.sd_beta[1])))**(1/(out_gyr_pow.beta[0] + out_gyr_pow.sd_beta[0]))
        gyrad_high_ = ((gyrad_opt_ + gyrad_opt_err_) / (10 ** (
                out_gyr_pow.beta[1] - out_gyr_pow.sd_beta[1])))**(1/(out_gyr_pow.beta[0] - out_gyr_pow.sd_beta[0]))
        return gyrad_, gyrad_low_, gyrad_high_

    # """
    # ===================================================================================
    # Brownian | Normalized MoI vs. Mass
    # ===================================================================================
    for tab in ["R1"]:
        df_ = table[tab]
        _, ax = plt.subplots()
        ax.set_xlabel(r"$m_\mathrm{BM} / m_0$")
        ax.set_ylabel(r"$I_\mathrm{BM} / I_0$")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(*n_lims_c)
        ax.set_ylim(*z_lims_c)

        mass_x = df_["mass_x"].to_numpy()
        mass_x_err = df_["mass_x_err"].to_numpy()
        mass_y = df_["mass_y"].to_numpy()
        mass_y_err = df_["mass_y_err"].to_numpy()
        n_x = mass_x / icaps.const.m_0
        n_x_low = n_x - (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_x
        n_y = mass_y / icaps.const.m_0
        n_y_low = n_y - (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_y
        n_brown = np.append(n_x, n_y)
        n_brown_err = np.append(lh_link([n_x_low, n_x_high], axis=0), lh_link([n_y_low, n_y_high], axis=0))
        moi_brown = df_["inertia"].to_numpy()
        moi_brown_err = df_["inertia_err"].to_numpy()
        z_brown = moi_brown / icaps.const.moi_0
        z_brown_low = z_brown - (moi_brown - moi_brown_err) / (icaps.const.moi_0 + icaps.const.moi_0_err)
        z_brown_high = (moi_brown + moi_brown_err) / (icaps.const.moi_0 - icaps.const.moi_0_err) - z_brown
        ax.errorbar(n_x, z_brown,
                    xerr=np.array([n_x_low, n_x_high], dtype=float),
                    yerr=np.array([z_brown_low, z_brown_high], dtype=float),
                    fmt="o", c="darkgray", markersize=3)
        ax.errorbar(n_x, z_brown,
                    fmt="o", c="black", markersize=3)
        ax.errorbar(n_y, z_brown,
                    xerr=np.array([n_y_low, n_y_high], dtype=float),
                    yerr=np.array([z_brown_low, z_brown_high], dtype=float),
                    fmt="o", c="darkgray", markersize=3)
        ax.errorbar(n_y, z_brown,
                    fmt="o", c="black", markersize=3)
        xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
        z_brown2 = np.tile(z_brown, 2)
        z_brown2_low = z_brown2 - np.tile(-1 * (z_brown_low - z_brown), 2)
        z_brown2_high = np.tile(z_brown_high + z_brown, 2) - z_brown2
        z_brown2_err = lh_link([z_brown2_low, z_brown2_high], axis=0)
        out_brown = icaps.fit_model(icaps.fit_lin, np.log10(n_brown), np.log10(z_brown2),
                                    xerr=icaps.log_err(n_brown, n_brown_err),
                                    yerr=icaps.log_err(z_brown2, z_brown2_err),
                                    fit_type=0)
        alpha = out_brown.beta[0]
        alpha_err = out_brown.sd_beta[0]
        fracdim = 2 / (alpha - 1)
        fracdim_low = fracdim - 2 / (alpha + alpha_err - 1)
        fracdim_high = 2 / (alpha - alpha_err - 1) - fracdim
        gamma = 10 ** out_brown.beta[1]
        gamma_err = gamma * np.log(10) * out_brown.sd_beta[1]
        # gamma_low = gamma - 10 ** (out_brown.beta[1] - out_brown.sd_beta[1])
        # gamma_high = 10 ** (out_brown.beta[1] + out_brown.sd_beta[1]) - gamma
        beta = (gamma * 2/5) ** (1 / (1 - alpha))
        # beta_low = beta * np.sqrt((gamma_low/gamma * 1/(1 - alpha))**2
        #                           + (alpha_err * np.log(2/5 * gamma) / ((alpha - 1)**2))**2)
        # beta_high = beta * np.sqrt((gamma_high / gamma * 1 / (1 - alpha)) ** 2
        #                            + (alpha_err * np.log(2 / 5 * gamma) / ((alpha - 1) ** 2)) ** 2)
        beta_err = beta * np.sqrt((gamma_err/gamma * 1/(1 - alpha))**2
                                  + (alpha_err * np.log(2/5 * gamma) / ((alpha - 1)**2))**2)
        label = icaps.get_param_label([[r"$I_\mathrm{BM}/I_0 = \gamma \left(m_\mathrm{BM}/m_0\right)^\alpha$"],
                                       [r"$\gamma$", gamma, gamma_err],
                                       # [r"$\gamma$", gamma, [-gamma_low, gamma_high]],
                                       [r"$\alpha$", alpha, alpha_err],
                                       [r"$\beta$", beta, beta_err],
                                       # [r"$\beta$", beta, [-beta_low, beta_high]],
                                       [r"$d_\mathrm{f}$", fracdim, [-fracdim_low, fracdim_high]]])
        ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown.beta),
                c="red", lw=2, zorder=10, alpha=1, label=label)
        ax.legend(loc="upper left", fancybox=False)
        ax.minorticks_on()
        icaps.mirror_axis(ax)
        plt.tight_layout()
        plt.savefig(plot_path + f"Moi_vs_Mass_Brownian_{tab}.png", dpi=dpi)
        plt.close()

    # ===================================================================================
    # Optical | Normalized MoI vs. Mass
    # ===================================================================================
    ex0 = 10 ** out_ex0_s1.beta[0]
    # ex0_err = 10 ** out_ex0_s1.sd_beta[0]
    ex0_err = ex0 * np.log(10) * out_ex0_s1.sd_beta[0]
    for tab in ["R1", "T1", "BM_ALL"]:
        df_ = table[tab]
        for adapt in ["none", "moi", "gyrad"]:
            _, ax = plt.subplots()
            ax.set_xlabel(r"$m_\mathrm{opt} / m_0$")
            ax.set_ylabel(r"$I_\mathrm{opt} / I_0$")
            ax.set_xscale("log")
            ax.set_yscale("log")
            ax.minorticks_on()
            ax.set_xlim(*n_lims_c)
            ax.set_ylim(*z_lims_c)

            ex = df_[ex_col].to_numpy()
            ex_err = df_[ex_err_col].to_numpy()
            gyrad = df_[gyrad_col].to_numpy() * icaps.const.px_ldm / 1e6
            gyrad_err = df_[gyrad_err_col].to_numpy() * icaps.const.px_ldm / 1e6
            if adapt in ("moi", "gyrad"):
                n_opt, n_opt_low, n_opt_high = adapt_n_opt(ex, ex_err)
            else:
                n_opt = ex / ex0
                n_opt_low = n_opt - (ex - ex_err) / (ex0 + ex0_err)
                n_opt_high = (ex + ex_err) / (ex0 - ex0_err) - n_opt

            fails = np.repeat(False, len(ex))
            if adapt == "gyrad":
                gyrad_real, gyrad_real_err = adapt_gyrad_opt(gyrad, gyrad_err)
                z_opt = icaps.const.m_0 * n_opt * gyrad_real ** 2 / icaps.const.moi_0
                z_opt_low = z_opt -\
                            (icaps.const.m_0 - icaps.const.m_0_err) *\
                            (n_opt - n_opt_low) * (gyrad_real - gyrad_real_err)**2 /\
                            (icaps.const.moi_0 + icaps.const.moi_0_err)
                z_opt_high = (icaps.const.m_0 + icaps.const.m_0_err) *\
                             (n_opt + n_opt_high) * (gyrad_real + gyrad_real_err)**2 /\
                             (icaps.const.moi_0 - icaps.const.moi_0_err) - z_opt
            else:
                i_opt = ex * gyrad ** 2
                i_opt_err = i_opt * np.sqrt((ex_err / ex) ** 2 + (2 * gyrad_err / gyrad) ** 2)
                if adapt == "moi":
                    z_opt, fails = adapt_z_opt(i_opt, ret_fails_=True)
                    z_opt_low = z_opt - adapt_z_opt(i_opt - i_opt_err)
                    z_opt_high = adapt_z_opt(i_opt + i_opt_err) - z_opt
                else:
                    z_opt = (i_opt / ex0 * icaps.const.m_0) / icaps.const.moi_0
                    z_opt_low = z_opt - ((i_opt - i_opt_err) /
                                         (ex0 + ex0_err) *
                                         (icaps.const.m_0 - icaps.const.m_0_err)) / (icaps.const.moi_0 +
                                                                                     icaps.const.moi_0_err)
                    z_opt_high = ((i_opt + i_opt_err) / (ex0 - ex0_err) *
                                  (icaps.const.m_0 + icaps.const.m_0_err)) / (icaps.const.moi_0 -
                                                                              icaps.const.moi_0_err) - z_opt

            # mass_opt = mass_opt[moi_opt > 0]  # todo: This is a hotfix.
            # mass_opt_low = mass_opt_low[moi_opt > 0]
            # mass_opt_high = mass_opt_high[moi_opt > 0]
            # moi_opt_low = moi_opt_low[moi_opt > 0]
            # moi_opt_high = moi_opt_high[moi_opt > 0]
            # moi_opt = moi_opt[moi_opt > 0]

            # z_opt_bound = 1e-24 / icaps.const.moi_0
            # idx = (~fails) & (z_opt > z_opt_bound)
            idx = (~fails)

            if tab == "R1":
                ax.errorbar(n_opt[idx], z_opt[idx],
                            xerr=[n_opt_low[idx], n_opt_high[idx]],
                            yerr=[z_opt_low[idx], z_opt_high[idx]],
                            fmt="o", c="gray", markersize=3, alpha=1)
            ax.errorbar(n_opt[idx], z_opt[idx], fmt="o", c="black", markersize=3)
            # ax.errorbar(n_opt[~idx], z_opt[~idx], fmt="o", mfc="none", mec="black", markersize=3)

            xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
            n_opt_err = lh_link([n_opt_low, n_opt_high], axis=0)
            z_opt_err = lh_link([z_opt_low, z_opt_high], axis=0)
            out_opt = icaps.fit_model(icaps.fit_lin, np.log10(n_opt[idx]), np.log10(z_opt[idx]),
                                      xerr=icaps.log_err(n_opt[idx], n_opt_err[idx]),
                                      yerr=icaps.log_err(z_opt[idx], z_opt_err[idx]),
                                      fit_type=0)
            # fracdim = 2 / (out_opt.beta[0] - 1)
            # fracdim_low = 2 / (out_opt.beta[0] + out_opt.sd_beta[0] - 1)
            # fracdim_high = 2 / (out_opt.beta[0] - out_opt.sd_beta[0] - 1)
            alpha = out_opt.beta[0]
            alpha_err = out_opt.sd_beta[0]
            fracdim = 2 / (alpha - 1)
            fracdim_low = fracdim - 2 / (alpha + alpha_err - 1)
            fracdim_high = 2 / (alpha - alpha_err - 1) - fracdim
            gamma = 10 ** out_opt.beta[1]
            gamma_err = gamma * np.log(10) * out_opt.sd_beta[1]
            # gamma_low = gamma - 10 ** (out_brown.beta[1] - out_brown.sd_beta[1])
            # gamma_high = 10 ** (out_brown.beta[1] + out_brown.sd_beta[1]) - gamma
            beta = (gamma * 2 / 5) ** (1 / (1 - alpha))
            # beta_low = beta * np.sqrt((gamma_low / gamma * 1 / (1 - alpha)) ** 2
            #                           + (alpha_err * np.log(2 / 5 * gamma) / ((alpha - 1) ** 2)) ** 2)
            # beta_high = beta * np.sqrt((gamma_high / gamma * 1 / (1 - alpha)) ** 2
            #                            + (alpha_err * np.log(2 / 5 * gamma) / ((alpha - 1) ** 2)) ** 2)
            beta_err = beta * np.sqrt((gamma_err/gamma * 1/(1 - alpha))**2
                                      + (alpha_err * np.log(2/5 * gamma) / ((alpha - 1)**2))**2)
            label = icaps.get_param_label([[r"$I_\mathrm{opt}/I_0 = \gamma \left(m_\mathrm{opt}/m_0\right)^\alpha$"],
                                           [r"$\gamma$", gamma, gamma_err],
                                           # [r"$\gamma$", gamma, [-gamma_low, gamma_high]],
                                           [r"$\alpha$", alpha, alpha_err],
                                           [r"$\beta$", beta, beta_err],
                                           # [r"$\beta$", beta, [-beta_low, beta_high]],
                                           [r"$d_\mathrm{f}$", fracdim, [-fracdim_low, fracdim_high]]])
            ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt.beta),
                    c="red", lw=2, zorder=10, alpha=1, label=label)
            ax.legend(fancybox=False, loc="upper left")
            icaps.mirror_axis(ax)
            plt.tight_layout()
            plt.savefig(plot_path + "Moi_vs_Mass_Optical_adapt_{}_{}.png".format(adapt, tab), dpi=dpi)
            plt.close()

    # ===================================================================================
    # Brownian | m²/I vs tau_t
    # ===================================================================================
    for tab in ["R1"]:
        df_ = table[tab]
        fig, ax = plt.subplots()
        ax.set_xlabel(r"$m_\mathrm{BM}^2 / I_\mathrm{BM}\, (\mathrm{kgm^{-2}})$")
        ax.set_ylabel(r"$\tau_\mathrm{t}\, (\mathrm{s})$")
        ax.set_xscale("log")
        ax.set_yscale("log")
        mass_x = df_["mass_x"].to_numpy()
        mass_x_err = df_["mass_x_err"].to_numpy()
        mass_y = df_["mass_y"].to_numpy()
        mass_y_err = df_["mass_y_err"].to_numpy()
        mass_brown = np.append(mass_x, mass_y)
        mass_brown_err = np.append(mass_x_err, mass_y_err)
        moi_brown = df_["inertia"].to_numpy()
        moi_brown_err = df_["inertia_err"].to_numpy()
        moi_brown2 = np.tile(moi_brown, 2)
        moi_brown2_err = np.tile(moi_brown_err, 2)
        tau_x = df_["tau_x"].to_numpy()
        tau_x_err = df_["tau_x_err"].to_numpy()
        tau_y = df_["tau_y"].to_numpy()
        tau_y_err = df_["tau_y_err"].to_numpy()
        tau_t = np.append(tau_x, tau_y)
        tau_t_err = np.append(tau_x_err, tau_y_err)
        ax.errorbar(mass_x**2 / moi_brown, tau_x,
                    xerr=[mass_x**2 / moi_brown - (mass_x - mass_x_err)**2 / (moi_brown + moi_brown_err),
                          (mass_x + mass_x_err)**2 / (moi_brown - moi_brown_err) - mass_x**2 / moi_brown],
                    yerr=tau_x_err, c="darkgray", fmt="o", markersize=2)
        ax.errorbar(mass_x**2 / moi_brown, tau_x, c="black", fmt="o", markersize=2)
        ax.errorbar(mass_y**2 / moi_brown, tau_y,
                    xerr=[mass_y**2 / moi_brown - (mass_y - mass_y_err)**2 / (moi_brown + moi_brown_err),
                          (mass_y + mass_y_err)**2 / (moi_brown - moi_brown_err) - mass_y**2 / moi_brown],
                    yerr=tau_y_err, c="darkgray", fmt="o", markersize=2)
        ax.errorbar(mass_y**2 / moi_brown, tau_y, c="black", fmt="o", markersize=2)
        icaps.mirror_axis(ax)
        plt.tight_layout()
        plt.savefig(plot_path + "Tau_Relation_Brownian_{}.png".format(tab), dpi=dpi)
        plt.close()

        fig, ax = plt.subplots()
        norm = np.linspace(1 / len(tau_t), 1, len(tau_t))
        ax.errorbar(np.sort(mass_brown**2 / moi_brown2), norm, c="gray", fmt="o", markersize=2,
                    label=r"$m_\mathrm{BM}^2 / I_\mathrm{BM}$")
        ax.errorbar(np.sort(tau_t), norm, c="black", fmt="o", markersize=2,
                    label=r"$\tau_\mathrm{t}$")
        ax.legend()
        plt.tight_layout()
        plt.savefig(plot_path + "Tau_Relation_Cumu_Brownian_{}.png".format(tab), dpi=dpi)
        plt.close()

    # ===================================================================================
    # Brownian | r_g/r_0 vs. m/m_0
    # ===================================================================================
    for tab in ["R1"]:
        df_ = table[tab]
        _, ax = plt.subplots()
        ax.set_xlabel(r"$r_\mathrm{g,BM} / r_0$")
        ax.set_ylabel(r"$m_\mathrm{BM} / m_0$")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(*g_lims_c)
        ax.set_ylim(*n_lims_c)

        mass_x = df_["mass_x"].to_numpy()
        mass_x_err = df_["mass_x_err"].to_numpy()
        mass_y = df_["mass_y"].to_numpy()
        mass_y_err = df_["mass_y_err"].to_numpy()
        n_x = mass_x / icaps.const.m_0
        n_x_low = n_x - (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_x
        n_y = mass_y / icaps.const.m_0
        n_y_low = n_y - (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_y
        n_brown2 = np.append(n_x, n_y)
        n_brown2_err = np.append(lh_link([n_x_low, n_x_high], axis=0), lh_link([n_y_low, n_y_high], axis=0))
        moi_brown = df_["inertia"].to_numpy()
        moi_brown_err = df_["inertia_err"].to_numpy()
        r_x = np.sqrt(moi_brown / mass_x) / icaps.const.r_0
        r_x_low = r_x - np.sqrt((moi_brown - moi_brown_err) / (mass_x + mass_x_err)) / (
                icaps.const.r_0 + icaps.const.r_0_err)
        r_x_high = np.sqrt((moi_brown + moi_brown_err) / (mass_x - mass_x_err)) / (
                icaps.const.r_0 - icaps.const.r_0_err) - r_x
        r_y = np.sqrt(moi_brown / mass_y) / icaps.const.r_0
        r_y_low = r_y - np.sqrt((moi_brown - moi_brown_err) / (mass_y + mass_y_err)) / (
                    icaps.const.r_0 + icaps.const.r_0_err)
        r_y_high = np.sqrt((moi_brown + moi_brown_err) / (mass_y - mass_y_err)) / (
                    icaps.const.r_0 - icaps.const.r_0_err) - r_y
        r_brown2 = np.append(r_x, r_y)
        r_brown2_err = np.append(lh_link([r_x_low, r_x_high], axis=0), lh_link([r_y_low, r_y_high], axis=0))
        ax.errorbar(r_x, n_x,
                    xerr=np.array([r_x_low, r_x_high], dtype=float),
                    yerr=np.array([n_x_low, n_x_high], dtype=float),
                    fmt="o", c="darkgray", markersize=3)
        ax.errorbar(r_x, n_x, fmt="o", c="black", markersize=3)
        ax.errorbar(r_y, n_y,
                    xerr=np.array([r_y_low, r_y_high], dtype=float),
                    yerr=np.array([n_y_low, n_y_high], dtype=float),
                    fmt="o", c="darkgray", markersize=3)
        ax.errorbar(r_y, n_y, fmt="o", c="black", markersize=3)
        xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
        out_brown = icaps.fit_model(icaps.fit_lin, np.log10(r_brown2), np.log10(n_brown2),
                                    xerr=icaps.log_err(r_brown2, r_brown2_err),
                                    yerr=icaps.log_err(n_brown2, n_brown2_err),
                                    fit_type=0)
        fracdim = out_brown.beta[0]
        fracdim_err = out_brown.sd_beta[0]
        beta = 10 ** out_brown.beta[1]
        beta_err = beta * np.log(10) * out_brown.sd_beta[1]
        label = icaps.get_param_label([[r"$m_\mathrm{BM}/m_0 = \beta \left(r_\mathrm{g,BM}/r_0\right)^{d_\mathrm{f}}$"],
                                       # [r"$\beta$", beta, [-beta_low, beta_high]],
                                       [r"$\beta$", beta, beta_err],
                                       [r"$d_\mathrm{f}$", fracdim, fracdim_err]])
        ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown.beta),
                c="red", lw=2, zorder=10, alpha=1, label=label)
        ax.legend(loc="upper left", fancybox=False)
        ax.minorticks_on()
        icaps.mirror_axis(ax)
        plt.tight_layout()
        plt.savefig(plot_path + f"Gyrad_vs_Mass_Brownian_{tab}.png", dpi=dpi)
        plt.close()

    # ===================================================================================
    # Optical | r_g/r_0 vs. m/m_0
    # ===================================================================================
    ex0 = 10 ** out_ex0_s1.beta[0]
    # ex0_err = 10 ** out_ex0_s1.sd_beta[0]
    ex0_err = ex0 * np.log(10) * out_ex0_s1.sd_beta[0]
    for tab in ["R1", "T1", "BM_ALL"]:
        df_ = table[tab]
        for adapt in ["none", "gyrad", "gyradpow"]:
            _, ax = plt.subplots()
            ax.set_xlabel(r"$r_\mathrm{g,opt} / r_0$")
            ax.set_ylabel(r"$m_\mathrm{opt} / m_0$")
            ax.set_xscale("log")
            ax.set_yscale("log")
            ax.minorticks_on()
            ax.set_xlim(*g_lims_c)
            ax.set_ylim(*n_lims_c)

            ex = df_[ex_col].to_numpy()
            ex_err = df_[ex_err_col].to_numpy()
            gyrad = df_[gyrad_col].to_numpy() * icaps.const.px_ldm / 1e6
            gyrad_err = df_[gyrad_err_col].to_numpy() * icaps.const.px_ldm / 1e6
            if adapt in ["gyrad", "gyradpow"]:
                n_opt, n_opt_low, n_opt_high = adapt_n_opt(ex, ex_err)
            else:
                n_opt = ex / ex0
                n_opt_low = n_opt - (ex - ex_err) / (ex0 + ex0_err)
                n_opt_high = (ex + ex_err) / (ex0 - ex0_err) - n_opt

            if adapt in ["gyrad", "none"]:
                if adapt == "gyrad":
                    gyrad, gyrad_err = adapt_gyrad_opt(gyrad, gyrad_err)
                g_opt = gyrad / icaps.const.r_0
                g_opt_low = g_opt - (gyrad - gyrad_err) / (icaps.const.r_0 + icaps.const.r_0_err)
                g_opt_high = (gyrad + gyrad_err) / (icaps.const.r_0 - icaps.const.r_0_err) - g_opt
            else:
                gyrad, gyrad_low, gyrad_high = adapt_gyrad_opt_pow(gyrad, gyrad_err)
                g_opt = gyrad / icaps.const.r_0
                g_opt_low = g_opt - (gyrad - gyrad_low) / (icaps.const.r_0 + icaps.const.r_0_err)
                g_opt_high = (gyrad + gyrad_high) / (icaps.const.r_0 - icaps.const.r_0_err) - g_opt

            if tab == "R1":
                ax.errorbar(g_opt, n_opt,
                            xerr=[g_opt_low, g_opt_high],
                            yerr=[n_opt_low, n_opt_high],
                            fmt="o", c="darkgray", markersize=3, alpha=1)
            ax.errorbar(g_opt, n_opt, fmt="o", c="black", markersize=3)

            xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
            out_opt = icaps.fit_model(icaps.fit_lin, np.log10(g_opt), np.log10(n_opt),
                                      xerr=icaps.log_err(g_opt, lh_link([g_opt_low, g_opt_high], axis=0)),
                                      yerr=icaps.log_err(n_opt, lh_link([n_opt_low, n_opt_high], axis=0)),
                                      fit_type=0)
            fracdim = out_opt.beta[0]
            fracdim_err = out_opt.sd_beta[0]
            beta = 10 ** out_opt.beta[1]
            beta_err = beta * np.log(10) * out_opt.sd_beta[1]
            label = icaps.get_param_label([
                [r"$m_\mathrm{opt}/m_0 = \beta \left(r_\mathrm{g,opt}/r_0\right)^{d_\mathrm{f}}$"],
                # [r"$\beta$", beta, [-beta_low, beta_high]],
                [r"$\beta$", beta, beta_err],
                [r"$d_\mathrm{f}$", fracdim, fracdim_err]])
            ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt.beta),
                    c="red", lw=2, zorder=10, alpha=1, label=label)
            ax.legend(fancybox=False, loc="upper left")
            icaps.mirror_axis(ax)
            plt.tight_layout()
            plt.savefig(plot_path + "Gyrad_vs_Mass_Optical_adapt_{}_{}.png".format(adapt, tab), dpi=dpi)
            plt.close()
    # """
    # ===================================================================================
    # Collage | Normalized MoI vs. Mass
    # ===================================================================================
    fig, ax = plt.subplots(ncols=3, figsize=(6.4 * 2, 4.8))
    fig.subplots_adjust(wspace=0., bottom=0.13, top=0.94, left=0.05, right=0.95)
    ex0 = 10 ** out_ex0_s1.beta[0]
    ex0_err = ex0 * np.log(10) * out_ex0_s1.sd_beta[0]
    for i in range(3):
        ax[i].set_xlim(*n_lims_c)
        ax[i].set_ylim(*z_lims_c)
        ax[i].set_xscale("log")
        ax[i].set_yscale("log")
        ax[i].minorticks_on()
        icaps.mirror_axis(ax[i])
        if i > 0:
            ax[i].yaxis.set_ticklabels([])
        if i < 2:
            xticks = ax[i].xaxis.get_major_ticks()
            xticks[-2].label1.set_visible(False)
    ax[1].set_xlabel(r"$m/m_0$")
    ax[0].set_ylabel(r"$I/I_0$")
    ax[0].set_title("Brownian Motion Data")
    ax[1].set_title("Uncorrected Optical Data")
    ax[2].set_title("Corrected Optical Data")
    df_ = table["R1"]
    xspace = np.linspace(*ax[0].get_xlim(), 1000)
    for i in range(3):
        ax_ = ax[i]
        if i == 0:
            func_label = r"$I_\mathrm{BM}/I_0 = \gamma \left(m_\mathrm{BM}/m_0\right)^\alpha$"
            mass_x = df_["mass_x"].to_numpy()
            mass_x_err = df_["mass_x_err"].to_numpy()
            mass_y = df_["mass_y"].to_numpy()
            mass_y_err = df_["mass_y_err"].to_numpy()
            n_x = mass_x / icaps.const.m_0
            n_x_low = n_x - (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
            n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_x
            n_y = mass_y / icaps.const.m_0
            n_y_low = n_y - (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
            n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_y
            n = np.append(n_x, n_y)
            n_low = np.append(n_x_low, n_y_low)
            n_high = np.append(n_x_high, n_y_high)
            moi_brown = df_["inertia"].to_numpy()
            moi_brown_err = df_["inertia_err"].to_numpy()
            z_brown = moi_brown / icaps.const.moi_0
            z_brown_low = z_brown - (moi_brown - moi_brown_err) / (icaps.const.moi_0 + icaps.const.moi_0_err)
            z_brown_high = (moi_brown + moi_brown_err) / (icaps.const.moi_0 - icaps.const.moi_0_err) - z_brown
            z = np.tile(z_brown, 2)
            z_low = z - np.tile(-1 * (z_brown_low - z_brown), 2)
            z_high = np.tile(z_brown_high + z_brown, 2) - z
            fails = np.repeat(False, len(n))
        else:
            func_label = r"$I_\mathrm{opt}/I_0 = \gamma \left(m_\mathrm{opt}/m_0\right)^\alpha$"
            ex = df_[ex_col].to_numpy()
            ex_err = df_[ex_err_col].to_numpy()
            gyrad = df_[gyrad_col].to_numpy() * icaps.const.px_ldm / 1e6
            gyrad_err = df_[gyrad_err_col].to_numpy() * icaps.const.px_ldm / 1e6
            if i == 2:
                n, n_low, n_high = adapt_n_opt(ex, ex_err)
            else:
                n = ex / ex0
                n_low = n - (ex - ex_err) / (ex0 + ex0_err)
                n_high = (ex + ex_err) / (ex0 - ex0_err) - n

            fails = np.repeat(False, len(ex))
            i_opt = ex * gyrad ** 2
            i_opt_err = i_opt * np.sqrt((ex_err / ex) ** 2 + (2 * gyrad_err / gyrad) ** 2)
            if i == 2:
                z, fails = adapt_z_opt(i_opt, ret_fails_=True)
                z_low = z - adapt_z_opt(i_opt - i_opt_err)
                z_high = adapt_z_opt(i_opt + i_opt_err) - z
            else:
                z = (i_opt / ex0 * icaps.const.m_0) / icaps.const.moi_0
                z_low = z - ((i_opt - i_opt_err) /
                             (ex0 + ex0_err) *
                             (icaps.const.m_0 - icaps.const.m_0_err)) / (icaps.const.moi_0 + icaps.const.moi_0_err)
                z_high = ((i_opt + i_opt_err) / (ex0 - ex0_err) *
                          (icaps.const.m_0 + icaps.const.m_0_err)) / (icaps.const.moi_0 - icaps.const.moi_0_err) - z
        ax_.errorbar(n[~fails], z[~fails],
                     xerr=np.array([n_low[~fails], n_high[~fails]], dtype=float),
                     yerr=np.array([z_low[~fails], z_high[~fails]], dtype=float),
                     fmt="o", c="darkgray", markersize=3)
        ax_.errorbar(n[~fails], z[~fails], fmt="o", c="black", markersize=3)
        n_err = lh_link([n_low, n_high], axis=0)
        z_err = lh_link([z_low, z_high], axis=0)
        out = icaps.fit_model(icaps.fit_lin, np.log10(n[~fails]), np.log10(z[~fails]),
                              xerr=icaps.log_err(n[~fails], n_err[~fails]),
                              yerr=icaps.log_err(z[~fails], z_err[~fails]), fit_type=0)
        alpha = out.beta[0]
        alpha_err = out.sd_beta[0]
        fracdim = 2 / (alpha - 1)
        fracdim_low = fracdim - 2 / (alpha + alpha_err - 1)
        fracdim_high = 2 / (alpha - alpha_err - 1) - fracdim
        gamma = 10 ** out.beta[1]
        gamma_err = gamma * np.log(10) * out.sd_beta[1]
        beta = (gamma * 2 / 5) ** (1 / (1 - alpha))
        beta_err = beta * np.sqrt((gamma_err / gamma * 1 / (1 - alpha)) ** 2
                                  + (alpha_err * np.log(2 / 5 * gamma) / ((alpha - 1) ** 2)) ** 2)
        label = icaps.get_param_label([
            # [func_label],
            [r"$\gamma$", gamma, gamma_err],
            [r"$\alpha$", alpha, alpha_err],
            [r"$\beta$", beta, beta_err],
            [r"$d_\mathrm{f}$", fracdim, [-fracdim_low, fracdim_high]]])
        ax_.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta),
                 c="red", lw=2, zorder=10, alpha=1, label=label)
        ax_.legend(loc="upper left", fancybox=False)
    plt.savefig(plot_path + "Collage_Moi_vs_Mass_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Collage | r_g/r_0 vs. m/m_0
    # ===================================================================================
    fig, ax = plt.subplots(ncols=3, figsize=(6.4 * 2, 4.8))
    fig.subplots_adjust(wspace=0., bottom=0.13, top=0.94, left=0.05, right=0.95)
    ex0 = 10 ** out_ex0_s1.beta[0]
    ex0_err = ex0 * np.log(10) * out_ex0_s1.sd_beta[0]
    for i in range(3):
        ax[i].set_xlim(*g_lims_c)
        ax[i].set_ylim(*n_lims_c)
        ax[i].set_xscale("log")
        ax[i].set_yscale("log")
        ax[i].minorticks_on()
        icaps.mirror_axis(ax[i])
        if i > 0:
            ax[i].yaxis.set_ticklabels([])
        if i < 2:
            xticks = ax[i].xaxis.get_major_ticks()
            xticks[-2].label1.set_visible(False)
    ax[1].set_xlabel(r"$r_\mathrm{g}/r_0$")
    ax[0].set_ylabel(r"$m/m_0$")
    ax[0].set_title("Brownian Motion Data")
    ax[1].set_title("Uncorrected Optical Data")
    ax[2].set_title("Corrected Optical Data")
    df_ = table["R1"]
    xspace = np.linspace(*ax[0].get_xlim(), 1000)
    for i in range(3):
        ax_ = ax[i]
        if i == 0:
            func_label = r"$m_\mathrm{BM}/m_0 = \beta \left(r_\mathrm{g,BM}/r_0\right)^{d_\mathrm{f}}$"
            mass_x = df_["mass_x"].to_numpy()
            mass_x_err = df_["mass_x_err"].to_numpy()
            mass_y = df_["mass_y"].to_numpy()
            mass_y_err = df_["mass_y_err"].to_numpy()
            n_x = mass_x / icaps.const.m_0
            n_x_low = n_x - (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
            n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_x
            n_y = mass_y / icaps.const.m_0
            n_y_low = n_y - (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
            n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_y
            n = np.append(n_x, n_y)
            n_low = np.append(n_x_low, n_y_low)
            n_high = np.append(n_x_high, n_y_high)
            moi_brown = df_["inertia"].to_numpy()
            moi_brown_err = df_["inertia_err"].to_numpy()
            r_x = np.sqrt(moi_brown / mass_x) / icaps.const.r_0
            r_x_low = r_x - np.sqrt((moi_brown - moi_brown_err) / (mass_x + mass_x_err)) / (
                    icaps.const.r_0 + icaps.const.r_0_err)
            r_x_high = np.sqrt((moi_brown + moi_brown_err) / (mass_x - mass_x_err)) / (
                    icaps.const.r_0 - icaps.const.r_0_err) - r_x
            r_y = np.sqrt(moi_brown / mass_y) / icaps.const.r_0
            r_y_low = r_y - np.sqrt((moi_brown - moi_brown_err) / (mass_y + mass_y_err)) / (
                    icaps.const.r_0 + icaps.const.r_0_err)
            r_y_high = np.sqrt((moi_brown + moi_brown_err) / (mass_y - mass_y_err)) / (
                    icaps.const.r_0 - icaps.const.r_0_err) - r_y
            g = np.append(r_x, r_y)
            g_low = np.append(r_x_low, r_y_low)
            g_high = np.append(r_x_high, r_y_high)
        else:
            func_label = r"$m_\mathrm{opt}/m_0 = \beta \left(r_\mathrm{g,opt}/r_0\right)^{d_\mathrm{f}}$"
            ex = df_[ex_col].to_numpy()
            ex_err = df_[ex_err_col].to_numpy()
            gyrad = df_[gyrad_col].to_numpy() * icaps.const.px_ldm / 1e6
            gyrad_err = df_[gyrad_err_col].to_numpy() * icaps.const.px_ldm / 1e6
            if i == 2:
                n, n_low, n_high = adapt_n_opt(ex, ex_err)
            else:
                n = ex / ex0
                n_low = n - (ex - ex_err) / (ex0 + ex0_err)
                n_high = (ex + ex_err) / (ex0 - ex0_err) - n
            if i == 2:
                gyrad, gyrad_err = adapt_gyrad_opt(gyrad, gyrad_err)
            g = gyrad / icaps.const.r_0
            g_low = g - (gyrad - gyrad_err) / (icaps.const.r_0 + icaps.const.r_0_err)
            g_high = (gyrad + gyrad_err) / (icaps.const.r_0 - icaps.const.r_0_err) - g
        ax_.errorbar(g, n,
                     xerr=np.array([g_low, g_high], dtype=float), yerr=np.array([n_low, n_high], dtype=float),
                     fmt="o", c="darkgray", markersize=3)
        ax_.errorbar(g, n, fmt="o", c="black", markersize=3)
        n_err = lh_link([n_low, n_high], axis=0)
        g_err = lh_link([g_low, g_high], axis=0)
        out = icaps.fit_model(icaps.fit_lin, np.log10(g), np.log10(n),
                              xerr=icaps.log_err(g, g_err), yerr=icaps.log_err(n, n_err), fit_type=0)
        fracdim = out.beta[0]
        fracdim_err = out.sd_beta[0]
        beta = 10 ** out.beta[1]
        beta_err = beta * np.log(10) * out.sd_beta[1]
        label = icaps.get_param_label([
            # [func_label],
            [r"$\beta$", beta, beta_err],
            [r"$d_\mathrm{f}$", fracdim, fracdim_err]])
        ax_.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta),
                 c="red", lw=2, zorder=10, alpha=1, label=label)
        ax_.legend(loc="upper left", fancybox=False)
    plt.savefig(plot_path + "Collage_Mass_vs_Gyrad_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Brownian | Normalized MoI vs. Mass (POSTER)
    # ===================================================================================
    for tab in ["R1"]:
        df_ = table[tab]
        _, ax = plt.subplots()
        ax.set_xlabel(r"$m / m_0$")
        ax.set_ylabel(r"$I / I_0$")
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlim(*n_lims_c)
        ax.set_ylim(*z_lims_c)

        mass_x = df_["mass_x"].to_numpy()
        mass_x_err = df_["mass_x_err"].to_numpy()
        mass_y = df_["mass_y"].to_numpy()
        mass_y_err = df_["mass_y_err"].to_numpy()
        n_x = mass_x / icaps.const.m_0
        n_x_low = n_x - (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_x
        n_y = mass_y / icaps.const.m_0
        n_y_low = n_y - (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
        n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err) - n_y
        n_brown = np.append(n_x, n_y)
        n_brown_err = np.append(lh_link([n_x_low, n_x_high], axis=0), lh_link([n_y_low, n_y_high], axis=0))
        moi_brown = df_["inertia"].to_numpy()
        moi_brown_err = df_["inertia_err"].to_numpy()
        z_brown = moi_brown / icaps.const.moi_0
        z_brown_low = z_brown - (moi_brown - moi_brown_err) / (icaps.const.moi_0 + icaps.const.moi_0_err)
        z_brown_high = (moi_brown + moi_brown_err) / (icaps.const.moi_0 - icaps.const.moi_0_err) - z_brown
        ax.errorbar(n_x, z_brown,
                    xerr=np.array([n_x_low, n_x_high], dtype=float),
                    yerr=np.array([z_brown_low, z_brown_high], dtype=float),
                    fmt="o", c="darkgray", markersize=3)
        ax.errorbar(n_x, z_brown,
                    fmt="o", c="black", markersize=3)
        ax.errorbar(n_y, z_brown,
                    xerr=np.array([n_y_low, n_y_high], dtype=float),
                    yerr=np.array([z_brown_low, z_brown_high], dtype=float),
                    fmt="o", c="darkgray", markersize=3)
        ax.errorbar(n_y, z_brown,
                    fmt="o", c="black", markersize=3)
        xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
        z_brown2 = np.tile(z_brown, 2)
        z_brown2_low = z_brown2 - np.tile(-1 * (z_brown_low - z_brown), 2)
        z_brown2_high = np.tile(z_brown_high + z_brown, 2) - z_brown2
        z_brown2_err = lh_link([z_brown2_low, z_brown2_high], axis=0)
        out_brown = icaps.fit_model(icaps.fit_lin, np.log10(n_brown), np.log10(z_brown2),
                                    xerr=icaps.log_err(n_brown, n_brown_err),
                                    yerr=icaps.log_err(z_brown2, z_brown2_err),
                                    fit_type=0)
        alpha = out_brown.beta[0]
        alpha_err = out_brown.sd_beta[0]
        fracdim = 2 / (alpha - 1)
        fracdim_low = fracdim - 2 / (alpha + alpha_err - 1)
        fracdim_high = 2 / (alpha - alpha_err - 1) - fracdim
        gamma = 10 ** out_brown.beta[1]
        gamma_err = gamma * np.log(10) * out_brown.sd_beta[1]
        beta = (gamma * 2 / 5) ** (1 / (1 - alpha))
        beta_err = beta * np.sqrt((gamma_err / gamma * 1 / (1 - alpha)) ** 2
                                  + (alpha_err * np.log(2 / 5 * gamma) / ((alpha - 1) ** 2)) ** 2)
        label = icaps.get_param_label([[r"$I/I_0 = \gamma \left(m/m_0\right)^\alpha$"],
                                       [r"$\gamma$", gamma, gamma_err],
                                       [r"$\alpha$", alpha, alpha_err],
                                       # [r"$\beta$", beta, beta_err],
                                       [r"$d_\mathrm{f}$", fracdim, [-fracdim_low, fracdim_high]]])
        ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown.beta),
                c="red", lw=2, zorder=10, alpha=1, label=label)
        ax.legend(loc="upper left", fancybox=False)
        ax.minorticks_on()
        icaps.mirror_axis(ax)
        plt.tight_layout()
        plt.savefig(plot_path + f"Poster_Moi_vs_Mass_Brownian_{tab}.png", dpi=dpi)
        plt.close()


if __name__ == '__main__':
    main()


