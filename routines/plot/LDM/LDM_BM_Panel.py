import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.fft import fft, fftfreq
from scipy.stats import gmean
from scipy.ndimage import uniform_filter1d

# Paths
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM/"
csv_of_final = project_path + "OF_Final.csv"
csv_erf_t_flagged = project_path + "ERF_Trans_flagged.csv"
csv_erf_r_flagged = project_path + "ERF_Rot_flagged.csv"
csv_disp_t = project_path + "DISP_Trans.csv"
csv_disp_r = project_path + "DISP_Rot.csv"
csv_vmu_of = project_path + "vmu/OF_Trans.csv"
csv_outs = project_path + "coll_erf_params.csv"
plot_path = project_path + "plots/"
icaps.mk_folders([plot_path], clear=False)

# Filters
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
max_rss_trans = 40e-4
max_rss_rot = 60e-4
max_rel_err_trans_mass = 0.1
max_rel_err_trans_tau = 0.1
max_rel_err_rot_moi = 0.5
max_rel_err_rot_tau = 0.5

# Plots
mass_lims = 2e-15, 1e-11
moi_lims = 5e-27, 1e-21
dpi = 600
fancybox = False
roller = "log"
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df_all = pd.read_csv(csv_of_final)
    df_t1 = icaps.brownian.filter_rss(df_all, thresh=max_rss_trans, mode="trans")
    df_t1 = icaps.brownian.filter_rel_err(df_t1, mass=max_rel_err_trans_mass, mode="trans")
    print(df_t1["tau"].mean(), df_t1["tau"].median(), gmean(df_t1["tau"].to_numpy()))
    df_t2 = icaps.brownian.filter_rel_err(df_t1, tau=max_rel_err_trans_tau, mode="trans")
    print(df_t2["tau"].mean(), df_t2["tau"].median(), gmean(df_t2["tau"].to_numpy()))
    df_r1 = df_t1.dropna(subset=["tau_rot"])
    df_r1 = icaps.brownian.filter_rss(df_r1, thresh=max_rss_rot, mode="rot")
    df_r1 = icaps.brownian.filter_rel_err(df_r1, mass=max_rel_err_rot_moi, mode="rot")
    df_r2 = icaps.brownian.filter_rel_err(df_r1, tau=max_rel_err_trans_tau, mode="trans")
    df_r2 = icaps.brownian.filter_rel_err(df_r2, tau=max_rel_err_rot_tau, mode="rot")
    print(len(df_t1["particle"].unique()), len(df_t2["particle"].unique()))
    print(len(df_r1["particle"].unique()), len(df_r2["particle"].unique()))
    df_erf_t = pd.read_csv(csv_erf_t_flagged)
    df_erf_r = pd.read_csv(csv_erf_r_flagged)
    df_disp_t = pd.read_csv(csv_disp_t)
    df_disp_r = pd.read_csv(csv_disp_r)

    # =========================================================================================================
    # DRIFT PLOTS
    # =========================================================================================================
    cmap = plt.get_cmap("Greys")
    fig, ax = plt.subplots(nrows=3, ncols=2, figsize=(8 * 1.5, 6 * 2))
    fig.subplots_adjust(bottom=0.07, top=0.97, left=0.08, right=0.95, hspace=0.3, wspace=0.)
    dts_ = [1, 5, 10, 50]
    markers = ["o", "s", "d", "^"]
    ins_pos = [0.62, 0.12, 0.32, 0.33]
    ins_ylim = (0.35, 0.65)
    particles_t = df_t1["particle"].unique()
    particles_r = df_r1["particle"].unique()
    for j, axis in enumerate(["x", "y"]):
        df_t1_ = df_t1[["particle", "n_frames", f"mass_{axis}", f"mass_{axis}_err", f"tau_{axis}", f"tau_{axis}_err"]]
        ins = []
        for i in range(2):
            ax_ = ax[i][j]
            ins_ = ax_.inset_axes(ins_pos)
            ins_.minorticks_on()
            if i == 0:
                ins_.set_xlim(-0.25, 0.25)
            else:
                ins_.set_xlim(-0.5, 0.5)
            ins_.set_ylim(ins_ylim)
            ax_.axhline(0.5, c="gray", ls="--", lw=1, zorder=1000)
            ax_.axvline(0, c="gray", ls="--", lw=1, zorder=1000)
            ins_.axhline(0.5, c="gray", ls="--", lw=1, zorder=1000)
            ins_.axvline(0, c="gray", ls="--", lw=1, zorder=1000)
            ins.append(ins_)

        for i, dt in enumerate(dts_):
            idx = (df_erf_t["axis"] == axis) & (df_erf_t["dt"] == dt) & (df_erf_t["particle"].isin(particles_t))
            ax_ = ax[0][j]
            y = df_erf_t.loc[idx, "drift"].dropna().to_numpy() * 1e6
            y = np.sort(y)
            y_norm = np.linspace(1 / len(y), 1, len(y))
            ax_.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                         color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                         label=r"$\Delta t = {{{}}} \,\rm ms$".format(dt)
                         )
            ins[0].errorbar(y, y_norm, fmt=markers[i], markersize=2,
                            color=cmap(0.2 + 0.8 * (1 - i / len(dts_))))

            ax_ = ax[1][j]
            df_erf_t_ = df_erf_t[idx]
            df_erf_t_ = pd.merge(df_erf_t_, df_t1_, on="particle", how="left")
            df_erf_t_["l_drift"] = np.sqrt(2 * icaps.const.k_B * icaps.const.T * df_erf_t_[f"tau_{axis}"] /
                                           df_erf_t_[f"mass_{axis}"] * df_erf_t_["n_frames"] / icaps.const.fps_ldm)
            y = (df_erf_t_["drift"] * df_erf_t_["n_frames"] / icaps.const.fps_ldm /
                 (df_erf_t_["l_drift"] * dt * 1e-3)).dropna().to_numpy()
            y = np.sort(y)
            y_norm = np.linspace(1 / len(y), 1, len(y))
            ax_.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                         color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                         # label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt)
                         )
            ins[1].errorbar(y, y_norm, fmt=markers[i], markersize=2,
                            color=cmap(0.2 + 0.8 * (1 - i / len(dts_))))
        for i in range(2):
            ax_ = ax[i][j]
            ax_.minorticks_on()
            if i == 0:
                ax_.set_xlabel(r"$\delta_{{{}}} \,\rm(\mu m)$".format(axis))
                ax_.set_xlim(-5, 5)
            else:
                label = r"$(\delta_{{{}}} \cdot t_\mathrm{{obs}}) / (l_{{\mathrm{{drift}}, {}}} \cdot \Delta t)$"
                ax_.set_xlabel(label.format(axis, axis))
                ax_.set_xlim(-10, 10)
                tick_labels = [""]
                tick_labels.extend(np.arange(-7.5, 8, 2.5))
                tick_labels.append("")
                ax_.set_xticklabels(tick_labels)
            if j == 0:
                ax_.set_ylabel(r"Normalized Cumulative Frequency")
            else:
                ax_.yaxis.tick_right()
                ax_.set_yticklabels([])
            # ax_.legend(fancybox=fancybox)
            icaps.mirror_axis(ax_, axis="x")
    df_r1_ = df_t1[["particle", "n_frames", "inertia", "inertia_err", "tau_rot", "tau_rot_err"]]
    ins = []
    for i in range(2):
        ax_ = ax[2][i]
        ins_ = ax_.inset_axes(ins_pos)
        ins_.minorticks_on()
        if i == 0:
            ins_.set_xlim(-0.025, 0.025)
        else:
            ins_.set_xlim(-0.05 * 10/3, 0.05 * 10/3)
        ins_.set_ylim(ins_ylim)
        ax_.axhline(0.5, c="gray", ls="--", lw=1, zorder=1000)
        ax_.axvline(0, c="gray", ls="--", lw=1, zorder=1000)
        ins_.axhline(0.5, c="gray", ls="--", lw=1, zorder=1000)
        ins_.axvline(0, c="gray", ls="--", lw=1, zorder=1000)
        ins.append(ins_)
    for i, dt in enumerate(dts_):
        idx = (df_erf_r["dt"] == dt) & (df_erf_r["particle"].isin(particles_r))
        ax_ = ax[2][0]
        y = df_erf_r.loc[idx, "drift"].dropna().to_numpy()
        y = np.sort(y)
        y_norm = np.linspace(1 / len(y), 1, len(y))
        ax_.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                     color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                     # label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt)
                     )
        ins[0].errorbar(y, y_norm, fmt=markers[i], markersize=2,
                        color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                        # label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt)
                        )

        ax_ = ax[2][1]
        df_erf_r_ = df_erf_r[idx]
        df_erf_r_ = pd.merge(df_erf_r_, df_r1_, on="particle", how="left")
        df_erf_r_["l_drift"] = np.sqrt(2 * icaps.const.k_B * icaps.const.T * df_erf_r_["tau_rot"] /
                                       df_erf_r_["inertia"] * df_erf_r_["n_dots"] / icaps.const.fps_ldm)
        y = (df_erf_r_["drift"] * df_erf_r_["n_dots"] / icaps.const.fps_ldm /
             (df_erf_r_["l_drift"] * dt * 1e-3)).dropna().to_numpy()
        y = np.sort(y)
        y_norm = np.linspace(1 / len(y), 1, len(y))
        ax_.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                     color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                     # label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt)
                     )
        ins[1].errorbar(y, y_norm, fmt=markers[i], markersize=2,
                        color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                        # label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt)
                        )
    for i in range(2):
        ax_ = ax[2][i]
        ax_.minorticks_on()
        if i == 0:
            ax_.set_xlabel(r"$\delta_\theta \,\rm(rad)$")
            ax_.set_xlim(-0.5, 0.5)
        else:
            label = r"$(\delta_\theta \cdot t_\mathrm{{obs}}) / (l_{{\mathrm{{drift}}, \theta}} \cdot \Delta t)$"
            ax_.set_xlabel(label)
            ax_.set_xlim(-3, 3)
        if i == 0:
            ax_.set_ylabel(r"Normalized Cumulative Frequency")
        else:
            ax_.yaxis.tick_right()
            ax_.set_yticklabels([])
        # ax_.legend(fancybox=fancybox)
        icaps.mirror_axis(ax_, axis="x")
    ax[0][0].legend(loc="upper left", fancybox=fancybox, framealpha=1.0, facecolor="white").set_zorder(2000)
    plt.savefig(plot_path + "Drift_Panel.png", dpi=dpi)
    plt.close()

    # =========================================================================================================
    # PARAMETERS (Trans)
    # =========================================================================================================
    fig = plt.figure(figsize=(8 * 1.5, 6 * 1.5))
    fig.subplots_adjust(bottom=0.1, top=0.93, left=0.05, right=0.95)
    outer = gridspec.GridSpec(3, 3, figure=fig, wspace=0.0, hspace=0.0)
    for j, dt in enumerate([1, 5, 25]):
        idx = df_erf_t["particle"].isin(df_t1["particle"].unique()) & (df_erf_t["dt"] == dt)
        sig0 = df_erf_t.loc[idx, "sigma0"].to_numpy()
        sig0_err = df_erf_t.loc[idx, "sigma0_err"].to_numpy()
        sig1 = df_erf_t.loc[idx, "sigma1"].to_numpy()
        sig1_err = df_erf_t.loc[idx, "sigma1_err"].to_numpy()
        w0 = df_erf_t.loc[idx, "w0"].to_numpy()
        w0_err = df_erf_t.loc[idx, "w0_err"].to_numpy()
        mu01 = df_erf_t.loc[idx, "drift01"].to_numpy()
        mu01_err = df_erf_t.loc[idx, "drift01_err"].to_numpy()
        rss_sg = df_erf_t.loc[idx, "rss"].to_numpy()
        rss_dg = df_erf_t.loc[idx, "rss_dg"].to_numpy()
        sigcomb = np.array([sig0, sig1])
        sigcomb_err = np.array([sig0_err, sig1_err])
        idx = np.argmax(sigcomb, axis=0)
        idx = tuple(np.array([idx, np.arange(0, len(idx))]))
        sigmax = sigcomb[idx]
        sigmax_err = sigcomb_err[idx]
        idx = np.argmin(sigcomb, axis=0)
        idx = tuple(np.array([idx, np.arange(0, len(idx))]))
        sigmin = sigcomb[idx]
        sigmin_err = sigcomb_err[idx]
        wmax = np.array([w0[i] if sig0[i] > sig1[i] else 1 - w0[i] for i in range(len(sig0))])
        wmin = np.array([w0[i] if sig0[i] <= sig1[i] else 1 - w0[i] for i in range(len(sig0))])
        ax0 = fig.add_subplot(outer[j].subgridspec(1, 1, wspace=0.0, hspace=0.0)[0, 0])
        ax1 = fig.add_subplot(outer[j + 3].subgridspec(1, 1, wspace=0.0, hspace=0.0)[0, 0])
        ax2 = fig.add_subplot(outer[j + 6].subgridspec(1, 1, wspace=0.0, hspace=0.0)[0, 0])
        pos = ax2.get_position()
        pos.y0 -= 0.025
        pos.y1 -= 0.025
        ax2.set_position(pos)
        ax0.errorbar(rss_sg/rss_dg, sigmax/sigmin, fmt=".", c="black", markersize=2)
        ax1.errorbar(rss_sg/rss_dg, wmax, fmt=".", c="black", markersize=2)
        ax1.errorbar(rss_sg/rss_dg, wmin, fmt=".", c="black", markersize=2)
        ax2.errorbar(wmax, sigmax/sigmin, fmt=".", c="black", markersize=2)
        ax2.errorbar(wmin, sigmax/sigmin, fmt=".", c="black", markersize=2)
        ax0.minorticks_on()
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax0.set_xlim(0, 32)
        ax0.set_ylim(0, 18)
        ax1.set_xlim(0, 32)
        ax1.set_ylim(-0.05, 1.05)
        ax2.set_xlim(-0.05, 1.05)
        ax2.set_ylim(0, 18)
        ax0.xaxis.tick_top()
        # ax1.set_xticks([])
        ax1.xaxis.set_ticklabels([])
        ax2.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
        ax1.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
        ax2.xaxis.set_ticklabels([0, 0.2, 0.4, 0.6, 0.8, 1.0])
        ax1.yaxis.set_ticklabels([0, 0.2, 0.4, 0.6, 0.8, 1.0])
        # ax2.spines["bottom"].set_color("gray")
        # ax2.tick_params(axis="x", colors="gray", which="both")
        # ax2.xaxis.label.set_color("gray")
        ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.983, 0.976), xycoords="axes fraction",
                     size=15, ha="right", va="top", bbox=dict(boxstyle="square", fc="w"))
        if j == 0:
            ax0.set_ylabel(r"$\max(\sigma_{x, y}, w_{x, y}) / \min(\sigma_{x, y}, w_{x, y})$")
            ax1.yaxis.set_ticklabels([])
            ax2.set_ylabel(r"$\max(\sigma_{x, y}, w_{x, y}) / \min(\sigma_{x, y}, w_{x, y})$")
        else:
            ax0.yaxis.tick_right()
            ax0.yaxis.set_ticklabels([])
            ax2.yaxis.set_ticklabels([])
            ax1.yaxis.tick_right()
            ax2.yaxis.tick_right()
            if j == 1:
                ax1.yaxis.set_ticklabels([])
                ax2.set_xlabel(r"$a_{x,y}, 1-a_{x,y}$")
                ax0.set_xlabel(r"$\mathrm{RSS}_\mathrm{single}/\mathrm{RSS}_\mathrm{double}$")
                ax0.xaxis.set_label_position("top")
                ax0.xaxis.set_label_coords(0.5, 1.16)
            else:
                ax1.set_ylabel(r"$a_{x,y}, 1-a_{x,y}$")
                ax1.yaxis.set_label_position("right")
        twiny = ax2.twiny()
        twiny.set_xlim(ax2.get_xlim())
        twiny.set_position(ax2.get_position())
        twiny.minorticks_on()
        twiny.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
        twiny.xaxis.set_ticklabels([])
    plt.savefig(plot_path + "Parameterspace_trans.png", dpi=dpi)
    plt.close()

    # =========================================================================================================
    # TRANS
    # =========================================================================================================
    fig = plt.figure(figsize=(8*1.6, 6*1.6))
    fig.subplots_adjust(bottom=0.056, top=0.942, left=0.05, right=0.95)
    outer = gridspec.GridSpec(2, 2, figure=fig, wspace=0.2, hspace=0.2)
    dts = [1, 5]
    df_outs = None
    fit_coll = False
    try:
        df_outs = pd.read_csv(csv_outs)
    except FileNotFoundError:
        fit_coll = True

    # ==========================================================================================================
    # top left
    inner = outer[0].subgridspec(4, 2, wspace=0.0, hspace=0.0)
    xlim = 5
    alpha = 1
    xspace = np.linspace(-xlim, xlim, 1000)
    particles = df_t2["particle"].unique()
    pb = icaps.SimpleProgressBar(len(particles) * 4, "Progress")
    x_alls = []
    y_alls = []
    for j, dt in enumerate(dts):
        x_all = np.array([])
        y_all = np.array([])
        df_disp_t_ = df_disp_t[["particle", f"dx_{dt}", f"dy_{dt}"]].dropna()
        df_erf_t_ = df_erf_t[df_erf_t["dt"] == dt]
        for particle in particles:
            for axis in ["x", "y"]:
                dx = df_disp_t_.loc[df_disp_t_["particle"] == particle, f"d{axis}_{dt}"].to_numpy() * icaps.const.px_ldm
                idx = (df_erf_t_["particle"] == particle) & (df_erf_t_["axis"] == axis)
                drift = df_erf_t_.loc[idx, "drift01"].to_numpy()[0] * 1e6
                sigma = df_erf_t_.loc[idx, "sigma_diff"].to_numpy()[0] * 1e6
                if sigma > 1e3:
                    pb.tick()
                    continue
                xi_ = (dx - drift) / sigma
                x = np.sort(xi_)
                y = np.linspace(1 / len(x), 1, len(x))
                x_all = np.append(x_all, x)
                y_all = np.append(y_all, y)
                pb.tick()
        idx = np.argsort(x_all)
        x_all = x_all[idx]
        y_all = y_all[idx]
        x_alls.append(x_all)
        y_alls.append(y_all)
        ax0 = fig.add_subplot(inner[:2, j])
        ax1 = fig.add_subplot(inner[2, j])
        ax2 = fig.add_subplot(inner[3, j])
        ax0.errorbar(x_all, y_all,
                     markersize=1, mec="none", mfc="darkgray", alpha=alpha, zorder=0, fmt=".")
        ax0.plot(xspace, icaps.fit_erf(xspace, 0, 1),
                 c="red", alpha=1, lw=1, ls="-", zorder=10, label=r"$\Phi$")
        if fit_coll:
            out = icaps.fit_model(icaps.fit_erf_dg, x_all, y_all)
            w0, mu01, sig0, sig1 = out.beta
            w0_err, mu01_err, sig0_err, sig1_err = out.sd_beta
            if sig0 > sig1:
                w0 = 1 - w0
                sig0, sig1 = sig1, sig0
                sig0_err, sig1_err = sig1_err, sig0_err
            df_outs_ = pd.DataFrame(data={
                "dt": [dt], "w0": [w0], "w0_err": [w0_err], "mu01": [mu01], "mu01_err": [mu01_err],
                "sig0": [sig0], "sig0_err": [sig0_err], "sig1": [sig1], "sig1_err": [sig1_err]})
            if df_outs is None:
                df_outs = df_outs_
            else:
                df_outs = df_outs.append(df_outs_, ignore_index=True, sort=False)
        else:
            w0, mu01, sig0, sig1 = df_outs.loc[df_outs["dt"] == dt, ["w0", "mu01", "sig0", "sig1"]].to_numpy()[0]
        label = icaps.get_param_label([
            [r"$a_{x,y}$", 1 - w0],
            [r"$\sigma_{x,y}$", sig0],
            [r"$w_{x,y}$", sig1]
        ])
        ax0.plot(xspace, icaps.fit_erf_dg(xspace, w0, mu01, sig0, sig1),
                 c="blue", alpha=1, lw=1, ls="--", zorder=11, label=r"$\Phi_2$")
        res_sg = y_all - icaps.fit_erf(x_all, 0, 1)
        rss_sg = np.sum(res_sg ** 2) / len(res_sg)
        res_dg = y_all - icaps.fit_erf_dg(x_all, w0, mu01, sig0, sig1)
        rss_dg = np.sum(res_dg ** 2) / len(res_dg)
        ax1.errorbar(x_all, res_sg, mec="none", mfc="darkgray", markersize=1, fmt=".", alpha=alpha, zorder=0)
        ax2.errorbar(x_all, res_dg, mec="none", mfc="darkgray", markersize=1, fmt=".", alpha=alpha, zorder=1)
        ax1.axhline(0, c="black", lw=1, ls="--", zorder=10, alpha=1)
        ax2.axhline(0, c="black", lw=1, ls="--", zorder=10, alpha=1)
        ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.029, 0.96), xycoords="axes fraction",
                     size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))
        ax0.annotate(label, xy=(0.68, 0.29), xycoords="axes fraction",
                     size=12, ha="left", va="top")
        ax1.annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_sg * 1e4),
                     xy=(0.98, 0.2), xycoords="axes fraction",
                     size=12, ha="right", va="top")
        ax2.annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_dg * 1e4),
                     xy=(0.98, 0.2), xycoords="axes fraction",
                     size=12, ha="right", va="top")
        ax1.annotate(r"$\Phi$",
                     xy=(0.02, 0.95), xycoords="axes fraction",
                     size=15, ha="left", va="top")
        ax2.annotate(r"$\Phi_2$",
                     xy=(0.02, 0.95), xycoords="axes fraction",
                     size=15, ha="left", va="top")
        ax0.set_xlim(-xlim, xlim)
        ax1.set_xlim(-xlim, xlim)
        ax2.set_xlim(-xlim, xlim)
        ax0.set_ylim(-0.05, 1.05)
        ax1.set_ylim(-0.1, 0.1)
        ax2.set_ylim(-0.1, 0.1)
        ax0.minorticks_on()
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax0.set_yticks([0, 0.5, 1])
        ax1.set_yticks([-0.05, 0, 0.05])
        ax2.set_yticks([-0.05, 0, 0.05])
        if j == 0:
            # ax0.legend(ncol=2, loc=(0.0, 1.038), fontsize=15, fancybox=False)
            ax1.yaxis.set_ticklabels([])
            ax2.yaxis.set_ticklabels([])
            ax0.set_ylabel("Normalized Cumulative Frequency")
            ax0.yaxis.set_label_coords(-0.15, 0.5)
            ax2.set_xlabel(r"$\Delta \xi_{\mathrm{G,}x,y}$")
            ax2.xaxis.set_label_coords(1.0, -0.32)
        else:
            ax0.yaxis.tick_right()
            ax0.yaxis.set_ticklabels([])
            ax1.yaxis.tick_right()
            ax2.yaxis.tick_right()
            ax1.set_ylabel("Residual")
            ax1.yaxis.set_label_coords(1.342, 0.0)
        ax0.xaxis.set_ticklabels([-4, -2, 0, 2, 4])
        ax0.xaxis.tick_top()
        ax0.xaxis.set_ticklabels([])
        ax1.xaxis.set_ticklabels([])
        ax2.set_xticks([-4, -2, 0, 2, 4])
    if fit_coll:
        df_outs.to_csv(csv_outs, index=False)
    pb.close()

    # ==========================================================================================================
    # top right
    inner = outer[1].subgridspec(2, 1, wspace=0.0, hspace=0.0)
    df_outs = pd.read_csv(csv_outs)
    for j, dt in enumerate(dts):
        ax0 = fig.add_subplot(inner[j, 0])
        w0, mu01, sig0, sig1 = df_outs.loc[df_outs["dt"] == dt, ["w0", "mu01", "sig0", "sig1"]].to_numpy()[0]
        bin_size = 0.25
        bin_lim = np.ceil(np.max(np.abs(x_alls[j])) * 100) / 100
        bins = np.arange(bin_size / 2, bin_lim + bin_size, bin_size)
        bins = np.append(-bins[::-1], bins)
        hist, _ = np.histogram(x_alls[j], bins=bins, density=True)
        ax0.bar(bins[:-1] + bin_size / 2, hist,
                width=bin_size * 0.9, color="lightgray")
        ax0.plot(xspace, np.exp(- xspace ** 2 / 2) / np.sqrt(2 * np.pi),
                 c="red", ls="-", lw=1, zorder=10,
                 label=r"$\Phi$"
                 )
        ax0.plot(xspace,
                 w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)) +
                 (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
                 c="blue", ls="--", lw=1, zorder=11,
                 label=r"$\Phi_2$"
                 )
        ax0.plot(xspace, w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)),
                 c="magenta", ls=":", lw=1, zorder=12,
                 label=r"$(1 - a_{x, y}) \cdot \Phi(\sigma_{x, y})$")
        ax0.plot(xspace, (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
                 c="green", ls="-.", lw=1, zorder=13,
                 label=r"$a_{x, y} \cdot \Phi(w_{x, y})$")
        ax0.set_xlim(-xlim, xlim)
        ax0.set_ylim(0.0, 0.47)
        ax0.minorticks_on()
        twinx = ax0.twinx()
        twinx.set_ylim(*ax0.get_ylim())
        twinx.minorticks_on()
        ax0.yaxis.set_ticklabels([])
        if j == 0:
            twinx.set_ylabel("Normalized Frequency")
            twinx.yaxis.set_label_coords(1.08, 0.0)
            ax0.xaxis.tick_top()
            ax0.xaxis.set_ticklabels([])
            ax0.legend(loc=(-0.75, 1.06), fancybox=False, ncol=4)
            # ax0.legend(loc=(0, 1.038), fancybox=False, ncol=4)
        else:
            ax0.set_xlabel(r"$\Delta \xi_{\mathrm{G,}x,y}$")
            ax0.xaxis.set_label_coords(0.5, -0.32 * 0.5)
        ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.013, 0.96), xycoords="axes fraction",
                     size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))

    # ==========================================================================================================
    # bottom left
    idx = df_erf_t["particle"].isin(particles) & (df_erf_t["dt"] == 1)
    sig0 = df_erf_t.loc[idx, "sigma0"].to_numpy()
    sig0_err = df_erf_t.loc[idx, "sigma0_err"].to_numpy()
    sig1 = df_erf_t.loc[idx, "sigma1"].to_numpy()
    sig1_err = df_erf_t.loc[idx, "sigma1_err"].to_numpy()
    w0 = df_erf_t.loc[idx, "w0"].to_numpy()
    w0_err = df_erf_t.loc[idx, "w0_err"].to_numpy()
    mu01 = df_erf_t.loc[idx, "drift01"].to_numpy()
    mu01_err = df_erf_t.loc[idx, "drift01_err"].to_numpy()
    rss_sg = df_erf_t.loc[idx, "rss"].to_numpy()
    rss_dg = df_erf_t.loc[idx, "rss_dg"].to_numpy()
    sigcomb = np.array([sig0, sig1])
    sigcomb_err = np.array([sig0_err, sig1_err])
    idx = np.argmax(sigcomb, axis=0)
    idx = tuple(np.array([idx, np.arange(0, len(idx))]))
    sigmax = sigcomb[idx]
    sigmax_err = sigcomb_err[idx]
    idx = np.argmin(sigcomb, axis=0)
    idx = tuple(np.array([idx, np.arange(0, len(idx))]))
    sigmin = sigcomb[idx]
    sigmin_err = sigcomb_err[idx]
    wmax = np.array([w0[i] if sig0[i] > sig1[i] else 1 - w0[i] for i in range(len(sig0))])
    wmin = np.array([w0[i] if sig0[i] <= sig1[i] else 1 - w0[i] for i in range(len(sig0))])

    inner = outer[2].subgridspec(1, 1, wspace=0., hspace=0.)
    ax = fig.add_subplot(inner[0, 0])
    ax.errorbar(wmax, sigmax / sigmin, c="black", fmt="o", markersize=3, label="Unfiltered")
    ax.errorbar(wmin, sigmax / sigmin, c="black", fmt="o", markersize=3, label="Unfiltered")
    # idx = (wmax < 0.9) & (wmin < 0.9) & (wmax >= 0.1) & (wmin >= 0.1)
    # idx = idx & (sigmax_err / sigmax < .1) & (sigmin_err / sigmin < .1)
    # ax.errorbar(wmax[idx], sigmax[idx] / sigmin[idx], c="black", fmt="o", markersize=3,
    #             label="Filtered")
    ax.set_xlabel(r"$a_{x, y}, 1 - a_{x, y}$")
    ax.set_ylabel(r"$\max(w_{x, y}, \sigma_{x, y}) / \min(w_{x, y}, \sigma_{x, y})$")
    ax.set_ylim(0.7, 10)
    ax.set_xlim(-0.05, 1.05)
    icaps.mirror_axis(ax)

    # ==========================================================================================================
    # bottom right
    inner = outer[3].subgridspec(1, 1, wspace=0., hspace=0.)
    ax = fig.add_subplot(inner[0, 0])
    xf_all = np.array([])
    yfp_all = np.array([])
    pb = icaps.SimpleProgressBar(len(particles) * 2, "Progress")
    for i, particle in enumerate(particles):
        for axis in ["x", "y"]:
            y = df_disp_t.loc[df_disp_t["particle"] == particle,
                              f"d{axis}_1"].dropna().to_numpy() * icaps.const.px_ldm * 1e-3
            y = np.cumsum(y)
            y = np.append(np.array([0.]), y)
            yf = fft(y)
            n = len(y)
            xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
            yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
            tau = df_all.loc[df_all["particle"] == particle, f"tau_{axis}"].to_numpy()[0]
            mass = df_all.loc[df_all["particle"] == particle, f"mass_{axis}"].to_numpy()[0]
            # yfp /= icaps.const.k_B * icaps.const.T * tau / mass
            xf_all = np.append(xf_all, xf)
            yfp_all = np.append(yfp_all, yfp)
            pb.tick()
    pb.close()
    idx = np.argsort(xf_all)
    xf_all = xf_all[idx]
    yfp_all = yfp_all[idx]
    ax.errorbar(xf_all, yfp_all, c="lightgray", fmt=".", markersize=1, alpha=0.5, zorder=0)
    if roller == "lin":
        movavrg = pd.Series(yfp_all).rolling(len(particles), center=True).mean().to_numpy()
    else:
        movavrg = 10 ** pd.Series(np.log10(yfp_all)).rolling(len(particles), center=True).mean().to_numpy()
        # print(np.argwhere(~np.isnan(movavrg)).squeeze()[0])
    ax.errorbar(xf_all, movavrg, c="black", fmt=".", markersize=1, alpha=1, zorder=1)
    ax.set_ylabel(r"$P_{x, y} \,\rm (arb. units)$")
    ax.set_xlabel(r"$f \,\rm (Hz)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(7e-2, 5.5e2)
    ax.set_ylim(3e-11, 3e-2)
    xspace = np.linspace(2e-1, 400, 1000)
    ax.plot(xspace, 7e-5 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax.text(100, 2.4e-8, r"$\sim f^{-2}$", color="red",
            bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    ax.minorticks_on()
    icaps.mirror_axis(ax, minorticks=True)

    plt.savefig(plot_path + "Panel_trans.png", dpi=dpi)
    plt.close()

    # =========================================================================================================
    # ROT
    # =========================================================================================================
    fig = plt.figure(figsize=(8*1.6, 6*1.6))
    fig.subplots_adjust(bottom=0.056, top=0.942, left=0.05, right=0.95)
    outer = gridspec.GridSpec(2, 2, figure=fig, wspace=0.2, hspace=0.2)
    dts = [1, 17]

    # ==========================================================================================================
    # top left
    inner = outer[0].subgridspec(4, 2, wspace=0.0, hspace=0.0)
    xlim = 5
    alpha = 1
    df_outs = None
    xspace = np.linspace(-xlim, xlim, 1000)
    particles = df_r2["particle"].unique()
    pb = icaps.SimpleProgressBar(len(particles) * 2, "Progress")
    x_alls = []
    y_alls = []
    for j, dt in enumerate(dts):
        x_all = np.array([])
        y_all = np.array([])
        df_disp_r_ = df_disp_r[["particle", f"omega_{dt}"]].dropna()
        df_erf_r_ = df_erf_r[df_erf_r["dt"] == dt]
        for particle in particles:
            dx = df_disp_r_.loc[df_disp_r_["particle"] == particle, f"omega_{dt}"].to_numpy()
            idx = (df_erf_r_["particle"] == particle)
            drift = df_erf_r_.loc[idx, "drift01"].to_numpy()[0]
            sigma = df_erf_r_.loc[idx, "sigma_diff"].to_numpy()[0]
            if sigma > np.sqrt(np.pi ** 2 / 4):
                pb.tick()
                continue
            if np.isnan(sigma) or np.isnan(drift):
                pb.tick()
                continue
            xi_ = (dx - drift) / sigma
            x = np.sort(xi_)
            y = np.linspace(1 / len(x), 1, len(x))
            x_all = np.append(x_all, x)
            y_all = np.append(y_all, y)
            pb.tick()
        idx = np.argsort(x_all)
        x_all = x_all[idx]
        y_all = y_all[idx]
        x_alls.append(x_all)
        y_alls.append(y_all)
        ax0 = fig.add_subplot(inner[:2, j])
        ax1 = fig.add_subplot(inner[2, j])
        ax2 = fig.add_subplot(inner[3, j])
        ax0.errorbar(x_all, y_all, markersize=1, mec="none", mfc="darkgray", alpha=alpha, zorder=0, fmt=".")
        ax0.plot(xspace, icaps.fit_erf(xspace, 0, 1),
                 c="red", alpha=1, lw=1, ls="-", zorder=10, label=r"$\Phi$")
        out = icaps.fit_model(icaps.fit_erf_dg, x_all, y_all, p0=[0.8, 0, 1, 1])
        w0, mu01, sig0, sig1 = out.beta
        w0_err, mu01_err, sig0_err, sig1_err = out.sd_beta
        if sig0 > sig1:
            w0 = 1 - w0
            sig0, sig1 = sig1, sig0
            sig0_err, sig1_err = sig1_err, sig0_err
        df_outs_ = pd.DataFrame(data={
            "dt": [dt], "w0": [w0], "w0_err": [w0_err], "mu01": [mu01], "mu01_err": [mu01_err],
            "sig0": [sig0], "sig0_err": [sig0_err], "sig1": [sig1], "sig1_err": [sig1_err]})
        if df_outs is None:
            df_outs = df_outs_
        else:
            df_outs = df_outs.append(df_outs_, ignore_index=True, sort=False)
        label = icaps.get_param_label([
            [r"$a_\theta$", 1 - w0],
            [r"$\sigma_\theta$", sig0],
            [r"$w_\theta$", sig1]
        ])
        ax0.plot(xspace, icaps.fit_erf_dg(xspace, w0, mu01, sig0, sig1),
                 c="blue", alpha=1, lw=1, ls="--", zorder=11, label=r"$\Phi_2$")
        res_sg = y_all - icaps.fit_erf(x_all, 0, 1)
        rss_sg = np.sum(res_sg ** 2) / len(res_sg)
        res_dg = y_all - icaps.fit_erf_dg(x_all, w0, mu01, sig0, sig1)
        rss_dg = np.sum(res_dg ** 2) / len(res_dg)
        ax1.errorbar(x_all, res_sg, mec="none", mfc="darkgray", markersize=1, fmt=".", alpha=alpha, zorder=0)
        ax2.errorbar(x_all, res_dg, mec="none", mfc="darkgray", markersize=1, fmt=".", alpha=alpha, zorder=1)
        ax1.axhline(0, c="black", lw=1, ls="--", zorder=10, alpha=1)
        ax2.axhline(0, c="black", lw=1, ls="--", zorder=10, alpha=1)
        ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.029, 0.96), xycoords="axes fraction",
                     size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))
        ax0.annotate(label, xy=(0.71, 0.28), xycoords="axes fraction",
                     size=12, ha="left", va="top")
        ax1.annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_sg * 1e4),
                     xy=(0.98, 0.2), xycoords="axes fraction",
                     size=12, ha="right", va="top")
        ax2.annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_dg * 1e4),
                     xy=(0.98, 0.2), xycoords="axes fraction",
                     size=12, ha="right", va="top")
        ax1.annotate(r"$\Phi$",
                     xy=(0.02, 0.95), xycoords="axes fraction",
                     size=15, ha="left", va="top")
        ax2.annotate(r"$\Phi_2$",
                     xy=(0.02, 0.95), xycoords="axes fraction",
                     size=15, ha="left", va="top")
        ax0.set_xlim(-xlim, xlim)
        ax1.set_xlim(-xlim, xlim)
        ax2.set_xlim(-xlim, xlim)
        ax0.set_ylim(-0.05, 1.05)
        ax1.set_ylim(-0.1, 0.1)
        ax2.set_ylim(-0.1, 0.1)
        ax0.minorticks_on()
        ax1.minorticks_on()
        ax2.minorticks_on()
        ax0.set_yticks([0, 0.5, 1])
        ax1.set_yticks([-0.05, 0, 0.05])
        ax2.set_yticks([-0.05, 0, 0.05])
        if j == 0:
            # ax0.legend(ncol=2, loc=(0.0, 1.038), fontsize=15, fancybox=False)
            ax1.yaxis.set_ticklabels([])
            ax2.yaxis.set_ticklabels([])
            ax0.set_ylabel("Normalized Cumulative Frequency")
            ax0.yaxis.set_label_coords(-0.15, 0.5)
            ax2.set_xlabel(r"$\Delta \xi_{\mathrm{G,}\theta}$")
            ax2.xaxis.set_label_coords(1.0, -0.32)
        else:
            ax0.yaxis.tick_right()
            ax0.yaxis.set_ticklabels([])
            ax1.yaxis.tick_right()
            ax2.yaxis.tick_right()
            ax1.set_ylabel("Residual")
            ax1.yaxis.set_label_coords(1.342, 0.0)
        ax0.xaxis.set_ticklabels([-4, -2, 0, 2, 4])
        ax0.xaxis.tick_top()
        ax0.xaxis.set_ticklabels([])
        ax1.xaxis.set_ticklabels([])
        ax2.set_xticks([-4, -2, 0, 2, 4])
    pb.close()

    # ==========================================================================================================
    # top right
    inner = outer[1].subgridspec(2, 1, wspace=0.0, hspace=0.0)
    for j, dt in enumerate(dts):
        ax0 = fig.add_subplot(inner[j, 0])
        w0, mu01, sig0, sig1 = df_outs.loc[df_outs["dt"] == dt, ["w0", "mu01", "sig0", "sig1"]].to_numpy()[0]
        bin_size = 0.25
        bin_lim = np.ceil(np.max(np.abs(x_alls[j])) * 100) / 100
        bins = np.arange(bin_size / 2, bin_lim + bin_size, bin_size)
        bins = np.append(-bins[::-1], bins)
        hist, _ = np.histogram(x_alls[j], bins=bins, density=True)
        ax0.bar(bins[:-1] + bin_size / 2, hist,
                width=bin_size * 0.9, color="lightgray")
        ax0.plot(xspace, np.exp(- xspace ** 2 / 2) / np.sqrt(2 * np.pi),
                 c="red", ls="-", lw=1, zorder=10,
                 label=r"$\Phi$"
                 )
        ax0.plot(xspace,
                 w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)) +
                 (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
                 c="blue", ls="--", lw=1, zorder=11,
                 label=r"$\Phi_2$"
                 )
        ax0.plot(xspace, w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)),
                 c="magenta", ls=":", lw=1, zorder=12,
                 label=r"$(1 - a_\theta) \cdot \Phi(\sigma_\theta)$")
        ax0.plot(xspace, (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
                 c="green", ls="-.", lw=1, zorder=13,
                 label=r"$a_\theta \cdot \Phi(w_\theta)$")
        ax0.set_xlim(-xlim, xlim)
        ax0.set_ylim(0.0, 0.47)
        ax0.minorticks_on()
        twinx = ax0.twinx()
        twinx.set_ylim(*ax0.get_ylim())
        twinx.minorticks_on()
        ax0.yaxis.set_ticklabels([])
        if j == 0:
            twinx.set_ylabel("Normalized Frequency")
            twinx.yaxis.set_label_coords(1.08, 0.0)
            ax0.xaxis.tick_top()
            ax0.xaxis.set_ticklabels([])
            ax0.legend(loc=(-0.70, 1.06), fancybox=False, ncol=4)
            # ax0.legend(loc=(0, 1.038), fancybox=False, ncol=4)
        else:
            ax0.set_xlabel(r"$\Delta \xi_{\mathrm{G,}\theta}$")
            ax0.xaxis.set_label_coords(0.5, -0.32 * 0.5)
        ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.013, 0.96), xycoords="axes fraction",
                     size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))

    # ==========================================================================================================
    # bottom left
    inner = outer[2].subgridspec(1, 1, wspace=0., hspace=0.)
    ax = fig.add_subplot(inner[0, 0])
    idx = df_erf_r["particle"].isin(particles) & (df_erf_r["dt"] == 1)
    sig0 = df_erf_r.loc[idx, "sigma0"].to_numpy()
    sig0_err = df_erf_r.loc[idx, "sigma0_err"].to_numpy()
    sig1 = df_erf_r.loc[idx, "sigma1"].to_numpy()
    sig1_err = df_erf_r.loc[idx, "sigma1_err"].to_numpy()
    w0 = df_erf_r.loc[idx, "w0"].to_numpy()
    w0_err = df_erf_r.loc[idx, "w0_err"].to_numpy()
    mu01 = df_erf_r.loc[idx, "drift01"].to_numpy()
    mu01_err = df_erf_r.loc[idx, "drift01_err"].to_numpy()
    sigcomb = np.array([sig0, sig1])
    sigcomb_err = np.array([sig0_err, sig1_err])
    idx = np.argmax(sigcomb, axis=0)
    idx = tuple(np.array([idx, np.arange(0, len(idx))]))
    sigmax = sigcomb[idx]
    sigmax_err = sigcomb_err[idx]
    idx = np.argmin(sigcomb, axis=0)
    idx = tuple(np.array([idx, np.arange(0, len(idx))]))
    sigmin = sigcomb[idx]
    sigmin_err = sigcomb_err[idx]
    wmax = np.array([w0[i] if sig0[i] > sig1[i] else 1 - w0[i] for i in range(len(sig0))])
    wmin = np.array([w0[i] if sig0[i] <= sig1[i] else 1 - w0[i] for i in range(len(sig0))])
    ax.errorbar(wmax, sigmax / sigmin, c="black", fmt="o", markersize=3, label="Unfiltered")
    ax.errorbar(wmin, sigmax / sigmin, c="black", fmt="o", markersize=3, label="Unfiltered")
    # idx = (wmax < 0.9) & (wmin < 0.9) & (wmax >= 0.1) & (wmin >= 0.1)
    # idx = idx & (sigmax_err / sigmax < .1) & (sigmin_err / sigmin < .1)
    # ax.errorbar(wmax[idx], sigmax[idx] / sigmin[idx], c="black", fmt="o", markersize=3,
    #             label="Filtered")
    ax.set_xlabel(r"$a_\theta, 1 - a_\theta$")
    ax.set_ylabel(r"$\max(w_\theta, \sigma_\theta) / \min(w_\theta, \sigma_\theta)$")
    ax.set_ylim(0.7, 10)
    ax.set_xlim(-0.05, 1.05)
    icaps.mirror_axis(ax)

    # ==========================================================================================================
    # bottom right
    inner = outer[3].subgridspec(1, 1, wspace=0., hspace=0.)
    ax = fig.add_subplot(inner[0, 0])
    xf_all = np.array([])
    yfp_all = np.array([])
    pb = icaps.SimpleProgressBar(len(particles), "Progress")
    for i, particle in enumerate(particles):
        y = df_disp_r.loc[df_disp_r["particle"] == particle,
                          f"omega_1"].dropna().to_numpy()
        y = np.cumsum(y)
        y = np.append(np.array([0.]), y)
        yf = fft(y)
        n = len(y)
        xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
        yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
        tau = df_all.loc[df_all["particle"] == particle, "tau_rot"].to_numpy()[0]
        mass = df_all.loc[df_all["particle"] == particle, "inertia"].to_numpy()[0]
        # yfp /= icaps.const.k_B * icaps.const.T * tau / mass
        xf_all = np.append(xf_all, xf)
        yfp_all = np.append(yfp_all, yfp)
        pb.tick()
    pb.close()
    idx = np.argsort(xf_all)
    xf_all = xf_all[idx]
    yfp_all = yfp_all[idx]
    ax.errorbar(xf_all, yfp_all, c="lightgray", fmt=".", markersize=1, alpha=1, zorder=0)
    if roller == "lin":
        movavrg = pd.Series(yfp_all).rolling(len(particles), center=True).mean().to_numpy()
    else:
        movavrg = 10 ** pd.Series(np.log10(yfp_all)).rolling(len(particles), center=True).mean().to_numpy()
    ax.errorbar(xf_all, movavrg, c="black", fmt=".", markersize=1, alpha=1, zorder=1)
    ax.set_ylabel(r"$P_\theta \,\rm (arb. units)$")
    ax.set_xlabel(r"$f \,\rm (Hz)$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(7e-2, 5.5e2)
    ax.set_ylim(3e-7, 3e1)
    xspace = np.linspace(2e-1, 400, 1000)
    ax.plot(xspace, 1e0 * xspace ** (-2), c="red", ls="--", lw=2, zorder=3)
    ax.text(100, 5e-4, r"$\sim f^{-2}$", color="red",
            bbox={"boxstyle": "square", "fc": "white", "ec": "black"})
    icaps.mirror_axis(ax)

    plt.savefig(plot_path + "Panel_rot.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()


