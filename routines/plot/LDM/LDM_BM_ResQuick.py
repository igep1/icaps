import icaps
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_BM/"
csv_of_trans = project_path + "OF_Trans.csv"

dts = np.arange(1, 11, 1)
dts = np.append(dts, np.arange(11, 26, 2))
dts = np.append(dts, np.arange(30, 51, 5))
max_rss_trans = 40e-4
max_rss_rot = 60e-4
max_rel_err_trans_mass = 0.1
max_rel_err_trans_tau = 0.1
max_rel_err_rot_moi = 0.5
max_rel_err_rot_tau = 0.5
ldm_ex_col = "max_ex"
ldm_ex_err_col = "median_ex_err_median"


dpi = 600
fancybox = False
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df = pd.read_csv(csv_of_trans)
    rss_x = df.loc[(df["axis"] == "x"), "rss"].to_numpy()
    rss_y = df.loc[(df["axis"] == "y"), "rss"].to_numpy()
    mass_x = df.loc[(df["axis"] == "x"), "mass"].to_numpy()
    mass_y = df.loc[(df["axis"] == "y"), "mass"].to_numpy()
    mass_x_err = df.loc[(df["axis"] == "x"), "mass_err"].to_numpy()
    mass_y_err = df.loc[(df["axis"] == "y"), "mass_err"].to_numpy()
    tau_x = df.loc[(df["axis"] == "x"), "tau"].to_numpy()
    tau_y = df.loc[(df["axis"] == "y"), "tau"].to_numpy()
    tau_x_err = df.loc[(df["axis"] == "x"), "tau_err"].to_numpy()
    tau_y_err = df.loc[(df["axis"] == "y"), "tau_err"].to_numpy()
    mass_x_ = mass_x[(rss_x <= max_rss_trans) & (rss_y <= max_rss_trans)
                     & (mass_x_err / mass_x <= max_rel_err_trans_mass) & (mass_y_err / mass_y <= max_rel_err_trans_mass)
                     # & (tau_x_err / tau_x <= max_rel_err_trans_tau) & (tau_y_err / tau_y <= max_rel_err_trans_tau)
    ]
    mass_y_ = mass_y[(rss_x <= max_rss_trans) & (rss_y <= max_rss_trans)
                     & (mass_x_err / mass_x <= max_rel_err_trans_mass) & (mass_y_err / mass_y <= max_rel_err_trans_mass)
                     # & (tau_x_err / tau_x <= max_rel_err_trans_tau) & (tau_y_err / tau_y <= max_rel_err_trans_tau)
    ]
    print(len(mass_x_), len(mass_y_))
    _, ax = plt.subplots()
    ax.scatter(mass_x_, mass_y_, c="black", marker="o", s=1, zorder=1)
    ax.plot(ax.get_xlim(), ax.get_xlim(), c="gray", lw=1, ls="--", zorder=0)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel(r"$m_x\,\rm(kg)$")
    ax.set_ylabel(r"$m_y\,\rm(kg)$")
    plt.show()


if __name__ == '__main__':
    main()

