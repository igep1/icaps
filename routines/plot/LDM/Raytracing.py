import icaps
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/thilo/"
data_path = project_path + "raytracing plots/data_plot/"
csv_data = data_path + "data.csv"
csv_x = data_path + "z.csv"
csv_y = data_path + "y.csv"
csv_dx = data_path + "Δz.csv"
csv_dy = data_path + "Δy.csv"

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    # df = pd.read_csv(csv_data)
    # cols = df.columns
    # df.rename(columns={cols[0]: "gen", cols[1]: "median", cols[2]: "sigma", cols[3]: "2sigma",
    #                    cols[4]: "max"}, inplace=True)
    # df["gen"] = df["gen"].astype(int)
    # _, ax = plt.subplots()
    # ax.set_yscale("log")
    # ax.set_ylim(5e-15, 1e-10)
    # icaps.mirror_axis(ax)
    # ax.set_xlabel(r"$m \, (m_0)$")
    # ax.set_ylabel(r"$(\Delta x)^2, (\Delta y)^2 \, \rm (\mu m ^2)$")
    # ax.scatter(2 ** df["gen"], df["max"] * 1e-12,
    #            marker="^", edgecolor="black", facecolor="none", s=20, label="Maximum")
    # ax.scatter(2 ** df["gen"], df["2sigma"] * 1e-12,
    #            marker="D", edgecolor="black", facecolor="none", s=20, label=r"$2\sigma$")
    # ax.scatter(2 ** df["gen"], df["sigma"] * 1e-12,
    #            marker="s", edgecolor="black", facecolor="none", s=20, label=r"$\sigma$")
    # ax.scatter(2 ** df["gen"], df["median"] * 1e-12,
    #            marker="o", edgecolor="black", facecolor="none", s=20, label="Median")
    # ax.set_xscale("log")
    # ax.set_xlim(1, 1000)
    # ax.legend(fancybox=False)
    # plt.tight_layout()
    # plt.savefig(project_path + "raytracing_noise_distributions.png", dpi=dpi)
    # plt.close()

    df = pd.read_csv(csv_x, header=None).rename(columns={0: "x"})
    df = df.join(pd.read_csv(csv_y, header=None).rename(columns={0: "y"}))
    df = df.join(pd.read_csv(csv_dx, header=None).rename(columns={0: "dx"}))
    df = df.join(pd.read_csv(csv_dy, header=None).rename(columns={0: "dy"}))
    fig, ax = plt.subplots(2, 2)
    for j in range(2):
        for i in range(2):
            ax_ = ax[j][i]
            ax_.minorticks_on()
            if i == 0:
                ax_.set_ylim(-1, 1)
                ax_.set_yticks([-0.8, -0.4, 0.0, 0.4, 0.8])
                ax_.set_xlim(0, 2 * np.pi)
                ax_.axhline(0, color="gray", zorder=2, ls="--", lw=1)
                if j == 0:
                    icaps.mirror_axis(ax_, axis="x")
                    ax_.set_xticks([])
                    ax_.plot(np.deg2rad(df.index), df["x"], c="black", ls="-", lw=1, zorder=1)
                    ax_.set_ylabel(r"${{{}}} \, \rm (\mu m)$".format("x"))
                else:
                    ax_.plot(np.deg2rad(df.index), df["y"], c="black", ls="-", lw=1, zorder=1)
                    ax_.set_xlabel("Rotation Angle (rad)")
                    ax_.set_ylabel(r"${{{}}} \, \rm (\mu m)$".format("y"))
            else:
                ax_.set_xlim(-0.02, 0.5)
                ax_.yaxis.tick_right()
                ax_.set_ylim(-0.05, 1.05)
                ax_.set_yticks([0.0, 0.25, 0.5, 0.75, 1.0])
                if j == 0:
                    icaps.mirror_axis(ax_, axis="x")
                    ax_.set_xticks([])
                    x = df["dx"].dropna()
                    nx = np.linspace(1 / len(x), 1, len(x))
                    ax_.scatter(x, nx, color="black", marker="o", s=2)
                else:
                    ax_.set_xlabel(r"$(\Delta x)^2,(\Delta y)^2 \, \rm (\mu m ^2)$")
                    y = df["dy"].dropna()
                    ny = np.linspace(1 / len(y), 1, len(y))
                    ax_.scatter(y, ny, color="black", marker="o", s=2)
    fig.text(0.97, 0.5, "Normalized Cumulative Frequency", va="center", rotation=+90.)
    fig.subplots_adjust(hspace=.001, wspace=.02)
    fig.subplots_adjust(left=0.15, right=0.87)
    fig.subplots_adjust(bottom=0.15, top=0.92)
    # plt.show()
    plt.savefig(project_path + "raytracing_rot_dists.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()
