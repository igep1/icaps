import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# Paths
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
plot_path = project_path + "plots/collective/"
csv_of_final = project_path + "OF_Final_diff_drop1.csv"
csv_disp_t = project_path + "DISP_Trans.csv"
csv_erf_t_flagged = project_path + "ERF_Trans_flagged.csv"
csv_disp_r = project_path + "DISP_Rot.csv"
csv_erf_r_flagged = project_path + "ERF_Rot_flagged.csv"

# Filters
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
max_rss_trans = 40e-4
max_rss_rot = 60e-4
max_rel_err_trans_mass = 0.1
max_rel_err_trans_tau = 0.1
max_rel_err_rot_moi = 0.5
max_rel_err_rot_tau = 0.5

# Plots
axes = ["x", "y"]
m = {"SG": ("drift", "sigma"), "SR": ("drift01", "sigma_diff")}
dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)
icaps.mk_folders([plot_path])


def main():
    # df = pd.read_csv(project_path + "Phases_Hand.csv")
    # print(len(df[(df["phase"] == 0) & (df["frame"] < 260000)]))
    # print(len(df[(df["phase"] == 0) & (df["frame"] >= 260000)]))
    df_all = pd.read_csv(csv_of_final)
    df_t1 = icaps.brownian.filter_rss(df_all, thresh=max_rss_trans, mode="trans")
    df_t1 = icaps.brownian.filter_rel_err(df_t1, mass=max_rel_err_trans_mass, mode="trans")
    df_t2 = icaps.brownian.filter_rel_err(df_t1, tau=max_rel_err_trans_tau, mode="trans")
    df_r1 = df_t1.dropna(subset=["tau_rot"])
    df_r1 = icaps.brownian.filter_rss(df_r1, thresh=max_rss_rot, mode="rot")
    df_r1 = icaps.brownian.filter_rel_err(df_r1, mass=max_rel_err_rot_moi, mode="rot")
    df_r2 = icaps.brownian.filter_rel_err(df_r1, tau=max_rel_err_trans_tau, mode="trans")
    df_r2 = icaps.brownian.filter_rel_err(df_r2, tau=max_rel_err_rot_tau, mode="rot")
    df_of_t = df_t1.copy()
    particles_t = df_of_t["particle"].unique()
    df_of_r = df_r1.copy()
    particles_r = df_of_r["particle"].unique()
    df_erf_t = pd.read_csv(csv_erf_t_flagged)
    df_erf_r = pd.read_csv(csv_erf_r_flagged)
    df_disp_t = pd.read_csv(csv_disp_t)
    df_disp_r = pd.read_csv(csv_disp_r)

    # # ===================================================================================
    # # COLLECTIVE ERF 3 x 2
    # # ===================================================================================
    # # ===========================
    # # Trans 3 x 2
    # dts_ = [1, 5, 10, 21, 35, 50]
    # for axis in ["x", "y"]:
    #     pb = icaps.SimpleProgressBar(len(particles_t) * len(dts_), f"Collective ERF {axis} SR 3 x 2")
    #     fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={"height_ratios": np.tile([2, 1], len(dts_) // 2)},
    #                            figsize=[5.2, 6.4])
    #     plt.subplots_adjust(hspace=.001, wspace=.001)
    #     xspace = np.linspace(-10, 10, 1000)
    #     for j, dt in enumerate(dts_):
    #         col = j % 2
    #         row0 = 2 * (j // 2)
    #         row1 = 2 * (j // 2) + 1
    #         ax0 = ax[row0][col]
    #         ax1 = ax[row1][col]
    #         ax0.set_xlim(-5, 5)
    #         ax1.set_xlim(-5, 5)
    #         ax0.set_ylim(-0.02, 1.02)
    #         ax1.set_ylim(-0.1, 0.1)
    #         ax0.minorticks_on()
    #         ax1.minorticks_on()
    #         ax0.plot(xspace, icaps.fit_erf(xspace, 0, 1), c="red", lw=2, zorder=2, alpha=0.6)
    #         ax1.axhline(0, c="gray", lw=2, ls="--", zorder=2)
    #         ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.027, 0.95), xycoords="axes fraction",
    #                      size=13, ha="left", va="top",
    #                      bbox=dict(boxstyle="square", fc="w"))
    #         if row0 == 0:
    #             ax0.xaxis.tick_top()
    #             ax0.set_xticks([-4, 0, 4])
    #             ax0.xaxis.set_ticklabels([])
    #             ax1.set_xticks([])
    #         else:
    #             ax0.set_xticks([])
    #             ax1.set_xticks([])
    #         if row0 == 4:
    #             ax1.set_xticks([-4, 0, 4])
    #         if col == 1:
    #             ax0.set_yticks([])
    #             ax1.yaxis.tick_right()
    #         else:
    #             ax1.set_yticks([])
    #         x_all = np.array([])
    #         y_all = np.array([])
    #         df_disp_t_ = df_disp_t[["particle", f"d{axis}_{dt}"]].dropna()
    #         df_erf_t_ = df_erf_t[(df_erf_t["dt"] == dt) & (df_erf_t["axis"] == axis)]
    #         # for particle in df_t2["particle"].unique():
    #         for particle in particles_t:
    #             dx = df_disp_t_.loc[df_disp_t_["particle"] == particle, f"d{axis}_{dt}"].to_numpy() * icaps.const.px_ldm
    #             idx = (df_erf_t_["particle"] == particle)
    #             drift = df_erf_t_.loc[idx, "drift01"].to_numpy()[0] * 1e6
    #             sigma = df_erf_t_.loc[idx, "sigma_diff"].to_numpy()[0] * 1e6
    #             if sigma > 1e3:
    #                 pb.tick()
    #                 continue
    #             xi_ = (dx - drift) / sigma
    #             x = np.sort(xi_)
    #             y = np.linspace(1 / len(x), 1, len(x))
    #             x_all = np.append(x_all, x)
    #             y_all = np.append(y_all, y)
    #             pb.tick()
    #         ax0.errorbar(x_all, y_all,
    #                      mfc="black", mec="none", markersize=1, fmt=".", zorder=1, alpha=0.1)
    #         ax1.errorbar(x_all, y_all - icaps.fit_erf(x_all, 0, 1),
    #                      mfc="black", mec="none", markersize=1, fmt=".", zorder=1, alpha=0.1)
    #
    #     fig.text(0.50, 0.01, r"$\Delta\xi_{{\mathrm{{G,}}{{{}}}}}$".format(axis), ha="center")
    #     fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    #     fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
    #     fig.subplots_adjust(bottom=0.08, top=0.95)
    #     fig.subplots_adjust(left=0.09, right=0.87)
    #     plt.savefig(plot_path + f"erf_coll_large_SR_{axis}.png", dpi=dpi)
    #     plt.close()
    #     pb.close()
    #
    # # ===========================
    # # Rot 3 x 2
    # dts_ = [1, 5, 10, 21, 35, 50]
    # pb = icaps.SimpleProgressBar(len(particles_r) * len(dts_), "Collective ERF Rot SR 3 x 2")
    # fig, ax = plt.subplots(nrows=6, ncols=2, gridspec_kw={"height_ratios": np.tile([2, 1], len(dts_) // 2)},
    #                        figsize=[5.2, 6.4])
    # plt.subplots_adjust(hspace=.001, wspace=.001)
    # xspace = np.linspace(-10, 10, 1000)
    # for j, dt in enumerate(dts_):
    #     col = j % 2
    #     row0 = 2 * (j // 2)
    #     row1 = 2 * (j // 2) + 1
    #     ax0 = ax[row0][col]
    #     ax1 = ax[row1][col]
    #     ax0.set_xlim(-5, 5)
    #     ax1.set_xlim(-5, 5)
    #     ax0.set_ylim(-0.02, 1.02)
    #     ax1.set_ylim(-0.1, 0.1)
    #     ax0.minorticks_on()
    #     ax1.minorticks_on()
    #     ax0.plot(xspace, icaps.fit_erf(xspace, 0, 1), c="red", lw=2, zorder=2, alpha=0.6)
    #     ax1.axhline(0, c="gray", lw=2, ls="--", zorder=2)
    #     ax0.annotate(r"$\Delta t$ = {} ms".format(dt), xy=(0.027, 0.95), xycoords="axes fraction",
    #                  size=13, ha="left", va="top",
    #                  bbox=dict(boxstyle="square", fc="w"))
    #     if row0 == 0:
    #         ax0.xaxis.tick_top()
    #         ax0.set_xticks([-4, 0, 4])
    #         ax0.xaxis.set_ticklabels([])
    #         ax1.set_xticks([])
    #     else:
    #         ax0.set_xticks([])
    #         ax1.set_xticks([])
    #     if row0 == 4:
    #         ax1.set_xticks([-4, 0, 4])
    #     if col == 1:
    #         ax0.set_yticks([])
    #         ax1.yaxis.tick_right()
    #     else:
    #         ax1.set_yticks([])
    #     x_all = np.array([])
    #     y_all = np.array([])
    #     df_disp_r_ = df_disp_r[["particle", f"omega_{dt}"]].dropna()
    #     df_erf_r_ = df_erf_r[df_erf_r["dt"] == dt]
    #     for particle in particles_r:
    #         dx_label = "omega_{}".format(dt)
    #         dx = df_disp_r_.loc[df_disp_r_["particle"] == particle, dx_label].to_numpy()
    #         idx = df_erf_r_["particle"] == particle
    #         drift = df_erf_r_.loc[idx, "drift01"].to_numpy()[0]
    #         sigma = df_erf_r_.loc[idx, "sigma_diff"].to_numpy()[0]
    #         if sigma > np.sqrt(np.pi ** 2 / 4):
    #             pb.tick()
    #             continue
    #         xi_ = (dx - drift) / sigma
    #         x = np.sort(xi_)
    #         y = np.linspace(1 / len(x), 1, len(x))
    #         x_all = np.append(x_all, x)
    #         y_all = np.append(y_all, y)
    #         pb.tick()
    #     ax0.errorbar(x_all, y_all,
    #                  mfc="black", mec="none", markersize=1, fmt=".", zorder=1)
    #     ax1.errorbar(x_all, y_all - icaps.fit_erf(x_all, 0, 1),
    #                  mfc="black", mec="none", markersize=1, fmt=".", zorder=1)
    #
    # fig.text(0.50, 0.01, r"$\Delta\xi_{{\mathrm{{G,}}\theta}}$", ha="center")
    # fig.text(0.005, 0.50, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    # fig.text(0.97, 0.50, "Residual", va="center", rotation=+90.)
    # fig.subplots_adjust(bottom=0.08, top=0.95)
    # fig.subplots_adjust(left=0.09, right=0.87)
    # plt.savefig(plot_path + "erf_coll_large_SR_rot.png", dpi=dpi)
    # plt.close()
    # pb.close()

    # ===================================================================================
    # Collective OF
    # ===================================================================================
    # Trans X | Y
    modes = [
        # "SG",
        "SR"
    ]
    for mode in modes:
        pb = icaps.SimpleProgressBar(len(axes) * len(particles_t), f"Collective OF Trans {mode}")
        for axis in axes:
            df_erf_t_ = df_erf_t[(df_erf_t["axis"] == axis) & (df_erf_t["dt"].isin(dts_trim))]
            fig, ax = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]})
            ax[1].set_xscale("log")
            theta_full = np.array([])
            xi_full = np.array([])
            res_full = np.array([])
            for i, particle in enumerate(particles_t):
                sigma = df_erf_t_.loc[(df_erf_t_["particle"] == particle), m[mode][1]].to_numpy()
                dt = df_erf_t_.loc[(df_erf_t_["particle"] == particle), "dt"].to_numpy()
                mass = df_of_t.loc[df_of_t["particle"] == particle, "mass_{}".format(axis)].to_numpy()[0]
                tau = df_of_t.loc[df_of_t["particle"] == particle, "tau_{}".format(axis)].to_numpy()[0]
                xi = sigma ** 2 / (2 * icaps.const.k_B * icaps.const.T / mass * tau ** 2)
                theta = dt / (tau * 1e3)
                xi = xi[sigma <= 1e-3]
                theta = theta[sigma <= 1e-3]
                res = np.log10(xi / (theta - 1 + np.exp(-theta)))
                theta_full = np.append(theta_full, theta)
                xi_full = np.append(xi_full, xi)
                res_full = np.append(res_full, res)
                pb.tick()
            ax[0].errorbar(theta_full, xi_full, mfc="black", mec="none", markersize=2,
                           fmt=".", zorder=1)
            ax[1].errorbar(theta_full, res_full, mfc="black", mec="none", markersize=2,
                           fmt=".", zorder=1)
            ax[0].set_yscale("log")
            ax[0].set_xscale("log")
            ax[0].set_xlim(0.05, 25)
            ax[0].set_ylim(1e-3, 40)
            ax[1].set_ylim(-0.5, 0.5)
            ax[1].set_xlim(0.05, 25)
            ax[0].minorticks_on()
            ax[1].minorticks_on()
            ax[0].set_xticks([])
            ax[1].yaxis.tick_right()
            ax[1].yaxis.set_label_position("right")
            ax[0].set_ylabel(r"$\left<\Delta \xi_{{\mathrm{{OFT}}, {}}} ^2\right>$".format(axis))
            ax[1].set_xlabel(r"$\vartheta_{{{}}}$".format(axis))
            ax[1].set_ylabel("Residual (dex)")
            xspace = np.linspace(ax[0].get_xlim()[0], ax[0].get_xlim()[1], 1000)
            ax[1].axhline(0, color="gray", ls="--", zorder=100, alpha=1, lw=2)
            ax[0].plot(xspace, xspace - 1 + np.exp(-xspace), color="red", ls="-", lw=2, alpha=0.6, zorder=1000)
            fig.subplots_adjust(hspace=.001, wspace=.001)
            fig.subplots_adjust(left=0.13, right=0.87)
            plt.savefig(plot_path + "of_coll_{}_{}.png".format(mode, axis), dpi=dpi)
            plt.close()
        pb.close()

    # ===========================
    # Trans X & Y
    modes = [
        # "SG",
        "SR"
    ]
    for mode in modes:
        pb = icaps.SimpleProgressBar(len(axes) * len(particles_t), f"Collective OF Trans {mode}")
        fig, ax = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]})
        for axis in axes:
            df_erf_t_ = df_erf_t[(df_erf_t["axis"] == axis) & (df_erf_t["dt"].isin(dts_trim))]
            ax[1].set_xscale("log")
            theta_full = np.array([])
            xi_full = np.array([])
            res_full = np.array([])
            for i, particle in enumerate(particles_t):
                sigma = df_erf_t_.loc[(df_erf_t_["particle"] == particle), m[mode][1]].to_numpy()
                dt = df_erf_t_.loc[(df_erf_t_["particle"] == particle), "dt"].to_numpy()
                mass = df_of_t.loc[df_of_t["particle"] == particle, "mass_{}".format(axis)].to_numpy()[0]
                tau = df_of_t.loc[df_of_t["particle"] == particle, "tau_{}".format(axis)].to_numpy()[0]
                xi = sigma ** 2 / (2 * icaps.const.k_B * icaps.const.T / mass * tau ** 2)
                theta = dt / (tau * 1e3)
                xi = xi[sigma <= 1e-3]
                theta = theta[sigma <= 1e-3]
                res = np.log10(xi / (theta - 1 + np.exp(-theta)))
                theta_full = np.append(theta_full, theta)
                xi_full = np.append(xi_full, xi)
                res_full = np.append(res_full, res)
                pb.tick()
            ax[0].errorbar(theta_full, xi_full, mfc="black", mec="none", markersize=2,
                           fmt=".", zorder=1)
            ax[1].errorbar(theta_full, res_full, mfc="black", mec="none", markersize=2,
                           fmt=".", zorder=1)
        ax[0].set_yscale("log")
        ax[0].set_xscale("log")
        ax[0].set_xlim(0.05, 25)
        ax[0].set_ylim(1e-3, 40)
        ax[1].set_ylim(-0.5, 0.5)
        ax[1].set_xlim(0.05, 25)
        ax[0].minorticks_on()
        ax[1].minorticks_on()
        ax[0].xaxis.tick_top()
        ax[0].xaxis.set_ticklabels([])
        ax[1].yaxis.set_label_position("right")
        ax[1].yaxis.tick_right()
        ax[0].set_ylabel(r"$\left<\Delta \xi_\mathrm{OFT} ^2\right>$")
        ax[1].set_xlabel(r"$\vartheta_\mathrm{t}$")
        ax[1].set_ylabel("Residual (dex)")
        xspace = np.linspace(ax[0].get_xlim()[0], ax[0].get_xlim()[1], 1000)
        ax[1].axhline(0, color="gray", ls="--", zorder=100, alpha=1, lw=2)
        ax[0].plot(xspace, xspace - 1 + np.exp(-xspace), color="red", ls="-", lw=2, alpha=0.6)
        fig.subplots_adjust(hspace=.001, wspace=.001)
        fig.subplots_adjust(left=0.13, right=0.87)
        plt.savefig(plot_path + "of_coll_{}_both.png".format(mode), dpi=dpi)
        plt.close()
        pb.close()

    # ===========================
    # Rot
    modes = [
        # "SG",
        "SR"
    ]
    df_erf_r_ = df_erf_r[df_erf_r["dt"].isin(dts_trim)]
    for mode in modes:
        pb = icaps.ProgressBar(len(particles_r), f"Collective OF Rot {mode}")
        fig, ax = plt.subplots(nrows=2, gridspec_kw={"height_ratios": [2, 1]})
        ax[1].set_xscale("log")
        theta_full = np.array([])
        xi_full = np.array([])
        res_full = np.array([])
        for i, particle in enumerate(particles_r):
            sigma = df_erf_r_.loc[(df_erf_r_["particle"] == particle), m[mode][1]].to_numpy()
            dt = df_erf_r_.loc[(df_erf_r_["particle"] == particle), "dt"].to_numpy()
            inertia = df_of_r.loc[df_of_r["particle"] == particle, "inertia"].to_numpy()[0]
            tau = df_of_r.loc[df_of_r["particle"] == particle, "tau_rot"].to_numpy()[0]
            xi = sigma ** 2 / (2 * icaps.const.k_B * icaps.const.T / inertia * tau ** 2)
            theta = dt / (tau * 1e3)
            xi = xi[sigma <= np.sqrt(np.pi ** 2 / 4)]
            theta = theta[sigma <= np.sqrt(np.pi ** 2 / 4)]
            res = np.log10(xi / (theta - 1 + np.exp(-theta)))
            theta_full = np.append(theta_full, theta)
            xi_full = np.append(xi_full, xi)
            res_full = np.append(res_full, res)
            pb.tick()
        ax[0].errorbar(theta_full, xi_full, mfc="black", mec="none", markersize=2,
                       fmt=".", zorder=1)
        ax[1].errorbar(theta_full, res_full, mfc="black", mec="none", markersize=2,
                       fmt=".", zorder=1)
        ax[0].set_yscale("log")
        ax[0].set_xscale("log")
        ax[0].set_xlim(0.05, 25)
        ax[0].set_ylim(1e-3, 40)
        ax[1].set_xlim(0.05, 25)
        ax[0].minorticks_on()
        ax[1].minorticks_on()
        ax[1].set_ylim(-0.5, 0.5)
        ax[0].set_xticks([])
        ax[1].yaxis.tick_right()
        ax[1].yaxis.set_label_position("right")
        ax[0].set_ylabel(r"$\left<\Delta \xi_{\rm OFR} ^2\right>$")
        ax[1].set_xlabel(r"$\vartheta_{\rm r}$")
        ax[1].set_ylabel("Residual (dex)")
        xspace = np.linspace(ax[0].get_xlim()[0], ax[0].get_xlim()[1], 1000)
        ax[1].axhline(0, color="gray", ls="--", zorder=100, alpha=1, lw=2)
        ax[0].plot(xspace, xspace - 1 + np.exp(-xspace), color="red", ls="-", lw=2, alpha=0.6)
        fig.subplots_adjust(hspace=.001, wspace=.001)
        fig.subplots_adjust(left=0.13, right=0.87)
        plt.savefig(plot_path + "of_coll_{}_rot.png".format(mode), dpi=dpi)
        plt.close()
        pb.close()

    # ===================================================================================
    # DG EVALUATION
    # ===================================================================================
    idx = df_erf_t["particle"].isin(particles_t) & (df_erf_t["dt"] == 1)
    sig0 = df_erf_t.loc[idx, "sigma0"].to_numpy()
    sig0_err = df_erf_t.loc[idx, "sigma0_err"].to_numpy()
    sig1 = df_erf_t.loc[idx, "sigma1"].to_numpy()
    sig1_err = df_erf_t.loc[idx, "sigma1_err"].to_numpy()
    w0 = df_erf_t.loc[idx, "w0"].to_numpy()
    w0_err = df_erf_t.loc[idx, "w0_err"].to_numpy()
    mu01 = df_erf_t.loc[idx, "drift01"].to_numpy()
    mu01_err = df_erf_t.loc[idx, "drift01_err"].to_numpy()
    sigcomb = np.array([sig0, sig1])
    sigcomb_err = np.array([sig0_err, sig1_err])
    idx = np.argmax(sigcomb, axis=0)
    idx = tuple(np.array([idx, np.arange(0, len(idx))]))
    sigmax = sigcomb[idx]
    sigmax_err = sigcomb_err[idx]
    idx = np.argmin(sigcomb, axis=0)
    idx = tuple(np.array([idx, np.arange(0, len(idx))]))
    sigmin = sigcomb[idx]
    sigmin_err = sigcomb_err[idx]
    wmax = np.array([w0[i] if sig0[i] > sig1[i] else 1 - w0[i] for i in range(len(sig0))])
    wmin = np.array([w0[i] if sig0[i] <= sig1[i] else 1 - w0[i] for i in range(len(sig0))])
    # sig_h = np.array([sig0[i] if w0[i] > 0.5 else sig1[i] for i in range(len(sig0))])
    # sig_l = np.array([sig0[i] if w0[i] <= 0.5 else sig1[i] for i in range(len(sig0))])
    # amp_h = np.array([w0[i] if w0[i] > 0.5 else 1 - w0[i] for i in range(len(sig0))])
    # amp_l = np.array([w0[i] if w0[i] <= 0.5 else 1 - w0[i] for i in range(len(sig0))])

    # ===========================
    # MinMax
    _, ax = plt.subplots()
    ax.errorbar(wmax, sigmax, color="black", fmt="^", markersize=3)
    ax.errorbar(wmin, sigmin, color="gray", fmt="v", markersize=3)
    ax.set_yscale("log")
    ax.set_ylim(1e-8, 1e-4)
    ax.set_xlim(0, 1)
    ax.set_xlabel("Amplitude")
    ax.set_ylabel(r"$\sigma_{x, y}, w_{x, y}$")
    icaps.mirror_axis(ax)
    plt.savefig(plot_path + "dg_eval.png", dpi=dpi)
    plt.close()

    # ===========================
    # MinMax Split
    fig, ax = plt.subplots(2)
    ax[0].errorbar(wmax, sigmax, color="black", fmt="^", markersize=3)
    ax[1].errorbar(wmin, sigmin, color="black", fmt="^", markersize=3)
    for i in range(2):
        ax[i].set_yscale("log")
        ax[i].set_ylim(1e-8, 1e-4)
        ax[i].set_xlim(0, 1)
        ax[i].set_yticks([1e-7, 1e-6, 1e-5])
        # icaps.mirror_axis(ax, "y")
    fig.text(0.01, 0.5, r"$\sigma_{x, y}, w_{x, y}$", va="center", rotation=+90.)
    fig.subplots_adjust(hspace=.001, wspace=.001)
    ax[0].set_xticks([])
    ax[0].xaxis.tick_top()
    ax[1].set_xlabel("Amplitude")
    plt.savefig(plot_path + "dg_eval_split.png", dpi=dpi)
    plt.close()

    # ===========================
    # Wide VS Narrow Gaussian (Lin)
    _, ax = plt.subplots()
    ax.errorbar(sigmax, sigmin, c="black", fmt="o", markersize=3)
    ax.set_xlabel(r"$\sigma_{x, y}$")
    ax.set_ylabel(r"$w_{x, y}$")
    ax.set_ylim(-1e-7, 3e-6)
    ax.set_xlim(-1e-7, 3e-6)
    icaps.mirror_axis(ax)
    plt.savefig(plot_path + "dg_eval_lin.png", dpi=dpi)
    plt.close()

    # ===========================
    # Wide VS Narrow Gaussian (Log)
    _, ax = plt.subplots()
    ax.errorbar(sigmax, sigmin, c="lightgray", fmt="o", markersize=3)
    idx = (wmax < 0.9) & (wmin < 0.9) & (wmax >= 0.1) & (wmin >= 0.1)
    ax.errorbar(sigmax[idx], sigmin[idx], c="gray", fmt="o", markersize=3)
    idx = idx & (((wmax < 0.4) | (wmax >= 0.6)) & ((wmin < 0.4) | (wmin >= 0.6)))
    ax.errorbar(sigmax[idx], sigmin[idx], c="black", fmt="o", markersize=3)
    ax.set_xlabel(r"$\sigma_{x, y}$")
    ax.set_ylabel(r"$w_{x, y}$")
    ax.set_yscale("log")
    ax.set_ylim(1e-8, 1e-5)
    ax.set_xscale("log")
    ax.set_xlim(1e-8, 1e-5)
    icaps.mirror_axis(ax)
    plt.savefig(plot_path + "dg_eval_log.png", dpi=dpi)
    plt.close()

    # ===========================
    # Narrow/Wide VS Amplitude
    fig, ax = plt.subplots()
    ax.errorbar(wmax, sigmax / sigmin, c="lightgray", fmt="o", markersize=3, label="Unfiltered")
    idx = (wmax < 0.9) & (wmin < 0.9) & (wmax >= 0.1) & (wmin >= 0.1)
    # ax.errorbar(sigmax[idx], sigmin[idx], c="gray", fmt="o", markersize=3)
    # idx = idx & (((wmax < 0.4) | (wmax >= 0.6)) & ((wmin < 0.4) | (wmin >= 0.6)))
    # ax.errorbar(sigmax[idx], sigmin[idx], c="black", fmt="o", markersize=3)
    idx = idx & (sigmax_err / sigmax < .1) & (sigmin_err / sigmin < .1)
    # label = r"$(0.1 < a \le 0.9) \land (0.1 < 1 - a \le 0.9) \land (\frac{\Delta \sigma_{x, y}}{\sigma_{x, y}} < 0.1) "\
    #         r"\land (\frac{\Delta w_{x, y}}{w_{x, y}} < 0.1)$"
    ax.errorbar(wmax[idx], sigmax[idx] / sigmin[idx], c="black", fmt="o", markersize=3,
                label="Filtered")
    ax.set_xlabel(r"$a_{x, y}$")
    ax.set_ylabel(r"$w_{x, y} / \sigma_{x, y}$")
    # ax.set_yscale("log")
    # ax.set_ylim(1e-1, 1e0)
    # ax.set_ylim(-0.05, 1.05)
    ax.set_ylim(0.7, 10)
    # ax.set_xscale("log")
    # ax.set_xlim(5e-8, 5e-6)
    ax.set_xlim(-0.05, 1.05)
    icaps.mirror_axis(ax)
    # ax.legend(loc="upper right", fancybox=False)
    fig.subplots_adjust(bottom=0.12)
    plt.savefig(plot_path + "dg_eval_ratio.png", dpi=dpi)
    plt.close()

    # ===========================
    # Distribution
    _, ax = plt.subplots()
    idx = (sigmin < 1) & (sigmin > 0) & (sigmax < 1) & (sigmax > 0)
    x = np.sort(sigmin[idx])
    x_norm = np.linspace(1 / len(x), 1, len(x))
    ax.scatter(x, x_norm, c="black", marker="o", s=3)
    plt.savefig(plot_path + "dg_eval_dist.png", dpi=dpi)
    plt.close()

    # ===========================
    # Measurement VS Simulation
    bands = True
    s2 = 0.9545
    ms = 3
    df_eval = pd.DataFrame(data={
        "particle": np.repeat(particles_t, 2),
        "axis": np.tile(["x", "y"], len(particles_t)),
        "mass": 0.0,
        "sigmin": 0.0,
        "sigmax": 0.0
    })
    for particle in particles_t:
        for axis in ["x", "y"]:
            mass = df_of_t.loc[df_of_t["particle"] == particle, f"mass_{axis}"].to_numpy()[0]
            idx_eval = (df_eval["particle"] == particle) & (df_eval["axis"] == axis)
            df_eval.loc[idx_eval, "mass"] = mass
            idx_erf = (df_erf_t["particle"] == particle) & (df_erf_t["axis"] == axis)
            sig0 = df_erf_t.loc[idx_erf, "sigma0"].to_numpy()[0]
            sig1 = df_erf_t.loc[idx_erf, "sigma1"].to_numpy()[0]
            df_eval.loc[idx_eval, "sigmin"] = np.min([sig0, sig1])
            df_eval.loc[idx_eval, "sigmax"] = np.max([sig0, sig1])
    df_eval["n"] = df_eval["mass"] / icaps.const.m_0
    df_eval = df_eval[(df_eval["sigmin"] > 0) & (df_eval["sigmax"] > 0) &
                      (df_eval["sigmin"] < 1) & (df_eval["sigmax"] < 1)]
    bin_exp = np.arange(0, int(np.ceil(np.log2(np.max(df_eval["n"])))) + 1)
    bins = 2 ** (bin_exp - 0.5)
    bin_centers = 2 ** bin_exp[:-1]
    df_eval["bin"] = np.digitize(df_eval["n"], bins=bins)
    medians = np.zeros_like(bin_centers, dtype=float)
    medians[:] = np.nan
    quant95 = np.zeros_like(bin_centers, dtype=float)
    quant95[:] = np.nan
    maxs = np.zeros_like(bin_centers, dtype=float)
    maxs[:] = np.nan
    for i, bin_center in enumerate(bin_centers):
        # n = df_eval.loc[df_eval["bin"] == i, "n"].to_numpy()
        sigminsqr = df_eval.loc[df_eval["bin"] == i, "sigmin"].to_numpy() ** 2
        sigminsqr = np.sort(sigminsqr)
        if len(sigminsqr) == 0:
            continue
        medians[i] = np.quantile(sigminsqr, 0.5)
        quant95[i] = np.quantile(sigminsqr, s2)
        maxs[i] = np.max(sigminsqr)
    fig, ax = plt.subplots()
    # ax.scatter(bin_centers, maxs, marker="^", s=10, label="Maximum", facecolor="none", edgecolor="black")
    if bands:
        plt.rcParams["hatch.linewidth"] = 2.0
        ax.fill_between(bin_centers, medians, quant95,
                        ec="black", fc="gray", alpha=1, label="Observation", lw=2)
    else:
        ax.scatter(bin_centers, quant95, marker="D", s=int(round(ms * 10)),
                   label=r"$2\sigma$ (obs.)", facecolor="none", edgecolor="black")
        ax.scatter(bin_centers, medians, marker="o", s=int(round(ms * 10)),
                   label="Median (obs.)", facecolor="none", edgecolor="black")

    df_thilo = pd.read_csv(project_path + "thilo/raytracing plots/data_plot/data.csv")
    cols = df_thilo.columns
    df_thilo.rename(columns={cols[0]: "gen", cols[1]: "median", cols[2]: "sigma", cols[3]: "2sigma",
                             cols[4]: "max"}, inplace=True)
    df_thilo["gen"] = df_thilo["gen"].astype(int)
    if bands:
        ax.fill_between(2 ** df_thilo["gen"], df_thilo["median"] * 1e-12, df_thilo["2sigma"] * 1e-12,
                        ec="red", fc="none", hatch="//", alpha=1, label="Simulation", lw=2)
    else:
        ax.scatter(2 ** df_thilo["gen"], df_thilo["2sigma"] * 1e-12,
                   marker="D", s=int(round(ms * 10)), label=r"$2\sigma$ (sim.)", c="black")
        ax.scatter(2 ** df_thilo["gen"], df_thilo["median"] * 1e-12,
                   marker="o", s=int(round(ms * 10)), label="Median (sim.)", c="black")
    ax.legend(loc="upper right", fancybox=False)
    ax.set_xlim(0.8, 3e3)
    ax.set_xscale("log")
    if bands:
        ax.set_ylim(1e-15, 1e-11)
    else:
        ax.set_ylim(1e-15, 1e-10)
    ax.set_yscale("log")
    ax.set_xlabel(r"$m \, (m_0)$")
    ax.set_ylabel(r"$\langle\Delta x^2\rangle, \langle\Delta y^2\rangle \,\rm (m^2)$")
    icaps.mirror_axis(ax)
    fig.subplots_adjust(left=0.15, right=0.95)
    fig.subplots_adjust(bottom=0.15, top=0.95)
    plt.savefig(plot_path + "dg_eval_mass.png", dpi=dpi)
    plt.close()
    if bands:
        plt.rcParams["hatch.linewidth"] = 1.0

    # # ===================================================================================
    # # INVESTIGATE OFs
    # # ===================================================================================
    # dts_ofinvest = [1]
    # df = pd.DataFrame(data={
    #     "particle": np.repeat(particles_t, 2 * len(dts_ofinvest)),
    #     "axis": np.tile(["x", "y"], len(particles_t) * len(dts_ofinvest)),
    #     "dt": np.tile(np.repeat(dts_ofinvest, 2), len(particles_t)),
    #     "sigma": 0.0,
    #     "sigmin": 0.0,
    #     "sigmax": 0.0,
    #     "fit_value": 0.0,
    #     "mass": 0.0,
    #     "tau": 0.0
    # })
    # pb = icaps.SimpleProgressBar(len(particles_t) * 2 * len(dts_ofinvest), "Building OF Invest. Table")
    # for particle in particles_t:
    #     for axis in ["x", "y"]:
    #         mass = df_of_t.loc[df_of_t["particle"] == particle, f"mass_{axis}"].to_numpy()[0]
    #         tau = df_of_t.loc[df_of_t["particle"] == particle, f"tau_{axis}"].to_numpy()[0]
    #         for dt in dts_ofinvest:
    #             idx = (df["particle"] == particle) & (df["axis"] == axis) & (df["dt"] == dt)
    #             idx2 = (df_erf_t["particle"] == particle) & (df_erf_t["axis"] == axis) &\
    #                    (df_erf_t["dt"] == dt)
    #             sig0 = df_erf_t.loc[idx2, "sigma0"].to_numpy()[0]
    #             sig1 = df_erf_t.loc[idx2, "sigma1"].to_numpy()[0]
    #             df.loc[idx, "sigma"] = df_erf_t.loc[idx2, "sigma_diff"].to_numpy()[0]
    #             df.loc[idx, "sigmin"] = np.min([sig0, sig1])
    #             df.loc[idx, "sigmax"] = np.max([sig0, sig1])
    #             df.loc[idx, "fit_value"] = 10 ** icaps.fit_of_log(dt * 1e-3, mass, tau)
    #             df.loc[idx, "mass"] = mass
    #             df.loc[idx, "tau"] = tau
    #             pb.tick()
    # pb.close()
    # _, ax = plt.subplots()
    # ax.scatter(df.loc[df["dt"] == 1, "sigma"] ** 2, df.loc[df["dt"] == 1, "fit_value"], c="black", s=2, marker="o")
    # ax.set_xlabel(r"$\sigma_{\rm obs} \,\rm (m^2)$")
    # ax.set_ylabel(r"$\sigma_{\rm fit} \,\rm (m^2)$")
    # # ax.set_xlim(-0.05e-12, 2e-12)
    # ax.set_xlim(1e-15, 3e-12)
    # ax.set_ylim(*ax.get_xlim())
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    # ax.plot(ax.get_xlim(), ax.get_xlim(), c="gray", ls="--", lw=1, zorder=2)
    #
    # _, ax = plt.subplots()
    # ax.scatter(df.loc[df["dt"] == 1, "sigmin"] ** 2, df.loc[df["dt"] == 1, "fit_value"], c="black", s=2, marker="o")
    # ax.set_xlabel(r"$\sigma_{\rm obs} \,\rm (m^2)$")
    # ax.set_ylabel(r"$\sigma_{\rm fit} \,\rm (m^2)$")
    # # ax.set_xlim(-0.05e-12, 2e-12)
    # ax.set_xlim(1e-15, 3e-12)
    # ax.set_ylim(*ax.get_xlim())
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    # ax.plot(ax.get_xlim(), ax.get_xlim(), c="gray", ls="--", lw=1, zorder=2)
    # plt.show()

    """
    # ===================================================================================
    # INVESTIGATE ERFs
    # ===================================================================================
    mode = "SR"
    pb = icaps.SimpleProgressBar(len(particles_t), "Progress")
    for particle in particles_t:
        pb.set_stage(particle)
        dx_label = "d{}_{}".format("x", 1)
        df_erf_ = df_erf_t[(df_erf_t["dt"] == 1) & (df_erf_t["axis"] == "x")]
        dx = df_disp_t.loc[(df_disp_t["particle"] == particle), dx_label].to_numpy()
        drift = df_erf_.loc[df_erf_["particle"] == particle, m[mode][0]].to_numpy()[0] * 1e6
        sigma = df_erf_.loc[df_erf_["particle"] == particle, m[mode][1]].to_numpy()[0] * 1e6
        xi_ = (dx - drift) / sigma
        x = np.sort(xi_)
        y = np.linspace(1 / len(x), 1, len(x))
        if sigma > 100:
            print("\t", sigma)
            fig, ax = plt.subplots()
            fig.canvas.manager.window.wm_geometry("+%d+%d" % (50, 50))
            ax.set_xlim(-5, 5)
            ax.errorbar(x, y, c="gray", markersize=1, fmt=",")
            plt.show()
        pb.tick()
    pb.close()
    """

    # ===================================================================================
    # COLLECTIVE ERF
    # ===================================================================================
    # # Trans
    # dts_ = (1, 5)
    # xlim = 5
    # modes = [
    #     # "SG",
    #     "SR"
    # ]
    # alpha = 1
    # xspace = np.linspace(-10, 10, 1000)
    # for mode in modes:
    #     pb = icaps.SimpleProgressBar(len(axes) * len(dts_) * len(particles_t), "Collective ERF Trans {}".format(mode))
    #     for axis in axes:
    #         outs = np.zeros((len(dts_), 4))
    #         x_alls = []
    #         fig, ax = plt.subplots(nrows=3, ncols=2,
    #                                gridspec_kw={"height_ratios": [2, 1, 1]})
    #         for j, dt_ in enumerate(dts_):
    #             dx_label = "d{}_{}".format(axis, dt_)
    #             df_disp_ = df_disp_t[["particle", dx_label]].dropna()
    #             df_erf_ = df_erf_t[(df_erf_t["dt"] == dt_) & (df_erf_t["axis"] == axis)]
    #             x_all = np.array([], dtype=float)
    #             y_all = np.array([], dtype=float)
    #             for particle in particles_t:
    #                 dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy() * icaps.const.px_ldm
    #                 drift = df_erf_.loc[df_erf_["particle"] == particle, m[mode][0]].to_numpy()[0] * 1e6
    #                 sigma = df_erf_.loc[df_erf_["particle"] == particle, m[mode][1]].to_numpy()[0] * 1e6
    #                 if sigma > 1e3:
    #                     continue
    #                 xi_ = (dx - drift) / sigma
    #                 x = np.sort(xi_)
    #                 y = np.linspace(1 / len(x), 1, len(x))
    #                 x_all = np.append(x_all, x)
    #                 y_all = np.append(y_all, y)
    #                 pb.tick()
    #             x_alls.append(x_all)
    #             ax[0][j].errorbar(x_all, y_all, markersize=1, c="gray", alpha=alpha, zorder=0, fmt=",")
    #             ax[0][j].plot(xspace, icaps.fit_erf(xspace, 0, 1),
    #                           c="red", alpha=1, lw=1, ls="-", zorder=10,
    #                           label=r"$\Phi$")
    #             out = icaps.fit_model(icaps.fit_erf_dg, x_all, y_all)
    #             outs[j] = out.beta
    #             w0, mu01, sig0, sig1 = out.beta
    #             if sig0 > sig1:
    #                 w0 = 1 - w0
    #                 sig0, sig1 = sig1, sig0
    #             label = icaps.get_param_label([
    #                 [r"$a_{{{}}}$".format(axis), 1 - w0],
    #                 [r"$\sigma_{{{}}}$".format(axis), sig0],
    #                 [r"$w_{{{}}}$".format(axis), sig1]
    #             ])
    #             ax[0][j].plot(xspace, icaps.fit_erf_dg(xspace, *out.beta),
    #                           c="blue", alpha=1, lw=1, ls="--", zorder=11,
    #                           label=r"$\Phi_2$")
    #             res_sg = y_all - icaps.fit_erf(x_all, 0, 1)
    #             rss_sg = np.sum(res_sg ** 2) / len(res_sg)
    #             res_dg = y_all - icaps.fit_erf_dg(x_all, *out.beta)
    #             rss_dg = np.sum(res_dg ** 2) / len(res_dg)
    #             ax[1][j].errorbar(x_all, res_sg, c="gray", markersize=1, fmt=",", alpha=alpha, zorder=0,
    #                               label=icaps.get_param_label([
    #                                  [r"$\Phi$"],
    #                                  [r"$\mathrm{RSS}$", rss_sg * 1e4, r"$\cdot 10^{-4}$"]
    #                               ]))
    #             ax[2][j].errorbar(x_all, res_dg, c="gray", markersize=1, fmt=",", alpha=alpha, zorder=1,
    #                               label=icaps.get_param_label([
    #                                  [r"$\Phi_2$"],
    #                                  [r"$\mathrm{RSS}$", rss_dg * 1e4, r"$\cdot 10^{-4}$"]
    #                               ]))
    #             if j == 0:
    #                 ax[0][j].legend(ncol=2, loc=(0.0, 1.02), fontsize=15, fancybox=False)
    #             for i in range(1, 3):
    #                 ax[i][j].axhline(0, c="black", lw=1, ls="--", zorder=10, alpha=0.5)
    #             ax[0][j].annotate(r"$\Delta t$ = {} ms".format(dt_), xy=(0.029, 0.96), xycoords="axes fraction",
    #                               size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))
    #             ax[0][j].annotate(label, xy=(0.7, 0.27), xycoords="axes fraction",
    #                               size=12, ha="left", va="top")
    #             ax[1][j].annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_sg * 1e4),
    #                               xy=(0.525, 0.2), xycoords="axes fraction",
    #                               size=12, ha="left", va="top")
    #             ax[2][j].annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_dg * 1e4),
    #                               xy=(0.525, 0.2), xycoords="axes fraction",
    #                               size=12, ha="left", va="top")
    #             ax[1][j].annotate(r"$\Phi$",
    #                               xy=(0.02, 0.95), xycoords="axes fraction",
    #                               size=15, ha="left", va="top")
    #             ax[2][j].annotate(r"$\Phi_2$",
    #                               xy=(0.02, 0.95), xycoords="axes fraction",
    #                               size=15, ha="left", va="top")
    #         ax[0][1].set_yticks([])
    #         for j in range(len(dts_)):
    #             ax[0][j].set_xticks([])
    #             ax[0][j].set_xlim(-xlim, xlim)
    #             ax[0][j].set_ylim(-0.05, 1.05)
    #             ax[0][j].minorticks_on()
    #         for i in range(1, 3):
    #             ax[i][0].set_yticks([])
    #             ax[i][1].set_yticks([-0.05, 0, 0.05])
    #             for j in range(len(dts_)):
    #                 ax[i][j].set_ylim(-0.10, 0.10)
    #                 ax[i][j].minorticks_on()
    #                 ax[i][j].set_xticks([-4, -2, 0, 2, 4])
    #                 ax[i][j].set_xlim(-xlim, xlim)
    #             ax[i][1].yaxis.tick_right()
    #             ax[i][1].yaxis.set_label_position("right")
    #         fig.text(0.005, 0.67, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    #         fig.text(0.97, 0.3, "Residual", va="center", rotation=+90.)
    #         fig.text(0.50, 0.02, r"$\Delta\xi_{{\mathrm{{G,}}{}}}$".format(axis), ha="center")
    #         fig.subplots_adjust(hspace=.001, wspace=.001)
    #         fig.subplots_adjust(left=0.11, right=0.87)
    #         plt.savefig(plot_path + "erf_coll_{}_{}.png".format(mode, axis), dpi=dpi)
    #         plt.close()
    #
    #         fig, ax = plt.subplots(nrows=2)
    #         for j, dt_ in enumerate(dts_):
    #             w0, mu01, sig0, sig1 = outs[j]
    #             if sig0 > sig1:
    #                 w0 = 1 - w0
    #                 sig0, sig1 = sig1, sig0
    #             bin_size = 0.25
    #             bin_lim = np.ceil(np.max(np.abs(x_alls[j])) * 100) / 100
    #             bins = np.arange(bin_size / 2, bin_lim + bin_size, bin_size)
    #             bins = np.append(-bins[::-1], bins)
    #             hist, _ = np.histogram(x_alls[j], bins=bins, density=True)
    #             ax[j].bar(bins[:-1] + bin_size / 2, hist,
    #                       width=bin_size * 0.9, color="lightgray")
    #             ax[j].plot(xspace, np.exp(- xspace ** 2 / 2) / np.sqrt(2 * np.pi),
    #                        c="red", ls="-", lw=1, zorder=10, label=r"$\Phi$")
    #             ax[j].plot(xspace,
    #                        w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)) +
    #                        (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
    #                        c="blue", ls="--", lw=1, zorder=11, label=r"$\Phi_2$")
    #             ax[j].plot(xspace, w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)),
    #                        c="magenta", ls=":", lw=1, zorder=12,
    #                        label=r"$(1 - a_{{{}}}) \cdot \Phi(\sigma_{{{}}})$".format(axis, axis))
    #             ax[j].plot(xspace, (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
    #                        c="green", ls="-.", lw=1, zorder=13,
    #                        label=r"$a_{{{}}} \cdot \Phi(w_{{{}}})$".format(axis, axis))
    #             ax[j].set_xlim(-xlim, xlim)
    #             ax[j].set_ylim(0.0, 0.5)
    #             ax[j].minorticks_on()
    #             ax[j].annotate(r"$\Delta t$ = {} ms".format(dt_), xy=(0.013, 0.96), xycoords="axes fraction",
    #                            size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))
    #         icaps.mirror_axis(ax[0], axis="both")
    #         icaps.mirror_axis(ax[1], axis="y")
    #         ax[0].set_xticks([])
    #         ax[0].legend(loc=(0.0, 1.037), fancybox=False, ncol=2)
    #         ax[1].set_xlabel(r"$\Delta\xi_{{\mathrm{{G,}}{}}}$".format(axis))
    #         fig.subplots_adjust(hspace=.001, wspace=.001)
    #         fig.subplots_adjust(left=0.11, right=0.87)
    #         fig.text(0.005, 0.5, "Normalized Frequency", va="center", rotation="vertical")
    #         fig.subplots_adjust(bottom=0.11, top=0.83)
    #         plt.savefig(plot_path + f"bell_coll_{mode}_{axis}.png", dpi=dpi)
    #     pb.close()
    #
    # # ===========================
    # # Rot
    # dts_ = (1, 17)
    # xlim = 5
    # modes = [
    #     # "SG",
    #     "SR"
    # ]
    # alpha = 1
    # xspace = np.linspace(-10, 10, 1000)
    # for mode in modes:
    #     pb = icaps.SimpleProgressBar(len(dts_) * len(particles_r),
    #                                  "Collective ERF Rot {}".format(mode))
    #     outs = np.zeros((len(dts_), 4))
    #     x_alls = []
    #     fig, ax = plt.subplots(nrows=3, ncols=2,
    #                            gridspec_kw={"height_ratios": [2, 1, 1]})
    #     for j, dt_ in enumerate(dts_):
    #         dx_label = "omega_{}".format(dt_)
    #         df_disp_ = df_disp_r[["particle", dx_label]].dropna()
    #         df_erf_ = df_erf_r[(df_erf_r["dt"] == dt_) & (df_erf_r["axis"] == "rot")]
    #         x_all = np.array([], dtype=float)
    #         y_all = np.array([], dtype=float)
    #         for particle in particles_r:
    #             dx = df_disp_.loc[df_disp_["particle"] == particle, dx_label].to_numpy()
    #             drift = df_erf_.loc[df_erf_["particle"] == particle, m[mode][0]].to_numpy()[0]
    #             sigma = df_erf_.loc[df_erf_["particle"] == particle, m[mode][1]].to_numpy()[0]
    #             if sigma > 1:
    #                 continue
    #             if np.isnan(sigma) or np.isnan(drift):
    #                 continue
    #             xi_ = (dx - drift) / sigma
    #             x = np.sort(xi_)
    #             y = np.linspace(1 / len(x), 1, len(x))
    #             x_all = np.append(x_all, x)
    #             y_all = np.append(y_all, y)
    #             pb.tick()
    #         x_alls.append(x_all)
    #         ax[0][j].errorbar(x_all, y_all, markersize=1, mfc="gray", mec="none",
    #                           alpha=alpha, zorder=0, fmt=".")
    #         ax[0][j].plot(xspace, icaps.fit_erf(xspace, 0, 1),
    #                       c="red", alpha=1, lw=1, ls="-", zorder=10,
    #                       label=r"$\Phi$")
    #         out = icaps.fit_model(icaps.fit_erf_dg, x_all, y_all, p0=[0.8, 0, 1, 1])
    #         outs[j] = out.beta
    #         w0, mu01, sig0, sig1 = out.beta
    #         if sig0 > sig1:
    #             w0 = 1 - w0
    #             sig0, sig1 = sig1, sig0
    #         label = icaps.get_param_label([
    #             [r"$a_\theta$", 1 - w0],
    #             [r"$\sigma_\theta$", sig0],
    #             [r"$w_\theta$", sig1]
    #         ])
    #         ax[0][j].plot(xspace, icaps.fit_erf_dg(xspace, *out.beta),
    #                       c="blue", alpha=1, lw=1, ls="--", zorder=11,
    #                       label=r"$\Phi_2$")
    #         res_sg = y_all - icaps.fit_erf(x_all, 0, 1)
    #         rss_sg = np.sum(res_sg ** 2) / len(res_sg)
    #         res_dg = y_all - icaps.fit_erf_dg(x_all, *out.beta)
    #         rss_dg = np.sum(res_dg ** 2) / len(res_dg)
    #         ax[1][j].errorbar(x_all, res_sg, mfc="gray", mec="none", markersize=1,
    #                           fmt=".", alpha=alpha, zorder=0,
    #                           label=icaps.get_param_label([
    #                               [r"$\Phi$"],
    #                               [r"$\mathrm{RSS}$", rss_sg * 1e4, r"$\cdot 10^{-4}$"]
    #                           ]))
    #         ax[2][j].errorbar(x_all, res_dg, mfc="gray", mec="none", markersize=1,
    #                           fmt=".", alpha=alpha, zorder=1,
    #                           label=icaps.get_param_label([
    #                               [r"$\Phi_2$"],
    #                               [r"$\mathrm{RSS}$", rss_dg * 1e4, r"$\cdot 10^{-4}$"]
    #                           ]))
    #         if j == 0:
    #             ax[0][j].legend(ncol=2, loc=(0.0, 1.02), fontsize=15, fancybox=False)
    #         for i in range(1, 3):
    #             ax[i][j].axhline(0, c="black", lw=1, ls="--", zorder=10, alpha=0.5)
    #         ax[0][j].annotate(r"$\Delta t$ = {} ms".format(dt_), xy=(0.029, 0.96), xycoords="axes fraction",
    #                           size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))
    #         ax[0][j].annotate(label, xy=(0.7, 0.27), xycoords="axes fraction",
    #                           size=12, ha="left", va="top")
    #         ax[1][j].annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_sg * 1e4),
    #                           xy=(0.525, 0.2), xycoords="axes fraction",
    #                           size=12, ha="left", va="top")
    #         ax[2][j].annotate(r"$\mathrm{{RSS}} = {{{:.2f}}} \cdot 10^{{-4}}$".format(rss_dg * 1e4),
    #                           xy=(0.525, 0.2), xycoords="axes fraction",
    #                           size=12, ha="left", va="top")
    #         ax[1][j].annotate(r"$\Phi$",
    #                           xy=(0.02, 0.95), xycoords="axes fraction",
    #                           size=15, ha="left", va="top")
    #         ax[2][j].annotate(r"$\Phi_2$",
    #                           xy=(0.02, 0.95), xycoords="axes fraction",
    #                           size=15, ha="left", va="top")
    #     ax[0][1].set_yticks([])
    #     for j in range(len(dts_)):
    #         ax[0][j].set_xticks([])
    #         ax[0][j].set_xlim(-xlim, xlim)
    #         ax[0][j].set_ylim(-0.05, 1.05)
    #         ax[0][j].minorticks_on()
    #     for i in range(1, 3):
    #         ax[i][0].set_yticks([])
    #         ax[i][1].set_yticks([-0.05, 0, 0.05])
    #         for j in range(len(dts_)):
    #             ax[i][j].set_ylim(-0.10, 0.10)
    #             ax[i][j].minorticks_on()
    #             ax[i][j].set_xticks([-4, -2, 0, 2, 4])
    #             ax[i][j].set_xlim(-xlim, xlim)
    #         ax[i][1].yaxis.tick_right()
    #         ax[i][1].yaxis.set_label_position("right")
    #     fig.text(0.005, 0.67, "Normalized Cumulative Frequency", va="center", rotation="vertical")
    #     fig.text(0.97, 0.3, "Residual", va="center", rotation=+90.)
    #     fig.text(0.50, 0.02, r"$\Delta\xi_{{\mathrm{{G,}}\theta}}$", ha="center")
    #     fig.subplots_adjust(hspace=.001, wspace=.001)
    #     fig.subplots_adjust(left=0.11, right=0.87)
    #     plt.savefig(plot_path + "erf_coll_{}_{}.png".format(mode, "rot"), dpi=dpi)
    #     plt.close()
    #
    #     fig, ax = plt.subplots(nrows=2)
    #     for j, dt_ in enumerate(dts_):
    #         w0, mu01, sig0, sig1 = outs[j]
    #         if sig0 > sig1:
    #             w0 = 1 - w0
    #             sig0, sig1 = sig1, sig0
    #         bin_size = 0.25
    #         bin_lim = np.ceil(np.max(np.abs(x_alls[j])) * 100) / 100
    #         bins = np.arange(bin_size / 2, bin_lim + bin_size, bin_size)
    #         bins = np.append(-bins[::-1], bins)
    #         hist, _ = np.histogram(x_alls[j], bins=bins, density=True)
    #         ax[j].bar(bins[:-1] + bin_size / 2, hist,
    #                   width=bin_size * 0.9, color="lightgray")
    #         ax[j].plot(xspace, np.exp(- xspace ** 2 / 2) / np.sqrt(2 * np.pi),
    #                    c="red", ls="-", lw=1, zorder=10, label=r"$\Phi$")
    #         ax[j].plot(xspace,
    #                    w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)) +
    #                    (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
    #                    c="blue", ls="--", lw=1, zorder=11, label=r"$\Phi_2$")
    #         ax[j].plot(xspace, w0 / np.sqrt(2 * np.pi * sig0 ** 2) * np.exp(- xspace ** 2 / (2 * sig0 ** 2)),
    #                    c="magenta", ls=":", lw=1, zorder=12,
    #                    label=r"$(1 - a_\theta) \cdot \Phi(\sigma_\theta)$")
    #         ax[j].plot(xspace,
    #                    (1 - w0) / np.sqrt(2 * np.pi * sig1 ** 2) * np.exp(- xspace ** 2 / (2 * sig1 ** 2)),
    #                    c="green", ls="-.", lw=1, zorder=13,
    #                    label=r"$a_\theta \cdot \Phi(w_\theta)$")
    #         ax[j].set_xlim(-xlim, xlim)
    #         ax[j].set_ylim(0.0, 0.5)
    #         ax[j].minorticks_on()
    #         ax[j].annotate(r"$\Delta t$ = {} ms".format(dt_), xy=(0.013, 0.96), xycoords="axes fraction",
    #                        size=15, ha="left", va="top", bbox=dict(boxstyle="square", fc="w"))
    #     icaps.mirror_axis(ax[0], axis="both")
    #     icaps.mirror_axis(ax[1], axis="y")
    #     ax[0].set_xticks([])
    #     ax[0].legend(loc=(0.0, 1.037), fancybox=False, ncol=2)
    #     ax[1].set_xlabel(r"$\Delta\xi_{{\mathrm{{G,}}\theta}}$")
    #     fig.subplots_adjust(hspace=.001, wspace=.001)
    #     fig.subplots_adjust(left=0.11, right=0.87)
    #     fig.text(0.005, 0.5, "Normalized Frequency", va="center", rotation="vertical")
    #     fig.subplots_adjust(bottom=0.11, top=0.83)
    #     plt.savefig(plot_path + f"bell_coll_{mode}_rot.png", dpi=dpi)
    #     pb.close()


if __name__ == '__main__':
    main()

