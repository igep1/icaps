import os

from deprecation import deprecated

import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

fancybox = False
dpi = 600
min_dt_tau_ratio = 2
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))

norm_rel_err_trans = 0.2
norm_rel_err_rot = 0.5
norm_chisqr_trans = 50e-4
norm_chisqr_rot = 60e-4
range_rel_err_trans = np.around(np.arange(5, 105, 5) * 1e-2, 2)
range_chisqr_trans = np.around(np.arange(5, 105, 5) * 1e-4, 4)
range_rel_err_rot = np.around(np.arange(5, 105, 5) * 1e-2, 2)
range_chisqr_rot = np.around(np.arange(5, 105, 5) * 1e-4, 4)
exp_lims = (4.8, 10)

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
csv_of_final = project_path + "OF_Final_noseq_adapt3.csv"
plot_path = project_path + "plots/FilterEval/"
csv_out = plot_path + "log.csv"
icaps.mk_folders([plot_path], clear=False)
files = icaps.get_files(plot_path + "*.png")
for file in files:
    os.remove(plot_path + file)


@deprecated
def parse_filter_key(df, key, axis="x"):
    if key.replace(" ", "") == "":
        return 1
    symbols = ("+", "-", "/", "*")
    inside = [symb in key for symb in symbols]
    if any(inside):
        idx = int(np.argmax(inside))
        symb = symbols[idx]
        keys = key.split(symb)
        series = parse_filter_key(df, keys[0], axis)
        if isinstance(series, int) and (symb == "+"):
            series = 0
        if len(keys) > 1:
            for key in keys[1:]:
                if symb == "+":
                    series += parse_filter_key(df, key, axis)
                elif symb == "-":
                    series -= parse_filter_key(df, key, axis)
                elif symb == "/":
                    series /= parse_filter_key(df, key, axis)
                else:
                    series *= parse_filter_key(df, key, axis)
    elif "{}" in key:
        key = key.format(axis)
        series = df[key]
    else:
        series = df[key]
    return series


@deprecated
def filter_df(df, filters, ret_count=False, id_column="particle", axes=None):
    # todo: does not work properly!!!
    count = {}
    if axes is None:
        axes = ("x", "y")
    for key, value in filters.items():
        opera = key[-2:]
        if opera[1] != "=":
            key = key[:-1]
            opera = opera[1]
        else:
            key = key[:-2]
        n_bracket = key.count("{}")
        if n_bracket > 0:
            series = [parse_filter_key(df, key, axis=axis) for axis in axes]
            count_names = [key.format(*[axis for i in range(n_bracket)]) for axis in axes]
        else:
            series = [parse_filter_key(df, key)]
            count_names = [key]
        for i, series_ in enumerate(series):
            if opera == ">":
                df = df[series_ > value]
            elif opera == "<":
                df = df[series_ < value]
            elif opera == ">=":
                df = df[series_ >= value]
            elif opera == "<=":
                df = df[series_ <= value]
            elif opera == "==":
                df = df[series_ == value]
            elif opera == "!=":
                df = df[series_ != value]
            count[count_names[i]] = len(df[id_column].unique())
    if ret_count:
        return df, count
    return df


def filter_rel_err(df, mass=None, tau=None, mode="trans", ret_count=False, id_column="particle"):
    count = []
    if mode == "trans":
        if mass is not None:
            df = df[df["mass_x_err"] / df["mass_x"] <= mass]
            df = df[df["mass_y_err"] / df["mass_y"] <= mass]
        count.append(len(df[id_column].unique()))
        if tau is not None:
            df = df[df["tau_x_err"] / df["tau_x"] <= tau]
            df = df[df["tau_y_err"] / df["tau_y"] <= tau]
        count.append(len(df[id_column].unique()))
    else:
        if mass is not None:
            df = df[df["inertia_err"] / df["inertia"] <= mass]
        count.append(len(df[id_column].unique()))
        if tau is not None:
            df = df[df["tau_rot_err"] / df["tau_rot"] <= tau]
        count.append(len(df[id_column].unique()))
    if ret_count:
        return df, count
    return df


def filter_chisqr(df, trans=None, rot=None, ret_count=False, id_column="particle"):
    count = []
    if trans is not None:
        df = df[df["chisqr_x"] <= trans]
        df = df[df["chisqr_y"] <= trans]
    count.append(len(df[id_column].unique()))
    if rot is not None:
        df = df[df["chisqr_rot"] <= rot]
    count.append(len(df[id_column].unique()))
    if ret_count:
        return df, tuple(count)
    return df


def log_err(val, err):
    return np.log10(err) / np.log10(val) / np.log(10)


def calculate():
    df = pd.read_csv(csv_of_final)
    df_log = pd.DataFrame(data={
        "mass_rel_err": np.repeat(range_rel_err_trans, len(range_chisqr_trans) * len(range_rel_err_trans)),
        "tau_rel_err": np.tile(np.repeat(range_rel_err_trans, len(range_chisqr_trans)), len(range_rel_err_trans)),
        "chisqr": np.tile(range_chisqr_trans, len(range_rel_err_trans) ** 2)
    })
    # df_log = pd.DataFrame(data={
    #     "rel_err": np.repeat(range_rel_err_trans, len(range_chisqr_trans)),
    #     "chisqr": np.tile(range_chisqr_trans, len(range_rel_err_trans))
    # })
    df_log["n_all"] = len(df["particle"].unique())
    df_log["n_parts_mass_rel_err"] = 0
    df_log["n_parts_tau_rel_err"] = 0
    df_log["n_parts_chi_sqr"] = 0
    df_log["n1"] = 0
    df_log["n2"] = 0
    df_log["n1_low"] = 0
    df_log["n1_exp"] = 0
    df_log["n1_high"] = 0
    df_log["n2_low"] = 0
    df_log["n2_exp"] = 0
    df_log["n2_high"] = 0
    df_log["n2_alt_low"] = 0
    df_log["n2_alt_exp"] = 0
    df_log["n2_alt_high"] = 0
    df_log["tm_slope"] = 0.
    df_log["tm_slope_err"] = 0.
    df_log["tm_intercept"] = 0.
    df_log["tm_intercept_err"] = 0.
    df_log["tm_chisqr"] = 0.
    # todo: ex0 + err => basis for D_opt fit
    df_log["D_opt"] = 0.
    df_log["D_opt_low"] = 0.
    df_log["D_opt_high"] = 0.
    df_log["D_brown"] = 0.
    df_log["D_brown_low"] = 0.
    df_log["D_brown_high"] = 0.
    df_log["D_opt_chisqr"] = 0.
    df_log["D_brown_chisqr"] = 0.
    p1 = icaps.const.pressure_i1
    p2 = icaps.const.pressure_i2

    pb = icaps.ProgressBar(len(range_rel_err_trans)**2 * len(range_chisqr_trans), "Calculating")
    # pb = icaps.ProgressBar(len(range_rel_err_trans) * len(range_chisqr_trans), "Calculating")
    for k, mass_rel_err in enumerate(range_rel_err_trans):
    # for k in [0]:
        df_1, count = filter_rel_err(df, mass=mass_rel_err, tau=None, mode="trans", ret_count=True)
        n_parts_mass_rel_err = count[0]
        l_chisqr = len(range_chisqr_trans)
        l_rel_err = len(range_rel_err_trans)
        l_both = l_chisqr * l_rel_err
        df_log.loc[l_both * k: l_both * (k+1), "n_parts_mass_rel_err"] = n_parts_mass_rel_err
        for j, tau_rel_err in enumerate(range_rel_err_trans):
            df_2, count = filter_rel_err(df_1, mass=None, tau=tau_rel_err, mode="trans", ret_count=True)
            # df_2, count = filter_rel_err(df, mass=tau_rel_err, tau=tau_rel_err, mode="trans", ret_count=True)
            # n_parts_mass_rel_err = count[0]
            # df_log.loc[l_both * k + l_chisqr * j: l_both * k + l_chisqr * (j + 1), "n_parts_mass_rel_err"] = n_parts_mass_rel_err
            n_parts_tau_rel_err = count[1]
            df_log.loc[l_both * k + l_chisqr * j: l_both * k + l_chisqr * (j + 1), "n_parts_tau_rel_err"] = n_parts_tau_rel_err
            for i, chisqr in enumerate(range_chisqr_trans):
                idx = l_both * k + l_chisqr * j + i
                df_3, count = filter_chisqr(df_2, trans=chisqr, ret_count=True)
                df_log.loc[idx, "n_parts_chisqr"] = count[0]
                start_frame = np.tile(df_3["start_frame"], 2)
                tau = np.hstack([df_3["tau_x"].to_numpy(), df_3["tau_y"].to_numpy()]).ravel() * 1e3
                tau_err = np.hstack([df_3["tau_x_err"].to_numpy(), df_3["tau_y_err"].to_numpy()]).ravel() * 1e3
                mass = np.hstack([df_3["mass_x"].to_numpy(), df_3["mass_y"].to_numpy()]).ravel()
                mass_err = np.hstack([df_3["mass_x_err"].to_numpy(), df_3["mass_y_err"].to_numpy()]).ravel()
                tau1 = tau[start_frame < icaps.const.end_ff]
                tau1_err = tau_err[start_frame < icaps.const.end_ff]
                tau2 = tau[start_frame >= icaps.const.end_ff]
                tau2_err = tau_err[start_frame >= icaps.const.end_ff]
                tau2_alt = tau2 * p2 / p1
                n1 = len(tau1)
                n2 = len(tau2)
                df_log.loc[idx, "n1"] = n1
                df_log.loc[idx, "n2"] = n2
                cols = ["1", "2", "2_alt"]
                ns = [n1, n2, n2]
                for q, tau_ in enumerate([tau1, tau2, tau2_alt]):
                    n_low = len(tau_[tau_ < exp_lims[0]])
                    n_high = len(tau_[tau_ >= exp_lims[1]])
                    n_exp = ns[q] - (n_low + n_high)
                    df_log.loc[idx, "n{}_low".format(cols[q])] = n_low
                    df_log.loc[idx, "n{}_exp".format(cols[q])] = n_exp
                    df_log.loc[idx, "n{}_high".format(cols[q])] = n_high
                tau_corr = np.append(tau1, tau2_alt)
                tau_corr_err = np.append(tau1_err, tau2_err)
                out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(tau_corr),
                                      # xerr=np.log10(mass_err) / np.log10(mass) / np.log(10),
                                      # yerr=np.log10(tau_corr_err) / np.log10(tau_corr) / np.log(10)
                                      )
                df_log.loc[idx, "tm_slope"] = out.beta[0]
                df_log.loc[idx, "tm_slope_err"] = out.sd_beta[0]
                df_log.loc[idx, "tm_intersect"] = out.beta[1]
                df_log.loc[idx, "tm_intersect_err"] = out.sd_beta[1]
                chisqr_ = np.sum((np.log10(tau_corr) - icaps.fit_lin(mass, *out.beta)) ** 2) / len(mass)
                df_log.loc[idx, "tm_chisqr"] = chisqr_

                mass_brown = df_3["mass"].to_numpy()
                mass_brown_err = df_3["mass_err"].to_numpy()
                mass_opt = df_3["mean_ex"].to_numpy() / icaps.const.ldm_ex0 * icaps.const.m_0
                mass_opt_err = mass_opt * np.sqrt(
                    (df_3["mean_ex_err"].to_numpy() / df_3["mean_ex"].to_numpy()) ** 2
                    + (icaps.const.m_0_err / icaps.const.m_0) ** 2
                    + (icaps.const.ldm_ex0_err / icaps.const.ldm_ex0) ** 2)
                moi_brown = df_3["inertia"].to_numpy()
                moi_brown_err = df_3["inertia_err"].to_numpy()
                moi_opt = mass_opt * (df_3["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
                moi_opt_err = moi_opt * np.sqrt((mass_opt_err / mass_opt) ** 2
                                                + (2 * df_3["mean_gyrad_err"].to_numpy() /
                                                   df_3["mean_gyrad"].to_numpy()) ** 2)
                out = icaps.fit_model(icaps.fit_lin, np.log10(mass_brown), np.log10(moi_brown),
                                      xerr=log_err(mass_brown, mass_brown_err), yerr=log_err(moi_brown, moi_brown_err)
                                      )
                fracdim = 2 / (out.beta[0] - 1)
                fracdim_low = 2 / (out.beta[0] + out.sd_beta[0] - 1)
                fracdim_high = 2 / (out.beta[0] - out.sd_beta[0] - 1)
                chisqr = np.sum((np.log10(moi_brown) - icaps.fit_lin(mass_brown, *out.beta)) ** 2) / len(mass_brown)
                df_log.loc[idx, "D_brown"] = fracdim
                df_log.loc[idx, "D_brown_low"] = fracdim_low
                df_log.loc[idx, "D_brown_high"] = fracdim_high
                df_log.loc[idx, "D_brown_chisqr"] = chisqr
                out = icaps.fit_model(icaps.fit_lin, np.log10(mass_opt), np.log10(moi_opt),
                                      xerr=log_err(mass_opt, mass_opt_err), yerr=log_err(moi_opt, moi_opt_err)
                                      )
                fracdim = 2 / (out.beta[0] - 1)
                fracdim_low = 2 / (out.beta[0] + out.sd_beta[0] - 1)
                fracdim_high = 2 / (out.beta[0] - out.sd_beta[0] - 1)
                chisqr = np.sum((np.log10(moi_brown) - icaps.fit_lin(mass_brown, *out.beta)) ** 2) / len(mass_brown)
                df_log.loc[idx, "D_opt"] = fracdim
                df_log.loc[idx, "D_opt_low"] = fracdim_low
                df_log.loc[idx, "D_opt_high"] = fracdim_high
                df_log.loc[idx, "D_opt_chisqr"] = chisqr

                pb.tick()
    pb.finish()
    return df_log


def plot(df, m_range, tau_range):
    cols = ["1", "2_alt"]
    ns = ["n1", "n2"]
    max_n = np.max(np.append(df["n1"], df["n2"]))
    # todo: 1 and 2_alt in einen Plot
    pb = icaps.ProgressBar(len(m_range) * len(tau_range) * (len(ns) + 1), "Plotting")
    for k, mass_rel_err in enumerate(m_range):
        df_1 = df[df["mass_rel_err"] == mass_rel_err]
        for j, tau_rel_err in enumerate(tau_range):
            df_2 = df_1[df_1["tau_rel_err"] == tau_rel_err]
            for i, n in enumerate(ns):
                _, ax = plt.subplots()
                ax.set_title(r"$\Delta m/m$" + " = {:0<4}".format(mass_rel_err) +
                             r"$\Delta\tau/\tau$" + " = {:0<4}".format(tau_rel_err))
                ax.set_ylim(-0.02, 1.02)
                ax.set_xlabel(r"$\chi^2$ Threshold")
                ax.set_ylabel("Percentile")
                ax.scatter(df_2["chisqr"], df_2["n{}_high".format(cols[i])] / df_2[n],
                           facecolor="none", edgecolor="black", marker="^", label="high")
                ax.scatter(df_2["chisqr"], df_2["n{}_exp".format(cols[i])] / df_2[n],
                           facecolor="none", edgecolor="black", marker="o", label="expected")
                ax.scatter(df_2["chisqr"], df_2["n{}_low".format(cols[i])] / df_2[n],
                           facecolor="none", edgecolor="black", marker="v", label="low")
                ax.legend(fancybox=fancybox)
                ax_twin = ax.twinx()
                ax_twin.tick_params(axis="y", colors="red")
                ax_twin.yaxis.label.set_color("red")
                ax_twin.spines["right"].set_color("red")
                ax_twin.set_ylabel("n")
                ax_twin.set_ylim(-0.02 * max_n, 1.02 * max_n)
                ax_twin.plot(df_2["chisqr"], df_2[n], c="red", lw=2)
                plt.savefig(plot_path + "perc_{:0<4}_{:0<4}_{}.png".format(mass_rel_err, tau_rel_err, cols[i]), dpi=dpi)
                plt.close()
                pb.tick()

            _, ax = plt.subplots()
            ax.errorbar(df_2["chisqr"], df_2["tm_slope"], yerr=df_2["tm_slope_err"],
                        fmt="o", markerfacecolor="none", color="black")
            ax.set_xlabel(r"$\chi^2$ Threshold")
            ax.set_ylabel(r"$\tau-m$ Slope")
            ax.set_ylim(-0.02, 1.02)
            ax_twin = ax.twinx()
            ax_twin.set_ylim(-1, 11)
            ax_twin.tick_params(axis="y", colors="red")
            ax_twin.yaxis.label.set_color("red")
            ax_twin.spines["right"].set_color("red")
            ax_twin.set_ylabel(r"$\tau-m$ Intersect")
            ax_twin.errorbar(df_2["chisqr"], df_2["tm_intersect"], yerr=df_2["tm_intersect_err"],
                             fmt="x", markerfacecolor="none", color="red")
            plt.savefig(plot_path + "tm_{:0<4}_{:0<4}.png".format(mass_rel_err, tau_rel_err), dpi=dpi)
            plt.close()

            _, ax = plt.subplots()
            ax.errorbar(df_2["chisqr"], df_2["D_opt"],
                        # yerr=[df_2["D_opt_low"], df_2["D_opt_high"]],
                        fmt="o", markerfacecolor="none", color="black")
            ax.set_xlabel(r"$\chi^2$ Threshold")
            ax.set_ylabel(r"$D_{opt}$")
            ax.set_ylim(0.98, 2.52)
            plt.savefig(plot_path + "D_opt_{:0<4}_{:0<4}.png".format(mass_rel_err, tau_rel_err), dpi=dpi)
            plt.close()

            _, ax = plt.subplots()
            ax.errorbar(df_2["chisqr"], df_2["D_brown"],
                        # yerr=[df_2["D_brown_low"], df_2["D_brown_high"]],
                        fmt="o", markerfacecolor="none", color="black")
            ax.set_xlabel(r"$\chi^2$ Threshold")
            ax.set_ylabel(r"$D_{brown}$")
            ax.set_ylim(0.98, 2.52)
            plt.savefig(plot_path + "D_brown_{:0<4}_{:0<4}.png".format(mass_rel_err, tau_rel_err), dpi=dpi)
            plt.close()

            pb.tick()
    pb.finish()


if __name__ == '__main__':
    # df_log = calculate()
    # df_log.to_csv(csv_out, index=False)
    df_log = pd.read_csv(csv_out)
    plot(df_log, m_range=range_rel_err_trans, tau_range=range_rel_err_trans) # todo: crashes
