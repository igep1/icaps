import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
plot_path = project_path + "plots/light_particles/"
csv_final = project_path + "OF_Final.csv"
csv_erf = project_path + "ERF_Trans.csv"
# csv_erf = project_path + "ERF_Trans_Exp_0.csv"
csv_disp = project_path + "DISP_Trans.csv"
dpi = 600
max_chisqr_trans = 40e-4
max_rel_err_trans_mass = 0.1
icaps.mk_folders(plot_path)
icaps.mk_folders(plot_path + "parts/", clear=True)

plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df_all = pd.read_csv(csv_final)
    df_m = icaps.brownian.filter_chisqr(df_all, thresh=max_chisqr_trans, mode="trans")
    df_m = icaps.brownian.filter_rel_err(df_m, mass=max_rel_err_trans_mass, mode="trans")
    particles = df_m["particle"].unique()
    df_erf = pd.read_csv(csv_erf)
    df_erf = df_erf[df_erf["particle"].isin(particles)]
    df_disp = pd.read_csv(csv_disp)
    df_disp = df_disp[df_disp["particle"].isin(particles)]

    for dt in [1, 5, 10, 15]:
        # _, ax = plt.subplots()
        mass = np.zeros(len(particles))
        chisqr_sg = np.zeros(len(particles))
        chisqr_dg = np.zeros(len(particles))
        for i, particle in enumerate(particles):
            mass[i] = df_m.loc[df_m["particle"] == particle, "mass_x"].to_numpy()[0]
            chisqr_sg[i] = df_erf.loc[(df_erf["particle"] == particle) &
                                      (df_erf["axis"] == "x") &
                                      (df_erf["dt"] == dt), "chisqr"].to_numpy()[0]
            chisqr_dg[i] = df_erf.loc[(df_erf["particle"] == particle) &
                                      (df_erf["axis"] == "x") &
                                      (df_erf["dt"] == dt), "chisqr_dg"].to_numpy()[0]

        # ax.scatter(mass, chisqr_sg - chisqr_dg, s=3, marker="o", c="black")
        # ax.axhline(0, c="gray", ls="--", lw=2)
        # ax.set_xlim(1e-15, 1e-11)
        # ax.set_ylim(-0.002, 0.002)
        # ax.set_xscale("log")
        # plt.savefig(plot_path + f"chisqr_sg_dg_{dt}.png", dpi=dpi)
        if dt > 1:
            for i, particle in enumerate(particles):
                if chisqr_sg[i] - chisqr_dg[i] < -0.0001:
                    dx = df_disp.loc[df_disp["particle"] == particle, f"dx_{dt}"].dropna().to_numpy()
                    dx = np.sort(dx)
                    dx_norm = np.linspace(1 / len(dx), 1, len(dx))
                    _, ax = plt.subplots()
                    ax.scatter(dx, dx_norm, c="black", s=1, marker="o")
                    xspace = np.linspace(*ax.get_xlim(), 1000)
                    sigma = df_erf.loc[(df_erf["particle"] == particle) &
                                       (df_erf["axis"] == "x") &
                                       (df_erf["dt"] == dt), "sigma"].to_numpy()[0]
                    drift = df_erf.loc[(df_erf["particle"] == particle) &
                                       (df_erf["axis"] == "x") &
                                       (df_erf["dt"] == dt), "drift"].to_numpy()[0]
                    chisqr_sg_ = df_erf.loc[(df_erf["particle"] == particle) &
                                            (df_erf["axis"] == "x") &
                                            (df_erf["dt"] == dt), "chisqr"].to_numpy()[0]
                    ax.plot(xspace, icaps.fit_erf(xspace, drift * 1e6, sigma * 1e6),
                            c="red", lw=1,
                            label=r"SG ($\xi^2 = {{{:.4f}}} \cdot 10^{{-4}}$)".format(chisqr_sg[i] * 1e4))
                    sigma0 = df_erf.loc[(df_erf["particle"] == particle) &
                                        (df_erf["axis"] == "x") &
                                        (df_erf["dt"] == dt), "sigma0"].to_numpy()[0]
                    sigma1 = df_erf.loc[(df_erf["particle"] == particle) &
                                        (df_erf["axis"] == "x") &
                                        (df_erf["dt"] == dt), "sigma1"].to_numpy()[0]
                    drift01 = df_erf.loc[(df_erf["particle"] == particle) &
                                         (df_erf["axis"] == "x") &
                                         (df_erf["dt"] == dt), "drift01"].to_numpy()[0]
                    w0 = df_erf.loc[(df_erf["particle"] == particle) &
                                    (df_erf["axis"] == "x") &
                                    (df_erf["dt"] == dt), "w0"].to_numpy()[0]
                    chisqr_dg_ = df_erf.loc[(df_erf["particle"] == particle) &
                                            (df_erf["axis"] == "x") &
                                            (df_erf["dt"] == dt), "chisqr_dg"].to_numpy()[0]
                    ax.plot(xspace, icaps.fit_erf_dg(xspace, w0, drift01 * 1e6, sigma0 * 1e6, sigma1 * 1e6),
                            c="blue", lw=1,
                            label=r"DG ($\xi^2 = {{{:.4f}}} \cdot 10^{{-4}}$)".format(chisqr_dg[i] * 1e4))
                    xlim = np.max(np.abs(ax.get_xlim()))
                    ax.set_xlim(-xlim, xlim)
                    ax.legend()
                    # plt.show()
                    plt.savefig(plot_path + "parts/{:06d}_{}.png".format(particle, dt))
                    plt.close()


if __name__ == '__main__':
    main()
