import scipy.optimize
from mpl_toolkits.axes_grid1.inset_locator import InsetPosition
import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mplc

# Paths
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT6/"
csv_of_final = project_path + "OF_Final_diff_drop1.csv"
csv_erf_t_flagged = project_path + "ERF_Trans_flagged.csv"
csv_erf_r_flagged = project_path + "ERF_Rot_flagged.csv"
csv_vmu_of = project_path + "vmu/OF_Trans.csv"
plot_path = project_path + "plots/results/"
icaps.mk_folders([plot_path], clear=False)

# Filters
dts_trim = np.arange(1, 11, 1)
dts_trim = np.append(dts_trim, np.arange(11, 26, 2))
dts_trim = np.append(dts_trim, np.arange(30, 51, 5))
max_rss_trans = 40e-4
max_rss_rot = 60e-4
max_rel_err_trans_mass = 0.1
max_rel_err_trans_tau = 0.1
max_rel_err_rot_moi = 0.5
max_rel_err_rot_tau = 0.5
ldm_ex_col = "max_ex"
ldm_ex_err_col = "median_ex_err_median"

# Plots
mass_lims = 2e-15, 1e-11
moi_lims = 5e-27, 1e-21
dpi = 600
fancybox = False
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    # ===================================================================================
    # Needed Tables:
    # - df_t1                        rss_trans, mass_rel_err
    # - df_t2       from df_t1       tau_rel_err
    # - df_r1      from df_t1       rot_notnan, rss_rot, moi_rel_err
    # - df_r2       from df_r1     tau_rel_err, tau_rot_rel_err
    # ===================================================================================
    df_all = pd.read_csv(csv_of_final)
    log = icaps.OutLog().add("all", 0, len(df_all["particle"].unique()), op="none")
    df_t1, log = icaps.brownian.filter_rss(df_all, thresh=max_rss_trans, mode="trans", log=log)
    df_t1, log = icaps.brownian.filter_rel_err(df_t1, mass=max_rel_err_trans_mass, mode="trans", log=log)
    df_t2, log = icaps.brownian.filter_rel_err(df_t1, tau=max_rel_err_trans_tau, mode="trans", log=log)

    df_r1 = df_t1.dropna(subset=["tau_rot"])
    log.add("rot_notnan", 0, len(df_r1["particle"].unique()), op="none")
    df_r1, log = icaps.brownian.filter_rss(df_r1, thresh=max_rss_rot, mode="rot", log=log)
    df_r1, log = icaps.brownian.filter_rel_err(df_r1, mass=max_rel_err_rot_moi, mode="rot", log=log)

    df_r2, log = icaps.brownian.filter_rel_err(df_r1, tau=max_rel_err_trans_tau, mode="trans", log=log)
    df_r2, log = icaps.brownian.filter_rel_err(df_r2, tau=max_rel_err_rot_tau, mode="rot", log=log)
    print(log)
    log.to_csv(plot_path + "info.csv")

    # ===================================================================================
    # INVESTIGATE ERF RSS
    # ===================================================================================
    _, ax = plt.subplots()
    df_erf_t = pd.read_csv(csv_erf_t_flagged)
    idx = (df_erf_t["dt"] == 1) & (df_erf_t["particle"].isin(df_t1["particle"].unique()))
    rss_sg = df_erf_t.loc[idx, "rss"].to_numpy()
    rss_dg = df_erf_t.loc[idx, "rss_dg"].to_numpy()
    rss_exp = df_erf_t.loc[idx, "rss_exp"].to_numpy()
    norm = np.linspace(1 / len(rss_sg), 1, len(rss_sg))
    ax.errorbar(np.sort(rss_sg), norm, c="black", fmt=".")
    ax.errorbar(np.sort(rss_dg), norm, c="gray", fmt=".")
    ax.errorbar(np.sort(rss_exp), norm, c="lightgray", fmt=".")
    print(np.median(rss_sg), np.median(rss_dg), np.median(rss_exp))

    _, ax = plt.subplots()
    df_erf_r = pd.read_csv(csv_erf_r_flagged)
    idx = (df_erf_r["dt"] == 1) & (df_erf_r["particle"].isin(df_r1["particle"].unique()))
    rss_sg = df_erf_r.loc[idx, "rss"].to_numpy()
    rss_dg = df_erf_r.loc[idx, "rss_dg"].to_numpy()
    rss_exp = df_erf_r.loc[idx, "rss_exp"].to_numpy()
    norm = np.linspace(1 / len(rss_sg), 1, len(rss_sg))
    ax.errorbar(np.sort(rss_sg), norm, c="black", fmt=".")
    ax.errorbar(np.sort(rss_dg), norm, c="gray", fmt=".")
    ax.errorbar(np.sort(rss_exp), norm, c="lightgray", fmt=".")
    print(np.median(rss_sg), np.median(rss_dg), np.median(rss_exp))
    # plt.show()
    plt.close()

    # ===================================================================================
    # Noise
    # ===================================================================================
    # Trans
    df_erf = pd.read_csv(csv_erf_t_flagged)
    df_erf = df_erf[df_erf["dt"].isin(dts_trim)]
    df_vmu = pd.read_csv(csv_vmu_of)
    _, ax = plt.subplots()
    ax.set_xlim(0.9e-3, 60e-3)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_ylim(0.8e-15, 1e-9)
    ax.set_xlabel(r"$\Delta t \,\rm (s)$")
    ax.set_ylabel(r"$\langle \Delta x^2 \rangle,\langle \Delta y^2 \rangle \, (\mathrm{m}^{\rm 2})$")

    particles = df_t2["particle"].unique()
    for i, particle in enumerate(particles):
        for j, axis in enumerate(df_erf["axis"].unique()):
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 0.98, df_erf_["sigma_diff"] ** 2,
                        c="black", fmt="o", markersize=1, zorder=11,
                        label="T2 (left), T1 (right)" if (i == 0) and (j == 0) else None)
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (~df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 0.98, df_erf_["sigma_diff"] ** 2,
                        c="lightgray", fmt="o", markersize=1, zorder=10,
                        # label="Dropped Data" if (i == 0) and (j == 0) else None
                        )
    particles = df_t1["particle"].unique()
    # particles_new = []
    # for particle_ in particles_:
    #     if particle_ not in particles:
    #         particles_new.append(particle_)
    # particles = particles_new
    for i, particle in enumerate(particles):
        for j, axis in enumerate(df_erf["axis"].unique()):
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 1.02, df_erf_["sigma_diff"] ** 2,
                        c="black", fmt="o", markersize=1, zorder=11)
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (~df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 1.02, df_erf_["sigma_diff"] ** 2,
                        c="lightgray", fmt="o", markersize=1, zorder=10)
    particles = df_t1.sort_values(by=["mass"])["particle"].unique()[::50]
    for i, particle in enumerate(particles):
        for j, axis in enumerate(df_erf["axis"].unique()):
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis)]
            ax.plot(df_erf_["dt"] * 1e-3 * 1.02, df_erf_["sigma_diff"] ** 2,
                    c="gray", ls="-", lw=1, zorder=5, alpha=0.5)
    for j, axis in enumerate(df_erf["axis"].unique()):
        df_erf_ = df_erf[(df_erf["particle"] == 720240) & (df_erf["axis"] == axis)]
        ax.plot(df_erf_["dt"] * 1e-3 * 1.02, df_erf_["sigma_diff"] ** 2,
                c="blue", ls="-", lw=1, zorder=5, alpha=0.5)

    xspace = np.linspace(*ax.get_xlim(), 1000)
    sigma_X = 10 ** icaps.fit_of_log(xspace, df_vmu.loc[df_vmu["axis"] == "X", "mass"].to_numpy()[0],
                                     df_vmu.loc[df_vmu["axis"] == "X", "tau"].to_numpy()[0])
    sigma_Y = 10 ** icaps.fit_of_log(xspace, df_vmu.loc[df_vmu["axis"] == "Y", "mass"].to_numpy()[0],
                                     df_vmu.loc[df_vmu["axis"] == "Y", "tau"].to_numpy()[0])
    sigma_Z = 10 ** icaps.fit_of_log(xspace, df_vmu.loc[df_vmu["axis"] == "Z", "mass"].to_numpy()[0],
                                     df_vmu.loc[df_vmu["axis"] == "Z", "tau"].to_numpy()[0])
    sigma_max = np.max([sigma_X, sigma_Y, sigma_Z], axis=0)
    sigma_min = np.min([sigma_X, sigma_Y, sigma_Z], axis=0)
    ax.fill_between(xspace, sigma_min, sigma_max, ec="red", fc="lightcoral", alpha=1,
                    label="CMS Drift", zorder=0)
    # ax.annotate("Cloud Center", (2e-2, 1e-13), (2e-2, 1e-13), color="red")

    ax.axhline(1.6e-15, c="red", lw=2, ls="--", label="Rocket Vibrations")

    xvals = np.array(ax.get_xlim())
    # df_thilo = pd.read_csv(project_path + "thilo/raytracing plots/data_plot/data.csv")
    # cols = df_thilo.columns
    # df_thilo.rename(columns={cols[0]: "gen", cols[1]: "median", cols[2]: "sigma", cols[3]: "2sigma",
    #                          cols[4]: "max"}, inplace=True)
    # df_thilo["gen"] = df_thilo["gen"].astype(int)
    # ax.fill_between(xvals, np.repeat(np.min(df_thilo["median"] * 1e-12), 2),
    #                 np.repeat(np.max(df_thilo["median"] * 1e-12), 2),
    #                 ec="red", fc="none", hatch="//", lw=1, label="Discretisation Noise", zorder=0)
    # ax.fill_between(xvals, np.repeat(np.min(df_thilo["2sigma"] * 1e-12), 2),
    #                 np.repeat(np.max(df_thilo["2sigma"] * 1e-12), 2),
    #                 ec="red", fc="none", hatch="\\\\", lw=1, label=r"$2\sigma$ (Sim.)", zorder=0)

    # df_nsim = pd.read_csv(drive_letter + ":/icaps/data/LDM_Noise/noise_sim.csv")
    df_nsim = pd.read_csv(project_path + "thilo/noise_sim.csv")
    median_min = 1e30
    median_max = 1e-30
    for nsim_part in df_nsim["particle"].unique():
        x = df_nsim.loc[df_nsim["particle"] == nsim_part, ["dx", "dy"]].to_numpy().flatten() * icaps.const.px_ldm
        x = np.abs(x)
        x = (x * 1e-6) ** 2
        x = x[~np.isnan(x)]
        median = np.median(x)
        median_min = np.min([median_min, median])
        median_max = np.max([median_max, median])
    ax.fill_between(xvals, np.repeat(median_min, 2), np.repeat(median_max, 2),
                    ec="red", fc="none", hatch="\\\\", lw=1, zorder=0,
                    # label=r"Shot Noise + Brightness Fluct."
                    label=r"Optical Noise"
                    )
    # ax.annotate(xy=(3.5e-2, 1e-15), xytext=(3.5e-2, 4e-15), text="$10^{-15}$", color="red", zorder=1,
    #             bbox={"boxstyle": "square", "fc": "white", "ec": "none"},
    #             # arrowprops={"arrowstyle": "simple", "fc": "red", "ec": "white"}
    #             )
    # px0, px1 = ax.get_xlim()
    # py0, py1 = ax.get_ylim()
    # ax.arrow(x=(np.log(4.2e-2) - np.log(px0)) / (np.log(px1) - np.log(px0)),
    #          y=(np.log(3.5e-15) - np.log(py0)) / (np.log(py1) - np.log(py0)),
    #          dx=(np.log(4.2e-2) - np.log(px0)) / (np.log(px1) - np.log(px0)) -
    #             (np.log(4.2e-2) - np.log(px0)) / (np.log(px1) - np.log(px0)),
    #          dy=(np.log(9.9e-16) - np.log(py0)) / (np.log(py1) - np.log(py0)) -
    #             (np.log(3.5e-15) - np.log(py0)) / (np.log(py1) - np.log(py0)),
    #          fc="red", ec="white", fill="true",
    #          length_includes_head=True,
    #          width=0.01, lw=2,
    #          transform=ax.transAxes, zorder=2)

    icaps.mirror_axis(ax)
    handles, labels = ax.get_legend_handles_labels()
    handles = [handles[-1], handles[0], handles[1], handles[2]]
    labels = [labels[-1], labels[0], labels[1], labels[2]]
    ax.legend(handles=handles, labels=labels, loc=[0, 1.04], fancybox=fancybox, ncol=2, columnspacing=0.5)
    plt.subplots_adjust(top=0.82, left=0.15)
    plt.savefig(plot_path + "Noise_Trans.png", dpi=dpi)
    plt.close()

    # ===========================
    # Rot
    df_erf = pd.read_csv(csv_erf_r_flagged)
    df_erf = df_erf[df_erf["dt"].isin(dts_trim)]
    _, ax = plt.subplots()
    ax.set_xlim(0.9e-3, 60e-3)
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_ylim(1e-5, 5e0)
    ax.set_xlabel(r"$\Delta t \,\rm (s)$")
    ax.set_ylabel(r"$\langle \Delta \theta^2 \rangle \, (\mathrm{rad}^{\rm 2})$")
    xspace = np.linspace(*ax.get_xlim(), 1000)
    particles = df_r2["particle"].unique()
    for i, particle in enumerate(particles):
        for j, axis in enumerate(df_erf["axis"].unique()):
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 0.98, df_erf_["sigma_diff"] ** 2,
                        c="black", fmt="o", markersize=1, zorder=201,
                        label="R2 (left), R1 (right)" if (i == 0) and (j == 0) else None)
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (~df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 0.98, df_erf_["sigma_diff"] ** 2,
                        c="lightgray", fmt="o", markersize=1, zorder=200,
                        # label="Dropped Data" if (i == 0) and (j == 0) else None
                        )
    particles = df_r1["particle"].unique()
    # particles_new = []
    # for particle_ in particles_:
    #     if particle_ not in particles:
    #         particles_new.append(particle_)
    # particles = particles_new
    for i, particle in enumerate(particles):
        for j, axis in enumerate(df_erf["axis"].unique()):
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 1.02, df_erf_["sigma_diff"] ** 2,
                        c="black", fmt="o", markersize=1, zorder=101,
                        # label="R1" if (i == 0) and (j == 0) else None
                        )
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis) & (~df_erf["of_used"])]
            ax.errorbar(df_erf_["dt"] * 1e-3 * 1.02, df_erf_["sigma_diff"] ** 2,
                        c="lightgray", fmt="o", markersize=1, zorder=100,
                        # label="Dropped Data" if (i == 0) and (j == 0) else None
                        )
    particles = df_r1.sort_values(by=["inertia"])["particle"].unique()[::4]
    for i, particle in enumerate(particles):
        for j, axis in enumerate(df_erf["axis"].unique()):
            df_erf_ = df_erf[(df_erf["particle"] == particle) & (df_erf["axis"] == axis)]
            ax.plot(df_erf_["dt"] * 1e-3, df_erf_["sigma_diff"] ** 2,
                    c="gray", ls="-", lw=1, zorder=50, alpha=0.5)
    for j, axis in enumerate(df_erf["axis"].unique()):
        df_erf_ = df_erf[(df_erf["particle"] == 720240) & (df_erf["axis"] == axis)]
        ax.plot(df_erf_["dt"] * 1e-3, df_erf_["sigma_diff"] ** 2,
                c="blue", ls="-", lw=1, zorder=50, alpha=0.5)

    df_nsim = pd.read_csv(drive_letter + ":/icaps/data/LDM_Noise/noise_sim.csv")
    median_min = 1e30
    median_max = 1e-30
    xvals = np.array(ax.get_xlim())
    for nsim_part in df_nsim["particle"].unique():
        x = df_nsim.loc[df_nsim["particle"] == nsim_part, "dangle"].to_numpy()
        x = x[~np.isnan(x)]
        if len(x) == 0:
            continue
        x = np.abs(x)
        x = x ** 2
        median = np.median(x)
        if median == 0.:
            continue
        median_min = np.min([median_min, median])
        median_max = np.max([median_max, median])
    print("!", median_min, median_max)
    ax.fill_between(xvals, np.repeat(median_min, 2), np.repeat(median_max, 2),
                    ec="red", fc="none", hatch="\\\\", lw=1, zorder=-100,
                    label=r"Optical Noise")
    ax.annotate(xy=(3.5e-2, 1e-5), xytext=(3.5e-2, 4e-5), text="$10^{-6}$", color="red", zorder=1,
                bbox={"boxstyle": "square", "fc": "white", "ec": "none"},
                # arrowprops={"arrowstyle": "simple", "fc": "red", "ec": "white"}
                )
    px0, px1 = ax.get_xlim()
    py0, py1 = ax.get_ylim()
    ax.arrow(x=(np.log(4.2e-2) - np.log(px0)) / (np.log(px1) - np.log(px0)),
             y=(np.log(3.5e-5) - np.log(py0)) / (np.log(py1) - np.log(py0)),
             dx=(np.log(4.2e-2) - np.log(px0)) / (np.log(px1) - np.log(px0)) -
                (np.log(4.2e-2) - np.log(px0)) / (np.log(px1) - np.log(px0)),
             dy=(np.log(9.9e-6) - np.log(py0)) / (np.log(py1) - np.log(py0)) -
                (np.log(3.5e-5) - np.log(py0)) / (np.log(py1) - np.log(py0)),
             fc="red", ec="white", fill="true",
             length_includes_head=True,
             width=0.01, lw=2,
             transform=ax.transAxes, zorder=2)
    ax.axhline(np.pi ** 2 / 4, lw=2, ls="--", c="red", label=r"$\pi^2/4\mathrm{-boundary}$")

    # ax.legend(loc="upper left", fancybox=fancybox)
    handles, labels = ax.get_legend_handles_labels()
    handles = [handles[-1], handles[1], handles[0]]
    labels = [labels[-1], labels[1], labels[0]]
    ax.legend(handles=handles, labels=labels, loc=[0, 1.04], fancybox=fancybox, ncol=2, columnspacing=0.5)
    plt.subplots_adjust(top=0.82, left=0.15)
    icaps.mirror_axis(ax)
    # plt.tight_layout()
    plt.savefig(plot_path + "Noise_Rot.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # MOI CUMULATIVE
    # ===================================================================================
    _, ax = plt.subplots()
    ax.minorticks_on()
    moi = df_r1["inertia"].to_numpy()
    moi_err = df_r1["inertia_err"].to_numpy()
    idx = np.argsort(moi)
    moi = moi[idx]
    moi_err = moi_err[idx]
    moi_norm = np.linspace(1 / len(moi), 1, len(moi))
    ax.errorbar(moi, moi_norm, xerr=moi_err, c="black", markersize=1, fmt="o")
    ax.set_ylabel("Normalized Cumulative Frequency")
    ax.set_xlabel(r"MoI (kgm$^2$)")
    ax.set_xscale("log")
    icaps.mirror_axis(ax)
    plt.subplots_adjust(bottom=0.12, top=0.92)
    plt.savefig(plot_path + "MoI_Hist_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # CUMULATIVE DRIFT
    # ===================================================================================
    # Rot
    df_erf_r = pd.read_csv(csv_erf_r_flagged)
    df_erf_r = df_erf_r[df_erf_r["dt"].isin(dts_trim)]
    axis = "rot"
    dts_ = [1, 5, 10, 50]
    markers = ["o", "s", "d", "^"]
    cmap = plt.get_cmap("Greys")
    particles = df_r1["particle"].unique()
    fig, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$\delta_\theta \,\rm(rad)$")
    ax.set_ylabel(r"Normalized Cumulative Frequency")
    for i, dt in enumerate(dts_):
        df_erf_r_ = df_erf_r[(df_erf_r["axis"] == axis) &
                             (df_erf_r["dt"] == dt) &
                             (df_erf_r["particle"].isin(particles))]
        y = df_erf_r_["drift"].dropna().to_numpy()
        y = np.sort(y)
        y_norm = np.linspace(1 / len(y), 1, len(y))
        ax.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                    color=cmap(0.2 + 0.8 * (1 - i/len(dts_))),
                    label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt))
    ax.set_xlim(-0.5, 0.5)
    ax.axhline(0.5, c="gray", ls="--", lw=1)
    ax.axvline(0, c="gray", ls="--", lw=1)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.subplots_adjust(bottom=0.12, top=0.92)
    # plt.savefig(plot_path + f"Drift_{axis}_SG_R1.png", dpi=dpi)
    plt.close()

    fig, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$\delta_\theta / \sigma_\theta$")
    ax.set_ylabel(r"Normalized Cumulative Frequency")
    for i, dt in enumerate(dts_):
        df_erf_r_ = df_erf_r[(df_erf_r["axis"] == axis) &
                             (df_erf_r["dt"] == dt) &
                             (df_erf_r["particle"].isin(particles))]
        y = (df_erf_r_["drift"] / df_erf_r_["sigma"]).dropna().to_numpy()
        y = np.sort(y)
        y_norm = np.linspace(1 / len(y), 1, len(y))
        ax.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                    color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                    label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt))
    ax.set_xlim(-2, 2)
    ax.axhline(0.5, c="gray", ls="--", lw=1)
    ax.axvline(0, c="gray", ls="--", lw=1)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.subplots_adjust(bottom=0.12, top=0.92)
    # plt.savefig(plot_path + f"DriftSig_SG_{axis}_R1.png", dpi=dpi)
    plt.close()

    fig, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(
        r"$(\delta_\theta \cdot t_\mathrm{{obs}}) / (l_{{\mathrm{{drift}}, \theta}} \cdot \Delta t)$")
    ax.set_ylabel(r"Normalized Cumulative Frequency")
    df_ = df_t1[["particle", "n_frames", "inertia", "inertia_err", "tau_rot", "tau_rot_err"]]
    for i, dt in enumerate(dts_):
        df_erf_r_ = df_erf_r[(df_erf_r["axis"] == axis) &
                           (df_erf_r["dt"] == dt) &
                           (df_erf_r["particle"].isin(particles))]
        df_erf_r_ = pd.merge(df_erf_r_, df_, on="particle", how="left")
        df_erf_r_["l_drift"] = np.sqrt(2 * icaps.const.k_B * icaps.const.T * df_erf_r_["tau_rot"] /
                                      df_erf_r_["inertia"] * df_erf_r_["n_dots"] / icaps.const.fps_ldm)
        y = (df_erf_r_["drift"] * df_erf_r_["n_dots"] / icaps.const.fps_ldm /
             (df_erf_r_["l_drift"] * dt * 1e-3)).dropna().to_numpy()
        y = np.sort(y)
        y_norm = np.linspace(1 / len(y), 1, len(y))
        ax.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                    color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                    label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt))
    ax.set_xlim(-3, 3)
    ax.axhline(0.5, c="gray", ls="--", lw=1)
    ax.axvline(0, c="gray", ls="--", lw=1)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.subplots_adjust(bottom=0.12, top=0.92)
    # plt.show()
    # plt.savefig(plot_path + "DriftL_SG_rot_R1.png", dpi=dpi)
    plt.close()

    df_erf_t = pd.read_csv(csv_erf_t_flagged)
    df_erf_t = df_erf_t[df_erf_t["dt"].isin(dts_trim)]

    # ===========================
    # Trans
    axes = ["x", "y"]
    particles = df_t1["particle"].unique()
    dts_ = [1, 5, 10, 50]
    markers = ["o", "s", "d", "^"]
    for axis in axes:
        fig, ax = plt.subplots()
        ax.minorticks_on()
        ax.set_xlabel(r"$\delta_{{{}}} \,\rm(\mu m)$".format(axis))
        ax.set_ylabel(r"Normalized Cumulative Frequency")
        for i, dt in enumerate(dts_):
            df_erf_t_ = df_erf_t[(df_erf_t["axis"] == axis) &
                               (df_erf_t["dt"] == dt) &
                               (df_erf_t["particle"].isin(particles))]
            y = df_erf_t_["drift"].dropna().to_numpy() * 1e6
            y = np.sort(y)
            y_norm = np.linspace(1 / len(y), 1, len(y))
            ax.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                        color=cmap(0.2 + 0.8 * (1 - i/len(dts_))),
                        label=r"$\Delta t = {{{}}} \,\rm ms$".format(dt))
        ax.set_xlim(-5, 5)
        ax.axhline(0.5, c="gray", ls="--", lw=1)
        ax.axvline(0, c="gray", ls="--", lw=1)
        ax.legend(fancybox=fancybox)
        icaps.mirror_axis(ax)
        plt.subplots_adjust(bottom=0.12, top=0.92)
        # plt.savefig(plot_path + f"Drift_{axis}_SG_T1.png", dpi=dpi)
        plt.close()

        fig, ax = plt.subplots()
        ax.minorticks_on()
        ax.set_xlabel(r"$\delta_{{{}}} / \sigma_{{{}}}$".format(axis, axis))
        ax.set_ylabel(r"Normalized Cumulative Frequency")
        for i, dt in enumerate(dts_):
            df_erf_t_ = df_erf_t[(df_erf_t["axis"] == axis) &
                               (df_erf_t["dt"] == dt) &
                               (df_erf_t["particle"].isin(particles))]
            y = (df_erf_t_["drift"] / df_erf_t_["sigma"]).dropna().to_numpy()
            y = np.sort(y)
            y_norm = np.linspace(1 / len(y), 1, len(y))
            ax.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                        color=cmap(0.2 + 0.8 * (1 - i/len(dts_))),
                        label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt))
        ax.set_xlim(-2, 2)
        ax.axhline(0.5, c="gray", ls="--", lw=1)
        ax.axvline(0, c="gray", ls="--", lw=1)
        ax.legend(fancybox=fancybox)
        icaps.mirror_axis(ax)
        plt.subplots_adjust(bottom=0.12, top=0.92)
        # plt.savefig(plot_path + f"DriftSig_SG_{axis}_T1.png", dpi=dpi)
        plt.close()

        fig, ax = plt.subplots()
        ax.minorticks_on()
        ax.set_xlabel(r"$(\delta_{{{}}} \cdot t_\mathrm{{obs}}) / (l_{{\mathrm{{drift}}, {}}} \cdot \Delta t)$".format(axis, axis))
        ax.set_ylabel(r"Normalized Cumulative Frequency")
        df_ = df_t1[["particle", "n_frames", f"mass_{axis}", f"mass_{axis}_err", f"tau_{axis}", f"tau_{axis}_err"]]
        for i, dt in enumerate(dts_):
            df_erf_t_ = df_erf_t[(df_erf_t["axis"] == axis) &
                               (df_erf_t["dt"] == dt) &
                               (df_erf_t["particle"].isin(particles))]
            df_erf_t_ = pd.merge(df_erf_t_, df_, on="particle", how="left")
            df_erf_t_["l_drift"] = np.sqrt(2 * icaps.const.k_B * icaps.const.T * df_erf_t_[f"tau_{axis}"] /
                                          df_erf_t_[f"mass_{axis}"] * df_erf_t_["n_frames"] / icaps.const.fps_ldm)
            y = (df_erf_t_["drift"] * df_erf_t_["n_frames"] / icaps.const.fps_ldm /
                 (df_erf_t_["l_drift"] * dt * 1e-3)).dropna().to_numpy()
            y = np.sort(y)
            y_norm = np.linspace(1 / len(y), 1, len(y))
            ax.errorbar(y, y_norm, fmt=markers[i], markersize=2,
                        color=cmap(0.2 + 0.8 * (1 - i / len(dts_))),
                        label=r"$\Delta t = {{{}}}\,\rm ms$".format(dt))
        ax.set_xlim(-10, 10)
        ax.axhline(0.5, c="gray", ls="--", lw=1)
        ax.axvline(0, c="gray", ls="--", lw=1)
        ax.legend(fancybox=fancybox)
        icaps.mirror_axis(ax)
        plt.subplots_adjust(bottom=0.12, top=0.92)
        # plt.show()
        # plt.savefig(plot_path + f"DriftL_SG_{axis}_T1.png", dpi=dpi)
        plt.close()

    # ===================================================================================
    # DG EVAL
    # ===================================================================================
    # icaps.mk_folders(plot_path + "dg_eval/")
    # particles = df_t1["particle"].unique()
    # for dt in [1, 2, 3, 4, 5]:
    #     for scale in ["lin", "log"]:
    #         _, ax = plt.subplots()
    #         sig_diff = df_erf_t.loc[(df_erf_t["dt"] == dt) & df_erf_t["particle"].isin(particles), "sigma_diff"].to_numpy()
    #         sig0 = df_erf_t.loc[(df_erf_t["dt"] == dt) & df_erf_t["particle"].isin(particles), "sigma0"].to_numpy()
    #         sig1 = df_erf_t.loc[(df_erf_t["dt"] == dt) & df_erf_t["particle"].isin(particles), "sigma1"].to_numpy()
    #         sig01_t = np.vstack((sig0, sig1)).transpose()
    #         sigmax = np.max(sig01_t, axis=1)
    #         sigmin = np.min(sig01_t, axis=1)
    #         idx = np.argwhere(sig_diff <= 1)
    #         sig_diff = sig_diff[idx]
    #         sigmax = sigmax[idx]
    #         sigmin = sigmin[idx]
    #         ax.errorbar(sig_diff * 1e6, sigmax/sig_diff, c="red", fmt="^", label="high", markersize=2)
    #         ax.errorbar(sig_diff * 1e6, sigmin/sig_diff, c="blue", fmt="v", label="low", markersize=2)
    #         ax.set_yscale("log")
    #         ax.set_ylim(5e-2, 5e1 * dt)
    #         if scale == "lin":
    #             ax.set_xlim(-0.1, 1.1 * (1 + dt))
    #         else:
    #             ax.set_xscale("log")
    #             ax.set_xlim(1e-1, 1e1)
    #         xspace = np.linspace(*ax.get_xlim(), 1000)
    #         ax.plot(xspace, 10**(-0.5) / xspace, c="gray", lw=2)
    #         ax.set_xlabel(r"$\sigma_\mathrm{diff} \mathrm{(\mu m)}$")
    #         ax.set_ylabel(r"$\sigma_\mathrm{DG} / \sigma_\mathrm{diff}$")
    #         ax.legend(loc="lower right")
    #         # plt.show()
    #         plt.savefig(plot_path + f"dg_eval/dg_eval_{scale}_{dt}.png", dpi=dpi)
    #         plt.close()

    # ===================================================================================
    # Relative Error Plots
    # ===================================================================================
    # Mass
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$m_x+m_y$ (kg)")
    ax.set_ylabel(r"$\frac{m_x-m_y}{m_x+m_y}$")
    ax.set_xscale("log")
    ax.errorbar(df_t1["mass_x"] + df_t1["mass_y"],
                (df_t1["mass_x"] - df_t1["mass_y"]) / (df_t1["mass_x"] + df_t1["mass_y"]),
                # xerr=df["mass_x_err"], yerr=df["mass_y_err"],
                fmt=".", color="black",
                markersize=5, zorder=10)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_RelErr_T1.png", dpi=dpi)
    plt.close()

    # ===========================
    # Mass Hist
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$(m_x-m_y)\,/\,(m_x+m_y)$")
    ax.set_ylabel(r"Frequency")
    y = (df_t1["mass_x"] - df_t1["mass_y"]) / (df_t1["mass_x"] + df_t1["mass_y"])
    bin_size = 0.04
    bin_lim = np.ceil(np.max(np.abs(y)) * 100) / 100
    bins = np.arange(bin_size / 2, bin_lim + bin_size, bin_size)
    bins = np.append(-bins[::-1], bins)
    hist, _ = np.histogram(y, bins=bins)
    # hist = hist / np.max(hist)
    ax.bar(bins[:-1] + bin_size / 2, hist, width=bin_size * 0.9, color="black")
    ax.set_xlim(-0.86, +0.86)
    ax.set_ylim(0, 1.05 * np.max(hist))
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_RelErr_Hist_T1.png", dpi=dpi)
    plt.close()

    # ===========================
    # Tau
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$\tau_x+\tau_y$ (ms)")
    ax.set_ylabel(r"$\frac{\tau_x-\tau_y}{\tau_x+\tau_y}$")
    # ax.set_yscale("log")
    ax.errorbar((df_t2["tau_x"] + df_t2["tau_y"]) * 1e3,
                (df_t2["tau_x"] - df_t2["tau_y"]) / (df_t2["tau_x"] + df_t2["tau_y"]),
                # xerr=df["mass_x_err"], yerr=df["mass_y_err"],
                fmt=".", color="black",
                markersize=5, zorder=10)
    ax.set_xlim(left=1, right=np.min([np.max((df_t2["tau_x"] + df_t2["tau_y"]) * 1000), 40]))
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_RelErr_T2.png", dpi=dpi)
    plt.close()

    # ===========================
    # Tau Hist
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$(\tau_x-\tau_y)\,/\,(\tau_x+\tau_y)$")
    ax.set_ylabel(r"Frequency")
    y = (df_t2["tau_x"] - df_t2["tau_y"]) / (df_t2["tau_x"] + df_t2["tau_y"])
    bin_size = 0.04
    bin_lim = np.ceil(np.max(np.abs(y)) * 100) / 100
    bins = np.arange(bin_size / 2, bin_lim + bin_size, bin_size)
    bins = np.append(-bins[::-1], bins)
    hist, _ = np.histogram(y, bins=bins)
    # hist = hist / np.max(hist)
    ax.bar(bins[:-1] + bin_size / 2, hist, width=bin_size * 0.9, color="black")
    ax.set_xlim(-0.86, +0.86)
    ax.set_ylim(0, 1.05 * np.max(hist))
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_RelErr_Hist_T2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Mass X VS Y
    # ===================================================================================
    # Inset
    _, ax = plt.subplots(figsize=(4.8, 4.8))
    ax.minorticks_on()
    ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_ylabel(r"$m_y$ (kg)")
    ax.errorbar(df_t1["mass_x"], df_t1["mass_y"],
                xerr=df_t1["mass_x_err"], yerr=df_t1["mass_y_err"],
                fmt=".", color="black", markersize=5, zorder=100)
    ax.set_xlim(icaps.const.m_0 * 0.1, icaps.const.m_0 * 7.2)
    ax.set_ylim(*ax.get_xlim())
    # xlims = ax.get_xlim()
    # ylims = ax.get_ylim()
    # slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
    # ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=101, label=r"$m_y = m_x$"
    mono_dots = [icaps.const.m_0 * i for i in range(1, 7)]
    ax.scatter(mono_dots, mono_dots, c="red", zorder=99)
    for i in range(1, 7):
        circle = plt.Circle((icaps.const.m_0 * i, icaps.const.m_0 * i),
                            radius=icaps.const.m_0_err * i,
                            color="darkorange", zorder=1)
        ax.add_patch(circle)
        circle = plt.Circle((icaps.const.m_0 * i, icaps.const.m_0 * i),
                            radius=2 * icaps.const.m_0_err * i,
                            color="gold", zorder=0)
        ax.add_patch(circle)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_XY_Inset_T1.png", dpi=dpi)
    plt.close()

    # ===========================
    # Full (T1)
    corrcoef = np.corrcoef(df_t1["mass_x"], df_t1["mass_y"])
    print(f"Mass CorrCoef:\n{corrcoef}")
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_ylabel(r"$m_y$ (kg)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(df_t1["mass_x"], df_t1["mass_y"],
                xerr=df_t1["mass_x_err"], yerr=df_t1["mass_y_err"],
                fmt=".", color="black", markersize=5, zorder=10)
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
    ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=11, label=r"$m_y = m_x$")
    out = icaps.fit_model(icaps.fit_lin, df_t1["mass_x"], df_t1["mass_y"],
                          xerr=df_t1["mass_x_err"], yerr=df_t1["mass_y_err"], p0=[1.0, 0.0])
    # xspace = np.linspace(*xlims, 1000)
    # ax.plot(xspace, icaps.fit_lin(xspace, *out.beta), lw=1, ls="-", c="red", alpha=0.5)
    print("Mass beta:", out.beta, out.sd_beta)
    print("Mass offset (m_0):", out.beta[1] / icaps.const.m_0, out.sd_beta[1] / icaps.const.m_0)
    ins_pos = [0.63, 0.02, 0.47 * 4.8 / 6.4, 0.47]
    ins = ax.inset_axes(ins_pos)
    ins.errorbar(df_t1["mass_x"] / icaps.const.m_0, df_t1["mass_y"] / icaps.const.m_0,
                 xerr=df_t1["mass_x_err"] / icaps.const.m_0, yerr=df_t1["mass_y_err"] / icaps.const.m_0,
                 fmt=".", color="black", elinewidth=1, markersize=2, zorder=100)
    # ins.set_xlabel(r"$m_x$ ($m_{\rm 0}$)")
    ins.set_ylabel(r"$m_y$ ($m_{\rm 0}$)")
    ins.set_xlim(0.1, 7.2)
    mono_dots = np.arange(1, 8, 1)
    ins_twin = ins.twiny()
    ins_twin.set_xlim(*ins.get_xlim())
    ins_twin.xaxis.set_ticks(mono_dots)
    ins_twin.minorticks_on()
    ins_twin.set_xlabel(r"$m_x$ ($m_{\rm 0}$)")
    ins_twin.set_axes_locator(InsetPosition(ax, ins_pos))
    ins.xaxis.set_visible(False)
    ins.yaxis.set_ticks(mono_dots)
    ins.set_ylim(*ins.get_xlim())
    ins.scatter(mono_dots[:-1], mono_dots[:-1], c="red", zorder=99, s=4)
    for i in range(1, 7):
        circle = plt.Circle((i, i),
                            radius=icaps.const.m_0_err / icaps.const.m_0 * i,
                            color="darkorange", zorder=1)
        ins.add_patch(circle)
        circle = plt.Circle((i, i),
                            radius=2 * icaps.const.m_0_err / icaps.const.m_0 * i,
                            color="gold", zorder=0)
        ins.add_patch(circle)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_XY_T1.png", dpi=dpi)
    plt.close()

    # ===========================
    # Full (T2)
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.set_xlabel(r"$m_x$ (kg)")
    ax.set_ylabel(r"$m_y$ (kg)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(df_t2["mass_x"], df_t2["mass_y"],
                xerr=df_t2["mass_x_err"], yerr=df_t2["mass_y_err"],
                fmt=".", color="black", markersize=5, zorder=10)
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    slope1 = (np.min([xlims[0], ylims[0]]), np.max([xlims[1], ylims[1]]))
    ax.plot(slope1, slope1, linestyle="--", c="gray", lw=2, zorder=11, label=r"$m_y = m_x$")
    ins = ax.inset_axes([0.66, 0.14, 0.43 * 4.8 / 6.4, 0.43])
    ins.errorbar(df_t2["mass_x"] / icaps.const.m_0, df_t2["mass_y"] / icaps.const.m_0,
                 xerr=df_t2["mass_x_err"] / icaps.const.m_0, yerr=df_t2["mass_y_err"] / icaps.const.m_0,
                 fmt=".", color="black", elinewidth=1, markersize=2, zorder=100)
    ins.set_xlabel(r"$m_x$ ($m_{\rm 0}$)")
    ins.set_ylabel(r"$m_y$ ($m_{\rm 0}$)")
    ins.set_xlim(0.1, 7.2)
    mono_dots = np.arange(1, 8, 1)
    ins.xaxis.set_ticks(mono_dots)
    ins.yaxis.set_ticks(mono_dots)
    ins.set_ylim(*ins.get_xlim())
    ins.scatter(mono_dots[:-1], mono_dots[:-1], c="red", zorder=99, s=4)
    for i in range(1, 7):
        circle = plt.Circle((i, i),
                            radius=icaps.const.m_0_err / icaps.const.m_0 * i,
                            color="darkorange", zorder=1)
        ins.add_patch(circle)
        circle = plt.Circle((i, i),
                            radius=2 * icaps.const.m_0_err / icaps.const.m_0 * i,
                            color="gold", zorder=0)
        ins.add_patch(circle)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Mass_XY_T2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Tau Trans X VS Y
    # ===================================================================================
    corrcoef = np.corrcoef(df_t2["tau_x"], df_t2["tau_y"])
    print(f"Tau CorrCoef:\n{corrcoef}")
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.errorbar(df_t2["tau_x"] * 1e3, df_t2["tau_y"] * 1e3,
                xerr=df_t2["tau_x_err"] * 1e3, yerr=df_t2["tau_y_err"] * 1e3,
                fmt=".", color="black", markersize=3, zorder=10)
    ax.set_xlabel(r"$\tau_x$ (ms)")
    ax.set_ylabel(r"$\tau_y$ (ms)")
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    lims = (min(xlims[0], ylims[0]), max(xlims[1], ylims[1]))
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 20)
    ax.plot(ax.get_xlim(), ax.get_xlim(), color="gray", linestyle="--")
    out = icaps.fit_model(icaps.fit_lin, df_t2["tau_x"] * 1e3, df_t2["tau_y"] * 1e3,
                          xerr=df_t2["tau_x_err"] * 1e3, yerr=df_t2["tau_y_err"] * 1e3, p0=[1.0, 0.0])
    # xspace = np.linspace(*ax.get_xlim(), 1000)
    # ax.plot(xspace, icaps.fit_lin(xspace, *out.beta), lw=1, ls="-", c="red", alpha=0.5)
    print("Tau beta:", out.beta, out.sd_beta)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_XY_T2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Tau Trans VS Rot
    # ===================================================================================
    # Mean
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.errorbar(df_r2["tau"] * 1e3, df_r2["tau_rot"] * 1e3,
                xerr=df_r2["tau_err"] * 1e3, yerr=df_r2["tau_rot_err"] * 1e3,
                fmt=".", color="black", markersize=3, zorder=10)
    ax.set_xlabel(r"$\langle\tau_{\rm t}\rangle$ (ms)")
    ax.set_ylabel(r"$\tau_{\rm r}$ (ms)")
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    lims = (min(xlims[0], ylims[0]), max(xlims[1], ylims[1]))
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 20)
    ax.plot(lims, lims, color="gray", linestyle="--")
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_TR_mean_R2.png", dpi=dpi)
    plt.close()

    # ===========================
    # Mixed
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.errorbar(df_r2["tau_x"] * 1e3, df_r2["tau_rot"] * 1e3,
                xerr=df_r2["tau_x_err"] * 1e3, yerr=df_r2["tau_rot_err"] * 1e3,
                fmt="o", color="black", markersize=3, zorder=10, label=r"$x$ (black), $y$ (gray)")
    ax.errorbar(df_r2["tau_y"] * 1e3, df_r2["tau_rot"] * 1e3,
                xerr=df_r2["tau_y_err"] * 1e3, yerr=df_r2["tau_rot_err"] * 1e3,
                fmt="o", color="gray", markersize=3, zorder=10)
    ax.set_xlabel(r"$\tau_{\rm t}$ (ms)")
    ax.set_ylabel(r"$\tau_{\rm r}$ (ms)")
    # ax.set_xscale("log")
    # ax.set_yscale("log")
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    lims = (min(xlims[0], ylims[0]), max(xlims[1], ylims[1]))
    ax.set_xlim(lims)
    ax.set_ylim(lims)
    ax.set_xlim(0, 20)
    ax.set_ylim(0, 20)
    ax.plot(lims, lims, color="gray", linestyle="--")
    ax.legend(fancybox=fancybox, loc="upper left")
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_TR_mixed_R2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Tau Cumulative
    # ===================================================================================
    t0 = 4.8
    t1 = 15
    _, ax = plt.subplots()
    ax.minorticks_on()
    tau = np.hstack([df_t2["tau_x"].to_numpy(), df_t2["tau_y"].to_numpy()]).ravel()
    start_frame = np.tile(df_t2["start_frame"], 2)
    tau_err = np.hstack([df_t2["tau_x_err"].to_numpy(), df_t2["tau_y_err"].to_numpy()]).ravel()
    tau1 = np.sort(tau[start_frame < icaps.const.end_ff]) * 1000
    tau2 = np.sort(tau[start_frame >= icaps.const.end_ff]) * 1000
    norm_tau1 = np.linspace(1 / len(tau1), 1, len(tau1))
    norm_tau2 = np.linspace(1 / len(tau2), 1, len(tau2))
    p1 = icaps.const.pressure_i1
    p2 = icaps.const.pressure_i2
    tau3 = tau2 * p2 / p1
    norm_tau3 = np.linspace(1 / len(tau3), 1, len(tau3))
    fc = ["black", "gray", "lightgray"]
    ec = ["black", "gray", "lightgray"]
    markers = ["o", "d", "s"]
    ax.errorbar(tau1, norm_tau1, mfc=fc[0], mec=ec[0], fmt=markers[0], markersize=2,
                label=r"$\tau_\mathrm{t, 1} = \tau_{x,y}$"
                      + r"$\left(p_{{\rm 1}} = {{{}}}\,\mathrm{{Pa}}  \right)$".format(round(p1)))
    ax.errorbar(tau2, norm_tau2, mfc=fc[2], mec=ec[2], fmt=markers[2], markersize=2,
                label=r"$\tau_\mathrm{t, 2} = \tau_{x,y}$"
                      + r"$\left(p_{{\rm 2}} = {{{}}}\,\mathrm{{Pa}}  \right)$".format(round(p2)))
    ax.errorbar(tau3, norm_tau3, mfc=fc[1], mec=ec[1], fmt=markers[1], markersize=2,
                label=r"$\tau_\mathrm{t, 2}' = \tau_\mathrm{t, 2} \cdot \frac{p_{\rm 2}}{p_{\rm 1}}$")
    ax.axvline(t0, c="gray", ls="--")
    ax.axvline(t1, c="gray", ls="--")
    try:
        perc1 = [np.max(norm_tau1[tau1 < t0]), np.max(norm_tau1[tau1 <= t1])]
    except ValueError:
        perc1 = [0., np.max(norm_tau1[tau1 <= t1])]
    try:
        perc3 = [np.max(norm_tau3[tau3 < t0]), np.max(norm_tau3[tau3 <= t1])]
    except ValueError:
        perc3 = [0., np.max(norm_tau3[tau3 <= t1])]
    ax.set_xlim(0, 25)
    ax.set_ylim(-0.05, 1.05)
    xys = [[1.7, 0.2], [9.3, 0.5], [16, 0.9]]
    percs = [[perc3[0], perc1[0]], [perc3[1] - perc3[0], perc1[1] - perc1[0]], [1. - perc3[1], 1. - perc1[1]]]
    pad = np.array([ax.get_xlim()[1], ax.get_ylim()[1]]) * 0.005
    for i, xy in enumerate(xys):
        ann = ax.annotate(r"{:.0f} \%".format(percs[i][0] * 100), xy=[xy[0], xy[1] - 0.05], xycoords="data", size=14,
                          ha="left", va="top", c=ec[1])
        ax.figure.canvas.draw()
        wex = np.array(ax.transData.inverted().transform(ann.get_window_extent()))
        wxy = wex[0] + np.array([-0.5, 0.]) - pad
        ann = ax.annotate(r"{:.0f} \%".format(percs[i][1] * 100), xy=xy, xycoords="data", size=14,
                          ha="left", va="top", c=ec[0])
        ax.figure.canvas.draw()
        wex = np.array(ax.transData.inverted().transform(ann.get_window_extent()))
        wwh = wex[1] + pad - wxy
        rect = plt.Rectangle(wxy + np.array([0, 0.004]), *wwh, facecolor="none", edgecolor="black")
        ax.add_patch(rect)
        ax.errorbar(xy[0] - pad[0] * 2.8, xy[1] - pad[1] * 2.8 - 0.003,
                    mec=ec[0], mfc=fc[0], marker=markers[0], markersize=2)
        ax.errorbar(xy[0] - pad[0] * 2.8, xy[1] - 0.05 - pad[1] * 2.8 - 0.003,
                    mec=ec[1], mfc=fc[1], marker=markers[1], markersize=2)
    ax.legend(loc="lower right", fancybox=fancybox, framealpha=1.)
    ax.set_xlabel(r"$\tau_\mathrm{t}$ (ms)")
    ax.set_ylabel("Normalized Cumulative Frequency")
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_Cumu_T2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # TAU_ROT CUMULATIVE
    # ===================================================================================
    t0 = 4.8
    t1 = 15
    _, ax = plt.subplots()
    ax.minorticks_on()
    tau_r_i1 = df_r2.loc[df_r2["start_frame"] < icaps.const.end_ff, "tau_rot"].to_numpy() * 1e3
    tau_r_i1_err = df_r2.loc[df_r2["start_frame"] < icaps.const.end_ff, "tau_rot_err"].to_numpy() * 1e3
    tau_r_i2 = df_r2.loc[df_r2["start_frame"] >= icaps.const.end_ff, "tau_rot"].to_numpy() * 1e3
    tau_r_i2_err = df_r2.loc[df_r2["start_frame"] >= icaps.const.end_ff, "tau_rot_err"].to_numpy() * 1e3
    idx = np.argsort(tau_r_i1)
    tau_r_i1 = tau_r_i1[idx]
    tau_r_i1_err = tau_r_i1_err[idx]
    tau_r_i1_norm = np.linspace(1 / len(tau_r_i1), 1, len(tau_r_i1))
    idx = np.argsort(tau_r_i2)
    tau_r_i2 = tau_r_i2[idx]
    tau_r_i2_err = tau_r_i2_err[idx]
    tau_r_i2_norm = np.linspace(1 / len(tau_r_i2), 1, len(tau_r_i2))
    p1 = icaps.const.pressure_i1
    p2 = icaps.const.pressure_i2
    tau_r_i2_ = tau_r_i2 * p2 / p1
    label = r"$\tau_{{\rm r,1}} = \tau_{{\rm r}}(p_1 = {{{}}}\,\mathrm{{Pa}})$".format(round(p1))
    _, _, bars = ax.errorbar(tau_r_i1, tau_r_i1_norm, xerr=tau_r_i1_err,
                             mfc="black", mec="black", markersize=2, fmt="o", label=label)
    [bar.set_alpha(0.0) for bar in bars]
    label = r"$\tau_{{\rm r,2}} = \tau_{{\rm r}}(p_2 = {{{}}}\,\mathrm{{Pa}})$".format(round(p2))
    _, _, bars = ax.errorbar(tau_r_i2, tau_r_i2_norm, xerr=tau_r_i2_err,
                             mfc="lightgray", mec="lightgray", markersize=2, fmt="s",
                             label=label)
    [bar.set_alpha(0.0) for bar in bars]
    _, _, bars = ax.errorbar(tau_r_i2_, tau_r_i2_norm, xerr=tau_r_i2_err,
                             mfc="gray", mec="gray", markersize=2, fmt="d",
                             label=r"$\tau_{\rm r,2}' = \tau_{\rm r,2} \cdot \frac{p_2}{p_1}$")
    [bar.set_alpha(0.0) for bar in bars]
    ax.axvline(t0, c="gray", ls="--")
    ax.axvline(t1, c="gray", ls="--")
    try:
        perc1 = [np.max(tau_r_i1_norm[tau_r_i1 < t0]), np.max(tau_r_i1_norm[tau_r_i1 <= t1])]
    except ValueError:
        perc1 = [0., np.max(tau_r_i1_norm[tau_r_i1 <= t1])]
    try:
        perc3 = [np.max(tau_r_i2_norm[tau_r_i2_ < t0]), np.max(tau_r_i2_norm[tau_r_i2_ <= t1])]
    except ValueError:
        perc3 = [0., np.max(tau_r_i2_norm[tau_r_i2_ <= t1])]
    ax.set_xlim(0, 25)
    ax.set_ylim(-0.05, 1.05)
    xys = [[1.7, 0.6], [9.3, 0.6], [16, 0.9]]
    percs = [[perc3[0], perc1[0]], [perc3[1] - perc3[0], perc1[1] - perc1[0]], [1. - perc3[1], 1. - perc1[1]]]
    pad = np.array([ax.get_xlim()[1], ax.get_ylim()[1]]) * 0.005
    for i, xy in enumerate(xys):
        ann = ax.annotate(r"{:.0f} \%".format(percs[i][0] * 100), xy=[xy[0], xy[1] - 0.05], xycoords="data", size=14,
                          ha="left", va="top", c=ec[1])
        ax.figure.canvas.draw()
        wex = np.array(ax.transData.inverted().transform(ann.get_window_extent()))
        wxy = wex[0] + np.array([-0.5, 0.]) - pad
        ann = ax.annotate(r"{:.0f} \%".format(percs[i][1] * 100), xy=xy, xycoords="data", size=14,
                          ha="left", va="top", c=ec[0])
        ax.figure.canvas.draw()
        wex = np.array(ax.transData.inverted().transform(ann.get_window_extent()))
        wwh = wex[1] + pad - wxy
        rect = plt.Rectangle(wxy + np.array([0, 0.004]), *wwh, facecolor="none", edgecolor="black")
        ax.add_patch(rect)
        ax.errorbar(xy[0] - pad[0] * 2.8, xy[1] - pad[1] * 2.8 - 0.003,
                    mec=ec[0], mfc=fc[0], marker=markers[0], markersize=2)
        ax.errorbar(xy[0] - pad[0] * 2.8, xy[1] - 0.05 - pad[1] * 2.8 - 0.003,
                    mec=ec[1], mfc=fc[1], marker=markers[1], markersize=2)
    ax.set_ylabel("Normalized Cumulative Frequency")
    ax.set_xlabel(r"$\tau_r$ (ms)")
    ax.legend(fancybox=fancybox, framealpha=1.)
    icaps.mirror_axis(ax)
    # plt.subplots_adjust(bottom=0.12, top=0.92)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_R_Hist_R2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Tau VS Mass
    # ===================================================================================
    _, ax = plt.subplots()
    mass = np.hstack([df_t2["mass_x"].to_numpy(), df_t2["mass_y"].to_numpy()]).ravel()
    mass_err = np.hstack([df_t2["mass_x_err"].to_numpy(), df_t2["mass_y_err"].to_numpy()]).ravel()
    ax.minorticks_on()
    ax.set_xlabel(r"$m$ (kg)")
    ax.set_ylabel(r"$\tau_{x, y}$ (ms)")
    ax.set_xscale("log")
    # ax.set_yscale("log")
    ax.errorbar(mass[start_frame < icaps.const.end_ff], tau[start_frame < icaps.const.end_ff] * 1000,
                xerr=mass_err[start_frame < icaps.const.end_ff], yerr=tau_err[start_frame < icaps.const.end_ff] * 1000,
                fmt="o", color="black", markersize=3, zorder=10,
                label=r"$\tau_\mathrm{t, 1} = \tau_{x,y}$"
                      + r"$\left(p_1 = {{{}}}\,\mathrm{{Pa}}  \right)$".format(round(p1)))
    ax.errorbar(mass[start_frame >= icaps.const.end_ff], tau[start_frame >= icaps.const.end_ff] * p2 / p1 * 1000,
                xerr=mass_err[start_frame >= icaps.const.end_ff],
                yerr=tau_err[start_frame >= icaps.const.end_ff] * 1000,
                fmt="d", color="gray", markersize=3, zorder=10,
                label=r"$\tau_\mathrm{t, 2}' = \tau_\mathrm{t, 2} \cdot \frac{p_2}{p_1}$")
    tau_corr = np.append(tau[start_frame < icaps.const.end_ff], tau[start_frame >= icaps.const.end_ff] * p2 / p1) * 1000
    out = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(tau_corr), fit_type=0)
    xspace = np.linspace(np.min(mass), np.max(mass), 1000)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="black", lw=3, zorder=100, alpha=1.0, ls="--",
            label=r"$\tau_{\rm t} \propto m^a$" + "\n"
                  + r"$a = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]))
    ax.set_ylim(bottom=0, top=30)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_Mass_T2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Tau_Rot VS Inertia
    # ===================================================================================
    _, ax = plt.subplots()
    inertia = df_r2["inertia"].to_numpy()
    inertia_err = df_r2["inertia_err"].to_numpy()
    tau_rot = df_r2["tau_rot"].to_numpy()
    tau_rot_err = df_r2["tau_rot_err"].to_numpy()
    ax.minorticks_on()
    ax.set_xlabel(r"$I \mathrm{ (kgm^2)}$")
    ax.set_ylabel(r"$\tau_{\rm r}$ (ms)")
    ax.set_xscale("log")
    start_frame_rot = df_r2["start_frame"].to_numpy()
    # ax.set_yscale("log")
    ax.errorbar(inertia[start_frame_rot < icaps.const.end_ff], tau_rot[start_frame_rot < icaps.const.end_ff] * 1000,
                xerr=inertia_err[start_frame_rot < icaps.const.end_ff],
                yerr=tau_rot_err[start_frame_rot < icaps.const.end_ff] * 1000,
                fmt="o", color="black", markersize=5, zorder=10,
                label=r"$\tau_{\rm r, 1} = \tau_{\rm r}$" +
                      r"$\left(p_1 = {{{}}}\,\mathrm{{Pa}}  \right)$".format(round(p1)))
    ax.errorbar(inertia[start_frame_rot >= icaps.const.end_ff],
                tau_rot[start_frame_rot >= icaps.const.end_ff] * p2 / p1 * 1000,
                xerr=inertia_err[start_frame_rot >= icaps.const.end_ff],
                yerr=tau_rot_err[start_frame_rot >= icaps.const.end_ff] * 1000,
                fmt="d", color="gray", markersize=5, zorder=10,
                label=r"$\tau_{\rm r, 2}' = \tau_{\rm r, 2} \cdot \frac{p_2}{p_1}$")
    tau_rot_corr = np.append(tau_rot[start_frame_rot < icaps.const.end_ff],
                             tau_rot[start_frame_rot >= icaps.const.end_ff] * p2 / p1) * 1000
    out = icaps.fit_model(icaps.fit_lin, np.log10(inertia), np.log10(tau_rot_corr), fit_type=0)
    xspace = np.linspace(np.min(inertia), np.max(inertia), 1000)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out.beta), c="black", lw=3, zorder=100, alpha=1.0, ls="--",
            label=r"$\tau_{\rm r} \propto I^a$" + "\n"
                  + r"$a = {:.2f} \pm {:.2f}$".format(out.beta[0], out.sd_beta[0]))
    ax.set_ylim(bottom=0, top=20)
    ax.set_xlim(left=5e-26, right=1e-21)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Tau_Rot_Inertia_R2.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Derivation of ldm_ex0
    # ===================================================================================
    # T1
    _, ax = plt.subplots()
    ax.set_xlabel(r"$\langle m \rangle$ (kg)")
    ax.set_ylabel(r"$E_{\rm max} \cdot m_{\rm 0}$ (arb. units)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    mass_brown = df_t1["mass"].to_numpy()
    mass_brown_err = df_t1["mass_err"].to_numpy()
    mass_opt = df_t1[ldm_ex_col].to_numpy() * icaps.const.m_0
    mass_opt_err = mass_opt * np.sqrt((df_t1[ldm_ex_err_col].to_numpy() / df_t1[ldm_ex_col].to_numpy()) ** 2
                                      + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    ax.errorbar(mass_brown, mass_opt, xerr=mass_brown_err, yerr=mass_opt_err,
                fmt="o", c="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out = icaps.fit_model(icaps.fit_slope1, np.log10(mass_brown), np.log10(mass_opt),
                          icaps.log_err(mass_brown, mass_brown_err), icaps.log_err(mass_opt, mass_opt_err))
    ax.plot(xspace, 10 ** icaps.fit_slope1(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
            label=r"$E_{\rm max} \cdot m_{\rm 0} = E_{\rm 0} \cdot \langle m \rangle$"
                  + "\n"
                  + r"$E_{\rm 0}$" + r"$ = {{{:.0f}}} \pm {{{:.0f}}}$".format(10 ** out.beta[0],
                                                                                   10 ** out.sd_beta[0]),
            alpha=0.5, zorder=10)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Ex0_T1.png", dpi=dpi)
    plt.close()

    # ===========================
    # T2
    _, ax = plt.subplots()
    ax.set_xlabel(r"Brownian Mass (kg)")
    ax.set_ylabel(r"$E_{\rm max} \cdot m_{\rm 0}$ (arb. units)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    mass_brown = df_t2["mass"].to_numpy()
    mass_brown_err = df_t2["mass_err"].to_numpy()
    mass_opt = df_t2[ldm_ex_col].to_numpy() * icaps.const.m_0
    mass_opt_err = mass_opt * np.sqrt((df_t2[ldm_ex_err_col].to_numpy() / df_t2[ldm_ex_col].to_numpy()) ** 2
                                      + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    ax.errorbar(mass_brown, mass_opt, xerr=mass_brown_err, yerr=mass_opt_err,
                fmt="o", c="black", markersize=3)

    """
    mass_x_brown = df_t2["mass_x"].to_numpy()
    mass_x_brown_err = df_t2["mass_x_err"].to_numpy()
    mass_y_brown = df_t2["mass_y"].to_numpy()
    mass_y_brown_err = df_t2["mass_y_err"].to_numpy()
    mass_opt = df_t2["mean_ex"].to_numpy() * icaps.const.m_0
    mass_opt_err = mass_opt * np.sqrt((df_t2[ldm_ex_err_col].to_numpy() / df_t2[ldm_ex_col].to_numpy()) ** 2
                                      + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    ax.errorbar(mass_x_brown, mass_opt, xerr=mass_x_brown_err, yerr=mass_opt_err,
                fmt="o", c="black", markersize=3)
    ax.errorbar(mass_y_brown, mass_opt, xerr=mass_y_brown_err, yerr=mass_opt_err,
                fmt="x", c="gray", markersize=3)
    mass_brown = np.append(mass_x_brown, mass_y_brown)
    mass_brown_err = np.append(mass_x_brown_err, mass_y_brown_err)
    mass_opt = np.tile(mass_opt, 2)
    mass_opt_err = np.tile(mass_opt_err, 2)
    """

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out = icaps.fit_model(icaps.fit_slope1, np.log10(mass_brown), np.log10(mass_opt),
                          icaps.log_err(mass_brown, mass_brown_err), icaps.log_err(mass_opt, mass_opt_err)
                          )
    ax.plot(xspace, 10 ** icaps.fit_slope1(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
            label=r"$E_{\rm max} \cdot m_{\rm 0} = E_{\rm 0} \cdot \langle m \rangle$"
                  + "\n"
                  + r"$E_{\rm 0}$" + r"$ = {{{:.0f}}} \pm {{{:.0f}}}$".format(10 ** out.beta[0],
                                                                                   10 ** out.sd_beta[0]),
            alpha=0.5, zorder=10)
    this_ldm_ex0 = 10 ** out.beta[0]
    this_ldm_ex0_err = 10 ** out.sd_beta[0]
    ax.legend(fancybox=fancybox)
    ex_m0_lims = ax.get_ylim()
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Ex0_T2.png", dpi=dpi)
    plt.close()

    # ===========================
    # R1
    _, ax = plt.subplots()
    ax.set_xlabel(r"Brownian Mass (kg)")
    ax.set_ylabel(r"$E_{\rm max} \cdot m_{\rm 0}$ (arb. units)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*ex_m0_lims)
    mass_brown = df_r1["mass"].to_numpy()
    mass_brown_err = df_r1["mass_err"].to_numpy()
    mass_opt = df_r1[ldm_ex_col].to_numpy() * icaps.const.m_0
    mass_opt_err = mass_opt * np.sqrt((df_r1[ldm_ex_err_col].to_numpy() / df_r1[ldm_ex_col].to_numpy()) ** 2
                                      + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    ax.errorbar(mass_brown, mass_opt, xerr=mass_brown_err, yerr=mass_opt_err,
                fmt="o", c="black", markersize=3)
    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out = icaps.fit_model(icaps.fit_slope1, np.log10(mass_brown), np.log10(mass_opt),
                          icaps.log_err(mass_brown, mass_brown_err), icaps.log_err(mass_opt, mass_opt_err))
    ax.plot(xspace, 10 ** icaps.fit_slope1(np.log10(xspace), *out.beta), c="red", ls="-", lw=2,
            label=r"$E_{\rm max} \cdot m_{\rm 0} = E_{\rm 0} \cdot \langle m \rangle$" + "\n"
                  + r"$E_{\rm 0}$" + r"$ = {{{:.0f}}} \pm {{{:.0f}}}$".format(10 ** out.beta[0],
                                                                                   10 ** out.sd_beta[0]),
            alpha=0.5, zorder=10)
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "Ex0_R1.png", dpi=dpi)
    plt.close()

    """
    # ===================================================================================
    # Derivation of MoI Decline
    # ===================================================================================
    # Fit (R1)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
    ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    mass_opt = df_r1[ldm_ex_col].to_numpy() * icaps.const.m_0 / this_ldm_ex0
    mass_opt_err = mass_opt * np.sqrt((df_r1[ldm_ex_err_col].to_numpy() / df_r1[ldm_ex_col].to_numpy()) ** 2
                                      + (icaps.const.m_0_err / icaps.const.m_0) ** 2)

    moi_brown = df_r1["inertia"].to_numpy()
    moi_brown_err = df_r1["inertia_err"].to_numpy()
    moi_opt = mass_opt * (df_r1["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
    moi_opt_err = moi_opt * this_ldm_ex0 * np.sqrt((mass_opt_err / (mass_opt * this_ldm_ex0)) ** 2
                                                   + (df_r1["mean_gyrad_err"].to_numpy() /
                                                      df_r1["mean_gyrad"].to_numpy()) ** 2)
    moi_opt_low = (moi_opt * this_ldm_ex0 - moi_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    moi_opt_high = (moi_opt * this_ldm_ex0 + moi_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    ax.errorbar(moi_brown, moi_opt, xerr=moi_brown_err, yerr=[moi_opt - moi_opt_low, moi_opt_high - moi_opt],
                fmt="o", c="black", markersize=5,)
    log_err = icaps.log_err(moi_opt, np.mean([moi_opt - moi_opt_low, moi_opt_high - moi_opt], axis=0))

    ax.set_ylim(1e-25, 1e-21)
    ax.set_xlim(*ax.get_ylim())
    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    xlogpspace = np.logspace(-25, -21, 1000)
    ax.plot(xspace, xspace, color="gray", ls="--", lw=2, zorder=-1)
    out1 = icaps.fit_model(icaps.fit_MoI_lin, moi_brown, np.log10(moi_opt), fit_type=0,
                           xerr=moi_brown_err,
                           yerr=log_err
                           )
    rss1 = np.sum((np.log10(moi_opt) - icaps.fit_MoI_lin(moi_brown, *out1.beta)) ** 2) / len(moi_brown)
    ax.plot(xlogpspace, 10 ** icaps.fit_MoI_lin(xlogpspace, *out1.beta), ls="-", c="red", zorder=10,
            label=r"$a = {{{:.4f}}} \cdot 10^{{-25}}$".format(out1.beta[0] * 1e25) + "\n" +
                  r"$\mathrm{{RSS}} = {{{:.4f}}}$".format(rss1))
    out2 = icaps.fit_model(icaps.fit_MoI, moi_brown, np.log10(moi_opt), fit_type=0, p0=[1e-24, 1],
                           xerr=moi_brown_err,
                           yerr=log_err
                           )
    rss2 = np.sum((np.log10(moi_opt) - icaps.fit_MoI(moi_brown, *out2.beta)) ** 2) / len(moi_brown)
    ax.plot(xlogpspace, 10 ** icaps.fit_MoI(xlogpspace, *out2.beta), ls="-.", c="blue", zorder=10,
            label=r"$a = {{{:.4f}}} \cdot 10^{{-27}}$".format(out2.beta[0] * 1e27) + "\n" +
                  r"$b = {{{:.4f}}}$".format(out2.beta[1]) + "\n" +
                  r"$\mathrm{{RSS}} = {{{:.4f}}}$".format(rss2))

    out3 = icaps.fit_model(icaps.fit_MoI_half, moi_brown, np.log10(moi_opt), fit_type=0, p0=[1e-24],
                           xerr=moi_brown_err,
                           yerr=log_err
                           )
    rss3 = np.sum((np.log10(moi_opt) - icaps.fit_MoI_half(moi_brown, *out3.beta)) ** 2) / len(moi_brown)
    ax.plot(xlogpspace, 10 ** icaps.fit_MoI_half(xlogpspace, *out3.beta), ls="--", c="green", zorder=10,
            label=r"$a = {{{:.4f}}} \cdot 10^{{-27}}$".format(out3.beta[0] * 1e27) + "\n" +
                  r"$\mathrm{{RSS}} = {{{:.4f}}}$".format(rss3))

    def adapt_opt(opt, ret_fails=False):
        import warnings
        res = np.zeros_like(opt)
        fails = np.zeros_like(opt).astype(bool)
        for i, opt_ in enumerate(opt):
            x0 = - 2 * np.sqrt(out3.beta[0] * opt_) + out3.beta[0] + opt_
            with warnings.catch_warnings(record=True) as w:
                res[i] = scipy.optimize.fsolve(lambda q: 10 ** icaps.fit_MoI(q, *out2.beta) - opt_, x0=x0)
                if len(w) > 0:
                    fails[i] = True
            if fails[i]:
                fails[i] = False
                x0 = opt_
                with warnings.catch_warnings(record=True) as w:
                    res[i] = scipy.optimize.fsolve(lambda q: 10 ** icaps.fit_MoI(q, *out2.beta) - opt_, x0=x0)
                    if len(w) > 0:
                        fails[i] = True
        if ret_fails:
            return res, fails
        return res

    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "MoI_Decline_R1.png", dpi=dpi)
    plt.close()

    # ===========================
    # Residuals
    _, ax = plt.subplots(nrows=2)
    res1 = np.log10(moi_opt) - icaps.fit_MoI_lin(moi_brown, *out1.beta)
    res2 = np.log10(moi_opt) - icaps.fit_MoI(moi_brown, *out2.beta)
    ax[0].scatter(moi_brown, res1, c="red", marker="o", s=7,
                  label=r"$MoI_{\rm opt} = MoI_{\rm brown} + a$")
    ax[1].scatter(moi_brown, res2, facecolor="none", edgecolor="blue", marker="o", s=7,
                  label=r"$MoI_{\rm opt} = MoI_{\rm brown} * (1 + (a/MoI_{\rm brown})^b)^2$")
    # ax[0].legend()
    # ax[1].legend()
    ax[0].set_xscale("log")
    # ax[0].set_yscale("log")
    ax[1].set_xscale("log")
    # ax[1].set_yscale("log")
    ax[0].set_xlim(1e-25, 1e-21)
    ax[1].set_xlim(*ax[0].get_xlim())
    ax[0].set_ylim(-0.52, 0.52)
    ax[1].set_ylim(-0.52, 0.52)
    ax[1].set_xlabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
    ax[0].set_ylabel("Residual (log)")
    ax[1].set_ylabel("Residual (log)")
    icaps.mirror_axis(ax)
    plt.savefig(plot_path + "MoI_Decline_Residuals_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Brownian Fractal Dimension (m/m_0 vs. r_g/r_0)
    # ===================================================================================
    # mixed
    _, ax = plt.subplots()
    ax.set_xlabel(r"$r_{\rm g} / r_{\rm 0}$")
    ax.set_ylabel(r"$m_{\rm brown} / m_{\rm 0}$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    mass_x = df_r1["mass_x"].to_numpy()
    mass_x_err = df_r1["mass_x_err"].to_numpy()
    mass_y = df_r1["mass_y"].to_numpy()
    mass_y_err = df_r1["mass_y_err"].to_numpy()
    n_x = mass_x / icaps.const.m_0
    n_x_low = (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
    n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err)
    n_y = mass_y / icaps.const.m_0
    n_y_low = (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
    n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err)
    n = np.append(n_x, n_y)
    n_low = np.append(n_x_low, n_y_low)
    n_high = np.append(n_x_high, n_y_high)
    moi = df_r1["inertia"].to_numpy()
    moi_err = df_r1["inertia_err"].to_numpy()
    r_x = np.sqrt(moi/mass_x) / icaps.const.r_0
    r_x_low = np.sqrt((moi - moi_err)/(mass_x + mass_x_err)) / (icaps.const.r_0 + icaps.const.r_0_err)
    r_x_high = np.sqrt((moi + moi_err)/(mass_x - mass_x_err)) / (icaps.const.r_0 - icaps.const.r_0_err)
    r_y = np.sqrt(moi / mass_y) / icaps.const.r_0
    r_y_low = np.sqrt((moi - moi_err) / (mass_y + mass_y_err)) / (icaps.const.r_0 + icaps.const.r_0_err)
    r_y_high = np.sqrt((moi + moi_err) / (mass_y - mass_y_err)) / (icaps.const.r_0 - icaps.const.r_0_err)
    r = np.append(r_x, r_y)
    r_low = np.append(r_x_low, r_y_low)
    r_high = np.append(r_x_high, r_y_high)
    ax.errorbar(r_x, n_x,
                xerr=[r_x - r_x_low, r_x_high - r_x],
                yerr=[n_x - n_x_low, n_x_high - n_x],
                fmt="o", c="black", markersize=3, label=r"$x$ (black), $y$ (gray)")
    ax.errorbar(r_y, n_y,
                xerr=[r_y - r_y_low, r_y_high - r_y],
                yerr=[n_y - n_y_low, n_y_high - n_y],
                fmt="o", c="gray", markersize=3)
    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_brown2_mixed = icaps.fit_model(icaps.fit_lin, np.log10(r), np.log10(n),
                                       xerr=icaps.log_err(r, np.max([r - r_low, r_high - r])),
                                       yerr=icaps.log_err(n, np.max([n - n_low, n_high - n])),
                                       fit_type=0)
    fracdim = out_brown2_mixed.beta[0]
    fracdim_err = out_brown2_mixed.sd_beta[0]
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown2_mixed.beta), c="red", lw=2, zorder=10, alpha=0.5,
            label=
                  # r"$\frac{m_{\rm brown}}{m_{\rm 0}} = \beta \left(\frac{r_{\rm g}}{r_{\rm 0}}\right)^{d_{\rm f}}$"
                  # + "\n" +
                  r"$\beta = {:.2f} \pm {:.2f}$".format(10 ** out_brown2_mixed.beta[1],
                                                        10 ** out_brown2_mixed.sd_beta[1]) + "\n" +
                  r"$d_{{\rm f}} = {:.2f} \pm {:.2f}$".format(fracdim, fracdim_err))
    b = 1.8
    out_brown2_mixed_ = icaps.fit_model(lambda q0, q1: icaps.fit_lin(q0, q1, np.log10(b)), np.log10(r), np.log10(n),
                                        xerr=icaps.log_err(r, np.max([r - r_low, r_high - r])),
                                        yerr=icaps.log_err(n, np.max([n - n_low, n_high - n])),
                                        fit_type=0)
    fracdim = out_brown2_mixed_.beta[0]
    fracdim_err = out_brown2_mixed_.sd_beta[0]
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), out_brown2_mixed_.beta[0], np.log10(b)),
            c="blue", lw=2, zorder=10, alpha=0.5, ls="--",
            label=r"$\beta = {}$".format(b) + "\n" +
                  r"$d_{{\rm f}} = {:.2f} \pm {:.2f}$".format(fracdim, fracdim_err))
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Brown2_mixed_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Brownian Fractal Dimension (MoI vs. Mass)
    # ===================================================================================
    # mean
    _, ax = plt.subplots()
    ax.set_xlabel(r"Brownian Mass (kg)")
    ax.set_ylabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass = df_r1["mass"].to_numpy()
    moi = df_r1["inertia"].to_numpy()
    mass_err = df_r1["mass_err"].to_numpy()
    moi_err = df_r1["inertia_err"].to_numpy()
    ax.errorbar(mass, moi, xerr=mass_err, yerr=moi_err,
                fmt="o", c="black", markersize=5)
    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_brown_mean = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(moi),
                                     xerr=icaps.log_err(mass, mass_err), yerr=icaps.log_err(moi, moi_err),
                                     fit_type=0)
    alpha = out_brown_mean.beta[0]
    alpha_err = out_brown_mean.sd_beta[0]
    fracdim = 2 / (alpha - 1)
    fracdim_low = 2 / (alpha + alpha_err - 1)
    fracdim_high = 2 / (alpha - alpha_err - 1)
    gamma = 10 ** out_brown_mean.beta[1]
    gamma_err = 10 ** out_brown_mean.sd_beta[1]
    beta = 1 / icaps.const.m_0 * (gamma / icaps.const.r_0 ** 2) ** (1 / (1 - alpha))
    beta_low = 1 / (icaps.const.m_0 + icaps.const.m_0_err) * (
            (gamma + gamma_err) / (icaps.const.r_0 - icaps.const.r_0_err) ** 2) ** (1 / (1 - (alpha + alpha_err)))
    beta_high = 1 / (icaps.const.m_0 - icaps.const.m_0_err) * (
            (gamma - gamma_err) / (icaps.const.r_0 + icaps.const.r_0_err) ** 2) ** (1 / (1 - (alpha - alpha_err)))
    beta_err = beta * np.sqrt((icaps.const.m_0_err / icaps.const.m_0) ** 2 +
                              (2/(1 - alpha) * icaps.const.r_0_err / icaps.const.r_0) ** 2 +
                              (1/(1 - alpha) * gamma_err / gamma) ** 2 +
                              (icaps.const.r_0 / (gamma * (1 - alpha) ** 3) * alpha_err) ** 2)
    label = icaps.get_param_label([[r"$I = \gamma m^\alpha$"],
                                   [r"$\gamma$", gamma, gamma_err],
                                   [r"$\alpha$", alpha, alpha_err],
                                   [r"$\beta$", beta, beta_err],
                                   [r"$d_\mathrm{f}$", fracdim, [fracdim_low - fracdim, fracdim_high - fracdim]]])
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown_mean.beta),
            c="red", lw=2, zorder=10, alpha=0.5, label=label)
    ax.legend(loc="lower right")
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Brown_mean_R1.png", dpi=dpi)
    plt.close()

    # ===========================
    # mixed
    _, ax = plt.subplots()
    ax.set_xlabel(r"Brownian Mass (kg)")
    ax.set_ylabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass_x = df_r1["mass_x"].to_numpy()
    mass_x_err = df_r1["mass_x_err"].to_numpy()
    mass_y = df_r1["mass_y"].to_numpy()
    mass_y_err = df_r1["mass_y_err"].to_numpy()
    mass = np.append(mass_x, mass_y)
    mass_err = np.append(mass_x_err, mass_y_err)
    moi = df_r1["inertia"].to_numpy()
    moi_err = df_r1["inertia_err"].to_numpy()
    ax.errorbar(mass_x, moi, xerr=mass_x_err, yerr=moi_err,
                fmt="o", c="black", markersize=3, label=r"$x$ (black), $y$ (gray)")
    ax.errorbar(mass_y, moi, xerr=mass_y_err, yerr=moi_err,
                fmt="o", c="gray", markersize=3)
    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    moi = np.tile(moi, 2)
    moi_err = np.tile(moi_err, 2)
    out_brown_mixed = icaps.fit_model(icaps.fit_lin, np.log10(mass), np.log10(moi),
                                      xerr=icaps.log_err(mass, mass_err), yerr=icaps.log_err(moi, moi_err),
                                      fit_type=0)
    alpha = out_brown_mixed.beta[0]
    alpha_err = out_brown_mixed.sd_beta[0]
    fracdim = 2 / (alpha - 1)
    fracdim_low = 2 / (alpha + alpha_err - 1)
    fracdim_high = 2 / (alpha - alpha_err - 1)
    gamma = 10 ** out_brown_mixed.beta[1]
    gamma_err = 10 ** out_brown_mixed.sd_beta[1]
    beta = 1 / icaps.const.m_0 * (gamma / icaps.const.r_0 ** 2) ** (1 / (1 - alpha))
    beta_low = 1 / (icaps.const.m_0 + icaps.const.m_0_err) * (
            (gamma + gamma_err) / (icaps.const.r_0 - icaps.const.r_0_err) ** 2) ** (1 / (1 - (alpha + alpha_err)))
    beta_high = 1 / (icaps.const.m_0 - icaps.const.m_0_err) * (
            (gamma - gamma_err) / (icaps.const.r_0 + icaps.const.r_0_err) ** 2) ** (1 / (1 - (alpha - alpha_err)))
    beta_err = beta * np.sqrt((icaps.const.m_0_err / icaps.const.m_0) ** 2 +
                              (2/(1 - alpha) * icaps.const.r_0_err / icaps.const.r_0) ** 2 +
                              (1/(1 - alpha) * gamma_err / gamma) ** 2 +
                              (icaps.const.r_0 / (gamma * (1 - alpha) ** 3) * alpha_err) ** 2)
    print(beta_low, beta_high, beta, beta - beta_low, beta_high - beta, beta_err)
    label = icaps.get_param_label([[r"$I = \gamma m^\alpha$"],
                                   [r"$\gamma$", gamma, gamma_err],
                                   [r"$\alpha$", alpha, alpha_err],
                                   [r"$\beta$", beta, beta_err],
                                   [r"$d_\mathrm{f}$", fracdim, [fracdim_low - fracdim, fracdim_high - fracdim]]])
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown_mixed.beta),
            c="red", lw=2, zorder=10, alpha=0.5, label=label)
    ax.legend(loc="lower right")
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Brown_mixed_R1.png", dpi=dpi)
    plt.close()

    # ===========================
    # mixed (normalized)
    _, ax = plt.subplots()
    ax.set_xlabel(r"$m / m_{\rm 0}$")
    ax.set_ylabel(r"$I / I_{\rm 0}$")
    # ax.set_xlabel(r"$m_\mathrm{brown} / m_{\rm 0}$")
    # ax.set_ylabel(r"$I_\mathrm{brown} / I_{\rm 0}$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    # ax.set_xlim(*mass_lims)
    # ax.set_ylim(*moi_lims)
    mass_x = df_r1["mass_x"].to_numpy()
    mass_x_err = df_r1["mass_x_err"].to_numpy()
    mass_y = df_r1["mass_y"].to_numpy()
    mass_y_err = df_r1["mass_y_err"].to_numpy()
    n_x = mass_x / icaps.const.m_0
    n_x_low = (mass_x - mass_x_err) / (icaps.const.m_0 + icaps.const.m_0_err)
    n_x_high = (mass_x + mass_x_err) / (icaps.const.m_0 - icaps.const.m_0_err)
    n_y = mass_y / icaps.const.m_0
    n_y_low = (mass_y - mass_y_err) / (icaps.const.m_0 + icaps.const.m_0_err)
    n_y_high = (mass_y + mass_y_err) / (icaps.const.m_0 - icaps.const.m_0_err)
    n_m = np.append(n_x, n_y)
    n_m_err = np.append(np.max([n_x - n_x_low, n_x_high - n_x], axis=0),
                        np.max([n_y - n_y_low, n_y_high - n_y], axis=0))
    moi = df_r1["inertia"].to_numpy()
    moi_err = df_r1["inertia_err"].to_numpy()
    n_moi = moi / icaps.const.moi_0
    n_moi_low = (moi - moi_err) / (icaps.const.moi_0 + icaps.const.moi_0_err)
    n_moi_high = (moi + moi_err) / (icaps.const.moi_0 - icaps.const.moi_0_err)
    ax.errorbar(n_x, n_moi, xerr=[n_x - n_x_low, n_x_high - n_x], yerr=[n_moi - n_moi_low, n_moi_high - n_moi],
                fmt="o", c="black", markersize=3, label=r"$x$ (black), $y$ (gray)")
    ax.errorbar(n_y, n_moi, xerr=[n_y - n_y_low, n_y_high - n_y], yerr=[n_moi - n_moi_low, n_moi_high - n_moi],
                fmt="o", c="gray", markersize=3)
    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    n_moi = np.tile(n_moi, 2)
    n_moi_low = np.tile(n_moi_low, 2)
    n_moi_high = np.tile(n_moi_high, 2)
    n_moi_err = np.max([n_moi - n_moi_low, n_moi_high - n_moi], axis=0)
    out_brown_n_mixed = icaps.fit_model(icaps.fit_lin, np.log10(n_m), np.log10(n_moi),
                                        xerr=icaps.log_err(n_m, n_m_err), yerr=icaps.log_err(n_moi, n_moi_err),
                                        fit_type=0)
    alpha = out_brown_n_mixed.beta[0]
    alpha_err = out_brown_n_mixed.sd_beta[0]
    fracdim = 2 / (alpha - 1)
    fracdim_low = 2 / (alpha + alpha_err - 1)
    fracdim_high = 2 / (alpha - alpha_err - 1)
    gamma = 10 ** out_brown_n_mixed.beta[1]
    gamma_err = 10 ** out_brown_n_mixed.sd_beta[1]
    # beta = 1 / icaps.const.m_0 * (gamma / icaps.const.r_0 ** 2) ** (1 / (1 - alpha))
    # beta_low = 1 / (icaps.const.m_0 + icaps.const.m_0_err) * (
    #         (gamma + gamma_err) / (icaps.const.r_0 - icaps.const.r_0_err) ** 2) ** (1 / (1 - (alpha + alpha_err)))
    # beta_high = 1 / (icaps.const.m_0 - icaps.const.m_0_err) * (
    #         (gamma - gamma_err) / (icaps.const.r_0 + icaps.const.r_0_err) ** 2) ** (1 / (1 - (alpha - alpha_err)))
    label = icaps.get_param_label([[r"$I = \gamma m^\alpha$"],
                                   [r"$\gamma$", gamma, gamma_err],
                                   [r"$\alpha$", alpha, alpha_err],
                                   # [r"$\beta$", beta, beta_err],
                                   [r"$d_\mathrm{f}$", fracdim, [fracdim_low - fracdim, fracdim_high - fracdim]]])
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown_n_mixed.beta),
            c="red", lw=2, zorder=10, alpha=0.5, label=label)
    ax.legend(loc="lower right")
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Brown_mixed_norm_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Optical Fractal Dimension (adapted to MoI decline)
    # ===================================================================================
    # full table (long)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Optical Mass (kg)")
    ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass_opt = df_t1[ldm_ex_col].to_numpy() / this_ldm_ex0 * icaps.const.m_0
    mass_opt_err = mass_opt * this_ldm_ex0 * np.sqrt((df_t1[ldm_ex_err_col].to_numpy() /
                                                      df_t1[ldm_ex_col].to_numpy()) ** 2
                                                     + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    mass_opt_low = (mass_opt * this_ldm_ex0 - mass_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    mass_opt_high = (mass_opt * this_ldm_ex0 + mass_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)

    moi_opt = mass_opt * (df_t1["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
    moi_opt, fails = adapt_opt(moi_opt, ret_fails=True)    # todo: Warmfix
    moi_opt_err = moi_opt * this_ldm_ex0 * np.sqrt((mass_opt_err / (mass_opt * this_ldm_ex0)) ** 2
                                                   + (df_t1["mean_gyrad_err"].to_numpy() /
                                                      df_t1["mean_gyrad"].to_numpy()) ** 2)
    moi_opt_low = (moi_opt * this_ldm_ex0 - moi_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    moi_opt_high = (moi_opt * this_ldm_ex0 + moi_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    mass_opt = mass_opt[moi_opt > 0]
    mass_opt_low = mass_opt_low[moi_opt > 0]
    mass_opt_high = mass_opt_high[moi_opt > 0]
    moi_opt_low = moi_opt_low[moi_opt > 0]
    moi_opt_high = moi_opt_high[moi_opt > 0]
    moi_opt = moi_opt[moi_opt > 0]  # todo: Hotfix

    ax.errorbar(mass_opt, moi_opt,
                xerr=[mass_opt - mass_opt_low, mass_opt_high - mass_opt],
                yerr=[moi_opt - moi_opt_low, moi_opt_high - moi_opt],
                fmt="o", c="gray", markersize=3, alpha=1)
    ax.errorbar(mass_opt[~fails], moi_opt[~fails], fmt="o", c="black", markersize=3)
    ax.errorbar(mass_opt[fails], moi_opt[fails], fmt="o", mfc="none", mec="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_opt_long = icaps.fit_model(icaps.fit_lin, np.log10(mass_opt[(~fails) & (moi_opt > 1e-24)]), np.log10(moi_opt[(~fails) & (moi_opt > 1e-24)]),
                                   xerr=icaps.log_err(mass_opt[(~fails) & (moi_opt > 1e-24)],
                                                       np.mean([mass_opt[(~fails) & (moi_opt > 1e-24)] - mass_opt_low[(~fails) & (moi_opt > 1e-24)],
                                                                mass_opt_high[(~fails) & (moi_opt > 1e-24)] - mass_opt[(~fails) & (moi_opt > 1e-24)]], axis=0)),
                                   yerr=icaps.log_err(moi_opt[(~fails) & (moi_opt > 1e-24)],
                                                      np.mean([moi_opt[(~fails) & (moi_opt > 1e-24)] - moi_opt_low[(~fails) & (moi_opt > 1e-24)],
                                                               moi_opt_high[(~fails) & (moi_opt > 1e-24)] - moi_opt[(~fails) & (moi_opt > 1e-24)]], axis=0)),
                                   fit_type=0)
    fracdim = 2 / (out_opt_long.beta[0] - 1)
    fracdim_low = 2 / (out_opt_long.beta[0] + out_opt_long.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_long.beta[0] - out_opt_long.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_long.beta), c="red", lw=2, zorder=10, alpha=0.5,
            label=r"$I = \gamma m^\alpha$" + "\n" +
                  r"$\gamma = {:.0f} \pm {:.0f}$".format(10 ** out_opt_long.beta[1],
                                                 10 ** out_opt_long.sd_beta[1]) + "\n" +
                  r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_long.beta[0], out_opt_long.sd_beta[0]) + "\n" +
                  r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                  fracdim_low - fracdim))
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Opt_T1.png", dpi=dpi)
    plt.close()

    # ===========================
    # full table (long, unadapted)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Optical Mass (kg)")
    ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass_opt = df_t1[ldm_ex_col].to_numpy() / this_ldm_ex0 * icaps.const.m_0
    mass_opt_err = mass_opt * this_ldm_ex0 * np.sqrt((df_t1[ldm_ex_err_col].to_numpy() /
                                                      df_t1[ldm_ex_col].to_numpy()) ** 2
                                                     + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    mass_opt_low = (mass_opt * this_ldm_ex0 - mass_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    mass_opt_high = (mass_opt * this_ldm_ex0 + mass_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    moi_opt = mass_opt * (df_t1["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
    moi_opt_err = moi_opt * this_ldm_ex0 * np.sqrt((mass_opt_err / (mass_opt * this_ldm_ex0)) ** 2
                                                   + (df_t1["mean_gyrad_err"].to_numpy() /
                                                      df_t1["mean_gyrad"].to_numpy()) ** 2)
    moi_opt_low = (moi_opt * this_ldm_ex0 - moi_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    moi_opt_high = (moi_opt * this_ldm_ex0 + moi_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    mass_opt = mass_opt[moi_opt > 0]
    mass_opt_low = mass_opt_low[moi_opt > 0]
    mass_opt_high = mass_opt_high[moi_opt > 0]
    moi_opt_low = moi_opt_low[moi_opt > 0]
    moi_opt_high = moi_opt_high[moi_opt > 0]
    moi_opt = moi_opt[moi_opt > 0]  # todo: Hotfix

    ax.errorbar(mass_opt, moi_opt,
                xerr=[mass_opt - mass_opt_low, mass_opt_high - mass_opt],
                yerr=[moi_opt - moi_opt_low, moi_opt_high - moi_opt],
                fmt="o", c="gray", markersize=3, alpha=1)
    ax.errorbar(mass_opt, moi_opt, fmt="o", c="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_opt_long = icaps.fit_model(icaps.fit_lin, np.log10(mass_opt[moi_opt > 1e-24]), np.log10(moi_opt[moi_opt > 1e-24]),
                                   xerr=icaps.log_err(mass_opt[moi_opt > 1e-24], np.mean([mass_opt[moi_opt > 1e-24] - mass_opt_low[moi_opt > 1e-24],
                                                                        mass_opt_high[moi_opt > 1e-24] - mass_opt[moi_opt > 1e-24]], axis=0)),
                                   yerr=icaps.log_err(moi_opt[moi_opt > 1e-24], np.mean([moi_opt[moi_opt > 1e-24] - moi_opt_low[moi_opt > 1e-24],
                                                                        moi_opt_high[moi_opt > 1e-24] - moi_opt[moi_opt > 1e-24]], axis=0)),
                                   fit_type=0)
    fracdim = 2 / (out_opt_long.beta[0] - 1)
    fracdim_low = 2 / (out_opt_long.beta[0] + out_opt_long.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_long.beta[0] - out_opt_long.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_long.beta), c="red", lw=2, zorder=10, alpha=0.5,
            label=r"$I = \gamma m^\alpha$" + "\n" +
                  r"$\gamma = {:.0f} \pm {:.0f}$".format(10 ** out_opt_long.beta[1],
                                                 10 ** out_opt_long.sd_beta[1]) + "\n" +
                  r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_long.beta[0], out_opt_long.sd_beta[0]) + "\n" +
                  r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                  fracdim_low - fracdim))
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Opt_T1_unadapted.png", dpi=dpi)
    plt.close()

    # ===========================
    # tau table (medium)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Optical Mass (kg)")
    ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass_opt = df_t2[ldm_ex_col].to_numpy() / this_ldm_ex0 * icaps.const.m_0
    mass_opt_err = mass_opt * this_ldm_ex0 * np.sqrt((df_t2[ldm_ex_err_col].to_numpy() /
                                                      df_t2[ldm_ex_col].to_numpy()) ** 2
                                                     + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    mass_opt_low = (mass_opt * this_ldm_ex0 - mass_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    mass_opt_high = (mass_opt * this_ldm_ex0 + mass_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    moi_opt = mass_opt * (df_t2["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
    moi_opt, fails = adapt_opt(moi_opt, ret_fails=True)  # todo: Warmfix
    moi_opt_err = moi_opt * this_ldm_ex0 * np.sqrt((mass_opt_err / (mass_opt * this_ldm_ex0)) ** 2
                                                   + (df_t2["mean_gyrad_err"].to_numpy() /
                                                      df_t2["mean_gyrad"].to_numpy()) ** 2)
    moi_opt_low = (moi_opt * this_ldm_ex0 - moi_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    moi_opt_high = (moi_opt * this_ldm_ex0 + moi_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    mass_opt = mass_opt[moi_opt > 0]
    mass_opt_low = mass_opt_low[moi_opt > 0]
    mass_opt_high = mass_opt_high[moi_opt > 0]
    moi_opt_low = moi_opt_low[moi_opt > 0]
    moi_opt_high = moi_opt_high[moi_opt > 0]
    moi_opt = moi_opt[moi_opt > 0]  # todo: Hotfix

    ax.errorbar(mass_opt, moi_opt,
                xerr=[mass_opt - mass_opt_low, mass_opt_high - mass_opt],
                yerr=[moi_opt - moi_opt_low, moi_opt_high - moi_opt],
                fmt="o", c="gray", markersize=3, alpha=1)
    ax.errorbar(mass_opt[~fails], moi_opt[~fails], fmt="o", c="black", markersize=3)
    ax.errorbar(mass_opt[fails], moi_opt[fails], fmt="o", mfc="none", mec="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_opt_medium = icaps.fit_model(icaps.fit_lin, np.log10(mass_opt[(~fails) & (moi_opt > 1e-24)]),
                                   np.log10(moi_opt[(~fails) & (moi_opt > 1e-24)]),
                                   xerr=icaps.log_err(mass_opt[(~fails) & (moi_opt > 1e-24)],
                                                      np.mean([mass_opt[(~fails) & (moi_opt > 1e-24)] - mass_opt_low[
                                                          (~fails) & (moi_opt > 1e-24)],
                                                               mass_opt_high[(~fails) & (moi_opt > 1e-24)] - mass_opt[
                                                                   (~fails) & (moi_opt > 1e-24)]], axis=0)),
                                   yerr=icaps.log_err(moi_opt[(~fails) & (moi_opt > 1e-24)],
                                                      np.mean([moi_opt[(~fails) & (moi_opt > 1e-24)] - moi_opt_low[
                                                          (~fails) & (moi_opt > 1e-24)],
                                                               moi_opt_high[(~fails) & (moi_opt > 1e-24)] - moi_opt[
                                                                   (~fails) & (moi_opt > 1e-24)]], axis=0)),
                                   fit_type=0)
    fracdim = 2 / (out_opt_medium.beta[0] - 1)
    fracdim_low = 2 / (out_opt_medium.beta[0] + out_opt_medium.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_medium.beta[0] - out_opt_medium.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_medium.beta), c="red", lw=2, zorder=10, alpha=0.5,
            label=r"$I = \gamma m^\alpha$" + "\n" +
                  r"$\gamma = {:.0f} \pm {:.0f}$".format(10 ** out_opt_medium.beta[1],
                                                 10 ** out_opt_medium.sd_beta[1]) + "\n" +
                  r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_medium.beta[0], out_opt_medium.sd_beta[0]) + "\n"
                  + r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                    fracdim_low - fracdim))
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Opt_T2.png", dpi=dpi)
    plt.close()

    # ===========================
    # tau table (medium, unadapted)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Optical Mass (kg)")
    ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass_opt = df_t2[ldm_ex_col].to_numpy() / this_ldm_ex0 * icaps.const.m_0
    mass_opt_err = mass_opt * this_ldm_ex0 * np.sqrt((df_t2[ldm_ex_err_col].to_numpy() /
                                                      df_t2[ldm_ex_col].to_numpy()) ** 2
                                                     + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    mass_opt_low = (mass_opt * this_ldm_ex0 - mass_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    mass_opt_high = (mass_opt * this_ldm_ex0 + mass_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    moi_opt = mass_opt * (df_t2["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
    moi_opt_err = moi_opt * this_ldm_ex0 * np.sqrt((mass_opt_err / (mass_opt * this_ldm_ex0)) ** 2
                                                   + (df_t2["mean_gyrad_err"].to_numpy() /
                                                      df_t2["mean_gyrad"].to_numpy()) ** 2)
    moi_opt_low = (moi_opt * this_ldm_ex0 - moi_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    moi_opt_high = (moi_opt * this_ldm_ex0 + moi_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    mass_opt = mass_opt[moi_opt > 0]
    mass_opt_low = mass_opt_low[moi_opt > 0]
    mass_opt_high = mass_opt_high[moi_opt > 0]
    moi_opt_low = moi_opt_low[moi_opt > 0]
    moi_opt_high = moi_opt_high[moi_opt > 0]
    moi_opt = moi_opt[moi_opt > 0]  # todo: Hotfix

    ax.errorbar(mass_opt, moi_opt,
                xerr=[mass_opt - mass_opt_low, mass_opt_high - mass_opt],
                yerr=[moi_opt - moi_opt_low, moi_opt_high - moi_opt],
                fmt="o", c="gray", markersize=3, alpha=1)
    ax.errorbar(mass_opt, moi_opt, fmt="o", c="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_opt_medium = icaps.fit_model(icaps.fit_lin, np.log10(mass_opt[moi_opt > 1e-24]),
                                   np.log10(moi_opt[moi_opt > 1e-24]),
                                   xerr=icaps.log_err(mass_opt[moi_opt > 1e-24], np.mean(
                                       [mass_opt[moi_opt > 1e-24] - mass_opt_low[moi_opt > 1e-24],
                                        mass_opt_high[moi_opt > 1e-24] - mass_opt[moi_opt > 1e-24]], axis=0)),
                                   yerr=icaps.log_err(moi_opt[moi_opt > 1e-24],
                                                      np.mean([moi_opt[moi_opt > 1e-24] - moi_opt_low[moi_opt > 1e-24],
                                                               moi_opt_high[moi_opt > 1e-24] - moi_opt[
                                                                   moi_opt > 1e-24]], axis=0)),
                                   fit_type=0)
    fracdim = 2 / (out_opt_medium.beta[0] - 1)
    fracdim_low = 2 / (out_opt_medium.beta[0] + out_opt_medium.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_medium.beta[0] - out_opt_medium.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_medium.beta), c="red", lw=2, zorder=10, alpha=0.5,
            label=r"$I = \gamma m^\alpha$" + "\n" +
                  r"$\gamma = {:.0f} \pm {:.0f}$".format(10 ** out_opt_medium.beta[1],
                                                 10 ** out_opt_medium.sd_beta[1]) + "\n" +
                  r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_medium.beta[0], out_opt_medium.sd_beta[0]) + "\n" +
                  r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                  fracdim_low - fracdim))
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Opt_T2_unadapted.png", dpi=dpi)
    plt.close()

    # ===========================
    # brownian table (short)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Optical Mass (kg)")
    ax.set_ylabel(r"Optical Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass_opt = df_r1[ldm_ex_col].to_numpy() / this_ldm_ex0 * icaps.const.m_0
    mass_opt_err = mass_opt * this_ldm_ex0 * np.sqrt((df_r1[ldm_ex_err_col].to_numpy() /
                                                      df_r1[ldm_ex_col].to_numpy()) ** 2
                                                     + (icaps.const.m_0_err / icaps.const.m_0) ** 2)
    mass_opt_low = (mass_opt * this_ldm_ex0 - mass_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    mass_opt_high = (mass_opt * this_ldm_ex0 + mass_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    moi_opt = mass_opt * (df_r1["mean_gyrad"].to_numpy() * icaps.const.px_ldm / 1e6) ** 2
    moi_opt, fails = adapt_opt(moi_opt, ret_fails=True)    # todo: Warmfix
    moi_opt_err = moi_opt * this_ldm_ex0 * np.sqrt((mass_opt_err / (mass_opt * this_ldm_ex0)) ** 2
                                                   + (df_r1["mean_gyrad_err"].to_numpy() /
                                                      df_r1["mean_gyrad"].to_numpy()) ** 2)
    moi_opt_low = (moi_opt * this_ldm_ex0 - moi_opt_err) / (this_ldm_ex0 + this_ldm_ex0_err)
    moi_opt_high = (moi_opt * this_ldm_ex0 + moi_opt_err) / (this_ldm_ex0 - this_ldm_ex0_err)
    mass_opt = mass_opt[moi_opt > 0]
    mass_opt_low = mass_opt_low[moi_opt > 0]
    mass_opt_high = mass_opt_high[moi_opt > 0]
    moi_opt_low = moi_opt_low[moi_opt > 0]
    moi_opt_high = moi_opt_high[moi_opt > 0]
    moi_opt = moi_opt[moi_opt > 0]  # todo: Hotfix

    ax.errorbar(mass_opt, moi_opt,
                xerr=[mass_opt - mass_opt_low, mass_opt_high - mass_opt],
                yerr=[moi_opt - moi_opt_low, moi_opt_high - moi_opt],
                fmt="o", c="gray", markersize=3, alpha=1)
    ax.errorbar(mass_opt[~fails], moi_opt[~fails], fmt="o", c="black", markersize=3)
    ax.errorbar(mass_opt[fails], moi_opt[fails], fmt="o", mfc="none", mec="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    out_opt_short = icaps.fit_model(icaps.fit_lin, np.log10(mass_opt[~fails]), np.log10(moi_opt[~fails]),
                                    xerr=icaps.log_err(mass_opt[~fails],
                                                       np.mean([mass_opt[~fails] - mass_opt_low[~fails],
                                                                mass_opt_high[~fails] - mass_opt[~fails]], axis=0)),
                                    yerr=icaps.log_err(moi_opt[~fails],
                                                       np.mean([moi_opt[~fails] - moi_opt_low[~fails],
                                                                moi_opt_high[~fails] - moi_opt[~fails]], axis=0)),
                                    fit_type=0)
    fracdim = 2 / (out_opt_short.beta[0] - 1)
    fracdim_low = 2 / (out_opt_short.beta[0] + out_opt_short.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_short.beta[0] - out_opt_short.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_short.beta), c="red", lw=2, zorder=10, alpha=0.5,
            label=r"$I = \gamma m^\alpha$" + "\n" +
                  r"$\gamma = {:.0f} \pm {:.0f}$".format(10 ** out_opt_short.beta[1],
                                                 10 ** out_opt_short.sd_beta[1]) + "\n" +
                  r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_short.beta[0], out_opt_short.sd_beta[0]) + "\n" +
                  r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                  fracdim_low - fracdim))
    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Opt_R1.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # Fractal Dimension Comparison
    # ===================================================================================
    _, ax = plt.subplots()
    ax.set_xlabel(r"Brownian Mass (kg)")
    ax.set_ylabel(r"Brownian Moment of Inertia (kgm$\rm^2$)")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlim(*mass_lims)
    ax.set_ylim(*moi_lims)
    mass = df_r1["mass"].to_numpy()
    mass_err = df_r1["mass_err"].to_numpy()
    moi = df_r1["inertia"].to_numpy()
    moi_err = df_r1["inertia_err"].to_numpy()
    ax.errorbar(mass, moi, xerr=mass_err, yerr=moi_err,
                fmt="o", c="black", markersize=3)

    xspace = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], 1000)
    fracdim = 2 / (out_brown_mean.beta[0] - 1)
    fracdim_low = 2 / (out_brown_mean.beta[0] + out_brown_mean.sd_beta[0] - 1)
    fracdim_high = 2 / (out_brown_mean.beta[0] - out_brown_mean.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown_mean.beta),
            c="red", lw=2, ls="-", zorder=10, alpha=0.5,
            label="Brownian fit (mean)\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_brown_mean.beta[0],
                                                                                   out_brown_mean.sd_beta[0]) + "\n"
                  + r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                    fracdim_low - fracdim))
    fracdim = 2 / (out_brown_mixed.beta[0] - 1)
    fracdim_low = 2 / (out_brown_mixed.beta[0] + out_brown_mixed.sd_beta[0] - 1)
    fracdim_high = 2 / (out_brown_mixed.beta[0] - out_brown_mixed.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_brown_mixed.beta),
            c="darkred", lw=2, ls="-.", zorder=10, alpha=0.5,
            label="Brownian fit (mixed)\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_brown_mixed.beta[0],
                                                                                    out_brown_mixed.sd_beta[0]) + "\n"
                  + r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                    fracdim_low - fracdim))
    fracdim = 2 / (out_opt_long.beta[0] - 1)
    fracdim_low = 2 / (out_opt_long.beta[0] + out_opt_long.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_long.beta[0] - out_opt_long.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_long.beta),
            c="lime", lw=2, ls=":", zorder=10, alpha=0.5,
            label="Optical fit (T1)\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_long.beta[0],
                                                                                out_opt_long.sd_beta[0]) + "\n"
                  + r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                    fracdim_low - fracdim))

    fracdim = 2 / (out_opt_medium.beta[0] - 1)
    fracdim_low = 2 / (out_opt_medium.beta[0] + out_opt_medium.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_medium.beta[0] - out_opt_medium.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_medium.beta),
            c="green", lw=2, ls="--", zorder=10, alpha=0.5,
            label="Optical fit (T2)\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_medium.beta[0],
                                                                                out_opt_medium.sd_beta[0]) + "\n"
                  + r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                    fracdim_low - fracdim))

    fracdim = 2 / (out_opt_short.beta[0] - 1)
    fracdim_low = 2 / (out_opt_short.beta[0] + out_opt_short.sd_beta[0] - 1)
    fracdim_high = 2 / (out_opt_short.beta[0] - out_opt_short.sd_beta[0] - 1)
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_opt_short.beta),
            c="darkgreen", lw=2, ls=":", zorder=10, alpha=0.5,
            label="Optical fit (R1)\n" + r"$\alpha = {:.2f} \pm {:.2f}$".format(out_opt_short.beta[0],
                                                                                out_opt_short.sd_beta[0]) + "\n"
                  + r"$d_{{\rm f}} = {:.2f}^{{+{:.2f}}}_{{{:.2f}}}$".format(fracdim, fracdim_high - fracdim,
                                                                    fracdim_low - fracdim))

    ax.legend(fancybox=fancybox)
    icaps.mirror_axis(ax)
    plt.tight_layout()
    plt.savefig(plot_path + "D_Comp_R1.png", dpi=dpi)
    plt.close()
    """


if __name__ == '__main__':
    main()

