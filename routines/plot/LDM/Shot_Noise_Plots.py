import os
import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import scipy.signal
import scipy.stats
from scipy.fft import fft, fftfreq

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_Noise/"
plot_path = project_path + "plots/"
orig_path = project_path + "orig/"
ff_path = drive_letter + ":/icaps/data/FFs/ff_216700_260300.bmp"
csv_fstats = project_path + "frame_stats.csv"
csv_fboxes = project_path + "frame_boxes.csv"
csv_flat_hist = project_path + "flat_hist_nocloud.csv"
csv_stats = project_path + "stats_nocloud.csv"
npy_total = project_path + "total_nocloud.npy"
npy_px_hist = project_path + "px_hist_nocloud.npy"
icaps.mk_folders([plot_path])

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    # df = pd.read_csv(csv_flat_hist)
    # _, ax = plt.subplots()
    # ax.bar(df["value"] / 256, df["count"], width=0.9 / 256, color="black")
    # ax.set_xlim(0.7, 0.825)
    # ax.set_ylim(0, 1.05 * np.max(df["count"]))
    # icaps.mirror_axis(ax)
    # plt.show()

    ff_start, ff_stop = map(int, ff_path.split("/")[-1].split(".")[0].split("_")[1:])
    df_box = pd.read_csv(csv_fboxes)
    frames = df_box["frame"].to_numpy()
    time = frames / icaps.const.fps_ldm
    corners = ["tl", "tr", "bl", "br"]

    # df = pd.read_csv(csv_fstats)
    # fig, ax = plt.subplots()
    # idx = (df["frame"] <= ff_stop) & (df["frame"] >= ff_start + 1000)
    # ax.errorbar(df.loc[idx, "frame"] / icaps.const.fps_ldm, df.loc[idx, "mean"],
    #             c="black", markersize=1, fmt=".", lw=1, zorder=100)
    # _, ax = plt.subplots()
    # y = df.loc[idx, "mean"]
    # y = np.cumsum(y)
    # y = np.append(np.array([0.]), y)
    # yf = fft(y)
    # n = len(y)
    # xf = fftfreq(n, 1 / icaps.const.fps_ldm)[:n // 2]
    # yfp = 1 / (n * icaps.const.fps_ldm) * (np.abs(yf[0:n // 2])) ** 2
    # ax.scatter(xf, yfp, c="black", s=2, marker=".")
    # ax.set_yscale("log")
    # plt.show()

    # ff = icaps.load_img(".".join(ff_path.split(".")[:-1]) + ".npy").astype(float)
    # frames = np.arange(ff_start, ff_stop + 1)
    # nan_arr = np.zeros(len(frames))
    # nan_arr[:] = np.nan
    # df_nstats = pd.DataFrame(data={"frame": frames, "mean": nan_arr, "median": nan_arr, "std": nan_arr,
    #                                "abs_mean": nan_arr, "abs_median": nan_arr, "abs_std": nan_arr,
    #                                "min": nan_arr, "max": nan_arr, "abs_max": nan_arr,
    #                                "mean2": nan_arr, "median2": nan_arr, "std2": nan_arr,
    #                                "min2": nan_arr, "max2": nan_arr,
    #                                "abs_mean2": nan_arr, "abs_median2": nan_arr, "abs_std2": nan_arr})
    # pb = icaps.SimpleProgressBar(len(frames))
    # ff_mean = np.mean(ff)
    # for i, frame in enumerate(frames):
    #    texus_path = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame)
    #    img = icaps.load_img(texus_path + icaps.generate_file(frame), strip=True).astype(float)
    #    noise = img - ff
    #    abs_noise = np.abs(noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "mean"] = np.mean(noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "median"] = np.median(noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "std"] = np.std(noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_mean"] = np.mean(abs_noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_median"] = np.median(abs_noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_std"] = np.std(abs_noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "min"] = np.min(noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "max"] = np.max(noise)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_max"] = np.max(abs_noise)
    #    noise2 = img - ff / ff_mean * np.mean(img)
    #    abs_noise2 = np.abs(noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "mean2"] = np.mean(noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "median2"] = np.median(noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "std2"] = np.std(noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_mean2"] = np.mean(abs_noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_median2"] = np.median(abs_noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_std2"] = np.std(abs_noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "min2"] = np.min(noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "max2"] = np.max(noise2)
    #    df_nstats.loc[df_nstats["frame"] == frame, "abs_max2"] = np.max(abs_noise2)
    #    pb.tick()
    # pb.close()
    # df_nstats.to_csv(project_path + "noise_stats.csv", index=False)

    df_nstats = pd.read_csv(project_path + "noise_stats.csv")
    _, ax = plt.subplots()
    ax.errorbar(df_nstats["frame"] / icaps.const.fps_ldm, df_nstats["abs_mean2"],
                c="gray", markersize=3, fmt=".", label="Abs Mean", zorder=10)
    ax.errorbar(df_nstats["frame"] / icaps.const.fps_ldm, df_nstats["abs_median2"],
                c="magenta", markersize=3, fmt=".", label="Abs Median", zorder=10)
    ax.errorbar(df_nstats["frame"] / icaps.const.fps_ldm, df_nstats["abs_std2"],
                c="black", markersize=3, fmt=".", label="Abs Std", zorder=10)
    # ax.errorbar(df_nstats["frame"] / icaps.const.fps_ldm, df_nstats["abs_max2"],
    #             c="red", markersize=3, fmt="^", label="Abs. Max")
    ax.errorbar(df_nstats["frame"] / icaps.const.fps_ldm, df_nstats["max2"],
                c="red", markersize=3, fmt="x", label="Max")
    # ax.errorbar(df_nstats["frame"] / icaps.const.fps_ldm, df_nstats["min"],
    #             c="blue", markersize=3, fmt="+", label="Min")
    ax.legend()
    # plt.show()
    plt.close()

    # ff = icaps.load_img(".".join(ff_path.split(".")[:-1]) + ".npy")
    # bin_size = 0.25
    # bins = np.arange(-6 - bin_size / 2, 6 + bin_size, bin_size)
    # hist = np.zeros(len(bins) - 1, dtype=np.uint64)
    # pb = icaps.SimpleProgressBar(ff_stop - ff_start)
    # for frame in range(ff_start, ff_stop + 1):
    #     file = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame)
    #     img = icaps.load_img(file + icaps.generate_file(frame), strip=True).astype(float)
    #     noise = img - ff
    #     hist_, _ = np.histogram(noise, bins)
    #     hist += hist_.astype(np.uint64)
    #     pb.tick()
    # pb.close()
    # df_hist = pd.DataFrame(data={"value": bins[:-1] + bin_size / 2, "count": hist})
    # df_hist.to_csv(project_path + "noise_hist.csv", index=False)

    df_hist = pd.read_csv(project_path + "noise_hist.csv")
    vals = df_hist["value"].to_numpy()
    counts = df_hist["count"].to_numpy()
    avrg = np.average(vals, weights=counts)
    std = np.sqrt(np.average((vals - avrg) ** 2, weights=counts))
    print(avrg)
    print(std, 2 * std, 3 * std, 4 * std)
    _, ax = plt.subplots()
    ax.set_xlim(-4, 4)
    print(np.sum(counts))
    print((1024 * 1024 * (ff_stop - ff_start + 1)))
    # ax.bar(vals, counts / (1024 * 1024 * (ff_stop - ff_start + 1)), width=0.25 * 0.9, color="black")
    ax.bar(vals, counts / np.sum(counts), width=0.25 * 0.9, color="black")
    xspace = np.linspace(-5, 5, 1000)
    print(np.sum(counts / np.sum(counts)))
    ax.plot(xspace, 1/4 * 1 / np.sqrt(2 * np.pi * std ** 2) * np.exp(-(xspace - avrg) ** 2 / (2 * std ** 2)), c="red")
    # ax.plot(xspace, scipy.stats.norm.pdf(xspace, loc=avrg), c="red")
    ax.minorticks_on()
    icaps.mirror_axis(ax)
    ax.set_ylabel("Frequency per Frame per Pixel")
    ax.set_xlabel("Gray Value Deviation from Flat-Field Image")
    plt.tight_layout()
    # plt.show()
    plt.savefig(plot_path + "ff_px_hist.png", dpi=dpi)
    plt.close()

    # ff = icaps.load_img(".".join(ff_path.split(".")[:-1]) + ".npy").astype(float)
    # bin_size = 0.25
    # bins = np.arange(-6 - bin_size / 2, 6 + bin_size, bin_size)
    # hist = np.zeros(len(bins) - 1, dtype=np.uint64)
    # pb = icaps.SimpleProgressBar(ff_stop - ff_start)
    # ff_mean = np.mean(ff)
    # for frame in range(ff_start, ff_stop + 1):
    #     file = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM", frame)
    #     img = icaps.load_img(file + icaps.generate_file(frame), strip=True).astype(float)
    #     noise = img - ff / ff_mean * np.mean(img)
    #     hist_, _ = np.histogram(noise, bins)
    #     hist += hist_.astype(np.uint64)
    #     pb.tick()
    # pb.close()
    # df_hist = pd.DataFrame(data={"value": bins[:-1] + bin_size / 2, "count": hist})
    # df_hist.to_csv(project_path + "noise_hist_adapted.csv", index=False)

    df_hist = pd.read_csv(project_path + "noise_hist_adapted.csv")
    vals = df_hist["value"].to_numpy()
    counts = df_hist["count"].to_numpy()
    avrg = np.average(vals, weights=counts)
    std = np.sqrt(np.average((vals - avrg) ** 2, weights=counts))
    print(avrg)
    print(std, 2 * std, 3 * std, 4 * std)
    _, ax = plt.subplots()
    ax.set_xlim(-3, 3)
    print(np.sum(counts))
    print((1024 * 1024 * (ff_stop - ff_start + 1)))
    # ax.bar(vals, counts / (1024 * 1024 * (ff_stop - ff_start + 1)), width=0.25 * 0.9, color="black")
    ax.bar(vals, counts / np.sum(counts), width=0.25 * 0.9, color="black")
    xspace = np.linspace(-5, 5, 1000)
    print(np.sum(counts / np.sum(counts)))
    ax.plot(xspace, 1/4 * 1 / np.sqrt(2 * np.pi * std ** 2) * np.exp(-(xspace - avrg) ** 2 / (2 * std ** 2)), c="red")
    # ax.plot(xspace, scipy.stats.norm.pdf(xspace, loc=avrg), c="red")
    ax.minorticks_on()
    icaps.mirror_axis(ax)
    ax.set_ylabel("Frequency per Frame per Pixel")
    ax.set_xlabel("Gray Value Deviation from Flat-Field Image")
    plt.tight_layout()
    # plt.show()
    plt.savefig(plot_path + "ff_px_hist_adapted.png", dpi=dpi)
    plt.close()

    # df_hist = pd.read_csv(project_path + "old/ldm_noise_hist_0.25.csv")
    # vals = df_hist["bin_center"].to_numpy()
    # counts = df_hist["count"].to_numpy()
    # avrg = np.average(vals, weights=counts)
    # print(avrg)
    # print(np.average((vals - avrg) ** 2, weights=counts))
    # _, ax = plt.subplots()
    # ax.set_xlim(-5, 5)
    # ax.bar(vals, counts / (1024 * 1024 * (ff_stop - ff_start)), width=0.25 * 0.9, color="black")

    # df_hist = pd.read_csv(project_path + "noise.csv")
    # vals = df_hist["value_c"].to_numpy()
    # counts = df_hist["count"].to_numpy()
    # avrg = np.average(vals, weights=counts)
    # print(avrg)
    # print(np.average((vals - avrg) ** 2, weights=counts))
    # _, ax = plt.subplots()
    # ax.bar(vals, counts / (ff_stop - ff_start), width=0.1 * 0.9, color="black")

    # for corner in corners:
    #     col = "corner_" + corner
    #     _, ax = plt.subplots()
    #     ax.scatter(time, df[col], c="black", s=1)
    #     ax.set_title(col)
    #     ax.set_xlim(-5, np.max(time) + 5)
    #     ax.set_ylim(0, 255)
    # plt.show()
    # for corner in corners:
    #     col = "corner_" + corner
    #     _, ax = plt.subplots()
    #     ax.scatter(time, np.gradient(df[col]), c="black", s=1)
    #     # ax.scatter(time[:-1], np.diff(df[col]), c="black", s=1)
    #     ax.set_xlim(-5, np.max(time) + 5)
    #     ax.set_ylim(-80, 80)
    #     ax.set_title(col)
    # plt.show()

    dt = 1000
    # for corner in corners:
    #     col = "corner_" + corner
    #     _, ax = plt.subplots()
    #     val = df[col].to_numpy()
    #     diff = np.array([val[i + dt] - val[i] for i in range(len(val) - dt)])
    #     ax.scatter(time[:-dt], diff, c="black", s=1)
    #     ax.set_xlim(-5, np.max(time) + 5)
    #     ax.set_ylim(-80, 80)
    #     ax.set_title(col)
    # plt.show()

    fig_hist, ax_hist = plt.subplots()
    hist_max = 0
    data_c = {}
    fit1 = [3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
    fit2 = [1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
    for corner in corners:
        col = "corner_" + corner
        fig, ax = plt.subplots()
        val = df_box[col].to_numpy()
        diff = np.array([val[i + dt] - val[i] for i in range(len(val) - dt)])
        idx = np.abs(diff) <= 1
        val = val[:-dt][idx]
        time_ = time[:-dt][idx]
        data_c[corner] = [time_, val]
        ax.scatter(time_, val, c="black", s=1)
        ax.set_title(corner)
        ax.set_xlim(-5, np.max(time) + 5)
        ax.set_ylim(0, 255)
        out1 = icaps.fit_model(icaps.fit_lin,
                               time_[time_ < ff_start / icaps.const.fps_ldm],
                               val[time_ < ff_start / icaps.const.fps_ldm])
        xspace = np.linspace(np.min(time), ff_start / icaps.const.fps_ldm, 1000)
        ax.plot(xspace, icaps.fit_lin(xspace, *out1.beta), c="red",
                label=icaps.get_param_label([
                    [r"$ax+b$"],
                    [r"$a$", out1.beta[0], out1.sd_beta[0], 4],
                    [r"$b$", out1.beta[1], out1.sd_beta[1], 2],
                ]))

        out2 = icaps.fit_model(icaps.fit_lin,
                               time_[time_ > ff_stop / icaps.const.fps_ldm],
                               val[time_ > ff_stop / icaps.const.fps_ldm])
        xspace = np.linspace(ff_stop / icaps.const.fps_ldm, np.max(time), 1000)
        ax.plot(xspace, icaps.fit_lin(xspace, *out2.beta), c="blue",
                label=icaps.get_param_label([
                    [r"$ax+b$"],
                    [r"$a$", out2.beta[0], out2.sd_beta[0], 4],
                    [r"$b$", out2.beta[1], out2.sd_beta[1], 2],
                ]))
        idx1 = time_ < ff_start / icaps.const.fps_ldm
        chisqr1 = np.sum((val[idx1] - icaps.fit_lin(time_[idx1], *out1.beta)) ** 2) / len(val[idx1])
        q1 = np.sum(np.abs(val[idx1] - icaps.fit_lin(time_[idx1], *out1.beta))) / len(val[idx1])
        idx2 = time_ > ff_stop / icaps.const.fps_ldm
        chisqr2 = np.sum((val[idx2] - icaps.fit_lin(time_[idx2], *out2.beta)) ** 2) / len(val[idx2])
        q2 = np.sum(np.abs(val[idx2] - icaps.fit_lin(time_[idx2], *out2.beta))) / len(val[idx2])
        print(corner, chisqr1, chisqr2, q1, q2)
        ax.legend(loc="lower right")
        plt.savefig(plot_path + f"ShotNoise_{corner}.png", dpi=dpi)
        plt.close(fig.number)

        x = df_box.loc[(df_box["frame"] >= ff_start) & (df_box["frame"] <= ff_stop), col].to_numpy()
        x = np.sort(x)
        # norm_x = np.linspace(1 / len(x), 1, len(x))
        # ax_hist.errorbar(x, norm_x, fmt=".", c="black")
        bins = np.arange(0.5, 256, 1)
        hist, _ = np.histogram(x, bins=bins)
        hist = hist / (ff_stop - ff_start)
        # hist = hist / np.max(hist)
        ax_hist.bar((bins[:-1] + 0.5) / 256, hist, width=0.9 / 256, color="black")
        # ax.set_xlim(-0.86, +0.86)
        hist_max = np.max([hist_max, np.max(hist)])
    ax_hist.set_ylim(0, 1.05 * hist_max)
    ax_hist.set_xlim(0.7, 0.825)
    ax_hist.set_ylabel("Normalized Frequency")
    ax_hist.set_xlabel("Normalized Brightness")
    ax_hist.minorticks_on()
    twiny = plt.twiny(ax_hist)
    twiny.minorticks_on()
    twiny.set_xlim(*(np.array(ax_hist.get_xlim()) * 256))
    twiny.set_xlabel("Gray Value")
    plt.figure(fig_hist.number)
    # plt.show()
    plt.savefig(plot_path + "FF_corner_hists.png", dpi=dpi)
    plt.close()

    df = pd.read_csv(csv_fstats)
    fig, ax = plt.subplots()
    idx = np.argsort(df["frame"].to_numpy())
    ax.errorbar(df["frame"].to_numpy()[idx] / icaps.const.fps_ldm, df["mean"].to_numpy()[idx],
                c="black", markersize=1, fmt="-", lw=1, zorder=100)
    # ax.errorbar(df["frame"].to_numpy()[idx] / icaps.const.fps_ldm, df["max"].to_numpy()[idx] / 256,
    #             c="red", markersize=1, fmt="o", lw=1)
    # for corner in ["tr"]:
    #     col = "corner_" + corner
    #     # ax.errorbar(df["frame"].to_numpy()[idx] / icaps.const.fps_ldm, df_box[col].to_numpy() / 256,
    #     #             c="blue", markersize=1, fmt=",", lw=1)
    #     ax.errorbar(data_c[corner][0], data_c[corner][1],
    #                 c="red", markersize=1, fmt="o", lw=1)
    # ax.scatter(df["frame"] / icaps.const.fps_ldm, df["min"] / 256, c="blue", s=1)
    # empty_frames = pd.read_csv(drive_letter + ":/icaps/data/FFs/empty_frames.csv", header=None)[1].to_numpy()
    # idx = df["frame"].isin(empty_frames)
    # ax.errorbar(df.loc[idx, "frame"] / icaps.const.fps_ldm, df.loc[idx, "mean"],
    #             c="red", fmt=".", markersize=1, zorder=101)
    # fit1 = [3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
    # fit2 = [1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
    # xspace = np.linspace(0, ff_start / icaps.const.fps_ldm, 1000)
    # idx = df["frame"] < ff_start
    # out = icaps.fit_model(icaps.fit_lin, df.loc[idx, "frame"], df.loc[idx, "mean"])
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, *out.beta), color="blue", zorder=110)
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, fit1[0], fit1[1]),
    #         color="red", zorder=110, lw=2, ls="--")
    # xspace = np.linspace(ff_stop / icaps.const.fps_ldm, 367, 1000)
    # idx = df["frame"] > ff_stop
    # out = icaps.fit_model(icaps.fit_lin, df.loc[idx, "frame"], df.loc[idx, "mean"])
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, *out.beta), color="blue", zorder=110)
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, fit2[0], fit2[1]),
    #         color="red", zorder=111, lw=2, ls="--")

    df_time = pd.read_excel(project_path + "timelines check flight.xlsx", sheet_name="VMU timeline as flown")
    df_time_clean = None
    cloud_lost = False
    cloud_loss_time = 0.
    done_colors = []
    stage_hist = {}
    for i, row in df_time.iterrows():
        if i == len(df_time) - 1:
            break
        if (row["Activity"] == "Injection") & cloud_lost:
            cloud_lost = False
            start = row["time LDM(s)"]
            ax.fill_between([cloud_loss_time, start - 2.9], 0, 256,
                            fc="gray", ec="none", alpha=0.3)
            df_ = pd.DataFrame(data={"time LDM(s)": [cloud_loss_time], "activity": ["CloudLost"], "substage": [""],
                                     "count": [1]})
            if df_time_clean is None:
                df_time_clean = df_
            else:
                df_time_clean = df_time_clean.append(df_)
        if row["Comment"] == "Loss of cloud":
            cloud_lost = True
            cloud_loss_time = df_time.loc[i, "time LDM(s)"]
        if cloud_lost:
            continue
        start = row["time LDM(s)"]
        stop = df_time.loc[i + 1, "time LDM(s)"]
        color = "magenta"
        label = ""
        if row["Activity"] == "Injection":
            color = "red"
            label = "Injection"
            start -= 2.9
        elif row["Activity"] == "Scanning":
            color = "yellow"
            label = "Scan"
        elif (row["Activity"] == "ElectricField") or (row["Activity"] == "Electricfield"):
            color = "lime"
            label = "Electric Field"
        elif row["Activity"] == "Levitation":
            color = "blue"
            label = "Levitation"
        elif row["Activity"] == "Squeezing":
            color = "violet"
            label = "Squeezing"
        elif "TargetAgg" in str(row["Activity"]):
            color = "cyan"
            label = "Targeting"
        if np.isnan(start):
            start = df_time.loc[i - 1, "time LDM(s)"]
            color = "blue"
        if label != "":
            if label not in stage_hist.keys():
                stage_hist[label] = 0
            substage = ""
            if label == "Scan":
                substage = "-1"
                if row["Parameter"] == "0;0;-3":
                    stage_hist[label] += 1
                if row["Parameter"] == "0;0;3":
                    substage = "+2"
            elif label == "Electric Field":
                substage = "0"
                if row["Parameter"] == "14/-14":
                    substage = "-1"
                    stage_hist[label] += 1
                elif row["Parameter"] == "-14/14":
                    substage = "+1"
            elif label == "Targeting":
                substage = "Search"
                if row["Parameter"] == "-3,0,0":
                    stage_hist[label] += 1
                if row["Activity"] == "TargetAggPos":
                    substage = "Pos"
                elif row["Activity"] == "TargetAggScan":
                    substage = "Scan"
            else:
                stage_hist[label] += 1
            df_ = pd.DataFrame(data={"time LDM(s)": [start], "activity": [label], "substage": [substage],
                                     "count": [stage_hist[label]]})
            if df_time_clean is None:
                df_time_clean = df_
            else:
                df_time_clean = df_time_clean.append(df_)
        ax.fill_between([start, stop], 0, 256, fc=color, ec="none", alpha=0.3,
                        label=label if color not in done_colors else "")
        if color not in done_colors:
            done_colors.append(color)
    df_time_clean.sort_values(by="time LDM(s)")
    # df_time_clean["count"] = df_time_clean.groupby("activity").rank(method="first", ascending=True).astype(int)
    df_time_clean.to_csv(project_path + "timeline_check_flight_clean.csv", index=False)
    ax.set_xlim(-5, 367 + 5)
    ax.text(233, 0.66 * 256, "Cloud Lost", color="dimgray", rotation=90)
    ax.set_ylabel("Mean Gray Value")
    ax.legend(loc=(-0.08, 1.025), ncol=3, fancybox=False)
    ax.set_ylim(0.65 * 256, 0.765 * 256)
    ax.minorticks_on()
    # twinx = plt.twinx(ax)
    # twinx.minorticks_on()
    # twinx.set_ylabel("Mean Gray Value")
    # twinx.set_ylim(*(np.array(ax.get_ylim()) * 256))
    ax.set_xlabel("Time (s)")
    icaps.mirror_axis(ax, axis="both")
    fig.subplots_adjust(top=0.835)
    plt.savefig(plot_path + "ShotNoise_Brightness.png", dpi=dpi)
    plt.close()

    df = pd.read_csv(csv_fstats)
    fig, ax = plt.subplots()
    idx = np.argsort(df["frame"].to_numpy())
    ax.errorbar(df["frame"].to_numpy()[idx] / icaps.const.fps_ldm, df["mean"].to_numpy()[idx],
                c="black", markersize=1, fmt="-", lw=1, zorder=100)
    # ax.errorbar(df["frame"].to_numpy()[idx] / icaps.const.fps_ldm, df["max"].to_numpy()[idx] / 256,
    #             c="red", markersize=1, fmt="o", lw=1)
    # for corner in ["tr"]:
    #     col = "corner_" + corner
    #     # ax.errorbar(df["frame"].to_numpy()[idx] / icaps.const.fps_ldm, df_box[col].to_numpy() / 256,
    #     #             c="blue", markersize=1, fmt=",", lw=1)
    #     ax.errorbar(data_c[corner][0], data_c[corner][1],
    #                 c="red", markersize=1, fmt="o", lw=1)
    # ax.scatter(df["frame"] / icaps.const.fps_ldm, df["min"] / 256, c="blue", s=1)
    # empty_frames = pd.read_csv(drive_letter + ":/icaps/data/FFs/empty_frames.csv", header=None)[1].to_numpy()
    # idx = df["frame"].isin(empty_frames)
    # ax.errorbar(df.loc[idx, "frame"] / icaps.const.fps_ldm, df.loc[idx, "mean"],
    #             c="red", fmt=".", markersize=1, zorder=101)
    # fit1 = [3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
    # fit2 = [1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
    # xspace = np.linspace(0, ff_start / icaps.const.fps_ldm, 1000)
    # idx = df["frame"] < ff_start
    # out = icaps.fit_model(icaps.fit_lin, df.loc[idx, "frame"], df.loc[idx, "mean"])
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, *out.beta), color="blue", zorder=110)
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, fit1[0], fit1[1]),
    #         color="red", zorder=110, lw=2, ls="--")
    # xspace = np.linspace(ff_stop / icaps.const.fps_ldm, 367, 1000)
    # idx = df["frame"] > ff_stop
    # out = icaps.fit_model(icaps.fit_lin, df.loc[idx, "frame"], df.loc[idx, "mean"])
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, *out.beta), color="blue", zorder=110)
    # ax.plot(xspace, icaps.fit_lin(xspace * icaps.const.fps_ldm, fit2[0], fit2[1]),
    #         color="red", zorder=111, lw=2, ls="--")

    df_time = pd.read_excel(project_path + "timelines check flight.xlsx", sheet_name="VMU timeline as flown")
    cloud_lost = False
    cloud_loss_time = 0.
    done_labels = []
    handles = []
    for i, row in df_time.iterrows():
        if i == len(df_time) - 1:
            break
        if (row["Activity"] == "Injection") & cloud_lost:
            cloud_lost = False
            start = row["time LDM(s)"]
            ax.fill_between([cloud_loss_time, start - 2.9], 0, 256,
                            fc="white", ec="none", alpha=0.3)
        if row["Comment"] == "Loss of cloud":
            cloud_lost = True
            cloud_loss_time = df_time.loc[i, "time LDM(s)"]
        if cloud_lost:
            continue
        start = row["time LDM(s)"]
        stop = df_time.loc[i + 1, "time LDM(s)"]
        color = "magenta"
        hatch = ""
        label = ""
        if row["Activity"] == "Injection":
            color = "darkgray"
            hatch = "--"
            label = "Injection"
            start -= 2.9
        elif row["Activity"] == "Scanning":
            color = "darkgray"
            hatch = ""
            label = "Scan"
        elif (row["Activity"] == "ElectricField") or (row["Activity"] == "Electricfield"):
            color = "dimgray"
            hatch = ""
            label = "Electric Field"
        elif row["Activity"] == "Levitation":
            color = "lightgray"
            hatch = ""
            label = "Levitation"
        elif row["Activity"] == "Squeezing":
            color = "darkgray"
            hatch = r"\\\\"
            label = "Squeezing"
        elif "TargetAgg" in str(row["Activity"]):
            color = "darkgray"
            hatch = "////"
            label = "Targeting"
        if np.isnan(start):
            start = df_time.loc[i - 1, "time LDM(s)"]
            color = "lightgray"
        ax.fill_between([start, stop], 0, 256, fc=color, ec="none", alpha=1,
                        # label=label if label not in done_labels else "",
                        hatch=hatch
                        )
        if label not in done_labels:
            if color != "magenta":
                handle = mpatches.Patch(facecolor=color, alpha=1, hatch=hatch, label=label)
                handles.append(handle)
            done_labels.append(label)
    ax.set_xlim(-5, 367 + 5)
    ax.text(233, 0.66 * 256, "Cloud Lost", color="dimgray", rotation=90)
    ax.set_ylabel("Mean Gray Value")
    ax.legend(handles=handles, loc=(-0.08, 1.025), ncol=3, fancybox=False)
    ax.set_ylim(0.65 * 256, 0.765 * 256)
    ax.minorticks_on()
    # twinx = plt.twinx(ax)
    # twinx.minorticks_on()
    # twinx.set_ylabel("Mean Gray Value")
    # twinx.set_ylim(*(np.array(ax.get_ylim()) * 256))
    ax.set_xlabel("Time (s)")
    icaps.mirror_axis(ax, axis="both")
    fig.subplots_adjust(top=0.835)
    plt.savefig(plot_path + "ShotNoise_Brightness_Gray.png", dpi=dpi)
    plt.close()



    thresh = 40
    _, ax = plt.subplots()
    ax.scatter(df.loc[df["mean"] - df["min"] <= thresh, "frame"],
               df.loc[df["mean"] - df["min"] <= thresh, "mean"],
               c="black", s=1)

    ffs_path = drive_letter + ":/icaps/data/FFs/"
    files = icaps.get_files(ffs_path, frmt="bmp")
    frames = np.array([], dtype=int)
    for file in files:
        start, stop = map(int, map(float, file.split(".")[0].split("_")[1:]))
        frames = np.append(frames, np.arange(start, stop + 1))
    _, ax = plt.subplots()
    ax.scatter(df.loc[df["frame"].isin(frames), "frame"],
               df.loc[df["frame"].isin(frames), "mean"],
               c="black", s=1)
    ax.set_ylim(-1, 256)
    ax.set_xlim(np.min(df["frame"]), np.max(df["frame"]))

    # plt.show()
    plt.close()




    start_frame, stop_frame = map(int, ff_path.split("/")[-1].split(".")[0].split("_")[1:])
    nframes = stop_frame + 1 - start_frame

    plot_path_ = plot_path + "nocloud/"
    icaps.mk_folders(plot_path_)

    df = pd.read_csv(csv_stats)
    _, ax = plt.subplots()
    _, _, bars = ax.errorbar(df["frame"], df["mean"], yerr=df["std"], c="black", markersize=1, fmt="o")
    [bar.set_alpha(0.003) for bar in bars]
    plt.savefig(plot_path_ + "mean_trend_alt.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    t = df["frame"].to_numpy() / icaps.const.fps_ldm
    idx = np.argsort(t)
    mean = df["mean"].to_numpy()[idx]
    std = df["std"].to_numpy()[idx]
    t = t[idx]
    _, _, bars = ax.errorbar(t, mean - std,
                             c="gray", markersize=1, fmt=".", label="Mean - Std")
    _, _, bars = ax.errorbar(t, mean + std,
                             c="black", markersize=1, fmt=".", label="Mean + Std")
    ax.legend()
    plt.savefig(plot_path_ + "FF_trend.png", dpi=dpi)
    plt.close()


    # px_hist = np.load(npy_px_hist)
    # means = np.zeros(px_hist.shape[:2], dtype=np.float)
    # stds = np.zeros(px_hist.shape[:2], dtype=np.float)
    # px_values = np.arange(256)
    # pb = icaps.SimpleProgressBar(px_hist.shape[1] * px_hist.shape[0], "Progress")
    # for j in range(px_hist.shape[0]):
    #     for i in range(px_hist.shape[1]):
    #         hist = px_hist[j, i]
    #         vals = hist * px_values
    #         mean = np.sum(vals) / nframes
    #         std = np.sqrt(1/(nframes - 1) * np.sum(hist * (px_values - mean) ** 2))
    #         means[j, i] = mean
    #         stds[j, i] = std
    #         pb.tick()
    # pb.close()
    # np.save(plot_path_ + "means.npy", means)
    # np.save(plot_path_ + "stds.npy", stds)

    means = np.load(plot_path_ + "means.npy")
    stds = np.load(plot_path_ + "stds.npy")
    _, ax = plt.subplots()
    cmap = plt.get_cmap("plasma")
    ax.set_xlabel(r"$x \,\rm (px)$")
    ax.set_ylabel(r"$y \,\rm (px)$")
    plt.imshow(means, cmap=cmap)
    plt.colorbar(plt.cm.ScalarMappable(norm=plt.Normalize(vmin=np.min(means),
                                                          vmax=np.max(means)),
                                       cmap=cmap), label="Gray Value")
    plt.tight_layout()
    plt.savefig(plot_path_ + "FF.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    cmap = plt.get_cmap("binary_r")
    ax.set_xlabel(r"$x \,\rm (px)$")
    ax.set_ylabel(r"$y \,\rm (px)$")
    plt.imshow(means, cmap=cmap)
    plt.colorbar(plt.cm.ScalarMappable(norm=plt.Normalize(vmin=np.min(means),
                                                          vmax=np.max(means)),
                                       cmap=cmap), label="Gray Value")
    plt.tight_layout()
    plt.savefig(plot_path_ + "FF_gray.png", dpi=dpi)
    plt.close()

    _, ax = plt.subplots()
    plt.imshow(stds)
    plt.colorbar()
    plt.savefig(plot_path_ + "stds.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()

