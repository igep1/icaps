import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_Growth/calibration/"
plot_path = project_path + "plots/"
icaps.mk_folders([project_path, plot_path])


def main():
    pass


if __name__ == '__main__':
    main()


