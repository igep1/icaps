import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_Growth/matching/"
plot_path = project_path + "plots/"
csv_main = project_path + "Match_Results_same_tracklength_clean.csv"

out_ex0 = {"beta": [0.92613601, 2.58613955], "sd_beta": [0.00926108, 0.01404969]}
lh_link = np.mean

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def adapt_n_opt(ex_, ex_err_=None):
    n_ = (ex_ / (10 ** out_ex0["beta"][1])) ** (1 / (out_ex0["beta"][0]))
    if ex_err_ is not None:
        n_low_ = n_ - ((ex_ - ex_err_) / (10 ** (out_ex0["beta"][1]
                                                 + out_ex0["sd_beta"][1]))) ** (1 / (out_ex0["beta"][0]
                                                                                  + out_ex0["sd_beta"][0]))
        n_high_ = ((ex_ + ex_err_) / (10 ** (out_ex0["beta"][1]
                                             - out_ex0["sd_beta"][1]))) ** (1 / (out_ex0["beta"][0]
                                                                              - out_ex0["sd_beta"][0])) - n_
        return n_, n_low_, n_high_
    return n_


def main():
    df = pd.read_csv(csv_main)
    ex_ldm = df["ldm_mass"].to_numpy()
    ex_ldm_err = df["ldm_mass_err_median"].to_numpy()
    ex_oos = df["oos_mass"].to_numpy()
    ex_oos_err = df["oos_mass_err_median"].to_numpy()
    n_ldm, n_ldm_low, n_ldm_high = adapt_n_opt(ex_ldm, ex_ldm_err)

    fig, ax = plt.subplots()
    fig.subplots_adjust(bottom=0.15, top=0.8)
    ax.set_xlabel(r"$n_\mathrm{LDM}$")
    ax.set_ylabel(r"$\mu_\mathrm{OOS} \,(\mathrm{counts})$")
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(n_ldm, ex_oos, xerr=[n_ldm_low, n_ldm_high], yerr=ex_oos_err,
                color="darkgray", markersize=3, fmt="o")
    ax.errorbar(n_ldm, ex_oos, color="black", markersize=3, fmt="o")

    xspace = np.linspace(*ax.get_xlim(), 1000)
    n_ldm_err = lh_link([n_ldm_low, n_ldm_high], axis=0)
    out_s1 = icaps.fit_model(icaps.fit_slope1, np.log10(n_ldm), np.log10(ex_oos),
                             icaps.log_err(n_ldm, n_ldm_err), icaps.log_err(ex_oos, ex_oos_err),
                             fit_type=0, p0=[300.])
    label = icaps.get_param_label([
        [r"$\mu_{\rm OOS} = \mu'_\mathrm{0, OOS} n_\mathrm{LDM}$"],
        [r"$\mu'_\mathrm{0, OOS}$", 10 ** out_s1.beta[0], 10 ** out_s1.sd_beta[0]]
    ])
    ax.plot(xspace, 10 ** icaps.fit_slope1(np.log10(xspace), *out_s1.beta), c="blue", ls="--", lw=2,
            label=label, alpha=1, zorder=10)
    chisqr = icaps.chisqr(np.log10(ex_oos), icaps.fit_slope1(np.log10(n_ldm), *out_s1.beta))
    print(chisqr)

    out_lin = icaps.fit_model(icaps.fit_lin, np.log10(n_ldm), np.log10(ex_oos),
                              icaps.log_err(n_ldm, n_ldm_err), icaps.log_err(ex_oos, ex_oos_err),
                              fit_type=0, p0=[1., 300.])
    label = icaps.get_param_label([
        [r"$\mu_{\rm OOS} = \zeta_0 n_\mathrm{LDM}^{\zeta_1}$"],
        [r"$\zeta_0$", 10 ** out_lin.beta[1], 10 ** out_lin.sd_beta[1]],
        [r"$\zeta_1$", out_lin.beta[0], out_lin.sd_beta[0]]
    ])
    ax.plot(xspace, 10 ** icaps.fit_lin(np.log10(xspace), *out_lin.beta), c="red", ls="-", lw=2,
            label=label, alpha=1, zorder=10)
    chisqr = icaps.chisqr(np.log10(ex_oos), icaps.fit_lin(np.log10(n_ldm), *out_lin.beta))
    print(chisqr)
    icaps.mirror_axis(ax)
    ax.legend(loc=(0, 1.05), ncol=2, fancybox=False, fontsize=13)
    print(out_lin.beta, out_lin.sd_beta, out_s1.beta, out_s1.sd_beta)
    plt.savefig(plot_path + "OOS_Ex0_median_errs.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()



