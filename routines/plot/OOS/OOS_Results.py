import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.cm import ScalarMappable
import matplotlib.colors as mplc

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "XZ"
# plane = "YZ"
stages = "U1-7"
# stages = "E1-3"
# plot_to = None
plot_to = -2
mask = True
# mask = False
# dist_frames = [701, 1170, 1560, 2200, 2580, 3270, 3630, 4325, 4700, 5375, 5810, 6425, 6880, 7450]
# dist_frames = [300, 350, 400, 450, 500, 550, 600, 650, 700]
project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/"
orig_path = project_path + "orig_strip/"
plot_path = project_path + "plots/" + f"{stages}/" + ("masked/" if mask else "full/")
stages_path = project_path + "stages/"
volume_mask = project_path + "volume/mask_const.bmp"
csv_base = project_path + f"OOS_{plane}_{stages}.csv"
csv_loc = csv_base
if mask:
    csv_loc = ".".join(csv_loc.split(".")[:-1]) + "_masked.csv"
csv_tot = ".".join(csv_loc.split(".")[:-1]) + "_total.csv"
csv_track = ".".join(csv_loc.split(".")[:-1]) + "_linked.csv"
if mask:
    csv_track = ".".join(csv_track.split(".")[:-1]) + "_masked.csv"
icaps.mk_folders([stages_path, plot_path])

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)

out_ex0 = {"beta": [0.84958653, 1.53913371], "sd_beta": [0.06888633, 0.11342147]}


def adapt_n(ex_, ex_err_=None):
    n_ = (ex_ / (10 ** out_ex0["beta"][1])) ** (1 / (out_ex0["beta"][0]))
    if ex_err_ is not None:
        n_low_ = n_ - ((ex_ - ex_err_) / (10 ** (out_ex0["beta"][1]
                                                 + out_ex0["sd_beta"][1]))) ** (1 / (out_ex0["beta"][0]
                                                                                  + out_ex0["sd_beta"][0]))
        n_high_ = ((ex_ + ex_err_) / (10 ** (out_ex0["beta"][1]
                                             - out_ex0["sd_beta"][1]))) ** (1 / (out_ex0["beta"][0]
                                                                              - out_ex0["sd_beta"][0])) - n_
        return n_, n_low_, n_high_
    return n_


def main():
    if mask:
        volume = np.count_nonzero(icaps.load_img(volume_mask))
    else:
        shp = icaps.load_img(orig_path + icaps.generate_file(0, prefix=None, digits=5)).shape
        volume = shp[0] * shp[1]
    volume *= icaps.const.px_oos ** 2 * icaps.const.dof_oos * 1e-9

    stage_start = int(float(stages[1]))
    stage_stop = stage_start
    if len(stages) > 2:
        stage_stop = int(float(stages[3]))
    stage_names = [stages[0] + str(i) for i in range(stage_start, stage_stop + 1)]
    if plot_to is not None:
        stage_names = stage_names[:plot_to]
    stage_lims = np.array([icaps.const.oos_stages[stage_names[i]] for i in range(len(stage_names))])
    stage_tlims = stage_lims / icaps.const.fps_oos

    # ===================================================================================
    # MEAN DISTRIBUTIONS
    # ===================================================================================
    df = None
    plot_mean_dists = False
    if plot_mean_dists:
        df = pd.read_csv(csv_loc)
        dist_plot_path = plot_path + "mean_dists/"
        icaps.mk_folder(dist_plot_path)
        stage_letter = stages[0]
        s_l, s_r = stages[1:].split("-")
        s_l = int(float(s_l))
        s_r = int(float(s_r))
        for i in range(s_l, s_r + 1):
            stage = stage_letter + str(i)
            f0, f1 = icaps.const.oos_stages[stage]
            print(stage, f0, f1)
            ex = df.loc[(df["frame"] >= f0) & (df["frame"] <= f1), "ex"].to_numpy()
            mass = adapt_n(ex)
            idx = np.argsort(mass)
            mass = mass[idx]
            # norm = np.linspace(1 / len(mass), 1, len(mass))
            norm = np.cumsum(mass)
            _, ax = plt.subplots()
            # ax.set_xscale("log")
            # ax.set_xlim(0.5, 1e5)
            ax.set_xlabel(r"$m$ ($m_{\rm 0}$)")
            ax.set_ylabel("Normalized Cumulative Frequency")
            ax.errorbar(mass, norm,
                        # xerr=[mass_low, mass_high],
                        c="black", fmt="o", markersize=1, label="{:05d}_{:05d}".format(f0, f1))
            icaps.mirror_axis(ax)
            plt.show()
            # plt.savefig(dist_plot_path + "MeanDist_{:05d}_{:05d}.png".format(f0, f1), dpi=dpi)
            # plt.close()

    # ===================================================================================
    # DISTRIBUTIONS
    # ===================================================================================
    # plot_dists = False
    # if plot_dists:
    #     if df is None:
    #         df = pd.read_csv(csv_loc)
    #     dist_plot_path = plot_path + "dists/"
    #     icaps.mk_folder(dist_plot_path)
    #     pb = icaps.SimpleProgressBar(len(dist_frames), "Plotting Distributions")
    #     for frame in dist_frames:
    #         pb.set_stage(frame)
    #         ex = df.loc[df["frame"] == frame, "ex"].to_numpy()
    #         # mass = ex / icaps.const.oos_ex0
    #         # mass_low = ex[idx] / (icaps.const.oos_ex0 + icaps.const.oos_ex0_err)
    #         # mass_high = ex[idx] / (icaps.const.oos_ex0 - icaps.const.oos_ex0_err)
    #         mass, mass_low, mass_high = adapt_n(ex, 0)
    #         idx = np.argsort(mass)
    #         mass = mass[idx]
    #         mass_low = mass_low[idx]
    #         mass_high = mass_high[idx]
    #         norm = np.linspace(1 / len(mass), 1, len(mass))
    #         _, ax = plt.subplots()
    #         ax.set_xscale("log")
    #         ax.set_xlim(0.5, 1e5)
    #         ax.set_xlabel(r"$m$ ($m_{\rm 0}$)")
    #         ax.set_ylabel("Normalized Cumulative Frequency")
    #         ax.errorbar(mass, norm,
    #                     # xerr=[mass_low, mass_high],
    #                     c="black", fmt="o", markersize=1, label="{:05d}".format(frame))
    #         icaps.mirror_axis(ax)
    #         plt.savefig(dist_plot_path + "Dist_{:05d}.png".format(frame), dpi=dpi)
    #         plt.close()
    #         pb.tick()
    #     pb.close()

    # ===================================================================================
    # COLLECTIVE DISTRIBUTIONS
    # ===================================================================================
    if df is None:
        df = pd.read_csv(csv_loc)
    for variant in ("Mass", "Frequency"):
        for norm in ["cummedian"]:
            pb = icaps.SimpleProgressBar(len(stage_lims), "Plotting Collective Distributions (Mean)")
            fig, ax = plt.subplots()
            # cmap = plt.get_cmap("plasma")
            cmap = plt.get_cmap("binary_r")
            time_span = np.max(stage_tlims) - np.min(stage_tlims)
            for start_frame, stop_frame in stage_lims:
                frames = np.arange(start_frame, stop_frame + 1)
                ex = df.loc[df["frame"].isin(frames), "ex"].to_numpy()
                mass, mass_low, mass_high = adapt_n(ex, 0)
                idx = np.argsort(mass)
                mass = mass[idx]
                mass_low = mass_low[idx]
                mass_high = mass_high[idx]
                if variant == "Mass":
                    y = np.cumsum(mass) / np.max(np.cumsum(mass))
                else:
                    y = np.linspace(1 / len(mass), 1, len(mass))
                # x = mass / (np.sum(mass**2) / np.sum(mass))
                x = mass
                if norm == "median":
                    x /= np.median(mass)
                elif norm == "cummedian":
                    idx = np.argmin(np.abs(y - 0.5))
                    x /= np.sort(x)[idx]
                tcenter = np.mean([start_frame, stop_frame]) / icaps.const.fps_oos
                ax.errorbar(x, y,
                            # xerr=[mass - mass_low, mass_high - mass],
                            c=cmap((tcenter - np.min(stage_tlims)) / time_span),
                            fmt="o", markersize=2,
                            label=r"$({:.1f}-{:.1f}) \,\mathrm{{s}}$".format(start_frame / icaps.const.fps_oos,
                                                                             stop_frame / icaps.const.fps_oos)
                            )
                pb.tick()
            ax.set_xscale("log")
            if norm == "median":
                ax.set_xlim(7e-2, 6e3)
            elif norm == "cummedian":
                ax.set_xlim(1e-2, 1e2)
            else:
                ax.set_xlim(3e-1, 8e3)
            if norm == "median":
                ax.set_xlabel(r"$m/\bar{m}$")
            elif norm == "cummedian":
                ax.set_xlabel(r"$m/\bar{m}$")
            elif norm == "mean":
                ax.set_xlabel(r"$m/\langle m \rangle$")
            else:
                ax.set_xlabel(r"$m \, (\mathrm{kg})$")
            ax.set_ylabel("Normalized Cumulative {}".format(variant))
            icaps.mirror_axis(ax)
            # fig.colorbar(ScalarMappable(norm=mplc.Normalize(vmin=np.min(stage_tlims),
            #                                                 vmax=np.max(stage_tlims)), cmap=cmap),
            #              ax=ax, label="Time (s)")
            ax.legend(loc="upper left", fancybox=False, fontsize=13)
            plt.savefig(plot_path + "Dist_Coll_NC{}_norm{}.png".format(variant, norm), dpi=dpi)
            plt.close()
            pb.close()

    # ===================================================================================
    # CALCULATING TOTALS
    # ===================================================================================
    # df_tot = pd.DataFrame(data={"frame": df["frame"].unique(), "ex_tot": 0, "ex_max": 0,
    #                             "ex_median": 0., "ex_median_err": 0., "ex_mean": 0., "ex_mean_err": 0.,
    #                             "n_lg_mean": 0., "n_lg_mean_err": 0.})
    # pb = icaps.SimpleProgressBar(len(df["frame"].unique()), "Calculating Totals")
    # for frame in df["frame"].unique():
    #     idx_tot = df_tot["frame"] == frame
    #     df_ = df[df["frame"] == frame]
    #     df_tot.loc[idx_tot, "ex_tot"] = np.sum(df_["ex"])
    #     df_tot.loc[idx_tot, "ex_max"] = np.max(df_["ex"])
    #     df_tot.loc[idx_tot, "ex_median"] = np.median(df_["ex"])
    #     df_tot.loc[idx_tot, "ex_median_err"] = np.median(np.abs(df_["ex"] - np.median(df_["ex"])))
    #     df_tot.loc[idx_tot, "ex_mean"] = np.mean(df_["ex"])
    #     df_tot.loc[idx_tot, "ex_mean_err"] = np.std(df_["ex"])
    #     # n_lg = 10 ** np.mean(np.log10(df_["ex"] / icaps.const.oos_ex0))
    #     # df_tot.loc[idx_tot, "n_lg_mean"] = n_lg
    #     # std = 10 ** np.sqrt(1/(len(df_) - 1) * np.sum(np.log10(df_["ex"] / icaps.const.oos_ex0 / n_lg) ** 2))
    #     # df_tot.loc[idx_tot, "n_lg_mean_err"] = std
    #     pb.tick()
    # df_tot.to_csv(csv_tot, index=False)
    # pb.close()

    df_tot = pd.read_csv(csv_tot)
    df_tot = df_tot[(df_tot["frame"] <= np.max(stage_lims)) & (df_tot["frame"] >= np.min(stage_lims))]
    # ===================================================================================
    # HeinePratsinis
    # ===================================================================================
    _, ax = plt.subplots()
    t = df_tot["frame"].to_numpy() / icaps.const.fps_oos
    # y = df_tot["n_lg_mean"].to_numpy()
    # ax.errorbar(t, y, c="black", markersize=1, fmt="o", zorder=1)
    y, y_low, y_high = adapt_n(df_tot["ex_mean"], df_tot["ex_mean_err"])
    ax.errorbar(t, y, c="gray", markersize=1, fmt="o", zorder=1)
    y, y_low, y_high = adapt_n(df_tot["ex_median"], df_tot["ex_median_err"])
    ax.errorbar(t, y, c="lightgray", markersize=1, fmt="o", zorder=0)

    def fit_pow(t_, a, b, c):
        return c + a * t_ ** b

    out = icaps.fit_model(fit_pow, t[t < 110], y[t < 110])
    tspace = np.linspace(np.min(t), np.max(t[t < 110]), 1000)
    ax.plot(tspace, fit_pow(tspace, *out.beta), c="red", zorder=2, label=icaps.get_param_label([
        [r"$\langle m \rangle = a \cdot t^b + c$"],
        [r"$a$", out.beta[0], out.sd_beta[0]],
        [r"$b$", out.beta[1], out.sd_beta[1]],
        [r"$c$", out.beta[2], out.sd_beta[2]],
    ]))
    ax.legend(fancybox=False)
    ax.set_xlabel("Time (s)")
    ax.set_ylabel(r"$\langle m \rangle \, (m_0)$")
    ax.set_yscale("log")
    # plt.show()
    plt.close()

    # ===================================================================================
    # GROWTH
    # ===================================================================================
    fig, ax = plt.subplots()
    y, y_low, y_high = adapt_n(df_tot["ex_max"], 0)
    _, _, bars = ax.errorbar(df_tot["frame"] / icaps.const.fps_oos, y,
                             mfc="none", mec="black", markersize=1, fmt="o",
                             # label="Max"
                             )
    y, y_low, y_high = adapt_n(df_tot["ex_mean"], df_tot["ex_mean_err"])
    _, _, bars = ax.errorbar(df_tot["frame"] / icaps.const.fps_oos, y,
                             mfc="black", mec="black", markersize=1, fmt="o",
                             # label="Mean"
                             )
    y, y_low, y_high = adapt_n(df_tot["ex_median"], df_tot["ex_median_err"])
    _, _, bars = ax.errorbar(df_tot["frame"] / icaps.const.fps_oos, y,
                             mfc="gray", mec="gray", markersize=1, fmt="o",
                             # label="Median"
                             )
    [bar.set_alpha(0.3) for bar in bars]
    ax.annotate(text="Maximum", xy=(0.8, 0.7), xycoords="axes fraction", c="black")
    ax.annotate(text="Mean", xy=(0.8, 0.35), xycoords="axes fraction", c="black")
    ax.annotate(text="Median", xy=(0.8, 0.05), xycoords="axes fraction", c="black")
    ax.set_yscale("log")
    # ax.set_xscale("log")
    xlims = ax.get_xlim()
    ylims = ax.get_ylim()
    xspace = np.linspace(*xlims, 1000)
    # ax.plot(xspace, icaps.fit_growth(xspace, 0.5, 50, 100, 20.5/100),
    #         c="blue", ls="--", lw=2, label="Ballistic")
    # ax.plot(xspace, icaps.fit_growth(xspace, -0.394, 50, 100, 10.35/100),
    #         c="blue", ls="-", lw=2, label="Diffusive")
    ax.plot(xspace, icaps.fit_growth_c(xspace, 0.5, 100, 20.5 / 100, -0.02377),
            c="black", ls="--", lw=2, label="Ballistic")
    ax.plot(xspace, icaps.fit_growth_c(xspace, -0.394, 100, 10.35 / 100, 0.3417),
            c="black", ls="-", lw=2, label="Diffusive")
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    # ax.legend(loc=(0, 1.05), fancybox=False, ncol=3)
    ax.legend(loc="upper left", fancybox=False)
    fig.subplots_adjust(bottom=0.12, top=0.95, left=0.1, right=0.98)
    ax.set_xlabel(r"$t \,(\mathrm{s})$")
    ax.set_ylabel(r"$m/m_0$")
    # ax.grid()
    icaps.mirror_axis(ax)
    plt.savefig(plot_path + f"Growth_{plane}.png", dpi=dpi)
    plt.close()

    # ===================================================================================
    # MASS DENSITY
    # ===================================================================================
    _, ax = plt.subplots()
    y, y_low, y_high = adapt_n(df_tot["ex_tot"], 0)
    _, _, bars = ax.errorbar(df_tot["frame"] / icaps.const.fps_oos,
                             y / volume / 1e4,
                             c="black", markersize=1, fmt="o")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel(r"Mass Density ($10^4\,m_{\rm 0} mm^{-3}$)")
    # ax.set_yscale("log")
    # ax.set_xscale("log")
    # ax.grid()
    ax.minorticks_on()
    icaps.mirror_axis(ax, axis="both")
    # plt.subplots_adjust(left=0.2, right=0.92)
    plt.tight_layout()
    plt.savefig(plot_path + f"Totals_{plane}.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()

