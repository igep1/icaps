import icaps
import numpy as np
import pandas as pd
import trackpy as tp
import matplotlib.pyplot as plt
import time

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_Growth/threshold_mcs/"
plane = "XZ"
strip = True
orig_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/orig{'_strip' if strip else ''}/"
df_path = drive_letter + f":/icaps/data/OOS_Growth/background/"
df_path += f"{plane}{'_strip' if strip else ''}/DF_{plane}{'_strip' if strip else ''}_11035_13000.npy"
subtract_df = True
sub_proj_path = project_path + f"{plane}{'_strip' if strip else ''}{'_dfsub' if subtract_df else ''}/"
locate_path = sub_proj_path + "located/"
link_path = sub_proj_path + "linked/"
plot_path = sub_proj_path + "plot/"
icaps.mk_folders([sub_proj_path, locate_path, link_path, plot_path])
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def main():
    # tbases = [10, 11, 12, 13, 14, 16, 18, 20, 25, 30, 40, 50, 60]
    # tseeds = np.append(np.arange(100, 255, 25), 255)
    # frames = np.arange(400, 600)

    # for testing
    frames = [5500]
    tbases = [11]
    tseeds = [200]

    memory = [1, 2, 3]
    searchradii = [3, 5, 10]
    print("tbase ({}):".format(len(tbases)), tbases)
    print("tseed ({}):".format(len(tseeds)), tseeds)
    print("frames ({}):".format(len(frames)), "{}-{}".format(np.min(frames), np.max(frames)))
    print("memory ({}):".format(len(memory)), memory)
    print("searchradii ({}):".format(len(searchradii)), searchradii)
    print("frames to locate in:", f"{len(frames)} -> {len(tbases) * len(tseeds) * len(frames)}")
    ntables = len(tbases) * len(tseeds)
    print("tables to link:", f"{ntables} -> {ntables * len(memory) * len(searchradii)}")

    # ===============================================================================================================
    # LOCATE
    # """
    imgs = [icaps.load_img(orig_path + icaps.generate_file(frame, prefix=None, digits=5)) for frame in frames]
    darkfield = icaps.load_img(df_path)
    pb = icaps.SimpleProgressBar(len(tbases) * len(tseeds) * len(frames), "Locating")
    for tbase in tbases:
        for tseed in tseeds:
            pb.set_stage(f"{tbase} {tseed}")
            df = None
            for i, frame in enumerate(frames):
                img = imgs[i]
                if subtract_df:
                    img = img.astype(float)
                    img -= darkfield
                    img[img < 0] = 0
                df_, mark = icaps.wacca(img, frame, tbase, tseed, ret_mark=True, mark_label=True, mark_enlarge=4)
                _, ax = plt.subplots()
                ax.imshow(mark)
                plt.show()
                if df is None:
                    df = df_
                else:
                    df = df.append(df_)
                pb.tick()
            df.to_csv(locate_path + f"{tbase}_{tseed}.csv", index=False)
    pb.close()
    # """

    # ===============================================================================================================
    # LINK
    """
    # tp.quiet()
    # pb = icaps.SimpleProgressBar(len(tbases) * len(tseeds) * len(memory) * len(searchradii), "Linking")
    for tbase in tbases:
        for tseed in tseeds:
            filename = f"{tbase}_{tseed}"
            df = pd.read_csv(locate_path + filename + ".csv")
            for memory_ in memory:
                for searchrad in searchradii:
                    # pb.set_stage(f"{tbase} {tseed} {memory_} {searchrad}")
                    df_link = tp.link_df(df, search_range=searchrad, memory=memory_)
                    df_link.to_csv(link_path + filename + f"_{memory_}_{searchrad}.csv", index=False)
                    # pb.tick()
    # pb.close()
    """


if __name__ == '__main__':
    main()


