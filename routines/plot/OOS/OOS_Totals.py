import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "XZ"
project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/"
orig_path = project_path + "orig_strip/"
plot_path = project_path + "totals/"
csv_total = project_path + "total_intensity_strip_XZ.csv"
icaps.mk_folder(plot_path)

out_ex0 = {"beta": [0.84958653, 1.53913371], "sd_beta": [0.06888633, 0.11342147]}
ex0_simple = 10 ** 1.30501706
ex0_simple_err = ex0_simple * np.log(10) * 0.03489476

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def adapt_n(ex_, ex_err_=None):
    n_ = (ex_ / (10 ** out_ex0["beta"][1])) ** (1 / (out_ex0["beta"][0]))
    if ex_err_ is not None:
        n_low_ = n_ - ((ex_ - ex_err_) / (10 ** (out_ex0["beta"][1]
                                                 + out_ex0["sd_beta"][1]))) ** (1 / (out_ex0["beta"][0]
                                                                                  + out_ex0["sd_beta"][0]))
        n_high_ = ((ex_ + ex_err_) / (10 ** (out_ex0["beta"][1]
                                             - out_ex0["sd_beta"][1]))) ** (1 / (out_ex0["beta"][0]
                                                                              - out_ex0["sd_beta"][0])) - n_
        return n_, n_low_, n_high_
    return n_


def main():
    df = pd.read_csv(csv_total)
    shp = icaps.load_img(orig_path + icaps.generate_file(0, prefix=None, digits=5)).shape
    volume = shp[0] * shp[1]
    volume *= icaps.const.px_oos ** 2 * icaps.const.dof_oos * 1e-9

    # ===================================================================================
    # MASS DENSITY
    # ===================================================================================
    _, ax = plt.subplots()
    y, y_low, y_high = adapt_n(df["tot"], 0)
    _, _, bars = ax.errorbar(df["frame"] / icaps.const.fps_oos,
                             y / volume,
                             c="black", markersize=1, fmt="o", label=r"Power Law Calibration")
    y = df["tot"] / ex0_simple
    _, _, bars = ax.errorbar(df["frame"] / icaps.const.fps_oos,
                             y / volume,
                             c="gray", markersize=1, fmt="o", label=r"Linear Calibration")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel(r"Mass Density ($m_{\rm 0} mm^{-3}$)")
    ax.set_yscale("log")
    # ax.set_xscale("log")
    ax.grid()
    ax.minorticks_on()
    icaps.mirror_axis(ax, axis="both")
    # plt.subplots_adjust(left=0.2, right=0.92)
    ax.legend(fancybox=False)
    plt.tight_layout()
    plt.savefig(plot_path + f"Totals_{plane}.png", dpi=dpi)
    plt.close()


if __name__ == '__main__':
    main()


