import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# plane = "XZ"
# thresh_key = "T13_100"
# # thresh_key = "T50_255"
# project_path = drive_letter + f":/icaps/data/OOS_Growth/{plane}/{thresh_key}/"
# stage_name = icaps.gen_oos_stagename(plane, "U", from_=1, to=7, masked=True, totals=False)
# csv_tracks = project_path + f"OOS_{stage_name}.csv"
# plot_path = project_path + f"plots/{stage_name}/"
# sub_plot_path = plot_path + "timelines/"
# icaps.mk_folders([project_path, plot_path, sub_plot_path])

project_path = drive_letter + ":/icaps/data/OOS_EScans/EScan1/"
csv_tracks = project_path + "OOS_XZ_00300_00700_Noah.csv"
plot_path = project_path + "plots/"
sub_plot_path = plot_path + "timelines/"
icaps.mk_folders([plot_path, sub_plot_path])
orig_path = drive_letter + ":/icaps/data/OOS_Growth/XZ/orig/"

dpi = 600
plt.rcParams.update({"font.size": 15})
plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
plt.rc("text", usetex=True)


def main():
    df = pd.read_csv(csv_tracks)
    df = icaps.filter_track_length(df, 250)
    particles = df["particle"].unique()
    pb = icaps.SimpleProgressBar(len(particles))
    cmap = plt.get_cmap("binary_r")
    for particle in particles:
        df_ = df[df["particle"] == particle]
        fig, ax = plt.subplots(nrows=2)
        fig.subplots_adjust(hspace=0)
        t0 = np.min(df_["frame"]) / icaps.const.fps_oos
        frame_max = df_.loc[df_["mass"] == np.max(df_["mass"]), "frame"].to_numpy()[0]
        idx = np.argmin(np.abs(df_["mass"].to_numpy() - np.median(df_["mass"])))
        frame_median = df_["frame"].to_numpy()[idx]
        # img_median = icaps.load_img(orig_path + icaps.generate_file(frame_median, prefix=None, digits=5))
        # tl = df_.loc[df_["frame"] == frame_median, ["bx", "by"]].to_numpy()[0]
        # wh = df_.loc[df_["frame"] == frame_median, ["bw", "bh"]].to_numpy()[0]
        # img_median = icaps.crop(img_median, tl=tl, br=wh, wh=True)
        # ins = ax[0].inset_axes([0.0, 0.3, 0.3, 0.5])
        # ins.xaxis.set_ticklabels([])
        # ins.yaxis.set_ticklabels([])
        # ins.pcolor(img_median, cmap=cmap)
        # print(wh)
        #
        # img_max = icaps.load_img(orig_path + icaps.generate_file(frame_max, prefix=None, digits=5))
        # tl = df_.loc[df_["frame"] == frame_max, ["bx", "by"]].to_numpy()[0]
        # wh = df_.loc[df_["frame"] == frame_max, ["bw", "bh"]].to_numpy()[0]
        # img_max = icaps.crop(img_max, tl=tl, br=wh, wh=True)
        # ins = ax[0].inset_axes([0.4, 0.3, 0.3, 0.5])
        # ins.xaxis.set_ticklabels([])
        # ins.yaxis.set_ticklabels([])
        # ins.pcolor(img_max, cmap=cmap)
        # print(wh)

        for i, param in enumerate(["mass", "area"]):
            norm = 1
            # if param == "mass":
            #     norm = 255
            ax[i].grid()
            ax[i].plot(df_["frame"] / icaps.const.fps_oos - t0, df_[param] / norm, c="black", lw=1)
            ax[i].axhline(np.mean(df_[param]), c="red", lw=1, ls="--", label="Mean")
            ax[i].axhline(np.median(df_[param]), c="blue", lw=1, ls="-.", label="Median")
            if param == "mass":
                ax[i].set_ylabel(r"$I \,\rm (counts)$")
            else:
                ax[i].set_ylabel(r"$A \,\rm (px)$")
            ax[i].set_yscale("log")
        ax[0].legend(loc=(0.0, 1.037), fancybox=False, ncol=2)
        ax[1].set_xlabel(r"$t \,\rm (s)$")
        plt.savefig(sub_plot_path + f"timeline_{particle}.png", dpi=dpi)
        plt.close()
        pb.tick()
    pb.close()


if __name__ == '__main__':
    main()



