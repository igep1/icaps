import icaps
from scipy.optimize import curve_fit
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_XZ/"
in_path = project_path + "orig_strip/"
out_path = project_path + "total_hist.csv"
scans = [
    (1176, 1542),
    (2226, 2577),
    (3277, 3630),
    (4326, 4699),
    (5376, 5776),
    (6426, 6839),
    (7477, 8858)
]
fps = 50

files, _ = icaps.get_files(in_path)
files = icaps.drop_intervals(files, scans + [(0 * fps, 30 * fps), (110 * fps, 200 * fps)], prefix=None)

# bins = np.arange(0, 257, 1)
# tot_hist = np.zeros(256)
# pb = icaps.ProgressBar(len(files), "Calculating Histograms")
# for file in files:
#     img = icaps.load_img(in_path + file)
#     hist, _ = np.histogram(img, bins=bins)
#     tot_hist += hist
#     pb.tick()
# pb.finish()
# df = pd.DataFrame(data={"intensity": bins[:-1], "frequency": tot_hist})
# df.to_csv(out_path, index=False)

df = pd.read_csv(out_path)
_, ax = plt.subplots()
ax.scatter(df["intensity"], df["frequency"]/len(files), s=1, color="black")
ax.set_yscale("log")
ax.set_ylabel("Mean Frequency per Frame")
ax.set_xlabel("Intensity")
ax.grid()
ax.set_axisbelow(True)
# plt.show()
plt.close()

_, ax = plt.subplots()
x = df["intensity"].to_numpy()[170:-1]
y = (df["frequency"]/len(files)).to_numpy()[170:-1]
y_255 = (df["frequency"]/len(files)).to_numpy()[-1]


def pow(x, p0, p1):
    return p0 * x ** p1


popt, pcov = curve_fit(pow, x, y)
xvals = np.arange(170, 550, 1)
ax.plot(xvals, pow(xvals, *popt), color="red", label="Powerfit\np0*x^p1\n"
                                                     + "p0 = " + str(popt[0]) + "\n"
                                                     + "p1 = " + str(popt[1])
        )

prev_val = 0
next_val = 0
i = 255
while next_val < y_255:
    prev_val = next_val
    next_val += pow(i, *popt)
    i += 1
val_idx = np.argmin([y_255-prev_val, y_255-next_val])
val = np.array([prev_val, next_val])[val_idx]
max_int = np.array([i-2, i-1])[val_idx]
print(max_int, val)

ax.scatter(max_int, pow(max_int, *popt), marker="x", color="blue", label=str(max_int))

ax.scatter(x, y, s=5, color="black")
# ax.set_yscale("log")
ax.set_ylabel("Mean Frequency per Frame")
ax.set_xlabel("Intensity")
ax.grid()
ax.set_axisbelow(True)
plt.show()






