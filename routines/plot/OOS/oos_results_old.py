import pandas

import icaps
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_Growth/"
match_path = project_path + "matching/"
match_plot_path = match_path + "plots/"
plot_path = project_path + "plots/"
csv_match = match_path + "Match_Results_same_tracklength.csv"
csv_match_clean = match_path + "Match_Results_same_tracklength_clean.csv"
csv_ldm_track = match_path + "LDM_Tracked_Neighbor_original.csv"
csv_oos_track = match_path + "OOS_Tracked_Neighbor_original.csv"
icaps.mk_folders([plot_path, match_plot_path], clear=False)
dpi = 600

err_type = "_median"
err_col = "{}_mass_err{}".format("{}", err_type)

ldm_ex0 = 291
ldm_ex0_err = 1


def parse_liststr(s, delimiter=";", ret_list=False):
    s = s[1:-1].replace("\n", "").replace(" ", ";").split(";")
    if "" in s:
        s = list(filter(lambda a: a != "", s))
    if ret_list:
        try:
            return np.array([float(s_) for s_ in s])
        except ValueError:
            return s
    return delimiter.join(s)


def main():
    pandas.set_option("display.max_rows", None)
    pandas.set_option("display.max_columns", None)
    pandas.set_option("display.width", None)

    df_match = pd.read_csv(csv_match)
    df_match.rename(columns={"frames_ldm": "ldm_frames", "frames_oos": "oos_frames",
                             "ldm_mass": "ldm_mass_old", "oos_mass": "oos_mass_old",
                             "ldm_area": "ldm_area_old", "oos_area": "oos_area_old"}, inplace=True)
    df_match.drop(columns=["ldm_mass_old", "oos_mass_old", "ldm_area_old", "oos_area_old"], inplace=True)
    df_match["ldm_x"] = df_match["ldm_x"].apply(parse_liststr)
    df_match["ldm_y"] = df_match["ldm_y"].apply(parse_liststr)
    df_match["ldm_frames"] = df_match["ldm_frames"].apply(parse_liststr)
    df_match["oos_frames"] = df_match["oos_frames"].apply(parse_liststr)
    df_match["oos_x"] = df_match["oos_x"].apply(parse_liststr)
    df_match["oos_y"] = df_match["oos_y"].apply(parse_liststr)
    df_match["ldm_mass"] = 0.
    df_match["ldm_mass_err"] = 0.
    df_match["oos_mass"] = 0.
    df_match["oos_mass_err"] = 0.
    df_match["ldm_area"] = 0.
    df_match["ldm_area_err"] = 0.
    df_match["oos_area"] = 0.
    df_match["oos_area_err"] = 0.
    ldm_ids = df_match["ldm_id"].to_numpy()
    df_ldm = pd.read_csv(csv_ldm_track)
    # for particle in df_ldm["particle"].unique():
    #     _, ax = plt.subplots()
    #     x = np.sort(df_ldm[df_ldm["particle"] == particle]["gyrad"].to_numpy())
    #     y = np.linspace(1 / len(x), 1, len(x))
    #     ax.scatter(x, y, c="black", s=1)
    #     ax.set_xscale("log")
    #     plt.show()
    df_ldm = df_ldm[df_ldm["particle"].isin(ldm_ids)]
    oos_ids = df_match["oos_id"].to_numpy()
    df_oos = pd.read_csv(csv_oos_track)
    df_oos = df_oos[df_oos["particle"].isin(oos_ids)]
    p_low = (1 - icaps.const.var1_inside) * 1 / 2
    p_high = 1 - p_low
    for i, row in df_match.iterrows():
        ldm_id = row["ldm_id"]
        ldm_frames = df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_frames"].to_numpy()[0].split(";")
        ldm_frames = [int(float(ldm_frame)) for ldm_frame in ldm_frames]
        ldm_mass = df_ldm.loc[(df_ldm["particle"] == ldm_id) & (df_ldm["frame"].isin(ldm_frames)), "mass"].to_numpy()
        ldm_area = df_ldm.loc[(df_ldm["particle"] == ldm_id) & (df_ldm["frame"].isin(ldm_frames)), "area"].to_numpy()
        df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_mass"] = np.max(ldm_mass)
        median_ex = np.median(ldm_mass)
        df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_mass_err_mean"] = np.mean(np.abs(ldm_mass - median_ex))
        df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_mass_err_median"] = np.median(np.abs(ldm_mass - median_ex))
        q = np.quantile(ldm_mass, p_high) - np.quantile(ldm_mass, p_low)
        df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_mass_err"] = q / (2 * np.sqrt(len(ldm_mass)))
        df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_area"] = np.mean(ldm_area)
        df_match.loc[df_match["ldm_id"] == ldm_id, "ldm_area_err"] = np.std(ldm_area)

        oos_id = row["oos_id"]
        oos_frames = df_match.loc[df_match["oos_id"] == oos_id, "oos_frames"].to_numpy()[0].split(";")
        oos_frames = [int(float(oos_frame)) for oos_frame in oos_frames]
        oos_mass = df_oos.loc[(df_oos["particle"] == oos_id) & (df_oos["frame"].isin(oos_frames)), "mass"].to_numpy()
        oos_area = df_oos.loc[(df_oos["particle"] == oos_id) & (df_oos["frame"].isin(oos_frames)), "area"].to_numpy()
        median_ex = np.median(oos_mass)
        df_match.loc[df_match["oos_id"] == oos_id, "oos_mass"] = median_ex
        df_match.loc[df_match["oos_id"] == oos_id, "oos_mass_err_mean"] = np.mean(np.abs(oos_mass - median_ex))
        df_match.loc[df_match["oos_id"] == oos_id, "oos_mass_err_median"] = np.median(np.abs(oos_mass - median_ex))
        q = np.quantile(oos_mass, p_high) - np.quantile(oos_mass, p_low)
        df_match.loc[df_match["oos_id"] == oos_id, "oos_mass_err"] = q / (2 * np.sqrt(len(oos_mass)))
        df_match.loc[df_match["oos_id"] == oos_id, "oos_area"] = np.mean(oos_area)
        df_match.loc[df_match["oos_id"] == oos_id, "oos_area_err"] = np.std(oos_area)

    # print(df_match)
    df_match.to_csv(csv_match_clean, index=False)

    df_match = pd.read_csv(csv_match_clean)
    _, ax = plt.subplots()
    ax.set_xlabel(r"$m_{\rm LDM}\,(m_{\rm 0})$")
    ax.set_ylabel(r"$E_{\rm OOS}$")
    x = df_match["ldm_mass"].to_numpy() / ldm_ex0
    x_low = (df_match["ldm_mass"].to_numpy() - df_match[err_col.format("ldm")].to_numpy()) / (ldm_ex0 + ldm_ex0_err)
    x_high = (df_match["ldm_mass"].to_numpy() + df_match[err_col.format("ldm")].to_numpy()) / (ldm_ex0 - ldm_ex0_err)
    y = df_match["oos_mass"].to_numpy()
    y_err = df_match[err_col.format("oos")].to_numpy()
    ax.errorbar(x, y,
                xerr=[x - x_low, x_high - x],
                yerr=y_err,
                fmt="o", color="black", markersize=5)
    # ax.errorbar(x, y,
    #             xerr=[x_low, x_high],
    #             yerr=y_err,
    #             fmt="o", color="black", markersize=5)
    ax.set_xscale("log")
    ax.set_yscale("log")
    out = icaps.fit_model(icaps.fit_lin0, x, y,
                          xerr=np.mean([x - x_low, x_high - x], axis=0),
                          yerr=y_err
                          )
    oos_ex0 = out.beta[0]
    oos_ex0_err = out.sd_beta[0]
    oos_ex0 = np.mean([17.2, 16.6])
    xspace = np.linspace(*ax.get_xlim(), 1000)
    ax.plot(xspace, icaps.fit_lin0(xspace, *out.beta), c="red", alpha=0.5,
            label=r"$E_{\rm OOS, 0}$" + r"$ = {{{:.1f}}} \pm {{{:.1f}}}$".format(out.beta[0], out.sd_beta[0]))
    ax.legend()
    icaps.mirror_axis(ax)
    plt.savefig(match_plot_path + "LDM_OOS_Match.png", dpi=dpi)
    plt.close()

    coll_out_path = project_path + "wacca_xz_undist_1_5_coll.csv"

    # df_oos_track = pd.read_csv(drive_letter + ":/icaps/data/OOS_XZ/wacca/wacca_xz_1to5.csv")
    # df_oos_track = pd.read_csv(drive_letter + ":/icaps/data/OOS_XZ/wacca/01560_02200_001/wacca_rot_link_01560_02200_001.csv")
    # for particle in df_oos_track["particle"].unique():
    #     _, ax = plt.subplots()
    #     x = np.sort(df_oos_track[df_oos_track["particle"] == particle]["mass"].to_numpy())
    #     y = np.linspace(1 / len(x), 1, len(x))
    #     ax.scatter(x, y, c="black", s=1)
    #     ax.set_xscale("log")
    #     plt.show()

    # df_oos_track = df_oos_track[df_oos_track["area"] >= 5]
    # frames = df_oos_track["frame"].unique()
    # df_oos = pd.DataFrame(data={"frame": frames})
    # df_oos["median_ex"] = 0.
    # df_oos["max_ex"] = 0.
    # pb = icaps.ProgressBar(len(frames))
    # for frame in frames:
    #     df_oos_track_ = df_oos_track[df_oos_track["frame"] == frame]
    #     # _, ax = plt.subplots()
    #     # hist, bins = np.histogram(df_oos_track_["mass"], bins=1000)
    #     # ax.scatter(bins[:-1], hist, c="black", s=1)
    #     # ax.set_xscale("log")
    #     # plt.show()
    #     oos_mass = df_oos_track_["mass"].to_numpy()
    #     median_ex = np.median(oos_mass)
    #     df_oos.loc[df_oos["frame"] == frame, "median_ex"] = median_ex
    #     df_oos.loc[df_oos["frame"] == frame, "max_ex"] = np.max(df_oos_track_["mass"])
    #     pb.tick()
    # df_oos.to_csv(coll_out_path, index=False)

    df_oos = pd.read_csv(coll_out_path)
    _, ax = plt.subplots()
    ax.set_xlabel(r"Time ($s$)")
    ax.set_ylabel(r"Mass ($m_{\rm 0}$)")
    ax.set_yscale("log")
    ax.minorticks_on()
    ax.grid()
    ax.set_ylim(bottom=1, top=1e5)
    max_mass = df_oos["max_ex"] / oos_ex0
    median_mass = df_oos["median_ex"] / oos_ex0
    median_mass_high = df_oos["median_ex"] / (oos_ex0 - oos_ex0_err)
    median_mass_low = df_oos["median_ex"] / (oos_ex0 + oos_ex0_err)
    max_mass_high = df_oos["max_ex"] / (oos_ex0 - oos_ex0_err)
    max_mass_low = df_oos["max_ex"] / (oos_ex0 + oos_ex0_err)
    # ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["max_ex"] / oos_ex0,
    #             yerr=[max_mass - max_mass_low, max_mass_high - max_mass],
    #             c="red", fmt="o", markersize=0, alpha=0.3, zorder=0)
    ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["max_ex"] / oos_ex0,
                c="red", fmt="o", markersize=1, label="Max", zorder=3, alpha=1)
    # ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["median_ex"] / oos_ex0,
    #             yerr=[median_mass - median_mass_low, median_mass_high - median_mass],
    #             c="black", fmt="o", markersize=0, alpha=0.3, zorder=0)
    ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["median_ex"] / oos_ex0,
                c="black", fmt="o", markersize=1, label="Median", zorder=3, alpha=1)
    ax.legend()
    icaps.mirror_axis(ax)
    plt.savefig(plot_path + "XZ_Undist_1to5_Coll.png", dpi=dpi)
    plt.close()

    # _, ax = plt.subplots()
    # ax.set_xlabel(r"Time ($s$)")
    # ax.set_ylabel(r"Mass ($m_{\rm 0}$)")
    # ax.set_yscale("log")
    # ax.minorticks_on()
    # ax.grid()
    # ax.set_ylim(bottom=1, top=1e5)
    # max_mass = df_oos["max_ex"] / oos_ex0
    # median_mass = df_oos["median_ex"] / oos_ex0
    # median_mass_high = df_oos["median_ex"] / (oos_ex0 - oos_ex0_err)
    # median_mass_low = df_oos["median_ex"] / (oos_ex0 + oos_ex0_err)
    # max_mass_high = df_oos["max_ex"] / (oos_ex0 - oos_ex0_err)
    # max_mass_low = df_oos["max_ex"] / (oos_ex0 + oos_ex0_err)
    # # ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["max_ex"] / oos_ex0,
    # #             yerr=[max_mass - max_mass_low, max_mass_high - max_mass],
    # #             c="red", fmt="o", markersize=0, alpha=0.3, zorder=0)
    # ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["max_ex"] / oos_ex0,
    #             c="red", fmt="o", markersize=1, label="Max", zorder=3, alpha=1)
    # # ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["median_ex"] / oos_ex0,
    # #             yerr=[median_mass - median_mass_low, median_mass_high - median_mass],
    # #             c="black", fmt="o", markersize=0, alpha=0.3, zorder=0)
    # ax.errorbar(df_oos["frame"] / icaps.const.fps_oos, df_oos["median_ex"] / oos_ex0,
    #             c="black", fmt="o", markersize=1, label="Median", zorder=3, alpha=1)
    # ax.legend()
    # icaps.mirror_axis(ax)
    # plt.savefig(plot_path + "XZ_Undist_1to5_Coll.png", dpi=dpi)
    # plt.close()


if __name__ == '__main__':
    main()

