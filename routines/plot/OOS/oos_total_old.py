import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/OOS_XZ/"
csv_total = project_path + "total_intensity_strip.csv"
oos_ex0 = np.mean([17.2, 16.6])


def main():
    df = pd.read_csv(csv_total)
    _, ax = plt.subplots()
    ax.minorticks_on()
    ax.errorbar(df["frame"] / icaps.const.fps_oos,
                df["tot"] / oos_ex0 / (1024 * 550 * (icaps.const.px_oos / 1e3) ** 2 * icaps.const.dof_oos / 1e3),
                fmt="o", markersize=1, c="black")
    ax.grid()
    ax.set_xlabel(r"Time ($s$)")
    # ax.ticklabel_format(axis="y", style="scientific")
    ax.set_ylabel(r"Mass Density ($m_{\rm 0}\, mm^{-3}$)")
    # ax.set_yscale("log")

    plt.savefig(project_path + "tot.png", dpi=600)
    plt.close()


if __name__ == '__main__':
    main()


