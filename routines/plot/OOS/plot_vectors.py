import icaps
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import trackpy as tp

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
plane = "XZ"
project_path = drive_letter + ":/icaps/data/OOS_" + plane + "/wacca/"

recalculate_velocities = True

# start = 1560
# stop = 2200
# start = 2580
# stop = 3270
start = 3630
stop = 4325
# start = 4700
# stop = 5375
step = 1
interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)

min_dt = 10

wacca_path = project_path + interval_string + "/"
csv_path = wacca_path + "wacca_rot_link_" + interval_string + ".csv"
vel_csv_path = wacca_path + "wacca_rot_link_vel_" + interval_string + "_dt" + "{:0>2}".format(min_dt) + ".csv"
plot_out_path = wacca_path + "quiver_single_" + interval_string + ".png"
plot_out_path2 = wacca_path + "quiver_field_" + interval_string + ".png"
img_width = 1024
img_height = 550


def plot_vectors(df):
    df = tp.filter_stubs(df, threshold=150)
    particles = np.unique(df["particle"].to_numpy())
    _, ax = plt.subplots()

    pb = icaps.ProgressBar(len(particles), "Plotting Velocities")
    x = np.zeros(len(particles))
    y = np.zeros(len(particles))
    vx = np.zeros(len(particles))
    vy = np.zeros(len(particles))
    for i, particle in enumerate(particles):
        df_ = df[df["particle"] == particle]
        x[i] = df_["x"].mean()
        y[i] = df_["y"].mean()
        vx[i] = df_["vx"].mean()
        vy[i] = df_["vy"].mean()
        pb.tick()
    pb.finish()
    ax.quiver(x, y, vx, vy)
    ax.set_xlabel("x in px" if "X" in plane else "y in px")
    ax.set_ylabel("z in px")
    ax.set_xlim(0, img_width)
    ax.set_ylim(img_height, 0)
    plt.savefig(plot_out_path, dpi=600)


def plot_vector_regions(df, count_norm=True):
    # df = tp.filter_stubs(df, threshold=150)
    region_size = 50
    xlims = [i for i in range(0, img_width, region_size)]
    xlims.append(img_width)
    ylims = [i for i in range(0, img_height, region_size)]
    ylims.append(img_height)
    _, ax = plt.subplots()
    ax.set_xlim(0, img_width)
    ax.set_ylim(img_height, 0)
    ax.set_xlabel("x in px" if "X" in plane else "y in px")
    ax.set_ylabel("z in px")
    ax.set_aspect(1)

    for xlim in xlims[1:-1]:
        ax.axvline(xlim, alpha=0.3, color="gray", linestyle="--")
    for ylim in ylims[1:-1]:
        ax.axhline(ylim, alpha=0.3, color="gray", linestyle="--")

    x = []
    y = []
    vx = []
    vy = []
    n = None
    if count_norm:
        n = []
    for j, ylim in enumerate(ylims[:-1]):
        df_y = df[df["y"] >= ylim]
        df_y = df_y[df_y["y"] < ylims[j + 1]]
        y_ = round((ylim + ylims[j + 1]) / 2)
        for i, xlim in enumerate(xlims[:-1]):
            df_x = df_y[df_y["x"] >= xlim]
            df_x = df_x[df_x["x"] < xlims[i + 1]]
            x_ = round((xlim + xlims[i + 1]) / 2)
            # print(df_x["vx"])
            vx_ = df_x["vx"].mean()
            vy_ = df_x["vy"].mean()
            if not np.isnan(vx_) and not np.isnan(vy_):
                if count_norm:
                    n.append(np.unique(df_x["particles"].to_numpy()).shape[0])
                y.append(y_)
                x.append(x_)
                vx.append(vx_)
                vy.append(vy_)
    x = np.array(x)
    y = np.array(y)
    vx = np.array(vx)
    vy = np.array(vy)
    n = np.array(n)
    print(np.mean(vx))
    print(np.mean(vy))
    print(np.array([vx, vy]))
    print(np.transpose(np.array([vx, vy])))
    print(np.linalg.norm(np.transpose(np.array([vx, vy])), axis=1))
    print(np.mean(np.linalg.norm(np.transpose(np.array([vx, vy])), axis=1)))
    print(np.std(np.linalg.norm(np.transpose(np.array([vx, vy])), axis=1)))
    if count_norm:
        ax.quiver(x, y, vx/n, vy/n, pivot="middle", angles="xy", scale=0.001, scale_units="xy")
    else:
        ax.quiver(x, y, vx, vy, pivot="middle", angles="xy", scale=0.001, scale_units="xy")
    ax.set_title("Velocity Scale: mm/s")
    plt.savefig(plot_out_path2, dpi=600)


def plot_traj(df):
    df = tp.filter_stubs(df, 20)
    particles = np.unique(df["particle"])
    _, ax = plt.subplots()
    for particle in particles:
        df_ = df[df["particle"] == particle]
        ax.set_xlim(0, img_width)
        ax.set_ylim(img_height, 0)
        ax.set_xlabel("x in px" if "X" in plane else "y in px")
        ax.set_ylabel("z in px")
        x = df_["x"].to_numpy()
        y = df_["y"].to_numpy()
        ax.scatter(x, y, s=1)
        ax.scatter(x[0], y[0], s=3, marker="x", color="black")
        ax.text(x[0], y[0], str(int(particle)), color="black", fontsize=6)
        vx = df_["vx"].mean()
        vy = df_["vy"].mean()
        ax.quiver(x[0], y[0], vx, vy, angles="xy", scale=1, scale_units="xy")
    plt.show()


def plot_single_traj(df, particle=12660):
    _, ax = plt.subplots()
    df_ = df[df["particle"] == particle]
    ax.set_xlabel("x in px" if "X" in plane else "y in px")
    ax.set_ylabel("z in px")
    ax.invert_yaxis()
    x = df_["x"].to_numpy()
    y = df_["y"].to_numpy()
    ax.scatter(x, y, s=1)
    ax.scatter(x[0], y[0], s=3, marker="x", color="black")
    ax.text(x[0], y[0], str(int(particle)), color="black", fontsize=6)
    vx = df_["vx"].to_numpy()
    vy = df_["vy"].to_numpy()
    ax.quiver(x, y, vx, vy, angles="xy", scale=1, scale_units="xy")
    plt.show()


def main():
    df = None
    # if recalculate_velocities:
    #     df = pd.read_csv(csv_path)
    #     # df = tp.filter_stubs(df, threshold=100)
    #     # i = 0
    #     # for frame in range(start, stop + 1):
    #     #     if i < 9:
    #     #         df = df[df["frame"] != frame]
    #     #     if i == 9:
    #     #         i = -1
    #     #     i += 1
    #     # tp.plot_traj(df)
    #     df = icaps.calc_velocities(df, min_dt=min_dt, show_progress=True)
    #     df.to_csv(vel_csv_path, index=False)
    if df is None:
        df = pd.read_csv(vel_csv_path)
    # plot_vectors(df)
    # df = df[df["frame"] > 2900]
    # df = df[df["frame"] < 3100]
    # plot_traj(df)
    # plot_single_traj(df)
    plot_vector_regions(df, count_norm=False)


if __name__ == '__main__':
    main()







