import icaps
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
# from matplotlib import rc
# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
# rc('text', usetex=True)

data_path = "D:/icaps/data/OOS_XZ/wacca/wacca_xz.csv"


def general_func(t, k, t_c, c):
    return ((1 - k) * ((t / t_c) + c))**(1 / (1 - k))


def general_func2(t, q, t_c, c):
    return (q * (t / t_c + c))**(1 / q)


def specific_func(t, t_c, c):
    return np.exp(t / t_c + c)


def get_kappa_func(k=None, t_c=None, c=None):
    if k is not None:
        assert t_c is None
        if k == 1:
            if c is not None:
                return lambda x1, x2: specific_func(t=x1, t_c=x2, c=c)
            else:
                return specific_func
        else:
            if c is not None:
                return lambda x1, x2: general_func(t=x1, k=k, t_c=x2, c=c)
            else:
                return lambda x1, x2, x3: general_func(t=x1, k=k, t_c=x2, c=x3)
    else:
        if t_c is not None:
            if c is not None:
                return lambda x1, x2: general_func(t=x1, k=x2, t_c=t_c, c=c)
            else:
                return lambda x1, x2, x3: general_func(t=x1, k=x2, t_c=t_c, c=x3)
        else:
            if c is not None:
                return lambda x1, x2, x3: general_func(t=x1, k=x2, t_c=x3, c=c)
            else:
                return general_func


def main():
    df = pd.read_csv(data_path)
    frames = np.unique(df["frame"].to_numpy())
    time = frames/50
    int_median = np.zeros(frames.size)
    for i, frame in enumerate(frames):
        df_ = df[df["frame"] == frame]
        int_median[i] = icaps.cumulative_median(df_["mass"])

    trange = np.linspace(np.min(time), np.max(time), 1000)
    func1 = get_kappa_func(k=1)
    popt1, pcov1 = curve_fit(func1, time, int_median)
    perr1 = np.sqrt(np.diag(pcov1))
    func2 = get_kappa_func(t_c=48, c=-15)
    popt2, pcov2 = curve_fit(func2, time, int_median, p0=[1.2])
    perr2 = np.sqrt(np.diag(pcov2))
    func3 = get_kappa_func(k=1.09, c=-15)
    popt3, pcov3 = curve_fit(func3, time, int_median, p0=[48])
    perr3 = np.sqrt(np.diag(pcov3))
    # popt4, pcov4 = curve_fit(lambda x1, x2: general_func2(x1, x2, t_c=48, c=10), time, int_median, p0=[-0.5])
    # perr4 = np.sqrt(np.diag(pcov4))

    _, ax = plt.subplots()
    ax.scatter(time, int_median, s=1, color="black")
    ax.plot(trange, func1(trange, *popt1), color="red", linewidth=1, linestyle="-",
            label="k = " + str(1) + "\n" +
                  "t_c = " + str(np.round(popt1[0], 2)) + " \u00b1 " + str(np.round(perr1[0], 2)) + "\n" +
                  "c = " + str(np.round(popt1[1], 2)) + " \u00b1 " + str(np.round(perr1[1], 2)))
    ax.plot(trange, func2(trange, *popt2), color="red", linewidth=1, linestyle="--",
            label="k = " + str(np.round(popt2[0], 2)) + " \u00b1 " + str(np.round(perr2[0], 2)) + "\n" +
                  "t_c = " + str(48) + "\nc = " + str(-10))

    ax.plot(trange, func3(trange, *popt3), color="red", linewidth=1, linestyle="-.",)
            # label="k = " + str(np.round(popt3[0], 2)) + " \u00b1 " + str(np.round(perr3[0], 2)) + "\n" +
            #       "t_c = " + str(np.round(popt3[1], 2)) + " \u00b1 " + str(np.round(perr3[1], 2)))
    # ax.plot(trange, general_func2(trange, t_c=48, c=10, *popt4), color="red", linewidth=1, linestyle="-.",
    #         label="k = " + str(np.round(1-popt4[0], 2)) + " \u00b1 " + str(np.round(perr4[0], 2)) + "\n" +
    #               "t_c = " + str(48))
    ax.grid()
    ax.set_axisbelow(True)
    ax.set_xlabel("Time in s")
    ax.set_ylabel("Median Intensity")
    ax.legend()
    plt.show()


if __name__ == '__main__':
    main()


