import icaps

# in_path = "H:/icaps/data/LDM_ROT2/particles/720240/mark/"
# out_path = "H:/icaps/data/LDM_ROT2/particles/720240/movie.mp4"

id = 555472
in_path = "H:/icaps/data/TrackPlot/tracks/{}/".format(id)
out_path = "H:/icaps/data/TrackPlot/movies/{}_fast.mp4".format(id)

icaps.mk_movie(in_path + "(::1).bmp", out_path, fps=15)

