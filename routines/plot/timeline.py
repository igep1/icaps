import icaps
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
hz = 10
log_file_path = drive_letter + ":/icaps/data/log_" + str(hz) + "Hz_15112019_113624.csv"


def main():
    df = pd.read_csv(log_file_path, delimiter=";")
    time = df["time(ms)"]
    time = (time - time[0])/1000
    temperature = df["TC1"]
    pressure = df["GV-PV"]

    matplotlib.rcParams.update({'font.size': 12})

    _, ax = plt.subplots()
    ax.set_xlim(-15, np.amax(time)+15)
    ax.set_xlabel("time in s")
    ax.set_ylabel("temperature in °C")
    twinx = ax.twinx()
    twinx.set_ylabel("pressure in mbar")
    ax.plot(time, temperature, c="red", linewidth=1)
    ax.yaxis.label.set_color("red")
    twinx.plot(time, pressure, c="blue", linewidth=1)
    twinx.yaxis.label.set_color("blue")

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()

