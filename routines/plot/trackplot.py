import icaps
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")

# mode = "OOS"
# oos_plane = "XZ"
# project_path = drive_letter + ":/icaps/data/OOS_{}/wacca_fullvolume/escan/".format(oos_plane)
# orig_path = drive_letter + ":/icaps/data/OOS_{}/orig_strip/".format(oos_plane)
# csv_path = drive_letter + ":/icaps/data/OOS_XZ/wacca/01560_02200_001/wacca_rot_link_01560_02200_001.csv"

camera = "OOS"
project_path = drive_letter + ":/icaps/data/OOS_EScans/EScan1/"
orig_path = project_path + "orig_strip/"
# csv_path = project_path + "OOS_XZ_08500_09000.csv"
csv_path = project_path + "OOS_XZ_00300_00700_Noah.csv"

# camera = "LDM"
# project_path = drive_letter + ":/icaps/data/TrackPlot/"
# orig_path = drive_letter + ":/icaps/data/LDM/ffc/"
# csv_path = drive_letter + ":/icaps/data/LDM/Tracks_all.csv"
# # csv_path = drive_letter + ":/icaps/data/LDM/Tracks_P.csv"
# csv_phases_path = drive_letter + ":/icaps/data/LDM/Phases_Hand.csv"

# particle_ids = (8790261, 869082, 3887742, 881673, 873930, 847467, 2004843, 5107687, 594116, 453584, 8567305, 8415536)
particle_ids = (
    1568,
    1674
    # 747039,
    # 847467,
    # 4626139,
    # 954360,
    # 5246542
)

out_path = project_path + "tracks/"
movie_path = project_path + "movies/"
enlarge = 8
if camera == "LDM":
    mark_size = 2
else:
    mark_size = 1
draw_bbox = True
draw_label = True
no_movie = False
crop_bounds = True
color = (0, 0, 255)
frame_gap = 1
fps = None

# df_phases = pd.read_csv(csv_phases_path)
df = pd.read_csv(csv_path)
# df = icaps.filter_edge(df, flag_only=True)
# df = icaps.filter_disturbance(df, df_phases=df_phases, flag_only=True)
icaps.mk_track_movies(df, orig_path, out_path, camera, particle_ids=particle_ids, frame_gap=frame_gap, enlarge=enlarge,
                      movie_path=movie_path, crop_bounds=crop_bounds, draw_bbox=draw_bbox, draw_label=draw_label,
                      mark_size=0, no_movie=no_movie, silent=True, color=color, fps=fps,
                      clear_out=True, clear_after=False,
                      # mark_flags=["flag_edge", "phase"],
                      # flag_color=[(255, 0, 0), (0, 255, 0)]
                      )




