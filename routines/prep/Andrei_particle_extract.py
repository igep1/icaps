import icaps
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
csv_path = drive_letter + ":/icaps/data/LDM_ROT3/Tracks_P.csv"
orig1_path = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_001/"
orig2_path = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_002/"
ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
project_path = drive_letter + ":/icaps/data/Andrei/"
ff_path = project_path + "flatfields/"
particle_path = project_path + "particles/"
parts = np.array([
    [373867, 5245],
    [903408, 80362],
    [371177, 5531],
    [384888, 6663],
    [700669, 56421],
    [667960, 54832],
    [8421148, 295260],
    [720240, 58802],
    [393067, 7789],
    [8424720, 295433],
    [435818, 17766]
])
ff_start = 216700
ff_stop = 260300
ff_randoms = [
    256291,
    232587,
    223682,
    247654,
    228458,
    245854,
    253259,
    237222,
    226957,
    233637
]


def main():
    # icaps.mk_folders([ff_path, particle_path])
    # for ff_random in ff_randoms:
    #     img = icaps.load_img(orig_path + icaps.generate_file(ff_random), strip=True)
    #     icaps.save_img(img, ff_path + "{:0>6}.bmp".format(ff_random))

    ff = icaps.load_img(drive_letter + ":/icaps/data/FFs/ff_216700_260300.npy")
    fit1 = [icaps.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
    fit2 = [icaps.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
    fit_cutoff = 260300
    thresh = 3

    # df = pd.read_csv(csv_path)
    # df = df[df["particle"].isin(parts[:, 0])]
    # df.to_csv(project_path + "tracks.csv", index=False)

    df = pd.read_csv(project_path + "tracks.csv")

    # ==============================================================================
    # CREATE IMAGES
    # df_out = pd.DataFrame(data={"frame": parts[:, 1],
    #                             "particle": parts[:, 0],
    #                             "rx": np.zeros(len(parts), dtype=float),
    #                             "ry": np.zeros(len(parts), dtype=float),
    #                             "area": np.zeros(len(parts), dtype=int),
    #                             "mass": np.zeros(len(parts), dtype=int),
    #                             "gyrad": np.zeros(len(parts), dtype=int),
    #                             "x": np.zeros(len(parts), dtype=float),
    #                             "y": np.zeros(len(parts), dtype=float),
    #                             "bx": np.zeros(len(parts), dtype=int),
    #                             "by": np.zeros(len(parts), dtype=int),
    #                             "bw": np.zeros(len(parts), dtype=int),
    #                             "bh": np.zeros(len(parts), dtype=int)})
    # pb = icaps.ProgressBar(len(parts) * len(ff_randoms), "Progress")
    # for part in parts:
    #     fit = fit1
    #     orig_path = orig1_path
    #     if part[1] >= fit_cutoff:
    #         fit = fit2
    #     if part[1] >= 20000:
    #         orig_path = orig2_path
    #     df_ = df[(df["frame"] == part[1]) & (df["particle"] == part[0])]
    #     orig_img = icaps.load_img(orig_path + icaps.generate_file(part[1]), strip=True)
    #     ffc_img = icaps.ff_subtract(orig_img, ff, t=part[1], fit=fit)
    #     ffc_img = np.around(ffc_img).astype(np.uint8)
    #     bbox = [df_["bx"].to_numpy()[0], df_["by"].to_numpy()[0], df_["bw"].to_numpy()[0], df_["bh"].to_numpy()[0]]
    #     crop_img = icaps.crop(ffc_img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True)
    #     crop_img = icaps.threshold(crop_img, thresh=thresh)
    #     icaps.save_img(crop_img, particle_path + "{}/{}_{:0>6}_thresh{}.bmp".format(part[0], part[0], part[1],
    #                                                                                 thresh))
    #     orig_crop_img = icaps.crop(orig_img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True)
    #     icaps.save_img(orig_crop_img, particle_path + "{}/{}_{:0>6}_orig.bmp".format(part[0], part[0], part[1]))
    #     for ff_random in ff_randoms:
    #         img = icaps.load_img(ff_path + "{:0>6}.bmp".format(ff_random))
    #         crop_img = icaps.crop(img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True)
    #         icaps.save_img(crop_img, particle_path + "{}/{}_{:0>6}_{}_flat.bmp".format(part[0], part[0], part[1],
    #                                                                                    ff_random))
    #         pb.tick()
    #     df_out.loc[df_out["particle"] == part[0], "rx"] = df_["x"].to_numpy()[0] - df_["bx"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "ry"] = df_["y"].to_numpy()[0] - df_["by"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "area"] = df_["area"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "mass"] = df_["mass"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "gyrad"] = df_["gyrad"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "x"] = df_["x"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "y"] = df_["y"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "bx"] = df_["bx"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "by"] = df_["by"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "bw"] = df_["bw"].to_numpy()[0]
    #     df_out.loc[df_out["particle"] == part[0], "bh"] = df_["bh"].to_numpy()[0]
    #
    # pb.finish()
    # df_out.to_csv(project_path + "out.csv", index=False)

    # ==============================================================================
    # OVERLAY ANALYSIS
    # pb = icaps.ProgressBar(len(parts) * len(ff_randoms), "Overlay Analysis")
    # df_pos = pd.DataFrame(data={
    #     "particle": np.repeat(parts[:, 0], len(ff_randoms)),
    #     "frame": np.repeat(parts[:, 1], len(ff_randoms)),
    #     "flatfield": np.tile(ff_randoms, len(parts)),
    #     "nx": np.zeros(len(parts) * len(ff_randoms)),
    #     "ny": np.zeros(len(parts) * len(ff_randoms)),
    #     "rx": np.zeros(len(parts) * len(ff_randoms)),
    #     "ry": np.zeros(len(parts) * len(ff_randoms))
    # })
    # df_out = pd.read_csv(project_path + "out.csv")
    # for part in parts:
    #     df_out_ = df_out[df_out["particle"] == part[0]]
    #     bbox = [df_out_["bx"].to_numpy()[0], df_out_["by"].to_numpy()[0], df_out_["bw"].to_numpy()[0],
    #             df_out_["bh"].to_numpy()[0]]
    #     ff_crop = icaps.crop(ff, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True)
    #     part_path = particle_path + "{}/".format(part[0])
    #     overlay_path = part_path + "overlays/"
    #     prefix = "{}_{:0>6}".format(part[0], part[1])
    #     part_img = icaps.load_img(part_path + "{}_thresh3.bmp".format(prefix))
    #     for ff_random in ff_randoms:
    #         flat_img = icaps.load_img(part_path + "{}_{:0>6}_flat.bmp".format(prefix, ff_random))
    #         flat_img = ff_crop - flat_img
    #         overlay_img = np.sum([flat_img, part_img], axis=0)
    #         # print(overlay_img)
    #         overlay_img[overlay_img < 0] = 0
    #         overlay_img = np.round(overlay_img).astype(np.uint8)
    #         # print(overlay_img)
    #         overlay_img = icaps.threshold(overlay_img, thresh)
    #         # print(overlay_img)
    #         # icaps.cvshow(overlay_img, size=5)
    #         icaps.save_img(overlay_img, overlay_path + "{}_{}_overlay.bmp".format(prefix, ff_random))
    #         nx, ny = icaps.measure_com(overlay_img)
    #         rx = df_out.loc[(df_out["particle"] == part[0]) & (df_out["frame"] == part[1]), "rx"]
    #         rx = rx.to_numpy()[0]
    #         ry = df_out.loc[(df_out["particle"] == part[0]) & (df_out["frame"] == part[1]), "ry"].to_numpy()[0]
    #         df_pos.loc[(df_pos["particle"] == part[0]) & (df_pos["flatfield"] == ff_random), "nx"] = nx
    #         df_pos.loc[(df_pos["particle"] == part[0]) & (df_pos["flatfield"] == ff_random), "ny"] = ny
    #         df_pos.loc[(df_pos["particle"] == part[0]) & (df_pos["flatfield"] == ff_random), "rx"] = rx
    #         df_pos.loc[(df_pos["particle"] == part[0]) & (df_pos["flatfield"] == ff_random), "ry"] = ry
    #         pb.tick()
    # pb.finish()
    # df_pos.to_csv(project_path + "noise_positions.csv", index=False)

    # ==============================================================================
    # PLOTTING
    df_out = pd.read_csv(project_path + "out.csv")
    df_pos = pd.read_csv(project_path + "noise_positions.csv")
    plot_path = project_path + "plots/"
    position_path = plot_path + "positions/"
    offset_path = plot_path + "offsets/"
    icaps.mk_folders([position_path, offset_path], clear=True)
    pb = icaps.ProgressBar(len(parts), "Plotting")
    _, ax0 = plt.subplots()
    # ax0.scatter(0, 0, s=20, marker="x", c="red")
    ax0.axhline(0, ls="--", c="gray")
    ax0.axvline(0, ls="--", c="gray")
    for part in parts:

        df_pos_ = df_pos[df_pos["particle"] == part[0]]
        # df_out_ = df_out[df_out["particle"] == part[0]]
        # w = df_out_["bw"].to_numpy()[0]
        # h = df_out_["bh"].to_numpy()[0]
        nx = df_pos_["nx"].to_numpy()
        ny = df_pos_["ny"].to_numpy()
        rx = df_pos_["rx"].to_numpy()[0]
        ry = df_pos_["ry"].to_numpy()[0]

        # _, ax = plt.subplots()
        # ax.scatter(nx-rx, ny-ry, s=10, c="black")
        # ax.scatter(0, 0, s=20, marker="x", c="red")
        # # ax.set_xlim(-w/2, w/2)
        # # ax.set_ylim(-h/2, h/2)
        # plt.savefig(position_path + "{}_pos.png".format(part[0]))
        # plt.close()

        ax0.scatter(nx-rx, ny-ry, s=10)

        # _, ax = plt.subplots()
        # offx = np.sort(nx - rx)
        # offy = np.sort(ny - ry)
        # offx_cumu = np.linspace(1 / len(offx), 1, len(offx))
        # offy_cumu = np.linspace(1 / len(offy), 1, len(offy))
        # ax.scatter(offx, offx_cumu, s=10, c="blue", label="x", marker="o")
        # ax.scatter(offy, offy_cumu, s=10, c="red", label="y", marker="x")
        # ax.legend()
        # plt.savefig(offset_path + "{}_off.png".format(part[0]))
        # plt.close()
        pb.tick()
    pb.finish()
    plt.show()

    # ==============================================================================
    # OTHER STUFF
    # orig_img = icaps.load_img(orig2_path + icaps.generate_file(295433))
    # icaps.save_img(orig_img, project_path + "295433_orig.bmp")
    # ffc_img = icaps.load_img(ffc_path + icaps.generate_file(295433))
    # df2 = icaps.cca_fast(ffc_img, thresh=3, frame=295433, mark_path=project_path + "295433_mark.bmp",
    #                      mark_labels=True)
    # pd.set_option("display.max_rows", 10000)
    # print(df2[["label", "area", "mass"]])

    # img = icaps.load_img(particle_path + "8424720/8424720_295433_crop.bmp")
    # print(df[(df["frame"] == parts[-1][1]) & (df["particle"] == parts[-1][0])][["frame", "particle", "mass"]])
    # mass = df.loc[(df["frame"] == parts[-1][1]) & (df["particle"] == parts[-1][0]), "mass"].to_numpy()[0]
    # print("table mass", mass)
    # print("t0 mass", np.sum(img))
    # print("t3 mass", np.sum(icaps.threshold(img, 49)))


if __name__ == '__main__':
    main()

