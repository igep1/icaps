import icaps
import cv2
import numpy as np

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# in_path = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_002/"
# out_path = drive_letter + ":/icaps/data/FFs/"
# start = 216700
# stop = 260300

plane = "XZ"
start = 11100
stop = 13000
in_path = drive_letter + ":/icaps/data/OOS/" + plane + "/empty_strip/"
out_path = drive_letter + ":icaps/data/OOS/" + plane + "/"

img = icaps.average_ff(in_path, strip=False, show_progress=True)
cv2.imwrite(out_path + "ff_" + f'{start:06d}' + "_" + f'{stop:06d}' + ".bmp", img)
np.save(out_path + "ff_" + f'{start:06d}' + "_" + f'{stop:06d}' + ".npy", img)


