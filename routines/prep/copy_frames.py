import icaps
import numpy as np
import pandas as pd


def main():
    drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
    ffc_path = drive_letter + ":/icaps/data/LDM/ffc/"
    df_in_path = drive_letter + ":/icaps/data/LDM_ROT/Tracks_PS20L500.csv"
    out_path = drive_letter + ":/icaps/data/LDM_ROT/frames/"
    icaps.mk_folder(out_path)

    df = pd.read_csv(df_in_path)
    frames = np.unique(df["frame"].to_numpy())
    print("Frames:", len(frames))
    pb = icaps.ProgressBar(len(frames), "Progress")
    for frame in frames:
        img = icaps.load_img(ffc_path + icaps.generate_file(frame))
        df_ = df[df["frame"] == frame]
        stats = df_[["bx", "by", "bw", "bh"]].to_numpy()
        label = df_["particle"].to_numpy()
        img = icaps.draw_labelled_bbox_bulk(img, stats, label, (0, 0, 255))
        icaps.save_img(img, out_path + icaps.generate_file(frame))
        pb.tick()
    pb.finish()


if __name__ == '__main__':
    main()
