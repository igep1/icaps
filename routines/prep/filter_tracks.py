import icaps
import numpy as np
import pandas as pd
import trackpy as tp


def print_stats(df, prev_nframes, prev_nparts, orig_nframes, orig_nparts, title):
    print(title)
    nframes = np.unique(df["frame"].to_numpy()).shape[0]
    nparts = np.unique(df["particle"].to_numpy()).shape[0]
    print("frames:\t\t" + str(nframes) + "\t" + str(np.round((1-nframes/prev_nframes)*100, 2)) + " %" +
          "\t" + str(np.round((1-nframes/orig_nframes)*100, 2)) + " %")
    print("particles:\t" + str(nparts) + "\t" + str(np.round((1-nparts/prev_nparts)*100, 2)) + " %" +
          "\t" + str(np.round((1-nparts/orig_nparts)*100, 2)) + " %")
    print("")
    return nframes, nparts


def filter_sharpness(df, threshold=20, show_progress=True):
    to_drop = np.zeros(len(df.index), np.uint64)
    pb = None
    if show_progress:
        pb = icaps.ProgressBar(len(df.index), "Filtering Sharpness (" + str(threshold) + ")")
    j = 0
    for i, row in df.iterrows():
        if row["mass"]/row["area"] < threshold:
            to_drop[j] = i
        j += 1
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()
    to_drop = to_drop[np.nonzero(to_drop)]
    df = df.drop(index=to_drop)
    return df


def filter_track_length(df, threshold=100, show_progress=True):
    particles = np.unique(df["particle"].to_numpy())
    pb = None
    if show_progress:
        pb = icaps.ProgressBar(particles.shape[0], "Filtering Track Length (" + str(threshold) + ")")
    for particle in particles:
        df_ = df[df["particle"] == particle]
        track_length = np.unique(df_["frame"].to_numpy()).shape[0]
        if track_length < threshold:
            df = df[df["particle"] != particle]
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()
    return df


def filter_area(df, threshold=100, show_progress=True):
    particles = np.unique(df["particle"].to_numpy())
    pb = None
    if show_progress:
        pb = icaps.ProgressBar(particles.shape[0], "Filtering Area (" + str(threshold) + ")")
    for particle in particles:
        df_ = df[df["particle"] == particle]
        max_area = np.max(df_["area"].to_numpy())
        if max_area < threshold:
            df = df[df["particle"] != particle]
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()
    return df


def main():
    drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
    # in_path = drive_letter + ":/icaps/data/LDM/Tracks_all.csv"
    in_path = drive_letter + ":/icaps/data/LDM/Tracks_P.csv"
    # in_path = drive_letter + ":/icaps/data/LDM_ROT/Tracks_PS20L100.csv"
    out_path = drive_letter + ":/icaps/data/LDM_ROT/"
    thresh_sharp = 20
    thresh_length = 500

    df = pd.read_csv(in_path)
    prev_nframes = np.unique(df["frame"].to_numpy()).shape[0]
    prev_nparts = np.unique(df["particle"].to_numpy()).shape[0]
    orig_nframes = prev_nframes
    orig_nparts = prev_nparts
    df_phases = pd.read_csv(drive_letter + ":/icaps/data/LDM/Phases_Hand.csv")
    undisturbed = df_phases[df_phases["phase"] == 0]["frame"].to_numpy()
    prev_nframes, prev_nparts = print_stats(df, prev_nframes, prev_nparts, orig_nframes, orig_nparts,
                                            "No Filter")

    # Disturbance Filter
    df = df[np.in1d(df["frame"].to_numpy(), undisturbed)]
    prev_nframes, prev_nparts = print_stats(df, prev_nframes, prev_nparts, orig_nframes, orig_nparts,
                                            "Disturbance Filter")

    # df.to_csv(out_path + "Tracks_P.csv", index=False)

    # Sharpness Filter
    df = filter_sharpness(df, thresh_sharp)
    prev_nframes, prev_nparts = print_stats(df, prev_nframes, prev_nparts, orig_nframes, orig_nparts,
                                            "Sharpness Filter")

    # Track Length Filter
    df = filter_track_length(df, thresh_length)
    prev_nframes, prev_nparts = print_stats(df, prev_nframes, prev_nparts, orig_nframes, orig_nparts,
                                            "Track Length Filter")

    # df.to_csv(out_path + "Tracks_PS" + str(thresh_sharp) + "L" + str(thresh_length) + ".csv", index=False)


if __name__ == '__main__':
    main()


