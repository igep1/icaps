import pandas as pd
import icaps
import numpy as np


drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM_ROT3/"
in_path = project_path + "Tracks_all.csv"
phase_path = project_path + "Phases_Hand.csv"
edge_dist = 1
track_length = 100
out_path = project_path + "Tracks_PE{}L{}.csv".format(edge_dist, track_length)


def main():
    # df = pd.read_csv(in_path)
    # df_phase = pd.read_csv(phase_path)
    # df = icaps.filter_disturbance(df, df_phase, tally=True, silent=False)
    # df.to_csv(project_path + "Tracks_P.csv", index=False)

    df = pd.read_csv(project_path + "Tracks_P.csv")
    df = icaps.filter_edge(df, distance=edge_dist, tally=True, silent=True)
    print(len(df["particle"].unique()))
    # df.to_csv(project_path + "Tracks_PE{}.csv".format(edge_dist), index=False)

    # df = pd.read_csv(project_path + "Tracks_PE{}.csv".format(edge_dist))
    df = icaps.filter_track_length(df, threshold=track_length, tally=True, silent=True)
    # df.to_csv(out_path, index=False)

    print(np.unique(df["particle"].to_numpy()).shape[0])


if __name__ == '__main__':
    main()




