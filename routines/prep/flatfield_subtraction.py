import icaps
import numpy as np

fit1 = [icaps.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [icaps.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (260300, )

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/LDM/"
orig_path = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_002/"
# orig_path = project_path + "orig/"
out_path = project_path + "ffc/"
ff_path = project_path + "ff_216700_260300.npy"
thresh = 0


def main():
    ff = icaps.load_img(ff_path)
    icaps.ff_subtract_bulk(orig_path, out_path, ff, thresh, fits=[fit1, fit2],
                           fit_cutoffs=fit_cutoffs, silent=False, clear_out=True)
    # icaps.ff_subtract_bulk(orig_path + "(95784:95785)*.bmp", out_path, ff, thresh, silent=False, clear_out=False)


if __name__ == '__main__':
    main()





