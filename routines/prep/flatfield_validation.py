import icaps
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
ffs_path = drive_letter + ":/icaps/data/FFs/"
orig_path = ffs_path + "orig/"
# texus_path1 = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_001/"
# texus_path2 = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_002/"
# data_path = ffs_path + "empty_frames.csv"


def main():
    # empty_frames = pd.read_csv(data_path, index_col=0, header=None)[1].to_numpy()
    # icaps.mk_folder(ffs_path + "orig/")
    #
    # for frame in empty_frames:
    #     file = icaps.generate_file(frame)
    #     if frame < 20000:
    #         img = icaps.load_img(texus_path1 + file)
    #     else:
    #         img = icaps.load_img(texus_path2 + file)
    #     icaps.save_img(img, ffs_path + "orig/" + file)

    # files = icaps.get_files(orig_path)
    # pb = icaps.ProgressBar(len(files))
    # imax = np.zeros(len(files))
    # imean = np.zeros(len(files))
    # itot = np.zeros(len(files))
    # frames = np.zeros(len(files))
    # for i, file in enumerate(files):
    #     frames[i] = icaps.get_frame(file)
    #     img = icaps.load_img(orig_path + file)
    #     img = icaps.strip_header(img)
    #     imax[i] = np.amax(img)
    #     imean[i] = np.mean(img)
    #     itot[i] = np.sum(img)
    #     pb.tick()
    # pb.finish()
    # df = pd.DataFrame(data={"frame": frames,
    #                         "max": imax,
    #                         "mean": imean,
    #                         "total": itot})
    # df.to_csv(ffs_path + "data.csv")

    df = pd.read_csv(ffs_path + "data.csv", index_col=0)
    _, ax = plt.subplots()
    ax.scatter(df["frame"] / 1000, df["max"], color="black", s=1, label="Max")
    ax.scatter(df["frame"] / 1000, df["mean"], color="gray", s=1, label="Mean")
    ax.scatter(df["frame"] / 1000, df["total"]/np.amax(df["total"])*100, color="blue", s=1, label="Total")
    ax.legend()
    ax.grid()
    ax.set_axisbelow(True)
    plt.show()


if __name__ == '__main__':
    main()

