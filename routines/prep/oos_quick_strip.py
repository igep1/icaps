import icaps

drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
# in_path = drive_letter + ":/icaps/texus56_SRE_20191115/OOS/XZ/000000000_10h_11m_09s_936ms/"
in_path = drive_letter + ":/icaps/texus56_SRE_20191115/OOS/YZ/000000000_10h_11m_11s_462ms/"
project_path = drive_letter + ":/icaps/data/OOS_YZ/"
orig_path = project_path + "orig/"
strip_path = project_path + "orig_strip/"
from_ = 10149
to_ = 18435
step = 1
frmt = "bmp"
id_string = "({from_}:{to_}:{step}).{frmt}".format(from_=from_, to_=to_, step=step, frmt=frmt)


def main():
    # icaps.mk_folder(orig_path)
    # icaps.mk_folder(strip_path)
    # icaps.copy_files(in_path + id_string, orig_path, show_progress=True)

    files = icaps.get_files(orig_path + id_string)
    pb = icaps.ProgressBar(len(files), title="Stripping OOS Images")
    for file in files:
        img = icaps.load_img(orig_path + file)
        img = icaps.crop(img, tl=(0, 120), br=(1024, 670))
        icaps.save_img(img, strip_path + file)
        pb.tick()
    pb.finish()


if __name__ == '__main__':
    main()









